#pragma once

#include "math/Matrix.hpp"

#include <type_traits>

namespace pm { namespace math {

template <typename Arithmetic>
Vector<Arithmetic, 4> hamiltonProduct(const Vector<Arithmetic, 4>& v1, const Vector<Arithmetic, 4>& v2);

template <typename Arithmetic, bool RowMajor>
Arithmetic determinant(const Matrix<Arithmetic, 3, 3, RowMajor>& source);

template <typename Arithmetic, bool RowMajor>
Matrix<Arithmetic, 3, 3, RowMajor> inverse(const Matrix<Arithmetic, 3, 3, RowMajor>& source);

template <typename Arithmetic>
Vector<Arithmetic, 3> crossProduct(const Vector<Arithmetic, 3>& v1, const Vector<Arithmetic, 3>& v2);

template <typename Arithmetic, unsigned Rows>
Arithmetic length(const Vector<Arithmetic, Rows>& v);

template <typename Arithmetic, unsigned Rows>
Arithmetic lengthSq(const Vector<Arithmetic, Rows>& v);

template <typename Arithmetic, unsigned Rows>
Vector<Arithmetic, Rows> normalize(const Vector<Arithmetic, Rows>& v);

template <typename Arithmetic, unsigned Rows>
Vector<Arithmetic, Rows>& normalizeSelf(Vector<Arithmetic, Rows>& v);

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Cols, Rows, RowMajor> transpose(const Matrix<Arithmetic, Rows, Cols, RowMajor>& m);

template <typename Arithmetic, unsigned RowsCols, bool RowMajor>
Matrix<Arithmetic, RowsCols, RowsCols, RowMajor>& transposeSelf(
	Matrix<Arithmetic, RowsCols, RowsCols, RowMajor>& m
);

//  Matrix-Matrix operators:

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor> operator+(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m1, 
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
);

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor> operator-(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m1, 
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
);

template <typename Arithmetic, unsigned Rows1, unsigned Cols2, unsigned Rows2Cols1, bool RowMajor>
Matrix<Arithmetic, Rows1, Cols2, RowMajor> operator*(
	const Matrix<Arithmetic, Rows1, Rows2Cols1, RowMajor>& m1, 
	const Matrix<Arithmetic, Rows2Cols1, Cols2, RowMajor>& m2
);

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor>& operator+=(
	Matrix<Arithmetic, Rows, Cols, RowMajor>& m1, 
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
);

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor>& operator-=(
	Matrix<Arithmetic, Rows, Cols, RowMajor>& m1, 
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
);

// Self multiplication, square matrices only.
template <typename Arithmetic, unsigned RowsCols, bool RowMajor>
Matrix<Arithmetic, RowsCols, RowsCols, RowMajor>& operator*=(
	Matrix<Arithmetic, RowsCols, RowsCols, RowMajor>& m1, 
	const Matrix<Arithmetic, RowsCols, RowsCols, RowMajor>& m2
);

// Dot-product, vectors only.
template <typename Arithmetic, unsigned Rows>
Arithmetic operator*(const Vector<Arithmetic, Rows>& v1, const Vector<Arithmetic, Rows>& v2);

// Scalar-Matrix operators:

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor> operator*(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m, 
	Arithmetic s
);

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor> operator*(
	Arithmetic s,
    const Matrix<Arithmetic, Rows, Cols, RowMajor>& m
);

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor> operator/(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m, 
	Arithmetic s
);

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor>& operator*=(
	Matrix<Arithmetic, Rows, Cols, RowMajor>& m, 
	Arithmetic s
);

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor>& operator/=(
	Matrix<Arithmetic, Rows, Cols, RowMajor>& m, 
	Arithmetic s
);

// Unary minus:
template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor> operator-(const Matrix<Arithmetic, Rows, Cols, RowMajor>& m);

//  Comparison operators:

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
bool operator==(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m1, 
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
);

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
bool operator!=(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m1, 
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
);

} }

#include "math/MatrixOP.inl"