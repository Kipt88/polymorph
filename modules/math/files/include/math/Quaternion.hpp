#pragma once

#include "math/Matrix.hpp"
#include <type_traits>

namespace pm { namespace math {

template <typename Arithmetic>
class Quaternion;

template <typename Arithmetic>
Vector<Arithmetic, 3> operator*(const Quaternion<Arithmetic>& q, const Vector<Arithmetic, 3>& v);

template <typename Arithmetic>
Quaternion<Arithmetic> operator*(const Quaternion<Arithmetic>& q1, const Quaternion<Arithmetic>& q2);

template <typename Arithmetic>
Quaternion<Arithmetic>& operator*=(Quaternion<Arithmetic>& q1, const Quaternion<Arithmetic>& q2);

template <typename Arithmetic>
bool operator!=(const Quaternion<Arithmetic>& q1, const Quaternion<Arithmetic>& q2);

template <typename Arithmetic>
bool operator==(const Quaternion<Arithmetic>& q1, const Quaternion<Arithmetic>& q2);


/**
 * Unit quaternion class.
 */
template <typename Arithmetic>
class Quaternion
{
public:
	Quaternion();
	explicit Quaternion(const Arithmetic* data);

	Quaternion inverse() const;
	Quaternion& inverseSelf();

	Arithmetic* data();
	const Arithmetic* data() const;

	template <bool RowMajor=false>
	Matrix<Arithmetic, 4, 4, RowMajor> asAffineMatrix3() const;

	static Quaternion fromAxisAngle(const Vector<Arithmetic, 3>& axis, Arithmetic angle);

	template <typename Arithmetic2>
	operator Quaternion<Arithmetic2>() const;

	static const Quaternion IDENTITY;

private:
	std::array<Arithmetic, 4> _data;
};

} }

#include "math/Quaternion.inl"
