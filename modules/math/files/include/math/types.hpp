#pragma once

#include <cmath>
#include <Cpp11.hpp>

namespace pm { namespace math {

typedef float real_t;
typedef real_t radian_t;

constexpr real_t PI = static_cast<real_t>(3.1415926535897932384626433832795);
constexpr real_t TWO_PI = PI * static_cast<real_t>(2.0);
constexpr real_t HALF_PI = PI / static_cast<real_t>(2.0);
constexpr real_t QUARTER_PI = PI / static_cast<real_t>(4.0);

enum Dimension
{
	DYNAMIC = 0
};

typedef unsigned int index_t;

template <typename Arithmetic>
constexpr Arithmetic sign(Arithmetic s)
{
	return s > Arithmetic{ 0 } ? Arithmetic{ 1 } : s == Arithmetic{ 0 } ? Arithmetic{ 0 } : Arithmetic{ -1 };
}

template <typename Arithmetic>
constexpr Arithmetic degToRad(Arithmetic deg)
{
	return deg * (PI / static_cast<Arithmetic>(180));
}

template <typename Arithmetic>
constexpr Arithmetic radToDeg(Arithmetic rad)
{
	return rad * (static_cast<Arithmetic>(180) / PI);
}

template <typename Arithmetic>
constexpr Arithmetic square(Arithmetic val)
{
	return val * val;
}

template <typename Arithmetic>
constexpr Arithmetic pow(Arithmetic r, unsigned int i)
{
	return i == 0 ? 1 : (i == 1 ? r : r * pow(r, i - 1));
}

template <typename Arithmetic>
constexpr Arithmetic clamped(Arithmetic v, Arithmetic minV, Arithmetic maxV)
{
	return v < minV ? minV : v > maxV ? maxV : v;
}

template <typename Arithmetic, typename Scalar>
constexpr Arithmetic lerp(Arithmetic a, Arithmetic b, Scalar t)
{
	return a + (b - a) * t;
}

} }
