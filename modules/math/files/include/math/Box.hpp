#pragma once

#include "math/Matrix.hpp"

namespace pm { namespace math {

template <typename Arithmetic>
class AxisAlignedBox3
{
public:
	AxisAlignedBox3() = default;

	AxisAlignedBox3(Arithmetic minX, Arithmetic minY, Arithmetic minZ,
				    Arithmetic maxX, Arithmetic maxY, Arithmetic maxZ);

	AxisAlignedBox3(const Vector<Arithmetic, 3>& c1, 
					const Vector<Arithmetic, 3>& c2);

	bool inside(Arithmetic x, Arithmetic y, Arithmetic z) const;
	bool inside(const Vector<Arithmetic, 3>& p) const;

	const Vector<Arithmetic, 3>& min() const;
	const Vector<Arithmetic, 3>& max() const;

	Arithmetic width() const;
	Arithmetic height() const;
	Arithmetic depth() const;

	AxisAlignedBox3<Arithmetic> operator+(const Vector<Arithmetic, 3>& p) const;
	AxisAlignedBox3<Arithmetic> operator-(const Vector<Arithmetic, 3>& p) const;
	AxisAlignedBox3<Arithmetic>& operator+=(const Vector<Arithmetic, 3>& p);
	AxisAlignedBox3<Arithmetic>& operator-=(const Vector<Arithmetic, 3>& p);

private:
	Vector<Arithmetic, 3> _minP;
	Vector<Arithmetic, 3> _maxP;
};

template <typename Arithmetic>
class AxisAlignedBox2
{
public:
	AxisAlignedBox2() = default;

	explicit AxisAlignedBox2(
		Arithmetic minX, Arithmetic minY,
		Arithmetic maxX, Arithmetic maxY);

	explicit AxisAlignedBox2(
		const Vector<Arithmetic, 2>& c1, 
		const Vector<Arithmetic, 2>& c2
	);

	AxisAlignedBox2(const AxisAlignedBox3<Arithmetic>& box3);

	bool inside(Arithmetic x, Arithmetic y) const;
	bool inside(const Vector<Arithmetic, 2>& p) const;

	const Vector<Arithmetic, 2>& min() const;
	const Vector<Arithmetic, 2>& max() const;

	Vector<Arithmetic, 2> middle() const;

	Arithmetic width() const;
	Arithmetic height() const;

	AxisAlignedBox2<Arithmetic> operator+(const Vector<Arithmetic, 2>& p) const;
	AxisAlignedBox2<Arithmetic> operator-(const Vector<Arithmetic, 2>& p) const;
	AxisAlignedBox2<Arithmetic>& operator+=(const Vector<Arithmetic, 2>& p);
	AxisAlignedBox2<Arithmetic>& operator-=(const Vector<Arithmetic, 2>& p);

	template <typename Arithmetic2>
	operator AxisAlignedBox2<Arithmetic2>() const;

private:
	Vector<Arithmetic, 2> _minP;
	Vector<Arithmetic, 2> _maxP;
};

} }

#include "math/Box.inl"
