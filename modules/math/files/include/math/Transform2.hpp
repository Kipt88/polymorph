#pragma once

#include "math/Matrix.hpp"
#include "math/types.hpp"

#include <type_traits>

namespace pm { namespace math {

template <typename Arithmetic>
struct SimilarityTransform2;



template <typename Arithmetic>
Vector<Arithmetic, 2> operator*(const SimilarityTransform2<Arithmetic>& t, const Vector<Arithmetic, 2>& v);

template <typename Arithmetic>
SimilarityTransform2<Arithmetic> operator*(const SimilarityTransform2<Arithmetic>& t1, const SimilarityTransform2<Arithmetic>& t2);

template <typename Arithmetic>
SimilarityTransform2<Arithmetic>& operator*=(SimilarityTransform2<Arithmetic>& t1, const SimilarityTransform2<Arithmetic>& t2);


/**
 * 2D similarity transform (translation, rotation, uniform scale), angle preserving.
 */
template <typename Arithmetic>
struct SimilarityTransform2
{
	void transform(Vector<Arithmetic, 2>& vec) const;

	SimilarityTransform2<Arithmetic> inverse() const;
	void inverseSelf();

	template <bool RowMajor>
	operator Matrix<Arithmetic, 3, 3, RowMajor>() const;
	
	template <bool RowMajor>
	operator Matrix<Arithmetic, 4, 4, RowMajor>() const;
	
	static SimilarityTransform2<Arithmetic> createTranslation(const Vector<Arithmetic, 2>& translation);
	static SimilarityTransform2<Arithmetic> createScaling(Arithmetic uniformScale);
	static SimilarityTransform2<Arithmetic> createRotation(Arithmetic rotation);

	static const SimilarityTransform2<Arithmetic> IDENTITY;

	Vector<Arithmetic, 2> translation;
	Arithmetic scale;
	Arithmetic rotation;
};

} }

#include "math/Transform2.inl"