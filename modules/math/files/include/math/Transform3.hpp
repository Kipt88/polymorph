#pragma once

#include "math/Matrix.hpp"
#include "math/Quaternion.hpp"
#include "math/types.hpp"

#include <type_traits>

namespace pm { namespace math {

template <typename Arithmetic>
struct SimilarityTransform3;

template <typename Arithmetic>
Vector<Arithmetic, 3> operator*(const SimilarityTransform3<Arithmetic>& t, const Vector<Arithmetic, 3>& v);

template <typename Arithmetic>
SimilarityTransform3<Arithmetic> operator*(const SimilarityTransform3<Arithmetic>& t1, const SimilarityTransform3<Arithmetic>& t2);

template <typename Arithmetic>
SimilarityTransform3<Arithmetic>& operator*=(SimilarityTransform3<Arithmetic>& t1, const SimilarityTransform3<Arithmetic>& t2);

/**
 * 3D transformation class.
 */
template <typename Arithmetic>
struct SimilarityTransform3
{
public:
	void transform(Vector<Arithmetic, 3>& vec) const;

	SimilarityTransform3<Arithmetic> inverse() const;
	void inverseSelf();

	template <bool RowMajor=false>
	Matrix<Arithmetic, 4, 4, RowMajor> asAffineMatrix3() const;
	
	static SimilarityTransform3<Arithmetic> createTranslation(const Vector<Arithmetic, 3>& translation);

	static SimilarityTransform3<Arithmetic> createScaling(Arithmetic uniformScale);

	static SimilarityTransform3<Arithmetic> createRotation(const Quaternion<Arithmetic>& rotation);

	static const SimilarityTransform3<Arithmetic> IDENTITY;

	Vector<Arithmetic, 3> translation;
	Arithmetic scale;
	Quaternion<Arithmetic> rotation;
};

} }

#include "math/Transform3.inl"