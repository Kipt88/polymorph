#pragma once

#include "math/Matrix.hpp"

namespace pm { namespace math {

template <bool RowMajor>
class Projection;

template <typename Arithmetic, bool RowMajor>
Vector<Arithmetic, 2> operator*(const Projection<RowMajor>& p, const Vector<Arithmetic, 2>& v);

template <typename Arithmetic, bool RowMajor>
Vector<Arithmetic, 3> operator*(const Projection<RowMajor>& p, const Vector<Arithmetic, 3>& v);

template <typename Arithmetic, bool RowMajor>
Vector<Arithmetic, 4> operator*(const Projection<RowMajor>& p, const Vector<Arithmetic, 4>& v);

template <bool RowMajor=false>
class Projection
{
public:
	Projection();
	
	Projection(const Projection& other) = default;
	Projection& operator=(const Projection&) = default;

	const real_t* data() const;

	const Matrix<real_t, 4, 4, RowMajor>& asMatrix() const;

	void project(Vector<real_t, 2>& v) const;
	void project(Vector<real_t, 3>& v) const;
	void project(Vector<real_t, 4>& v) const;

	Projection<RowMajor> inverse() const;
	void inverseSelf();


	static Projection<RowMajor> ortho(
		real_t left, real_t right, 
		real_t bottom, real_t top, 
		real_t near = -0.5f, real_t far = 0.5f
	);

private:
	Projection(
		const Matrix<real_t, 4, 4, RowMajor>& projMat,
		const Matrix<real_t, 4, 4, RowMajor>& invMat
	);

	Matrix<real_t, 4, 4, RowMajor> _projMat;
	Matrix<real_t, 4, 4, RowMajor> _invMat;
};

} }

#include "math/Projection.inl"
