#pragma once

#include "math/types.hpp"

#include <array>
#include <initializer_list>

namespace pm { namespace math {

// Generic
template <typename Arithmetic, unsigned int Rows, unsigned int Cols, bool RowMajor = false>
class Matrix
{
public:
	static const Matrix<Arithmetic, Rows, Cols, RowMajor> ZERO;

	Matrix() = default;
	Matrix(const std::initializer_list<Arithmetic>& il);
	Matrix(const Arithmetic* data);
	
	Arithmetic* data();
	const Arithmetic* data() const;

	const Arithmetic& operator()(index_t r, index_t c) const;
	Arithmetic& operator()(index_t r, index_t c);

	template <typename Arithmetic2, bool RowMajor2>
	explicit operator Matrix<Arithmetic2, Rows, Cols, RowMajor2>() const;

private:
	std::array<Arithmetic, Rows * Cols> _data;
};

// Square
template <typename Arithmetic, unsigned int Rows, bool RowMajor>
class Matrix<Arithmetic, Rows, Rows, RowMajor>
{
public:
	static const Matrix<Arithmetic, Rows, Rows, RowMajor> ZERO;
	static const Matrix<Arithmetic, Rows, Rows, RowMajor> IDENTITY;

	Matrix() = default;
	Matrix(const std::initializer_list<Arithmetic>& il);
	Matrix(const Arithmetic* data);
	explicit Matrix(Arithmetic diagonal);
	
	Arithmetic* data();
	const Arithmetic* data() const;

	const Arithmetic& operator()(index_t r, index_t c) const;
	Arithmetic& operator()(index_t r, index_t c);

	template <typename Arithmetic2, bool RowMajor2>
	explicit operator Matrix<Arithmetic2, Rows, Rows, RowMajor2>() const;

private:
	std::array<Arithmetic, Rows * Rows> _data;
};

// Vector
template <typename Arithmetic, unsigned int Rows, bool RowMajor>
class Matrix<Arithmetic, Rows, 1, RowMajor>
{
public:
	static const Matrix<Arithmetic, Rows, 1, RowMajor> ZERO;

	Matrix() = default;
	Matrix(const std::initializer_list<Arithmetic>& il);
	Matrix(const Arithmetic* data);

	Arithmetic* data();
	const Arithmetic* data() const;

	const Arithmetic& operator()(index_t r, index_t c) const;
	Arithmetic& operator()(index_t r, index_t c);

	const Arithmetic& operator[](index_t r) const;
	Arithmetic& operator[](index_t r);

	template <typename Arithmetic2, bool RowMajor2>
	explicit operator Matrix<Arithmetic2, Rows, 1, RowMajor2>() const;

private:
	std::array<Arithmetic, Rows> _data;
};

template <typename Arithmetic, unsigned Rows, bool RowMajor = false>
using Vector = Matrix<Arithmetic, Rows, 1, RowMajor>;

} }

#include "math/Matrix.inl"

#include "math/MatrixOP.hpp"