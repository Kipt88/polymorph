#pragma once

#include "math/Matrix.hpp"
#include "math/Box.hpp"
#include <optional.hpp>
#include <vector>
#include <utility>

namespace pm { namespace math {

using param_t = float; // Curve parameter ([0, 1] for finite curves).

// Geometry types:

template <typename Arithmetic>
using Line2 = std::pair<Vector<Arithmetic, 2>, Vector<Arithmetic, 2>>;

template <typename Arithmetic>
using Polygon2 = std::vector<Vector<Arithmetic, 2>>;

template <typename Arithmetic>
struct Circle2
{
	Vector<Arithmetic, 2> position;
	Arithmetic radius;
};

// Curve getters:

template <typename Arithmetic>
Vector<Arithmetic, 2> curve(const Line2<Arithmetic>& l, param_t t);

template <typename Arithmetic>
Vector<Arithmetic, 2> curve(const Circle2<Arithmetic>& c, param_t t);

template <typename Arithmetic>
Vector<Arithmetic, 2> curve(const Polygon2<Arithmetic>& p, param_t t);

template <typename Arithmetic>
Vector<Arithmetic, 2> curveLine(const Vector<Arithmetic, 2>& p1, const Vector<Arithmetic, 2>& p2, param_t t);

template <typename Arithmetic>
Vector<Arithmetic, 2> curveCircle(const Vector<Arithmetic, 2>& p, Arithmetic r, param_t t);

template <typename Arithmetic, typename It>
Vector<Arithmetic, 2> curvePolygon(It vertBegin, It vertEnd, param_t t);

// Optional intersection types:

template <typename Arithmetic>
using single_intersection2_t = std::optional<Vector<Arithmetic, 2>>;

template <typename Arithmetic>
using double_intersection2_t = std::pair<
	std::optional<Vector<Arithmetic, 2>>,
	std::optional<Vector<Arithmetic, 2>>
>;

template <typename Arithmetic>
using n_intersection2_t = std::vector<Vector<Arithmetic, 2>>;

using single_intersection_param_t = std::optional<param_t>;
using double_intersection_param_t = std::pair<std::optional<param_t>, std::optional<param_t>>;
using n_intersection_param_t = std::vector<param_t>;

// Point hit-tests:

template <typename Arithmetic>
bool insideConvex(const Vector<Arithmetic, 2>& point, const Polygon2<Arithmetic>& polygon);

// Intersections:

template <typename Arithmetic>
single_intersection2_t<Arithmetic> intersection(const Line2<Arithmetic>& a, const Line2<Arithmetic>& b);

template <typename Arithmetic>
double_intersection2_t<Arithmetic> intersection(const Line2<Arithmetic>& l, const Circle2<Arithmetic>& c);

template <typename Arithmetic>
double_intersection2_t<Arithmetic> intersection(const Circle2<Arithmetic>& c, const Line2<Arithmetic>& l);

template <typename Arithmetic>
n_intersection2_t<Arithmetic> intersection(const Line2<Arithmetic>& l, const Polygon2<Arithmetic>& p);

template <typename Arithmetic>
n_intersection2_t<Arithmetic> intersection(const Polygon2<Arithmetic>& p, const Line2<Arithmetic>& l);

template <typename Arithmetic>
single_intersection2_t<Arithmetic> lineIntersection(
	const Vector<Arithmetic, 2>& p1, 
	const Vector<Arithmetic, 2>& p2,
	const Vector<Arithmetic, 2>& q1,
	const Vector<Arithmetic, 2>& q2
);

template <typename Arithmetic>
double_intersection2_t<Arithmetic> lineCircleIntersection(
	const Vector<Arithmetic, 2>& p1,
	const Vector<Arithmetic, 2>& p2,
	const Vector<Arithmetic, 2>& c,
	Arithmetic r
);

template <typename Arithmetic, typename It>
n_intersection2_t<Arithmetic> linePolygonIntersection(
	It vertBegin,
	It vertEnd,
	const Vector<Arithmetic, 2>& p1,
	const Vector<Arithmetic, 2>& p2
);

// Parameter of line in intersection:

template <typename Arithmetic>
single_intersection_param_t lineIntersectionParam(
	const Vector<Arithmetic, 2>& p1,
	const Vector<Arithmetic, 2>& p2,
	const Vector<Arithmetic, 2>& q1,
	const Vector<Arithmetic, 2>& q2
);

template <typename Arithmetic>
double_intersection_param_t lineCircleIntersectionParam(
	const Vector<Arithmetic, 2>& p1,
	const Vector<Arithmetic, 2>& p2,
	const Vector<Arithmetic, 2>& c,
	Arithmetic r
);

template <typename Arithmetic, typename It>
n_intersection_param_t linePolygonIntersectionParam(
	It vertBegin,
	It vertEnd,
	const Vector<Arithmetic, 2>& p1,
	const Vector<Arithmetic, 2>& p2
);

} }

#include "math/Geometry.inl"