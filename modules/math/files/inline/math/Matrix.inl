#include "math/Matrix.hpp"

#include <Assert.hpp>
#include <utility>
#include <memory>

namespace pm { namespace math {

#define MATRIX_TEMPLATES_DECLARATION \
	typename Arithmetic, unsigned int Rows, unsigned int Cols, bool RowMajor

#define MATRIX_TEMPLATES \
	Arithmetic, Rows, Cols, RowMajor

#define MATRIX_SQUARE_TEMPLATES_DECLARATION \
	typename Arithmetic, unsigned int Rows, bool RowMajor

#define MATRIX_SQUARE_TEMPLATES \
	Arithmetic, Rows, Rows, RowMajor

#define MATRIX_COLUMN_TEMPLATES_DECLARATION \
	typename Arithmetic, unsigned int Rows, bool RowMajor

#define MATRIX_COLUMN_TEMPLATES \
	Arithmetic, Rows, 1, RowMajor


namespace detail {

template <unsigned int Rows, unsigned int Cols, bool RowMajor>
struct index_helper
{
	static constexpr index_t index(index_t row, index_t col)
	{
		return col * Rows + row;
	}
};

template <unsigned int Rows, unsigned int Cols>
struct index_helper<Rows, Cols, true>
{
	static constexpr index_t index(index_t row, index_t col)
	{
		return row * Cols + col;
	}
};

template <unsigned int Rows, unsigned int Cols, bool RowMajor>
index_t index(index_t row, index_t col)
{
	ASSERT(row < Rows && col < Cols, "Bad index.");
	return index_helper<Rows, Cols, RowMajor>::index(row, col);
}

template <typename Arithmetic, unsigned int Rows, unsigned int Cols>
std::array<Arithmetic, Rows * Cols> dataZero()
{
	std::array<Arithmetic, Rows * Cols> data;

	for (auto& d : data)
		d = Arithmetic(0);

	return data;
}

template <typename Arithmetic, unsigned int Rows>
std::array<Arithmetic, Rows * Rows> dataDiagonal(Arithmetic val)
{
	std::array<Arithmetic, Rows * Rows> data;

	for (index_t i = 0; i < Rows; ++i)
	{
		for (index_t j = 0; j < Rows; ++j)
		{
			if (i == j)
				data[i * Rows + j] = val;
			else
				data[i * Rows + j] = Arithmetic(0);
		}
	}

	return data;
}

template <
	typename Arithmetic, 
	unsigned int Rows, 
	unsigned int Cols, 
	bool RowMajor,
	bool SourceRowMajor,
	typename It
>
std::array<Arithmetic, Rows * Cols> dataRange(It start)
{
	std::array<Arithmetic, Rows * Cols> data;

	for (index_t i = 0; i < Rows; ++i)
	{
		for (index_t j = 0; j < Cols; ++j)
		{
			data[detail::index<Rows, Cols, RowMajor>(i, j)] =
				static_cast<const Arithmetic&>(*(start + detail::index<Rows, Cols, SourceRowMajor>(i, j)));
		}
	}

	return data;
}

}

// -- Constructors --

template <MATRIX_TEMPLATES_DECLARATION>
Matrix<MATRIX_TEMPLATES>::Matrix(const std::initializer_list<Arithmetic>& data)
: _data(detail::dataRange<Arithmetic, Rows, Cols, RowMajor, true>(data.begin()))
{
}

template <MATRIX_SQUARE_TEMPLATES_DECLARATION>
Matrix<MATRIX_SQUARE_TEMPLATES>::Matrix(const std::initializer_list<Arithmetic>& data)
: _data(detail::dataRange<Arithmetic, Rows, Rows, RowMajor, true>(data.begin()))
{
}

template <MATRIX_COLUMN_TEMPLATES_DECLARATION>
Matrix<MATRIX_COLUMN_TEMPLATES>::Matrix(const std::initializer_list<Arithmetic>& data)
: _data(detail::dataRange<Arithmetic, Rows, 1, RowMajor, true>(data.begin()))
{
}

//

template <MATRIX_TEMPLATES_DECLARATION>
Matrix<MATRIX_TEMPLATES>::Matrix(const Arithmetic* data)
: _data(detail::dataRange<Arithmetic, Rows, Cols, RowMajor, true>(data))
{
}

template <MATRIX_SQUARE_TEMPLATES_DECLARATION>
Matrix<MATRIX_SQUARE_TEMPLATES>::Matrix(const Arithmetic* data)
: _data(detail::dataRange<Arithmetic, Rows, Rows, RowMajor, true>(data))
{
}

template <MATRIX_COLUMN_TEMPLATES_DECLARATION>
Matrix<MATRIX_COLUMN_TEMPLATES>::Matrix(const Arithmetic* data)
: _data(detail::dataRange<Arithmetic, Rows, 1, RowMajor, true>(data))
{
}

//

template <MATRIX_SQUARE_TEMPLATES_DECLARATION>
Matrix<MATRIX_SQUARE_TEMPLATES>::Matrix(Arithmetic s)
: _data(detail::dataDiagonal<Arithmetic, Rows>(s))
{
}

// -- data() --

template <MATRIX_TEMPLATES_DECLARATION>
Arithmetic* Matrix<MATRIX_TEMPLATES>::data()
{
	return _data.data();
}

template <MATRIX_SQUARE_TEMPLATES_DECLARATION>
Arithmetic* Matrix<MATRIX_SQUARE_TEMPLATES>::data()
{
	return _data.data();
}

template <MATRIX_COLUMN_TEMPLATES_DECLARATION>
Arithmetic* Matrix<MATRIX_COLUMN_TEMPLATES>::data()
{
	return _data.data();
}

//

template <MATRIX_TEMPLATES_DECLARATION>
const Arithmetic* Matrix<MATRIX_TEMPLATES>::data() const
{
	return _data.data();
}

template <MATRIX_SQUARE_TEMPLATES_DECLARATION>
const Arithmetic* Matrix<MATRIX_SQUARE_TEMPLATES>::data() const
{
	return _data.data();
}

template <MATRIX_COLUMN_TEMPLATES_DECLARATION>
const Arithmetic* Matrix<MATRIX_COLUMN_TEMPLATES>::data() const
{
	return _data.data();
}

// -- operator() --

template <MATRIX_TEMPLATES_DECLARATION>
const Arithmetic& Matrix<MATRIX_TEMPLATES>::operator()(index_t r, index_t c) const
{
	return _data[detail::index<Rows, Cols, RowMajor>(r, c)];
}

template <MATRIX_SQUARE_TEMPLATES_DECLARATION>
const Arithmetic& Matrix<MATRIX_SQUARE_TEMPLATES>::operator()(index_t r, index_t c) const
{
	return _data[detail::index<Rows, Rows, RowMajor>(r, c)];
}

template <MATRIX_COLUMN_TEMPLATES_DECLARATION>
const Arithmetic& Matrix<MATRIX_COLUMN_TEMPLATES>::operator()(index_t r, index_t c) const
{
	return _data[detail::index<Rows, 1, RowMajor>(r, c)];
}

//

template <MATRIX_TEMPLATES_DECLARATION>
Arithmetic& Matrix<MATRIX_TEMPLATES>::operator()(index_t r, index_t c)
{
	return _data[detail::index<Rows, Cols, RowMajor>(r, c)];
}

template <MATRIX_SQUARE_TEMPLATES_DECLARATION>
Arithmetic& Matrix<MATRIX_SQUARE_TEMPLATES>::operator()(index_t r, index_t c)
{
	return _data[detail::index<Rows, Rows, RowMajor>(r, c)];
}

template <MATRIX_COLUMN_TEMPLATES_DECLARATION>
Arithmetic& Matrix<MATRIX_COLUMN_TEMPLATES>::operator()(index_t r, index_t c)
{
	return _data[detail::index<Rows, 1, RowMajor>(r, c)];
}

// -- operator[] --

template <MATRIX_COLUMN_TEMPLATES_DECLARATION>
const Arithmetic& Matrix<MATRIX_COLUMN_TEMPLATES>::operator[](index_t r) const
{
	return _data[detail::index<Rows, 1, RowMajor>(r, 0)];
}

//

template <MATRIX_COLUMN_TEMPLATES_DECLARATION>
Arithmetic& Matrix<MATRIX_COLUMN_TEMPLATES>::operator[](index_t r)
{
	return _data[detail::index<Rows, 1, RowMajor>(r, 0)];
}

// -- cast operator --

template <MATRIX_TEMPLATES_DECLARATION>
template <typename Arithmetic2, bool RowMajor2>
Matrix<MATRIX_TEMPLATES>::operator Matrix<Arithmetic2, Rows, Cols, RowMajor2>() const
{
	Matrix<Arithmetic2, Rows, Cols, RowMajor2> rMat;

	rMat._data = detail::dataRange<Arithmetic, Rows, Cols, RowMajor, RowMajor2>(_data.begin());

	return rMat;
}

template <MATRIX_SQUARE_TEMPLATES_DECLARATION>
template <typename Arithmetic2, bool RowMajor2>
Matrix<MATRIX_SQUARE_TEMPLATES>::operator Matrix<Arithmetic2, Rows, Rows, RowMajor2>() const
{
	Matrix<Arithmetic2, Rows, Rows, RowMajor2> rMat;

	auto d = detail::dataRange<Arithmetic, Rows, Rows, RowMajor, RowMajor2>(_data.begin());

	for (unsigned int i = 0; i < d.size(); ++i)
		rMat.data()[i] = d[i];

	return rMat;
}

template <MATRIX_COLUMN_TEMPLATES_DECLARATION>
template <typename Arithmetic2, bool RowMajor2>
Matrix<MATRIX_COLUMN_TEMPLATES>::operator Matrix<Arithmetic2, Rows, 1, RowMajor2>() const
{
	Matrix<Arithmetic2, Rows, 1, RowMajor2> rMat;

	auto d = detail::dataRange<Arithmetic, Rows, 1, RowMajor, RowMajor2>(_data.begin());
	
	for (unsigned int i = 0; i < d.size(); ++i)
		rMat.data()[i] = d[i];

	return rMat;
}

// -- Constants --

template <MATRIX_TEMPLATES_DECLARATION>
const Matrix<MATRIX_TEMPLATES> Matrix<MATRIX_TEMPLATES>::ZERO = 
	detail::dataZero<Arithmetic, Rows, Cols>().data();

template <MATRIX_SQUARE_TEMPLATES_DECLARATION>
const Matrix<MATRIX_SQUARE_TEMPLATES> Matrix<MATRIX_SQUARE_TEMPLATES>::ZERO = 
	detail::dataZero<Arithmetic, Rows, Rows>().data();

template <MATRIX_SQUARE_TEMPLATES_DECLARATION>
const Matrix<MATRIX_SQUARE_TEMPLATES> Matrix<MATRIX_SQUARE_TEMPLATES>::IDENTITY = 
	detail::dataDiagonal<Arithmetic, Rows>(Arithmetic{ 1 }).data();

template <MATRIX_COLUMN_TEMPLATES_DECLARATION>
const Matrix<MATRIX_COLUMN_TEMPLATES> Matrix<MATRIX_COLUMN_TEMPLATES>::ZERO =
	detail::dataZero<Arithmetic, Rows, 1>().data();

} }


#undef MATRIX_TEMPLATES_DECLARATION
#undef MATRIX_TEMPLATES
#undef MATRIX_SQUARE_TEMPLATES_DECLARATION
#undef MATRIX_SQUARE_TEMPLATES
#undef MATRIX_COLUMN_TEMPLATES_DECLARATION
#undef MATRIX_COLUMN_TEMPLATES