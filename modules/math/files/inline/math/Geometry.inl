#include "math/Geometry.hpp"
#include <iterator>
#include <algorithm>

namespace pm { namespace math {

template <typename Arithmetic>
Vector<Arithmetic, 2> curve(const Line2<Arithmetic>& l, param_t t)
{
	return curveLine(l.first, l.second, t);
}

template <typename Arithmetic>
Vector<Arithmetic, 2> curve(const Circle2<Arithmetic>& c, param_t t)
{
	return curveCircle(c.position, c.radius, t);
}

template <typename Arithmetic>
Vector<Arithmetic, 2> curve(const Polygon2<Arithmetic>& p, param_t t)
{
	return curvePolygon(p.begin(), p.end(), t);
}

template <typename Arithmetic>
Vector<Arithmetic, 2> curveLine(const Vector<Arithmetic, 2>& p1, const Vector<Arithmetic, 2>& p2, param_t t)
{
	return p1 + (p2 - p1) * static_cast<Arithmetic>(t);
}

template <typename Arithmetic>
Vector<Arithmetic, 2> curveCircle(const Vector<Arithmetic, 2>& p, Arithmetic r, param_t t)
{
	param_t ang = t * TWO_PI;

	return {
		p[0] + r * std::cos(ang),
		p[1] + r * std::sin(ang)
	};
}

template <typename Arithmetic, typename It>
Vector<Arithmetic, 2> curvePolygon(It vertBegin, It vertEnd, param_t t)
{
	// it:       b                   e
	// t:        0                   1
	// tIndex:   0    1    2    3    4
	//          v1 - v2 - v3 - v4 - v1

	auto count = std::distance(vertBegin, vertEnd);
	auto size = static_cast<param_t>(count + 1);

	auto tIndex = t * size;

	auto lineIndex = static_cast<unsigned int>(tIndex) % (count + 1); // t == 1 same as t == 0.
	auto lineT = tIndex - static_cast<param_t>(lineIndex);

	auto itA = std::next(vertBegin, lineIndex);
	if (itA == vertEnd)
		itA = vertBegin;

	auto itB = std::next(itA);
	if (itB == vertEnd)
		itB = vertBegin;

	return curveLine(*itA, *itB, lineT);
}

template <typename Arithmetic>
bool insideConvex(const Vector<Arithmetic, 2>& point, const Polygon2<Arithmetic>& polygon)
{
	for (unsigned int i = 0; i < polygon.size(); ++i)
	{
		auto v0 = polygon[i];
		auto v1 = polygon[(i + 1) % polygon.size()];

		auto crossZ = (point[0] - v0[0]) * (v1[1] - v0[1]) - (point[1] - v0[1]) * (v1[0] - v0[0]);

		if (crossZ > 0.0)
			return false;
	}

	return true;
}

template <typename Arithmetic>
single_intersection2_t<Arithmetic> intersection(const Line2<Arithmetic>& a, const Line2<Arithmetic>& b)
{
	return lineIntersection(a.first, a.second, b.first, b.second);
}

template <typename Arithmetic>
double_intersection2_t<Arithmetic> intersection(const Line2<Arithmetic>& l, const Circle2<Arithmetic>& c)
{
	return lineCircleIntersection(l.first, l.second, c.position, c.radius);
}

template <typename Arithmetic>
double_intersection2_t<Arithmetic> intersection(const Circle2<Arithmetic>& c, const Line2<Arithmetic>& l)
{
	return lineCircleIntersection(l.first, l.second, c.position, c.radius);
}

template <typename Arithmetic>
n_intersection2_t<Arithmetic> intersection(const Line2<Arithmetic>& l, const Polygon2<Arithmetic>& p)
{
	return linePolygonIntersection(p.begin(), p.end(), l.first, l.second);
}

template <typename Arithmetic>
n_intersection2_t<Arithmetic> intersection(const Polygon2<Arithmetic>& p, const Line2<Arithmetic>& l)
{
	return linePolygonIntersection(p.begin(), p.end(), l.first, l.second);
}

template <typename Arithmetic>
single_intersection2_t<Arithmetic> lineIntersection(
	const Vector<Arithmetic, 2>& p1,
	const Vector<Arithmetic, 2>& p2,
	const Vector<Arithmetic, 2>& q1,
	const Vector<Arithmetic, 2>& q2
)
{
	if (auto t = lineIntersectionParam(p1, p2, q1, q2))
		return curveLine(p1, p2, *t);

	return std::nullopt;
}

template <typename Arithmetic>
double_intersection2_t<Arithmetic> lineCircleIntersection(
	const Vector<Arithmetic, 2>& p1,
	const Vector<Arithmetic, 2>& p2,
	const Vector<Arithmetic, 2>& center,
	Arithmetic r
)
{
	double_intersection2_t<Arithmetic> rVal;

	auto t = lineCircleIntersectionParam(p1, p2, center, r);

	if (t.first)
		rVal.first = curveLine(p1, p2, *t.first);

	if (t.second)
		rVal.second = curveLine(p1, p2, *t.second);

	return rVal;
}

template <typename Arithmetic, typename It>
n_intersection2_t<Arithmetic> linePolygonIntersection(
	It vertBegin,
	It vertEnd,
	const Vector<Arithmetic, 2>& p1,
	const Vector<Arithmetic, 2>& p2
)
{
	n_intersection2_t<Arithmetic> rVal;

	auto ts = linePolygonIntersectionParam(vertBegin, vertEnd, p1, p2);

	for (const auto& t : ts)
		rVal.push_back(curveLine(p1, p2, t));

	return rVal;
}

template <typename Arithmetic>
single_intersection_param_t lineIntersectionParam(
	const Vector<Arithmetic, 2>& p1,
	const Vector<Arithmetic, 2>& p2,
	const Vector<Arithmetic, 2>& q1,
	const Vector<Arithmetic, 2>& q2
)
{
	auto deltaP = p2 - p1;
	auto deltaQ = q2 - q1;

	auto denumerator = (deltaP * deltaP) * (deltaQ * deltaQ) - square(deltaP * deltaQ);

	if (denumerator == 0)
		return std::nullopt;

	auto t = ((deltaQ * deltaQ) * (deltaP * (q1 - p1)) - (deltaP * deltaQ) * (deltaQ * (q1 - p1))) 
		/ denumerator;

	auto s = ((deltaP * deltaP) * (deltaQ * (p1 - q1)) - (deltaQ * deltaP) * (deltaP * (p1 - q1)))
		/ denumerator;

	if (Arithmetic(0) <= t && t < Arithmetic(1) && Arithmetic(0) <= s && s < Arithmetic(1))
		return t;
	
	return std::nullopt;
}

template <typename Arithmetic>
double_intersection_param_t lineCircleIntersectionParam(
	const Vector<Arithmetic, 2>& p1,
	const Vector<Arithmetic, 2>& p2,
	const Vector<Arithmetic, 2>& center,
	Arithmetic r
)
{
	auto c_to_p1 = p1 - center;
	auto deltaP = p2 - p1;
	
	auto a = deltaP * deltaP;
	auto b = Arithmetic(2) * (c_to_p1 * deltaP);
	auto c = c_to_p1 * c_to_p1 - square(r);

	auto discriminant = std::sqrt(square(b) - Arithmetic(4) * a * c);

	if (discriminant < Arithmetic(0))
		return { std::nullopt, std::nullopt };

	double_intersection_param_t rVal;

	Arithmetic t1 = (-b - discriminant) / (Arithmetic(2) * a);
	Arithmetic t2 = (-b + discriminant) / (Arithmetic(2) * a);

	if (Arithmetic(0) <= t1 && t1 < Arithmetic(1))
		rVal.first = t1;

	if (Arithmetic(0) <= t2 && t2 < Arithmetic(1))
	{
		if (!rVal.first)
			rVal.first = t2;
		else
			rVal.second = t2;
	}

	return rVal;
}

template <typename Arithmetic, typename It>
n_intersection_param_t linePolygonIntersectionParam(
	It vertBegin,
	It vertEnd,
	const Vector<Arithmetic, 2>& p1,
	const Vector<Arithmetic, 2>& p2
)
{
	ASSERT(std::distance(vertBegin, vertEnd) >= 3, "Not a proper polygon.");

	n_intersection_param_t rVal;

	for (auto it = vertBegin; it != vertEnd; ++it)
	{
		auto next = std::next(it);

		if (next == vertEnd)
			next = vertBegin;

		if (auto t = lineIntersectionParam(p1, p2, *it, *next))
			rVal.push_back(*t);
	}

	std::sort(rVal.begin(), rVal.end());

	return rVal;
}


} }
