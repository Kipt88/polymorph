#include <Assert.hpp>

#include <utility>

namespace pm { namespace math {

// Operators:

template <typename Arithmetic>
Vector<Arithmetic, 4> hamiltonProduct(const Vector<Arithmetic, 4>& v1, const Vector<Arithmetic, 4>& v2)
{
	auto a1 = v1[3];
	auto b1 = v1[0];
	auto c1 = v1[1];
	auto d1 = v1[2];
	
	auto a2 = v2[3];
	auto b2 = v2[0];
	auto c2 = v2[1];
	auto d2 = v2[2];

	return {
		a1 * b2 + b1 * a2 + c1 * d2 - d1 * c2,
		a1 * c2 - b1 * d2 + c1 * a2 + d1 * b2,
		a1 * d2 + b1 * c2 - c1 * b2 + d1 * a2,
		a1 * a2 - b1 * b2 - c1 * c2 - d1 * d2
	};
}

template <typename Arithmetic, bool RowMajor>
Arithmetic determinant(const Matrix<Arithmetic, 3, 3, RowMajor>& source)
{
	//
	//      | a b c |
	// det( | d e f | ) = aei + bfg + cdh - ceg - bdi - afh
	//      | g h i |
	//

	const auto
		a = source(0, 0),
		b = source(0, 1),
		c = source(0, 2),
		d = source(1, 0),
		e = source(1, 1),
		f = source(1, 2),
		g = source(2, 0),
		h = source(2, 1),
		i = source(2, 2);

	return a * e * i
		 + b * f * g
		 + c * d * h
		 - c * e * g
		 - b * d * i
		 - a * f * h;
}

template <typename Arithmetic, bool RowMajor>
Matrix<Arithmetic, 3, 3, RowMajor> inverse(const Matrix<Arithmetic, 3, 3, RowMajor>& source)
{
	//
	// | a b c |-1    | A D G |T             | a b c |
	// | d e f |    = | B E H |  * (1 / det( | d e f | ))
	// | g h k |      | C F K |              | g h k |
	//
	// A = (ek - fh)
	// B = -(dk - fg)
	// C = (dh - eg)
	// D = -(bk - ch)
	// E = (ak - cg)
	// F = -(ah - bg)
	// G = (bf - ce)
	// H = -(af - cd)
	// K = (ae - bd)
	//

	const auto
		a = source(0, 0),
		b = source(0, 1),
		c = source(0, 2),
		d = source(1, 0),
		e = source(1, 1),
		f = source(1, 2),
		g = source(2, 0),
		h = source(2, 1),
		k = source(2, 2);

	const auto
		A = e * k - f * h,
		B = -(d * k - f * g),
		C = d * h - e * g,
		D = -(b * k - c * h),
		E = a * k - c * g,
		F = -(a * h - b * g),
		G = b * f - c * e,
		H = -(a * f - c * d),
		K = a * e - b * d;

	const auto det = determinant(source);

	return {
		A / det, D / det, G / det,
		B / det, E / det, H / det,
		C / det, F / det, K / det
	};
}

template <typename Arithmetic>
Vector<Arithmetic, 3> crossProduct(const Vector<Arithmetic, 3>& v1, const Vector<Arithmetic, 3>& v2)
{
	Vector<Arithmetic, 3> rVec;

	rVec[0] = v1[1] * v2[2] - v1[2] * v2[1];
	rVec[1] = v1[2] * v2[0] - v1[0] * v2[2];
	rVec[2] = v1[0] * v2[1] - v1[1] * v2[0];

	return rVec;
}

template <typename Arithmetic, unsigned Rows>
Arithmetic length(const Vector<Arithmetic, Rows>& v)
{
	return std::sqrt(lengthSq(v));
}

template <typename Arithmetic, unsigned Rows>
Arithmetic lengthSq(const Vector<Arithmetic, Rows>& v)
{
	return v * v;
}

template <typename Arithmetic, unsigned Rows>
Vector<Arithmetic, Rows> normalize(const Vector<Arithmetic, Rows>& v)
{
	Vector<Arithmetic, Rows> rVec{ v };
	normalizeSelf(rVec);

	return rVec;
}

template <typename Arithmetic, unsigned Rows>
Vector<Arithmetic, Rows>& normalizeSelf(Vector<Arithmetic, Rows>& v)
{
	return v /= length(v);
}

template <
	typename Arithmetic,
	unsigned Rows,
	unsigned Cols,
	bool RowMajor
>
Matrix<Arithmetic, Cols, Rows, RowMajor> transpose(const Matrix<Arithmetic, Rows, Cols, RowMajor>& m)
{
	Matrix<Arithmetic, Cols, Rows, RowMajor> rMat;

	for (index_t j = 0; j < Cols; ++j)
		for (index_t i = 0; i < Rows; ++i)
			rMat(j, i) = m(i, j);

	return rMat;
}

template <
	typename Arithmetic,
	unsigned RowsCols,
	bool RowMajor
>
Matrix<Arithmetic, RowsCols, RowsCols, RowMajor>& transposeSelf(
	Matrix<Arithmetic, RowsCols, RowsCols, RowMajor>& m
)
{
	for (index_t j = 0; j < RowsCols / 2; ++j)
		for (index_t i = 0; i < RowsCols / 2; ++i)
			std::swap(m(i, j), m(j, i));
}

//  Matrix-Matrix operators:

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor> operator+(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m1,
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
)
{
	Matrix<Arithmetic, Rows, Cols, RowMajor> rMat;

	for (index_t row = 0; row < Rows; ++row)
		for (index_t col = 0; col < Cols; ++col)
			rMat(row, col) = m1(row, col) + m2(row, col);

	return rMat;
}

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor> operator-(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m1,
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
)
{
	Matrix<Arithmetic, Rows, Cols, RowMajor> rMat;

	for (index_t row = 0; row < Rows; ++row)
		for (index_t col = 0; col < Cols; ++col)
			rMat(row, col) = m1(row, col) - m2(row, col);

	return rMat;
}

template <typename Arithmetic, unsigned Rows1, unsigned Cols2, unsigned Rows2Cols1, bool RowMajor>
Matrix<Arithmetic, Rows1, Cols2, RowMajor> operator*(
	const Matrix<Arithmetic, Rows1, Rows2Cols1, RowMajor>& m1, 
	const Matrix<Arithmetic, Rows2Cols1, Cols2, RowMajor>& m2
)
{
	Matrix<Arithmetic, Rows1, Cols2, RowMajor> rMat;

	for (index_t row = 0; row < Rows1; ++row)
	{
		for (index_t col = 0; col < Cols2; ++col)
		{
			auto& value = rMat(row, col);

			value = 0;

			for (index_t k = 0; k < Rows2Cols1; ++k)
				value += m1(row, k) * m2(k, col);
		}
	}

	return rMat;
}

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor>& operator+=(
	Matrix<Arithmetic, Rows, Cols, RowMajor>& m1, 
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
)
{
	for (index_t row = 0; row < Rows; ++row)
		for (index_t col = 0; col < Cols; ++col)
			m1(row, col) += m2(row, col);

	return m1;
}

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor>& operator-=(
	Matrix<Arithmetic, Rows, Cols, RowMajor>& m1, 
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
)
{
	for (index_t row = 0; row < Rows; ++row)
		for (index_t col = 0; col < Cols; ++col)
			m1(row, col) -= m2(row, col);

	return m1;
}

// Self multiplication, square matrices only.
template <typename Arithmetic, unsigned RowsCols, bool RowMajor>
Matrix<Arithmetic, RowsCols, RowsCols, RowMajor>& operator*=(
	Matrix<Arithmetic, RowsCols, RowsCols, RowMajor>& m1, 
	const Matrix<Arithmetic, RowsCols, RowsCols, RowMajor>& m2
)
{
	return m1 = m1 * m2;
}

// Dot-product, vectors only.
template <typename Arithmetic, unsigned Rows>
Arithmetic operator*(const Vector<Arithmetic, Rows>& v1, const Vector<Arithmetic, Rows>& v2)
{
	Arithmetic rVal{ 0 };

	for (index_t row = 0; row < Rows; ++row)
		rVal += v1(row, 0) * v2(row, 0);

	return rVal;
}

// Scalar-Matrix operators:

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor> operator*(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m, 
	Arithmetic s
)
{
	Matrix<Arithmetic, Rows, Cols, RowMajor> rMat{ m };

	return rMat *= s;
}

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor> operator*(
	Arithmetic s,
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m
)
{
	return m * s;
}

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor> operator/(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m, 
	Arithmetic s
)
{
	Matrix<Arithmetic, Rows, Cols, RowMajor> rMat{ m };

	return rMat /= s;
}

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor>& operator*=(
	Matrix<Arithmetic, Rows, Cols, RowMajor>& m, 
	Arithmetic s
)
{
	for (index_t row = 0; row < Rows; ++row)
		for (index_t col = 0; col < Cols; ++col)
			m(row, col) *= s;

	return m;
}

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor>& operator/=(
	Matrix<Arithmetic, Rows, Cols, RowMajor>& m, 
	Arithmetic s
)
{
	for (index_t row = 0; row < Rows; ++row)
		for (index_t col = 0; col < Cols; ++col)
			m(row, col) /= s;

	return m;
}

//  Unary operator:
template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
Matrix<Arithmetic, Rows, Cols, RowMajor> operator-(const Matrix<Arithmetic, Rows, Cols, RowMajor>& m)
{
	return m * Arithmetic{ -1 };
}

//  Comparison operators:

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
bool operator==(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m1, 
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
)
{
	if (&m1 == &m2)
		return true;

	for (index_t row = 0; row < Rows; ++row)
		for (index_t col = 0; col < Cols; ++col)
			if (m1(row, col) != m2(row, col))
				return false;

	return true;
}
template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
bool operator!=(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m1,
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
)
{
	return !(m1 == m2);
}

} }
