#include "math/Box.hpp"

#include <Debug.hpp>

#include <algorithm>

namespace pm { namespace math {

template <typename Arithmetic>
AxisAlignedBox3<Arithmetic>::AxisAlignedBox3(Arithmetic minX, Arithmetic minY, Arithmetic minZ,
										   	 Arithmetic maxX, Arithmetic maxY, Arithmetic maxZ)
: _minP( {minX, minY, minZ} ), 
  _maxP( {maxX, maxY, maxZ} )
{
	ASSERT(minX <= maxX && minY <= maxY && minZ <= maxZ, "Bad arguments to Box constructor.");
}

template <typename Arithmetic>
AxisAlignedBox3<Arithmetic>::AxisAlignedBox3(const Vector<Arithmetic, 3>& c1,
										     const Vector<Arithmetic, 3>& c2)
: _minP( {std::min(c1[0], c2[0]), std::min(c1[1], c2[1]), std::min(c1[2], c2[2])} ),
  _maxP( {std::max(c1[0], c2[0]), std::max(c1[1], c2[1]), std::max(c1[2], c2[2])} )
{
}

template <typename Arithmetic>
bool AxisAlignedBox3<Arithmetic>::inside(Arithmetic x, Arithmetic y, Arithmetic z) const
{
	if (   (x < _minP[0] || x > _maxP[0])
		|| (y < _minP[1] || y > _maxP[1])
		|| (z < _minP[2] || z > _maxP[2]) )
	{
		return false;
	}

	return true;
}

template <typename Arithmetic>
bool AxisAlignedBox3<Arithmetic>::inside(const Vector<Arithmetic, 3>& p) const
{
	if (   (p[0] < _minP[0] || p[0] > _maxP[0])
		|| (p[1] < _minP[1] || p[1] > _maxP[1])
		|| (p[2] < _minP[2] || p[2] > _maxP[2]) )
	{
		return false;
	}

	return true;
}

template <typename Arithmetic>
const Vector<Arithmetic, 3>& AxisAlignedBox3<Arithmetic>::min() const
{
	return _minP;
}

template <typename Arithmetic>
const Vector<Arithmetic, 3>& AxisAlignedBox3<Arithmetic>::max() const
{
	return _maxP;
}

template <typename Arithmetic>
Arithmetic AxisAlignedBox3<Arithmetic>::width() const
{
	return _maxP[0] - _minP[0];
}

template <typename Arithmetic>
Arithmetic AxisAlignedBox3<Arithmetic>::height() const
{
	return _maxP[1] - _minP[1];
}

template <typename Arithmetic>
Arithmetic AxisAlignedBox3<Arithmetic>::depth() const
{
	return _maxP[2] - _minP[2];
}

template <typename Arithmetic>
AxisAlignedBox3<Arithmetic> 
	AxisAlignedBox3<Arithmetic>::operator+(const Vector<Arithmetic, 3>& p) const
{
	AxisAlignedBox3<Arithmetic> rBox(*this);

	rBox._minP += p;
	rBox._maxP += p;

	return rBox;
}

template <typename Arithmetic>
AxisAlignedBox3<Arithmetic>& 
	AxisAlignedBox3<Arithmetic>::operator+=(const Vector<Arithmetic, 3>& p)
{
	_minP += p;
	_maxP += p;

	return *this;
}

template <typename Arithmetic>
AxisAlignedBox3<Arithmetic> 
	AxisAlignedBox3<Arithmetic>::operator-(const Vector<Arithmetic, 3>& p) const
{
	AxisAlignedBox3<Arithmetic> rBox(*this);

	rBox._minP -= p;
	rBox._maxP -= p;

	return rBox;
}

template <typename Arithmetic>
AxisAlignedBox3<Arithmetic>& 
	AxisAlignedBox3<Arithmetic>::operator-=(const Vector<Arithmetic, 3>& p)
{
	_minP -= p;
	_maxP -= p;

	return *this;
}

//////

template <typename Arithmetic>
AxisAlignedBox2<Arithmetic>::AxisAlignedBox2(Arithmetic minX, Arithmetic minY,
											 Arithmetic maxX, Arithmetic maxY)
: _minP({ minX, minY }),
  _maxP({ maxX, maxY })
{
	ASSERT(minX <= maxX && minY <= maxY, "Bad arguments to Box constructor.");
}

template <typename Arithmetic>
AxisAlignedBox2<Arithmetic>::AxisAlignedBox2(
	const Vector<Arithmetic, 2>& c1,
	const Vector<Arithmetic, 2>& c2
)
: _minP({ std::min(c1[0], c2[0]), std::min(c1[1], c2[1]) }),
  _maxP({ std::max(c1[0], c2[0]), std::max(c1[1], c2[1]) })
{
}

template <typename Arithmetic>
AxisAlignedBox2<Arithmetic>::AxisAlignedBox2(const AxisAlignedBox3<Arithmetic>& box3)
: _minP({ box3.min()[0], box3.min()[1] }),
  _maxP({ box3.max()[0], box3.max()[1] })
{
}

template <typename Arithmetic>
bool AxisAlignedBox2<Arithmetic>::inside(Arithmetic x, Arithmetic y) const
{
	if ((x < _minP[0] || x > _maxP[0])
		|| (y < _minP[1] || y > _maxP[1]))
	{
		return false;
	}

	return true;
}

template <typename Arithmetic>
bool AxisAlignedBox2<Arithmetic>::inside(const Vector<Arithmetic, 2>& p) const
{
	if ((p[0] < _minP[0] || p[0] > _maxP[0])
		|| (p[1] < _minP[1] || p[1] > _maxP[1]))
	{
		return false;
	}

	return true;
}

template <typename Arithmetic>
const Vector<Arithmetic, 2>& AxisAlignedBox2<Arithmetic>::min() const
{
	return _minP;
}

template <typename Arithmetic>
const Vector<Arithmetic, 2>& AxisAlignedBox2<Arithmetic>::max() const
{
	return _maxP;
}

template <typename Arithmetic>
Vector<Arithmetic, 2> AxisAlignedBox2<Arithmetic>::middle() const
{
	return { _minP[0] + width() / 2, _minP[1] + height() / 2 };
}

template <typename Arithmetic>
Arithmetic AxisAlignedBox2<Arithmetic>::width() const
{
	return _maxP[0] - _minP[0];
}

template <typename Arithmetic>
Arithmetic AxisAlignedBox2<Arithmetic>::height() const
{
	return _maxP[1] - _minP[1];
}

template <typename Arithmetic>
AxisAlignedBox2<Arithmetic> 
	AxisAlignedBox2<Arithmetic>::operator+(const Vector<Arithmetic, 2>& p) const
{
	AxisAlignedBox2<Arithmetic> rBox(*this);

	rBox._minP += p;
	rBox._maxP += p;

	return rBox;
}

template <typename Arithmetic>
AxisAlignedBox2<Arithmetic>& 
	AxisAlignedBox2<Arithmetic>::operator+=(const Vector<Arithmetic, 2>& p)
{
	_minP += p;
	_maxP += p;

	return *this;
}

template <typename Arithmetic>
AxisAlignedBox2<Arithmetic> 
	AxisAlignedBox2<Arithmetic>::operator-(const Vector<Arithmetic, 2>& p) const
{
	AxisAlignedBox2<Arithmetic> rBox(*this);

	rBox._minP -= p;
	rBox._maxP -= p;

	return rBox;
}

template <typename Arithmetic>
AxisAlignedBox2<Arithmetic>& 
	AxisAlignedBox2<Arithmetic>::operator-=(const Vector<Arithmetic, 2>& p)
{
	_minP -= p;
	_maxP -= p;

	return *this;
}

template <typename Arithmetic>
template <typename Arithmetic2>
AxisAlignedBox2<Arithmetic>::operator AxisAlignedBox2<Arithmetic2>() const
{
	return AxisAlignedBox2<Arithmetic2>{
		static_cast<Vector<Arithmetic2, 2>>(_minP),
		static_cast<Vector<Arithmetic2, 2>>(_maxP)
	};
}

} }