#include "math/Quaternion.hpp"

#include "math/types.hpp"
#include <cmath>

namespace pm { namespace math {

template <typename Arithmetic>
const Quaternion<Arithmetic> Quaternion<Arithmetic>::IDENTITY = Quaternion<Arithmetic>();

template <typename Arithmetic>
Vector<Arithmetic, 3> operator*(const Quaternion<Arithmetic>& q, const Vector<Arithmetic, 3>& v)
{
	auto invQ = q.inverse();

	Vector<Arithmetic, 4> qV = q.data();
	Vector<Arithmetic, 4> qInvV = invQ.data();
	Vector<Arithmetic, 4> vv = { v(0, 0), v(1, 0), v(2, 0), Arithmetic{0} };

	auto res = hamiltonProduct(qV, hamiltonProduct(vv, qInvV));

	return Vector<Arithmetic, 3>{ res.data() };
}

template <typename Arithmetic>
Quaternion<Arithmetic> operator*(const Quaternion<Arithmetic>& q1, const Quaternion<Arithmetic>& q2)
{
	Quaternion<Arithmetic> res{ q1 };
	
	res *= q2;

	return res;
}

template <typename Arithmetic>
Quaternion<Arithmetic>& operator*=(Quaternion<Arithmetic>& q1, const Quaternion<Arithmetic>& q2)
{
	Vector<Arithmetic, 3> source = q1.data();
	Arithmetic& s1 = q1.data()[3];

	Vector<Arithmetic, 3> v2 = q2.data();
	const Arithmetic& s2 = q2.data()[3];

	Vector<Arithmetic, 3> v1;

	v1 = source * s2 + v2 * s1 + crossProduct(source, v2);
	s1 = s1 * s2 - source * v2;

	source = v1;

	q1.data()[0] = source[0];
	q1.data()[1] = source[1];
	q1.data()[2] = source[2];

	return q1;
}

template <typename Arithmetic>
bool operator!=(const Quaternion<Arithmetic>& q1, const Quaternion<Arithmetic>& q2)
{
	return !(q1 == q2);
}

template <typename Arithmetic>
bool operator==(const Quaternion<Arithmetic>& q1, const Quaternion<Arithmetic>& q2)
{
	for (unsigned int i = 0; i < 4; ++i)
	{
		if (q1.data()[i] != q2.data()[i])
			return false;
	}

	return true;
}

template <typename Arithmetic>
Quaternion<Arithmetic>::Quaternion()
: _data{{0, 0, 0, 1}}
{
}

template <typename Arithmetic>
Quaternion<Arithmetic>::Quaternion(const Arithmetic* data)
: _data{ data[0], data[1], data[2], data[3] }
{
}

template <typename Arithmetic>
Quaternion<Arithmetic> Quaternion<Arithmetic>::inverse() const
{
	Quaternion<Arithmetic> res(*this);

	return res.inverseSelf();
}

template <typename Arithmetic>
Quaternion<Arithmetic>& Quaternion<Arithmetic>::inverseSelf()
{
	for (unsigned int i = 0; i < 3; ++i)
		_data.data()[i] = -_data.data()[i];

	return *this;
}

template <typename Arithmetic>
Arithmetic* Quaternion<Arithmetic>::data()
{
	return _data.data();
}

template <typename Arithmetic>
const Arithmetic* Quaternion<Arithmetic>::data() const
{
	return _data.data();
}

template <typename Arithmetic>
template <bool RowMajor>
Matrix<Arithmetic, 4, 4, RowMajor> Quaternion<Arithmetic>::asAffineMatrix3() const
{
	Matrix<Arithmetic, 4, 4, RowMajor> m(Arithmetic(1));

	Arithmetic one{ 1 };
	Arithmetic two{ 2 };
	Arithmetic i = _data[0];
	Arithmetic j = _data[1];
	Arithmetic k = _data[2];
	Arithmetic r = _data[3];

	m(0, 0) = one - two * (square(j) + square(k));
	m(0, 1) = two * (i * j - k * r);
	m(0, 2) = two * (i * k + j * r);
	
	m(1, 0) = two * (i * j + k * r);
	m(1, 1) = one - two * (square(i) + square(k));
	m(1, 2) = two * (j * k - i * r);

	m(2, 0) = two * (i * k - j * r);
	m(2, 1) = two * (j * k + i * r);
	m(2, 2) = one - two * (square(i) + square(j));

	return m;
}

template <typename Arithmetic>
Quaternion<Arithmetic> Quaternion<Arithmetic>::fromAxisAngle(
	const Vector<Arithmetic, 3>& axis, 
	Arithmetic angle
)
{
	Quaternion q;

	Vector<Arithmetic, 3> v = q._data.data();
	v = axis * std::sin(angle / Arithmetic(2));

	q._data[0] = v[0];
	q._data[1] = v[1];
	q._data[2] = v[2];
	q._data[3] = std::cos(angle / Arithmetic{ 2 });

	return q;
}

template <typename Arithmetic>
template <typename Arithmetic2>
Quaternion<Arithmetic>::operator Quaternion<Arithmetic2>() const
{
	Quaternion<Arithmetic2> rVal;

	for (unsigned int i = 0; i < 4; ++i)
		rVal._data[i] = static_cast<Arithmetic2>(_data[i]);

	return rVal;
}

} }
