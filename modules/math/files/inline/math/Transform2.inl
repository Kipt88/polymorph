#include "math/Transform2.hpp"

namespace pm { namespace math {

// ==== Operators ====

template <typename Arithmetic>
Vector<Arithmetic, 2> operator*(const SimilarityTransform2<Arithmetic>& t, const Vector<Arithmetic, 2>& v)
{
	Vector<Arithmetic, 2> v2{ v };

	t.transform(v2);

	return v2;
}

template <typename Arithmetic>
SimilarityTransform2<Arithmetic> operator*(const SimilarityTransform2<Arithmetic>& t1, const SimilarityTransform2<Arithmetic>& t2)
{
	SimilarityTransform2<Arithmetic>  rTrans;

	rTrans.rotation = t1.rotation + t2.rotation;
	rTrans.scale = t1.scale * t2.scale;

	Arithmetic
		sinThis = std::sin(t1.rotation),
		cosThis = std::cos(t1.rotation);

	rTrans.translation[0] =
		t1.scale * (cosThis * t2.translation[0] - sinThis * t2.translation[1]) + t1.translation[0];

	rTrans.translation[1] =
		t1.scale * (sinThis * t2.translation[0] + cosThis * t2.translation[1]) + t1.translation[1];

	return rTrans;
}

template <typename Arithmetic>
SimilarityTransform2<Arithmetic>& operator*=(SimilarityTransform2<Arithmetic>& t1, const SimilarityTransform2<Arithmetic>& t2)
{
	return t1 = t1 * t2;
}


// ==== SimilarityTransform2 ====

template <typename Arithmetic>
const SimilarityTransform2<Arithmetic> SimilarityTransform2<Arithmetic>::IDENTITY = { { 0, 0 }, 1, 0 };

template <typename Arithmetic>
void SimilarityTransform2<Arithmetic>::transform(Vector<Arithmetic, 2>& vec) const
{
	Arithmetic sinR = std::sin(rotation);
	Arithmetic cosR = std::cos(rotation);

	vec *= scale;
	
	auto x = vec[0];
	auto y = vec[1];

	vec[0] = x * cosR - y * sinR;
	vec[1] = x * sinR + y * cosR;

	vec += translation;
}

template <typename Arithmetic>
SimilarityTransform2<Arithmetic> SimilarityTransform2<Arithmetic>::inverse() const
{
	Arithmetic
		sinMinRot = std::sin(-rotation),
		cosMinRot = std::cos(-rotation),
		invScale = static_cast<Arithmetic>(1) / scale,
		x = translation[0],
		y = translation[1];


	SimilarityTransform2<Arithmetic> rTrans;

	rTrans.rotation = -rotation;
	rTrans.scale = invScale;

	rTrans.translation[0] = -invScale * (cosMinRot * x - sinMinRot * y);
	rTrans.translation[1] = -invScale * (sinMinRot * x + cosMinRot * y);

	return rTrans;
}

template <typename Arithmetic>
void SimilarityTransform2<Arithmetic>::inverseSelf()
{
	*this = inverse();
}

template <typename Arithmetic>
template <bool RowMajor>
SimilarityTransform2<Arithmetic>::operator Matrix<Arithmetic, 3u, 3u, RowMajor>() const
{
	auto rMat = Matrix<Arithmetic, 3u, 3u, RowMajor>::IDENTITY;

	rMat(0, 0) = std::cos(rotation) * scale;
	rMat(0, 1) = -std::sin(rotation) * scale;
	rMat(0, 2) = translation[0];

	rMat(1, 0) = std::sin(rotation) * scale;
	rMat(1, 1) = std::cos(rotation) * scale;
	rMat(1, 2) = translation[1];

	return rMat;
}

template <typename Arithmetic>
template <bool RowMajor>
SimilarityTransform2<Arithmetic>::operator Matrix<Arithmetic, 4u, 4u, RowMajor>() const
{
	auto rMat = Matrix<Arithmetic, 4u, 4u, RowMajor>::IDENTITY;

	rMat(0, 0) = std::cos(rotation) * scale;
	rMat(0, 1) = -std::sin(rotation) * scale;
	rMat(0, 3) = translation[0];

	rMat(1, 0) = std::sin(rotation) * scale;
	rMat(1, 1) = std::cos(rotation) * scale;
	rMat(1, 3) = translation[1];

	return rMat;
}

template <typename Arithmetic>
SimilarityTransform2<Arithmetic> SimilarityTransform2<Arithmetic>::createTranslation(const Vector<Arithmetic, 2>& translation)
{
	return SimilarityTransform2<Arithmetic>{ translation, 1, 0 };
}

template <typename Arithmetic>
SimilarityTransform2<Arithmetic> SimilarityTransform2<Arithmetic>::createScaling(Arithmetic scale)
{
	return SimilarityTransform2<Arithmetic>{ Vector<Arithmetic, 2>{ 0, 0 }, scale, 0 };
}

template <typename Arithmetic>
SimilarityTransform2<Arithmetic> SimilarityTransform2<Arithmetic>::createRotation(Arithmetic rotation)
{
	return SimilarityTransform2<Arithmetic>{ Vector<Arithmetic, 2>{ 0, 0 }, 1, rotation };
}


} }