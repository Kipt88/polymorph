#include "math/Transform3.hpp"

namespace pm { namespace math {

template <typename Arithmetic>
const SimilarityTransform3<Arithmetic> SimilarityTransform3<Arithmetic>::IDENTITY = {
	{0, 0, 0},
	1,
	{}
};

template <typename Arithmetic>
Vector<Arithmetic, 3> operator*(const SimilarityTransform3<Arithmetic>& t, const Vector<Arithmetic, 3>& v)
{
	Vector<Arithmetic, 3> v2(v);

	t.transform(v2);

	return v2;
}

template <typename Arithmetic>
SimilarityTransform3<Arithmetic> operator*(const SimilarityTransform3<Arithmetic>& t1, const SimilarityTransform3<Arithmetic>& t2)
{
	SimilarityTransform3<Arithmetic> rTrans;

	rTrans.scale = t1.scale * t2.scale;

	Vector<Arithmetic, 4> q1{ t1.rotation.data() };
	Vector<Arithmetic, 4> q2{ t2.rotation.data() };

	auto r = hamiltonProduct(q1, q2);

	for (unsigned int i = 0; i < 4; ++i)
		rTrans.rotation.data()[i] = r.data()[i];

	rTrans.translation = t1 * t2.translation;

	return rTrans;
}

template <typename Arithmetic>
SimilarityTransform3<Arithmetic>& operator*=(SimilarityTransform3<Arithmetic>& t1, const SimilarityTransform3<Arithmetic>& t2)
{
	return t1 = t1 * t2;
}

template <typename Arithmetic>
void SimilarityTransform3<Arithmetic>::transform(Vector<Arithmetic, 3>& vec) const
{
	vec *= scale;
	vec = rotation * vec;
	vec += translation;
}

template <typename Arithmetic>
SimilarityTransform3<Arithmetic> SimilarityTransform3<Arithmetic>::inverse() const
{
	// (T * Q * S)^-1
	// (T * Q)^-1 / s
	// Q^-1 * T^-1 / s
	// Q^-1 * -v / s

	SimilarityTransform3<Arithmetic> rTrans;

	rTrans.scale = Arithmetic(1) / scale;
	rTrans.rotation = rotation.inverse();

	rTrans.translation = rTrans.rotation * -translation * rTrans.scale;

	return rTrans;
}

template <typename Arithmetic>
void SimilarityTransform3<Arithmetic>::inverseSelf()
{
	*this = inverse();
}

template <typename Arithmetic>
template <bool RowMajor>
Matrix<Arithmetic, 4u, 4u, RowMajor> SimilarityTransform3<Arithmetic>::asAffineMatrix3() const
{
	auto rMat = rotation.template asAffineMatrix3<RowMajor>();
	
	rMat(0, 0) *= scale;
	rMat(0, 1) *= scale;
	rMat(0, 2) *= scale;
	rMat(0, 3) = translation[0];

	rMat(1, 0) *= scale;
	rMat(1, 1) *= scale;
	rMat(1, 2) *= scale;
	rMat(1, 3) = translation[1];

	rMat(2, 0) *= scale;
	rMat(2, 1) *= scale;
	rMat(2, 2) *= scale;
	rMat(2, 3) = translation[2];

	return rMat;
}

template <typename Arithmetic>
SimilarityTransform3<Arithmetic> SimilarityTransform3<Arithmetic>::createTranslation(const Vector<Arithmetic, 3>& translation)
{
	return SimilarityTransform3<Arithmetic>{ translation, 1, Quaternion<Arithmetic>::IDENTITY };
}

template <typename Arithmetic>
SimilarityTransform3<Arithmetic> SimilarityTransform3<Arithmetic>::createScaling(Arithmetic scale)
{
	return SimilarityTransform3<Arithmetic>{
		Vector<Arithmetic, 3>{ 0, 0, 0 },
		scale,
		Quaternion<Arithmetic>::IDENTITY
	};
}

template <typename Arithmetic>
SimilarityTransform3<Arithmetic> SimilarityTransform3<Arithmetic>::createRotation(const Quaternion<Arithmetic>& rotation)
{
	return SimilarityTransform3<Arithmetic>{ Vector<Arithmetic, 3>{ 0, 0, 0 }, 1, rotation };
}


} }
