#include "math/Projection.hpp"

namespace pm { namespace math {

template <typename Arithmetic, bool RowMajor>
Vector<Arithmetic, 2> operator*(const Projection<RowMajor>& p, const Vector<Arithmetic, 2>& v)
{
	auto v2 = static_cast<Vector<real_t, 2>>(v);

	p.project(v2);

	return static_cast<Vector<Arithmetic, 2>>(v2);
}

template <typename Arithmetic, bool RowMajor>
Vector<Arithmetic, 3> operator*(const Projection<RowMajor>& p, const Vector<Arithmetic, 3>& v)
{
	auto v3 = static_cast<Vector<real_t, 3>>(v);

	p.project(v3);

	return static_cast<Vector<Arithmetic, 3>>(v3);
}

template <typename Arithmetic, bool RowMajor>
Vector<Arithmetic, 4> operator*(const Projection<RowMajor>& p, const Vector<Arithmetic, 4>& v)
{
	auto v4 = static_cast<Vector<real_t, 4>>(v);

	p.project(v4);

	return static_cast<Vector<Arithmetic, 4>>(v4);
}


template <bool RowMajor>
Projection<RowMajor>::Projection()
: _projMat(Matrix<real_t, 4, 4, RowMajor>::IDENTITY),
  _invMat(Matrix<real_t, 4, 4, RowMajor>::IDENTITY)
{
}

template <bool RowMajor>
Projection<RowMajor>::Projection(
	const Matrix<real_t, 4, 4, RowMajor>& projMat,
	const Matrix<real_t, 4, 4, RowMajor>& invMat
)
: _projMat(projMat),
  _invMat(invMat)
{
}

template <bool RowMajor>
const real_t* Projection<RowMajor>::data() const
{
	return _projMat.data();
}

template <bool RowMajor>
const Matrix<real_t, 4u, 4u, RowMajor>& Projection<RowMajor>::asMatrix() const
{
	return _projMat;
}

template <bool RowMajor>
void Projection<RowMajor>::project(Vector<real_t, 2>& v) const
{
	Vector<real_t, 4> v4 = { v[0], v[1], 0.0f, 1.0f };
	v4 = _projMat * v4;

	v[0] = v4[0];
	v[1] = v4[1];
}

template <bool RowMajor>
void Projection<RowMajor>::project(Vector<real_t, 3>& v) const
{
	Vector<real_t, 4, RowMajor> v4 = { v[0], v[1], v[2], 1.0f };
	v4 = _projMat * v4;

	v[0] = v4[0];
	v[1] = v4[1];
	v[2] = v4[2];
}

template <bool RowMajor>
void Projection<RowMajor>::project(Vector<real_t, 4>& v) const
{
	v = _projMat * v;
}

template <bool RowMajor>
Projection<RowMajor> Projection<RowMajor>::inverse() const
{
	return Projection<RowMajor>(_invMat, _projMat);
}

template <bool RowMajor>
void Projection<RowMajor>::inverseSelf()
{
	std::swap(_projMat, _invMat);
}

template <bool RowMajor>
Projection<RowMajor> Projection<RowMajor>::ortho(
	real_t left, real_t right, 
	real_t bottom, real_t top, 
	real_t near_, real_t far_
)
{
	real_t rightMleft = right - left;
	real_t rightPleft = right + left;
	real_t topMbottom = top - bottom;
	real_t topPbottom = top + bottom;
	real_t farMnear = far_ - near_;		// For some reason 'far - near' causes error.
	real_t farPnear = far_ + near_;


	Matrix<real_t, 4, 4, RowMajor> projMat = Matrix<real_t, 4, 4, RowMajor>::ZERO;
	
	projMat(0, 0) = 2.0f / rightMleft;
	projMat(0, 3) = -rightPleft / rightMleft;

	projMat(1, 1) = 2.0f / topMbottom;
	projMat(1, 3) = -topPbottom / topMbottom;

	projMat(2, 2) = -2.0f / farMnear;
	projMat(2, 3) = -farPnear / farMnear;

	projMat(3, 3) = 1.0f;


	Matrix<real_t, 4, 4, RowMajor> invMat = Matrix<real_t, 4, 4, RowMajor>::ZERO;

	invMat(0, 0) = rightMleft / 2.0f;
	invMat(0, 3) = rightPleft / 2.0f;

	invMat(1, 1) = topMbottom / 2.0f;
	invMat(1, 3) = topPbottom / 2.0f;

	invMat(2, 2) = farMnear / -2.0f;
	invMat(2, 3) = farPnear / 2.0f;

	invMat(3, 3) = 1.0f;

	return Projection<RowMajor>(projMat, invMat);
}

} }