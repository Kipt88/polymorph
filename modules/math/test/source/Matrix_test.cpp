#include <gtest/gtest.h>
#include <math/Matrix.hpp>
#include <type_traits>

using namespace pm::math;

template <typename Arithmetic, unsigned int Rows, unsigned int Cols, bool RowMajor>
void testConstructors()
{
	{
		std::array<Arithmetic, Rows * Cols> arr;
		for (auto it = arr.begin(); it != arr.end(); ++it)
			*it = static_cast<Arithmetic>(it - arr.begin());


		Matrix<Arithmetic, Rows, Cols, true> mat(arr.data());
		
		for (index_t row = 0; row < Rows; ++row)
			for (index_t col = 0; col < Cols; ++col)
				ASSERT_EQ(mat(row, col), arr[Cols * row + col]);
	}

}

template <typename Arithmetic, unsigned int Dim, bool RowMajor>
void testSquareConstructors()
{
	Arithmetic diagonal = 2;
	Matrix<Arithmetic, Dim, Dim, RowMajor> mat((Arithmetic(diagonal)));

	for (index_t row = 0; row < Dim; ++row)
	{
		for (index_t col = 0; col < Dim; ++col)
		{
			if (col == row)
				ASSERT_EQ(mat(row, col), diagonal);
			else
				ASSERT_EQ(mat(row, col), Arithmetic(0));
		}
	}
}


TEST(Matrix, ctor_float22)
{
	testConstructors<float, 2, 2, true>();
	testConstructors<float, 2, 2, false>();
	
	testSquareConstructors<float, 2, true>();
	testSquareConstructors<float, 2, false>();
}

TEST(Matrix, ctor_float23)
{
	testConstructors<float, 2, 3, true>();
	testConstructors<float, 2, 3, false>();
}

TEST(Matrix, ctor_float32)
{
	testConstructors<float, 3, 2, true>();
	testConstructors<float, 3, 2, false>();
}

TEST(Matrix, ctor_float33)
{
	testConstructors<float, 3, 3, true>();
	testConstructors<float, 3, 3, false>();

	testSquareConstructors<float, 3, true>();
	testSquareConstructors<float, 3, false>();
}

TEST(Matrix, ctor_int22)
{
	testConstructors<int, 2, 2, true>();
	testConstructors<int, 2, 2, false>();

	testSquareConstructors<int, 2, true>();
	testSquareConstructors<int, 2, false>();
}

TEST(Matrix, ctor_int23)
{
	testConstructors<int, 2, 3, true>();
	testConstructors<int, 2, 3, false>();
}

TEST(Matrix, ctor_int32)
{
	testConstructors<int, 3, 2, true>();
	testConstructors<int, 3, 2, false>();
}

TEST(Matrix, ctor_int33)
{
	testConstructors<int, 3, 3, true>();
	testConstructors<int, 3, 3, false>();

	testSquareConstructors<int, 3, true>();
	testSquareConstructors<int, 3, false>();
}

TEST(Matrix, ctor_vec)
{
	Matrix<int, 4, 1, true> vec4 = {0, 1, 2, 3};

	ASSERT_EQ(vec4[0], 0);
	ASSERT_EQ(vec4[1], 1);
	ASSERT_EQ(vec4[2], 2);
	ASSERT_EQ(vec4[3], 3);
}

template <typename Arithmetic, unsigned int Rows, unsigned int Cols, bool RowMajor>
void testArithmetics()
{
	{
		Matrix<Arithmetic, Rows, Cols, RowMajor> mat;

		for (index_t row = 0; row < Rows; ++row)
			for (index_t col = 0; col < Cols; ++col)
				mat(row, col) = row + Arithmetic(2) * col;

		Matrix<Arithmetic, Rows, Cols, RowMajor> mat2 = mat * Arithmetic(2);
		for (index_t row = 0; row < Rows; ++row)
			for (index_t col = 0; col < Cols; ++col)
				ASSERT_EQ(mat2(row, col), Arithmetic((row + Arithmetic(2) * col) * Arithmetic(2)));
	}

	{
		Matrix<Arithmetic, Rows, Cols, RowMajor> mat;

		for (index_t row = 0; row < Rows; ++row)
			for (index_t col = 0; col < Cols; ++col)
				mat(row, col) = row + Arithmetic(2) * col;

		Matrix<Arithmetic, Rows, Cols, RowMajor> mat2 = mat / Arithmetic(2);
		for (index_t row = 0; row < Rows; ++row)
			for (index_t col = 0; col < Cols; ++col)
				ASSERT_EQ(mat2(row, col), Arithmetic((row + Arithmetic(2) * col) / Arithmetic(2) ));
	}

	{
		Matrix<Arithmetic, Rows, Cols, RowMajor> mat1, mat2;

		for (index_t row = 0; row < Rows; ++row)
		{
			for (index_t col = 0; col < Cols; ++col)
			{
				mat1(row, col) = row + Arithmetic(2) * col;
				mat2(row, col) = col + Arithmetic(2) * row;
			}
		}

		Matrix<Arithmetic, Rows, Cols, RowMajor> mat3 = mat1 + mat2;

		for (index_t row = 0; row < Rows; ++row)
		{
			for (index_t col = 0; col < Cols; ++col)
			{
				ASSERT_EQ(
					mat3(row, col),
					Arithmetic(Arithmetic(3) * row + Arithmetic(3) * col)
				);
			}
		}
	}
}

TEST(Matrix, arithmetics_float22)
{
	testArithmetics<float, 2, 2, true>();
	testArithmetics<float, 2, 2, false>();
}

TEST(Matrix, arithmetics_float23)
{
	testArithmetics<float, 2, 3, true>();
	testArithmetics<float, 2, 3, false>();
}

TEST(Matrix, arithmetics_float32)
{
	testArithmetics<float, 3, 2, true>();
	testArithmetics<float, 3, 2, false>();
}

TEST(Matrix, arithmetics_int22)
{
	testArithmetics<int, 2, 2, true>();
	testArithmetics<int, 2, 2, false>();
}

TEST(Matrix, arithmetics_int23)
{
	testArithmetics<int, 2, 3, true>();
	testArithmetics<int, 2, 3, false>();
}

TEST(Matrix, arithmetics_int32)
{
	testArithmetics<int, 3, 2, true>();
	testArithmetics<int, 3, 2, false>();
}

TEST(Matrix, mutliplication)
{
	int m1[] = 
	{
		-3,  6,  0,  5,
		-2, -1,  2,  7,
		-4,  1,  3,  8,
		 4,  9, 10, 11
	};

	int m2[] =
	{
		12,  6,
		13, 14,
		15, 16,
		 4, -2
	};

	int m3[] =
	{
		 62,  56,
		 21,  -8,
		 42,  22,
		359, 288
	};

	int m4[] =
	{
		 3,  6,  0,  5,
		-2, -1,  5,  7,
		-4,  7,  3, -3,
		 4,  9, 10,  1
	};

	{
		Matrix<int, 4, 4, true> mat1(m1);
		Matrix<int, 4, 2, true> mat2(m2);

		Matrix<int, 4, 2, true> mat3(m3);
		
		Matrix<int, 4, 2, true> resMat = mat1 * mat2;

		ASSERT_EQ(mat3, resMat);
	}

	{
		Matrix<int, 4, 4, true> mat1(m1);
		Matrix<int, 4, 4, true> mat2(m4);

		Matrix<int, 4, 4, true> mat3 = mat1 * mat2;
		mat1 *= mat2;

		ASSERT_EQ(mat3, mat1);
	}
}

TEST(Matrix, inverse)
{
	int m1[] =
	{
		1, 2, 3,
		0, 1, 4,
		5, 6, 0
	};

	int m2[] =
	{
		-24,  18,  5,
		 20, -15, -4,
		 -5,   4,  1
	};

	Matrix<int, 3, 3, true> mat1(m1);
	Matrix<int, 3, 3, true> mat2(m2);
	Matrix<int, 3, 3, true> mat3 = inverse(mat1);

	auto id = Matrix<int, 3, 3, true>::IDENTITY;

	ASSERT_EQ(mat2, mat3);
	ASSERT_EQ(mat1 * mat2, id);
}

TEST(Matrix, determinant)
{
	Matrix<int, 3, 3> m = 
	{
		-2,  2, -3,
		-1,  1,  3,
		 2,  0, -1
	};

	ASSERT_EQ(determinant(m), 18);
}

TEST(Matrix, casts)
{
	{
		Matrix<int, 3, 3, true> m1 =
		{
			-2,  2, -3,
			-1,  1,  3,
			 2,  0, -1
		};

		auto m2 = static_cast<Matrix<short, 3, 3, false>>(m1);

		for (unsigned int r = 0; r < 3; ++r)
		{
			for (unsigned int c = 0; c < 3; ++c)
			{
				ASSERT_EQ(m1(r, c), m2(r, c));
			}
		}
	}
}