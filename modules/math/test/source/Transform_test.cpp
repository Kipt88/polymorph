#include <gtest/gtest.h>

#include <math/Transform2.hpp>
#include <math/Transform3.hpp>
#include <math/Matrix.hpp>

using namespace pm::math;

template <typename Arithmetic, unsigned Rows, unsigned Cols, bool RowMajor>
void assertAlmostEqualMatrices(
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m1, 
	const Matrix<Arithmetic, Rows, Cols, RowMajor>& m2
)
{
	for (unsigned row = 0; row < Rows; ++row)
	{
		for (unsigned col = 0; col < Cols; ++col)
		{
			ASSERT_NEAR(
				m1(row, col),
				m2(row, col),
				0.0001f
			);
		}
	}
}

// SimilarityTransform2:

TEST(SimilarityTransform2, factories)
{
	// Rotation factory
	{
		auto trans = SimilarityTransform2<double>::createRotation(HALF_PI);

		Matrix<double, 2, 1> v2 = { 1, 0 };
		Matrix<double, 2, 1> exp = { 0, 1 };

		auto res = trans * v2;

		assertAlmostEqualMatrices(res, exp);
	}

	// Scale factory
	{
		auto trans = SimilarityTransform2<double>::createScaling(2);
		Matrix<double, 2, 1> v2 = { 1, 0.5 };
		Matrix<double, 2, 1> exp = { 2, 1 };

		auto res = trans * v2;

		assertAlmostEqualMatrices(res, exp);
	}

	// Translation factory
	{
		auto trans = SimilarityTransform2<double>::createTranslation(Matrix<double, 2, 1>{ 2, 1 });
		Matrix<double, 2, 1> v2 = { 1, 0.5 };
		Matrix<double, 2, 1> exp = { 3, 1.5 };

		auto res = trans * v2;

		assertAlmostEqualMatrices(res, exp);
	}
}

TEST(SimilarityTransform2, arithmetics)
{
	// operator*(const Vector&)
	{
		SimilarityTransform2<double> trans;

		trans.scale = 0.1f;
		trans.translation = {1.5f, 8.5f};
		trans.rotation = 4.15f;

		Matrix<double, 2, 1> v = { 1.5f, -5.2f };

		auto v1 = trans * v;
		auto v2 = static_cast<Matrix<double, 3, 3>>(trans) * Matrix<double, 3, 1>{ v[0], v[1], 1.0f };

		ASSERT_FLOAT_EQ(v1[0], v2[0]);
		ASSERT_FLOAT_EQ(v1[1], v2[1]);
	}

	// operator*(const AffineTransform2&)
	{
		SimilarityTransform2<double> trans1;
		SimilarityTransform2<double> trans2;

		trans1.rotation = PI * 0.12f;
		trans2.rotation = PI * 1.32f;

		trans1.scale = 1.5f;
		trans2.scale = -0.8f;

		trans1.translation = Vector<double, 2>{ 1.0f, 0.8f };
		trans2.translation = Vector<double, 2>{ 3.1f, 5.7f };

		auto trans3 = trans1 * trans2;

		auto mat1 = static_cast<Matrix<double, 3, 3>>(trans1) * static_cast<Matrix<double, 3, 3>>(trans2);
		auto mat2 = static_cast<Matrix<double, 3, 3>>(trans3);

		assertAlmostEqualMatrices(mat1, mat2);
	}

	// inverse
	{
		SimilarityTransform2<double> trans1;

		trans1.rotation = PI * 0.12f;
		trans1.scale = 1.5f;
		trans1.translation = Vector<double, 2>{ 1.0f, 0.8f };

		auto trans2 = trans1.inverse();

		auto res1 = trans1 * trans2;
		auto res2 = trans2 * trans1;

		auto mat1 = static_cast<Matrix<double, 3, 3>>(res1);
		auto mat2 = static_cast<Matrix<double, 3, 3>>(res2);

		assertAlmostEqualMatrices(
			mat1,
			Matrix<double, 3, 3>::IDENTITY
		);

		assertAlmostEqualMatrices(
			mat2,
			Matrix<double, 3, 3>::IDENTITY
		);
	}
}

// Transform3:

TEST(SimilarityTransform3, factories)
{
	// Rotation factory
	{
		auto trans = SimilarityTransform3<double>::createRotation(
			Quaternion<double>::fromAxisAngle(Vector<double, 3>{0, 1, 0}, HALF_PI)
		);

		Vector<double, 3> v3 = { 1, 0, 0 };
		Vector<double, 3> exp = { 0, 0, -1 };

		auto res = trans * v3;

		assertAlmostEqualMatrices(res, exp);
	}

	// Scale factory
	{
		auto trans = SimilarityTransform3<double>::createScaling(2);
		
		Vector<double, 3> v2 = { 1, 0.5, 0.25 };
		Vector<double, 3> exp = { 2, 1, 0.5 };

		auto res = trans * v2;

		assertAlmostEqualMatrices(res, exp);
	}

	// Translation factory
	{
		auto trans = SimilarityTransform3<double>::createTranslation(
			Vector<double, 3>{ 3, 2, 1 }
		);

		Vector<double, 3> v3 = { 1, 0.5, 0.25 };
		Vector<double, 3> exp = { 4, 2.5, 1.25 };

		auto res = trans * v3;

		assertAlmostEqualMatrices(res, exp);
	}
}

TEST(SimilarityTransform3, matrix)
{
	SimilarityTransform3<double> trans;

	trans.scale = 0.1f;
	trans.translation = { 1.5f, 8.5f, -3.4f };
	trans.rotation = Quaternion<double>::fromAxisAngle(
		normalize(Vector<double, 3>{-1.0f, 1.5f, 1.0f}),
		-HALF_PI
	);

	auto m = Matrix<double, 4, 4>::IDENTITY;
	
	m(0, 0) *= trans.scale;
	m(1, 1) *= trans.scale;
	m(2, 2) *= trans.scale;

	m *= trans.rotation.asAffineMatrix3();

	m(0, 3) = trans.translation[0];
	m(1, 3) = trans.translation[1];
	m(2, 3) = trans.translation[2];

	assertAlmostEqualMatrices(m, trans.asAffineMatrix3());

	ASSERT_FLOAT_EQ(0.0f, m(3, 0));
	ASSERT_FLOAT_EQ(0.0f, m(3, 1));
	ASSERT_FLOAT_EQ(0.0f, m(3, 2));
	ASSERT_FLOAT_EQ(1.0f, m(3, 3));
}

TEST(SimilarityTransform3, arithmetics)
{
	// Transform3::operator*(const Vector&)
	{
		SimilarityTransform3<double> trans;

		trans.scale = 0.1f;
		trans.translation = { 1.5f, 8.5f, -3.4f };
		trans.rotation = Quaternion<double>::fromAxisAngle(
			normalize(Vector<double, 3>{1.0f, 1.0f, 1.0f}), 
			HALF_PI
		);

		Vector<double, 3> v = { 10.5f, -50.2f, 20.25f };

		auto v1 = trans * v;
		auto v2 = trans.asAffineMatrix3() * Vector<double, 4>{v[0], v[1], v[2], 1.0f};

		ASSERT_FLOAT_EQ(v1[0], v2[0]);
		ASSERT_FLOAT_EQ(v1[1], v2[1]);
		ASSERT_FLOAT_EQ(v1[2], v2[2]);
	}

	// Transform3::operator*(const Transform3&)
	{
		SimilarityTransform3<double> trans1;
		SimilarityTransform3<double> trans2;

		trans1.rotation = Quaternion<double>::fromAxisAngle(
			normalize(Vector<double, 3>{1.0f, 1.0f, -1.0f}), 
			HALF_PI
		);
		trans2.rotation = Quaternion<double>::fromAxisAngle(
			normalize(Vector<double, 3>{-1.0f, 0.5f, 1.0f}),
			HALF_PI * 0.125f
		);

		trans1.scale = 1.5f;
		trans2.scale = -0.8f;

		trans1.translation = Vector<double, 3>{ 1.0f, 0.8f, 8.9f };
		trans2.translation = Vector<double, 3>{ 3.1f, 5.7f, 5.5f };

		auto trans3 = trans1 * trans2;

		auto mat1 = trans1.asAffineMatrix3<true>() * trans2.asAffineMatrix3<true>();
		auto mat2 = trans3.asAffineMatrix3<true>();

		assertAlmostEqualMatrices(mat1, mat2);
	}

	// Transform3::inverse
	{
		SimilarityTransform3<double> trans1;

		trans1.rotation = Quaternion<double>::fromAxisAngle(
			normalize(Vector<double, 3>{-1.0f, 0.5f, 1.0f}),
			HALF_PI * 0.125f
		);
		trans1.scale = 1.5f;
		trans1.translation = Vector<double, 3>{ 1.0f, 0.8f, 2.7f };

		auto trans2 = trans1.inverse();

		auto res1 = trans1 * trans2;
		auto res2 = trans2 * trans1;

		auto mat1 = res1.asAffineMatrix3<true>();
		auto mat2 = res2.asAffineMatrix3<true>();

		assertAlmostEqualMatrices(
			mat1,
			Matrix<double, 4, 4, true>::IDENTITY
		);

		assertAlmostEqualMatrices(
			mat2,
			Matrix<double, 4, 4, true>::IDENTITY
		);
	}
}
