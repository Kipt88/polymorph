#include <gtest/gtest.h>
#include <math/Projection.hpp>

using namespace pm::math;

TEST(Projection, normal)
{
	// Identity ortho projection.
	float
		left = -1.0f,
		right = 1.0f,
		bottom = -1.0f,
		top = 1.0f,
		near = 1.0f,
		far = -1.0f;

	auto proj = Projection<true>::ortho(left, right, bottom, top, near, far);

	Vector<float, 3> vec = { 0.5f, 0.25f, -0.75f };

	auto vecRes = proj * vec;

	ASSERT_EQ(vec, vecRes);

	proj.inverseSelf();

	vecRes = proj * vec;

	ASSERT_EQ(vec, vecRes);
}
