#include <gtest/gtest.h>
#include <math/Geometry.hpp>

using namespace pm::math;

TEST(Geometry, line2_line2_intersection)
{
	{
		// Hit case:

		Line2<float> line1 = {
			{ 3.0f, 1.0f },
			{ 1.0f, 3.0f }
		};

		Line2<float> line2 = {
			{ 1.0f, 1.0f },
			{ 3.0f, 3.0f }
		};

		auto p = intersection(line1, line2);

		ASSERT_TRUE(static_cast<bool>(p));
		ASSERT_FLOAT_EQ(2.0f, (*p)[0]);
		ASSERT_FLOAT_EQ(2.0f, (*p)[1]);
	}

	{
		// Miss case:

		Line2<float> line1 = {
			{ 3.0f, 1.0f },
			{ 1.0f, 3.0f }
		};

		Line2<float> line2 = {
			{ 1.0f, 1.0f },
			{ 0.0f, 3.0f }
		};

		auto p = intersection(line1, line2);

		ASSERT_FALSE(static_cast<bool>(p));
	}
}

TEST(Geometry, line2_circle2_intersection)
{
	{
		// Double-hit case:

		Circle2<float> circle;
		circle.position = { 2.0f, 2.0f };
		circle.radius = 2.0f;

		Line2<float> line = {
			{ -1.0f, 2.0f },
			{ 5.0f, 2.0f }
		};

		auto points = intersection(circle, line);

		ASSERT_TRUE(static_cast<bool>(points.first));
		ASSERT_FLOAT_EQ(0.0f, (*points.first)[0]);
		ASSERT_FLOAT_EQ(2.0f, (*points.first)[1]);

		ASSERT_TRUE(static_cast<bool>(points.second));
		ASSERT_FLOAT_EQ(4.0f, (*points.second)[0]);
		ASSERT_FLOAT_EQ(2.0f, (*points.second)[1]);
	}

	{
		// Start external single-hit case:

		Circle2<float> circle;
		circle.position = { 2.0f, 2.0f };
		circle.radius = 2.0f;

		Line2<float> line = {
			{ 2.0f, -1.0f },
			{ 2.0f, 2.0f }
		};

		auto points = intersection(circle, line);

		ASSERT_TRUE(static_cast<bool>(points.first));
		ASSERT_FLOAT_EQ(2.0f, (*points.first)[0]);
		ASSERT_FLOAT_EQ(0.0f, (*points.first)[1]);

		ASSERT_FALSE(static_cast<bool>(points.second));
	}

	{
		// Start internal single-hit case:

		Circle2<float> circle;
		circle.position = { 2.0f, 2.0f };
		circle.radius = 2.0f;

		Line2<float> line = {
			{ 2.0f, 2.0f },
			{ 2.0f, -1.0f }
		};

		auto points = intersection(circle, line);

		ASSERT_TRUE(static_cast<bool>(points.first));
		ASSERT_FLOAT_EQ(2.0f, (*points.first)[0]);
		ASSERT_FLOAT_EQ(0.0f, (*points.first)[1]);

		ASSERT_FALSE(static_cast<bool>(points.second));
	}

	{
		// External no-hit case:

		Circle2<float> circle;
		circle.position = { 2.0f, 2.0f };
		circle.radius = 2.0f;

		Line2<float> line = {
			{ 0.0f, 0.0f },
			{ 2.0f, -1.0f }
		};

		auto points = intersection(circle, line);

		ASSERT_FALSE(static_cast<bool>(points.first));
		ASSERT_FALSE(static_cast<bool>(points.second));
	}

	{
		// External no-hit case:

		Circle2<float> circle;
		circle.position = { 2.0f, 2.0f };
		circle.radius = 2.0f;

		Line2<float> line = {
			{ -1.0f, -1.0f },
			{ 1.0f, -0.1f }
		};

		auto points = intersection(circle, line);

		ASSERT_FALSE(static_cast<bool>(points.first));
		ASSERT_FALSE(static_cast<bool>(points.second));
	}
}

TEST(Geometry, line2_polygon2_intersection)
{
	Polygon2<float> p = {
		{ -3.0f,  0.0f },
		{ -2.0f, -2.0f },
		{  3.0f, -2.0f },
		{  2.0f,  1.0f },
		{  0.0f,  2.0f}
	};

	{
		Line2<float> l = {
			{  0.0f,  3.0f },
			{  0.0f, -3.0f }
		};

		auto points = intersection(l, p);

		ASSERT_EQ(2, points.size());
	}
}
