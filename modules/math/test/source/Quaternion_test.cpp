#include <gtest/gtest.h>
#include <math/Quaternion.hpp>

using namespace pm::math;


TEST(Quaternion, rotate)
{
	{
		Quaternion<float> q = Quaternion<float>::fromAxisAngle(Vector<float, 3>{0.0f, 1.0f, 0.0f}, HALF_PI);

		Vector<float, 3> v1{ 1.0f,  0.0f,  0.0f };
		Vector<float, 3> v2{ 0.0f,  0.0f, -1.0f };

		auto res = q * v1;

		for (unsigned int i = 0; i < 3; ++i)
		{
			ASSERT_FLOAT_EQ(res[i], v2[i]);
		}
	}

	{
		Quaternion<float> q1 = Quaternion<float>::fromAxisAngle(Vector<float, 3>{0.0f, 1.0f, 0.0f}, HALF_PI);
		Quaternion<float> q2 = Quaternion<float>::fromAxisAngle(Vector<float, 3>{1.0f, 0.0f, 0.0f}, HALF_PI);

		Vector<float, 3> v1{ 1.0f,  0.0f,  0.0f };
		Vector<float, 3> v2{ 0.0f,  1.0f,  0.0f };

		auto res1 = q2 * (q1 * v1);
		auto res2 = (q2 * q1) * v1;

		for (unsigned int i = 0; i < 3; ++i)
		{
			ASSERT_FLOAT_EQ(res1[i], v2[i]);
			ASSERT_FLOAT_EQ(res2[i], v2[i]);
		}
	}
};

TEST(Quaternion, inverse)
{
	{
		Quaternion<float> q = Quaternion<float>::fromAxisAngle(normalize(Vector<float, 3>{0.0f, 1.0f, 1.0f}), HALF_PI);

		auto q2 = q.inverse() * q;

		for (unsigned int i = 0; i < 3; ++i)
		{
			ASSERT_FLOAT_EQ(q2.data()[i], 0.0f);
		}

		ASSERT_FLOAT_EQ(q2.data()[3], 1.0f);
	}
};

TEST(Quaternion, matrix)
{
	Quaternion<float> q = Quaternion<float>::fromAxisAngle(normalize(Vector<float, 3>{0.3f, 1.0f, 1.0f}), HALF_PI * 0.7f);

	Vector<float, 4> v = { 1.0f, 2.0f, 3.0f, 1.0f };

	auto m = q.asAffineMatrix3<false>();

	auto mV = m * v;
	auto qV = q * Vector<float, 3>(v.data());

	for (unsigned int i = 0; i < 3; ++i)
	{
		ASSERT_FLOAT_EQ(mV[i], qV[i]);
	}

	ASSERT_FLOAT_EQ(0.0f, m(3, 0));
	ASSERT_FLOAT_EQ(0.0f, m(3, 1));
	ASSERT_FLOAT_EQ(0.0f, m(3, 2));
	ASSERT_FLOAT_EQ(1.0f, m(3, 3));
}
