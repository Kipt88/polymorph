#include <os/App.hpp>
#include <gr/Graphics.hpp>
#include <physics2d/Physics.hpp>
#include <concurrency/System.hpp>
#include <Time.hpp>
#include <Debug.hpp>

#include <AppRunner.hpp>

#include "BuildSettings.hpp"

namespace {

const char* TAG = "Startup";

}

int main(int argc, char* argv[])
{
	using namespace pm;
	
	INFO_OUT(TAG, "Starting application.");
	
	std::unique_ptr<gr::Graphics> graphics;
	std::unique_ptr<physics2d::Physics> physics;
	std::unique_ptr<pm::concurrency::ConcurrentSystem<AppRunner>> game;
	
	os::AppSystem osSystem(BuildSettings::WINDOW_TITLE, 800, 600);
	
	concurrency::Thread startupThread;
	
	osSystem.pushAction(
		[&](os::App& app)
		{
			startupThread = [&]
			{
				graphics = std::make_unique<gr::Graphics>(BuildSettings::GR_SYNC, std::ref(osSystem));
				physics = std::make_unique<physics2d::Physics>(30ms, 0.125f);
				game = std::make_unique<pm::concurrency::ConcurrentSystem<AppRunner>>(
					BuildSettings::GAME_SYNC,
					std::ref(osSystem),
					std::ref(*graphics),
					std::ref(*physics)
				);
			};
		}
	);
	
	osSystem.enter();
	
	INFO_OUT(TAG, "Closing application.");

	return 0;
}
