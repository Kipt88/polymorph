#include <Windows.h>

#include <os/App.hpp>
#include <gr/Graphics.hpp>
#include <physics2d/Physics.hpp>
#include <concurrency/System.hpp>
#include <Time.hpp>
#include <Debug.hpp>

#include <AppRunner.hpp>

#include "BuildSettings.hpp"

namespace {

const char* TAG = "Startup";

}

int APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	using namespace pm;

	Sleep(1000);

	INFO_OUT(TAG, "Starting application.");

	try
	{
		os::AppSystem appSystem(BuildSettings::UI_SYNC, BuildSettings::WINDOW_TITLE);

		{
			gr::Graphics graphics(BuildSettings::GR_SYNC, std::ref(appSystem));

			physics2d::Physics physics(30ms, 0.125f); // TODO Add to BuildSettings

			{
				try
				{
					pm::concurrency::System<AppRunner> app(BuildSettings::GAME_SYNC);

					app.enter(
						std::ref(appSystem),
						std::ref(graphics),
						std::ref(physics)
					);
				}
				catch (const pm::concurrency::InterruptException&)
				{
				}
			}
		}
	}
	catch (const std::exception& e)
	{
		MessageBox(NULL, e.what(), "Error", MB_OK);
		return -1;
	}
	catch (...)
	{
		MessageBox(NULL, "Unknown exception thrown.", "Error", MB_OK);
		return -1;
	}

	INFO_OUT(TAG, "Closing application.");

	return 0;
}
