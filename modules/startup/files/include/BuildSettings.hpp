#pragma once

#include <Time.hpp>

namespace pm {

namespace BuildSettings
{
	constexpr const char* WINDOW_TITLE = "Jam Summer 2016";
	constexpr Time::duration UI_SYNC = 66ms;
	constexpr Time::duration GR_SYNC = 16ms;
	constexpr Time::duration GAME_SYNC = 10ms;
}

}
