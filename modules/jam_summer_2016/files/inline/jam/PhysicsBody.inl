#include "jam/PhysicsBody.hpp"

namespace pm { namespace jam {

template <typename Action>
void PhysicsBody::withBody(Action&& action)
{
	_scope.withBody(std::forward<Action>(action));
}

template <typename OnCollision, OnCollision method>
void PhysicsBody::setCollisionHandler(method_class_t<OnCollision>& handler)
{
	_collisionHandler.obj = &handler;
	_collisionHandler.onCollision = &callTemplateMethod<OnCollision, method>;
}

} }
