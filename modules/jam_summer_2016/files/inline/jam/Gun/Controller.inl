#include "jam/Gun/Controller.hpp"

namespace pm { namespace jam {

namespace Gun {

template <typename CreateProjectile, CreateProjectile methodCreateProjectile>
void Controller::setProjectileFactory(method_class_t<CreateProjectile>& factory)
{
	_projectileFactory.obj = &factory;
	_projectileFactory.createProjectile = &callTemplateMethod<CreateProjectile, methodCreateProjectile>;
}

}

} }
