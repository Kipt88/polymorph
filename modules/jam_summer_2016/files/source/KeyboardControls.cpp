#include "jam/KeyboardControls.hpp"

namespace pm { namespace jam {

const KeyboardControls KeyboardControls::LEFT_WASD{
	os::Keys::A,
	os::Keys::D,
	os::Keys::W,
	os::Keys::S,
	os::Keys::Q,
	os::Keys::E,
	os::Keys::NUM[1]
};

const KeyboardControls KeyboardControls::RIGHT_NUMPAD{
	os::Keys::NUMPAD[4],
	os::Keys::NUMPAD[6],
	os::Keys::NUMPAD[8],
	os::Keys::NUMPAD[5],
	os::Keys::NUMPAD[7],
	os::Keys::NUMPAD[9],
	os::Keys::RETURN
};

os::key_t KeyboardControls::getThrustKeyForEngine(ShipData::EngineType engine) const
{
	switch (engine)
	{
	case ShipData::ENGINE_ROTATE_LEFT:
		return right;

	case ShipData::ENGINE_ROTATE_RIGHT:
		return left;

	case ShipData::ENGINE_STRAFE_LEFT:
		return rightStrafe;

	case ShipData::ENGINE_STRAFE_RIGHT:
		return leftStrafe;

	case ShipData::ENGINE_THRUST:
		return forward;
	}

	return 0;
}

os::key_t KeyboardControls::getReverseKeyForEngine(ShipData::EngineType engine) const
{
	switch (engine)
	{
	case ShipData::ENGINE_ROTATE_LEFT:
		return left;

	case ShipData::ENGINE_ROTATE_RIGHT:
		return right;

	case ShipData::ENGINE_STRAFE_LEFT:
		return leftStrafe;

	case ShipData::ENGINE_STRAFE_RIGHT:
		return rightStrafe;

	case ShipData::ENGINE_THRUST:
		return backward;
	}

	return 0;
}

} }
