#include "jam/CameraTypeData.hpp"

#include "jam/Camera/DampenVelocity.hpp"
#include "jam/Camera/FixRotation.hpp"

namespace pm { namespace jam {

ecs::component_reference<const ecs::Transform2> CameraTypeData::attachController(
	ecs::component_reference<const ecs::Transform2> input,
	ecs::entity_handle_t handle
) const
{
	auto& camTrans = handle.entity().add<ecs::Transform2>(ecs::LocalTransform2::IDENTITY);

	switch (type)
	{
	case CameraType::FOLLOW:
		camTrans.setParent(input);
		break;

	case CameraType::FIXED_ROTATION:
		handle.entity().add<Camera::FixRotation>(
			handle.systems().updateSystem(),
			input,
			camTrans
		);
		break;

	case CameraType::DAMPEN:
		handle.entity().add<Camera::DampenVelocity>(
			handle.systems().updateSystem(),
			input,
			camTrans
		);
		break;
	}

	return camTrans;
}

} }
