#include "jam/Camera/DampenVelocity.hpp"

namespace pm { namespace jam { namespace Camera {

DampenVelocity::DampenVelocity(
	ecs::UpdateSystem& updateSystem,
	ecs::component_reference<const ecs::Transform2> input,
	ecs::component_reference<ecs::Transform2> output,
	gr::real_t maxVelocity,
	gr::real_t maxAngularVelocity
)
: _input(input),
  _output(output),
  _maxVelocity(maxVelocity),
  _maxAngularVelocity(maxAngularVelocity),
  _lastTransform(input.get().world()),
  _scope()
{
	_scope = updateSystem.add<PM_TMETHOD(DampenVelocity::onUpdate)>(*this);
}

void DampenVelocity::onUpdate(const Time::duration& dt)
{
	auto transform = _input.get().world();

	gr::real_t maxDistance = _maxVelocity * toSeconds<gr::real_t>(dt);

	auto toNewPosition = transform.translation - _lastTransform.translation;

	if (math::lengthSq(toNewPosition) > math::square(maxDistance))
		toNewPosition *= (maxDistance / math::length(toNewPosition));

	_lastTransform.translation += toNewPosition;

	gr::real_t maxRotation = _maxAngularVelocity * toSeconds<gr::real_t>(dt);

	/*
	transform.rotation = std::fmod(transform.rotation, math::TWO_PI);
	if (transform.rotation < 0.0_r)
		transform.rotation += math::TWO_PI;

	_lastTransform.rotation = std::fmod(_lastTransform.rotation, math::TWO_PI);
	if (_lastTransform.rotation < 0.0_r)
		_lastTransform.rotation += math::TWO_PI;
	*/

	auto diff = transform.rotation - _lastTransform.rotation;
	/*
	if (diff > math::PI) // -1deg <-> 1deg (diff = 364 -> -2
		diff -= math::TWO_PI;
	*/

	if (diff < -maxRotation)
		diff = -maxRotation;
	else if (maxRotation < diff)
		diff = maxRotation;

	_lastTransform.rotation += diff;

	_output.get().local() = _lastTransform;
}


} } }
