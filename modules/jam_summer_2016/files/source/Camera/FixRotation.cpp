#include "jam/Camera/FixRotation.hpp"

namespace pm { namespace jam { namespace Camera {

FixRotation::FixRotation(
	ecs::UpdateSystem& updateSystem, 
	ecs::component_reference<const ecs::Transform2> input,
	ecs::component_reference<ecs::Transform2> output,
	gr::real_t targetRotation
)
: _input(input),
  _output(output),
  _targetRotation(targetRotation),
  _scope()
{
	_scope = updateSystem.add<PM_TMETHOD(FixRotation::onUpdate)>(*this);
}

void FixRotation::onUpdate(const Time::duration&)
{
	auto trans = _input.get().world();
	trans.rotation = _targetRotation;

	_output.get().local() = trans;
}

} } }
