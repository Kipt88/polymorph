#include "jam/KeyboardController.hpp"

namespace pm { namespace jam {

KeyboardController::KeyboardController(const KeyboardControls& controls, Ship::Interface ship)
: _controls(controls),
  _ship(ship),
  _forwardDown(0),
  _backwardDown(0),
  _leftDown(0),
  _rightDown(0),
  _strafeLeftDown(0),
  _strafeRightDown(0),
  _scope()
{
	_scope = ship.systems().keySystem().add<
		PM_TMETHOD(KeyboardController::onKeyDown),
		PM_TMETHOD(KeyboardController::onKeyUp)
	>(*this);
}

void KeyboardController::onKeyDown(os::key_t key)
{
	if (key == _controls.forward)
		_forwardDown = 1;
	else if (key == _controls.backward)
		_backwardDown = 1;
	else if (key == _controls.left)
		_leftDown = 1;
	else if (key == _controls.right)
		_rightDown = 1;
	else if (key == _controls.rightStrafe)
		_strafeRightDown = 1;
	else if (key == _controls.leftStrafe)
		_strafeLeftDown = 1;
	else if (key == _controls.shoot)
		_ship.shoot(true);

	_ship.forward(1.0_r * (_forwardDown - _backwardDown));
	_ship.strafe(1.0_r * (_strafeLeftDown - _strafeRightDown));
	_ship.rotate(1.0_r * (_leftDown - _rightDown));
}

void KeyboardController::onKeyUp(os::key_t key)
{
	if (key == _controls.forward)
		_forwardDown = 0;
	else if (key == _controls.backward)
		_backwardDown = 0;
	else if (key == _controls.left)
		_leftDown = 0;
	else if (key == _controls.right)
		_rightDown = 0;
	else if (key == _controls.rightStrafe)
		_strafeRightDown = 0;
	else if (key == _controls.leftStrafe)
		_strafeLeftDown = 0;
	else if (key == _controls.shoot)
		_ship.shoot(false);

	_ship.forward(1.0_r * (_forwardDown - _backwardDown));
	_ship.strafe(1.0_r * (_strafeLeftDown - _strafeRightDown));
	_ship.rotate(1.0_r * (_leftDown - _rightDown));
}

} }
