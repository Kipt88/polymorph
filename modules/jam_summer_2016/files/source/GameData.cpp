#include "jam/GameData.hpp"
#include "gen/bodies.hpp"
#include "gen/meshes.hpp"

namespace pm { namespace jam {

namespace GameData {

const std::vector<AsteroidData> ASTEROIDS{
	{
		{
			{ gen::bodies::ASTEROID1.id, gen::meshes::ASTEROID1.id }
		},
		gen::bodies::ASTEROID3.id, 
		gen::meshes::ASTEROID3.id
	},
	{
		{}, 
		gen::bodies::ASTEROID1.id, 
		gen::meshes::ASTEROID1.id
	}
};


}

} }
