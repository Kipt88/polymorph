#include "jam/ShipPlayerController.hpp"

#include "jam/KeyboardController.hpp"
#include "jam/PadController.hpp"

namespace pm { namespace jam {

namespace ShipPlayerController {

void attachController(const PlayerData& player, Ship::Interface ship)
{
	if (auto controls = player.keyControl)
	{
		ship.entity().add<KeyboardController>(
			*controls,
			ship
		);
	}
	else if (auto controls = player.padControl)
	{
		ship.entity().add<PadController>(
			*controls,
			ship
		);
	}
}

}

} }
