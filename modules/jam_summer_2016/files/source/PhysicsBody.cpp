#include "jam/PhysicsBody.hpp"

namespace pm { namespace jam {

PhysicsBody::PhysicsBody(
	ecs::PhysicsSystem& system,
	const physics2d::BodyDefinition& def,
	ecs::component_reference<ecs::Transform2> targetTransform
)
: _collisionHandler{ nullptr, nullptr },
  _targetTransform(targetTransform),
  _lastState(),
  _scope(
	  system.add<
		  PM_TMETHOD(PhysicsBody::updateState),
		  PM_TMETHOD(PhysicsBody::onCollision)
	  >(*this, def)
)
{
	setPosition(targetTransform.get().world().translation);
	setRotation(targetTransform.get().world().rotation);
}

PhysicsBody::PhysicsBody(
	ecs::PhysicsSystem& system,
	physics2d::BodyDefinitionHandle def,
	ecs::component_reference<ecs::Transform2> targetTransform
)
: PhysicsBody(system, *def, targetTransform)
{
}

void PhysicsBody::setPosition(const ecs::Vector2_r& pos)
{
	_scope.setPosition(pos);
}

void PhysicsBody::setRotation(gr::real_t angle)
{
	_scope.setRotation(angle);
}

void PhysicsBody::setVelocity(const ecs::Vector2_r& velocity)
{
	_scope.setVelocity(velocity);
}

void PhysicsBody::applyForce(const ecs::Vector2_r& force, const ecs::Vector2_r& localPos)
{
	_scope.applyForce(force, localPos);
}

void PhysicsBody::applyImpulse(const ecs::Vector2_r& impulse, const ecs::Vector2_r& localPos)
{
	_scope.applyImpulse(impulse, localPos);
}

ecs::Vector2_r PhysicsBody::getVelocity() const
{
	return static_cast<ecs::Vector2_r>(_lastState.velocity);
}

gr::real_t PhysicsBody::getAngularVelocity() const
{
	return static_cast<gr::real_t>(_lastState.angularVelocity);
}

gr::real_t PhysicsBody::getMass() const
{
	return static_cast<gr::real_t>(_lastState.mass);
}

const ecs::Transform2& PhysicsBody::transform() const
{
	return _targetTransform.get();
}

void PhysicsBody::updateState(const physics2d::BodyState& newState)
{
	auto& local = _targetTransform.get().local();

	local.translation = static_cast<ecs::Vector2_r>(newState.transform.translation);
	local.rotation    = static_cast<gr::real_t>(newState.transform.rotation);
	local.scale       = static_cast<gr::real_t>(newState.transform.scale);

	_lastState = newState;
}

void PhysicsBody::onCollision(const physics2d::CollisionData& data)
{
	if (_collisionHandler.obj)
		_collisionHandler.onCollision(_collisionHandler.obj, data);
}

} }
