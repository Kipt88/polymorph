#include "jam/Debug/Debug.hpp"

#include "jam/RenderingData.hpp"
#include <Debug.hpp>

namespace pm { namespace jam { namespace Debug {

namespace {

Debug* s_instance = nullptr;

}

Debug::Debug(ecs::Systems& systems)
: _meshes(),
  _freeMeshes(),
  _systems(systems),
  _renderScope()
{
	_renderScope = systems.renderSystem().add<
		PM_TMETHOD(Debug::render),
		PM_TMETHOD(Debug::renderCategory)
	>(*this);
}

void Debug::drawLines(
	void* handle, 
	const std::vector<LineDef>& lines, 
	const gr::Color& color
)
{
	id_t id = static_cast<id_t>(reinterpret_cast<std::uintptr_t>(handle));

	gr::VertexList<gr::VertexFormat::V2_C4> vertices;
	vertices.reserve(lines.size() * 2);

	for (const auto& line : lines)
	{
		vertices.push_back(gr::Vertex<gr::VertexFormat::V2_C4>{ line.a, color });
		vertices.push_back(gr::Vertex<gr::VertexFormat::V2_C4>{ line.b, color });
	}

	auto freeIt = std::find_if(
		_freeMeshes.begin(),
		_freeMeshes.end(),
		[&](const gr::MeshHandle& mesh)
		{
			return mesh->vertices().size() == vertices.size();
		}
	);

	if (freeIt != _freeMeshes.end())
	{
		gr::MeshHandle mesh = std::move(*freeIt);

		_meshes.push_back(std::make_pair(handle, mesh));
		_freeMeshes.erase(freeIt);

		_systems.graphics().pushAction(
			[mesh = std::move(mesh), vertices = std::move(vertices)](gr::Surface&)
			{
				mesh->vertices().write(vertices.data(), 0, vertices.size());
			}
		);
	}
	else
	{
		gr::MeshDefinition<gr::VertexFormat::V2_C4> meshDef;

		meshDef.usage = gr::BufferUsage::DYNAMIC;
		meshDef.vertices = std::move(vertices);

		_meshes.push_back(
			std::make_pair(
				handle,
				_systems.graphicsResources().createMesh(id, std::move(meshDef))
			)
		);
	}
}

void Debug::remove(void* handle)
{
	auto swapAndRemoveIf = [](auto first, auto last, auto predicate)
	{
		first = std::find_if(first, last, predicate);

		if (first != last)
		{
			for (auto it = first; ++it != last;)
			{
				if (!predicate(*it))
					std::swap(*first++, *it);
			}
		}

		return first;
	};

	auto it = swapAndRemoveIf(
		_meshes.begin(),
		_meshes.end(),
		[handle](const std::pair<void*, gr::MeshHandle>& element)
		{
			return element.first == handle;
		}
	);

	for (auto i = it; i != _meshes.end(); ++i)
	{
		_freeMeshes.push_back(i->second);
	}
	
	_meshes.erase(it, _meshes.end());
}

void Debug::clear()
{
	_meshes.clear();
}

void Debug::render(gr::ClientFrame& frame) const
{
	if (!_meshes.empty())
	{
		frame.push(
			[meshes = _meshes](gr::Surface& surface) mutable
			{
				for (auto& meshPair : meshes)
				{
					auto& mesh = meshPair.second;

					mesh->vertices().draw(gr::Primitive::LINES);
				}
			}
		);
	}
}

ecs::RenderSystem::render_category_t Debug::renderCategory() const
{
	return RenderingData::GAME_OBJECT_TYPE;
}

Debug& get()
{
	ASSERT(s_instance, "Debug has not yet been created.");
	return *s_instance;
}

void create(ecs::entity_handle_t handle)
{
	if (!s_instance)
		s_instance = &handle.entity().add<Debug>(handle.systems());
}

} } }
