#include "jam/ResetCondition.hpp"

#include "jam/GameState.hpp"
#include "jam/GameData.hpp"

namespace pm { namespace jam {

ResetCondition::ResetCondition(ecs::entity_handle_t state, Environment::Interface env)
: _state(state),
  _env(env),
  _transitionTime(GameData::RESET_DURATION),
  _updateScope(),
  _handlers()
{
}

void ResetCondition::addShip(Ship::Interface ship)
{
	_handlers.emplace_back(this, ship);
}

void ResetCondition::onUpdate(const Time::duration& dt)
{
	_transitionTime -= dt;

	if (_transitionTime <= Time::duration::zero())
	{
		_updateScope.remove();

		auto state = _state;
		auto env = _env;

		state.entity().clear();
		state.removeChildren(); // This probably removes 'this'.

		jam::SessionData session;

		session.level = &LevelData::LEVEL_1;

		jam::PlayerData p1{
			0,
			std::nullopt,
			std::nullopt, //jam::KeyboardControls::LEFT_WASD,
			jam::CameraType::FIXED_ROTATION,
			jam::ShipData::SHIP_TYPE_1
		};

		jam::PlayerData p2{
			1,
			std::nullopt,
			jam::KeyboardControls::RIGHT_NUMPAD,
			jam::CameraType::FOLLOW,
			jam::ShipData::SHIP_TYPE_1
		};

		session.players.push_back(p1);
		session.players.push_back(p2);

		GameState::create(state, env, session);
	}
}

ResetCondition::Handler::Handler(ResetCondition* thiz, Ship::Interface ship)
: _thiz(thiz),
  _ship(ship),
  _scope()
{
	_scope = ship.events().addListener<
		Ship::HpChanged,
		PM_TMETHOD(ResetCondition::Handler::onDamaged)
	>(*this);
}

void ResetCondition::Handler::onDamaged(const Ship::HpChanged& ev)
{
	if (ev.after <= 0.0)
	{
		_ship.destroyShip();

		if (_thiz->_handlers.size() <= 2)
		{
			if (!_thiz->_updateScope)
			{
				_thiz->_updateScope = _thiz->_state.systems().updateSystem().add<
					PM_TMETHOD(ResetCondition::onUpdate)
				>(*_thiz);
			}
		}

		auto it = std::find_if(
			_thiz->_handlers.begin(),
			_thiz->_handlers.end(),
			[this](const Handler& handler)
			{
				return &handler == this;
			}
		);

		_thiz->_handlers.erase(it);
	}
}

} }
