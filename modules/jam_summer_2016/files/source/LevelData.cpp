#include "jam/LevelData.hpp"

#include "gen/bodies.hpp"
#include "gen/meshes.hpp"
#include "gen/navmeshes.hpp"

namespace pm { namespace jam {

const LevelData LevelData::LEVEL_1{
	gen::bodies::EDGE.id,
	gen::meshes::EDGE.id,
	gen::navmeshes::EDGE.id,
	0, // 30,
	{
		ecs::LocalTransform2{ gr::Vector2_r{ -200.0_r, -200.0_r }, 1.0_r,  math::HALF_PI * 1.5_r },
		ecs::LocalTransform2{ gr::Vector2_r{  200.0_r, -200.0_r }, 1.0_r, -math::HALF_PI * 1.5_r },
		ecs::LocalTransform2{ gr::Vector2_r{  200.0_r,  200.0_r }, 1.0_r,  math::HALF_PI * 2.5_r },
		ecs::LocalTransform2{ gr::Vector2_r{ -200.0_r,  200.0_r }, 1.0_r, -math::HALF_PI * 2.5_r },
	}
};

} }
