#include "jam/ShieldUI.hpp"

#include "jam/UISlider.hpp"
#include "jam/RenderingData.hpp"

namespace pm { namespace jam {

namespace ShieldUI {

class Controller
{
public:
	explicit Controller(
		Environment::Interface_const environment, 
		Ship::Interface_const ship, 
		UISlider::Interface slider
	);

private:
	void onShieldEnergyChanged(const Ship::ShieldChanged& ev);
	void onUIAreaChanged(const Environment::UIAreaChanged& ev);

	unsigned int _shipIndex;
	UISlider::Interface _slider;
	Ship::Events::scope_t<Ship::ShieldChanged> _eventScope;
	Environment::Events::scope_t<Environment::UIAreaChanged> _uiEventScope;
};

Controller::Controller(
	Environment::Interface_const environment,
	Ship::Interface_const ship,
	UISlider::Interface slider
)
: _shipIndex(ship.getPlayer() - 1),
  _slider(slider),
  _eventScope(
	  ship.events().addListener<
		  Ship::ShieldChanged,
		  PM_TMETHOD(Controller::onShieldEnergyChanged)
	  >(*this)
  ),
  _uiEventScope(
	  environment.events().addListener<
		  Environment::UIAreaChanged,
		  PM_TMETHOD(Controller::onUIAreaChanged)
	  >(*this)
  )
{
	_slider.setValue(static_cast<gr::real_t>(ship.shield().energy() / ship.shield().maxEnergy()));
}

void Controller::onShieldEnergyChanged(const Ship::ShieldChanged& ev)
{
	_slider.setValue(static_cast<gr::real_t>(ev.after / ev.max));
}

void Controller::onUIAreaChanged(const Environment::UIAreaChanged& ev)
{
	if (ev.playerIndex == _shipIndex)
	{
		_slider.transform().get().local().translation = {
		   ev.area.middle()[0],
		   ev.area.middle()[1] + ev.area.height() * RenderingData::UI::Shield::RELATIVE_Y
		};

		_slider.setWidth(RenderingData::UI::Shield::RELATIVE_WIDTH * ev.area.width());
		_slider.setHeight(RenderingData::UI::Shield::RELATIVE_HEIGHT * ev.area.height());
	}
}

void create(
	Environment::Interface_const environment,
	Ship::Interface_const ship,
	ecs::entity_handle_t handle
)
{
	auto area = environment.uiScreenArea(ship.getPlayer() - 1);

	auto slider = UISlider::create(handle.createChild());

	slider.transform().get().local().translation = {
		area.middle()[0],
		area.middle()[1] + area.height() * RenderingData::UI::Shield::RELATIVE_Y
	};

	slider.setWidth(RenderingData::UI::Shield::RELATIVE_WIDTH * area.width());
	slider.setHeight(RenderingData::UI::Shield::RELATIVE_HEIGHT * area.height());
	slider.setPivot({ 0.5_r, 0.5_r });
	slider.setRenderCategory(RenderingData::UI_TYPE);
	slider.setColor(RenderingData::UI::Shield::COLOR);

	handle.entity().add<Controller>(environment, ship, slider);
}

}

} 

PM_MAKE_COMPONENT_ID(jam::ShieldUI::Controller);

}
