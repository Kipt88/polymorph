#include "jam/GunUI.hpp"

#include "jam/UISlider.hpp"
#include "jam/RenderingData.hpp"

namespace pm { namespace jam {

namespace GunUI {

class Controller
{
public:
	explicit Controller(
		Environment::Interface_const environment, 
		Ship::Interface_const ship, 
		UISlider::Interface slider
	);

private:
	void onGunCharge(const Ship::WeaponCharge& ev);
	void onUIAreaChanged(const Environment::UIAreaChanged& ev);

	unsigned int _shipIndex;
	UISlider::Interface _slider;
	Ship::Events::scope_t<Ship::WeaponCharge> _shipEventScope;
	Environment::Events::scope_t<Environment::UIAreaChanged> _uiEventScope;
};

Controller::Controller(
	Environment::Interface_const environment, 
	Ship::Interface_const ship, 
	UISlider::Interface slider
)
: _shipIndex(ship.getPlayer() - 1),
  _slider(slider),
  _shipEventScope(
	  ship.events().addListener<
		  Ship::WeaponCharge,
		  PM_TMETHOD(Controller::onGunCharge)
	  >(*this)
  ),
  _uiEventScope(
	  environment.events().addListener<
		  Environment::UIAreaChanged,
		  PM_TMETHOD(Controller::onUIAreaChanged)
	  >(*this)
  )
{
	_slider.setValue(1.0_r);
}

void Controller::onGunCharge(const Ship::WeaponCharge& ev)
{
	if (ev.after < ev.max)
		_slider.setColor(RenderingData::UI::Gun::CHARGE_COLOR);
	else
		_slider.setColor(RenderingData::UI::Gun::COLOR);

	_slider.setValue(static_cast<gr::real_t>(ev.after.count()) / static_cast<gr::real_t>(ev.max.count()));
}

void Controller::onUIAreaChanged(const Environment::UIAreaChanged& ev)
{
	if (ev.playerIndex == _shipIndex)
	{
		_slider.transform().get().local().translation = {
			ev.area.middle()[0],
			ev.area.middle()[1] + ev.area.height() * RenderingData::UI::Gun::RELATIVE_Y
		};

		_slider.setWidth(RenderingData::UI::Gun::RELATIVE_WIDTH * ev.area.width());
		_slider.setHeight(RenderingData::UI::Gun::RELATIVE_HEIGHT * ev.area.height());
	}
}

void create(Environment::Interface environment, Ship::Interface_const ship, ecs::entity_handle_t handle)
{
	auto area = environment.uiScreenArea(ship.getPlayer() - 1);

	auto slider = UISlider::create(handle.createChild());

	slider.transform().get().local().translation = {
		area.middle()[0],
		area.middle()[1] + area.height() * RenderingData::UI::Gun::RELATIVE_Y
	};

	slider.setWidth(RenderingData::UI::Gun::RELATIVE_WIDTH * area.width());
	slider.setHeight(RenderingData::UI::Gun::RELATIVE_HEIGHT * area.height());
	slider.setPivot({ 0.5_r, 0.5_r });
	slider.setRenderCategory(RenderingData::UI_TYPE);
	slider.setColor(RenderingData::UI::Gun::COLOR);

	handle.entity().add<Controller>(environment, ship, slider);
}

}

} 

PM_MAKE_COMPONENT_ID(jam::GunUI::Controller);

}
