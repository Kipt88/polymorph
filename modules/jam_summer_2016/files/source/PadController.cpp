#include "jam/PadController.hpp"

namespace pm { namespace jam {

PadController::PadController(const PadControls& controls, Ship::Interface ship)
: _controls(controls),
  _ship(ship),
  _scope()
{
	_scope = ship.systems().gamepadSystem().add<
		PM_TNULL_METHOD(),
		PM_TNULL_METHOD(),
		PM_TMETHOD(PadController::onButtonDown),
		PM_TMETHOD(PadController::onButtonUp),
		PM_TMETHOD(PadController::onAxis)
	>(*this, controls.controller);
}

void PadController::onButtonDown(os::GamepadButton button)
{
	if (button == _controls.shoot)
		_ship.shoot(true);
}

void PadController::onButtonUp(os::GamepadButton button)
{
	if (button == _controls.shoot)
		_ship.shoot(false);
}

void PadController::onAxis(unsigned int axis, const gr::Vector2_r& newPos, const gr::Vector2_r& /*oldPos*/)
{
	// Note:
	// Current implementation assumes controls are specified so you can't do opposite movement at same time.

	if (axis == _controls.left.axis)
	{
		gr::real_t power = newPos * _controls.left.forward;
		if (power >= 0.0_r)
			_ship.rotate(power);
	}

	if (axis == _controls.right.axis)
	{
		gr::real_t power = newPos * _controls.right.forward;
		if (power >= 0.0_r)
			_ship.rotate(-power);
	}

	if (axis == _controls.forward.axis)
	{
		gr::real_t power = newPos * _controls.forward.forward;
		if (power >= 0.0_r)
			_ship.forward(power);
	}

	if (axis == _controls.backward.axis)
	{
		gr::real_t power = newPos * _controls.backward.forward;
		if (power >= 0.0_r)
			_ship.forward(-power);
	}

	if (axis == _controls.rightStrafe.axis)
	{
		gr::real_t power = newPos * _controls.rightStrafe.forward;
		if (power >= 0.0_r)
			_ship.strafe(power);
	}

	if (axis == _controls.leftStrafe.axis)
	{
		gr::real_t power = newPos * _controls.leftStrafe.forward;
		if (power >= 0.0_r)
			_ship.strafe(-power);
	}
}

} }
