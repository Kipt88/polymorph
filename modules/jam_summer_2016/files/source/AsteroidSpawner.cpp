#include "jam/AsteroidSpawner.hpp"

#include "jam/Asteroid.hpp"
#include "jam/GameData.hpp"

namespace pm { namespace jam {

namespace {

const unsigned int SPAWN_TRIES = 5;
const char* LOG_TAG = "AsteroidSpawner";

}

AsteroidSpawner::AsteroidSpawner(
	ecs::entity_handle_t world, 
	unsigned int asteroidCount,
	const std::vector<gr::Vector2_r>& safeLocations
)
: _randomDevice(),
  _randomGen(_randomDevice()),
  _safeLocations(safeLocations),
  _asteroids(world.createChild())
{
	for (unsigned int i = 0; i < asteroidCount; ++i)
		spawnAsteroid();
}

void AsteroidSpawner::spawnAsteroid()
{
	std::uniform_int_distribution<std::size_t> asteroidTypeDist(0, GameData::ASTEROIDS.size() - 1);
	std::uniform_real_distribution<gr::real_t> asteroidDist(0.0_r, GameData::ASTEROID_SPAWN_BOX_SIZE);
	std::uniform_real_distribution<gr::real_t> angleDist(0.0_r, math::TWO_PI);

	const auto& asteroidType = GameData::ASTEROIDS[asteroidTypeDist(_randomGen)];

	std::vector<gr::AABox2> safeAreas;
	safeAreas.reserve(_safeLocations.size());

	for (const auto& pos : _safeLocations)
	{
		gr::AABox2 box(
			pos[0] - 0.5_r * GameData::ASTEROID_SHIP_SPAWN_BOX_SIZE_MARGIN,
			pos[1] - 0.5_r * GameData::ASTEROID_SHIP_SPAWN_BOX_SIZE_MARGIN,
			pos[0] + 0.5_r * GameData::ASTEROID_SHIP_SPAWN_BOX_SIZE_MARGIN,
			pos[1] + 0.5_r * GameData::ASTEROID_SHIP_SPAWN_BOX_SIZE_MARGIN
		);

		safeAreas.push_back(box);
	}

	for (unsigned int i = 0; i < SPAWN_TRIES; ++i)
	{
		auto x = asteroidDist(_randomGen);
		auto y = asteroidDist(_randomGen);

		ecs::Vector2_r spawnPos{
			-0.5_r * GameData::ASTEROID_SPAWN_BOX_SIZE + x,
			-0.5_r * GameData::ASTEROID_SPAWN_BOX_SIZE + y,
		};

		for (const auto& box : safeAreas)
		{
			if (box.inside(spawnPos))
				continue;
		}

		if (closeToAsteroid(spawnPos))
			continue;

		ecs::LocalTransform2 trans;
		trans.translation = spawnPos;
		trans.rotation = angleDist(_randomGen);
		trans.scale = 1.0_r;

		Asteroid::create(
			_asteroids.createChild(),
			_asteroids,
			asteroidType,
			trans
		);

		return;
	}

	DEBUG_OUT(LOG_TAG, "Failed to spawn asteroid.");
}

bool AsteroidSpawner::closeToAsteroid(const ecs::Vector2_r& pos) const
{
	for (unsigned int i = 0; i < _asteroids.childrenSize(); ++i)
	{
		Asteroid::Interface_const asteroid(_asteroids.child(i));

		auto p = asteroid.transform().get().world().translation;

		math::AxisAlignedBox2<gr::real_t> proximity(
			p[0] - 0.5_r * GameData::ASTEROID_ASTEROID_SPAWN_BOX_SIZE_MARGIN,
			p[1] - 0.5_r * GameData::ASTEROID_ASTEROID_SPAWN_BOX_SIZE_MARGIN,
			p[0] + 0.5_r * GameData::ASTEROID_ASTEROID_SPAWN_BOX_SIZE_MARGIN,
			p[1] + 0.5_r * GameData::ASTEROID_ASTEROID_SPAWN_BOX_SIZE_MARGIN
		);

		if (proximity.inside(pos[0], pos[1]))
			return true;
	}

	return false;
}

} }
