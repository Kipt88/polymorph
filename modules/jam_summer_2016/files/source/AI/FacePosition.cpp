#include "jam/AI/FacePosition.hpp"

namespace pm { namespace jam { namespace AI {

FacePosition::FacePosition(const FacePositionSettings& settings)
: _settings(settings),
  _targetPos(ecs::Vector2_r::ZERO),
  _rotate()
{
}

void FacePosition::setTargetPosition(const ecs::Vector2_r& pos)
{
	_targetPos = pos;
}

bool FacePosition::isFacingPosition(Ship::Interface_const ship) const
{
	return _rotate.inAcceptibleRotation(ship);
}

Commands FacePosition::update(Ship::Interface_const ship)
{
	ecs::Vector2_r toPosition = ship.transform().get().world().translation - _targetPos;

	_rotate.setTargetRotation(
		_settings.angleOffset
		+ math::HALF_PI
		+ std::atan2(toPosition[1], toPosition[0])
	);

	return _rotate.update(ship);
}

} } }
