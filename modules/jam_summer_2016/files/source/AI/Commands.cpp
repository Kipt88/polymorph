#include "jam/AI/Commands.hpp"

namespace pm { namespace jam { namespace AI {

Commands& Commands::add(const Commands& other)
{
	if (other.thrust)
		thrust = other.thrust;

	if (other.strafe)
		strafe = other.strafe;

	if (other.rotate)
		rotate = other.rotate;

	if (other.shoot)
		shoot = other.shoot;

	return *this;
}

void Commands::apply(Ship::Interface ship) const
{
	if (thrust)
		ship.forward(*thrust);

	if (strafe)
		ship.strafe(*strafe);

	if (rotate)
		ship.rotate(*rotate);

	if (shoot)
		ship.shoot(*shoot);
}

} } }
