#include "jam/AI/Move.hpp"

#include "jam/Debug/Debug.hpp"

namespace pm { namespace jam { namespace AI {

namespace {

#ifdef _DEBUG

void renderToTarget(void* handle, const ecs::Vector2_r& from, const ecs::Vector2_r& to)
{
	auto& debug = Debug::get();

	debug.remove(handle);

	Debug::Debug::LineDef line;
	line.a = from;
	line.b = to;

	gr::Color color = gr::RED;
	color.a = 0.5f;

	debug.drawLines(handle, { line }, color);
}

#endif

}

Move::Move(gr::real_t range)
: _targetPosition(ecs::Vector2_r::ZERO),
  _range(range)
{
}

void Move::setTargetPosition(const ecs::Vector2_r& position)
{
	_targetPosition = position;
}

bool Move::inAcceptiblePosition(Ship::Interface_const ship) const
{
	auto shipPos = ship.transform().get().world().translation;

	auto toShipCoordinates = ship.transform().get().world().inverse();
	auto translationToTarget = toShipCoordinates * _targetPosition;

	return std::abs(translationToTarget[0]) <= _range && std::abs(translationToTarget[1]) <= _range;
}

Commands Move::update(Ship::Interface_const ship)
{
#ifdef _DEBUG
	auto shipPos = ship.transform().get().world().translation;

	renderToTarget(this, shipPos, _targetPosition);
#endif

	Commands commands;

	auto toShipCoordinates = ship.transform().get().world().inverse();
	auto translationToTarget = toShipCoordinates * _targetPosition;

	for (unsigned int i = 0; i < 2; ++i)
	{
		if (std::abs(translationToTarget[i]) < _range)
		{
			if (i == 0)
				commands.strafe = 0.0_r;
			else
				commands.thrust = 0.0_r;
		}
		else
		{
			if (i == 0)
				commands.strafe = -math::sign(translationToTarget[0]);
			else
				commands.thrust = math::sign(translationToTarget[1]);
		}
	}

	return commands;
}

} } }
