#include "jam/AI/FindEnemy.hpp"

#include "jam/PhysicsData.hpp"

#include <Debug.hpp>
#include <random>

namespace pm { namespace jam { namespace AI {

FindEnemy::FindEnemy(Controller& controller, const FindEnemySettings& settings)
: _controller(controller),
  _settings(settings),
  _pathFollower()
{
}

void FindEnemy::update(const Time::duration& dt)
{
	auto ship = _controller.get().ships().ship;
	auto shipPos = ship.transform().get().local().translation;

	if (auto enemy = closeByEnemy())
	{
		// TODO Set state to something...

		// DEBUG_OUT("AI::FindEnemy", "Enemy found, %u.", ship.getPlayer());
	}

	if (_pathFollower.hasPath())
	{
		if (!unobstructedPath(shipPos, _pathFollower.nextWaypoint(ship)))
		{
			DEBUG_OUT("FindEnemy", "Can't follow current path, recalculating...");

			auto path = _controller.get().navmesh().findPath(shipPos, _pathFollower.endTarget());
			ASSERT(path, "Can't find path.");

			_pathFollower.setPath(*path);
		}
	}
	else
	{
		auto path = _controller.get().navmesh().findPath(shipPos, randomPointOfInterest(), 13.0_r);
		ASSERT(path, "Can't find path.");

		_pathFollower.setPath(*path);
	}

	_pathFollower.update(ship).apply(ship);
}

std::optional<Ship::Interface_const> FindEnemy::closeByEnemy() const
{
	const auto& ships = _controller.get().ships();

	ecs::Vector2_r pos = ships.ship.transform().get().local().translation;

	auto it = std::min_element(
		ships.otherShips.begin(),
		ships.otherShips.end(),
		[pos](const Ship::Interface_const& shipA, const Ship::Interface_const& shipB)
		{
			auto shipToA = shipA.transform().get().local().translation - pos;
			auto shipToB = shipB.transform().get().local().translation - pos;

			return math::lengthSq(shipToA) < math::lengthSq(shipToB);
		}
	);

	if (it == ships.otherShips.end())
	{
		// Seems other ships is emtpy, forever alone :(
		return std::nullopt;
	}

	if (math::lengthSq(it->transform().get().local().translation - pos) 
		<= math::square(_settings.visibleRadius))
	{
		return *it;
	}

	return std::nullopt;
}

ecs::Vector2_r FindEnemy::randomPointOfInterest() const
{
	const LevelData& levelData = _controller.get().level();

	const auto& ships = _controller.get().ships();
	ecs::Vector2_r pos = ships.ship.transform().get().local().translation;

	std::vector<ecs::Vector2_r> pointsOfInterest;
	pointsOfInterest.reserve(levelData.spawnLocations.size());

	for (const ecs::LocalTransform2& location : levelData.spawnLocations)
	{
		if (math::lengthSq(location.translation - pos) > math::square(_settings.visibleRadius))
			pointsOfInterest.push_back(location.translation);
	}

	ASSERT(!pointsOfInterest.empty(), "Can't find any points of interest.");

	std::random_device rd;
	std::mt19937 g(rd());

	std::uniform_int_distribution<unsigned int> distribution(0, pointsOfInterest.size() - 1);

	return pointsOfInterest[distribution(g)];
}

bool FindEnemy::unobstructedPath(const ecs::Vector2_r& from, const ecs::Vector2_r& to) const
{
	auto ship = _controller.get().ships().ship;
	auto& physics = ship.systems().physicsSystem();

	physics2d::CollisionFilter filter;

	filter.group = 0;

	filter.mask.reset();
	filter.mask[static_cast<physics2d::collision_group_t>(PhysicsData::Category::STATIC_WORLD)] = true;

	auto hit = physics.castLine(from, to, filter);

	return !static_cast<bool>(hit);
}

} } }
