#include "jam/AI/Rotate.hpp"

namespace pm { namespace jam { namespace AI {

// gr::real_t sensitivity = math::PI * 0.0125_r

Rotate::Rotate(const RotateSettings& settings)
: _targetRotation(0.0_r),
  _settings(settings)
{
}

void Rotate::setTargetRotation(gr::real_t targetRotation)
{
	_targetRotation = targetRotation;
}

bool Rotate::inAcceptibleRotation(Ship::Interface_const ship) const
{
	gr::real_t rotation = ship.transform().get().world().rotation;
	gr::real_t rotationToTarget = rotation - _targetRotation;

	rotationToTarget = std::fmod(rotationToTarget, math::TWO_PI);

	if (rotationToTarget < -math::PI)
		rotationToTarget += math::TWO_PI;
	else if (rotationToTarget > math::PI)
		rotationToTarget -= math::TWO_PI;

	return std::abs(rotationToTarget) < _settings.targetRotationRange;
}

Commands Rotate::update(Ship::Interface_const ship)
{
	Commands commands;

	gr::real_t rotation = ship.transform().get().world().rotation;
	gr::real_t rotationVelocity = ship.body().get().getAngularVelocity();

	gr::real_t rotationToTarget = rotation - _targetRotation;

	rotationToTarget = std::fmod(rotationToTarget, math::TWO_PI);

	if (rotationToTarget < -math::PI)
		rotationToTarget += math::TWO_PI;
	else if (rotationToTarget > math::PI)
		rotationToTarget -= math::TWO_PI;

	if (std::abs(rotationToTarget) < _settings.targetRotationRange)
	{
		commands.rotate = 0.0_r;
	}
	else
	{
		gr::real_t factor = 1.0_r;

		if (std::abs(rotationToTarget) < _settings.safetyRangeFactor * _settings.targetRotationRange)
			factor = _settings.safetyPowerFactor;

		// r_1 + v*t = r_2 => t = (r_2 - r_1) / v

		gr::real_t secsUntilTarget = std::numeric_limits<gr::real_t>::infinity();

		if (std::abs(rotationVelocity) > 0.0001_r)
			secsUntilTarget = -rotationToTarget / rotationVelocity;

		if (secsUntilTarget > toSeconds<gr::real_t>(_settings.acceptibleWait))
		{
			commands.rotate = -math::sign(rotationToTarget) * factor;
		}
		else if (secsUntilTarget < 0.0_r)
		{
			commands.rotate = -math::sign(rotationToTarget) * factor;
		}
		else
		{
			commands.rotate = 0.0_r;
			/*
			if (std::abs(rotationVelocity) > math::HALF_PI * 0.125_r)
			{
				// Brake?
				ship.rotate(0.0_r);
			}
			else
			{
				ship.rotate(0.0_r);
			}
			*/
		}
	}

	return commands;
}

} } }
