#include "jam/AI/AvoidObstacle.hpp"

#include "jam/PhysicsData.hpp"
#include "jam/Debug/Debug.hpp"

namespace pm { namespace jam { namespace AI {

AvoidObstacle::AvoidObstacle(const Time::duration& warningTime)
: _warningTime(warningTime)
{
}

std::optional<ecs::Vector2_r> AvoidObstacle::evasionPoint(const Ship::Interface_const& ship)
{
#ifdef _DEBUG
	auto& debug = Debug::get();
	debug.remove(this);
#endif

	auto& physics = ship.systems().physicsSystem();

	auto shipPos = ship.transform().get().world().translation;
	auto velocity = ship.body().get().getVelocity();
	auto endPos = shipPos + velocity * toSeconds<gr::real_t>(_warningTime);

	physics2d::CollisionFilter filter;

	filter.group = 0;

	filter.mask.reset();
	filter.mask[static_cast<physics2d::collision_group_t>(PhysicsData::Category::DYNAMIC_WORLD)] = true;

	if (auto hit = physics.castLine(shipPos, endPos, filter))
	{
#ifdef _DEBUG
		{
			auto pos = hit->pos;
			DEBUG_OUT("AvoidObstacle", "Obstacle at %.2f %.2f.", pos[0], pos[1]);

			Debug::Debug::LineDef line;
			line.a = shipPos;
			line.b = pos;

			debug.drawLines(this, { line });
		}
#endif
		
		// TODO
	}

	return std::nullopt;
}

} } }
