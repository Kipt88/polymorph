#include "jam/AI/FollowPath.hpp"

#include "jam/Debug/Debug.hpp"
#include <Debug.hpp>

//
#include "jam/PhysicsData.hpp"
//

namespace pm { namespace jam { namespace AI {

#ifdef _DEBUG

namespace {

void renderPath(void* handle, const ai::NavigationMesh::path_t& path)
{
	auto& debug = Debug::get();

	debug.remove(handle);

	if (path.portals.empty())
		return;

	std::vector<Debug::Debug::LineDef> lines;
	lines.reserve(path.portals.size() * 2);

	for (const auto& portal : path.portals)
	{
		lines.push_back(Debug::Debug::LineDef{ portal.center(), portal.center() + portal.forward() * 10.0_r });
		
		auto points = portal.points();
		lines.push_back(Debug::Debug::LineDef{ points[0], points[1] });
	}
	
	gr::Color color = gr::GREEN;
	color.a = 0.5f;

	debug.drawLines(handle, lines, color);
}

}

#endif

FollowPath::FollowPath(const ai::NavigationMesh::path_t& path)
: _currentPath(),
  _mover(),
  _facer(),
  _avoider()
{
	setPath(path);
}

void FollowPath::setPath(const ai::NavigationMesh::path_t& path)
{
	DEBUG_OUT("AI::FollowPath", "Path size is %u", static_cast<unsigned int>(path.portals.size()));

	// TODO This currently assumes "from" is same as initial position.

	_currentPath = path;
	std::reverse(_currentPath->portals.begin(), _currentPath->portals.end());

#ifdef _DEBUG
	renderPath(this, path);
#endif
}

bool FollowPath::hasPath() const
{
	return static_cast<bool>(_currentPath);
}

ecs::Vector2_r FollowPath::nextWaypoint(const Ship::Interface_const& ship) const
{
	ASSERT(hasPath(), "Can't get waypoint, don't have path.");

	if (!_currentPath->portals.empty())
	{
		auto shipPos = ship.transform().get().world().translation;

		return _currentPath->portals.back().closestPoint(shipPos, 2.0_r * 13.0_r);
	}
	
	return _currentPath->to;
}

ecs::Vector2_r FollowPath::endTarget() const
{
	ASSERT(hasPath(), "Can't get end target, don't have path.");

	return _currentPath->to;
}

Commands FollowPath::update(const Ship::Interface_const& ship)
{
#ifdef _DEBUG
	{
		Debug::get().remove(nullptr);

		auto& physics = ship.systems().physicsSystem();

		auto shipPos = ship.transform().get().world().translation;
		auto shipVelocity = ship.body().get().getVelocity();

		auto p0 = static_cast<physics2d::Vector2_r>(shipPos);
		auto p1 = p0 + static_cast<physics2d::Vector2_r>(shipVelocity);

		Debug::get().drawLines(
			nullptr,
			{ Debug::Debug::LineDef{ static_cast<gr::Vector2_r>(p0), static_cast<gr::Vector2_r>(p1) } },
			gr::GRAY
		);
	}
#endif

	if (!_currentPath)
		return Commands{};

	auto& path = *_currentPath;

	auto shipPos = ship.transform().get().local().translation;

	while (!path.portals.empty() && path.portals.back().sign(shipPos) <= 0)
	{
		// TODO Should check if mover is in acceptible position as well.

		path.portals.pop_back();
	}

	if (!path.portals.empty())
	{
		auto closestPoint = path.portals.back().closestPoint(shipPos, 2.0_r * 13.0_r);

		_mover.setTargetPosition(closestPoint);
		_facer.setTargetPosition(closestPoint);
	}
	else
	{
		_mover.setTargetPosition(path.to);
		_facer.setTargetPosition(path.to);

		if (_mover.inAcceptiblePosition(ship))
			_currentPath = std::nullopt;
	}

	/*
	{
		DEBUG_OUT(
			"AI::FollowPath", "Distance %.2f",
			static_cast<float>(
				math::length(ship.transform().get().local().translation - _reversePath.back())
			)
		);
	}
	*/

	auto commands = _mover.update(ship).add(_facer.update(ship));
	// _avoider.update(commands, ship);
	
	return std::move(commands);
}

} } }
