#include "jam/AI/Controller.hpp"

#include "jam/AI/FindEnemy.hpp"
#include "jam/Debug/Debug.hpp"
#include "gen/navmeshes.hpp"
#include <ai/NavigationMeshReader.hpp>
#include <filesystem/FileInputStream.hpp>
#include <filesystem.hpp>

namespace pm { namespace jam { namespace AI {

namespace {

ai::NavigationMesh loadMesh(id_t id)
{
	auto it = std::find_if(
		gen::navmeshes::ALL.begin(),
		gen::navmeshes::ALL.end(),
		[id](const resource::Resource& res)
		{
			return res.id == id;
		}
	);

	ASSERT(it != gen::navmeshes::ALL.end(), "Can't find navmesh.");

	std::filesystem::path path = gen::navmeshes::ROOT;
	path /= it->path;

	filesystem::FileInputStream input(path);

	return ai::NavigationMeshReader::read(input);
}

#ifdef _DEBUG
void renderNavigationMesh(void* handle, const ai::NavigationMesh& mesh)
{
	const auto& topology = mesh.topology();

	std::vector<Debug::Debug::LineDef> lines;
	lines.reserve(topology.edgeSize());

	for (edge_index_t i = 0; i < topology.edgeSize(); ++i)
	{
		Debug::Debug::LineDef line;

		auto vertexIndices = topology.edgeVertices(i);

		line.a = topology.vertex(vertexIndices[0]);
		line.b = topology.vertex(vertexIndices[1]);

		lines.push_back(line);
	}

	gr::Color color = gr::BLUE;
	color.a = 0.5f;

	Debug::get().drawLines(handle, lines, color);
}
#endif

}

Controller::Controller(
	level_data_ref level, 
	Ship::Interface ship, 
	const std::vector<Ship::Interface_const>& allShips
)
: _interface(ship, allShips),
  _level(level),
  _navmesh(loadMesh(level.get().navmeshId)),
  _state(),
  _nextState(),
  _scope()
{
	_scope = ship.systems().updateSystem().add<
		PM_TMETHOD(Controller::update)
	>(*this);

	setState(FindEnemy{ *this });

#ifdef _DEBUG
	renderNavigationMesh(this, _navmesh);
#endif
}

const ShipInterface& Controller::ships() const
{
	return _interface;
}

const LevelData& Controller::level() const
{
	return _level;
}

const ai::NavigationMesh& Controller::navmesh() const
{
	return _navmesh;
}

void Controller::update(const Time::duration& dt)
{
	if (_nextState)
		_state = std::move(_nextState);

	_state->update(dt);
}

} } }
