#include "jam/AI/ShipInterface.hpp"

#include <algorithm>

namespace pm { namespace jam { namespace AI {

ShipInterface::ShipInterface(Ship::Interface ship_, const std::vector<Ship::Interface_const>& allShips_)
: ship(ship_),
  otherShips(allShips_)
{
	auto it = std::find(otherShips.begin(), otherShips.end(), ship);

	if (it != otherShips.end())
		otherShips.erase(it);
}

} } }
