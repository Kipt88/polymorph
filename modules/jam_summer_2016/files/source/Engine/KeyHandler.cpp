#include "jam/Engine/KeyHandler.hpp"

namespace pm { namespace jam {

namespace Engine {

KeyHandler::KeyHandler(
	ecs::KeySystem& keySystem, 
	ecs::component_reference<Controller> controller, 
	os::key_t forwardKey,
	os::key_t backwardKey
)
: _controller(controller),
  _forwardKey(forwardKey),
  _backwardKey(backwardKey),
  _forwardDown(false),
  _backwardDown(false),
  _scope()
{
	_scope = keySystem.add<
		PM_TMETHOD(KeyHandler::onKeyDown),
		PM_TMETHOD(KeyHandler::onKeyUp)
	>(*this);
}

void KeyHandler::onKeyDown(os::key_t k)
{
	if (k == _forwardKey || k == _backwardKey)
	{
		if (k == _forwardKey)
			_forwardDown = true;
		else if (k == _backwardKey)
			_backwardDown = true;

		updateEngine();
	}
}

void KeyHandler::onKeyUp(os::key_t k)
{
	if (k == _forwardKey || k == _backwardKey)
	{
		if (k == _forwardKey)
			_forwardDown = false;
		else if (k == _backwardKey)
			_backwardDown = false;

		updateEngine();
	}
}

void KeyHandler::updateEngine()
{
	if (_forwardDown && !_backwardDown)
		_controller.get().setPower(1.0_r);
	else if (_backwardDown && !_forwardDown)
		_controller.get().setPower(-1.0_r);
	else
		_controller.get().stop();
}

}

} }
