#include "jam/Engine/GamepadHandler.hpp"

namespace pm { namespace jam {

namespace Engine {

GamepadHandler::GamepadHandler(
	ecs::GamepadSystem& gamepadSystem,
	ecs::component_reference<Controller> controller,
	unsigned int padId,
	unsigned int axis,
	const gr::Vector2_r& positiveDirection
)
: _axis(axis),
  _engineController(controller),
  _positiveDirection(math::normalize(positiveDirection)),
  _scope()
{
	_scope = gamepadSystem.add<
		PM_TNULL_METHOD(),
		PM_TNULL_METHOD(),
		PM_TNULL_METHOD(),
		PM_TNULL_METHOD(),
		PM_TMETHOD(GamepadHandler::onAxis)
	>(*this, padId);
}

void GamepadHandler::onAxis(unsigned int axis, const gr::Vector2_r& newPos, const gr::Vector2_r& /*oldPos*/)
{
	if (axis == _axis)
		_engineController.setPower(newPos * _positiveDirection);
}

}

} }
