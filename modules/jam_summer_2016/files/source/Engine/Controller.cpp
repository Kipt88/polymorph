#include "jam/Engine/Controller.hpp"

namespace pm { namespace jam {

namespace Engine {

Controller::Controller(
	ecs::UpdateSystem& updateSystem,
	ecs::component_reference<PhysicsBody> body,
	ecs::component_reference<ParticleEmitter> forwardParticles,
	ecs::component_reference<ParticleEmitter> backwardParticles,
	const ecs::Vector2_r& force,
	const ecs::Vector2_r& pos
)
: _body(body),
  _forwardParticles(forwardParticles),
  _backwardParticles(backwardParticles),
  _force(force),
  _pos(pos),
  _power(0.0_r),
  _scope()
{
	_scope = updateSystem.add<PM_TMETHOD(Controller::update)>(*this);
}

void Controller::setPower(gr::real_t power)
{
	ASSERT(std::abs(power) <= 1.0_r, "Trying to over power engine.");

	if (power > 0.0_r)
	{
		_forwardParticles.get().start();
		_backwardParticles.get().stop();
	}
	else if (power < 0.0_r)
	{
		_forwardParticles.get().stop();
		_backwardParticles.get().start();
	}
	else
	{
		stop();
	}

	_power = power;
}

void Controller::stop()
{
	_forwardParticles.get().stop();
	_backwardParticles.get().stop();

	_power = 0.0_r;
}

void Controller::update(const Time::duration&)
{
	if (_power != 0.0_r)
		_body.get().applyForce(_force * _power, _pos);
}

}

} }
