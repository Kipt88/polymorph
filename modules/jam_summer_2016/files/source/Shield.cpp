#include "jam/Shield.hpp"

#include "jam/RenderingData.hpp"
#include "jam/PhysicsData.hpp"
#include "gen/meshes.hpp"
#include <physics2d/Literals.hpp>
#include <ecs/Mesh2Renderer.hpp>
#include <gr/PlainRenderer.hpp>

namespace pm { namespace jam {

namespace Shield {

class Shield
{
public:
	explicit Shield(
		ecs::UpdateSystem& updateSystem, 
		ecs::component_reference<Ship::Events> events,
		ecs::component_reference<PhysicsBody> body,
		ecs::component_reference<ecs::Mesh2Renderer> renderer,
		const physics2d::ShapeDefinition& shieldShape,
		physics2d::real_t energy, 
		physics2d::real_t energyPerSec
	);

	physics2d::real_t energy() const;
	physics2d::real_t startEnergy() const;
	void inflictDamage(physics2d::real_t energy);

private:
	void onUpdate(const Time::duration& dt);

	void removeShield();
	void addShield();

	bool _hasShield;

	physics2d::real_t _startEnergy;
	physics2d::real_t _energy;
	physics2d::real_t _energyPerSec;

	physics2d::ShapeDefinition _shieldShape;

	ecs::component_reference<Ship::Events> _events;
	ecs::component_reference<PhysicsBody> _body;
	ecs::component_reference<ecs::Mesh2Renderer> _renderer;

	ecs::UpdateSystem::UpdaterScope _updateScope;
};


Shield::Shield(
	ecs::UpdateSystem& updateSystem,
	ecs::component_reference<Ship::Events> events,
	ecs::component_reference<PhysicsBody> body,
	ecs::component_reference<ecs::Mesh2Renderer> renderer,
	const physics2d::ShapeDefinition& shieldShape,
	physics2d::real_t energy,
	physics2d::real_t energyPerSec
)
: _hasShield(false),
  _startEnergy(energy),
  _energy(energy),
  _energyPerSec(energyPerSec),
  _shieldShape(shieldShape),
  _events(events),
  _body(body),
  _renderer(renderer),
  _updateScope()
{
	_updateScope = updateSystem.add<PM_TMETHOD(Shield::onUpdate)>(*this);

	_shieldShape.userData = this;

	addShield();
}

physics2d::real_t Shield::energy() const
{
	return _energy;
}

physics2d::real_t Shield::startEnergy() const
{
	return _startEnergy;
}

void Shield::inflictDamage(physics2d::real_t energy)
{
	auto energyBefore = _energy;

	_energy -= energy;

	if (_energy <= 0.0)
		_energy = 0.0;

	if (_hasShield)
		removeShield();

	_events.get().triggerEvent<Ship::ShieldChanged>({ energyBefore, _energy, _startEnergy });
}

void Shield::removeShield()
{
	ASSERT(_hasShield, "Shield isn't active.");

	_hasShield = false;

	_body.get().withBody(
		[this](physics2d::BodyHandle body)
		{
			auto shapes = body.getShapes();

			for (unsigned int i = 0; i < shapes.size(); ++i)
			{
				unsigned int index = shapes.size() - 1 - i;

				if (shapes[index].getUserData() == this)
					body.removeShape(index);
			}
		}
	);

	_renderer.get().active() = false;
}

void Shield::addShield()
{
	ASSERT(!_hasShield, "Shield is already active.");

	_hasShield = true;

	_body.get().withBody(
		[shape = _shieldShape](physics2d::BodyHandle body)
		{
			body.createShape(shape);
		}
	);

	_renderer.get().active() = true;
}

void Shield::onUpdate(const Time::duration& dt)
{
	auto energyBefore = _energy;

	_energy += toSeconds<physics2d::real_t>(dt) * _energyPerSec;

	if (_energy >= _startEnergy)
	{
		_energy = _startEnergy;

		if (!_hasShield)
			addShield();
	}

	_events.get().triggerEvent<Ship::ShieldChanged>({ energyBefore, _energy, _startEnergy });
}

Interface_const::Interface_const(ecs::const_entity_handle_t handle)
: ecs::const_entity_handle_t(handle)
{
}

physics2d::real_t Interface_const::energy() const
{
	return entity().get<Shield>().energy();
}

physics2d::real_t Interface_const::maxEnergy() const
{
	return entity().get<Shield>().startEnergy();
}


Interface::Interface(ecs::entity_handle_t handle)
: ecs::entity_handle_t(handle)
{
}

physics2d::real_t Interface::energy() const
{
	return entity().get<Shield>().energy();
}

physics2d::real_t Interface::maxEnergy() const
{
	return entity().get<Shield>().startEnergy();
}

void Interface::inflictDamage(physics2d::real_t energy) const
{
	entity().get<Shield>().inflictDamage(energy);
}

Interface::operator Interface_const() const
{
	return Interface_const(static_cast<const ecs::const_entity_handle_t&>(*this));
}


Interface create(
	Environment::Interface_const environment,
	ecs::component_reference<Ship::Events> events,
	ecs::component_reference<const ecs::Transform2> transform,
	ecs::component_reference<PhysicsBody> body,
	unsigned int playerIndex,
	const ShieldData& def,
	ecs::entity_handle_t handle
)
{
	auto& trans = handle.entity().add<ecs::Transform2>(transform);

	auto& mesh = handle.entity().add<gr::MeshHandle>(
		handle.systems().graphicsResources().acquireMesh(gen::meshes::SHIELD.id)
	);

	auto& renderer = handle.entity().add<ecs::Mesh2Renderer>(
		handle.systems().renderSystem(),
		gr::PlainRenderer::render,
		mesh,
		trans,
		RenderingData::GAME_OBJECT_TYPE
	);

	physics2d::ShapeDefinition shapeDef;

	shapeDef.shape = physics2d::Shape::CIRCLE;
	shapeDef.geometry.circle.radius = def.radius;
	shapeDef.material.elasticity = def.elasticity;
	shapeDef.material.density = 0.0001_kgpm2;
	shapeDef.filter.categories.reset();
	shapeDef.filter.categories[PhysicsData::shipCategory(playerIndex)] = true;
	shapeDef.filter.group = playerIndex + 1;

	handle.entity().add<Shield>(
		handle.systems().updateSystem(), 
		events, 
		body, 
		renderer, 
		shapeDef, 
		def.energy, 
		def.energyRegeneration
	);

	return Interface(handle);
}

}

}

PM_MAKE_COMPONENT_ID(jam::Shield::Shield);

}
