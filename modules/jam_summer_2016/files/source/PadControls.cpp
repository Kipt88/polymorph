#include "jam/PadControls.hpp"

namespace pm { namespace jam {

const PadControls PadControls::DEFAULT{
	0,
	os::GamepadButton::A,
	{ 1, {  1.0_r,  0.0_r } },
	{ 1, { -1.0_r,  0.0_r } },
	{ 0, {  0.0_r, -1.0_r } },
	{ 0, {  0.0_r,  1.0_r } },
	{ 0, {  1.0_r,  0.0_r } },
	{ 0, { -1.0_r,  0.0_r } }
};

PadControls::Axis PadControls::getAxisForEngine(ShipData::EngineType engine) const
{
	switch (engine)
	{
	case ShipData::ENGINE_ROTATE_LEFT:
		return left;

	case ShipData::ENGINE_ROTATE_RIGHT:
		return right;

	case ShipData::ENGINE_STRAFE_LEFT:
		return leftStrafe;

	case ShipData::ENGINE_STRAFE_RIGHT:
		return rightStrafe;

	case ShipData::ENGINE_THRUST:
		return backward;
	}

	std::terminate();
}

} }
