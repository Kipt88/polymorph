#include "jam/UISlider.hpp"

#include <gr/PlainRenderer.hpp>

namespace pm { namespace jam {

namespace UISlider {

class Controller
{
public:
	explicit Controller(
		ecs::Systems& systems, 
		ecs::component_reference<const ecs::Transform2> transform
	);

	gr::real_t getWidth() const;
	gr::real_t getHeight() const;
	gr::real_t getValue() const;
	const gr::Color& getColor() const;
	const gr::Vector2_r& getPivot() const;
	ecs::RenderSystem::render_category_t getRenderCategory() const;

	void setWidth(gr::real_t w);
	void setHeight(gr::real_t h);
	void setValue(gr::real_t r);
	void setColor(const gr::Color& color);
	void setPivot(const gr::Vector2_r& pivot);
	void setRenderCategory(ecs::RenderSystem::render_category_t category);
	
private:
	void render(gr::ClientFrame& frame) const;

	gr::real_t _value;
	gr::real_t _width;
	gr::real_t _height;
	gr::Color _color;
	gr::Vector2_r _pivot;
	ecs::RenderSystem::render_category_t _renderCategory;

	ecs::component_reference<const ecs::Transform2> _transform;

	gr::MeshHandle _mesh;
	ecs::RenderSystem::RenderableScope _renderScope;
};

Controller::Controller(
	ecs::Systems& systems, 
	ecs::component_reference<const ecs::Transform2> transform
)
: _value(0.0_r),
  _width(1.0_r),
  _height(1.0_r),
  _color(gr::WHITE),
  _pivot({ 0.5_r, 0.5_r }),
  _renderCategory(0),
  _transform(transform),
  _mesh(nullptr),
  _renderScope(
	systems.renderSystem().add<
	    PM_TMETHOD(Controller::render), 
	    PM_TMETHOD(Controller::getRenderCategory)
	>(*this)
  )
{
	id_t id = static_cast<id_t>(reinterpret_cast<std::uintptr_t>(this));

	gr::MeshDefinition<gr::VertexFormat::V2_C3, 4> def;

	def.usage = gr::BufferUsage::DYNAMIC;
	def.vertices.resize(4);
	
	gr::MeshDefinition<gr::VertexFormat::V2_C3, 4>::face_t face;
	face.indices = { 0, 1, 2, 3 };
	face.primitive = gr::Primitive::QUADS;
	face.materialId = 0;

	def.faces.push_back(std::move(face));

	_mesh = systems.graphicsResources().createMesh(
		id,
		std::move(def)
	);
}

gr::real_t Controller::getWidth() const
{
	return _width;
}

gr::real_t Controller::getHeight() const
{
	return _height;
}

gr::real_t Controller::getValue() const
{
	return _value;
}

const gr::Color& Controller::getColor() const
{
	return _color;
}

const gr::Vector2_r& Controller::getPivot() const
{
	return _pivot;
}

ecs::RenderSystem::render_category_t Controller::getRenderCategory() const
{
	return _renderCategory;
}

void Controller::setWidth(gr::real_t w)
{
	_width = w;
}

void Controller::setHeight(gr::real_t h)
{
	_height = h;
}

void Controller::setValue(gr::real_t v)
{
	ASSERT(0.0_r <= v && v <= 1.0_r, "Value not in range [0, 1]");

	_value = v;
}

void Controller::setColor(const gr::Color& c)
{
	_color = c;
}

void Controller::setPivot(const gr::Vector2_r& p)
{
	_pivot = p;
}

void Controller::setRenderCategory(ecs::RenderSystem::render_category_t category)
{
	_renderCategory = category;
}

void Controller::render(gr::ClientFrame& frame) const
{
	frame.push(
		[
		  mesh = _mesh, 
		  worldTrans = _transform.get().world(),
		  width = _value * _width,
		  height = _height,
		  color = _color,
		  pivot = _pivot
		](gr::Surface& surface) mutable
		{
			gr::VertexList<gr::VertexFormat::V2_C3> vl(4);

			gr::Vertex<gr::VertexFormat::V2_C3> v;
			v.color = color;

			v.pos = gr::Vector2_r{ -pivot[0] * width,           -pivot[1] * height };
			vl.push_back(v);

			v.pos = gr::Vector2_r{ (-pivot[0] + 1.0_r) * width, -pivot[1] * height };
			vl.push_back(v);

			v.pos = gr::Vector2_r{ (-pivot[0] + 1.0_r) * width, (-pivot[1] + 1.0_r) * height };
			vl.push_back(v);

			v.pos = gr::Vector2_r{ -pivot[0] * width,           (-pivot[1] + 1.0_r) * height };
			vl.push_back(v);

			mesh->vertices().write(vl.data(), 0, 4);
			
			gr::PlainRenderer::render(surface, worldTrans, mesh);
		}
	);
}

//

Interface_const::Interface_const(const_entity_handle_t handle)
	: const_entity_handle_t(handle)
{
}

ecs::component_reference<const ecs::Transform2> Interface_const::transform() const
{
	return entity().get<ecs::Transform2>();
}

gr::real_t Interface_const::getWidth() const
{
	return entity().get<Controller>().getWidth();
}

gr::real_t Interface_const::getHeight() const
{
	return entity().get<Controller>().getHeight();
}

gr::real_t Interface_const::getValue() const
{
	return entity().get<Controller>().getValue();
}

const gr::Color& Interface_const::getColor() const
{
	return entity().get<Controller>().getColor();
}

const gr::Vector2_r& Interface_const::getPivot() const
{
	return entity().get<Controller>().getPivot();
}

ecs::RenderSystem::render_category_t Interface_const::getRenderCategory() const
{
	return entity().get<Controller>().getRenderCategory();
}

//

Interface::Interface(entity_handle_t handle)
	: entity_handle_t(handle)
{
}

ecs::component_reference<ecs::Transform2> Interface::transform() const
{
	return entity().get<ecs::Transform2>();
}

gr::real_t Interface::getWidth() const
{
	return entity().get<Controller>().getWidth();
}

gr::real_t Interface::getHeight() const
{
	return entity().get<Controller>().getHeight();
}

gr::real_t Interface::getValue() const
{
	return entity().get<Controller>().getValue();
}

const gr::Color& Interface::getColor() const
{
	return entity().get<Controller>().getColor();
}

const gr::Vector2_r& Interface::getPivot() const
{
	return entity().get<Controller>().getPivot();
}

ecs::RenderSystem::render_category_t Interface::getRenderCategory() const
{
	return entity().get<Controller>().getRenderCategory();
}

void Interface::setWidth(gr::real_t w) const
{
	entity().get<Controller>().setWidth(w);
}

void Interface::setHeight(gr::real_t h) const
{
	entity().get<Controller>().setHeight(h);
}

void Interface::setValue(gr::real_t v) const
{
	entity().get<Controller>().setValue(v);
}

void Interface::setColor(const gr::Color& c) const
{
	entity().get<Controller>().setColor(c);
}

void Interface::setPivot(const gr::Vector2_r& p) const
{
	entity().get<Controller>().setPivot(p);
}

void Interface::setRenderCategory(ecs::RenderSystem::render_category_t c) const
{
	entity().get<Controller>().setRenderCategory(c);
}

Interface::operator Interface_const() const
{
	return Interface_const(static_cast<const entity_handle_t&>(*this));
}

//

Interface create(ecs::entity_handle_t handle)
{
	auto& trans = handle.entity().add<ecs::Transform2>();

	handle.entity().add<Controller>(handle.systems(), trans);

	return Interface(handle);
}

}

} 

PM_MAKE_COMPONENT_ID(jam::UISlider::Controller);

}
