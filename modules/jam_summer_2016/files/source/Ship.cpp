#include "jam/Ship.hpp"

#include "jam/Camera/FixRotation.hpp"
#include "jam/Camera/DampenVelocity.hpp"
#include "jam/ShipController.hpp"
#include "jam/Engine.hpp"
#include "jam/PhysicsBody.hpp"
#include "jam/Shield.hpp"
#include "jam/Hull.hpp"
#include "jam/Gun.hpp"
#include "jam/ParticleEmitter.hpp"
#include "jam/RenderingData.hpp"
#include "jam/PhysicsData.hpp"
#include "gen/meshes.hpp"
#include "gen/bodies.hpp"
#include <ecs/Mesh2Renderer.hpp>
#include <gr/PlainRenderer.hpp>

namespace pm { namespace jam {

namespace Ship {

namespace {

enum
{
	HULL_INDEX,
	SHIELD_INDEX,
	GUNS_INDEX,
	ENGINES_INDEX
};


}

Interface_const::Interface_const(const_entity_handle_t handle)
: const_entity_handle_t(handle)
{
}

ecs::component_reference<const PhysicsBody> Interface_const::body() const
{
	return entity().get<PhysicsBody>();
}

Shield::Interface_const Interface_const::shield() const
{
	return Shield::Interface_const(child(SHIELD_INDEX));
}

Hull::Interface_const Interface_const::hull() const
{
	return Hull::Interface_const(child(HULL_INDEX));
}

const Events& Interface_const::events() const
{
	return entity().get<Events>();
}

ecs::component_reference<const ecs::Transform2> Interface_const::transform() const
{
	return entity().get<ecs::Transform2>();
}

unsigned int Interface_const::getPlayer() const
{
	return entity().get<ShipController>().getPlayer();
}

Interface::Interface(entity_handle_t handle)
: entity_handle_t(handle)
{
}

ecs::component_reference<const PhysicsBody> Interface::body() const
{
	return entity().get<PhysicsBody>();
}

Shield::Interface Interface::shield() const
{
	return Shield::Interface(child(SHIELD_INDEX));
}

Hull::Interface Interface::hull() const
{
	return Hull::Interface(child(HULL_INDEX));
}

const Events& Interface::events() const
{
	return entity().get<Events>();
}

ecs::component_reference<const ecs::Transform2> Interface::transform() const
{
	return entity().get<ecs::Transform2>();
}

unsigned int Interface::getPlayer() const
{
	return entity().get<ShipController>().getPlayer();
}

void Interface::forward(gr::real_t power) const
{
	auto engines = child(ENGINES_INDEX);
	
	auto thrustEngine = Engine::Interface(
		engines.child(static_cast<unsigned int>(ShipData::EngineType::ENGINE_THRUST))
	);

	thrustEngine.setPower(power);
}

void Interface::strafe(gr::real_t power) const
{
	auto engines = child(ENGINES_INDEX);

	auto leftEngine = Engine::Interface(
		engines.child(static_cast<unsigned int>(ShipData::EngineType::ENGINE_STRAFE_LEFT))
	);

	auto rightEngine = Engine::Interface(
		engines.child(static_cast<unsigned int>(ShipData::EngineType::ENGINE_STRAFE_RIGHT))
	);

	leftEngine.setPower(-power);
	rightEngine.setPower(power);
}

void Interface::rotate(gr::real_t power) const
{
	auto engines = child(ENGINES_INDEX);

	auto leftEngine = Engine::Interface(
		engines.child(static_cast<unsigned int>(ShipData::EngineType::ENGINE_ROTATE_LEFT))
	);

	auto rightEngine = Engine::Interface(
		engines.child(static_cast<unsigned int>(ShipData::EngineType::ENGINE_ROTATE_RIGHT))
	);

	leftEngine.setPower(-power);
	rightEngine.setPower(power);
}

void Interface::shoot(bool active) const
{
	auto guns = child(GUNS_INDEX);

	for (unsigned int i = 0; i < guns.childrenSize(); ++i)
	{
		Gun::Interface gun(guns.child(i));

		gun.setActive(active);
	}
}

void Interface::destroyShip() const
{
	auto engines = child(ENGINES_INDEX);

	for (unsigned int i = 0; i < engines.childrenSize(); ++i)
	{
		auto engine = Engine::Interface(engines.child(i));

		engine.removeControl();
	}

	auto guns = child(GUNS_INDEX);

	for (unsigned int i = 0; i < guns.childrenSize(); ++i)
	{
		auto gun = Gun::Interface(guns.child(i));

		gun.removeControl();
	}

	auto emitter = createChild();

	ParticleData def;

	def.maxParticles = 50;
	def.color = gr::RED;
	def.renderCategory = RenderingData::GAME_OBJECT_TYPE;
	def.renderOrder = 1;

	def.angle.min = 0.0_r;
	def.angle.max = math::TWO_PI;
	def.speed.min = 20.0_r;
	def.speed.max = 40.0_r;
	def.acceleration.min = 0.0_r;
	def.acceleration.max = 0.0_r;
	def.lifetime.min = 750ms;
	def.lifetime.max = 1500ms;
	def.clusterSize.min = 5;
	def.clusterSize.max = 10;
	def.spawnArea = gr::AABox2{ -0.5_r, -0.5_r, 0.5_r, 0.5_r };
	def.spawnFrequency = 100ms;

	emitter.entity().add<ParticleEmitter>(
		systems(),
		def,
		transform()
	).start();
}

Interface::operator Interface_const() const
{
	return Interface_const(static_cast<const entity_handle_t&>(*this));
}


Interface create(
	ecs::entity_handle_t handle,
	ecs::entity_handle_t world,
	const ecs::LocalTransform2& transform,
	Environment::Interface environment,
	const PlayerData& playerData
)
{
	auto& systems = handle.systems();

	auto& events = handle.entity().add<Events>();
	auto& trans = handle.entity().add<ecs::Transform2>(transform);

	auto bodyDef = *handle.systems().physicsResources().loadBodyDefinition(
		playerData.ship.get().hull.bodyId
	).get();

	for (auto& shape : bodyDef.shapes)
	{
		shape.filter.categories.reset();
		shape.filter.categories[PhysicsData::shipCategory(playerData.playerIndex)] = true;
		shape.filter.group = playerData.playerIndex + 1;
	}

	auto& body = handle.entity().add<PhysicsBody>(handle.systems().physicsSystem(), bodyDef, trans);

	auto hull = Hull::create(
		environment, 
		events, 
		trans, 
		playerData.ship.get().hull, 
		handle.createChild()
	);

	auto shield = Shield::create(
		environment, 
		events, 
		trans, 
		body, 
		playerData.playerIndex, 
		playerData.ship.get().shield, 
		handle.createChild()
	);
	
	auto guns = handle.createChild();

	for (const auto& gunData : playerData.ship.get().guns)
	{
		Gun::create(world, events, playerData.playerIndex, gunData, body, guns.createChild());
	}
	
	auto engines = handle.createChild();

	for (unsigned int i = 0; i < playerData.ship.get().engines.size(); ++i)
	{
		const auto& engineDef = playerData.ship.get().engines[i];

		auto engine = Engine::create(
			body,
			engineDef.force,
			engineDef.pos,
			engineDef.particleSpawnArea,
			engines.createChild()
		);

		engine.transform().get().setParent(trans);
		engine.transform().get().local() = ecs::LocalTransform2::createTranslation(engineDef.pos);
	}

	handle.entity().add<ShipController>(
		handle.systems(),
		body, 
		hull,
		shield,
		playerData.playerIndex + 1
	);

	return Interface(handle);
}

}

} }
