#include "jam/Trail.hpp"

#include "jam/DestroyOnTimer.hpp"
#include "jam/RenderingData.hpp"
#include <ecs/Mesh2Renderer.hpp>
#include <gr/PlainRenderer.hpp>

namespace pm { namespace jam {

namespace {

const Time::duration TRAIL_LIFETIME = 10000ms;
const Time::duration TRAIL_SPAWN_FREQUENCY = 750ms;

const std::vector<gr::Vector2_r> TRAIL_SHAPE = {
	{ -1.0_r, -1.0_r },
	{  1.0_r, -1.0_r },
	{  1.0_r,  1.0_r },
	{ -1.0_r,  1.0_r }
};

}

Trail::Trail(
	ecs::entity_handle_t trailOwner,
	ecs::component_reference<const ecs::Transform2> transform,
	gr::MeshHandle mesh
)
: _trailOwner(trailOwner),
  _transform(transform),
  _mesh(mesh),
  _spawnTimer(TRAIL_SPAWN_FREQUENCY),
  _updateScope()
{
	_updateScope = _trailOwner.systems().updateSystem().add<PM_TMETHOD(Trail::update)>(*this);
}

void Trail::update(const Time::duration& dt)
{
	_spawnTimer -= dt;

	if (_spawnTimer <= Time::duration::zero())
	{
		_spawnTimer += TRAIL_SPAWN_FREQUENCY;

		auto trail = _trailOwner.createChild();

		trail.entity().add<DestroyOnTimer>(trail, TRAIL_LIFETIME);
		auto& mesh = trail.entity().add<gr::MeshHandle>(_mesh);
		auto& trans = trail.entity().add<ecs::Transform2>(_transform.get().world());

		trail.entity().add<ecs::Mesh2Renderer>(
			trail.systems().renderSystem(),
			gr::PlainRenderer::render,
			mesh,
			trans,
			RenderingData::GAME_OBJECT_TYPE
		);
	}
}

} }