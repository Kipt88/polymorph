#include "jam/DestroyOnTimer.hpp"

namespace pm { namespace jam {

DestroyOnTimer::DestroyOnTimer(ecs::entity_handle_t entity, const Time::duration& timer)
: _entity(entity),
  _timer(timer),
  _scope()
{
	_scope = entity.systems().updateSystem().add<PM_TMETHOD(DestroyOnTimer::update)>(*this);
}

void DestroyOnTimer::update(const Time::duration& dt)
{
	_timer -= dt;

	if (_timer < Time::duration::zero())
		_entity.remove();
}

} }
