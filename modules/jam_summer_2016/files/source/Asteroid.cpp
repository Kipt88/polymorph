#include "jam/Asteroid.hpp"

#include "jam/RenderingData.hpp"
#include "jam/AsteroidSubDivider.hpp"
#include <ecs/Mesh2Renderer.hpp>
#include <gr/PlainRenderer.hpp>
#include <random>

namespace pm { namespace jam {

namespace Asteroid {

Interface_const::Interface_const(const_entity_handle_t handle)
: const_entity_handle_t(handle)
{
}

ecs::component_reference<const PhysicsBody> Interface_const::body() const
{
	return entity().get<PhysicsBody>();
}

ecs::component_reference<const ecs::Transform2> Interface_const::transform() const
{
	return entity().get<ecs::Transform2>();
}

Interface::Interface(entity_handle_t handle)
: entity_handle_t(handle)
{
}

ecs::component_reference<PhysicsBody> Interface::body() const
{
	return entity().get<PhysicsBody>();
}

ecs::component_reference<const ecs::Transform2> Interface::transform() const
{
	return entity().get<ecs::Transform2>();
}

Interface::operator Interface_const() const
{
	return Interface_const(static_cast<const entity_handle_t&>(*this));
}

Interface create(
	ecs::entity_handle_t handle,
	ecs::entity_handle_t world,
	const GameData::AsteroidData& asteroidDef,
	const ecs::LocalTransform2& transform
)
{
	auto& systems = handle.systems();

	auto& bodyProvider = handle.systems().physics().bodyProvider();

	auto& trans = handle.entity().add<ecs::Transform2>(transform);
	auto& mesh = handle.entity().add<gr::MeshHandle>(systems.graphicsResources().acquireMesh(asteroidDef.mesh));
	
	handle.entity().add<ecs::Mesh2Renderer>(
		handle.systems().renderSystem(),
		gr::PlainRenderer::render,
		mesh,
		trans,
		RenderingData::GAME_OBJECT_TYPE
	);

	auto& body = handle.entity().add<PhysicsBody>(
		handle.systems().physicsSystem(),
		systems.physicsResources().loadBodyDefinition(asteroidDef.body).get(),
		trans
	);

	handle.entity().add<AsteroidSubDivider>(
		world,
		handle,
		asteroidDef,
		trans,
		body
	);

	return Interface(handle);
}

}

} }
