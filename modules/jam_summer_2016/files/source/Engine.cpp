#include "jam/Engine.hpp"

#include "jam/Engine/Controller.hpp"
#include "jam/Engine/GamepadHandler.hpp"
#include "jam/Engine/KeyHandler.hpp"
#include "jam/RenderingData.hpp"

namespace pm { namespace jam {

namespace Engine {

namespace {

constexpr unsigned int FORWARD_EMITTER_INDEX = 0;
constexpr unsigned int BACKWARD_EMITTER_INDEX = 1;
constexpr unsigned int CONTROLLER_INDEX = 2;

auto createEngineParticleEmitter(const gr::Vector2_r& force)
{
	auto angle = std::atan2(-force[1], -force[0]);

	ParticleData def;

	def.clusterSize.min = 1;
	def.clusterSize.max = 2;

	def.speed.min = 30.0_r;
	def.speed.max = 40.0_r;

	def.acceleration.min = -0.2_r;
	def.acceleration.max = -0.1_r;

	def.angle.min = angle - 0.125_r * math::HALF_PI;
	def.angle.max = angle + 0.125_r * math::HALF_PI;

	def.color = gr::GREEN;

	def.lifetime.min = 500ms;
	def.lifetime.max = 750ms;

	def.spawnFrequency = 50ms;

	def.maxParticles = 100;
	def.renderCategory = RenderingData::GAME_OBJECT_TYPE;
	def.renderOrder = 1;

	return def;
}

}

Interface_const::Interface_const(const_entity_handle_t handle)
: const_entity_handle_t(handle)
{
}

ecs::component_reference<const ecs::Transform2> Interface_const::transform() const
{
	return entity().get<ecs::Transform2>();
}


Interface::Interface(entity_handle_t handle)
: entity_handle_t(handle)
{
}

ecs::component_reference<ecs::Transform2> Interface::transform() const
{
	return entity().get<ecs::Transform2>();
}

void Interface::setKeyControlled(os::key_t forward, os::key_t backward) const
{
	auto controllerHandle = child(CONTROLLER_INDEX);
	auto& controller = controllerHandle.entity().get<Controller>();

	controllerHandle.entity().add<KeyHandler>(systems().keySystem(), controller, forward, backward);
}

void Interface::removeKeyControl() const
{
	auto controllerHandle = child(CONTROLLER_INDEX);
	
	if (controllerHandle.entity().has<KeyHandler>())
		controllerHandle.entity().remove<KeyHandler>();
}

void Interface::setGamepadControlled(unsigned int padId, unsigned int axis, const gr::Vector2_r& forwardDir) const
{
	auto controllerHandle = child(CONTROLLER_INDEX);
	auto& controller = controllerHandle.entity().get<Controller>();

	controllerHandle.entity().add<GamepadHandler>(systems().gamepadSystem(), controller, padId, axis, forwardDir);
}

void Interface::removeGamepadControl() const
{
	auto controllerHandle = child(CONTROLLER_INDEX);

	if (controllerHandle.entity().has<GamepadHandler>())
		controllerHandle.entity().remove<GamepadHandler>();
}

void Interface::removeControl() const
{
	auto controllerHandle = child(CONTROLLER_INDEX);

	if (controllerHandle.entity().has<GamepadHandler>())
		controllerHandle.entity().remove<GamepadHandler>();

	if (controllerHandle.entity().has<KeyHandler>())
		controllerHandle.entity().remove<KeyHandler>();

	controllerHandle.entity().get<Controller>().stop();
}

void Interface::setPower(gr::real_t power) const
{
	auto& controller = child(CONTROLLER_INDEX).entity().get<Controller>();

	controller.setPower(power);
}

Interface::operator Interface_const() const
{
	return Interface_const(static_cast<const entity_handle_t&>(*this));
}


Interface create(
	ecs::component_reference<PhysicsBody> body,
	const ecs::Vector2_r& force,
	const ecs::Vector2_r& pos,
	const math::AxisAlignedBox2<gr::real_t>& particleSpawnArea,
	ecs::entity_handle_t handle
)
{
	auto& trans = handle.entity().add<ecs::Transform2>();

	auto emitterForward = handle.createChild();
	
	auto emitterForwardData = createEngineParticleEmitter(force);
	emitterForwardData.spawnArea = particleSpawnArea;

	auto& particlesForward = emitterForward.entity().add<ParticleEmitter>(
		handle.systems(),
		emitterForwardData,
		trans
	);

	auto emitterBackward = handle.createChild();
	
	auto emitterBackwardData = createEngineParticleEmitter(-force);
	emitterBackwardData.spawnArea = particleSpawnArea;

	auto& particlesBackward = emitterBackward.entity().add<ParticleEmitter>(
		handle.systems(),
		emitterBackwardData,
		trans
	);

	handle.createChild().entity().add<Controller>(
		handle.systems().updateSystem(), 
		body, 
		particlesForward,
		particlesBackward,
		force,
		pos
	);

	return Interface(handle);
}

}

} }
