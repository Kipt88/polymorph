#include "jam/DestroyOnCollision.hpp"

namespace pm { namespace jam {

DestroyOnCollision::DestroyOnCollision(ecs::entity_handle_t entity, PhysicsBody& body)
: _entity(entity),
  _scope()
{
	body.setCollisionHandler<PM_TMETHOD(DestroyOnCollision::onCollision)>(*this);
}

void DestroyOnCollision::onCollision(const physics2d::CollisionData&)
{
	_scope = _entity.systems().updateSystem().add<PM_TMETHOD(DestroyOnCollision::destroyEntity)>(*this);
}

void DestroyOnCollision::destroyEntity(const Time::duration&)
{
	_entity.remove();
}

} }
