#include "AppRunner.hpp"

#include "jam/Environment.hpp"
#include "jam/GameState.hpp"
#include "gen/bodies.hpp"
#include "gen/meshes.hpp"
#include <os/DisplaySettings.hpp>
#include <filesystem/Paths.hpp>
#include <physics2d/BodyProvider.hpp>

#ifdef _DEBUG
	#include "jam/Debug/Debug.hpp"
#endif

namespace pm {

void startupMain(ecs::Game& game)
{
#ifdef _DEBUG
	jam::Debug::create(game.scene().create());
#endif

	{
		filesystem::path root{ gen::bodies::ROOT };

		if (root.is_relative())
			root = filesystem::Paths::executablePath().remove_filename() / root;

		for (const auto& body : gen::bodies::ALL)
		{
			physics2d::BodyProvider::registerBodyDefinition(
				body.id,
				root / body.path
			);
		};
	}

	{
		filesystem::path root{ gen::meshes::ROOT };

		if (root.is_relative())
			root = filesystem::Paths::executablePath().remove_filename() / root;

		for (const auto& mesh : gen::meshes::ALL)
		{
			gr::MeshProvider::registerMesh(
				mesh.id,
				root / mesh.path,
				gr::MeshProvider::mesh_t::MESH
			);
		}
	}
	
	jam::Environment::Interface env = jam::Environment::create(game.scene().create());

	//

	jam::SessionData session;

	session.level = &jam::LevelData::LEVEL_1;

	jam::PlayerData p1{
		0,
		std::nullopt,
		std::nullopt, /*jam::KeyboardControls::LEFT_WASD, */
		jam::CameraType::FIXED_ROTATION,
		jam::ShipData::SHIP_TYPE_1
	};

	jam::PlayerData p2{
		1,
		std::nullopt,
		jam::KeyboardControls::RIGHT_NUMPAD,
		jam::CameraType::FOLLOW,
		jam::ShipData::SHIP_TYPE_1
	};

	jam::PlayerData p3{
		2,
		std::nullopt,
		std::nullopt,
		jam::CameraType::FOLLOW,
		jam::ShipData::SHIP_TYPE_1
	};

	jam::PlayerData p4{
		3,
		std::nullopt,
		std::nullopt,
		jam::CameraType::FOLLOW,
		jam::ShipData::SHIP_TYPE_2
	};

	session.players.push_back(p1);
	session.players.push_back(p2);

	/*
	session.players.push_back(p3);
	session.players.push_back(p4);
	*/
	//

	jam::GameState::create(game.newState(), env, session);

	game.scene().systems().app().pushAction(
		[](os::App& app)
		{
			auto settings = os::DisplaySettings::current();

			app.window().enterFullscreen(settings);
		}
	);
}

}
