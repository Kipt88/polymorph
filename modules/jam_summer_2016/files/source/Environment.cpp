#include "jam/Environment.hpp"

#include "jam/RenderingData.hpp"
#include "jam/GameData.hpp"
#include <ecs/Camera.hpp>

namespace pm { namespace jam {

namespace Environment {

namespace {

gr::AABox2 playerUIScreenArea(const gr::AABox2& box, unsigned int playerIndex, unsigned int playerCount)
{
	ASSERT(playerCount > 0, "Trying to get screen area with 0 players.");
	ASSERT(playerCount < 5, "Trying to get screen area with >4 players.");
	ASSERT(playerIndex < playerCount, "Trying to get screen area with invalid player.");

	switch (playerCount)
	{
	case 1:
		return box;
	case 2:
		return gr::AABox2{
			box.min()[0] + 0.5_r * box.width() * playerIndex,
			box.min()[1],
			box.min()[0] + 0.5_r * box.width() * (playerIndex + 1.0_r),
			box.min()[1] + box.height()
		};
	case 3:
	case 4:
		return gr::AABox2{
			box.min()[0] + 0.5_r * box.width()  * (playerIndex % 2),
			box.min()[1] + 0.5_r * box.height() * (playerIndex < 2 ? 1 : 0),
			box.min()[0] + 0.5_r * box.width()  * ((playerIndex % 2) + 1.0_r),
			box.min()[1] + 0.5_r * box.height() * ((playerIndex < 2 ? 1 : 0) + 1.0_r)
		};
	}

	// Safeguard.
	std::terminate();
}

}

class Controller
{
public:
	explicit Controller(ecs::entity_handle_t entity);

	const Events& events() const;

private:
	ecs::entity_handle_t _entity;
	Events _events;
	os::AppEventScope<os::window_events::Resize> _onResize;
};

Controller::Controller(ecs::entity_handle_t entity)
: _entity(entity),
  _events(),
  _onResize(
	  entity.systems().updateSystem().addWindowEventListener<os::window_events::Resize>(
		  [this](const os::window_events::Resize& ev) mutable
		  {
			  gr::real_t screenW = ev.w;
			  gr::real_t screenH = ev.h;

			  auto& uiCam = _entity.child(0).entity().get<ecs::Camera>();
			  uiCam.projection() = gr::Projection::ortho(
				  -0.5f * screenW,
				   0.5f * screenW,
				  -0.5f * screenH,
				   0.5f * screenH
			  );

			  auto playerCameras = _entity.child(1);

			  auto aspect = screenW / screenH;

			  auto h = (playerCameras.childrenSize() <= 2 ? 1.0_r : 2.0_r) * RenderingData::CAMERA_VISIBLE_HEIGHT;
			  auto w = aspect * h;

			  auto gameCameraProjection = gr::Projection::ortho(-0.5_r * w, 0.5_r * w, -0.5_r * h, 0.5_r * h);

			  gr::AABox2 worldArea{ -0.5_r * w, -0.5_r * h, 0.5_r * w, 0.5_r * h };
			  gr::AABox2 clipArea{ gr::Vector2_r::ZERO, gr::Vector2_r{ screenW, screenH } };

			  unsigned int count = playerCameras.childrenSize();

			  for (unsigned int i = 0; i < count; ++i)
			  {
				  auto camera = playerCameras.child(i);

				  auto& trans = camera.entity().get<ecs::Transform2>();

				  trans.local() = ecs::LocalTransform2::createTranslation(
					  -playerUIScreenArea(
						  worldArea, 
						  i, 
						  count
					  ).middle()
				  );

				  auto& cam = camera.entity().get<ecs::Camera>();
				  cam.projection() = gameCameraProjection;
				  cam.setClipArea(playerUIScreenArea(clipArea, i, count));

				  _events.triggerEvent<UIAreaChanged>(
					  {
						  playerUIScreenArea(
							  gr::AABox2{ -0.5_r * screenW, -0.5_r * screenH, 0.5_r * screenW, 0.5_r * screenH },
							  i,
							  count
						  ),
						  i
					  }
				  );
			  }
		  }
	  )
  )
{
}

const Events& Controller::events() const
{
	return _events;
}

Interface_const::Interface_const(ecs::const_entity_handle_t handle)
: ecs::const_entity_handle_t(handle)
{
}

const Events& Interface_const::events() const
{
	return entity().get<Controller>().events();
}

gr::AABox2 Interface_const::uiScreenArea(unsigned int playerIndex) const
{
	const auto& screenSize = systems().graphics().getSurfaceSize();

	return playerUIScreenArea(
		gr::AABox2{ 
			-0.5_r * static_cast<gr::Vector2_r>(screenSize),
			 0.5_r * static_cast<gr::Vector2_r>(screenSize)
		}, 
		playerIndex, 
		child(1).childrenSize()
	);
}

ecs::component_reference<const ecs::Transform2> Interface_const::cameraTransform(unsigned int playerIndex) const
{
	return child(1).child(playerIndex).entity().get<ecs::Transform2>();
}


Interface::Interface(ecs::entity_handle_t handle)
: ecs::entity_handle_t(handle)
{
}

const Events& Interface::events() const
{
	return entity().get<Controller>().events();
}

gr::AABox2 Interface::uiScreenArea(unsigned int playerIndex) const
{
	return static_cast<const Interface_const&>(*this).uiScreenArea(playerIndex);
}

ecs::component_reference<ecs::Transform2> Interface::cameraTransform(unsigned int playerIndex) const
{
	return child(1).child(playerIndex).entity().get<ecs::Transform2>();
}

void Interface::setCameras(unsigned int count) const
{
	auto cameras = child(1);

	cameras.removeChildren();

	const auto& screenSize = systems().graphics().getSurfaceSize();

	auto aspect = static_cast<gr::real_t>(screenSize[0]) / screenSize[1];

	auto h = (count <= 2 ? 1.0_r : 2.0_r) * RenderingData::CAMERA_VISIBLE_HEIGHT;
	auto w = aspect * h;

	auto gameCameraProjection = gr::Projection::ortho(-0.5_r * w, 0.5_r * w, -0.5_r * h, 0.5_r * h);

	gr::AABox2 worldArea{ -0.5_r * w, -0.5_r * h, 0.5_r * w, 0.5_r * h };
	gr::AABox2 clipArea{ gr::Vector2_r::ZERO, static_cast<gr::Vector2_r>(screenSize) };
	
	for (unsigned int i = 0; i < count; ++i)
	{
		auto camera = cameras.createChild();
		
		auto& trans = camera.entity().add<ecs::Transform2>(
			ecs::LocalTransform2::createTranslation(-playerUIScreenArea(worldArea, i, count).middle())
		);

		auto& cam = camera.entity().add<ecs::Camera>(
			systems().renderSystem(),
			trans,
			RenderingData::GAME_OBJECT_TYPE,
			gameCameraProjection,
			gr::BLACK,
			static_cast<int>(i)
		);
		
		cam.setClipArea(playerUIScreenArea(clipArea, i, count));
	}
}

Interface::operator Interface_const() const
{
	return Interface_const(static_cast<const ecs::const_entity_handle_t&>(*this));
}

Interface create(ecs::entity_handle_t handle)
{
	handle.entity().add<Controller>(handle);

	const auto& screenSize = handle.systems().graphics().getSurfaceSize();

	auto uiCam = handle.createChild();
	
	auto& trans = uiCam.entity().add<ecs::Transform2>();

	uiCam.entity().add<ecs::Camera>(
		handle.systems().renderSystem(),
		trans,
		RenderingData::UI_TYPE,
		gr::Projection::ortho(
			-0.5f * screenSize[0],
			 0.5f * screenSize[0],
			-0.5f * screenSize[1],
			 0.5f * screenSize[1]
		),
		RenderingData::Order::UI
	);

	handle.createChild(); // Player camera holder.
	
	return Interface(handle);
}

}

}

PM_MAKE_COMPONENT_ID(jam::Environment::Controller);

}
