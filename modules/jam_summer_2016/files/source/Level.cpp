#include "jam/Level.hpp"

#include "jam/AsteroidSpawner.hpp"
#include "jam/RenderingData.hpp"
#include "jam/PhysicsBody.hpp"
#include "gen/meshes.hpp"
#include "gen/bodies.hpp"
#include <ecs/Mesh2Renderer.hpp>
#include <gr/PlainRenderer.hpp>

namespace pm { namespace jam {

namespace Level {

ecs::entity_handle_t create(const LevelData& levelData, ecs::entity_handle_t handle)
{
	auto& systems = handle.systems();

	auto bodyDef = systems.physicsResources().loadBodyDefinition(gen::bodies::EDGE.id).get();
	auto meshHandle = systems.graphicsResources().acquireMesh(gen::meshes::EDGE.id);

	auto border = handle.createChild();

	auto& trans = border.entity().add<ecs::Transform2>();

	border.entity().add<PhysicsBody>(systems.physicsSystem(), bodyDef, trans);

	auto& mesh = border.entity().add<gr::MeshHandle>(meshHandle);
	border.entity().add<ecs::Mesh2Renderer>(
		systems.renderSystem(),
		gr::PlainRenderer::render,
		mesh,
		trans,
		RenderingData::GAME_OBJECT_TYPE
	);

	std::vector<gr::Vector2_r> safeLocations;
	safeLocations.reserve(levelData.spawnLocations.size());

	for (const auto& location : levelData.spawnLocations)
		safeLocations.push_back(location.translation);

	handle.entity().add<AsteroidSpawner>(
		handle, 
		levelData.asteroidCount, 
		safeLocations
	);

	return handle;
}

}

} }
