#include "jam/ParticleEmitter.hpp"

#include <Debug.hpp>

namespace pm { namespace jam {

ParticleEmitter::ParticleEmitter(
	ecs::Systems& systems, 
	const ParticleData& data,
	ecs::component_reference<const ecs::Transform2> transform
)
: _emitting(false),
  _def(data),
  _spawnTimer(data.spawnFrequency),
  _particleData(),
  _vertices(_def.maxParticles),
  _mesh(),
  _transform(transform),
  _dev(),
  _gen(_dev()),
  _renderScope(),
  _updateScope()
{
	{
		id_t id = static_cast<id_t>(reinterpret_cast<std::uintptr_t>(this));

		gr::MeshDefinition<gr::VertexFormat::V2_C4> def;

		def.usage = gr::BufferUsage::DYNAMIC;
		def.vertices.resize(_def.maxParticles);

		_mesh = systems.graphicsResources().createMesh(id, std::move(def));
	}

	_particleData.reserve(_def.maxParticles);

	_updateScope = systems.updateSystem().add<
		PM_TMETHOD(ParticleEmitter::update)
	>(*this);

	_renderScope = systems.renderSystem().add<
		PM_TMETHOD(ParticleEmitter::render),
		PM_TMETHOD(ParticleEmitter::renderType)
	>(*this, _def.renderOrder);
}

void ParticleEmitter::start()
{
	_emitting = true;
}

void ParticleEmitter::stop()
{
	_emitting = false;
}

void ParticleEmitter::update(const Time::duration& dt)
{
	// TODO Use erase remove idiom.
	for (unsigned int i = 0; i < _vertices.size();)
	{
		auto& data = _particleData[i];

		data.timer -= dt;

		if (data.timer <= Time::duration::zero())
		{
			_vertices.erase(_vertices.begin() + i);
			_particleData.erase(_particleData.begin() + i);

			// DEBUG_OUT("ParticleEmitter", "Removing particle.");

			continue;
		}

		auto& vertex = _vertices.get(i);

		vertex.pos += data.velocity * toSeconds<gr::real_t>(dt);
		vertex.color[3] = data.timer.count() / static_cast<gr::real_t>(data.lifetime.count());

		data.velocity += data.acceleration * toSeconds<gr::real_t>(dt);

		++i;
	}

	if (_emitting)
	{
		_spawnTimer += dt;

		auto transform = _transform.get().world();

		while (_vertices.size() < _def.maxParticles && _spawnTimer >= _def.spawnFrequency)
		{
			// DEBUG_OUT("ParticleEmitter", "Adding particle.");

			_spawnTimer -= _def.spawnFrequency;
			
			std::uniform_real_distribution<gr::real_t> angleDist(
				_def.angle.min, 
				_def.angle.max
			);
			
			std::uniform_real_distribution<gr::real_t> speedDist(
				_def.speed.min, 
				_def.speed.max
			);
			
			std::uniform_real_distribution<gr::real_t> accDist(
				_def.acceleration.min, 
				_def.acceleration.max
			);

			std::uniform_real_distribution<gr::real_t> xDist(
				_def.spawnArea.min()[0],
				_def.spawnArea.max()[0]
			);
			
			std::uniform_real_distribution<gr::real_t> yDist(
				_def.spawnArea.min()[1],
				_def.spawnArea.max()[1]
			);

			std::uniform_int_distribution<Time::duration::rep> lifeDist(
				_def.lifetime.min.count(),
				_def.lifetime.max.count()
			);

			std::uniform_int_distribution<unsigned int> clusterDist(
				_def.clusterSize.min,
				_def.clusterSize.max
			);

			auto clusterSize = clusterDist(_gen);

			for (unsigned int i = 0; i < clusterSize && _vertices.size() < _def.maxParticles; ++i)
			{
				{
					gr::Vertex<gr::VertexFormat::V2_C4> v;
					v.color = _def.color;

					v.pos[0] = xDist(_gen);
					v.pos[1] = yDist(_gen);

					v.pos = transform * v.pos;

					_vertices.push_back(v);
				}

				{
					LifeTimeData d;

					auto angle = angleDist(_gen);
					auto speed = speedDist(_gen);
					auto acc = accDist(_gen);
					auto time = Time::duration{ lifeDist(_gen) };

					d.velocity = d.acceleration = { std::cos(angle), std::sin(angle) };

					auto t = transform;
					t.translation = gr::Vector2_r::ZERO;

					d.velocity = t * d.velocity * speed;
					d.acceleration = t * d.velocity * acc;

					d.timer = d.lifetime = time;

					_particleData.push_back(d);
				}
			}
		}
	}
}

void ParticleEmitter::render(gr::ClientFrame& frame) const
{
	if (!_vertices.empty())
	{
		frame.push(
			[vertices = _vertices, mesh = _mesh](gr::Surface& surface) mutable
			{
				mesh->vertices().write(vertices.data(), 0, vertices.size());

				mesh->vertices().draw(gr::Primitive::POINTS, 0, vertices.size());
			}
		);
	}
}

ecs::RenderSystem::render_category_t ParticleEmitter::renderType() const
{
	return _def.renderCategory;
}

} }
