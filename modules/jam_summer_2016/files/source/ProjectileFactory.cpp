#include "jam/ProjectileFactory.hpp"

#include "jam/Gun/Projectile.hpp"
#include "jam/DestroyOnCollision.hpp"
#include "jam/DestroyOnTimer.hpp"
#include "jam/RenderingData.hpp"
#include <ecs/Mesh2Renderer.hpp>
#include <gr/PlainRenderer.hpp>

namespace pm { namespace jam {

ProjectileFactory::ProjectileFactory(
	ecs::entity_handle_t projectileParent,
	ecs::component_reference<const PhysicsBody> shipBody
)
: _projectileParent(projectileParent),
  _shipBody(shipBody),
  _mesh(nullptr),
  _bodyDef(),
  _transform(ecs::LocalTransform2::IDENTITY),
  _impulse(ecs::Vector2_r::ZERO),
  _timeout(Time::duration::zero())
{
}

void ProjectileFactory::create()
{
	auto projectile = _projectileParent.createChild();

	auto& entity = projectile.entity();

	auto& trans = entity.add<ecs::Transform2>(_shipBody.get().transform().world() * _transform);
	auto& body = entity.add<PhysicsBody>(projectile.systems().physicsSystem(), _bodyDef, trans);
	auto& mesh = entity.add<gr::MeshHandle>(_mesh);
	
	entity.add<ecs::Mesh2Renderer>(
		projectile.systems().renderSystem(),
		gr::PlainRenderer::render,
		mesh,
		trans,
		RenderingData::GAME_OBJECT_TYPE
	);

	// entity.add<DestroyOnCollision>(projectile, body);

	entity.add<Gun::Projectile>(
		projectile.systems().updateSystem(),
		_shipBody.get().transform(),
		body,
		13.0_r // TODO Don't hard-code, pass as argument...
	);

	if (_timeout > Time::duration::zero())
		entity.add<DestroyOnTimer>(projectile, _timeout);

	body.applyImpulse(_impulse);
}

void ProjectileFactory::setMesh(gr::MeshHandle mesh)
{
	_mesh = mesh;
}

void ProjectileFactory::setBodyDef(const physics2d::BodyDefinition& bodyDef)
{
	_bodyDef = bodyDef;
}

void ProjectileFactory::setLocalTransform(const ecs::LocalTransform2& transform)
{
	_transform = transform;
}

void ProjectileFactory::setImpulse(const ecs::Vector2_r& impulse)
{
	_impulse = impulse;
}

void ProjectileFactory::setTimeout(const Time::duration& timeout)
{
	_timeout = timeout;
}

} }
