#include "jam/ShipController.hpp"

namespace pm { namespace jam {

ShipController::ShipController(
	ecs::Systems& systems, 
	ecs::component_reference<PhysicsBody> body,
	Hull::Interface hull,
	Shield::Interface shield,
	unsigned int player
)
: _body(body),
  _hull(hull),
  _shield(shield),
  _player(player)
{
	_body.get().setCollisionHandler<PM_TMETHOD(ShipController::onCollision)>(*this);
}

unsigned int ShipController::getPlayer() const
{
	return _player;
}

void ShipController::onCollision(const physics2d::CollisionData& data)
{
	if (!data.firstContact)
		return;

	auto mass = _body.get().getMass();

	if (mass < 0.0001_r)
		return;

	auto keLost = 0.5_r * math::lengthSq(data.worldImpulse) / mass;
	
	if (_shield.energy() >= _shield.maxEnergy())
	{
		_shield.inflictDamage(keLost);

		DEBUG_OUT("ShipController", "Energy left %.2fJ (-%.2fJ)", _shield.energy(), keLost);
	}
	else if (_hull.getHp() > 0.0)
	{
		_hull.inflictDamage(keLost);

		if (_hull.getHp() <= 0.0)
		{
			DEBUG_OUT("ShipController", "Player %i lost!", _player);
			// You lost, too bad....
		}
	}
}

} }
