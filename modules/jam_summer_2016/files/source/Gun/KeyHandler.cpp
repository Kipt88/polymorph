#include "jam/Gun/KeyHandler.hpp"

namespace pm { namespace jam {

namespace Gun {

KeyHandler::KeyHandler(
	ecs::KeySystem& keySystem, 
	ecs::component_reference<Controller> controller, 
	os::key_t key
)
: _controller(controller),
  _key(key),
  _scope()
{
	_scope = keySystem.add<
		PM_TMETHOD(KeyHandler::onKeyDown),
		PM_TMETHOD(KeyHandler::onKeyUp)
	>(*this);
}

void KeyHandler::onKeyDown(os::key_t key)
{
	if (key == _key)
		_controller.get().setActive(true);
}

void KeyHandler::onKeyUp(os::key_t key)
{
	if (key == _key)
		_controller.get().setActive(false);
}

}

} }
