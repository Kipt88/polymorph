#include "jam/Gun/GamepadHandler.hpp"

namespace pm { namespace jam {

namespace Gun {

GamepadHandler::GamepadHandler(
	ecs::GamepadSystem& gamepadSystem,
	ecs::component_reference<Controller> controller,
	unsigned int padId,
	os::GamepadButton button
)
: _controller(controller),
  _button(button),
  _scope()
{
	_scope = gamepadSystem.add<
		PM_TNULL_METHOD(),
		PM_TNULL_METHOD(),
		PM_TMETHOD(GamepadHandler::onGamepadButtonDown),
		PM_TMETHOD(GamepadHandler::onGamepadButtonUp),
		PM_TNULL_METHOD()
	>(*this, padId);
}

void GamepadHandler::onGamepadButtonDown(os::GamepadButton button)
{
	if (button == _button)
		_controller.get().setActive(true);
}

void GamepadHandler::onGamepadButtonUp(os::GamepadButton button)
{
	if (button == _button)
		_controller.get().setActive(false);
}

}

} }
