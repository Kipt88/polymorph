#include "jam/Gun/Projectile.hpp"

#include "jam/PhysicsData.hpp"
#include "jam/LevelData.hpp"

namespace pm { namespace jam { namespace Gun {

Projectile::Projectile(
	ecs::UpdateSystem& updateSystem,
	ecs::component_reference<const ecs::Transform2> shooterTransform,
	ecs::component_reference<PhysicsBody> bulletBody,
	gr::real_t shooterSize
)
: _shooterTransform(shooterTransform),
  _bulletBody(bulletBody),
  _shooterSize(shooterSize),
  _scope()
{
	_scope = updateSystem.add<PM_TMETHOD(Projectile::onUpdate)>(*this);
}

void Projectile::onUpdate(const Time::duration&)
{
	auto shooterPosition = _shooterTransform.get().world().translation;
	auto bulletPosition = _bulletBody.get().transform().world().translation;

	if (math::lengthSq(shooterPosition - bulletPosition) > math::square(_shooterSize))
	{
		_bulletBody.get().withBody(
			[](physics2d::BodyHandle body)
			{
				for (auto shape : body.getShapes())
				{
					auto filter = shape.getCollisionFilter();

					for (unsigned int i = 0; i < MAX_PLAYERS; ++i)
						filter.mask[PhysicsData::shipCategory(i)] = true;

					shape.setCollisionFilter(filter);
				}
			}
		);

		_scope.remove();
	}
}

} } }
