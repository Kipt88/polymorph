#include "jam/Gun/Controller.hpp"

namespace pm { namespace jam {

namespace Gun {

Controller::Controller(
	ecs::UpdateSystem& updateSystem, 
	Ship::Events& events,
	const Time::duration& cooldown
)
: _projectileFactory{ nullptr, nullptr },
  _cooldown(cooldown),
  _timer(cooldown),
  _shooting(false),
  _events(events),
  _updateScope()
{
	_updateScope = updateSystem.add<
		PM_TMETHOD(Controller::onUpdate)
	>(*this);
}

void Controller::setActive(bool active)
{
	_shooting = active;
}

void Controller::onUpdate(const Time::duration& dt)
{
	if (_timer >= Time::duration::zero())
	{
		auto before = _timer;

		_timer -= dt;

		if (_timer <= Time::duration::zero())
		{
			_timer += _cooldown;

			if (_shooting)
			{
				if (_projectileFactory.obj)
					_projectileFactory.createProjectile(_projectileFactory.obj);
			}
			else
			{
				_timer = Time::duration::zero();
			}
		}

		if (before != _timer)
		{
			_events.triggerEvent<Ship::WeaponCharge>(
				{
					_cooldown - before,
					_cooldown - _timer,
					_cooldown
				}
			);
		}
	}
}

}

} }