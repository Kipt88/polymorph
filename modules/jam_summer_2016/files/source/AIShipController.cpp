#include "jam/AIShipController.hpp"

#include "jam/AI/Controller.hpp"

namespace pm { namespace jam {

namespace AIShipController {

void attachController(
	level_data_ref levelData,
	Ship::Interface ship, 
	const std::vector<Ship::Interface_const>& otherShips
)
{
	ship.entity().add<AI::Controller>(
		levelData,
		ship,
		otherShips
	);
}

}

} }