#include "jam/AsteroidSubDivider.hpp"

#include "jam/Asteroid.hpp"
#include "jam/ParticleEmitter.hpp"
#include "jam/RenderingData.hpp"
#include "jam/DestroyOnTimer.hpp"
#include <physics2d/Literals.hpp>

namespace pm { namespace jam {

AsteroidSubDivider::AsteroidSubDivider(
	ecs::entity_handle_t world,
	ecs::entity_handle_t entity,
	const GameData::AsteroidData& asteroidDef,
	ecs::component_reference<ecs::Transform2> transform,
	ecs::component_reference<PhysicsBody> body
)
: _world(world),
  _entity(entity),
  _asteroidDef(asteroidDef),
  _body(body),
  _transform(transform),
  _scope()
{
	body.get().setCollisionHandler<PM_TMETHOD(AsteroidSubDivider::onCollision)>(*this);
}

void AsteroidSubDivider::onCollision(const physics2d::CollisionData& data)
{
	// if (energy > 1.5_kJ)
	if (math::length(data.worldImpulse) > 1.5_kJ)
		_scope = _entity.systems().updateSystem().add<PM_TMETHOD(AsteroidSubDivider::subDivideEntity)>(*this);
}

void AsteroidSubDivider::subDivideEntity(const Time::duration&)
{
	addParticles();

	auto v = _body.get().getVelocity();

	for (const auto& def : _asteroidDef.parts)
	{
		auto asteroid = Asteroid::create(
			_world.createChild(), 
			_world, 
			GameData::AsteroidData{ {}, def.body, def.mesh },
			_transform.get().world()
		);

		asteroid.body().get().setVelocity(v);
	}

	_entity.remove();
}

void AsteroidSubDivider::addParticles()
{
	ParticleData data;

	data.maxParticles = 60;

	data.clusterSize.min = 10;
	data.clusterSize.max = 20;

	data.speed.min = 10.0_r;
	data.speed.max = 15.0_r;

	data.acceleration.min = -0.02_r;
	data.acceleration.max = -0.01_r;

	data.angle.min = 0.0_r;
	data.angle.max = math::TWO_PI;

	data.color = gr::GRAY;

	data.lifetime.min = 500ms;
	data.lifetime.max = 700ms;

	data.spawnFrequency = 1s;

	data.renderCategory = RenderingData::GAME_OBJECT_TYPE;
	data.renderOrder = 1;

	data.spawnArea = gr::AABox2{ -5.0_r, -5.0_r, 5.0_r, 5.0_r };

	auto emitter = _world.createChild();

	auto& trans = emitter.entity().add<ecs::Transform2>(_transform.get().world());
	emitter.entity().add<ParticleEmitter>(emitter.systems(), data, trans).start();
	emitter.entity().add<DestroyOnTimer>(emitter, 500ms);
}

} }
