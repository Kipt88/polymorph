#include "jam/Hull.hpp"

#include "jam/RenderingData.hpp"
#include "gen/meshes.hpp"
#include <ecs/Mesh2Renderer.hpp>
#include <gr/PlainRenderer.hpp>

namespace pm { namespace jam {

namespace Hull {

class Controller
{
public:
	explicit Controller(Ship::Events& events, physics2d::real_t hp);

	physics2d::real_t getHp() const;
	physics2d::real_t getStartHp() const;
	void inflictDamage(physics2d::real_t hp);

private:
	physics2d::real_t _hp;
	physics2d::real_t _startHp;
	Ship::Events& _events;
};

Controller::Controller(Ship::Events& events, physics2d::real_t hp)
: _hp(hp),
  _startHp(hp),
  _events(events)
{
}

physics2d::real_t Controller::getHp() const
{
	return _hp;
}

physics2d::real_t Controller::getStartHp() const
{
	return _startHp;
}

void Controller::inflictDamage(physics2d::real_t hp)
{
	auto hpBefore = _hp;

	_hp -= hp;

	if (_hp < 0.0)
		_hp = 0.0;

	_events.triggerEvent<Ship::HpChanged>({ hpBefore, _hp, _startHp });
}


Interface_const::Interface_const(ecs::const_entity_handle_t handle)
: ecs::const_entity_handle_t(handle)
{
}

physics2d::real_t Interface_const::getHp() const
{
	return entity().get<Controller>().getHp();
}

physics2d::real_t Interface_const::getMaxHp() const
{
	return entity().get<Controller>().getStartHp();
}


Interface::Interface(ecs::entity_handle_t handle)
: ecs::entity_handle_t(handle)
{
}

physics2d::real_t Interface::getHp() const
{
	return entity().get<Controller>().getHp();
}

physics2d::real_t Interface::getMaxHp() const
{
	return entity().get<Controller>().getStartHp();
}

void Interface::inflictDamage(physics2d::real_t energy) const
{
	entity().get<Controller>().inflictDamage(energy);
}

Interface::operator Interface_const() const
{
	return Interface_const(static_cast<const ecs::const_entity_handle_t&>(*this));
}


Interface create(
	Environment::Interface_const environment,
	Ship::Events& events,
	ecs::component_reference<const ecs::Transform2> transform,
	const HullData& hullData,
	ecs::entity_handle_t handle
)
{
	auto& trans = handle.entity().add<ecs::Transform2>(transform);

	auto& mesh = handle.entity().add<gr::MeshHandle>(
		handle.systems().graphicsResources().acquireMesh(hullData.meshId)
	);

	handle.entity().add<ecs::Mesh2Renderer>(
		handle.systems().renderSystem(),
		gr::PlainRenderer::render,
		mesh,
		trans,
		RenderingData::GAME_OBJECT_TYPE
	);

	handle.entity().add<Controller>(events, hullData.hp);

	return Interface(handle);
}

}

}

PM_MAKE_COMPONENT_ID(jam::Hull::Controller);

}
