#include "jam/PlayerUI.hpp"

#include "jam/ShieldUI.hpp"
#include "jam/HullUI.hpp"
#include "jam/GunUI.hpp"

namespace pm { namespace jam {

namespace PlayerUI {

void create(Environment::Interface environment, Ship::Interface_const ship, ecs::entity_handle_t handle)
{
	HullUI::create(environment, ship, handle.createChild());
	ShieldUI::create(environment, ship, handle.createChild());
	GunUI::create(environment, ship, handle.createChild());
}

}

} }
