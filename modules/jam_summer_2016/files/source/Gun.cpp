#include "jam/Gun.hpp"

#include "jam/Gun/Controller.hpp"
#include "jam/Gun/KeyHandler.hpp"
#include "jam/Gun/GamepadHandler.hpp"
#include "jam/ProjectileFactory.hpp"
#include "jam/PhysicsData.hpp"
#include <method_traits.hpp>

namespace pm { namespace jam {

namespace Gun {

Interface_const::Interface_const(const_entity_handle_t handle)
: const_entity_handle_t(handle)
{
}


Interface::Interface(entity_handle_t handle)
: entity_handle_t(handle)
{
}

void Interface::setKeyControlled(os::key_t key) const
{
	auto& controller = entity().get<Controller>();

	entity().add<KeyHandler>(systems().keySystem(), controller, key);
}

void Interface::removeKeyControl() const
{
	if (entity().has<KeyHandler>())
		entity().remove<KeyHandler>();
}

void Interface::setGamepadControlled(unsigned int padId, os::GamepadButton button) const
{
	auto& controller = entity().get<Controller>();

	entity().add<GamepadHandler>(systems().gamepadSystem(), controller, padId, button);
}

void Interface::removeGamepadControl() const
{
	if (entity().has<GamepadHandler>())
		entity().remove<GamepadHandler>();
}

void Interface::removeControl() const
{
	if (entity().has<GamepadHandler>())
		entity().remove<GamepadHandler>();

	if (entity().has<KeyHandler>())
		entity().remove<KeyHandler>();

	entity().get<Controller>().setActive(false);
}

void Interface::setActive(bool active) const
{
	entity().get<Controller>().setActive(active);
}

Interface::operator Interface_const() const
{
	return Interface_const(static_cast<const entity_handle_t&>(*this));
}


Interface create(
	ecs::entity_handle_t world,
	Ship::Events& events, 
	unsigned int playerIndex,
	const GunData& gunData, 
	ecs::component_reference<const PhysicsBody> body,
	ecs::entity_handle_t handle
)
{
	auto& controller = handle.entity().add<Controller>(
		handle.systems().updateSystem(), 
		events, 
		gunData.cooldownPeriod
	);

	auto bodyDef = *handle.systems().physicsResources().loadBodyDefinition(
		gunData.bodyId
	).get();

	for (auto& shape : bodyDef.shapes)
	{
		shape.filter.categories[PhysicsData::shipCategory(playerIndex)] = true;

		shape.filter.mask[PhysicsData::shipCategory(playerIndex)] = false;
	}

	auto projectileMesh = handle.systems().graphicsResources().acquireMesh(gunData.meshId);

	auto& factory = handle.entity().add<ProjectileFactory>(world, body);

	factory.setLocalTransform(ecs::LocalTransform2::createTranslation(gunData.pos));
	factory.setImpulse(gunData.impulse);
	factory.setTimeout(gunData.lifetime);
	
	factory.setMesh(projectileMesh);
	factory.setBodyDef(bodyDef);

	controller.setProjectileFactory<PM_TMETHOD(ProjectileFactory::create)>(factory);

	return Interface(handle);
}

}

} }
