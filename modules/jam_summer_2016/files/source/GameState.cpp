#include "jam/GameState.hpp"

#include "jam/AI/FacePosition.hpp"
#include "jam/AI/Move.hpp"

#include "jam/Ship.hpp"
#include "jam/Level.hpp"
#include "jam/PlayerUI.hpp"
#include "jam/ResetCondition.hpp"
#include "jam/GameData.hpp"
#include "jam/ShipPlayerController.hpp"
#include "jam/AIShipController.hpp"
#include "jam/Debug/Debug.hpp"
#include <filesystem/Paths.hpp>
#include <random>

namespace pm { namespace jam {

namespace GameState {

void create(ecs::entity_handle_t handle, Environment::Interface env, const SessionData& sessionData)
{
#ifdef _DEBUG
	Debug::get().clear();
#endif

	handle.systems().physicsSystem().setDamping(1.0_r);

	env.setCameras(sessionData.players.size());

	auto level = Level::create(*sessionData.level, handle.createChild());

	auto& resetCondition = handle.createChild().entity().add<ResetCondition>(handle, env);

	std::vector<ecs::LocalTransform2> spawnLocations(
		sessionData.level->spawnLocations.begin(),
		sessionData.level->spawnLocations.end()
	);

	std::random_device rd;
	std::mt19937 g(rd());

	std::shuffle(spawnLocations.begin(), spawnLocations.end(), g);

	std::vector<Ship::Interface> ships;
	ships.reserve(sessionData.players.size());

	for (const auto& player : sessionData.players)
	{
		auto location = spawnLocations.back();
		spawnLocations.pop_back();

		auto ship = Ship::create(
			level.createChild(),
			handle,
			location,
			env,
			player
		);

		ships.push_back(ship);

		env.cameraTransform(player.playerIndex).get().setParent(
			player.camera.attachController(ship.transform(), ship.createChild())
		);

		if (!player.isAIControlled())
			ShipPlayerController::attachController(player, ship);

		PlayerUI::create(env, ship, handle.createChild());

		resetCondition.addShip(ship);
	}

	std::vector<Ship::Interface_const> constShips;
	constShips.reserve(ships.size());

	for (const auto& ship : ships)
		constShips.push_back(ship);

	for (unsigned int i = 0; i < ships.size(); ++i)
	{
		if (sessionData.players[i].isAIControlled())
			AIShipController::attachController(*sessionData.level, ships[i], constShips);
	}

	/*
	auto& testRotate = ships[3].entity().add<TestRotate>(
		AI::Move{},
		AI::FacePosition{},
		ships[2],
		ships[1],
		ecs::UpdateSystem::UpdaterScope{}
	);

	testRotate.scope = handle.systems().updateSystem().add<
		PM_TMETHOD(TestRotate::update)
	>(testRotate);
	*/
}

} 

} }
