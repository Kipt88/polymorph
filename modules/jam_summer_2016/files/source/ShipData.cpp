#include "jam/ShipData.hpp"

#include "gen/bodies.hpp"
#include "gen/meshes.hpp"
#include <physics2d/Literals.hpp>

namespace pm { namespace jam {

const ShipData ShipData::SHIP_TYPE_1{
	HullData{ 
		gen::bodies::SHIP_1.id,
		gen::meshes::SHIP_1.id,
		1500.0_kJ 
	},
	ShieldData{
		40.0_kJ,
		10.0_kJ,
		12.0_m,
		0.7f
	},
	{
		GunData{
			gen::bodies::CIRCLE_PROJECTILE.id,
			gen::meshes::CIRCLE_PROJECTILE.id,
			ecs::Vector2_r{ 0.0_r, 8.7_r },
			ecs::Vector2_r{ 0.0_r, 4000.0_r },
			1000ms,
			1500ms,
			GunData::GUN_BOUNCY_PROJECTILE
		}
	},
	{
		EngineData{
			ecs::Vector2_r{ 0.0_r, 300.0_r },
			ecs::Vector2_r{ -8.8_r, -4.5_r },
			gr::AABox2{{-0.5_r, -0.5_r}, {0.5_r, 0.5_r}}
		},
		EngineData{
			ecs::Vector2_r{ 0.0_r, 300.0_r },
			ecs::Vector2_r{ 8.8_r, -4.5_r },
			gr::AABox2{ -0.5_r, -0.5_r, 0.5_r, 0.5_r }
		},
		EngineData{
			ecs::Vector2_r{ 3000.0_r, 0.0_r },
			ecs::Vector2_r{ -7.5_r, -0.15_r },
			gr::AABox2{ -0.5_r, -0.5_r, 0.5_r, 0.5_r }
		},
		EngineData{
			ecs::Vector2_r{ -3000.0_r, 0.0_r },
			ecs::Vector2_r{ 7.5_r, -0.15_r },
			gr::AABox2{ -0.5_r, -0.5_r, 0.5_r, 0.5_r }
		},
		EngineData{
			ecs::Vector2_r{ 0.0_r, 7000.0_r },
			ecs::Vector2_r{ 0.0_r, -3.5_r },
			gr::AABox2{ -4.0_r, -0.5_r, 4.0_r, 0.5_r }
		}
	}
};

const ShipData ShipData::SHIP_TYPE_2{
	HullData{ 
		gen::bodies::SHIP_2.id,
		gen::meshes::SHIP_2.id,
		1500.0_kJ 
	},
	ShieldData{
		40.0_kJ,
		10.0_kJ,
		12.0_m,
		0.7f
	},
	{
		GunData{
			gen::bodies::CIRCLE_PROJECTILE.id,
			gen::meshes::CIRCLE_PROJECTILE.id,
			ecs::Vector2_r{ -7.0_r, 4.0_r },
			ecs::Vector2_r{ 0.0_r, 4000.0_r },
			2000ms,
			1500ms,
			GunData::GUN_BOUNCY_PROJECTILE
		},
		GunData{
			gen::bodies::CIRCLE_PROJECTILE.id,
			gen::meshes::CIRCLE_PROJECTILE.id,
			ecs::Vector2_r{ 7.0_r, 4.0_r },
			ecs::Vector2_r{ 0.0_r, 4000.0_r },
			2000ms,
			1500ms,
			GunData::GUN_BOUNCY_PROJECTILE
		}
	},
	{
		EngineData{
			ecs::Vector2_r{ 0.0_r, 300.0_r },
			ecs::Vector2_r{ -8.8_r, -4.5_r },
			gr::AABox2{{-0.5_r, -0.5_r}, {0.5_r, 0.5_r}}
		},
		EngineData{
			ecs::Vector2_r{ 0.0_r, 300.0_r },
			ecs::Vector2_r{ 8.8_r, -4.5_r },
			gr::AABox2{ -0.5_r, -0.5_r, 0.5_r, 0.5_r }
		},
		EngineData{
			ecs::Vector2_r{ 3000.0_r, 0.0_r },
			ecs::Vector2_r{ -7.5_r, -1.0_r },
			gr::AABox2{ -0.5_r, -0.5_r, 0.5_r, 0.5_r }
		},
		EngineData{
			ecs::Vector2_r{ -3000.0_r, 0.0_r },
			ecs::Vector2_r{ 7.5_r, -1.0_r },
			gr::AABox2{ -0.5_r, -0.5_r, 0.5_r, 0.5_r }
		},
		EngineData{
			ecs::Vector2_r{ 0.0_r, 7000.0_r },
			ecs::Vector2_r{ 0.0_r, -3.5_r },
			gr::AABox2{ -4.0_r, -0.5_r, 4.0_r, 0.5_r }
		}
	}
};

} }
