#pragma once

#include "jam/PhysicsBody.hpp"
#include "jam/Environment.hpp"
#include "jam/GameData.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

namespace Asteroid {

class Interface_const : public ecs::const_entity_handle_t
{
public:
	explicit Interface_const(ecs::const_entity_handle_t handle);

	ecs::component_reference<const PhysicsBody> body() const;
	ecs::component_reference<const ecs::Transform2> transform() const;
};

class Interface : public ecs::entity_handle_t
{
public:
	explicit Interface(ecs::entity_handle_t handle);

	ecs::component_reference<PhysicsBody> body() const;
	ecs::component_reference<const ecs::Transform2> transform() const;

	operator Interface_const() const;
};

Interface create(
	ecs::entity_handle_t handle, 
	ecs::entity_handle_t world,
	const GameData::AsteroidData& asteroidDef,
	const ecs::LocalTransform2& transform
);

}

} }
