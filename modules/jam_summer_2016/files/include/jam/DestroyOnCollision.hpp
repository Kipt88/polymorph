#pragma once

#include "jam/PhysicsBody.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

class DestroyOnCollision
{
public:
	explicit DestroyOnCollision(ecs::entity_handle_t entity, PhysicsBody& body);

private:
	void onCollision(const physics2d::CollisionData&);

	void destroyEntity(const Time::duration&);

	ecs::entity_handle_t _entity;
	ecs::UpdateSystem::UpdaterScope _scope;
};

}

PM_MAKE_COMPONENT_ID(jam::DestroyOnCollision);

}
