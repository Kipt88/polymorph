#pragma once

#include "jam/Environment.hpp"
#include "jam/SessionData.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

namespace GameState {

void create(ecs::entity_handle_t handle, Environment::Interface env, const SessionData& sessionData);

}

} }
