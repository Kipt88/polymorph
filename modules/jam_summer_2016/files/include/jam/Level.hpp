#pragma

#include "jam/Environment.hpp"
#include "jam/LevelData.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

namespace Level {

ecs::entity_handle_t create(const LevelData& levelData, ecs::entity_handle_t handle);

}

} }
