#pragma once

#include <ecs/Transform.hpp>
#include <gr/types.hpp>
#include <Id.hpp>
#include <array>
#include <functional>

namespace pm { namespace jam {

constexpr unsigned int MAX_PLAYERS = 4;

struct LevelData
{
	id_t bodyId;
	id_t meshId;
	id_t navmeshId;

	unsigned int asteroidCount;

	std::array<ecs::LocalTransform2, MAX_PLAYERS> spawnLocations;

	static const LevelData LEVEL_1;
};

typedef std::reference_wrapper<const LevelData> level_data_ref;

} }
