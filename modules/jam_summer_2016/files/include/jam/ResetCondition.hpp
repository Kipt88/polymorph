#pragma once

#include "jam/ShipEvents.hpp"
#include "jam/Ship.hpp"
#include <ecs/Scene.hpp>
#include <list>

namespace pm { namespace jam {

class ResetCondition
{
public:
	explicit ResetCondition(ecs::entity_handle_t state, Environment::Interface env);

	void addShip(Ship::Interface ship);

private:
	class Handler : Unique
	{
	public:
		explicit Handler(ResetCondition* thiz, Ship::Interface ship);

	private:
		void onDamaged(const Ship::HpChanged& ev);

		ResetCondition* _thiz;

		Ship::Interface _ship;
		Ship::Events::scope_t<Ship::HpChanged> _scope;
	};

	void onUpdate(const Time::duration& dt);

	ecs::entity_handle_t _state;
	Environment::Interface _env;

	Time::duration _transitionTime;

	ecs::UpdateSystem::UpdaterScope _updateScope;

	std::list<Handler> _handlers;
};

}

PM_MAKE_COMPONENT_ID(jam::ResetCondition);

}
