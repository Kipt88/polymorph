#pragma once

#include "jam/ObjectMeshes.hpp"
#include <ecs/Scene.hpp>
#include <ecs/Transform.hpp>

namespace pm { namespace jam {

namespace Environment {

struct UIAreaChanged
{
	gr::AABox2 area;
	unsigned int playerIndex;
};

typedef WeakEventManager<UIAreaChanged> Events;

class Interface_const : public ecs::const_entity_handle_t
{
public:
	explicit Interface_const(ecs::const_entity_handle_t handle);

	const Events& events() const;

	ecs::component_reference<const ecs::Transform2> cameraTransform(unsigned int playerIndex) const;
	gr::AABox2 uiScreenArea(unsigned int playerIndex) const;
};

class Interface : public ecs::entity_handle_t
{
public:
	explicit Interface(ecs::entity_handle_t handle);

	const Events& events() const;

	gr::AABox2 uiScreenArea(unsigned int playerIndex) const;
	ecs::component_reference<ecs::Transform2> cameraTransform(unsigned int playerIndex) const;
	
	void setCameras(unsigned int count) const;

	operator Interface_const() const;
};

Interface create(ecs::entity_handle_t handle);

}

} }
