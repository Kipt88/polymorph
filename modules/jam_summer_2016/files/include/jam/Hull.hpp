#pragma once

#include "jam/ShipEvents.hpp"
#include "jam/Environment.hpp"
#include "jam/ShipData.hpp"
#include <physics2d/types.hpp>
#include <ecs/Scene.hpp>
#include <Event.hpp>

namespace pm { namespace jam {

namespace Hull {

class Interface_const : public ecs::const_entity_handle_t
{
public:
	explicit Interface_const(ecs::const_entity_handle_t handle);

	physics2d::real_t getHp() const;
	physics2d::real_t getMaxHp() const;
};

class Interface : public ecs::entity_handle_t
{
public:
	explicit Interface(ecs::entity_handle_t handle);

	physics2d::real_t getHp() const;
	physics2d::real_t getMaxHp() const;

	void inflictDamage(physics2d::real_t energy) const;

	operator Interface_const() const;
};

Interface create(
	Environment::Interface_const environment,
	Ship::Events& events,
	ecs::component_reference<const ecs::Transform2> transform,
	const HullData& hullData,
	ecs::entity_handle_t handle
);

}

} }
