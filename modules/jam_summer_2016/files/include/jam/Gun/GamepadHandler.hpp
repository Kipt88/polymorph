#pragma once

#include "jam/Gun/Controller.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

namespace Gun {

class GamepadHandler
{
public:
	explicit GamepadHandler(
		ecs::GamepadSystem& gamepadSystem, 
		ecs::component_reference<Controller> controller, 
		unsigned int padId,
		os::GamepadButton button
	);

private:
	void onGamepadButtonDown(os::GamepadButton button);
	void onGamepadButtonUp(os::GamepadButton button);

	ecs::component_reference<Controller> _controller;
	os::GamepadButton _button;
	ecs::GamepadSystem::HandlerScope _scope;
};

}

}

PM_MAKE_COMPONENT_ID(jam::Gun::GamepadHandler);

}
