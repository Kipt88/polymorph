#pragma once

#include "jam/PhysicsBody.hpp"
#include <ecs/UpdateSystem.hpp>
#include <ecs/Transform.hpp>
#include <ecs/ComponentId.hpp>

namespace pm { namespace jam { namespace Gun {

class Projectile
{
public:
	explicit Projectile(
		ecs::UpdateSystem& updateSystem,
		ecs::component_reference<const ecs::Transform2> shooterTransform,
		ecs::component_reference<PhysicsBody> bulletBody,
		gr::real_t shooterSize
	);

private:
	void onUpdate(const Time::duration&);

	ecs::component_reference<const ecs::Transform2> _shooterTransform;
	ecs::component_reference<PhysicsBody> _bulletBody;
	gr::real_t _shooterSize;

	ecs::UpdateSystem::UpdaterScope _scope;
};

} } 

PM_MAKE_COMPONENT_ID(jam::Gun::Projectile);

}
