#pragma once

#include "jam/ShipEvents.hpp"
#include <ecs/Scene.hpp>
#include <method_traits.hpp>

namespace pm { namespace jam {

namespace Gun {

class Controller
{
public:
	explicit Controller(
		ecs::UpdateSystem& updateSystem, 
		Ship::Events& events, 
		const Time::duration& cooldown
	);

	void setActive(bool active);

	template <typename CreateProjectile, CreateProjectile methodCreateProjectile>
	void setProjectileFactory(method_class_t<CreateProjectile>& factory);

private:
	void onUpdate(const Time::duration& dt);

	struct ProjectileFactory
	{
		using func_createProjectile_t = void(*)(void*);

		void* obj;
		func_createProjectile_t createProjectile;
	};

	ProjectileFactory _projectileFactory;
	Time::duration _cooldown;
	Time::duration _timer;
	bool _shooting;
	
	Ship::Events& _events;
	ecs::UpdateSystem::UpdaterScope _updateScope;
};

} 

}

PM_MAKE_COMPONENT_ID(jam::Gun::Controller);

}

#include "jam/Gun/Controller.inl"
