#pragma once

#include "jam/Gun/Controller.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

namespace Gun {

class KeyHandler
{
public:
	explicit KeyHandler(
		ecs::KeySystem& keySystem, 
		ecs::component_reference<Controller> controller, 
		os::key_t key
	);

private:
	void onKeyDown(os::key_t key);
	void onKeyUp(os::key_t key);

	ecs::component_reference<Controller> _controller;
	os::key_t _key;
	ecs::KeySystem::HandlerScope _scope;
};

}

}

PM_MAKE_COMPONENT_ID(jam::Gun::KeyHandler);

}
