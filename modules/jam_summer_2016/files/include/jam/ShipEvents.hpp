#pragma once

#include <physics2d/types.hpp>
#include <ecs/ComponentId.hpp>
#include <Event.hpp>
#include <Time.hpp>

namespace pm { namespace jam { 

namespace Ship {

struct HpChanged 
{
	physics2d::real_t before; 
	physics2d::real_t after;
	physics2d::real_t max;
};

struct ShieldChanged
{
	physics2d::real_t before;
	physics2d::real_t after;
	physics2d::real_t max;
};

struct WeaponCharge
{
	Time::duration before;
	Time::duration after;
	Time::duration max;
};

typedef WeakEventManager<HpChanged, ShieldChanged, WeaponCharge> Events;

}

} 

PM_MAKE_COMPONENT_ID(jam::Ship::Events);

}
