#pragma once

#include <physics2d/Body.hpp>
#include <ecs/Transform.hpp>
#include <Time.hpp>
#include <functional>

namespace pm { namespace jam {

struct HullData
{
	id_t bodyId;
	id_t meshId;
	physics2d::real_t hp;
};

struct ShieldData
{
	physics2d::real_t energy;
	physics2d::real_t energyRegeneration;
	physics2d::real_t radius;
	physics2d::real_t elasticity;
};

struct EngineData
{
	ecs::Vector2_r force;
	ecs::Vector2_r pos;
	gr::AABox2 particleSpawnArea;
};

struct GunData
{
	enum Type
	{
		GUN_BOUNCY_PROJECTILE
	};

	id_t bodyId;
	id_t meshId;
	ecs::Vector2_r pos;
	ecs::Vector2_r impulse;
	Time::duration cooldownPeriod;
	Time::duration lifetime;
	Type type;
};

struct ShipData
{
	enum EngineType
	{
		ENGINE_ROTATE_LEFT,
		ENGINE_ROTATE_RIGHT,
		ENGINE_STRAFE_LEFT,
		ENGINE_STRAFE_RIGHT,
		ENGINE_THRUST,
		ENGINE_ENUM_SIZE
	};

	HullData hull;
	ShieldData shield;
	std::vector<GunData> guns;
	std::array<EngineData, ENGINE_ENUM_SIZE> engines;

	static const ShipData SHIP_TYPE_1;
	static const ShipData SHIP_TYPE_2;
};

typedef std::reference_wrapper<const ShipData> ship_data_ref;

} }
