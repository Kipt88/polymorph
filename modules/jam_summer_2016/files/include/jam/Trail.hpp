#pragma once

#include <ecs/Scene.hpp>
#include <ecs/Transform.hpp>
#include <gr/Color.hpp>

namespace pm { namespace jam {

class Trail
{
public:
	explicit Trail(
		ecs::entity_handle_t trailOwner,
		ecs::component_reference<const ecs::Transform2> transform,
		gr::MeshHandle mesh
	);

private:
	void update(const Time::duration& dt);

	ecs::entity_handle_t _trailOwner;
	ecs::component_reference<const ecs::Transform2> _transform;
	gr::MeshHandle _mesh;

	Time::duration _spawnTimer;

	ecs::UpdateSystem::UpdaterScope _updateScope;
};

} 

PM_MAKE_COMPONENT_ID(jam::Trail);

}
