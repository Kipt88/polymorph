#pragma once

#include "jam/PhysicsBody.hpp"
#include "jam/ParticleEmitter.hpp"
#include <ecs/Systems.hpp>
#include <ecs/Scene.hpp>
#include <ecs/Transform.hpp>
#include <ecs/ComponentId.hpp>

namespace pm { namespace jam {

namespace Engine {

class Interface_const : public ecs::const_entity_handle_t
{
public:
	explicit Interface_const(ecs::const_entity_handle_t handle);

	ecs::component_reference<const ecs::Transform2> transform() const;
};

class Interface : public ecs::entity_handle_t
{
public:
	explicit Interface(ecs::entity_handle_t handle);

	ecs::component_reference<ecs::Transform2> transform() const;

	// TODO Remove
	void setKeyControlled(os::key_t forward, os::key_t backward) const;
	void removeKeyControl() const;

	void setGamepadControlled(unsigned int padId, unsigned int axis, const gr::Vector2_r& forwardDir) const;
	void removeGamepadControl() const;
	//

	void removeControl() const;

	void setPower(gr::real_t power) const;

	operator Interface_const() const;
};

Interface create(
	ecs::component_reference<PhysicsBody> body, 
	const ecs::Vector2_r& force,
	const ecs::Vector2_r& pos,
	const math::AxisAlignedBox2<gr::real_t>& particleSpawnArea,
	ecs::entity_handle_t handle
);

}

} }
