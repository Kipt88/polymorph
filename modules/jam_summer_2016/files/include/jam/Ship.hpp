#pragma once

#include "jam/SessionData.hpp"
#include "jam/Shield.hpp"
#include "jam/Hull.hpp"
#include "jam/ShipEvents.hpp"
#include "jam/Environment.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

namespace Ship {

class Interface_const : public ecs::const_entity_handle_t
{
public:
	explicit Interface_const(ecs::const_entity_handle_t handle);

	ecs::component_reference<const PhysicsBody> body() const;
	Shield::Interface_const shield() const;
	Hull::Interface_const hull() const;
	const Events& events() const;
	ecs::component_reference<const ecs::Transform2> transform() const;
	unsigned int getPlayer() const;
};

class Interface : public ecs::entity_handle_t
{
public:
	explicit Interface(ecs::entity_handle_t handle);

	ecs::component_reference<const PhysicsBody> body() const;
	Shield::Interface shield() const;
	Hull::Interface hull() const;
	const Events& events() const;
	ecs::component_reference<const ecs::Transform2> transform() const;
	unsigned int getPlayer() const;

	void forward(gr::real_t power) const;
	void strafe(gr::real_t power) const;
	void rotate(gr::real_t power) const;
	void shoot(bool active) const;

	void destroyShip() const;

	operator Interface_const() const;
};

Interface create(
	ecs::entity_handle_t handle, 
	ecs::entity_handle_t world, 
	const ecs::LocalTransform2& transform,
	Environment::Interface environment,
	const PlayerData& playerData
);

}

} }
