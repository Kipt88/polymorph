#pragma once

#include "jam/Environment.hpp"
#include <ecs/Transform.hpp>
#include <ecs/Scene.hpp>
#include <random>

namespace pm { namespace jam {

class AsteroidSpawner
{
public:
	explicit AsteroidSpawner(
		ecs::entity_handle_t world, 
		unsigned int asteroidCount,
		const std::vector<gr::Vector2_r>& safeLocations
	);

private:
	void spawnAsteroid();
	bool closeToAsteroid(const ecs::Vector2_r& pos) const;

	std::random_device _randomDevice;
	std::mt19937 _randomGen;

	std::vector<gr::Vector2_r> _safeLocations;

	ecs::entity_handle_t _asteroids;
};

}

PM_MAKE_COMPONENT_ID(jam::AsteroidSpawner);

}
