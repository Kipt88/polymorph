#pragma once

#include <ecs/RenderSystem.hpp>
#include <gr/Color.hpp>

namespace pm { namespace jam {

namespace RenderingData {

// Camera rendering masks:
constexpr ecs::RenderSystem::render_category_t P1_OBJECT = 0x1;
constexpr ecs::RenderSystem::render_category_t P2_OBJECT = 0x2;
constexpr ecs::RenderSystem::render_category_t GAME_OBJECT_TYPE = P1_OBJECT | P2_OBJECT;
constexpr ecs::RenderSystem::render_category_t UI_TYPE = 0x10;

// Camera draw-order:
namespace Order
{
	constexpr int PLAYER_1 = 1;
	constexpr int PLAYER_2 = 2;
	constexpr int PLAYER_3 = 3;
	constexpr int PLAYER_4 = 4;
	constexpr int UI = 5;
}

// Visible scene height:
constexpr gr::real_t CAMERA_VISIBLE_HEIGHT = 300.0_r;

namespace UI
{
	namespace Shield
	{
		constexpr gr::real_t RELATIVE_WIDTH = 0.4_r;
		constexpr gr::real_t RELATIVE_HEIGHT = 0.01_r;
		constexpr gr::real_t RELATIVE_Y = -0.4_r;
		constexpr gr::Color COLOR = gr::BLUE;
	}

	namespace Hull
	{
		constexpr gr::real_t RELATIVE_WIDTH = 0.4_r;
		constexpr gr::real_t RELATIVE_HEIGHT = 0.01_r;
		constexpr gr::real_t RELATIVE_Y = -0.425_r;
		constexpr gr::Color COLOR = gr::GREEN;
	}

	namespace Gun
	{
		constexpr gr::real_t RELATIVE_WIDTH = 0.4_r;
		constexpr gr::real_t RELATIVE_HEIGHT = 0.01_r;
		constexpr gr::real_t RELATIVE_Y = -0.375_r;
		constexpr gr::Color CHARGE_COLOR = gr::GRAY;
		constexpr gr::Color COLOR = gr::WHITE;
	}
}

}

} }
