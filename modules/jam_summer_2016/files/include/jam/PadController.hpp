#pragma once

#include "jam/PadControls.hpp"
#include "jam/Ship.hpp"

namespace pm { namespace jam {

class PadController
{
public:
	explicit PadController(const PadControls& controls, Ship::Interface ship);

private:
	void onButtonDown(os::GamepadButton button);
	void onButtonUp(os::GamepadButton button);
	void onAxis(unsigned int axis, const gr::Vector2_r& newPos, const gr::Vector2_r& oldPos);

	PadControls _controls;
	Ship::Interface _ship;

	ecs::GamepadSystem::HandlerScope _scope;
};

}

PM_MAKE_COMPONENT_ID(jam::PadController);

}
