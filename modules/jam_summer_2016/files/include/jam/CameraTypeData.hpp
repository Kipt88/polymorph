#pragma once

#include <ecs/Scene.hpp>
#include <ecs/Transform.hpp>

namespace pm { namespace jam {

enum class CameraType
{
	FOLLOW,
	FIXED_ROTATION,
	DAMPEN
};

struct CameraTypeData
{
	CameraType type;

	ecs::component_reference<const ecs::Transform2> attachController(
		ecs::component_reference<const ecs::Transform2> input,
		ecs::entity_handle_t handle
	) const;
};

} }
