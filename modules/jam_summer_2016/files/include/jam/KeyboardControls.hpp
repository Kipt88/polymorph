#pragma once

#include "jam/ShipData.hpp"
#include <os/Keys.hpp>

namespace pm { namespace jam {

struct KeyboardControls
{
	os::key_t left;
	os::key_t right;
	os::key_t forward;
	os::key_t backward;
	os::key_t leftStrafe;
	os::key_t rightStrafe;
	os::key_t shoot;

	os::key_t getThrustKeyForEngine(ShipData::EngineType engine) const;
	os::key_t getReverseKeyForEngine(ShipData::EngineType engine) const;

	static const KeyboardControls LEFT_WASD;
	static const KeyboardControls RIGHT_NUMPAD;
};

} }
