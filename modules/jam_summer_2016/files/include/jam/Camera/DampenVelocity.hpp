#pragma once

#include <ecs/UpdateSystem.hpp>
#include <ecs/Transform.hpp>
#include <ecs/ComponentId.hpp>

namespace pm { namespace jam { namespace Camera {

class DampenVelocity
{
public:
	explicit DampenVelocity(
		ecs::UpdateSystem& updateSystem,
		ecs::component_reference<const ecs::Transform2> input,
		ecs::component_reference<ecs::Transform2> output,
		gr::real_t maxVelocity = 200.0_r,
		gr::real_t maxAngularVelocity = math::PI
	);

private:
	void onUpdate(const Time::duration& dt);

	ecs::component_reference<const ecs::Transform2> _input;
	ecs::component_reference<ecs::Transform2> _output;
	
	gr::real_t _maxVelocity;
	gr::real_t _maxAngularVelocity;

	ecs::LocalTransform2 _lastTransform;

	ecs::UpdateSystem::UpdaterScope _scope;
};

} } 

PM_MAKE_COMPONENT_ID(jam::Camera::DampenVelocity);

}
