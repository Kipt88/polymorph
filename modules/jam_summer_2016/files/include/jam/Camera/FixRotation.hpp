#pragma once

#include <ecs/UpdateSystem.hpp>
#include <ecs/Transform.hpp>
#include <ecs/ComponentId.hpp>

namespace pm { namespace jam { namespace Camera {

class FixRotation
{
public:
	explicit FixRotation(
		ecs::UpdateSystem& updateSystem, 
		ecs::component_reference<const ecs::Transform2> input,
		ecs::component_reference<ecs::Transform2> output,
		gr::real_t targetRotation = 0.0_r
	);

private:
	void onUpdate(const Time::duration&);

	ecs::component_reference<const ecs::Transform2> _input;
	ecs::component_reference<ecs::Transform2> _output;
	gr::real_t _targetRotation;
	ecs::UpdateSystem::UpdaterScope _scope;
};

} } 

PM_MAKE_COMPONENT_ID(jam::Camera::FixRotation);

}
