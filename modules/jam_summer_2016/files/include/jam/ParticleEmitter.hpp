#pragma once

#include <ecs/Scene.hpp>
#include <ecs/Systems.hpp>
#include <gr/BufferProvider.hpp>
#include <gr/Color.hpp>
#include <gr/VertexList.hpp>
#include <random>

namespace pm { namespace jam {

template <typename T>
struct ParticleDistribution
{
	T min;
	T max;
};

struct ParticleData
{
	unsigned int maxParticles;
	
	gr::Color color;
	ecs::RenderSystem::render_category_t renderCategory;
	int renderOrder;

	ParticleDistribution<gr::real_t> angle;
	ParticleDistribution<gr::real_t> speed;
	ParticleDistribution<gr::real_t> acceleration;
	ParticleDistribution<Time::duration> lifetime;
	ParticleDistribution<unsigned int> clusterSize;

	math::AxisAlignedBox2<gr::real_t> spawnArea;

	Time::duration spawnFrequency;
};

class ParticleEmitter
{
public:
	explicit ParticleEmitter(
		ecs::Systems& systems, 
		const ParticleData& data,
		ecs::component_reference<const ecs::Transform2> transform
	);

	void start();
	void stop();

private:
	void update(const Time::duration& dt);

	void render(gr::ClientFrame& frame) const;
	ecs::RenderSystem::render_category_t renderType() const;

	struct LifeTimeData
	{
		ecs::Vector2_r velocity;
		ecs::Vector2_r acceleration;

		Time::duration timer;
		Time::duration lifetime;
	};

	bool _emitting;

	ParticleData _def;
	Time::duration _spawnTimer;

	std::vector<LifeTimeData> _particleData;
	gr::VertexList<gr::VertexFormat::V2_C4> _vertices;
	gr::MeshHandle _mesh;

	ecs::component_reference<const ecs::Transform2> _transform;

	std::random_device _dev;
	std::mt19937 _gen;

	ecs::RenderSystem::RenderableScope _renderScope;
	ecs::UpdateSystem::UpdaterScope _updateScope;
};

} 

PM_MAKE_COMPONENT_ID(jam::ParticleEmitter);

}