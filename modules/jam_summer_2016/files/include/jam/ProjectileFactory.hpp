#pragma once

#include "jam/PhysicsBody.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

class ProjectileFactory
{
public:
	explicit ProjectileFactory(
		ecs::entity_handle_t projectileParent,
		ecs::component_reference<const PhysicsBody> shipBody
	);

	void create();

	void setMesh(gr::MeshHandle mesh);
	void setBodyDef(const physics2d::BodyDefinition& bodyDef);
	void setLocalTransform(const ecs::LocalTransform2& transform);
	void setImpulse(const ecs::Vector2_r& impulse);
	void setTimeout(const Time::duration& timeout);

private:
	ecs::entity_handle_t _projectileParent;
	ecs::component_reference<const PhysicsBody> _shipBody;
	gr::MeshHandle _mesh;
	physics2d::BodyDefinition _bodyDef;
	ecs::LocalTransform2 _transform;
	ecs::Vector2_r _impulse;

	Time::duration _timeout;
};

} 

PM_MAKE_COMPONENT_ID(jam::ProjectileFactory);

}