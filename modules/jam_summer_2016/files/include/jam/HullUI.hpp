#pragma once

#include "jam/Ship.hpp"
#include "jam/Environment.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

namespace HullUI {

void create(Environment::Interface environment, Ship::Interface_const ship, ecs::entity_handle_t handle);

}

} }
