#pragma once

#include <ecs/Scene.hpp>

namespace pm { namespace jam {

namespace UISlider {

class Interface_const : public ecs::const_entity_handle_t
{
public:
	explicit Interface_const(ecs::const_entity_handle_t handle);

	ecs::component_reference<const ecs::Transform2> transform() const;

	gr::real_t getWidth() const;
	gr::real_t getHeight() const;
	gr::real_t getValue() const;
	const gr::Color& getColor() const;
	const gr::Vector2_r& getPivot() const;
	ecs::RenderSystem::render_category_t getRenderCategory() const;
};

class Interface : public ecs::entity_handle_t
{
public:
	explicit Interface(ecs::entity_handle_t handle);

	ecs::component_reference<ecs::Transform2> transform() const;

	gr::real_t getWidth() const;
	gr::real_t getHeight() const;
	gr::real_t getValue() const;
	const gr::Color& getColor() const;
	const gr::Vector2_r& getPivot() const;
	ecs::RenderSystem::render_category_t getRenderCategory() const;

	void setWidth(gr::real_t w) const;
	void setHeight(gr::real_t h) const;
	void setValue(gr::real_t r) const;
	void setColor(const gr::Color& color) const;
	void setPivot(const gr::Vector2_r& pivot) const;
	void setRenderCategory(ecs::RenderSystem::render_category_t category) const;

	operator Interface_const() const;
};

Interface create(ecs::entity_handle_t handle);

}

} }
