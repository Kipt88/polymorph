#pragma once

#include "jam/Ship.hpp"
#include "jam/LevelData.hpp"

namespace pm { namespace jam {

namespace AIShipController {

void attachController(
	level_data_ref levelData, 
	Ship::Interface ship, 
	const std::vector<Ship::Interface_const>& otherShips
);

}

} }
