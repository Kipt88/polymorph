#pragma once

#include "jam/ShipData.hpp"
#include "jam/CameraTypeData.hpp"
#include "jam/LevelData.hpp"
#include "jam/PadControls.hpp"
#include "jam/KeyboardControls.hpp"
#include <optional.hpp>
#include <functional>

namespace pm { namespace jam {

struct PlayerData
{
	unsigned int playerIndex;

	std::optional<PadControls> padControl;
	std::optional<KeyboardControls> keyControl;

	CameraTypeData camera;

	ship_data_ref ship;

	bool isAIControlled() const
	{
		return !(padControl || keyControl);
	}
};

struct SessionData
{
	const LevelData* level;
	std::vector<PlayerData> players;
};

} }
