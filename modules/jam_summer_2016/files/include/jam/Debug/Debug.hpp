#pragma once

#include <ecs/Scene.hpp>
#include <gr/Color.hpp>

namespace pm { namespace jam { namespace Debug {

class Debug
{
public:
	struct LineDef
	{
		gr::Vector2_r a;
		gr::Vector2_r b;
	};

	explicit Debug(ecs::Systems& systems);

	void drawLines(
		void* handle,
		const std::vector<LineDef>& lines, 
		const gr::Color& color = gr::WHITE
	);

	void remove(void* handle);
	void clear();

private:
	void render(gr::ClientFrame& frame) const;
	ecs::RenderSystem::render_category_t renderCategory() const;

	std::vector<std::pair<void*, gr::MeshHandle>> _meshes;
	std::vector<gr::MeshHandle> _freeMeshes;

	ecs::Systems& _systems;
	ecs::RenderSystem::RenderableScope _renderScope;
};

Debug& get();
void create(ecs::entity_handle_t handle);

} } 

PM_MAKE_COMPONENT_ID(jam::Debug::Debug);

}
