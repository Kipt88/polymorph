#pragma once

#include "jam/ShipData.hpp"
#include <os/Gamepad.hpp>
#include <gr/types.hpp>

namespace pm { namespace jam {

struct PadControls
{
	struct Axis
	{
		unsigned int axis;
		gr::Vector2_r forward;
	};

	unsigned int controller;

	os::GamepadButton shoot;

	Axis left;
	Axis right;
	Axis forward;
	Axis backward;
	Axis leftStrafe;
	Axis rightStrafe;

	Axis getAxisForEngine(ShipData::EngineType engine) const;

	static const PadControls DEFAULT;
};

} }
