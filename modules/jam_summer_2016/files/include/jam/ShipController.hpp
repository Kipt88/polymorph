#pragma once

#include "jam/PhysicsBody.hpp"
#include "jam/Shield.hpp"
#include "jam/Hull.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

class ShipController : Unique
{
public:
	explicit ShipController(
		ecs::Systems& systems, 
		ecs::component_reference<PhysicsBody> body, 
		Hull::Interface hull,
		Shield::Interface shield,
		unsigned int player
	);

	unsigned int getPlayer() const;

private:
	void onCollision(const physics2d::CollisionData& data);

	ecs::component_reference<PhysicsBody> _body;
	Hull::Interface _hull;
	Shield::Interface _shield;

	unsigned int _player;
};

} 

PM_MAKE_COMPONENT_ID(jam::ShipController);

}
