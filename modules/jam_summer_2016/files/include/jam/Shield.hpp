#pragma once

#include "jam/ShipEvents.hpp"
#include "jam/Environment.hpp"
#include "jam/PhysicsBody.hpp"
#include "jam/ShipData.hpp"
#include <ecs/Scene.hpp>
#include <physics2d/Shape.hpp>
#include <physics2d/types.hpp>
#include <Event.hpp>

namespace pm { namespace jam {

namespace Shield {

class Interface_const : public ecs::const_entity_handle_t
{
public:
	explicit Interface_const(ecs::const_entity_handle_t handle);

	physics2d::real_t energy() const;
	physics2d::real_t maxEnergy() const;
};

class Interface : public ecs::entity_handle_t
{
public:
	explicit Interface(ecs::entity_handle_t handle);

	physics2d::real_t energy() const;
	physics2d::real_t maxEnergy() const;

	void inflictDamage(physics2d::real_t energy) const;

	operator Interface_const() const;
};

Interface create(
	Environment::Interface_const environment,
	ecs::component_reference<Ship::Events> events,
	ecs::component_reference<const ecs::Transform2> transform,
	ecs::component_reference<PhysicsBody> body,
	unsigned int playerIndex,
	const ShieldData& def,
	ecs::entity_handle_t handle
);

}

} }
