#pragma once

#include "jam/Environment.hpp"
#include "jam/GameData.hpp"
#include "jam/PhysicsBody.hpp"
#include <ecs/Scene.hpp>
#include <ecs/Transform.hpp>

namespace pm { namespace jam {

class AsteroidSubDivider
{
public:
	explicit AsteroidSubDivider(
		ecs::entity_handle_t world,
		ecs::entity_handle_t entity,
		const GameData::AsteroidData& asteroidDef,
		ecs::component_reference<ecs::Transform2> transform,
		ecs::component_reference<PhysicsBody> body
	);

private:
	void onCollision(const physics2d::CollisionData& data);
	void subDivideEntity(const Time::duration&);

	void addParticles();

	ecs::entity_handle_t _world;
	ecs::entity_handle_t _entity;
	GameData::AsteroidData _asteroidDef;
	ecs::component_reference<PhysicsBody> _body;
	ecs::component_reference<ecs::Transform2> _transform;

	ecs::UpdateSystem::UpdaterScope _scope;
};

} 

PM_MAKE_COMPONENT_ID(jam::AsteroidSubDivider);

}