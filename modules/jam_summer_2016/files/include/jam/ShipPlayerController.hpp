#pragma once

#include "jam/SessionData.hpp"
#include "jam/Ship.hpp"

namespace pm { namespace jam {

namespace ShipPlayerController {

void attachController(const PlayerData& player, Ship::Interface ship);

}

} }
