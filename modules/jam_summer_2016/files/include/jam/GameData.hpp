#pragma once

#include <gr/types.hpp>
#include <Time.hpp>
#include <filesystem.hpp>
#include <Id.hpp>
#include <vector>

namespace pm { namespace jam {

namespace GameData {

struct BodyMesh
{
	id_t body;
	id_t mesh;
};

struct AsteroidData
{
	std::vector<BodyMesh> parts;
	
	id_t body;
	id_t mesh;
};

constexpr gr::real_t ASTEROID_SPAWN_BOX_SIZE = 500.0_r;
constexpr gr::real_t ASTEROID_SHIP_SPAWN_BOX_SIZE_MARGIN = 50.0_r;
constexpr gr::real_t ASTEROID_ASTEROID_SPAWN_BOX_SIZE_MARGIN = 50.0_r;
constexpr unsigned int ASTEROID_COUNT = 40;

extern const std::vector<AsteroidData> ASTEROIDS;

constexpr Time::duration RESET_DURATION = 2s;

}

} }
