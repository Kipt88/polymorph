#pragma once

#include <ecs/PhysicsSystem.hpp>
#include <ecs/ComponentId.hpp>
#include <method_traits.hpp>

namespace pm { namespace jam {

class PhysicsBody
{
public:
	explicit PhysicsBody(
		ecs::PhysicsSystem& system, 
		physics2d::BodyDefinitionHandle def,
		ecs::component_reference<ecs::Transform2> targetTransform
	);

	explicit PhysicsBody(
		ecs::PhysicsSystem& system,
		const physics2d::BodyDefinition& def,
		ecs::component_reference<ecs::Transform2> targetTransform
	);

	void setPosition(const ecs::Vector2_r& pos);
	void setRotation(gr::real_t angle);
	void setVelocity(const ecs::Vector2_r& v);
	void applyForce(const ecs::Vector2_r& force, const ecs::Vector2_r& localPos = ecs::Vector2_r::ZERO);
	void applyImpulse(const ecs::Vector2_r& impulse, const ecs::Vector2_r& localPos = ecs::Vector2_r::ZERO);

	ecs::Vector2_r getVelocity() const;
	gr::real_t getAngularVelocity() const;
	gr::real_t getMass() const;

	template <typename Action>
	void withBody(Action&& action);

	template <typename OnCollision, OnCollision>
	void setCollisionHandler(method_class_t<OnCollision>& handler);

	const ecs::Transform2& transform() const;

private:
	struct CollisionHandler
	{
		using func_onCollision = void(*)(void*, const physics2d::CollisionData&);

		void* obj;
		func_onCollision onCollision;
	};

	void updateState(const physics2d::BodyState& newState);
	void onCollision(const physics2d::CollisionData& data);

	CollisionHandler _collisionHandler;
	ecs::component_reference<ecs::Transform2> _targetTransform;
	physics2d::BodyState _lastState;
	ecs::PhysicsSystem::DelegateScope _scope;
};

}

PM_MAKE_COMPONENT_ID(jam::PhysicsBody);

}

#include "jam/PhysicsBody.inl"