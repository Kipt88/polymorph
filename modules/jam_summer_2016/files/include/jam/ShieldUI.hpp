#pragma once

#include "jam/Ship.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

namespace ShieldUI {

void create(
	Environment::Interface_const environment,
	Ship::Interface_const ship, 
	ecs::entity_handle_t handle
);

}

} }