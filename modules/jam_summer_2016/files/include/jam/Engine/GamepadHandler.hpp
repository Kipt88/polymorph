#pragma once

#include "jam/Engine/Controller.hpp"

namespace pm { namespace jam {

namespace Engine {

class GamepadHandler
{
public:
	explicit GamepadHandler(
		ecs::GamepadSystem& gamepadSystem,
		ecs::component_reference<Controller> controller,
		unsigned int padId,
		unsigned int axis,
		const gr::Vector2_r& positiveDirection
	);

private:
	void onAxis(unsigned int axis, const gr::Vector2_r& newPos, const gr::Vector2_r& oldPos);

	unsigned int _axis;

	Controller& _engineController;
	gr::Vector2_r _positiveDirection;
	ecs::GamepadSystem::HandlerScope _scope;
};

}

}

PM_MAKE_COMPONENT_ID(jam::Engine::GamepadHandler);

}
