#pragma once

#include "jam/Engine/Controller.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

namespace Engine {

class KeyHandler
{
public:
	explicit KeyHandler(
		ecs::KeySystem& keySystem,
		ecs::component_reference<Controller> controller,
		os::key_t forwardKey,
		os::key_t backwardKey
	);

private:
	void onKeyDown(os::key_t k);
	void onKeyUp(os::key_t k);

	void updateEngine();

	ecs::component_reference<Controller> _controller;
	os::key_t _forwardKey;
	os::key_t _backwardKey;

	bool _forwardDown;
	bool _backwardDown;

	ecs::KeySystem::HandlerScope _scope;
};

}

} 

PM_MAKE_COMPONENT_ID(jam::Engine::KeyHandler);

}