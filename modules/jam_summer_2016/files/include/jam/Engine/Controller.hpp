#pragma once

#include "jam/PhysicsBody.hpp"
#include "jam/ParticleEmitter.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

namespace Engine {

class Controller
{
public:
	explicit Controller(
		ecs::UpdateSystem& updateSystem,
		ecs::component_reference<PhysicsBody> body,
		ecs::component_reference<ParticleEmitter> forwardParticles,
		ecs::component_reference<ParticleEmitter> backwardParticles,
		const ecs::Vector2_r& force,
		const ecs::Vector2_r& pos
	);

	void setPower(gr::real_t power);
	void stop();

private:
	void update(const Time::duration&);

	ecs::component_reference<PhysicsBody> _body;
	ecs::component_reference<ParticleEmitter> _forwardParticles;
	ecs::component_reference<ParticleEmitter> _backwardParticles;

	ecs::Vector2_r _force;
	ecs::Vector2_r _pos;

	gr::real_t _power;

	ecs::UpdateSystem::UpdaterScope _scope;
};

}

} 

PM_MAKE_COMPONENT_ID(jam::Engine::Controller);

}
