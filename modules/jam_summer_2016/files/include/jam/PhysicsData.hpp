#include <physics2d/types.hpp>

namespace pm { namespace jam {

namespace PhysicsData {

enum class Category : physics2d::collision_type_t
{
	DYNAMIC_WORLD,
	STATIC_WORLD,
	P1_SHIP,
	P1_PROJECTILE,
	P2_SHIP,
	P2_PROJECTILE, 
	P3_SHIP,
	P3_PROJECTILE,
	P4_SHIP,
	P4_PROJECTILE
};

constexpr physics2d::collision_type_t shipCategory(unsigned int index)
{
	return static_cast<physics2d::collision_type_t>(
		index == 0 ? Category::P1_SHIP
		: index == 1 ? Category::P2_SHIP
		: index == 2 ? Category::P3_SHIP
		: Category::P4_SHIP
	);
}

}

} }
