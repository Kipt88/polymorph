#pragma once

#include <ecs/Scene.hpp>
#include <ecs/Systems.hpp>

namespace pm { namespace jam {

class DestroyOnTimer
{
public:
	explicit DestroyOnTimer(ecs::entity_handle_t entity, const Time::duration& timer);

private:
	void update(const Time::duration& dt);

	ecs::entity_handle_t _entity;
	Time::duration _timer;

	ecs::UpdateSystem::UpdaterScope _scope;
};

} 

PM_MAKE_COMPONENT_ID(jam::DestroyOnTimer);

}