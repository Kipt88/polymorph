#pragma once

#include "jam/KeyboardControls.hpp"
#include "jam/Ship.hpp"

namespace pm { namespace jam {

class KeyboardController
{
public:
	explicit KeyboardController(const KeyboardControls& controls, Ship::Interface ship);

private:
	void onKeyDown(os::key_t key);
	void onKeyUp(os::key_t key);

	KeyboardControls _controls;
	Ship::Interface _ship;

	int _forwardDown;
	int _backwardDown;
	int _leftDown;
	int _rightDown;
	int _strafeLeftDown;
	int _strafeRightDown;

	ecs::KeySystem::HandlerScope _scope;
};

} 

PM_MAKE_COMPONENT_ID(jam::KeyboardController);

}
