#pragma once

#include "jam/ShipEvents.hpp"
#include "jam/ShipData.hpp"
#include "jam/PhysicsBody.hpp"
#include <ecs/Scene.hpp>

namespace pm { namespace jam {

namespace Gun {

class Interface_const : public ecs::const_entity_handle_t
{
public:
	explicit Interface_const(ecs::const_entity_handle_t handle);
};

class Interface : public ecs::entity_handle_t
{
public:
	explicit Interface(ecs::entity_handle_t handle);

	//
	void setKeyControlled(os::key_t key) const;
	void removeKeyControl() const;

	void setGamepadControlled(unsigned int padId, os::GamepadButton button) const;
	void removeGamepadControl() const;
	//

	void removeControl() const;

	void setActive(bool active) const;

	operator Interface_const() const;
};

Interface create(
	ecs::entity_handle_t world, 
	Ship::Events& events, 
	unsigned int playerIndex,
	const GunData& gunData, 
	ecs::component_reference<const PhysicsBody> body,
	ecs::entity_handle_t handle
);

}

} }

#include "jam/Gun.inl"
