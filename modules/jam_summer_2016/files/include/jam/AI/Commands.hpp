#pragma once

#include "jam/Ship.hpp"
#include <gr/types.hpp>
#include <optional.hpp>

namespace pm { namespace jam { namespace AI {

struct Commands
{
	std::optional<gr::real_t> thrust;
	std::optional<gr::real_t> strafe;
	std::optional<gr::real_t> rotate;
	std::optional<bool> shoot;

	Commands& add(const Commands& other);

	void apply(Ship::Interface ship) const;
};

} } }
