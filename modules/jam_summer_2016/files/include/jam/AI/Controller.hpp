#pragma once

#include "jam/AI/ShipInterface.hpp"
#include "jam/Ship.hpp"
#include "jam/LevelData.hpp"
#include <ecs/Scene.hpp>
#include <ai/NavigationMesh.hpp>

namespace pm { namespace jam { namespace AI {

class Controller
{
public:
	explicit Controller(
		level_data_ref level,
		Ship::Interface ship, 
		const std::vector<Ship::Interface_const>& allShips
	);

	template <typename State>
	void setState(State&& state)
	{
		_nextState = std::make_unique<Derived<State>>(std::forward<State>(state));
	}

	const ShipInterface& ships() const;
	const LevelData& level() const;
	const ai::NavigationMesh& navmesh() const;

private:
	void update(const Time::duration& dt);

	class Base
	{
	public:
		virtual ~Base() = default;

		virtual void update(const Time::duration& dt) = 0;
	};

	template <typename State>
	class Derived final : public Base
	{
	public:
		explicit Derived(State&& state) : _state(std::forward<State>(state)) {}

		void update(const Time::duration& dt) override
		{
			_state.update(dt);
		}

	private:
		std::decay_t<State> _state;
	};

	ShipInterface _interface;
	level_data_ref _level;
	ai::NavigationMesh _navmesh;

	std::unique_ptr<Base> _state;
	std::unique_ptr<Base> _nextState;

	ecs::UpdateSystem::UpdaterScope _scope;
};

} } 

PM_MAKE_COMPONENT_ID(jam::AI::Controller);

}
