#pragma once

#include "jam/Ship.hpp"
#include <vector>

namespace pm { namespace jam { namespace AI {

class ShipInterface
{
public:
	explicit ShipInterface(Ship::Interface ship, const std::vector<Ship::Interface_const>& allShips);

	Ship::Interface ship;
	std::vector<Ship::Interface_const> otherShips;
};

} } }
