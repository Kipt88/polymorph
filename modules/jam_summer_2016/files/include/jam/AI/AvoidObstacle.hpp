#pragma once

#include "jam/AI/Commands.hpp"
#include "jam/Ship.hpp"

namespace pm { namespace jam { namespace AI {

class AvoidObstacle
{
public:
	explicit AvoidObstacle(const Time::duration& warningTime = 1s);

	std::optional<ecs::Vector2_r> evasionPoint(const Ship::Interface_const& ship);

private:
	Time::duration _warningTime;
};

} } }
