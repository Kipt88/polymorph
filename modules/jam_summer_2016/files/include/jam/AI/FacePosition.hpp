#pragma once

#include "jam/AI/Rotate.hpp"

namespace pm { namespace jam { namespace AI {

struct FacePositionSettings
{
	gr::real_t angleOffset;
};

class FacePosition
{
public:
	explicit FacePosition(const FacePositionSettings& settings = { 0.0_r });

	void setTargetPosition(const ecs::Vector2_r& pos);

	bool isFacingPosition(Ship::Interface_const ship) const;
	Commands update(Ship::Interface_const ship);

private:
	FacePositionSettings _settings;
	ecs::Vector2_r _targetPos;
	Rotate _rotate;
};

} } }
