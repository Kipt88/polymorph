#pragma once

#include "jam/AI/Commands.hpp"
#include "jam/Ship.hpp"
#include <gr/types.hpp>
#include <Time.hpp>

namespace pm { namespace jam { namespace AI {


class Move
{
public:
	explicit Move(gr::real_t range = 13.0_r);

	void setTargetPosition(const ecs::Vector2_r& position);

	bool inAcceptiblePosition(Ship::Interface_const ship) const;
	Commands update(Ship::Interface_const ship);

private:
	ecs::Vector2_r _targetPosition;
	gr::real_t _range;
};

} } }
