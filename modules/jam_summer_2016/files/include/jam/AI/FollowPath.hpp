#pragma once

#include "jam/AI/Move.hpp"
#include "jam/AI/FacePosition.hpp"
#include "jam/AI/AvoidObstacle.hpp"

#include <ai/NavigationMesh.hpp>
#include <ecs/Transform.hpp>
#include <vector>

namespace pm { namespace jam { namespace AI {

class FollowPath
{
public:
	explicit FollowPath(const ai::NavigationMesh::path_t& path);
	FollowPath() = default;

	void setPath(const ai::NavigationMesh::path_t& path);
	bool hasPath() const;

	ecs::Vector2_r nextWaypoint(const Ship::Interface_const& ship) const;
	ecs::Vector2_r endTarget() const;

	Commands update(const Ship::Interface_const& ship);

private:
	std::optional<ai::NavigationMesh::path_t> _currentPath;

	Move _mover;
	FacePosition _facer;
	AvoidObstacle _avoider;
};

} } }
