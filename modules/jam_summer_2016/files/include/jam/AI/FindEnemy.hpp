#pragma once

#include "jam/AI/Controller.hpp"
#include "jam/AI/FollowPath.hpp"
#include "jam/RenderingData.hpp"

namespace pm { namespace jam { namespace AI {

struct FindEnemySettings
{
	gr::real_t visibleRadius; // Move to ShipInterface...
};

class FindEnemy
{
public:
	explicit FindEnemy(
		Controller& controller, 
		const FindEnemySettings& settings = { RenderingData::CAMERA_VISIBLE_HEIGHT }
	);

	void update(const Time::duration& dt);

private:
	std::optional<Ship::Interface_const> closeByEnemy() const;
	ecs::Vector2_r randomPointOfInterest() const;
	bool unobstructedPath(const ecs::Vector2_r& from, const ecs::Vector2_r& to) const;

	std::reference_wrapper<Controller> _controller;
	FindEnemySettings _settings;

	FollowPath _pathFollower;
};

} } }
