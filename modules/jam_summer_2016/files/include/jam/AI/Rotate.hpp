#pragma once

#include "jam/AI/Commands.hpp"
#include "jam/Ship.hpp"
#include <gr/types.hpp>

namespace pm { namespace jam { namespace AI {

struct RotateSettings
{
	// Range of rotation deemed acceptible to stop in.
	gr::real_t targetRotationRange;
	
	// Range factor to determine closeness.
	gr::real_t safetyRangeFactor;
	
	// Power factor used when close.
	gr::real_t safetyPowerFactor;  

	// Acceptible period to wait instead of trying to power engines to reach rotation.
	Time::duration acceptibleWait;
};

class Rotate
{
public:
	explicit Rotate(
		const RotateSettings& settings = {
			math::PI * 0.0125_r,
			2.0_r,
			0.125_r,
			500ms
		}
	);

	void setTargetRotation(gr::real_t targetRotation);

	bool inAcceptibleRotation(Ship::Interface_const ship) const;
	Commands update(Ship::Interface_const ship);

private:
	gr::real_t _targetRotation;
	RotateSettings _settings;
};

} } }
