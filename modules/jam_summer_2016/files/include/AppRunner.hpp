#pragma once

#include <ecs/Game.hpp>

namespace pm {

typedef ecs::Game AppRunner;

void startupMain(AppRunner& app);

}
