#pragma once

#include <filesystem.hpp>

namespace pm { namespace filesystem {

typedef std::filesystem::path path;

namespace Paths {

path executablePath();

}

} }
