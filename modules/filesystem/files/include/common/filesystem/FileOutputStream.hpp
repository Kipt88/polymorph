#pragma once

#include "filesystem/FileException.hpp"

#include <io/IOStream.hpp>
#include <filesystem.hpp>
#include <string>

namespace pm { namespace filesystem {

class FileOutputStream : public io::OutputStream
{
public:
	FileOutputStream(const std::filesystem::path& path, bool textFile = false);
	~FileOutputStream();

	void write(const void* data,
			   std::size_t typeAlignment,
			   std::size_t size,
			   std::size_t count);

private:
	std::FILE* _fp;
};

} }