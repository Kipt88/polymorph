#include "filesystem/Paths.hpp"

#include <Windows.h>

namespace pm { namespace filesystem {

namespace Paths {

path executablePath()
{
	HMODULE hModule = GetModuleHandle(NULL);
	CHAR p[MAX_PATH];
	GetModuleFileName(hModule, p, MAX_PATH);

	return path{ p };
}

}

} }
