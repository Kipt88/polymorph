#include "filesystem/FileOutputStream.hpp"

#include <string>

using namespace std::string_literals;

namespace pm { namespace filesystem {

FileOutputStream::FileOutputStream(const std::filesystem::path& path, bool textFile)
{
	auto enclosingDirectory = std::filesystem::path{ path }.remove_filename();

	if (!std::filesystem::exists(enclosingDirectory))
		std::filesystem::create_directories(enclosingDirectory);

#ifdef _WIN32
	if (fopen_s(&_fp, path.string().c_str(), textFile ? "w" : "wb") != 0)
		throw FileOpenException("Can't open '"s + path.string() + "' for writing."s);
#else
	_fp = std::fopen(path.string().c_str(), textFile ? "w" : "wb");
	if (!_fp)
		throw FileOpenException();
#endif
}

FileOutputStream::~FileOutputStream()
{
	std::fclose(_fp);
}

void FileOutputStream::write(const void* data,
							 std::size_t /*typeAlignment*/,
							 std::size_t size,
							 std::size_t count)
{
	if (fwrite(data, size, count, _fp) < count)
	{
		// WARNING: The data buffer may not be in a valid state after this.
		throw FileWriteException();
	}
}

} }
