#include <gtest/gtest.h>

#include <filesystem/FileOutputStream.hpp>
#include <filesystem/FileInputStream.hpp>
#include <filesystem/FileException.hpp>

using namespace pm;

TEST(FileOutputStream, primitive)
{
	const char* FILENAME = "test_FileOutputStream_primitive";

	{
		filesystem::FileOutputStream out(FILENAME);

		out << static_cast<char>(1);
		out << static_cast<int>(2);
		out << static_cast<long>(3);
	}

	{
		filesystem::FileInputStream in(FILENAME);

		char c;
		int i;
		long l;

		in >> c >> i >> l;

		ASSERT_EQ(c, 1);
		ASSERT_EQ(i, 2);
		ASSERT_EQ(l, 3);
	}
}

TEST(FileOutputStream, struct_block)
{
	const char* FILENAME = "test_FileOutputStream_struct_block";

	struct TestStruct
	{
		char c;
		int i;
		long l;
	};

	{
		filesystem::FileOutputStream out(FILENAME);

		// Note: Currently type alignement is not considered in file I/O,
		// hence writing primitive by primitive and reading as struct will
		// fail due to padding.
		out << TestStruct{ 1, 2, 3 };
	}

	{
		filesystem::FileInputStream in(FILENAME);

		TestStruct s;

		in >> s;

		ASSERT_EQ(s.c, 1);
		ASSERT_EQ(s.i, 2);
		ASSERT_EQ(s.l, 3);
	}
}
