#include <gtest/gtest.h>

#include <filesystem/FileInputStream.hpp>
#include <filesystem/FileException.hpp>

#include <filesystem.hpp>

#include <vector>
#include <algorithm>
#include <array>

using namespace pm;

const std::filesystem::path PATH_TO_TEST_FILE = std::filesystem::path{".."} / ".." / ".." / "test" / "test_files" / "test1.bin";

std::array<unsigned char, 16> EXPECTED_TEST_FILE_CONTENT{{
	0xDE, 0xAD, 0xBA, 0xAD, 0xBE, 0xEF, 0xCA, 0xCE,
	0xF0, 0x0D, 0xBA, 0xAD, 0xAB, 0x01, 0x00, 0x00
}};

TEST(FileInputStream, does_not_exist)
{
	ASSERT_THROW(
		{
			filesystem::FileInputStream in("does_not_exist.file");
		},
		filesystem::FileOpenException
	);
}

TEST(FileInputStream, read_char)
{
	std::cout << std::filesystem::current_path() << '\n';
	
	filesystem::FileInputStream in(PATH_TO_TEST_FILE.string());

	ASSERT_EQ(EXPECTED_TEST_FILE_CONTENT.size(), in.available());

	unsigned char c;
	for (unsigned int i = 0; in.available() > 0; ++i)
	{
		in >> c;
		ASSERT_EQ(c, EXPECTED_TEST_FILE_CONTENT[i]);
	}
}

TEST(FileInputStream, read_charblock)
{
	filesystem::FileInputStream in(PATH_TO_TEST_FILE.string());

	ASSERT_EQ(EXPECTED_TEST_FILE_CONTENT.size(), in.available());

	std::vector<unsigned char> data;
	data.resize(EXPECTED_TEST_FILE_CONTENT.size());

	io::readBlock(in, data.data(), in.available());

	ASSERT_TRUE(
		std::equal(
			EXPECTED_TEST_FILE_CONTENT.begin(),
			EXPECTED_TEST_FILE_CONTENT.end(),
			data.begin(),
			data.end()
		)
	);
}
