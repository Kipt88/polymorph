#include <gtest/gtest.h>
#include <SortedCollection.hpp>

#include <algorithm>

TEST(SortedCollection, normal)
{
	{
		SortedCollection<int> collection;

		collection.insert(3);
		collection.insert(1);
		collection.insert(5);
		collection.insert(2);
		
		ASSERT_TRUE(
			std::is_sorted(
				collection.begin(), 
				collection.end(),
				collection.compare()
			)
		);
	}

	{
		SortedCollection<int> collection;

		collection.insert(3);
		collection.insert(1);
		collection.insert(5);
		collection.insert(2);
		collection.insert(3);
		collection.insert(1);
		collection.insert(5);
		auto it = collection.insert(2);

		collection.erase(it);

		ASSERT_TRUE(
			std::is_sorted(
				collection.begin(),
				collection.end(),
				collection.compare()
			)
		);
	}

	{
		SortedCollection<int> collection;

		auto list = { 3, 1, 5, 2, 3, 1, 5 };

		for (const auto& i : list)
			collection.insert(i);

		for (const auto& i : list)
			ASSERT_EQ(*collection.find(i), i);

		ASSERT_EQ(collection.find(14), collection.end());
	}

	{
		SortedCollection<int> collection;

		collection.insert(3);
		collection.insert(1);

		auto it = collection.findLargerOrSame(2);

		ASSERT_EQ(*it, 3);

		collection.erase(it);

		ASSERT_EQ(collection.findLargerOrSame(2), collection.end());
		ASSERT_EQ(collection.findLargerOrSame(1), collection.begin());
	}

	{
		SortedCollection<int> collection;

		collection.insert(3);
		collection.insert(1);
		collection.insert(5);

		auto it = collection.findSmallerOrSame(4);

		ASSERT_EQ(*it, 3);

		it = collection.findSmallerOrSame(5);

		ASSERT_EQ(*it, 5);
	}
}
