#include <gtest/gtest.h>
#include <method_traits.hpp>

using namespace pm;

struct TestStruct
{
	int i;

	void setToOne()
	{
		i = 1;
	}

	int ret(int j)
	{
		return j;
	}
};

TEST(method_traits, test_call)
{
	TestStruct instance{ 0 };

	{
		callTemplateMethod<decltype(&TestStruct::setToOne), &TestStruct::setToOne>(&instance);
		ASSERT_EQ(instance.i, 1);

		int i = callTemplateMethod<decltype(&TestStruct::ret), &TestStruct::ret>(&instance, 2);
		ASSERT_EQ(i, 2);
	}

	{
		callTemplateMethod<std::nullptr_t, nullptr>(nullptr);
		callTemplateMethod<std::nullptr_t, nullptr>(&instance);
	}
}
