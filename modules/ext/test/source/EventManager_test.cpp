#include <gtest/gtest.h>

#include <Event.hpp>
#include <vector>

struct Empty1 {};
struct Empty2 {};
struct IntInt
{
	int a, b;
};

using namespace pm;

TEST(EventManager, listen_trigger)
{
	{
		// Just to check it doesn't crash.
		EventManager<Empty1, Empty2, IntInt> manager;
	}

	{
		EventManager<Empty1, Empty2, IntInt> manager;

		auto scope1 = manager.addListener<Empty1>([]{});
		auto scope2 = manager.addListener<Empty2>([]{});
		auto scope3 = manager.addListener<IntInt>(
			[](const IntInt& ii)
			{
				ASSERT_EQ(ii.a, 1);
				ASSERT_EQ(ii.b, 2);
			}
		);

		EventManager<Empty1, Empty2, IntInt>::scope_t<IntInt> scope4(std::move(scope3));

		manager.triggerEvent<Empty1>();
		manager.triggerEvent<Empty2>();
		manager.triggerEvent<IntInt>({ 1, 2 });
	}

	{
		EventManager<Empty1, Empty2, IntInt> manager;

		bool flag1 = false;
		bool flag2 = false;

		auto scope1 = manager.addListener<Empty1>([&]{ flag1 = true; });
		auto scope2 = manager.addListener<Empty2>([&]{ flag2 = true; });

		manager.triggerEvent<Empty1>();
		manager.triggerEvent<Empty2>();

		ASSERT_TRUE(flag1 && flag2);
	}
}
