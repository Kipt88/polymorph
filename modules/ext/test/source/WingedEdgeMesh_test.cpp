#include <gtest/gtest.h>

#include <WingedEdgeMesh.hpp>
#include <array>

struct Vertex
{
	char id;
};

struct Edge
{
	char id;
};

struct Face
{
	char id;
};

typedef pm::WingedEdgeMesh<Vertex, Edge, Face> Mesh;

// See https://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/model/winged-e.html for image.
//
// - Closed volume
// - All surfaces adjacent to each other.
// - All edges have exactly 2 adjacent faces.
//
Mesh createTetrahedron()
{
	std::array<Vertex, 4> vertices{{
		{'A'}, {'B'}, {'C'}, {'D'}
	}};

	std::array<Edge, 6> edges{{
		{'a'}, {'b'}, {'c'}, {'d'}, {'e'}, {'f'}
	}};

	std::array<Face, 4> faces{{
		{'1'}, {'2'}, {'3'}, {'4'}
	}};

	std::array<pm::vertex_index_t, 4> vertexIndices;
	std::array<pm::edge_index_t, 6> edgeIndices;

	Mesh mesh;

	// Vertices:

	for (unsigned int i = 0; i < vertices.size(); ++i)
		vertexIndices[i] = mesh.addVertex(vertices[i]);
	
	// Edges:

	// A-D
	edgeIndices[0] = mesh.addEdge(edges[0], vertexIndices[0], vertexIndices[3]);
	// A-B
	edgeIndices[1] = mesh.addEdge(edges[1], vertexIndices[0], vertexIndices[1]);
	// B-D
	edgeIndices[2] = mesh.addEdge(edges[2], vertexIndices[1], vertexIndices[3]);
	// B-C
	edgeIndices[3] = mesh.addEdge(edges[3], vertexIndices[1], vertexIndices[2]);
	// C-D
	edgeIndices[4] = mesh.addEdge(edges[4], vertexIndices[2], vertexIndices[3]);
	// A-C
	edgeIndices[5] = mesh.addEdge(edges[5], vertexIndices[0], vertexIndices[2]);

	// Faces:

	// D-A A-B B-D (a, b, c)
	mesh.addFace(faces[0], { edgeIndices[0], edgeIndices[1], edgeIndices[2] });

	// D-B B-C C-D (c, d, e)
	mesh.addFace(faces[1], { edgeIndices[2], edgeIndices[3], edgeIndices[4] });

	// D-C C-A A-D (e, f, a)
	mesh.addFace(faces[2], { edgeIndices[4], edgeIndices[5], edgeIndices[0] });

	// A-C C-B B-A (f, d, b)
	mesh.addFace(faces[3], { edgeIndices[5], edgeIndices[3], edgeIndices[1] });

	return mesh;
}

//
// Topology:
//
//    D -c- C
//    | \ 2 |
//    d  e  b
//    | 1 \ |
//    A -a- B
//
Mesh createTriangulatedSquare()
{
	Mesh mesh;

	auto A = mesh.addVertex({ 'A' });
	auto B = mesh.addVertex({ 'B' });
	auto C = mesh.addVertex({ 'C' });
	auto D = mesh.addVertex({ 'D' });

	auto AB = mesh.addEdge({ 'a' }, A, B);
	auto BC = mesh.addEdge({ 'b' }, B, C);
	auto CD = mesh.addEdge({ 'c' }, C, D);
	auto AD = mesh.addEdge({ 'd' }, A, D);
	auto BD = mesh.addEdge({ 'e' }, B, D);

	mesh.addFace({ '1' }, { AB, BD, AD });
	mesh.addFace({ '2' }, { BC, CD, BD });

	return mesh;
}

TEST(WingedEdgeMesh, tetrahedron_indices)
{
	Mesh mesh = createTetrahedron();

	{
		// Expected vertex content:
		std::array<Vertex, 4> vertices{{
			{'A'}, {'B'}, {'C'}, {'D'}
		}};

		ASSERT_EQ(vertices.size(), mesh.vertexSize());

		for (pm::vertex_index_t i = 0; i < mesh.vertexSize(); ++i)
		{
			ASSERT_EQ(vertices[i].id, mesh.vertex(i).id);
		}
	}

	{
		// Expected edge content:
		std::array<Edge, 6> edges{{
			{'a'}, {'b'}, {'c'}, {'d'}, {'e'}, {'f'}
		}};

		ASSERT_EQ(edges.size(), mesh.edgeSize());

		for (pm::edge_index_t i = 0; i < mesh.edgeSize(); ++i)
		{
			ASSERT_EQ(edges[i].id, mesh.edge(i).id);
		}
	}

	{
		// Expected face content:
		std::array<Face, 4> faces{{
			{'1'}, {'2'}, {'3'}, {'4'}
		}};

		ASSERT_EQ(faces.size(), mesh.faceSize());

		for (pm::face_index_t i = 0; i < mesh.faceSize(); ++i)
		{
			ASSERT_EQ(faces[i].id, mesh.face(i).id);
		}
	}
}

TEST(WingedEdgeMesh, tetrahedron_faceEdges)
{
	Mesh mesh = createTetrahedron();

	{
		std::array<pm::edge_index_t, 3> edges{{ 0, 1, 2 }};

		auto faceEdges = mesh.faceEdges(0);

		ASSERT_EQ(faceEdges.size(), edges.size());

		auto it = std::find(edges.begin(), edges.end(), faceEdges.front());
		ASSERT_NE(it, edges.end());

		std::size_t offset = std::distance(edges.begin(), it);

		for (std::size_t i = 0; i < edges.size(); ++i)
		{
			ASSERT_EQ(faceEdges[i], edges[(offset + i) % edges.size()]);
		}
	}

	{
		std::array<pm::edge_index_t, 3> edges{ { 2, 3, 4 } };

		auto faceEdges = mesh.faceEdges(1);

		ASSERT_EQ(faceEdges.size(), edges.size());

		auto it = std::find(edges.begin(), edges.end(), faceEdges.front());
		ASSERT_NE(it, edges.end());

		std::size_t offset = std::distance(edges.begin(), it);

		for (std::size_t i = 0; i < edges.size(); ++i)
		{
			ASSERT_EQ(faceEdges[i], edges[(offset + i) % edges.size()]);
		}
	}

	{
		std::array<pm::edge_index_t, 3> edges{ { 0, 4, 5 } };

		auto faceEdges = mesh.faceEdges(2);

		ASSERT_EQ(faceEdges.size(), edges.size());

		auto it = std::find(edges.begin(), edges.end(), faceEdges.front());
		ASSERT_NE(it, edges.end());

		std::size_t offset = std::distance(edges.begin(), it);

		for (std::size_t i = 0; i < edges.size(); ++i)
		{
			ASSERT_EQ(faceEdges[i], edges[(offset + i) % edges.size()]);
		}
	}

	{
		std::array<pm::edge_index_t, 3> edges{ { 1, 5, 3 } };

		auto faceEdges = mesh.faceEdges(3);

		ASSERT_EQ(faceEdges.size(), edges.size());

		auto it = std::find(edges.begin(), edges.end(), faceEdges.front());
		ASSERT_NE(it, edges.end());

		std::size_t offset = std::distance(edges.begin(), it);

		for (std::size_t i = 0; i < edges.size(); ++i)
		{
			ASSERT_EQ(faceEdges[i], edges[(offset + i) % edges.size()]);
		}
	}
}

TEST(WingedEdgeMesh, tetrahedron_adjacentFaces)
{
	Mesh mesh = createTetrahedron();

	// Each face in a tetrahedron is adjacent to all other faces.

	for (unsigned int i = 0; i < 4; ++i)
	{
		auto faces = mesh.adjacentFaces(i);

		ASSERT_EQ(faces.size(), 3);

		for (unsigned int j = 0; j < 4; ++j)
		{
			if (j != i)
				ASSERT_EQ(std::count(faces.begin(), faces.end(), j), 1);
			else
				ASSERT_EQ(std::count(faces.begin(), faces.end(), j), 0);
		}
	}
}


//
// Topology:
//
//    D -c- C
//    | \ 2 |
//    d  e  b
//    | 1 \ |
//    A -a- B
//
TEST(WingedEdgeMesh, triangulated_square_faceEdges)
{
	Mesh mesh = createTriangulatedSquare();

	{
		std::array<pm::edge_index_t, 3> edges{ { 0, 4, 3 } };

		auto faceEdges = mesh.faceEdges(0);

		ASSERT_EQ(faceEdges.size(), edges.size());

		auto it = std::find(edges.begin(), edges.end(), faceEdges.front());
		ASSERT_NE(it, edges.end());

		std::size_t offset = std::distance(edges.begin(), it);

		for (std::size_t i = 0; i < edges.size(); ++i)
		{
			ASSERT_EQ(faceEdges[i], edges[(offset + i) % edges.size()]);
		}
	}

	{
		std::array<pm::edge_index_t, 3> edges{ { 1, 2, 4 } };

		auto faceEdges = mesh.faceEdges(1);

		ASSERT_EQ(faceEdges.size(), edges.size());

		auto it = std::find(edges.begin(), edges.end(), faceEdges.front());
		ASSERT_NE(it, edges.end());

		std::size_t offset = std::distance(edges.begin(), it);

		for (std::size_t i = 0; i < edges.size(); ++i)
		{
			ASSERT_EQ(faceEdges[i], edges[(offset + i) % edges.size()]);
		}
	}
}

TEST(WingedEdgeMesh, triangulated_square_adjacentFaces)
{
	Mesh mesh = createTriangulatedSquare();

	// Two faces, adjacent to each other.

	for (unsigned int i = 0; i < 2; ++i)
	{
		auto faces = mesh.adjacentFaces(i);

		ASSERT_EQ(faces.size(), 1);
		ASSERT_EQ(faces.front(), (i + 1) % 2);
	}
}