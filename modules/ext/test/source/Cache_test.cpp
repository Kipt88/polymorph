#include <gtest/gtest.h>
#include <Cache.hpp>

using namespace pm;

struct TestStruct
{
	int i;

	bool operator<(const TestStruct& other) const
	{
		return i < other.i;
	}
};

TEST(Cache, insert_erase)
{
	Cache<TestStruct> cache;

	auto handle1 = cache.insert({ 1 });
	auto handle2 = cache.insert({ 2 });
	auto handle3 = cache.insert({ 3 });
	auto handle4 = cache.insert({ 4 });

	ASSERT_EQ(handle1->i, 1);
	ASSERT_EQ((*handle2).i, 2);
	ASSERT_NE(handle1, handle2);
	
	{
		auto t = cache.find_if([](const TestStruct& s) { return s.i == 1; });
		ASSERT_EQ(t, handle1);
	}

	{
		auto t = cache.erase_if([](const TestStruct& s) { return s.i == 1; });
		ASSERT_EQ(t, 1);
	}

	ASSERT_TRUE(!cache.find_if([](const TestStruct& s) { return s.i == 1; }));

	{
		auto t = cache.erase_if([](const TestStruct& s) { return s.i >= 3; });
		ASSERT_EQ(t, 2);
	}

	ASSERT_EQ(cache.size(), 1);

	cache.clear();

	handle1 = cache.insert({ 1 });
	handle2 = cache.insert({ 2 });
	handle3 = cache.insert({ 3 });
	handle4 = cache.insert({ 4 });

	ASSERT_EQ(*cache.begin(), handle1);

	handle1.use();

	ASSERT_EQ(*cache.begin(), handle2);
}