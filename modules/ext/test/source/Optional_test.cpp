#include <gtest/gtest.h>

#include <optional.hpp>

struct Movable
{
	Movable(int i_) : i(i_) {}

	Movable(Movable&& other) 
	: i(other.i)
	{
		other.i = 0; 
	}
	
	Movable& operator=(Movable&& other)
	{
		i = other.i; 
		other.i = 0; 
		return *this;
	}

	Movable(const Movable&) = delete;
	Movable& operator=(const Movable&) = delete;

	int i;
};

TEST(Optional, ctor)
{
	{
		std::optional<int> a;
		ASSERT_FALSE(static_cast<bool>(a));
	}

	{
		std::optional<int> a(1);
		
		ASSERT_TRUE(static_cast<bool>(a));

		std::optional<int> b(a);
		
		ASSERT_TRUE(static_cast<bool>(a));
		ASSERT_TRUE(static_cast<bool>(b));

		std::optional<int> c(std::move(b));

		ASSERT_TRUE(static_cast<bool>(a));
		ASSERT_TRUE(static_cast<bool>(b));
		ASSERT_TRUE(static_cast<bool>(c));

		ASSERT_EQ(*a, 1);
		ASSERT_EQ(*b, 1);
		ASSERT_EQ(*c, 1);
	}

	{
		std::optional<Movable> a(1);
		ASSERT_TRUE(static_cast<bool>(a));
	}

	{
		std::optional<int> a(std::nullopt);
		ASSERT_FALSE(static_cast<bool>(a));
	}
}

TEST(Optional, assignment)
{
	{
		std::optional<int> a;

		a = 4;
		ASSERT_TRUE(static_cast<bool>(a));
		ASSERT_EQ(*a, 4);

		std::optional<int> b;

		b = a;
		ASSERT_TRUE(static_cast<bool>(b));
		ASSERT_EQ(*b, *a);
	}

	{
		std::optional<int> a;

		std::optional<int> b;

		b = a;
		ASSERT_FALSE(static_cast<bool>(b));
	}

	{
		std::optional<int> a;

		ASSERT_FALSE(static_cast<bool>(a));

		std::optional<int> b(1);

		b = a;
		ASSERT_FALSE(static_cast<bool>(b));
	}

	{
		std::optional<Movable> a;

		a = Movable(1);

		ASSERT_TRUE(static_cast<bool>(a));
		ASSERT_EQ(a->i, 1);

		std::optional<Movable> b;

		b = std::move(a);

		ASSERT_TRUE(static_cast<bool>(a));
		ASSERT_EQ(a->i, 0);
		ASSERT_TRUE(static_cast<bool>(b));
		ASSERT_EQ(b->i, 1);
	}
}

TEST(Optional, value_or)
{
	{
		std::optional<int> a;

		int i = a.value_or(0);

		ASSERT_EQ(i, 0);
	}

	{
		int i = std::optional<int>().value_or(0);

		ASSERT_EQ(i, 0);
	}
}
