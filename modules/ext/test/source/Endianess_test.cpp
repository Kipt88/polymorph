#include <gtest/gtest.h>

#include <Endianess.hpp>

template <typename Scalar>
void testToggleEndianess()
{
	union PunUnion
	{
		Scalar scalar;
		unsigned char c[sizeof(Scalar)];
	} data;

	data.scalar = Scalar(1);

	PunUnion flippedData;
	flippedData.scalar = flippedEndianess(data.scalar);

	for (int i = 0; i < sizeof(Scalar); ++i)
		ASSERT_EQ(data.c[i], flippedData.c[sizeof(Scalar) - 1 - i]);
}

TEST(Endianess, unique)
{
	ASSERT_TRUE(isBigEndian() && !isLittleEndian() || isLittleEndian() && !isBigEndian());
}

TEST(Endianess, toggle_short)
{
	testToggleEndianess<short>();
}

TEST(Endianess, toggle_int)
{
	testToggleEndianess<int>();
}

TEST(Endianess, toggle_long)
{
	testToggleEndianess<long>();
}

TEST(Endianess, toggle_long_long)
{
	testToggleEndianess<long long>();
}

TEST(Endianess, toggle_ushort)
{
	testToggleEndianess<unsigned short>();
}

TEST(Endianess, toggle_uint)
{
	testToggleEndianess<unsigned int>();
}

TEST(Endianess, toggle_ulong)
{
	testToggleEndianess<unsigned long>();
}

TEST(Endianess, toggle_ulong_long)
{
	testToggleEndianess<unsigned long long>();
}

TEST(Endianess, toggle_float)
{
	testToggleEndianess<float>();
}

TEST(Endianess, toggle_double)
{
	testToggleEndianess<double>();
}

TEST(Endianess, toggle_long_double)
{
	testToggleEndianess<long double>();
}
