#pragma once

#include <exception>
#include <string>

/**
 * Helper to create new exception classes.
 * Exceptions should be described by their class, not their message.
 */
#define PM_MAKE_EXCEPTION_CLASS(clazz, super) \
	class clazz : public super \
	{ public: \
		clazz(const std::string& msg = "") noexcept : super(msg) {} \
		virtual ~clazz() noexcept = default; \
	}

namespace pm {

class Exception : public std::exception
{
public:
	Exception(const std::string& msg = "Exception") noexcept
	: _msg(msg)
	{
	}

	virtual ~Exception() noexcept = default;

	inline const char* what() const noexcept override
	{
		return _msg.c_str();
	}

private:
	std::string _msg;
};


}