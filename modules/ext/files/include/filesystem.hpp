#pragma once

#if defined(_WIN32) && defined(_MSC_VER) // VS has experimental filesystem.

#include <experimental/filesystem>

namespace std {

using namespace std::experimental;

}

#else

#include <boost/filesystem.hpp>

namespace std {
	
using namespace boost;

}

#endif