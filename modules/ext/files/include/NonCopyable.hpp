#pragma once

/**
 * Helper class to delete copy operators.
 */
class NonCopyable 
{
protected:
	NonCopyable() = default;
	~NonCopyable() = default;

	NonCopyable(const NonCopyable&) = delete;
	NonCopyable& operator=(const NonCopyable&) = delete;

	NonCopyable(NonCopyable&&) = default;
	NonCopyable& operator=(NonCopyable&&) = default;
};

/**
 * Helper class to delete move operators.
 */
class NonMovable
{
protected:
	NonMovable() = default;
	~NonMovable() = default;

	NonMovable(const NonMovable&) = default;
	NonMovable& operator=(const NonMovable&) = default;

	NonMovable(NonMovable&&) = delete;
	const NonMovable& operator=(NonMovable&&) = delete;
};

/**
 * Helper class to delete copy and move operators.
 */
class Unique
{
protected:
	Unique() = default;
	~Unique() = default;

	Unique(const Unique&) = delete;
	Unique& operator=(const Unique&) = delete;

	Unique(Unique&&) = delete;
	const Unique& operator=(Unique&&) = delete;
};
