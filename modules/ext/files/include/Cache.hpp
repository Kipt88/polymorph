#pragma once

#include <map>
#include <set>
#include <deque>
#include <queue>
#include <iterator>

namespace pm {

template <typename CacheType, typename It>
class handle_t
{
public:
	typedef typename CacheType::value_type value_type;
	typedef It iterator;

	handle_t(iterator it, iterator end, const CacheType& cache);

	const value_type& operator*() const;
	const value_type* operator->() const;
	bool operator==(const handle_t& other) const;
	bool operator!=(const handle_t& other) const;
	bool operator<(const handle_t& other) const;
	bool operator!() const;

	explicit operator bool();

	void use() const;

private:
	iterator _it;
	iterator _end;

	const CacheType* _cache;
};

namespace cache_algorithms {

/* Concept CacheAlgorithm:
 *
 *   template parameter Key
 *   - CopyConstructible
 *   - EqualityComparable
 *   - LessThanComparable
 *
 * Public interface:
 *   
 *   // Adds a new key, same key will never be added twice.
 *   void add(const Key& key);
 *
 *   // Notifies the algorithm key is being used and it's priority may be updated.
 *   // @pre Same key has been called with add.
 *   void use(const Key& key);
 *
 *   // Removes key.
 *   // @pre Same key has been called with add.
 *   void erase(const Key& key);
 */

// Least Recently Used cache algorithm.
template <typename Handle>
class LRU
{
public:
	typedef typename std::deque<Handle>::const_iterator
		const_iterator;

	void add(const Handle& handle);
	void use(const Handle& handle);
	void erase(const Handle& handle);
	void clear();

	const_iterator begin() const;
	const_iterator end() const;

private:
	std::deque<Handle> _handles;
};

}

template <
	typename Value, 
	template <typename> class CacheAlgorithm = cache_algorithms::LRU
>
class Cache
{
public:
	typedef Value value_type;
	typedef handle_t<Cache, typename std::set<Value>::const_iterator> handle_t;
	typedef typename CacheAlgorithm<handle_t>::const_iterator const_iterator;

	handle_t insert(const value_type& data);
	handle_t insert(value_type&& data);

	template <typename... Args>
	handle_t emplace(Args&&... args);

	template <typename Predicate>
	handle_t find_if(Predicate&& pred) const;

	void use(const handle_t& handle) const;

	template <typename Predicate>
	std::size_t erase_if(Predicate&& pred, std::size_t amount = 0);

	template <typename Predicate>
	std::size_t erase_while(Predicate&& pred);

	void erase(const handle_t& handle);

	std::size_t size() const;

	void clear();

	const_iterator begin() const;
	const_iterator end() const;

private:
	std::set<Value> _data;

	mutable CacheAlgorithm<handle_t> _algorithm;
};

template <
	typename Key, 
	typename Value, 
	template <typename> class CacheAlgorithm = cache_algorithms::LRU
>
class CacheMap
{
public:
	typedef std::pair<const Key, Value> value_type;
	typedef handle_t<CacheMap, typename std::map<Key, Value>::const_iterator> handle_t;
	typedef typename CacheAlgorithm<handle_t>::const_iterator const_iterator;
	

	handle_t insert(const value_type& data);
	handle_t insert(value_type&& data);

	template <typename... Args>
	handle_t emplace(Args&&... args);

	template <typename Predicate>
	handle_t find_if(Predicate&& pred) const;

	handle_t find(const Key& key) const;

	void use(const handle_t& handle) const;

	bool has(const Key& key) const;

	template <typename Predicate>
	std::size_t erase_if(Predicate&& pred, std::size_t amount = 0);

	template <typename Predicate>
	std::size_t erase_while(Predicate&& pred);

	void erase(const handle_t& handle);

	std::size_t size() const;

	void clear();

	const_iterator begin() const;
	const_iterator end() const;

private:
	std::map<Key, Value> _data;

	mutable CacheAlgorithm<handle_t> _algorithm;
};


}

#include "Cache.inl"
