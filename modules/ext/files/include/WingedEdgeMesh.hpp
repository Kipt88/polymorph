#pragma once

#include <optional.hpp>
#include <vector>

namespace pm {

typedef std::size_t vertex_index_t;
typedef std::size_t edge_index_t;
typedef std::size_t face_index_t;

namespace detail {

struct empty {};

template <typename VertexData>
struct Vertex
{
	edge_index_t edge;
	VertexData data;
};

template <typename EdgeData>
struct Edge
{
	std::array<vertex_index_t, 2> vertices; // begin, end vertices.
	std::array<face_index_t, 2> faces;      // left, right faces.

	edge_index_t prevCCW;
	edge_index_t nextCCW;

	edge_index_t prevCW;
	edge_index_t nextCW;

	EdgeData data;
};

template <typename FaceData>
struct Face
{
	edge_index_t edge;
	FaceData data;
};

}

template <
	typename VertexData = detail::empty, 
	typename EdgeData = detail::empty, 
	typename FaceData = detail::empty
>
class WingedEdgeMesh
{
public:
	typedef detail::Vertex<VertexData> vertex_t;
	typedef detail::Edge<EdgeData> edge_t;
	typedef detail::Face<FaceData> face_t;

	vertex_index_t addVertex(const VertexData& data);
	edge_index_t addEdge(const EdgeData& data, vertex_index_t vertexA, vertex_index_t vertexB);
	face_index_t addFace(const FaceData& data, const std::vector<edge_index_t>& edges);

	// Note, start face/edge/vertex can be any of the valid ones.

	std::optional<edge_index_t> findEdge(vertex_index_t a, vertex_index_t b) const;

	std::vector<vertex_index_t> faceVertices(face_index_t face) const;
	std::vector<edge_index_t> faceEdges(face_index_t face) const;
	std::vector<face_index_t> adjacentFaces(face_index_t face) const;

	std::array<std::optional<face_index_t>, 2> edgeFaces(edge_index_t edge) const;
	std::array<vertex_index_t, 2> edgeVertices(edge_index_t edge) const;

	VertexData& vertex(vertex_index_t index);
	EdgeData& edge(edge_index_t index);
	FaceData& face(face_index_t index);

	const VertexData& vertex(vertex_index_t index) const;
	const EdgeData& edge(edge_index_t index) const;
	const FaceData& face(face_index_t index) const;

	vertex_index_t vertexSize() const;
	edge_index_t edgeSize() const;
	face_index_t faceSize() const;

private:
	std::vector<vertex_t> _vertices;
	std::vector<edge_t> _edges;
	std::vector<face_t> _faces;
};

}

#include "WingedEdgeMesh.inl"
