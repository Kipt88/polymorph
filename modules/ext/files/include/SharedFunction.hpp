#pragma once

#include <memory>

namespace pm {

template <typename T>
class SharedFunction;

template <typename R, typename... Args>
class SharedFunction<R(Args...)>
{
public:
	template <typename Callable>
	SharedFunction(Callable&& func);

	SharedFunction() = default;

	SharedFunction(std::nullptr_t);

	explicit operator bool() const;

	R operator()(Args&&... args);

private:
	class CallableBase
	{
	public:
		virtual ~CallableBase() = default;

		virtual R operator()(Args&&...) = 0;
	};

	template <typename Callable>
	class CallableImpl : public CallableBase
	{
	public:
		CallableImpl(Callable&& callable);

		R operator()(Args&&...) override;

	private:
		std::decay_t<Callable> _callable;
	};

	std::shared_ptr<CallableBase> _impl;
};

}

#include "SharedFunction.inl"