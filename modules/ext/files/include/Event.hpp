#pragma once

#include "NonCopyable.hpp"
#include "method_traits.hpp"

#include <type_traits>
#include <memory>
#include <set>
#include <tuple>


#ifdef _MSC_VER
	// This disables warning regarding truncated type name.
	#pragma warning(disable : 4503)
#endif

namespace pm {

namespace detail {

template <typename EventParam>
class WeakEventAction
{
public:
	template <typename M, M method>
	void set(method_class_t<M>& obj);

	void operator()(EventParam e) const;
	bool operator<(const WeakEventAction& other) const;

private:
	using method_ptr_t = void(*)(void*, EventParam);

	void* _obj;
	method_ptr_t _method;
};

template <>
class WeakEventAction<void>
{
public:

	template <typename M, M method>
	void set(method_class_t<M>& obj);

	void operator()() const;

	bool operator<(const WeakEventAction& other) const;

private:
	using method_ptr_t = void(*)(void*);

	void* _obj;
	method_ptr_t _method;
};

template <typename EventParam>
class EventAction
{
public:
	template <
		typename Callable, 
		typename = std::enable_if_t<
			!std::is_same<
				std::decay_t<Callable>, 
				EventAction<EventParam>
			>::value
		>
	>
	EventAction(Callable&& action);

	EventAction(const EventAction& other) = default;

	void operator()(EventParam e) const;
	bool operator<(const EventAction& other) const;

private:
	class Impl
	{
	public:
		virtual ~Impl() = default;
		virtual void operator()(EventParam e) const = 0;
	};

	template <typename Callable>
	class Derived : public Impl, NonCopyable
	{
	public:
		Derived(Callable&& action);

		void operator()(EventParam e) const override;

	private:
		mutable std::decay_t<Callable> _action;
	};

	std::shared_ptr<Impl> _impl;
};

template <>
class EventAction<void>
{
public:
	template <
		typename Callable,
		typename = std::enable_if_t<
			!std::is_same<
				std::decay_t<Callable>, 
				EventAction<void>
			>::value
		>
	>
	EventAction(Callable&& action);

	EventAction(const EventAction& other) = default;

	void operator()() const;
	bool operator<(const EventAction& other) const;

private:
	class Impl
	{
	public:
		virtual ~Impl() = default;
		virtual void operator()() const = 0;
	};

	template <typename Callable>
	class Derived : public Impl, NonCopyable
	{
	public:
		Derived(Callable&& action);

		void operator()() const override;

	private:
		mutable std::decay_t<Callable> _action;
	};

	std::shared_ptr<Impl> _impl;
};

template <typename EventType, bool = std::is_empty<EventType>::value>
struct types
{
	typedef const EventType& event_param_t;
};

template <typename EventType>
struct types<EventType, true>
{
	typedef void event_param_t;
};

template <typename EventType>
using event_param_t = typename types<EventType>::event_param_t;

template <typename EventType>
using handle_t = typename std::set<detail::EventAction<event_param_t<EventType>>>::iterator;

template <typename EventType>
using weak_handle_t = typename std::set<detail::WeakEventAction<event_param_t<EventType>>>::iterator;

}

template <typename... EventTypes>
class EventManager;

template <typename ThisEventType, typename... EventTypes>
class EventHandlerScope : NonCopyable
{
public:
	EventHandlerScope();

	EventHandlerScope(
		const EventManager<EventTypes...>& manager, 
		const detail::handle_t<ThisEventType>& handle
	);
	
	EventHandlerScope(EventHandlerScope&&);

	~EventHandlerScope();

	void reset();

	EventHandlerScope& operator=(EventHandlerScope&& other);

	bool operator<(const EventHandlerScope& other) const;

	operator bool() const;

private:
	detail::handle_t<ThisEventType> _handle;
	const EventManager<EventTypes...>* _manager;
};

template <typename... EventTypes>
class EventManager : NonCopyable, NonMovable
{
public:
	EventManager() = default;

#ifdef _DEBUG
	~EventManager();
#endif

	template <typename EventType>
	using scope_t = EventHandlerScope<EventType, EventTypes...>;

	template <typename EventType>
	void triggerEvent();

	template <typename EventType>
	void triggerEvent(const detail::event_param_t<EventType>& e);

	template <typename EventType, typename Callable>
	scope_t<EventType> addListener(Callable&& callable) const;

	template <typename EventType>
	void removeListener(const detail::handle_t<EventType>& it) const;

private:
	template <typename EventType>
	using listeners_t = std::set<
		detail::EventAction<detail::event_param_t<EventType>>
	>;

	template <typename EventType>
	struct listeners_tag
	{
		listeners_t<EventType> listeners;
	};

	mutable std::tuple<listeners_tag<EventTypes>...> _eventListeners;
};

// Weak

template <typename... EventTypes>
class WeakEventManager;

template <typename ThisEventType, typename... EventTypes>
class WeakEventHandlerScope : NonCopyable
{
public:
	WeakEventHandlerScope();

	WeakEventHandlerScope(
		const WeakEventManager<EventTypes...>& manager,
		const detail::weak_handle_t<ThisEventType>& handle
	);

	WeakEventHandlerScope(WeakEventHandlerScope&&);

	~WeakEventHandlerScope();

	void reset();

	WeakEventHandlerScope& operator=(WeakEventHandlerScope&& other);

	bool operator<(const WeakEventHandlerScope& other) const;

	operator bool() const;

private:
	detail::weak_handle_t<ThisEventType> _handle;
	const WeakEventManager<EventTypes...>* _manager;
};

template <typename... EventTypes>
class WeakEventManager : NonCopyable, NonMovable
{
public:
	WeakEventManager() = default;

#ifdef _DEBUG
	~WeakEventManager();
#endif

	template <typename EventType>
	using scope_t = WeakEventHandlerScope<EventType, EventTypes...>;

	template <typename EventType>
	void triggerEvent();

	template <typename EventType>
	void triggerEvent(const detail::event_param_t<EventType>& e);

	template <typename EventType, typename M, M method>
	scope_t<EventType> addListener(method_class_t<M>& obj) const;

	template <typename EventType>
	void removeListener(const detail::weak_handle_t<EventType>& it) const;

private:
	template <typename EventType>
	using listeners_t = std::set<
		detail::WeakEventAction<detail::event_param_t<EventType>>
	>;

	template <typename EventType>
	struct listeners_tag
	{
		listeners_t<EventType> listeners;
	};

	mutable std::tuple<listeners_tag<EventTypes>...> _eventListeners;
};

}

#include "Event.inl"