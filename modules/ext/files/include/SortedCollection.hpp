#pragma once

#include <functional>
#include <vector>

template <typename T,
	      typename RandomAccessContainer = std::vector<T>, 
		  typename Compare = std::less<typename RandomAccessContainer::value_type>>
class SortedCollection
{
public:
	typedef typename RandomAccessContainer::const_iterator const_iterator;
	typedef typename RandomAccessContainer::const_reverse_iterator const_reverse_iterator;
	typedef typename RandomAccessContainer::value_type value_type;
	typedef typename RandomAccessContainer::size_type size_type;

	SortedCollection(const Compare& compare = Compare());

	const_iterator insert(const value_type& v);
	const_iterator insert(value_type&& v);

	const_iterator erase(const_iterator it);

	const_iterator find(const value_type& v);
	const_iterator findLargerOrSame(const value_type& v);
	const_reverse_iterator findSmallerOrSame(const value_type& v);

	void clear();

	bool empty() const;
	size_type size() const;

	const_iterator begin() const;
	const_iterator end() const;
	const_reverse_iterator rbegin() const;
	const_reverse_iterator rend() const;

	const Compare& compare() const;

private:
	RandomAccessContainer _container;
	Compare _compare;
};

#include "SortedCollection.inl"
