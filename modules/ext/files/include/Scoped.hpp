#pragma once

#include "NonCopyable.hpp"
#include <functional>


/**
 * A helper class which can be used to invoke
 * desired functionality on scope exit.
 */
template <typename ExitCallback>
class Scoped : NonCopyable
{
public:

	/**
	 * @param entry Callable object that is invoked upon object construction.
	 * @param exit Callable object that is invoked upon object destruction.
	 */
	template <typename Callable>
	Scoped(Callable&& entry, ExitCallback&& exit)
	: _atExit(std::forward<ExitCallback>(exit)),
	  _valid(true)
	{
		entry();
	}

	Scoped(Scoped<ExitCallback>&& other)
	: _atExit(std::move(other._atExit)),
	  _valid(other._valid)
	{
		other._valid = false;
	}

	~Scoped()
	{
		if (_valid) 
			_atExit();
	}

private:
	std::decay_t<ExitCallback> _atExit;
	bool _valid;
};

template <typename EntryCallback, typename ExitCallback>
Scoped<ExitCallback> make_scoped(EntryCallback&& entry, ExitCallback&& exit)
{
	return Scoped<ExitCallback>(std::forward<EntryCallback>(entry),
								std::forward<ExitCallback>(exit));
}

template <typename ExitCallback>
Scoped<ExitCallback> at_exit(ExitCallback&& exit)
{
	return Scoped<ExitCallback>([]{}, std::forward<ExitCallback>(exit));
}
