#pragma once

#include <random>

namespace pm {

namespace Random {

/** Thread local storage for random device. */
std::random_device& getDevice();

}

}
