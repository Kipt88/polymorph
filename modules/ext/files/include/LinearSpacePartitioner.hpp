#pragma once

#include <vector>

namespace pm {

/**
 * Manages merging and splitting of linear memory.
 */
class LinearSpacePartitioner
{
public:
	struct Partition
	{
		std::size_t begin;
		std::size_t end;

		std::size_t size() const;

		bool operator==(const Partition& other) const;
	};

	typedef typename std::vector<Partition>::iterator iterator;
	typedef typename std::vector<Partition>::const_iterator const_iterator;

	/**
	 * Adds partition to be managed.
	 */
	void insert(const Partition& partition);

	/**
	 * Removes and possibly splits a partition.
	 * Returns partition of size 0 if not possible.
	 */
	Partition removeSubPartition(std::size_t size);

	iterator erase(const_iterator it);

	/**
	 * Gets amount of partitions.
	 */
	std::size_t size() const;
	const_iterator begin() const;
	const_iterator end() const;

private:
	// Sorted by size.
	std::vector<Partition> _partitionBySizes;
	std::vector<Partition> _partitionByIndices;
};

}