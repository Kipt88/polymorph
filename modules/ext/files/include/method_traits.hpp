#pragma once

#include "type_list.hpp"
#include <type_traits>

#define PM_TMETHOD(m) decltype(&m), &m
#define PM_TNULL_METHOD() std::nullptr_t, nullptr

namespace pm {

template <typename M>
struct is_method : std::false_type {};

template <typename C, typename R, typename... Args>
struct is_method<R(C::*)(Args...)> : std::true_type {}; 

template <typename M>
struct method_traits;

template <typename M>
using method_class_t = typename method_traits<M>::class_t;

template <typename M>
using method_retval_t = typename method_traits<M>::retval_t;

template <typename M>
using method_params_t = typename method_traits<M>::params_t;

template <typename M>
constexpr bool is_const_method();

template <typename M, M, typename... Args>
method_retval_t<M> callTemplateMethod(void* instance, Args... args);

template <typename M, M, typename... Args>
method_retval_t<M> callTemplateMethod_const(const void* instance, Args... args);

}

#include "method_traits.inl"
