#pragma once

#if defined(_WIN32) && defined(_MSC_VER) // VS does not have experimental optional implementation yet.

// Subset of proposed std::optional.

#include <type_traits>

namespace std {

struct nullopt_t {};

constexpr nullopt_t nullopt{};

template <typename T>
class optional;

template <typename T>
bool operator==(const optional<T>& a, const optional<T>& b);

template <typename T>
bool operator!=(const optional<T>& a, const optional<T>& b);

template <typename T>
class optional
{
public:
	typedef T value_type;

	optional();
	optional(nullopt_t);
	optional(const optional& other);
	optional(optional&& other);
	optional(const T& v);
	optional(T&& v);

	~optional();

	optional& operator=(nullopt_t);
	optional& operator=(const optional& other);
	optional& operator=(optional&& other);

	template <typename U, typename = enable_if_t<is_same<T, decay_t<U>>::value>>
	optional& operator=(U&& v);

	const T* operator->() const;
	T* operator->();

	const T& operator*() const&;
	T& operator*() &;

	explicit operator bool() const;

	template <typename U>
	T value_or(U&& default_value) const&;

	template <typename U>
	T value_or(U&& default_value) &&;

private:
	aligned_storage_t<sizeof(T), alignment_of<T>::value> _storage;
	bool _active;
};

}

#include "optional.inl"

#else

#include <experimental/optional>

namespace std {

using namespace experimental;

}

#endif
