#pragma once

#include <memory>

namespace pm {

template <typename... Ts>
struct type_list;

template <typename List>
struct list_size;

template <typename List, std::size_t I>
struct list_type;

template <typename List, std::size_t I>
using list_type_t = typename list_type<List, I>::type;

}

#include "type_list.inl"
