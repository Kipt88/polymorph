#pragma once

#include <string>
#include <cstdint>
#include <cstddef>
#include <cinttypes>

#if defined(_MSC_VER)
#	pragma warning(disable:4307) // integral constant overflow
#endif

namespace pm {

typedef std::uint64_t id_t;

inline id_t filenameToId(const std::string& filename);

constexpr id_t hash(const char* str);
inline id_t hash(const std::string& str);

constexpr id_t operator "" _id(const char* str, std::size_t size);

}

#include "Id.inl"
