#pragma once

#include <chrono>

using namespace std::chrono_literals;

namespace pm {

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::system_clock SystemTime;

template <typename R, typename Duration>
R toSeconds(const Duration& duration)
{
	return std::chrono::duration_cast<std::chrono::duration<R>>(duration).count();
}

}
