#pragma once

#include "NonCopyable.hpp"

#include <memory>

template <typename T>
class UniqueFunction;

template <typename R, typename... Args>
class UniqueFunction<R(Args...)> : NonCopyable
{
public:
	template <
		typename Callable,
		typename = std::enable_if_t<
			!std::is_same<
				std::decay_t<Callable>,
				UniqueFunction<R(Args...)>
			>::value
		>
	>
	UniqueFunction(Callable&& func);

	UniqueFunction() = default;
	UniqueFunction(UniqueFunction<R(Args...)>&& other);

	UniqueFunction(UniqueFunction<R(Args...)>&) = delete;

	UniqueFunction(std::nullptr_t);

	UniqueFunction<R(Args...)>& operator=(UniqueFunction<R(Args...)>&& other) = default;

	explicit operator bool() const;

	R operator()(Args... args) const;

	void reset();

	void* implementation() const;

private:
	class CallableBase
	{
	public:
		virtual ~CallableBase() = default;

		virtual R operator()(Args...) const = 0;
		virtual void* implementation() const = 0;
	};

	template <typename Callable>
	class CallableImpl : public CallableBase
	{
	public:
		CallableImpl(Callable&& callable);

		R operator()(Args...) const override;
		void* implementation() const override;

	private:
		mutable std::decay_t<Callable> _callable;
	};

	std::unique_ptr<CallableBase> _impl;
};

#include "UniqueFunction.inl"
