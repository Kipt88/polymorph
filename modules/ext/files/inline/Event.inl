#include "Event.hpp"

#include "Assert.hpp"

namespace pm {

namespace detail {

// WeakEventAction<EventParam>

template <typename EventParam>
template <typename M, M method>
void WeakEventAction<EventParam>::set(method_class_t<M>& obj)
{
	_obj = &obj;
	_method = &callTemplateMethod<M, method, EventParam>;
}

template <typename EventParam>
void WeakEventAction<EventParam>::operator()(EventParam e) const
{
	(*_method)(_obj, e);
}

template <typename EventParam>
bool WeakEventAction<EventParam>::operator<(const WeakEventAction& other) const
{
	if (_obj < other._obj)
		return true;
	else if (other._obj < _obj)
		return false;

	return _method < other._method;
}

// WeakEventAction<void>

template <typename M, M method>
void WeakEventAction<void>::set(method_class_t<M>& obj)
{
	_obj = &obj;
	_method = &callTemplateMethod<M, method>;
}

// EventAction<EventParam>

template <typename EventParam>
template <typename Callable, typename>
EventAction<EventParam>::EventAction(Callable&& action)
: _impl(new Derived<Callable>(std::forward<Callable>(action)))
{
}

template <typename EventParam>
void EventAction<EventParam>::operator()(EventParam e) const
{
	(*_impl)(e);
}

template <typename EventParam>
bool EventAction<EventParam>::operator<(const EventAction& other) const
{
	return _impl.get() < other._impl.get();
}


template <typename EventParam>
template <typename Callable>
EventAction<EventParam>::Derived<Callable>::Derived(Callable&& action)
: _action(std::forward<Callable>(action))
{
}

template <typename EventParam>
template <typename Callable>
void EventAction<EventParam>::Derived<Callable>::operator()(EventParam e) const
{
	_action(e);
}

// EventAction<void>

template <typename Callable, typename>
EventAction<void>::EventAction(Callable&& action)
: _impl(new Derived<Callable>(std::forward<Callable>(action)))
{
}


template <typename Callable>
EventAction<void>::Derived<Callable>::Derived(Callable&& action)
: _action(std::forward<Callable>(action))
{
}

template <typename Callable>
void EventAction<void>::Derived<Callable>::operator()() const
{
	_action();
}

}

// EventHandlerScope

template <typename ThisEventType, typename... EventTypes>
EventHandlerScope<ThisEventType, EventTypes...>::EventHandlerScope()
: _handle(),
  _manager(nullptr)
{
}

template <typename ThisEventType, typename... EventTypes>
EventHandlerScope<ThisEventType, EventTypes...>::EventHandlerScope(
	const EventManager<EventTypes...>& manager, 
	const detail::handle_t<ThisEventType>& handle
)
: _handle(handle),
  _manager(&manager)
{
}

template <typename ThisEventType, typename... EventTypes>
EventHandlerScope<ThisEventType, EventTypes...>::EventHandlerScope(EventHandlerScope&& other)
: _handle(other._handle),
  _manager(other._manager)
{
	other._manager = nullptr;
}

template <typename ThisEventType, typename... EventTypes>
EventHandlerScope<ThisEventType, EventTypes...>::~EventHandlerScope()
{
	if (_manager)
		_manager->template removeListener<ThisEventType>(_handle);
}

template <typename ThisEventType, typename... EventTypes>
void EventHandlerScope<ThisEventType, EventTypes...>::reset()
{
	*this = EventHandlerScope();
}

template <typename ThisEventType, typename... EventTypes>
EventHandlerScope<ThisEventType, EventTypes...>& 
	EventHandlerScope<ThisEventType, EventTypes...>::operator=(EventHandlerScope&& other)
{
	if (_manager)
		_manager->template removeListener<ThisEventType>(_handle);

	_manager = other._manager;
	_handle = other._handle;

	other._manager = nullptr;

	return *this;
}

template <typename ThisEventType, typename... EventTypes>
bool EventHandlerScope<ThisEventType, EventTypes...>::operator<(const EventHandlerScope& other) const
{
	if (*this)
	{
		if (other)
			return *_handle < *other._handle;
		
		return false;
	}

	return other;
}

template <typename ThisEventType, typename... EventTypes>
EventHandlerScope<ThisEventType, EventTypes...>::operator bool() const
{
	return _manager != nullptr;
}

// EventManager

#ifdef _DEBUG

namespace detail {

template <typename Tuple, std::size_t I>
struct listener_size
{
	static std::size_t value(const Tuple& listeners)
	{
		return std::get<I>(listeners).listeners.size() 
			+ listener_size<Tuple, I - 1>::value(listeners);
	}
};

template <typename Tuple>
struct listener_size<Tuple, 0>
{
	static std::size_t value(const Tuple& listeners)
	{
		return std::get<0>(listeners).listeners.size();
	}
};

template <typename Tuple, std::size_t I = std::tuple_size<Tuple>::value - 1>
std::size_t listenerSize(const Tuple& listeners)
{
	return listener_size<Tuple, I>::value(listeners);
}

}

template<typename... EventTypes>
EventManager<EventTypes...>::~EventManager()
{
	// Destroying listener will call event manager, having listeners 
	// alive while destroying event manager will cause crash.

	ASSERT(
		detail::listenerSize(_eventListeners) == 0, 
		"Listeners alive when destroying event manager."
	);
}

#endif

template<typename... EventTypes>
template <typename EventType>
void EventManager<EventTypes...>::triggerEvent(const detail::event_param_t<EventType>& e)
{
	static_assert(!std::is_empty<EventType>::value,
				  "Use triggerEvent with no parameters for empty event types.");

	// Copy listeners to enable removal of listeners while triggering events.
	auto listeners = std::get<listeners_tag<EventType>>(_eventListeners).listeners;

	for (auto& listener : listeners)
		listener(e);
}

template<typename... EventTypes>
template <typename EventType>
void EventManager<EventTypes...>::triggerEvent()
{
	static_assert(std::is_empty<EventType>::value,
				  "Use triggerEvent with parameter for non-empty event types.");

	// Copy listeners to enable removal of listeners while triggering events.
	auto listeners = std::get<listeners_tag<EventType>>(_eventListeners).listeners;

	for (auto& listener : listeners)
		listener();
}


template <typename... EventTypes>
template <typename EventType, typename Callable>
typename EventManager<EventTypes...>::template scope_t<EventType>
	EventManager<EventTypes...>::addListener(Callable&& callable) const
{
	auto& listeners = std::get<listeners_tag<EventType>>(_eventListeners).listeners;
	
	auto pair = listeners.insert(std::forward<Callable>(callable));

	return scope_t<EventType>(*this, pair.first);
}

template <typename... EventTypes>
template <typename EventType>
void EventManager<EventTypes...>::removeListener(const detail::handle_t<EventType>& it) const
{
	auto& listeners = std::get<listeners_tag<EventType>>(_eventListeners).listeners;

	listeners.erase(it);
}

// WeakEventHandlerScope


template <typename ThisEventType, typename... EventTypes>
WeakEventHandlerScope<ThisEventType, EventTypes...>::WeakEventHandlerScope()
: _handle(),
  _manager(nullptr)
{
}

template <typename ThisEventType, typename... EventTypes>
WeakEventHandlerScope<ThisEventType, EventTypes...>::WeakEventHandlerScope(
	const WeakEventManager<EventTypes...>& manager, 
	const detail::weak_handle_t<ThisEventType>& handle
)
: _handle(handle),
  _manager(&manager)
{
}

template <typename ThisEventType, typename... EventTypes>
WeakEventHandlerScope<ThisEventType, EventTypes...>::WeakEventHandlerScope(WeakEventHandlerScope&& other)
: _handle(other._handle),
  _manager(other._manager)
{
	other._manager = nullptr;
}

template <typename ThisEventType, typename... EventTypes>
WeakEventHandlerScope<ThisEventType, EventTypes...>::~WeakEventHandlerScope()
{
	if (_manager)
		_manager->template removeListener<ThisEventType>(_handle);
}

template <typename ThisEventType, typename... EventTypes>
void WeakEventHandlerScope<ThisEventType, EventTypes...>::reset()
{
	*this = EventHandlerScope<ThisEventType>();
}

template <typename ThisEventType, typename... EventTypes>
WeakEventHandlerScope<ThisEventType, EventTypes...>&
	WeakEventHandlerScope<ThisEventType, EventTypes...>::operator=(WeakEventHandlerScope&& other)
{
	if (_manager)
		_manager->template removeListener<ThisEventType>(_handle);

	_manager = other._manager;
	_handle = other._handle;

	other._manager = nullptr;

	return *this;
}

template <typename ThisEventType, typename... EventTypes>
bool WeakEventHandlerScope<ThisEventType, EventTypes...>::operator<(const WeakEventHandlerScope& other) const
{
	if (*this)
	{
		if (other)
			return *_handle < *other._handle;
		
		return false;
	}

	return other;
}

template <typename ThisEventType, typename... EventTypes>
WeakEventHandlerScope<ThisEventType, EventTypes...>::operator bool() const
{
	return _manager != nullptr;
}

// WeakEventManager

#ifdef _DEBUG

template<typename... EventTypes>
WeakEventManager<EventTypes...>::~WeakEventManager()
{
	// Destroying listener will call event manager, having listeners 
	// alive while destroying event manager will cause crash.

	ASSERT(
		detail::listenerSize(_eventListeners) == 0, 
		"Listeners alive when destroying event manager."
	);
}

#endif

template<typename... EventTypes>
template <typename EventType>
void WeakEventManager<EventTypes...>::triggerEvent(const detail::event_param_t<EventType>& e)
{
	static_assert(!std::is_empty<EventType>::value,
				  "Use triggerEvent with no parameters for empty event types.");

	// Copy listeners to enable removal of listeners while triggering events.
	auto listeners = std::get<listeners_tag<EventType>>(_eventListeners).listeners;

	for (auto& listener : listeners)
		listener(e);
}

template<typename... EventTypes>
template <typename EventType>
void WeakEventManager<EventTypes...>::triggerEvent()
{
	static_assert(std::is_empty<EventType>::value,
				  "Use triggerEvent with parameter for non-empty event types.");

	// Copy listeners to enable removal of listeners while triggering events.
	auto listeners = std::get<listeners_tag<EventType>>(_eventListeners).listeners;

	for (auto& listener : listeners)
		listener();
}


template <typename... EventTypes>
template <typename EventType, typename M, M method>
typename WeakEventManager<EventTypes...>::template scope_t<EventType>
	WeakEventManager<EventTypes...>::addListener(method_class_t<M>& obj) const
{
	auto& listeners = std::get<listeners_tag<EventType>>(_eventListeners).listeners;
	
	detail::WeakEventAction<detail::event_param_t<EventType>> action;
	action.template set<M, method>(obj);

	auto pair = listeners.insert(action);

	return scope_t<EventType>(*this, pair.first);
}

template <typename... EventTypes>
template <typename EventType>
void WeakEventManager<EventTypes...>::removeListener(const detail::weak_handle_t<EventType>& it) const
{
	auto& listeners = std::get<listeners_tag<EventType>>(_eventListeners).listeners;

	listeners.erase(it);
}

}
