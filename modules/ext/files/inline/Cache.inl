#include "Cache.hpp"

#include "Assert.hpp"

#include <vector>
#include <algorithm>

namespace pm {

namespace cache_algorithms {

// ===== LRU =====

// Least used is in front of "_handles".

template <typename Handle>
void LRU<Handle>::add(const Handle& handle)
{
	_handles.push_back(handle);
}

template <typename Handle>
void LRU<Handle>::use(const Handle& handle)
{
	_handles.erase(std::find(_handles.begin(), _handles.end(), handle));
	_handles.push_back(handle);
}

template <typename Handle>
void LRU<Handle>::erase(const Handle& handle)
{
	auto it = std::find(_handles.begin(), _handles.end(), handle);

	ASSERT(it != _handles.end(), "Couldn't find key to remove.");

	_handles.erase(it);
}

template <typename Handle>
void LRU<Handle>::clear()
{
	_handles.clear();
}

template <typename Handle>
auto LRU<Handle>::begin() const -> const_iterator
{
	return _handles.begin();
}

template <typename Handle>
auto LRU<Handle>::end() const -> const_iterator
{
	return _handles.end();
}

}

// ===== handle_t =====

template <typename CacheType, typename It>
handle_t<CacheType, It>::handle_t(iterator it,
								  iterator end,
							      const CacheType& cache)
: _it(it), 
  _end(end),
  _cache(&cache)
{
}

template <typename CacheType, typename It>
auto handle_t<CacheType, It>::operator*() const -> const value_type&
{
	ASSERT(_it != _end, "Trying to get value of bad handle.");

	return *_it;
}

template <typename CacheType, typename It>
auto handle_t<CacheType, It>::operator->() const -> const value_type*
{
	ASSERT(_it != _end, "Trying to get value of bad handle.");

	return &(*_it);
}

template <typename CacheType, typename It>
bool handle_t<CacheType, It>::operator==(const handle_t& other) const
{
	if (!(*this))
		return !other;

	if (!other)
		return !(*this);

	return _it == other._it;
}

template <typename CacheType, typename It>
bool handle_t<CacheType, It>::operator!=(const handle_t& other) const
{
	return !(*this == other);
}

template <typename CacheType, typename It>
bool handle_t<CacheType, It>::operator<(const handle_t& other) const
{
	return _it < other._it;
}

template <typename CacheType, typename It>
bool handle_t<CacheType, It>::operator!() const
{
	return _it == _end;
}

template <typename CacheType, typename It>
handle_t<CacheType, It>::operator bool()
{
	return _it != _end;
}

template <typename CacheType, typename It>
void handle_t<CacheType, It>::use() const
{
	ASSERT(_it != _end, "Trying to use bad handle.");

	_cache->use(*this);
}

// ===== Cache =====

template <typename Value, template <typename> class CacheAlgorithm>
auto Cache<Value, CacheAlgorithm>::insert(const value_type& data) -> handle_t
{
	auto p = _data.insert(data);

	ASSERT(p.second, "Trying to re-add data.");
	
	handle_t handle(p.first, _data.end(), *this);

	_algorithm.add(handle);

	return handle;
}

template <typename Value, template <typename> class CacheAlgorithm>
auto Cache<Value, CacheAlgorithm>::insert(value_type&& data) -> handle_t
{
	auto p = _data.insert(std::move(data));

	ASSERT(p.second, "Trying to re-add data.");

	handle_t handle(p.first, _data.end(), *this);

	_algorithm.add(handle);

	return handle;
}

template <typename Value, template <typename> class CacheAlgorithm>
template <typename... Args>
auto Cache<Value, CacheAlgorithm>::emplace(Args&&... args) -> handle_t
{
	auto p = _data.emplace(std::forward<Args>(args)...);

	ASSERT(p.second, "Trying to re-add data.");

	handle_t handle(p.first, _data.end(), *this);

	_algorithm.add(handle);

	return handle;
}

template <typename Value, template <typename> class CacheAlgorithm>
template <typename Predicate>
auto Cache<Value, CacheAlgorithm>::find_if(Predicate&& pred) const -> handle_t
{
	auto it = std::find_if(_data.begin(), _data.end(), std::forward<Predicate>(pred));

	return handle_t(it, _data.end(), *this);
}

template <typename Value, template <typename> class CacheAlgorithm>
void Cache<Value, CacheAlgorithm>::use(const handle_t& handle) const
{
	_algorithm.use(handle);
}

template <typename Value, template <typename> class CacheAlgorithm>
template <typename Predicate>
auto Cache<Value, CacheAlgorithm>::erase_if(Predicate&& pred, 
									        std::size_t amount) -> std::size_t
{
	// Amount being zero means no limit.
	if (amount == 0)
		amount = _data.size();

	std::vector<handle_t> removedHandles;

	for (const auto& handle : _algorithm)
	{
		if (pred(*handle))
		{
			removedHandles.push_back(handle);

			if (removedHandles.size() >= amount)
				break;
		}
	}

	for (const auto& handle : removedHandles)
	{
		_algorithm.erase(handle);
		_data.erase(*handle);
	}

	return removedHandles.size();
}

template <typename Value, template <typename> class CacheAlgorithm>
template <typename Predicate>
std::size_t Cache<Value, CacheAlgorithm>::erase_while(Predicate&& pred)
{
	std::vector<Value> removedHandles;

	for (const auto& handle : _algorithm)
	{
		if (pred(*handle))
			removedHandles.push_back(handle);
		else
			break;
	}

	for (const auto& handle : removedHandles)
	{
		_algorithm.erase(handle);
		_data.erase(*handle);
	}

	return removedHandles.size();
}

template <typename Value, template <typename> class CacheAlgorithm>
void Cache<Value, CacheAlgorithm>::erase(const handle_t& handle)
{
	_algorithm.erase(handle);
	_data.erase(*handle);
}

template <typename Value, template <typename> class CacheAlgorithm>
std::size_t Cache<Value, CacheAlgorithm>::size() const
{
	return _data.size();
}

template <typename Value, template <typename> class CacheAlgorithm>
void Cache<Value, CacheAlgorithm>::clear()
{
	_algorithm.clear();
	_data.clear();
}

template <typename Value, template <typename> class CacheAlgorithm>
auto Cache<Value, CacheAlgorithm>::begin() const -> const_iterator
{
	return _algorithm.begin();
}

template <typename Value, template <typename> class CacheAlgorithm>
auto Cache<Value, CacheAlgorithm>::end() const -> const_iterator
{
	return _algorithm.end();
}

// ===== CacheMap =====

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
auto CacheMap<Key, Value, CacheAlgorithm>::insert(const value_type& data) -> handle_t
{
	auto p = _data.insert(data);

	ASSERT(p.second, "Trying to re-add data.");

	handle_t handle(p.first, _data.end(), *this);

	_algorithm.add(handle);

	return handle;
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
auto CacheMap<Key, Value, CacheAlgorithm>::insert(value_type&& data) -> handle_t
{
	auto p = _data.insert(std::move(data));

	ASSERT(p.second, "Trying to re-add data.");

	handle_t handle(p.first, _data.end(), *this);

	_algorithm.add(handle);

	return handle;
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
template <typename... Args>
auto CacheMap<Key, Value, CacheAlgorithm>::emplace(Args&&... args) -> handle_t
{
	auto p = _data.emplace(std::forward<Args>(args)...);

	ASSERT(p.second, "Trying to re-add data.");

	handle_t handle(p.first, _data.end(), *this);

	_algorithm.add(handle);

	return handle;
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
template <typename Predicate>
auto CacheMap<Key, Value, CacheAlgorithm>::find_if(Predicate&& pred) const -> handle_t
{
	auto it = std::find_if(_data.begin(), _data.end(), std::forward<Predicate>(pred));

	return handle_t(it, _data.end(), *this);
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
auto CacheMap<Key, Value, CacheAlgorithm>::find(const Key& key) const -> handle_t
{
	auto it = _data.find(key);

	return handle_t(it, _data.end(), *this);
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
void CacheMap<Key, Value, CacheAlgorithm>::use(const handle_t& handle) const
{
	_algorithm.use(handle);
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
bool CacheMap<Key, Value, CacheAlgorithm>::has(const Key& key) const
{
	return _data.find(key) != _data.end();
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
template <typename Predicate>
auto CacheMap<Key, Value, CacheAlgorithm>::erase_if(Predicate&& pred,
										 	        std::size_t amount) -> std::size_t
{
	// Amount being zero means no limit.
	if (amount == 0)
		amount = _data.size();

	std::vector<handle_t> removedHandles;

	for (const auto& handle : _algorithm)
	{
		if (pred(*handle))
		{
			removedHandles.push_back(handle);

			if (removedHandles.size() >= amount)
				break;
		}
	}

	for (const auto& handle : removedHandles)
	{
		_algorithm.erase(handle);
		_data.erase(handle->first);
	}

	return removedHandles.size();
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
template <typename Predicate>
std::size_t CacheMap<Key, Value, CacheAlgorithm>::erase_while(Predicate&& pred)
{
	std::vector<Value> removedHandles;

	for (const auto& handle : _algorithm)
	{
		if (pred(*handle))
			removedHandles.push_back(handle);
		else
			break;
	}

	for (const auto& handle : removedHandles)
	{
		_algorithm.erase(handle);
		_data.erase(*handle);
	}

	return removedHandles.size();
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
void CacheMap<Key, Value, CacheAlgorithm>::erase(const handle_t& handle)
{
	_algorithm.erase(handle);
	_data.erase(handle->first);
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
std::size_t CacheMap<Key, Value, CacheAlgorithm>::size() const
{
	return _data.size();
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
void CacheMap<Key, Value, CacheAlgorithm>::clear()
{
	_algorithm.clear();
	_data.clear();
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
auto CacheMap<Key, Value, CacheAlgorithm>::begin() const -> const_iterator
{
	return _algorithm.begin();
}

template <typename Key, typename Value, template <typename> class CacheAlgorithm>
auto CacheMap<Key, Value, CacheAlgorithm>::end() const -> const_iterator
{
	return _algorithm.end();
}

}
