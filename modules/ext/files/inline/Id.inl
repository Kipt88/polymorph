#include "Id.hpp"

#include <algorithm>

namespace pm {

namespace detail {

constexpr std::size_t stringLen(const char* str)
{
	return *str == '\0' ? 0 : 1 + stringLen(str + 1);
}

constexpr std::uintmax_t exp(std::size_t base, std::size_t e)
{
	return e == 0 ? 1 : base * exp(base, e - 1);
}

constexpr id_t hashImpl(const char* str, std::size_t e)
{
	return *str == '\0' ? 0 : exp(31, e) * *str + hashImpl(str + 1, e - 1);
}

}

constexpr id_t hash(const char* str)
{
	return detail::hashImpl(str, detail::stringLen(str) - 1);
}

inline id_t hash(const std::string& str)
{
	return detail::hashImpl(str.c_str(), detail::stringLen(str.c_str()) - 1);
}

constexpr id_t operator "" _id(const char* str, std::size_t size)
{
	return detail::hashImpl(str, size - 1);
}

inline id_t filenameToId(const std::string& filename)
{
	// Remove file extension to get id.
	auto it = std::find(filename.rbegin(), filename.rend(), '.');

	std::size_t begin = 0;
	std::size_t size = std::string::npos;

	if (it != filename.rend())
		size = filename.size() - (it - filename.rbegin()) - 1;

	it = std::find_if(filename.rbegin(), filename.rend(), [](char c) { return c == '\\' || c == '/'; });

	if (it != filename.rend())
	{
		begin = filename.size() - (it - filename.rbegin());
		size -= begin;
	}

	return hash(filename.substr(begin, size));
}

}
