#include "Endianess.hpp"

#include <utility>

inline bool isBigEndian()
{
	union
	{
		int i;
		char c[sizeof(int)];
	} temp;

	temp.i = 1;

	return temp.c[sizeof(int) - 1] == 1;
}

inline bool isLittleEndian()
{
	union
	{
		int i;
		char c[sizeof(int)];
	} temp;

	temp.i = 1;

	return temp.c[0] == 1;
}

template <typename Scalar>
inline Scalar flippedEndianess(const Scalar& val)
{
	Scalar rVal = val;
	flipEndianess(rVal);

	return rVal;
}

template <typename Scalar>
inline void flipEndianess(Scalar& val)
{
	static_assert(std::is_scalar<Scalar>::value, "Template isn't scalar.");

	flipEndianess(&val, sizeof(Scalar));
}

inline void flipEndianess(void* data, std::size_t size)
{
	unsigned char* start = reinterpret_cast<unsigned char*>(data);
	unsigned char* last = start + size - 1;

	for (std::size_t i = 0; i < size / 2; ++i)
		std::swap(*(start + i), *(last - i));
}