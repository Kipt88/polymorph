#include "Assert.hpp"

#include <algorithm>

#define INDEX_NONE ((unsigned int)-1)

namespace pm {

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::addVertex(const VertexData& data) -> vertex_index_t
{
	_vertices.push_back(vertex_t{ INDEX_NONE, data });
	return _vertices.size() - 1;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::addEdge(const EdgeData& data, vertex_index_t vertexA, vertex_index_t vertexB) -> edge_index_t
{
	_edges.push_back(edge_t{ { vertexA, vertexB },{ INDEX_NONE, INDEX_NONE }, INDEX_NONE, INDEX_NONE, INDEX_NONE, INDEX_NONE, data });
	return _edges.size() - 1;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::addFace(const FaceData& data, const std::vector<edge_index_t>& edges) -> face_index_t
{
	ASSERT(edges.size() > 2, "Trying to add edge with less than 3 edges.");
	_faces.push_back(face_t{ edges.front(), data });

	// All vertices and edges are defined for this face, build the topology.

	face_index_t faceIndex = _faces.size() - 1;

	for (unsigned int i = 0; i < edges.size(); ++i)
	{
		edge_index_t edgeIndex = edges[i];

		for (vertex_index_t vertexIndex : _edges[edgeIndex].vertices)
			_vertices[vertexIndex].edge = edgeIndex;

		edge_index_t previousEdgeIndex = i == 0 ? edges.back() : edges[i - 1];
		edge_index_t nextEdgeIndex = i == edges.size() - 1 ? edges.front() : edges[i + 1];

		edge_t& previousEdge = _edges[previousEdgeIndex];
		edge_t& edge = _edges[edgeIndex];

		// Is edge CCW in regards of current face?
		bool edgeCCW;

		if (edge.vertices[0] == previousEdge.vertices[0] || edge.vertices[0] == previousEdge.vertices[1])
		{
			edgeCCW = true;
		}
		else
		{
			ASSERT((edge.vertices[1] == previousEdge.vertices[0] || edge.vertices[1] == previousEdge.vertices[1]), "Corrupted mesh");
			edgeCCW = false;
		}

		if (edgeCCW)
		{
			ASSERT(edge.faces[0] == INDEX_NONE && edge.prevCCW == INDEX_NONE && edge.nextCCW == INDEX_NONE, "Corrupted mesh.");

			edge.faces[0] = faceIndex;
			edge.prevCCW = previousEdgeIndex;
			edge.nextCCW = nextEdgeIndex;
		}
		else
		{
			ASSERT(edge.faces[1] == INDEX_NONE && edge.prevCW == INDEX_NONE && edge.nextCW == INDEX_NONE, "Corrupted mesh.");

			edge.faces[1] = faceIndex;
			edge.prevCW = previousEdgeIndex;
			edge.nextCW = nextEdgeIndex;
		}
	}

	return faceIndex;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::vertex(vertex_index_t index) -> VertexData&
{
	return _vertices[index].data;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::edge(edge_index_t index) -> EdgeData&
{
	return _edges[index].data;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::face(face_index_t index) -> FaceData&
{
	return _faces[index].data;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::vertex(vertex_index_t index) const -> const VertexData&
{
	return _vertices[index].data;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::edge(edge_index_t index) const -> const EdgeData&
{
	return _edges[index].data;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::face(face_index_t index) const -> const FaceData&
{
	return _faces[index].data;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::vertexSize() const -> vertex_index_t
{
	return _vertices.size();
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::edgeSize() const -> edge_index_t
{
	return _edges.size();
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::faceSize() const -> face_index_t
{
	return _faces.size();
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::findEdge(vertex_index_t a, vertex_index_t b) const -> std::optional<edge_index_t>
{
	/* Doesn't work...
	const vertex_t& vA = _vertices[a];
	const vertex_t& vB = _vertices[b];

	std::array<edge_index_t, 5> candidateEdges = {
		vA.edge,
		_edges[vA.edge].nextCCW,
		_edges[vA.edge].nextCW,
		_edges[vA.edge].prevCCW,
		_edges[vA.edge].prevCW
	};

	for (auto edge : candidateEdges)
	{
		if (edge != INDEX_NONE)
		{
			if (_edges[edge].vertices[0] == a && _edges[edge].vertices[1] == b
				|| _edges[edge].vertices[0] == b && _edges[edge].vertices[1] == a)
			{
				return edge;
			}
		}
	}
	*/

	auto it = std::find_if(
		_edges.begin(),
		_edges.end(),
		[&](const edge_t& edge)
		{
			return edge.vertices[0] == a && edge.vertices[1] == b
				   || edge.vertices[0] == b && edge.vertices[1]== a;
		}
	);

	if (it != _edges.end())
		return std::distance(_edges.begin(), it);

	return std::nullopt;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::faceVertices(face_index_t index) const -> std::vector<vertex_index_t>
{
	std::vector<vertex_index_t> vertices;

	auto edges = faceEdges(index);

	for (edge_index_t i = 0; i < edges.size(); ++i)
	{
		const edge_t& edge = _edges[edges[i]];
		const edge_t& nextEdge = _edges[edges[(i + 1) % edges.size()]];

		bool firstEdgeCommon = (std::find(
			nextEdge.vertices.begin(),
			nextEdge.vertices.end(),
			edge.vertices[0]
		) != nextEdge.vertices.end());

		if (firstEdgeCommon)
		{
			vertices.push_back(edge.vertices[0]);
		}
		else
		{
			ASSERT(
				std::find(
					nextEdge.vertices.begin(), 
					nextEdge.vertices.end(), 
					edge.vertices[1]
				) != nextEdge.vertices.end(), 
				"Can't find common vertex on edge loop."
			);

			vertices.push_back(edge.vertices[1]);
		}
	}

	return vertices;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::faceEdges(face_index_t index) const -> std::vector<edge_index_t>
{
	std::vector<edge_index_t> edges;

	edge_index_t firstEdgeIndex = _faces[index].edge;
	edge_index_t edgeIndex = firstEdgeIndex;

	do
	{
		const auto& edge = _edges[edgeIndex];

		edges.push_back(edgeIndex);

		if (edge.faces[0] == index)
		{
			// Face is on left, next is CCW.
			edgeIndex = edge.nextCCW;
		}
		else
		{
			ASSERT(edge.faces[1] == index, "Corrupted mesh.");

			// Face is on right, next is CW.
			edgeIndex = edge.nextCW;
		}
	}
	while (edgeIndex != firstEdgeIndex);

	return edges;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::adjacentFaces(face_index_t index) const -> std::vector<face_index_t>
{
	std::vector<face_index_t> faces;

	edge_index_t firstEdgeIndex = _faces[index].edge;
	edge_index_t edgeIndex = firstEdgeIndex;

	do
	{
		const auto& edge = _edges[edgeIndex];

		if (edge.faces[0] == index)
		{
			// Face is on left, next is CCW.
			edgeIndex = edge.nextCCW;

			if (edge.faces[1] != INDEX_NONE)
				faces.push_back(edge.faces[1]);
		}
		else
		{
			ASSERT(edge.faces[1] == index, "Corrupted mesh.");

			// Face is on right, next is CW.
			edgeIndex = edge.nextCW;

			if (edge.faces[0] != INDEX_NONE)
				faces.push_back(edge.faces[0]);
		}
	}
	while (edgeIndex != firstEdgeIndex);

	return faces;
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::edgeFaces(edge_index_t index) const -> std::array<std::optional<face_index_t>, 2>
{
	const auto& edge = _edges[index];

	return {
		edge.faces[0] == INDEX_NONE ? std::nullopt : std::optional<face_index_t>{ edge.faces[0] },
		edge.faces[1] == INDEX_NONE ? std::nullopt : std::optional<face_index_t>{ edge.faces[1] }
	};
}

template <typename VertexData, typename EdgeData, typename FaceData>
auto WingedEdgeMesh<VertexData, EdgeData, FaceData>::edgeVertices(edge_index_t index) const -> std::array<vertex_index_t, 2>
{
	const auto& edge = _edges[index];

	return edge.vertices;
}

}

#undef INDEX_NONE
