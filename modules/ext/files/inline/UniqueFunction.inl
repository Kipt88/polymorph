#include "UniqueFunction.hpp"

#include <functional>

// UniqueFunction:

template <typename R, typename... Args>
UniqueFunction<R(Args...)>::UniqueFunction(UniqueFunction<R(Args...)>&& other)
: _impl(std::move(other._impl))
{
}

template <typename R, typename... Args>
UniqueFunction<R(Args...)>::UniqueFunction(std::nullptr_t)
: _impl(nullptr)
{
}

template <typename R, typename... Args>
template <typename Callable, typename>
UniqueFunction<R(Args...)>::UniqueFunction(Callable&& func)
: _impl(new CallableImpl<Callable>(std::forward<Callable>(func)))
{
}

template <typename R, typename... Args>
UniqueFunction<R(Args...)>::operator bool() const
{
	return _impl != nullptr;
}

template <typename R, typename... Args>
R UniqueFunction<R(Args...)>::operator()(Args... args) const
{
	if (!_impl)
		throw std::bad_function_call();

	return (*_impl)(std::forward<Args>(args)...);
}

template <typename R, typename... Args>
void UniqueFunction<R(Args...)>::reset()
{
	_impl.reset();
}

template <typename R, typename... Args>
void* UniqueFunction<R(Args...)>::implementation() const
{
	if (!_impl)
		return nullptr;

	return _impl->implementation();
}

// UniqueFunction::CallableImpl:

template <typename R, typename... Args>
template <typename Callable>
UniqueFunction<R(Args...)>::CallableImpl<Callable>::CallableImpl(Callable&& callable)
: _callable(std::forward<Callable>(callable))
{
}

template <typename R, typename... Args>
template <typename Callable>
R UniqueFunction<R(Args...)>::CallableImpl<Callable>::operator()(Args... args) const
{
	return _callable(std::forward<Args>(args)...);
}

template <typename R, typename... Args>
template <typename Callable>
void* UniqueFunction<R(Args...)>::CallableImpl<Callable>::implementation() const
{
	return &_callable;
}
