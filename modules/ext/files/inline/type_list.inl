#include "type_list.hpp"

namespace pm {

template <typename T, typename... Ts>
struct type_list<T, Ts...>
{
	typedef T type;
	typedef type_list<Ts...> next;
};

template <typename T>
struct type_list<T>
{
	typedef T type;
};

template <typename... Ts>
struct list_size<type_list<Ts...>>
{
	static constexpr std::size_t value = sizeof...(Ts);
};

namespace detail {

template <typename List, std::size_t Target, std::size_t CurrentIndex>
struct _get_type
{
	typedef typename _get_type<typename List::next, Target, CurrentIndex + 1>::type type;
};

template <typename List, std::size_t Target>
struct _get_type<List, Target, Target>
{
	typedef typename List::type type;
};

}

template <typename List, std::size_t I>
struct list_type
{
	static_assert(I < list_size<List>::value, "Out of bounds.");

	typedef typename detail::_get_type<List, I, 0>::type type;
};

}
