namespace std {

template <typename T>
bool operator==(const optional<T>& a, const optional<T>& b)
{
	if (a && b)
		return *a == *b;
	else if (!a && !b)
		return true;

	return false;
}

template <typename T>
bool operator!=(const optional<T>& a, const optional<T>& b)
{
	return !(a == b);
}

template <typename T>
optional<T>::optional()
: _storage(), _active(false)
{
}

template <typename T>
optional<T>::optional(nullopt_t)
: optional()
{
}

template <typename T>
optional<T>::optional(const optional& other)
: _storage(), _active(false)
{
	*this = other;
}

template <typename T>
optional<T>::optional(optional&& other)
: _storage(), _active(false)
{
	*this = std::move(other);
}

template <typename T>
optional<T>::optional(const T& v)
: _storage(), _active(false)
{
	*this = v;
}

template <typename T>
optional<T>::optional(T&& v)
: _storage(), _active(false)
{
	*this = std::move(v);
}

template <typename T>
optional<T>::~optional()
{
	if (_active)
	{
		reinterpret_cast<T*>(&_storage)->~T();
		_active = false;
	}
}

template <typename T>
optional<T>& optional<T>::operator=(nullopt_t)
{
	optional::~optional();

	return *this;
}

template <typename T>
optional<T>& optional<T>::operator=(const optional& other)
{
	if (other)
	{
		if (*this)
		{
			**this = *other;
		}
		else
		{
			new(&_storage) T(*other);
			_active = true;
		}
	}
	else
	{
		optional::~optional();
	}

	return *this;
}

template <typename T>
optional<T>& optional<T>::operator=(optional&& other)
{
	if (other)
	{
		if (*this)
		{
			**this = std::move(*other);
		}
		else
		{
			new(&_storage) T(std::move(*other));
			_active = true;
		}
	}
	else
	{
		optional::~optional();
	}

	return *this;
}

template <typename T>
template <typename U, typename>
optional<T>& optional<T>::operator=(U&& v)
{
	if (_active)
	{
		**this = std::forward<U>(v);
	}
	else
	{
		optional::~optional();

		new(&_storage) T(std::forward<U>(v));
		_active = true;
	}

	return *this;
}

template <typename T>
const T* optional<T>::operator->() const
{
	return reinterpret_cast<const T*>(&_storage);
}

template <typename T>
T* optional<T>::operator->()
{
	return reinterpret_cast<T*>(&_storage);
}

template <typename T>
const T& optional<T>::operator*() const&
{
	return *reinterpret_cast<const T*>(&_storage);
}

template <typename T>
T& optional<T>::operator*() &
{
	return *reinterpret_cast<T*>(&_storage);
}

template <typename T>
optional<T>::operator bool() const
{
	return _active;
}

template <typename T>
template <typename U>
T optional<T>::value_or(U&& default_value) const&
{
	return static_cast<bool>(*this) ? **this : static_cast<T>(std::forward<U>(default_value));
}

template <typename T>
template <typename U>
T optional<T>::value_or(U&& default_value) &&
{
	return static_cast<bool>(*this) ? std::move(**this) : static_cast<T>(std::forward<U>(default_value));
}

}