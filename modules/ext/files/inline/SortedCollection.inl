#include "SortedCollection.hpp"

#include <Debug.hpp>

namespace detail {

template <typename T, typename Iterator, typename Compare>
Iterator findFirstLargerOrSame(const T& v, Iterator begin, Iterator end, Compare compare)
{
	for (Iterator mid = begin + (end - begin) / 2; 
	     begin != end; 
		 mid = begin + (end - begin) / 2)
	{
		if (compare(*mid, v))
			begin = mid + 1;
		else
			end = mid;
	}

	return begin;
}

}

template <typename T, typename RAC, typename Compare>
SortedCollection<T, RAC, Compare>::SortedCollection(const Compare& compare)
: _container(),
  _compare(compare)
{
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::insert(const value_type& v) -> const_iterator
{
	return _container.insert(findLargerOrSame(v), v);
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::insert(value_type&& v) -> const_iterator
{
	return _container.insert(findLargerOrSame(v), std::move(v));
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::erase(const_iterator it) -> const_iterator
{
	return _container.erase(it);
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::find(const value_type& v) -> const_iterator
{
	const_iterator begin = _container.begin();
	const_iterator end = _container.end();

	for (const_iterator mid = begin + (end - begin) / 2;
		 begin != end;
		 mid = begin + (end - begin) / 2)
	{
		if (*mid == v)
			return mid;
		else if (_compare(*mid, v))
			begin = mid + 1;
		else
			end = mid;
	}

	return _container.end();
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::findLargerOrSame(const value_type& v) -> const_iterator
{
	return detail::findFirstLargerOrSame(
		v, 
		_container.begin(),
		_container.end(),
		_compare
	);
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::findSmallerOrSame(const value_type& v) -> const_reverse_iterator
{
	return detail::findFirstLargerOrSame(
		v,
		_container.rbegin(),
		_container.rend(),
		[this](const value_type& a, const value_type& b) { return _compare(b, a); }
	);
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::clear() -> void
{
	_container.clear();
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::empty() const -> bool
{
	return _container.empty();
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::size() const -> size_type
{
	return _container.size();
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::begin() const -> const_iterator
{
	return _container.begin();
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::end() const -> const_iterator
{
	return _container.end();
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::rbegin() const -> const_reverse_iterator
{
	return _container.rbegin();
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::rend() const -> const_reverse_iterator
{
	return _container.rend();
}

template <typename T, typename RAC, typename Compare>
auto SortedCollection<T, RAC, Compare>::compare() const -> const Compare&
{
	return _compare;
}