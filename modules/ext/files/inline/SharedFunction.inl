#include "SharedFunction.hpp"

namespace pm {

template <typename R, typename... Args>
template <typename Callable>
SharedFunction<R(Args...)>::SharedFunction(Callable&& func)
: _impl(std::make_shared<CallableImpl<Callable>>(std::forward<Callable>(func)))
{
}

template <typename R, typename... Args>
SharedFunction<R(Args...)>::SharedFunction(std::nullptr_t)
: SharedFunction()
{
}

template <typename R, typename... Args>
SharedFunction<R(Args...)>::operator bool() const
{
	return _impl != nullptr;
}

template <typename R, typename... Args>
R SharedFunction<R(Args...)>::operator()(Args&&... args)
{
	return (*_impl)(std::forward<Args>(args)...);
}


template <typename R, typename... Args>
template <typename Callable>
SharedFunction<R(Args...)>::CallableImpl<Callable>::CallableImpl(Callable&& callable)
: _callable(std::forward<Callable>(callable))
{
}

template <typename R, typename... Args>
template <typename Callable>
R SharedFunction<R(Args...)>::CallableImpl<Callable>::operator()(Args&&... args)
{
	return _callable(std::forward<Args>(args)...);
}

}