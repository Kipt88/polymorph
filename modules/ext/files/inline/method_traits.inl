#include "method_traits.hpp"

#include <utility>

namespace pm {

template <typename C, typename R, typename... Args>
struct method_traits<R(C::*)(Args...)>
{
	typedef C class_t;
	typedef R retval_t;
	typedef type_list<Args...> params_t;
	typedef std::false_type is_const_t;
};

template <typename C, typename R, typename... Args>
struct method_traits<R(C::*)(Args...) const>
{
	typedef C class_t;
	typedef R retval_t;
	typedef type_list<Args...> params_t;
	typedef std::true_type is_const_t;
};

template <>
struct method_traits<std::nullptr_t>
{
	typedef void class_t;
	typedef void retval_t;
	typedef void params_t;
	typedef std::true_type is_const_t;
};

template <typename M>
constexpr bool is_const_method()
{
	return method_traits<M>::is_const_t;
}

namespace detail {

template <typename M, M method, typename... Args>
struct _caller
{
	static method_retval_t<M> call(void* instance, Args... args)
	{
		return (static_cast<method_class_t<M>*>(instance)->*method)(args...);
	}
};

template <typename... Args>
struct _caller<std::nullptr_t, nullptr, Args...>
{
	static void call(void*, Args...)
	{
	}
};

template <typename M, M method, typename... Args>
struct _const_caller
{
	static method_retval_t<M> call(const void* instance, Args... args)
	{
		return (static_cast<const method_class_t<M>*>(instance)->*method)(args...);
	}
};

template <typename... Args>
struct _const_caller<std::nullptr_t, nullptr, Args...>
{
	static void call(const void*, Args...)
	{
	}
};

}

template <typename M, M method, typename... Args>
method_retval_t<M> callTemplateMethod(void* instance, Args... args)
{
	return detail::_caller<M, method, Args...>::call(instance, args...);
}

template <typename M, M method, typename... Args>
method_retval_t<M> callTemplateMethod_const(const void* instance, Args... args)
{
	return detail::_const_caller<M, method, Args...>::call(instance, args...);
}

}
