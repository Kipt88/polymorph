#include "Event.hpp"

namespace pm {

namespace detail {

void EventAction<void>::operator()() const
{
	(*_impl)();
}

bool EventAction<void>::operator<(const EventAction& other) const
{
	return _impl.get() < other._impl.get();
}

void WeakEventAction<void>::operator()() const
{
	(*_method)(_obj);
}

bool WeakEventAction<void>::operator<(const WeakEventAction& other) const
{
	if (_obj < other._obj)
		return true;

	return _method < other._method;
}

}

}