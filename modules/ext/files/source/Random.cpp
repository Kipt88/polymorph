#include "Random.hpp"

namespace pm {

namespace Random {

std::random_device& getDevice()
{
	static thread_local std::random_device device;

	return device;
}

}

}
