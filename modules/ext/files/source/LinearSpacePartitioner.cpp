#include "LinearSpacePartitioner.hpp"

#include "Assert.hpp"
#include <algorithm>

namespace pm {

namespace {

bool sizeCompare(const LinearSpacePartitioner::Partition& partition, std::size_t size)
{
	return partition.size() < size;
}

bool sizeCompare2(std::size_t size, const LinearSpacePartitioner::Partition& partition)
{
	return size < partition.size();
}

bool indexBeginCompare(const LinearSpacePartitioner::Partition& partition, std::size_t index)
{
	return partition.begin < index;
}

bool indexEndCompare(const LinearSpacePartitioner::Partition& partition, std::size_t index)
{
	return partition.end < index;
}

}

std::size_t LinearSpacePartitioner::Partition::size() const
{
	return end - begin;
}

bool LinearSpacePartitioner::Partition::operator==(const Partition& other) const
{
	return begin == other.begin && end == other.end;
}

auto LinearSpacePartitioner::insert(const Partition& partition) -> void
{
	iterator it = _partitionByIndices.insert(std::lower_bound(
			_partitionByIndices.begin(),
			_partitionByIndices.end(),
			partition.begin,
			indexBeginCompare
		),
		partition
	);

	iterator nextIt = std::next(it);
	
	if (nextIt != _partitionByIndices.end() && it->end == nextIt->begin)
	{
		// Merge with upper.
		it->end = nextIt->end;
		erase(nextIt);
	}
	
	iterator prevIt = _partitionByIndices.end();
	if (it != _partitionByIndices.begin())
		prevIt = std::prev(it);

	if (prevIt != _partitionByIndices.end() && prevIt->end == it->begin)
	{
		// Merge with lower.
		auto prevIt = std::prev(it);

		it->begin = prevIt->begin;
		it = erase(prevIt);
	}

	_partitionBySizes.insert(
		std::lower_bound(
			_partitionBySizes.begin(),
			_partitionBySizes.end(),
			it->size(),
			sizeCompare
		),
		*it
	);
}

auto LinearSpacePartitioner::removeSubPartition(std::size_t size) -> Partition
{
	ASSERT(size > 0, "Expected greater minimum partition size.");

	auto it = std::lower_bound(
		_partitionBySizes.begin(),
		_partitionBySizes.end(),
		size,
		sizeCompare
	);

	if (it == _partitionBySizes.end())
		return Partition{ 0, 0 };


	Partition requestedPartition(*it);

	std::size_t extraSize = it->size() - size;

	_partitionBySizes.erase(it);

	{
		auto it = std::lower_bound(
			_partitionByIndices.begin(),
			_partitionByIndices.end(),
			requestedPartition.begin,
			indexBeginCompare
		);

		ASSERT(it != _partitionByIndices.end(), "Can find partition in sizes but not by indices.");

		_partitionByIndices.erase(it);
	}

	// Partition left small sub-partition, add it back and merge if possible.
	if (extraSize > 0)
	{
		requestedPartition.end -= extraSize;

		Partition paddingPartition;
		paddingPartition.begin = requestedPartition.end;
		paddingPartition.end = paddingPartition.begin + extraSize;

		insert(paddingPartition);
	}

	return requestedPartition;
}

auto LinearSpacePartitioner::erase(const_iterator it) -> iterator
{
	auto sizeRange = std::make_pair(
		std::lower_bound(
			_partitionBySizes.begin(),
			_partitionBySizes.end(),
			it->size(),
			sizeCompare
		),
		std::upper_bound(
			_partitionBySizes.begin(),
			_partitionBySizes.end(),
			it->size(),
			sizeCompare2
		)
	);

	auto sizeIt = std::find(sizeRange.first, sizeRange.second, *it);
	ASSERT(sizeIt != sizeRange.second, "Can't find size.");

	_partitionBySizes.erase(sizeIt);
	return _partitionByIndices.erase(it);
}

auto LinearSpacePartitioner::size() const -> std::size_t
{
	return _partitionByIndices.size();
}

auto LinearSpacePartitioner::begin() const -> const_iterator
{
	return _partitionByIndices.begin();
}

auto LinearSpacePartitioner::end() const -> const_iterator
{
	return _partitionByIndices.end();
}

}