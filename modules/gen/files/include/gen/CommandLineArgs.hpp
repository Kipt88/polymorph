#pragma once

#include <vector>
#include <string>
#include <utility>

namespace pm { namespace gen {

class CommandLineArgs
{
public:
	template <typename Callable>
	static void parse(int argc, char* argv[], Callable&& action);

private:
	static std::pair<std::string, std::string> tokenize(const std::string& kv);
};

} }

#include "gen/CommandLineArgs.inl"
