#pragma once

#include <Exception.hpp>
#include <filesystem.hpp>
#include <vector>
#include <string>

namespace pm { namespace gen { 

namespace Indexer
{
	void generateHeader(
		const std::filesystem::path& headerOut,
		const std::string& nspace,
		const std::vector<std::filesystem::path>& resources,
		const std::filesystem::path& resourceRoot,
		bool forceExport
	);
}

} }