#include "gen/CommandLineArgs.hpp"

namespace pm { namespace gen {

template <typename Callable>
void CommandLineArgs::parse(int argc, char* argv[], Callable&& action)
{
	for (int i = 1; i < argc; ++i)
	{
		auto kv = tokenize(argv[i]);
		action(kv.first, kv.second);
	}
}

} }
