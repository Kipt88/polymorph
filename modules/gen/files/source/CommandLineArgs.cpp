#include "gen/CommandLineArgs.hpp"

#include <algorithm>

namespace pm { namespace gen {

std::pair<std::string, std::string> CommandLineArgs::tokenize(const std::string& kv)
{
	auto index = kv.find('=');

	if (index == std::string::npos)
		return std::make_pair(kv, "");
	
	return std::make_pair(kv.substr(0, index), kv.substr(index + 1));
}

} }
