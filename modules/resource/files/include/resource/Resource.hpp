#pragma once

#include <Id.hpp>

namespace pm { namespace resource {

struct Resource
{
	id_t id;
	const char* path;
};

} }
