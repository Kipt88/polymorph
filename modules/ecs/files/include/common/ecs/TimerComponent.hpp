#pragma once

#include "ecs/Systems.hpp"
#include <UniqueFunction.hpp>

namespace pm { namespace ecs {

class TimerComponent : Unique
{
public:
	TimerComponent(
		UpdateSystem& updateSystem, 
		const Time::duration& time = Time::duration::zero()
	);

	template <typename Callable>
	TimerComponent(
		UpdateSystem& updateSystem, 
		const Time::duration& time, 
		Callable&& onComplete
	);

	void reset(const Time::duration& time);

	template <typename Callable>
	void reset(const Time::duration& time, Callable&& onComplete);

	const Time::duration& timeLeft() const;

	void update(const Time::duration& dt);

private:
	Time::duration _timer;

	UniqueFunction<void()> _onComplete;

	UpdateSystem::UpdaterScope _scope;
};

} 

PM_MAKE_COMPONENT_ID(ecs::TimerComponent);

}

#include "ecs/TimerComponent.inl"
