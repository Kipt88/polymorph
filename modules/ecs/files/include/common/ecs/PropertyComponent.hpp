#pragma once

#include "ecs/ComponentId.hpp"
#include <Event.hpp>
#include <NonCopyable.hpp>
#include <functional>

namespace pm { namespace ecs {

template <typename Property>
struct PropertyChanged
{
	PropertyChanged() = delete;

	std::reference_wrapper<const Property> prop;
};

template <typename Property>
class PropertyComponent : Unique
{
public:
	typedef EventManager<PropertyChanged<Property>> PropertyEventManager;
	typedef EventHandlerScope<PropertyChanged<Property>, PropertyChanged<Property>> EventScope;

	PropertyComponent() = delete;

	template <typename R>
	PropertyComponent(R&& v);

	template <typename R>
	void set(R&& v);

	const Property& get() const;

	template <typename Callable>
	EventScope addListener(Callable&& callable) const;

private:
	Property _prop;

	mutable PropertyEventManager _eventManager;
};

typedef PropertyComponent<int> int_prop_t;
typedef PropertyComponent<unsigned int> uint_prop_t;

} 

PM_MAKE_COMPONENT_ID(ecs::int_prop_t);
PM_MAKE_COMPONENT_ID(ecs::uint_prop_t);

}

#include "ecs/PropertyComponent.inl"
