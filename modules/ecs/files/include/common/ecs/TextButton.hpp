#pragma once

#include "ecs/Button.hpp"
#include <gr2d/MeshFont2.hpp>
#include <string>

namespace pm { namespace ecs {

namespace TextButton {

class Interface_const : public Button::Interface_const
{
public:
	using Button::Interface_const::Interface_const;

	ecs::component_reference<const std::string> text() const;
};

class Interface : public Button::Interface
{
public:
	using Button::Interface::Interface;

	ecs::component_reference<std::string> text() const;
	void setText(const std::string& text) const;
	void setText(std::string&& text) const;

	operator Interface_const() const;
};

Interface create(
	ecs::entity_handle_t entity,
	const Camera& uiCamera,
	RenderSystem::render_category_t type,
	const gr::MeshFont2& font,
	int renderOrder = 0
);

}

} }
