#pragma once

#include <Id.hpp>
#include <gr/Mesh.hpp>

#include <memory>

#define PM_MAKE_COMPONENT_ID(clazz) \
	template <> \
	struct pm::ecs::component_id<std::remove_const_t<clazz>> { \
		static constexpr id_t value = #clazz""_id; \
		static constexpr const char* name = #clazz""; \
	}

namespace pm { namespace ecs {

template <typename ComponentType>
struct component_id;


// Common components:
PM_MAKE_COMPONENT_ID(gr::MeshHandle);
PM_MAKE_COMPONENT_ID(id_t);
PM_MAKE_COMPONENT_ID(bool);
PM_MAKE_COMPONENT_ID(int);
PM_MAKE_COMPONENT_ID(unsigned int);


struct null_component_reference_t {};
constexpr null_component_reference_t null_component_reference{};

template <typename T>
class opt_component_reference
{
public:
	static_assert(component_id<std::remove_const_t<T>>::name != nullptr, "Fooo");
		
	typedef T type;

	opt_component_reference() noexcept : _ptr(nullptr) {}
	opt_component_reference(null_component_reference_t) noexcept : _ptr(nullptr) {}
	opt_component_reference(T* ptr) noexcept : _ptr(ptr) {}
	opt_component_reference(T& ref) noexcept : _ptr(std::addressof(ref)) {}
	opt_component_reference(T&&) = delete;
	opt_component_reference(const opt_component_reference&) noexcept = default;

	opt_component_reference& operator=(const opt_component_reference&) noexcept = default;

	operator T&() const noexcept { return *_ptr; }
	T& get() const noexcept { return *_ptr; }

	operator opt_component_reference<const T>&() const noexcept { return *_ptr; }
	operator bool() const noexcept { return _ptr != nullptr; }

private:
	T* _ptr;
};

template <typename T>
class component_reference
{
public:
	typedef T type;

	component_reference(T& ref) noexcept : _ptr(std::addressof(ref)) {}
	component_reference(T&&) = delete;
	component_reference(const component_reference&) noexcept = default;

	component_reference& operator=(const component_reference&) noexcept = default;

	operator T&() const noexcept { return *_ptr; }
	T& get() const noexcept { return *_ptr; }

	operator opt_component_reference<T>&() const noexcept { return *_ptr; }
	
	/*
	template <typename = std::enable_if_t<!std::is_const<T>::value>>
	operator component_reference<const T>&() const noexcept { return *_ptr; }

	template <typename = std::enable_if_t<!std::is_const<T>::value>>
	operator opt_component_reference<const T>&() const noexcept { return *_ptr; }
	*/
private:
	T* _ptr;
};

template <typename T, typename U>
bool operator==(const opt_component_reference<T>& a, const opt_component_reference<U>& b)
{
	return a.get() == b.get();
}

template <typename T, typename U>
bool operator==(const component_reference<T>& a, const opt_component_reference<U>& b)
{
	return a.get() == b.get();
}

template <typename T, typename U>
bool operator==(const component_reference<T>& a, const component_reference<U>& b)
{
	return a.get() == b.get();
}

template <typename T, typename U>
bool operator!=(const opt_component_reference<T>& a, const opt_component_reference<U>& b)
{
	return !(a == b);
}

template <typename T, typename U>
bool operator!=(const component_reference<T>& a, const opt_component_reference<U>& b)
{
	return !(a == b);
}

template <typename T, typename U>
bool operator!=(const component_reference<T>& a, const component_reference<U>& b)
{
	return !(a == b);
}

} }
