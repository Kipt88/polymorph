#pragma once

#include "ecs/HitBox.hpp"
#include "ecs/Scene.hpp"
#include "ecs/Systems.hpp"
#include "ecs/Transform.hpp"
#include <gr/Mesh.hpp>
#include <method_traits.hpp>

namespace pm { namespace ecs {

namespace Button {

class Interface_const : public const_entity_handle_t
{
public:
	explicit Interface_const(const_entity_handle_t handle);

	component_reference<const HitBox> hitbox() const;
	component_reference<const Transform2> transform() const;
	component_reference<const gr::MeshHandle> mesh() const;

	const gr::AABox2& getBounds() const;
};

class Interface : public entity_handle_t
{
public:
	explicit Interface(ecs::entity_handle_t handle);

	component_reference<HitBox> hitbox() const;
	component_reference<Transform2> transform() const;
	component_reference<const gr::MeshHandle> mesh() const;

	const gr::AABox2& getBounds() const;

	void setTransform(const Transform2& trans) const;
	void setMesh(gr::MeshHandle mesh) const;

	template <typename M, M method>
	void setDelegate(method_class_t<M>& del) const;

	template <typename P, typename M, M method>
	void setDelegateWithContext(method_class_t<M>& del, P& param) const;

	operator Interface_const() const;
};

Interface create(
	ecs::entity_handle_t handle,
	const Camera& uiCamera,
	RenderSystem::render_category_t type,
	int renderOrder = 0
);

}

} }

#include "ecs/Button.inl"