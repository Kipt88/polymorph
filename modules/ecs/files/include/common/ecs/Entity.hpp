#pragma once

#include "ecs/Component.hpp"
#include "ecs/Systems.hpp"

#include <NonCopyable.hpp>
#include <Id.hpp>
#include <map>
#include <vector>

namespace pm { namespace ecs {

class Entity : NonCopyable
{
public:
	Entity() = default;
	Entity(Entity&&) = default;
	~Entity();

	template <typename ComponentType, typename... CtorArgs>
	ComponentType& add(CtorArgs&&... ctorArgs);

	template <typename ComponentType>
	ComponentType& get();

	template <typename ComponentType>
	const ComponentType& get() const;

	template <typename ComponentType>
	bool has() const;

	template <typename ComponentType>
	void remove();

	void clear();

	// Used for debugging.
	const std::map<id_t, Component>& components() const;

private:
	std::map<id_t, Component> _components;
	std::vector<std::map<id_t, Component>::iterator> _insertionOrder;
};

} }

#include "ecs/Entity.inl"
