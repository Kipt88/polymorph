#pragma once

#include "ecs/ComponentId.hpp"
#include <gr/types.hpp>

namespace pm { namespace ecs {

typedef gr::Vector2_r Vector2_r;
typedef gr::Transform2 LocalTransform2;

class Transform2
{
public:
	explicit Transform2(const LocalTransform2& transform = LocalTransform2::IDENTITY);
	explicit Transform2(const Transform2& parent, const LocalTransform2& local = LocalTransform2::IDENTITY);

	LocalTransform2& local();
	const LocalTransform2& local() const;

	LocalTransform2 world() const;

	void setParent(const Transform2& transform);

private:
	const Transform2* _parent;
	LocalTransform2 _local;
};

} 

PM_MAKE_COMPONENT_ID(ecs::Transform2);

}
