#pragma once

#include "ecs/PointerSystem.hpp"
#include "ecs/Component.hpp"
#include "ecs/RenderSystem.hpp"
#include "ecs/Transform.hpp"

namespace pm { namespace ecs {

class HitBox
{
public:
	explicit HitBox(
		PointerSystem& pointerSystem,
		component_reference<const Transform2> transform,
		component_reference<const Camera> camera
	);

	const gr::AABox2& getBounds() const;

	void setBounds(const gr::AABox2& box);
	void setBounds(gr::real_t w, gr::real_t h);

	bool inBounds(const gr::Vector2_r& worldPos) const;

	template <
		typename OnHitDown, OnHitDown onHitDown,
		typename OnHitUp, OnHitUp onHitUp,
		typename D
	>
	void setDelegate(D& del);
	
	template <
		typename OnHitDown, OnHitDown onHitDown,
		typename OnHitUp, OnHitUp onHitUp,
		typename D,
		typename P
	>
	void setDelegate(D& del, P& param);

	void resetDelegate();

private:
	void onPointerDown(const gr::Vector2_r& pos);
	void onPointerUp(const gr::Vector2_r& pos);

private:
	struct Delegate
	{
	public:
		Delegate();

		void callOnHitDown();
		void callOnHitUp();

		operator bool() const;

		using void_method_const_void_ptr_t = void(*)(void*, const void*);

		void* _obj;
		const void* _param;

		void_method_const_void_ptr_t _onHitDown;
		void_method_const_void_ptr_t _onHitUp;
	};

	Delegate _delegator;

	PointerSystem::HandlerScope _scope;
	gr::AABox2 _localBounds;
	ecs::component_reference<const Transform2> _transform;
};

PM_MAKE_COMPONENT_ID(ecs::HitBox);

} }

#include "ecs/HitBox.inl"
