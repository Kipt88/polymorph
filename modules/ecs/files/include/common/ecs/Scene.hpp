#pragma once

#include "ecs/Systems.hpp"
#include "ecs/Entity.hpp"
#include "ecs/ComponentId.hpp"

#include <NonCopyable.hpp>

#include <vector>
#include <functional>

namespace pm { namespace ecs {

namespace detail {

struct EntityData
{
	unsigned int childCount;
	std::list<EntityData>::iterator parent;
	Entity entity;
};

}

class Scene;
class const_entity_handle_t;


bool operator==(const const_entity_handle_t& a, const const_entity_handle_t& b);
bool operator!=(const const_entity_handle_t& a, const const_entity_handle_t& b);

typedef std::list<detail::EntityData> entity_storage_t;

class const_entity_handle_t
{
public:
	friend class Scene;
	friend bool operator==(const const_entity_handle_t&, const const_entity_handle_t&);

	explicit const_entity_handle_t(const Scene& scene, const entity_storage_t::const_iterator& it);
	const_entity_handle_t(const const_entity_handle_t&) = default;

	const Entity& entity() const;

	unsigned int childrenSize() const;
	const_entity_handle_t child(unsigned int index) const;

	bool hasParent() const;
	const_entity_handle_t parent() const;

	const Systems& systems() const;

private:
	entity_storage_t::const_iterator _it;
	std::reference_wrapper<const Scene> _scene;
};

class entity_handle_t
{
public:
	friend class Scene;

	explicit entity_handle_t(Scene& scene, const entity_storage_t::iterator& it);
	entity_handle_t(const entity_handle_t&) = default;

	Entity& entity() const;

	unsigned int childrenSize() const;
	entity_handle_t child(unsigned int index) const;

	bool hasParent() const;
	entity_handle_t parent() const;

	entity_handle_t createChild() const;
	void remove() const;
	void removeChildren() const;
	void removeChild(unsigned int index) const;

	Systems& systems() const;

	operator const_entity_handle_t() const;

private:
	entity_storage_t::iterator _it;
	std::reference_wrapper<Scene> _scene;
};

class Scene : Unique
{
public:
	friend class const_entity_handle_t;
	friend class entity_handle_t;

	explicit Scene(os::AppSystem& app, gr::Graphics& graphics, physics2d::Physics& physics);
	~Scene();

	entity_handle_t create();
	entity_handle_t create(entity_handle_t parent);

	void remove(entity_handle_t handle);
	void removeChildren(entity_handle_t handle);

	void update(const concurrency::FrameTime& time);
	void clear();

	Systems& systems();
	const Systems& systems() const;

	void dbgPrintHierarchy() const;

private:
	Systems _systems;
	entity_storage_t _entities;
};

PM_MAKE_COMPONENT_ID(ecs::entity_handle_t);
PM_MAKE_COMPONENT_ID(ecs::const_entity_handle_t);

} }
