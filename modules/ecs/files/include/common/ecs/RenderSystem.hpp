#pragma once

#include "ecs/SystemScope.hpp"

#include <gr/Graphics.hpp>
#include <os/App.hpp>
#include <gr/Frame.hpp>
#include <NonCopyable.hpp>

#include <memory>
#include <list>
#include <numeric>
#include <functional>

namespace pm { namespace ecs {

class Camera;

class RenderSystem : Unique
{
public:
	typedef std::uint32_t render_mask_t;
	typedef std::uint32_t render_category_t;

private:
	struct Renderable
	{
		using func_render_t = void(*)(const void*, gr::ClientFrame&);
		using func_renderType_t = render_category_t(*)(const void*);

		const void* obj;
		func_render_t render;
		func_renderType_t renderType;
	};

	typedef std::list<
		std::pair<
			const int, 
			const Renderable
		>
	> renderable_storage_t;

	typedef renderable_storage_t::const_iterator renderable_handle_t;

	typedef std::list<
		std::pair<
			const int, 
			std::reference_wrapper<const Camera>
		>
	> camera_storage_t;

	typedef camera_storage_t::const_iterator camera_handle_t;

	friend class SystemScope<RenderSystem, renderable_handle_t>;
	friend class SystemScope<RenderSystem, camera_handle_t>;

	void remove(renderable_handle_t handle);
	void remove(camera_handle_t handle);

public:
	typedef SystemScope<RenderSystem, renderable_handle_t> RenderableScope;
	typedef SystemScope<RenderSystem, camera_handle_t> CameraScope;

	explicit RenderSystem(gr::Graphics& graphics);

	template <
		typename Render, Render methodRender,
		typename RenderType, RenderType methodRenderType,
		typename T
	>
	RenderableScope add(const T& renderable, int order = 0);
	CameraScope add(const Camera& camera, int order = 0);

	void update(const concurrency::FrameTime&) const;

private:
	camera_storage_t _cameras;
	renderable_storage_t _renderables;

	gr::Graphics& _graphics;
};

} }

#include "ecs/RenderSystem.inl"
