#pragma once

#include <os/App.hpp>
#include <gr/Graphics.hpp>
#include <physics2d/Physics.hpp>
#include <ecs/Scene.hpp>

namespace pm { namespace ecs {

class Game : Unique
{
public:
	static constexpr const char* name = "App";

	explicit Game(
		concurrency::RunnerTasks<Game>& tasks,
		os::AppSystem& app,
		gr::Graphics& graphics,
		physics2d::Physics& physics
	);

	bool update(const concurrency::FrameTime& time);

	ecs::Scene& scene();
	ecs::entity_handle_t state();
	ecs::entity_handle_t newState();

	void quit();
	
	template <typename Callable>
	void pushAction(Callable&& action)
	{
		_tasks.access().pushAction(std::forward<Callable>(action));
	}

private:
	concurrency::RunnerTasks<Game>& _tasks;
	ecs::Scene _scene;

	os::AppEventScope<os::window_events::Quit> _quitScope;

	ecs::entity_handle_t _state;

	bool _quitFlag;
	UniqueFunction<void()> _stateRemover;
};

} }
