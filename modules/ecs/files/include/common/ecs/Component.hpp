#pragma once

#include "ecs/ComponentId.hpp"

#include <NonCopyable.hpp>
#include <Id.hpp>
#include <memory>

namespace pm { namespace ecs {

class Component : NonCopyable
{
public:
	template <typename ComponentType, typename... CtorArgs>
	Component(ComponentType*, CtorArgs&&... args);

	Component(Component&&) = default;

	template <typename ComponentType>
	ComponentType& as();

	template <typename ComponentType>
	const ComponentType& as() const;

	inline void release();

	inline const char* typeName() const;

private:
	struct Base
	{
		virtual ~Base() = default;
		virtual const char* typeName() const = 0;
	};

	template <typename ComponentType>
	struct Derived : Base
	{
		template <typename... CtorArgs>
		Derived(CtorArgs&&... args);

		const char* typeName() const override;

		ComponentType _component;
	};

	std::unique_ptr<Base> _impl;
};


template <typename ComponentType, typename... CtorArgs>
Component make_component(CtorArgs&&... args);

} }

#include "ecs/Component.inl"
