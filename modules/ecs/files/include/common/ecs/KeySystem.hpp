#pragma once

#include "ecs/SystemScope.hpp"
#include "ecs/Component.hpp"

#include <os/App.hpp>
#include <os/window_events.hpp>

#include <concurrency/TaskQueue.hpp>
#include <concurrency/FrameTime.hpp>

#include <NonCopyable.hpp>

#include <list>
#include <functional>
#include <tuple>

namespace pm { namespace ecs {

class KeySystem : Unique
{
public:
	struct Handler
	{
		using func_key_down_t = void(*)(void*, os::key_t);
		using func_key_up_t = void(*)(void*, os::key_t);

		void* obj;
		func_key_down_t keyDown;
		func_key_up_t keyUp;
	};

	typedef std::list<Handler> listeners_storage_t;

	typedef listeners_storage_t::const_iterator handle_t;
	typedef concurrency::TaskQueue<void(listeners_storage_t&)> InputQueue;
	typedef SystemScope<KeySystem, handle_t> HandlerScope;

public:
	explicit KeySystem(os::AppSystem& app);

	template <
		typename OnDown, OnDown methodOnDown,
		typename OnUp, OnUp methodOnUp,
		typename T
	>
	HandlerScope add(T& handler);

	void update(const concurrency::FrameTime&);

private:
	friend class SystemScope<KeySystem, handle_t>;

	void remove(handle_t handle);

	listeners_storage_t _handlers;

	InputQueue _tasks;

	std::tuple<
		os::AppEventScope<os::window_events::KeyDown>,
		os::AppEventScope<os::window_events::KeyUp>
	> _inputScope;
};

} }

// TODO Move to .inl-file.

namespace pm { namespace ecs {

template <
	typename OnDown, OnDown methodOnDown,
	typename OnUp, OnUp methodOnUp,
	typename T
>
KeySystem::HandlerScope KeySystem::add(T& handler)
{
	Handler h;

	h.obj = &handler;
	h.keyDown = &callTemplateMethod<OnDown, methodOnDown, os::key_t>;
	h.keyUp   = &callTemplateMethod<OnUp, methodOnUp, os::key_t>;

	auto it = _handlers.insert(_handlers.end(), h);

	return HandlerScope(*this, it);
}

} }
