#pragma once

#include "ecs/SystemScope.hpp"

#include <os/App.hpp>

#include <concurrency/FrameTime.hpp>
#include <concurrency/TaskQueue.hpp>

#include <Time.hpp>
#include <method_traits.hpp>

#include <list>
#include <set>
#include <functional>

namespace pm { namespace ecs {

class UpdateSystem : Unique
{
private:
	struct Updater
	{
		using func_update_t = void(*)(void*, const Time::duration&);

		void* obj;
		func_update_t update;

		bool operator==(const Updater& other) const;
	};

	typedef std::list<Updater> updater_storage_t;
	typedef updater_storage_t::iterator updater_handle_t;

	friend class SystemScope<UpdateSystem, updater_handle_t>;

	void remove(updater_handle_t handle);

public:
	typedef SystemScope<UpdateSystem, updater_handle_t> UpdaterScope;

	explicit UpdateSystem(os::AppSystem& app);

	template <typename M, M method>
	UpdaterScope add(method_class_t<M>& updater);

	template <typename EventType, typename Callable>
	os::AppEventScope<EventType> addWindowEventListener(Callable&& action);

	void update(const concurrency::FrameTime& time);

private:
	os::AppSystem& _app;

	updater_storage_t _updaters;
	concurrency::TaskQueue<void()> _events;
};

} }

#include "ecs/UpdateSystem.inl"
