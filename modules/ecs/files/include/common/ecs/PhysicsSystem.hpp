#pragma once

#include "ecs/SystemScope.hpp"
#include "ecs/Transform.hpp"
#include <physics2d/Physics.hpp>

namespace pm { namespace ecs {

class PhysicsSystem
{
private:
	struct Delegate
	{
		using func_updateState_t = void(*)(void*, const physics2d::BodyState&);
		using func_onCollision_t = void(*)(void*, const physics2d::CollisionData&);

		void* obj;
		func_updateState_t updateState;
		func_onCollision_t onCollision;
	};

	typedef std::map<physics2d::body_id_t, Delegate> delegate_storage_t;
	typedef physics2d::body_id_t delegate_handle_t;

public:
	class DelegateScope : private SystemScope<PhysicsSystem, delegate_handle_t>
	{
	public:
		explicit DelegateScope(PhysicsSystem& system, delegate_handle_t handle);

		void setPosition(const Vector2_r& pos);
		void setRotation(gr::real_t angle);
		void setVelocity(const Vector2_r& v);
		void applyForce(const Vector2_r& force, const Vector2_r& localPos = Vector2_r::ZERO);
		void applyImpulse(const Vector2_r& impulse, const Vector2_r& localPos = Vector2_r::ZERO);

		template <typename Action>
		void withBody(Action&& action);
	};

	explicit PhysicsSystem(physics2d::Physics& physics);

	void update(const concurrency::FrameTime& time);

	template <
		typename UpdateState, UpdateState,
		typename OnCollision, OnCollision,
		typename T
	>
	DelegateScope add(T& del, const physics2d::BodyDefinition& definition);

	std::optional<physics2d::CastHit> castLine(
		const physics2d::Vector2_r& p0,
		const physics2d::Vector2_r& p1,
		const physics2d::CollisionFilter& filter
	) const;

	void setGravity(const ecs::Vector2_r& v);
	void setDamping(gr::real_t d);

private:
	friend class DelegateScope;
	friend class SystemScope<PhysicsSystem, delegate_handle_t>;

	void remove(delegate_handle_t handle);

	physics2d::Physics& _physics;
	delegate_storage_t _delegates;
	physics2d::ClientFrame _nextFrame;
	physics2d::State _lastState;
	
	physics2d::body_id_t _counter;
};

} }

#include "ecs/PhysicsSystem.inl"