#pragma once

#include "ecs/SystemScope.hpp"
#include "ecs/Component.hpp"

#include <os/App.hpp>
#include <os/window_events.hpp>

#include <concurrency/TaskQueue.hpp>
#include <concurrency/FrameTime.hpp>

#include <NonCopyable.hpp>

#include <list>
#include <functional>
#include <tuple>

namespace pm { namespace ecs {

class GamepadSystem : Unique
{
public:
	struct Handler
	{
		using func_connect = void(*)(void*);
		using func_disconnect = void(*)(void*);
		using func_key_down_t = void(*)(void*, os::GamepadButton);
		using func_key_up_t = void(*)(void*, os::GamepadButton);
		using func_axis = void(*)(void*, unsigned int, const gr::Vector2_r&, const gr::Vector2_r&);

		void* obj;
		func_connect connect;
		func_disconnect disconnect;
		func_key_down_t keyDown;
		func_key_up_t keyUp;
		func_axis axis;

		unsigned int padId;
	};

	typedef std::list<Handler> listeners_storage_t;

	typedef listeners_storage_t::const_iterator handle_t;
	typedef concurrency::TaskQueue<void(listeners_storage_t&)> InputQueue;
	typedef SystemScope<GamepadSystem, handle_t> HandlerScope;

public:
	explicit GamepadSystem(os::AppSystem& app);

	template <
		typename OnConnect, OnConnect methodOnConnect,
		typename OnDisconnect, OnDisconnect methodOnDisconnect,
		typename OnKeyDown, OnKeyDown methodOnKeyDown,
		typename OnKeyUp, OnKeyUp methodOnKeyUp,
		typename OnAxis, OnAxis methodOnAxis,
		typename T
	>
	HandlerScope add(T& handler, unsigned int padId);

	void update(const concurrency::FrameTime&);

private:
	friend class SystemScope<GamepadSystem, handle_t>;

	void remove(handle_t handle);

	listeners_storage_t _handlers;

	InputQueue _tasks;

	std::tuple<
		os::AppEventScope<os::window_events::GamepadConnect>,
		os::AppEventScope<os::window_events::GamepadDisconnect>,
		os::AppEventScope<os::window_events::GamepadKeyDown>,
		os::AppEventScope<os::window_events::GamepadKeyUp>,
		os::AppEventScope<os::window_events::GamepadAxis>
	> _inputScope;
};

} }

// TODO Move to .inl-file.

namespace pm { namespace ecs {

template <
	typename OnConnect, OnConnect methodOnConnect,
	typename OnDisconnect, OnDisconnect methodOnDisconnect,
	typename OnKeyDown, OnKeyDown methodOnKeyDown,
	typename OnKeyUp, OnKeyUp methodOnKeyUp,
	typename OnAxis, OnAxis methodOnAxis,
	typename T
>
GamepadSystem::HandlerScope GamepadSystem::add(T& handler, unsigned int padId)
{
	Handler h;

	h.obj = &handler;
	h.padId = padId;

	h.connect    = &callTemplateMethod<OnConnect, methodOnConnect>;
	h.disconnect = &callTemplateMethod<OnDisconnect, methodOnDisconnect>;
	h.keyDown    = &callTemplateMethod<OnKeyDown, methodOnKeyDown, os::GamepadButton>;
	h.keyUp      = &callTemplateMethod<OnKeyUp, methodOnKeyUp, os::GamepadButton>;
	h.axis       = &callTemplateMethod<OnAxis, methodOnAxis, unsigned int, const gr::Vector2_r&, const gr::Vector2_r&>;

	auto it = _handlers.insert(_handlers.end(), h);

	return HandlerScope(*this, it);
}

} }
