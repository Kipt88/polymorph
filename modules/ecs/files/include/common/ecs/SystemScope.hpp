#pragma once

#include <NonCopyable.hpp>
#include <type_traits>

namespace pm { namespace ecs {

template <typename System, typename Handle>
class SystemScope : NonCopyable
{
public:
	SystemScope();
	SystemScope(System& system, const Handle& handle);
	SystemScope(SystemScope&& other);
	~SystemScope();

	explicit operator bool() const;

	void remove();

	SystemScope<System, Handle>& operator=(SystemScope&& other);

protected:
	System* _system;
	Handle _handle;
};

} }

#include "ecs/SystemScope.inl"
