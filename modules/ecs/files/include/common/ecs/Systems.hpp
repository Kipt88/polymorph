#pragma once

#include "ecs/RenderSystem.hpp"
#include "ecs/PointerSystem.hpp"
#include "ecs/KeySystem.hpp"
#include "ecs/GamepadSystem.hpp"
#include "ecs/UpdateSystem.hpp"
#include "ecs/PhysicsSystem.hpp"

#include <gr/Graphics.hpp>
#include <os/App.hpp>
#include <physics2d/Physics.hpp>
#include <Time.hpp>
#include <tuple>

namespace pm { namespace ecs {

class Systems
{
public:
	explicit Systems(
		os::AppSystem& app,
		gr::Graphics& graphics,
		physics2d::Physics& physics
	);

	void update(const concurrency::FrameTime& time);

	// ECS systems:
	RenderSystem& renderSystem();
	const RenderSystem& renderSystem() const;

	PointerSystem& pointerSystem();
	const PointerSystem& pointerSystem() const;

	KeySystem& keySystem();
	const KeySystem& keySystem() const;

	GamepadSystem& gamepadSystem();
	const GamepadSystem& gamepadSystem() const;

	UpdateSystem& updateSystem();
	const UpdateSystem& updateSystem() const;

	PhysicsSystem& physicsSystem();
	const PhysicsSystem& physicsSystem() const;

	// Engine systems:
	os::AppSystem& app();
	const os::AppSystem& app() const;

	gr::Graphics& graphics();
	const gr::Graphics& graphics() const;

	physics2d::Physics& physics();
	const physics2d::Physics& physics() const;

	gr::Resources& graphicsResources();
	physics2d::BodyProvider& physicsResources();

private:
	os::AppSystem&      _app;
	gr::Graphics&       _graphics;
	physics2d::Physics& _physics;

	UpdateSystem  _updateSystem;
	PointerSystem _pointerSystem;
	KeySystem     _keySystem;
	GamepadSystem _gamepadSystem;
	RenderSystem  _renderSystem;
	PhysicsSystem _physicsSystem;
};

} }
