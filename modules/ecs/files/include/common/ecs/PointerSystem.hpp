#pragma once

#include "ecs/SystemScope.hpp"
#include "ecs/RenderSystem.hpp"
#include "ecs/Camera.hpp"
#include "ecs/Component.hpp"

#include <gr/types.hpp>
#include <os/App.hpp>
#include <concurrency/TaskQueue.hpp>
#include <NonCopyable.hpp>
#include <method_traits.hpp>

#include <list>
#include <functional>
#include <tuple>

namespace pm { namespace ecs {

class PointerSystem : Unique
{
public:
	struct Handler
	{
		using func_pointer_down_t = void(*)(void*, const gr::Vector2_r&);
		using func_pointer_up_t = void(*)(void*, const gr::Vector2_r&);
		using func_pointer_move_t = void(*)(void*, const gr::Vector2_r&, const gr::Vector2_r&);
		using func_pointer_drag_t = void(*)(void*, const gr::Vector2_r&, const gr::Vector2_r&);
		using func_pointer_zoom_t = void(*)(void*, gr::real_t);

		void* obj;
		func_pointer_down_t pointerDown;
		func_pointer_up_t pointerUp;
		func_pointer_move_t pointerMove;
		func_pointer_drag_t pointerDrag;
		func_pointer_zoom_t pointerZoom;
	};

	typedef std::list<
		std::pair<
			Handler,
			component_reference<const Camera>
		>
	> listeners_storage_t;

	typedef listeners_storage_t::const_iterator handle_t;
	typedef concurrency::TaskQueue<void(listeners_storage_t&)> InputQueue;
	typedef SystemScope<PointerSystem, handle_t> HandlerScope;

public:
	explicit PointerSystem(os::AppSystem& app);

	template <
		typename OnDown, OnDown methodOnDown,
		typename OnUp, OnUp methodOnUp,
		typename OnMove, OnMove methodOnMove,
		typename OnDrag, OnDrag methodOnDrag,
		typename OnZoom, OnZoom methodOnZoom,
		typename T
	>
	HandlerScope add(
		T& handler, 
		component_reference<const Camera> camera
	);

	void update(const concurrency::FrameTime&);

private:
	friend class SystemScope<PointerSystem, handle_t>;

	void remove(handle_t handle);

	listeners_storage_t _handlers;

	InputQueue _tasks;

	std::tuple<
		os::AppEventScope<os::window_events::PointerDown>,
		os::AppEventScope<os::window_events::PointerUp>,
		os::AppEventScope<os::window_events::PointerZoom>,
		os::AppEventScope<os::window_events::PointerMove>,
		os::AppEventScope<os::window_events::PointerDrag>
	> _inputScope;
};

} }

#include "ecs/PointerSystem.inl"
