#pragma once

#include "ecs/RenderSystem.hpp"
#include "ecs/Transform.hpp"
#include "ecs/ComponentId.hpp"

#include <gr/types.hpp>
#include <gr2d/MeshFont2.hpp>

#include <string>

namespace pm { namespace ecs {

class StringRenderer : Unique
{
public:
	enum class Alignment
	{
		LEFT, CENTER, RIGHT
	};

	StringRenderer(
		RenderSystem& renderSystem,
		component_reference<const std::string> text,
		component_reference<const Transform2> trans,
		RenderSystem::render_category_t type,
		const gr::MeshFont2& font,
		Alignment alignment = Alignment::LEFT,
		const gr::Vector2_r& pivot = { 0.0_r, 0.5_r },
		int order = 0
	);

	void render(gr::ClientFrame& frame) const;
	RenderSystem::render_category_t renderType() const;

	bool isActive() const;
	void setActive(bool active = true);

	Alignment getAlignment() const;
	void setAlignment(Alignment alignment);

	gr::Vector2_r getPivot() const;
	void setPivot(const gr::Vector2_r& pivot);

private:
	RenderSystem::RenderableScope _scope;

	component_reference<const std::string> _text;
	component_reference<const Transform2> _trans;

	RenderSystem::render_category_t _type;

	gr::MeshFont2 _font;
	Alignment _alignment;
	gr::Vector2_r _pivot;

	bool _active;
};

PM_MAKE_COMPONENT_ID(ecs::StringRenderer);
PM_MAKE_COMPONENT_ID(std::string);

} }
