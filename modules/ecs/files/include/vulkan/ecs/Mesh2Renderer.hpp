#pragma once

#include "ecs/RenderSystem.hpp"
#include "ecs/Transform.hpp"
#include "ecs/ComponentId.hpp"

#include <gr/Mesh.hpp>
#include <gr/types.hpp>
#include <NonCopyable.hpp>

#include <functional>

namespace pm { namespace ecs {

class Mesh2Renderer : Unique
{
public:
	template <typename Impl>
	explicit Mesh2Renderer(
		RenderSystem& renderSystem,
		Impl&& impl,
		component_reference<const gr::MeshHandle> mesh,
		component_reference<const Transform2> trans,
		RenderSystem::render_category_t type,
		int order = 0
	);

	void render(gr::ClientFrame& frame) const;
	RenderSystem::render_category_t renderType() const;

	bool& active();
	const bool& active() const;

public:
	component_reference<const gr::MeshHandle> mesh;
	component_reference<const Transform2> trans;

private:
	typedef std::function<
		void (gr::Surface&, const gr::Matrix4x4_r&, gr::MeshHandle)
	> renderer_type;

	RenderSystem::RenderableScope _scope;

	renderer_type _impl;

	RenderSystem::render_category_t _type;

	bool _active;
};

PM_MAKE_COMPONENT_ID(ecs::Mesh2Renderer);

} }

#include "ecs/Mesh2Renderer.inl"
