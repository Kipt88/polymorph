#pragma once

#include "ecs/RenderSystem.hpp"
#include "ecs/Transform.hpp"
#include "ecs/ComponentId.hpp"

#include <gr/Mesh.hpp>
#include <gr/Color.hpp>
#include <gr/types.hpp>
#include <optional.hpp>

namespace pm { namespace ecs {

class Camera
{
public:
	Camera(
		RenderSystem& renderSystem, 
		ecs::component_reference<const Transform2> trans,
		RenderSystem::render_mask_t mask,
		const gr::Projection& projection,
		const gr::Color& bgColor,
		int order = 0
	);

	Camera(
		RenderSystem& renderSystem, 
		ecs::component_reference<const Transform2> trans,
		RenderSystem::render_mask_t mask,
		const gr::Projection& projection,
		int order = 0
	);

	Camera(
		RenderSystem& renderSystem, 
		ecs::component_reference<const Transform2> trans,
		RenderSystem::render_mask_t mask,
		const gr::Projection& projection,
		const gr::Color& bgColor,
		gr::MeshHandle bgImage,
		int order = 0
	);

	Camera(
		RenderSystem& renderSystem, 
		ecs::component_reference<const Transform2> trans,
		RenderSystem::render_mask_t mask,
		const gr::Projection& projection,
		gr::MeshHandle bgImage,
		int order = 0
	);

	void setBackgroundColor(const gr::Color& color);

	void setClipArea(const math::AxisAlignedBox2<unsigned int>& clipArea);
	void resetClipArea();

	void startFrame(gr::ClientFrame& frame) const;
	void endFrame(gr::ClientFrame& frame) const;
	
	RenderSystem::render_mask_t mask() const;

	gr::Projection& projection();
	const gr::Projection& projection() const;
	const Transform2& transformation() const;

private:
	RenderSystem::CameraScope _scope;

	component_reference<const Transform2> _trans;

	RenderSystem::render_mask_t _mask;

	gr::Projection _projection;

	gr::MeshHandle _bgImage;
	std::optional<gr::Color> _bgColor;
	std::optional<math::AxisAlignedBox2<unsigned int>> _clipArea;
};

PM_MAKE_COMPONENT_ID(ecs::Camera);

} }
