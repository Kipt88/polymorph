#include "ecs/Mesh2Renderer.hpp"

namespace pm { namespace ecs {

template <typename Impl>
Mesh2Renderer::Mesh2Renderer(
	RenderSystem& renderSystem,
	Impl&& impl,
	component_reference<const gr::MeshHandle> mesh_,
	component_reference<const Transform2> trans_,
	RenderSystem::render_category_t renderType,
	int order
)
: mesh(mesh_),
  trans(trans_), 
  _scope(renderSystem.add<
	  PM_TMETHOD(Mesh2Renderer::render), 
	  PM_TMETHOD(Mesh2Renderer::renderType)
  >(*this, order)),
  _impl(std::forward<Impl>(impl)),
  _type(renderType),
  _active(true)
{
}

} }
