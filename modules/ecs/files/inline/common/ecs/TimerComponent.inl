#include "ecs/TimerComponent.hpp"

namespace pm { namespace ecs {

template <typename Callable>
TimerComponent::TimerComponent(
	UpdateSystem& updateSystem,
	const Time::duration& time,
	Callable&& onComplete
)
: _timer(time),
  _onComplete(std::forward<Callable>(onComplete)),
  _scope(updateSystem.add<PM_TMETHOD(TimerComponent::update)>(*this))
{
}

template <typename Callable>
void TimerComponent::reset(const Time::duration& time, Callable&& onComplete)
{
	_timer = time;
	_onComplete = std::forward<Callable>(onComplete);
}

} }
