#include "ecs/PropertyComponent.hpp"

namespace pm { namespace ecs {

template <typename Property>
template <typename R>
PropertyComponent<Property>::PropertyComponent(R&& v)
: _prop(std::forward<R>(v))
{
}

template <typename Property>
template <typename R>
void PropertyComponent<Property>::set(R&& v)
{
	_prop = std::forward<R>(v);
	_eventManager.triggerEvent<PropertyChanged<Property>>({ std::cref(_prop) });
}

template <typename Property>
const Property& PropertyComponent<Property>::get() const
{
	return _prop;
}

template <typename Property>
template <typename Callable>
auto PropertyComponent<Property>::addListener(Callable&& callable) const -> EventScope
{
	return _eventManager.addListener<PropertyChanged<Property>>(
		std::forward<Callable>(callable)
	);
}

} }
