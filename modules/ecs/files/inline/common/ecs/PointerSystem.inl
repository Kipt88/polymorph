#include "ecs/PointerSystem.hpp"

namespace pm { namespace ecs {

template <
	typename OnDown, OnDown methodOnDown,
	typename OnUp, OnUp methodOnUp,
	typename OnMove, OnMove methodOnMove,
	typename OnDrag, OnDrag methodOnDrag,
	typename OnZoom, OnZoom methodOnZoom,
	typename T
>
auto PointerSystem::add(
	T& handler, 
	component_reference<const Camera> camera
) -> HandlerScope
{
	Handler h;

	h.obj = &handler;
	h.pointerDown = &callTemplateMethod<OnDown, methodOnDown, const gr::Vector2_r&>;
	h.pointerUp   = &callTemplateMethod<OnUp,   methodOnUp,   const gr::Vector2_r&>;
	h.pointerMove = &callTemplateMethod<OnMove, methodOnMove, const gr::Vector2_r&, const gr::Vector2_r&>;
	h.pointerDrag = &callTemplateMethod<OnDrag, methodOnDrag, const gr::Vector2_r&, const gr::Vector2_r&>;
	h.pointerZoom = &callTemplateMethod<OnZoom, methodOnZoom, gr::real_t>;

	auto it = _handlers.insert(_handlers.end(), std::make_pair(h, camera));

	return HandlerScope(*this, it);
}

} }
