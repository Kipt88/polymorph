#include "ecs/SystemScope.hpp"

#include <Assert.hpp>
#include <utility>

namespace pm { namespace ecs {

template <typename System, typename Handle>
SystemScope<System, Handle>::SystemScope()
: _system(nullptr),
  _handle()
{
}

template <typename System, typename Handle>
SystemScope<System, Handle>::SystemScope(System& system, const Handle& handle)
: _system(&system),
  _handle(handle)
{
}

template <typename System, typename Handle>
SystemScope<System, Handle>::SystemScope(SystemScope&& other)
: _system(other._system),
  _handle(std::move(other._handle))
{
	other._system = nullptr;
}

template <typename System, typename Handle>
SystemScope<System, Handle>::~SystemScope()
{
	if (_system)
		_system->remove(_handle);
}

template <typename System, typename Handle>
SystemScope<System, Handle>::operator bool() const
{
	return _system != nullptr;
}

template <typename System, typename Handle>
void SystemScope<System, Handle>::remove()
{
	ASSERT(_system != nullptr, "Trying to remove handle but it's already removed.");
	_system->remove(_handle);

	_system = nullptr;
}

template <typename System, typename Handle>
SystemScope<System, Handle>& SystemScope<System, Handle>::operator=(SystemScope&& other)
{
	if (_system)
		_system->remove(_handle);

	_system = other._system;
	_handle = other._handle;

	other._system = nullptr;

	return *this;
}

} }