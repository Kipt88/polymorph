#include "ecs/HitBox.hpp"

#include <method_traits.hpp>

namespace pm { namespace ecs {

namespace {

template <typename M, M method>
void callMethodIgnoreParam(void* instance, const void*)
{
	callTemplateMethod<M, method>(instance);
}

template <typename M, M method, typename P>
void callMethodWithParam(void* instance, const void* p)
{
	callTemplateMethod<M, method>(instance, *static_cast<P*>(const_cast<void*>(p)));
}

}

template <
	typename OnHitDown, OnHitDown onHitDown,
	typename OnHitUp, OnHitUp onHitUp,
	typename D
>
void HitBox::setDelegate(D& del)
{
	_delegator._obj = &del;
	_delegator._param = nullptr;
	_delegator._onHitDown = &callMethodIgnoreParam<OnHitDown, onHitDown>;
	_delegator._onHitUp = &callMethodIgnoreParam<OnHitUp, onHitUp>;
}

template <
	typename OnHitDown, OnHitDown onHitDown,
	typename OnHitUp, OnHitUp onHitUp,
	typename D,
	typename P
>
void HitBox::setDelegate(D& del, P& p)
{
	_delegator._obj = &del;
	_delegator._param = &p;
	_delegator._onHitDown = &callMethodWithParam<OnHitDown, onHitDown, P>;
	_delegator._onHitUp = &callMethodWithParam<OnHitUp, onHitUp, P>;
}

} }

