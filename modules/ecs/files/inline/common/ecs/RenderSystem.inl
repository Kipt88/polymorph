#include "ecs/RenderSystem.hpp"

#include <method_traits.hpp>

namespace pm { namespace ecs {

template <
	typename Render, Render methodRender,
	typename RenderType, RenderType methodRenderType,
	typename T
>
auto RenderSystem::add(const T& renderable, int order) -> RenderableScope
{
	Renderable r;

	r.obj = &renderable;
	r.render = &callTemplateMethod_const<Render, methodRender, gr::ClientFrame&>;
	r.renderType = &callTemplateMethod_const<RenderType, methodRenderType>;

	renderable_handle_t handle = _renderables.insert(
		std::find_if(
			_renderables.begin(),
			_renderables.end(),
			[&](const std::pair<const int, const Renderable>& a)
			{
				return a.first > order;
			}
		),
		std::make_pair(order, r)
	);

	return RenderableScope(*this, handle);
}

} } 
