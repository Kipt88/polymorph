#include "ecs/Component.hpp"

#include <Assert.hpp>

namespace pm { namespace ecs {
	
template <typename ComponentType, typename... CtorArgs>
Component::Component(ComponentType*, CtorArgs&&... args)
: _impl(std::make_unique<Derived<ComponentType>>(std::forward<CtorArgs>(args)...))
{
}

template <typename ComponentType>
ComponentType& Component::as()
{
	return static_cast<Derived<ComponentType>*>(_impl.get())->_component;
}

template <typename ComponentType>
const ComponentType& Component::as() const
{
	return static_cast<Derived<ComponentType>*>(_impl.get())->_component;
}

inline void Component::release()
{
	_impl.reset();
}

inline const char* Component::typeName() const
{
	return _impl->typeName();
}

template <typename ComponentType>
template <typename... CtorArgs>
Component::Derived<ComponentType>::Derived(CtorArgs&&... args)
: _component{ std::forward<CtorArgs>(args)... }
{
}

template <typename ComponentType>
const char* Component::Derived<ComponentType>::typeName() const
{
	return component_id<ComponentType>::name;
}

template <typename ComponentType, typename... CtorArgs>
Component make_component(CtorArgs&&... args)
{
	return Component(static_cast<ComponentType*>(nullptr), std::forward<CtorArgs>(args)...);
}

} }
