#include "ecs/PhysicsSystem.hpp"

#include <method_traits.hpp>

namespace pm { namespace ecs {

template <typename Action>
void PhysicsSystem::DelegateScope::withBody(Action&& action)
{
	_system->_nextFrame.withBody(
		_handle,
		std::forward<Action>(action)
	);
}

template <
	typename UpdateState, UpdateState updateState,
	typename OnCollision, OnCollision onCollision,
	typename T
>
auto PhysicsSystem::add(T& del, const physics2d::BodyDefinition& definition) -> DelegateScope
{
	auto id = _counter++;

	_nextFrame.createBody(id, definition, nullptr);

	Delegate d;

	d.obj = &del;
	d.updateState = &callTemplateMethod<UpdateState, updateState, const physics2d::BodyState&>;
	d.onCollision = &callTemplateMethod<OnCollision, onCollision, const physics2d::CollisionData&>;

	_delegates.insert(std::make_pair(id, d));

	return DelegateScope(*this, id);
}

} }
