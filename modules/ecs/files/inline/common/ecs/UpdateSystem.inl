#include "ecs/UpdateSystem.hpp"

namespace pm { namespace ecs {

template <typename M, M method>
auto UpdateSystem::add(method_class_t<M>& updater) -> UpdaterScope
{
	Updater up;

	up.obj = &updater;
	up.update = &callTemplateMethod<M, method, const Time::duration&>;

	auto it = _updaters.insert(_updaters.end(), up);

	return UpdaterScope(*this, it);
}

template <typename EventType, typename Callable>
os::AppEventScope<EventType> UpdateSystem::addWindowEventListener(Callable&& action)
{
	// NOTE Will only work for non-empty events, do some enable_if magic.

	return _app.addWindowEventListener<EventType>(
		[this, action_ = std::forward<Callable>(action)](const EventType& e)
		{
			auto events = _events.access();

			events.pushAction(
				[e, action_]() mutable
				{
					action_(e);
				}
			);
		}
	);
}

} }
