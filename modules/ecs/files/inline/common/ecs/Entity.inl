#include "ecs/Entity.hpp"

#include <Assert.hpp>
#include <algorithm>

namespace pm { namespace ecs {

template <typename ComponentType, typename... CtorArgs>
ComponentType& Entity::add(CtorArgs&&... ctorArgs)
{
	auto itPair = _components.insert(
		std::make_pair(
			static_cast<id_t>(component_id<ComponentType>::value),
			make_component<ComponentType>(
				std::forward<CtorArgs>(ctorArgs)...
			)
		)
	);

	ASSERT(itPair.second, "Trying to overwrite component.");

	DEBUG_OUT("Entity", "Adding component: %s", static_cast<const char*>(component_id<ComponentType>::name));

	_insertionOrder.push_back(itPair.first);

	return itPair.first->second.template as<ComponentType>();
}

template <typename ComponentType>
ComponentType& Entity::get()
{
	auto it = _components.find(static_cast<id_t>(component_id<ComponentType>::value));
	ASSERT(it != _components.end(), "Can't find component.");

	return it->second.template as<ComponentType>();
}

template <typename ComponentType>
const ComponentType& Entity::get() const
{
	auto it = _components.find(static_cast<id_t>(component_id<ComponentType>::value));
	ASSERT(it != _components.end(), "Can't find component.");

	return it->second.template as<ComponentType>();
}

template <typename ComponentType>
bool Entity::has() const
{
	return _components.find(static_cast<id_t>(component_id<ComponentType>::value)) != _components.end();
}

template <typename ComponentType>
void Entity::remove()
{
	DEBUG_OUT("Entity", "Removing component: %s", static_cast<const char*>(component_id<ComponentType>::name));

	auto it = _components.find(static_cast<id_t>(component_id<ComponentType>::value));
	ASSERT(it != _components.end(), "Can't find component.");

	auto insertIt = std::find(_insertionOrder.begin(), _insertionOrder.end(), it);
	ASSERT(insertIt != _insertionOrder.end(), "Can't find insertion order of component.");

	_insertionOrder.erase(insertIt);
	_components.erase(it);
}

} }
