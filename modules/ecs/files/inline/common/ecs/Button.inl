#include "ecs/Button.hpp"

namespace pm { namespace ecs {

namespace Button {

template <typename M, M method>
void Interface::setDelegate(method_class_t<M>& del) const
{
	auto& hitbox = entity().get<HitBox>();
	hitbox.setDelegate<method_class_t<M>, std::nullptr_t, M, nullptr, method>(del);
}

template <typename P, typename M, M method>
void Interface::setDelegateWithContext(method_class_t<M>& del, P& param) const
{
	auto& hitbox = entity().get<HitBox>();
	hitbox.setDelegate<method_class_t<M>, P, std::nullptr_t, M, nullptr, method>(del, param);
}

}

} }
