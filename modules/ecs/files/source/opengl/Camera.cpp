#include "ecs/Camera.hpp"

#include <gr/TextureRenderer.hpp>

namespace pm { namespace ecs {

Camera::Camera(
	RenderSystem& renderSystem,
	ecs::component_reference<const Transform2> trans,
	RenderSystem::render_mask_t mask,
	const gr::Projection& projection,
	const gr::Color& bgColor,
	int order
)
: _scope(renderSystem.add(*this, order)),
  _trans(trans),
  _mask(mask),
  _projection(projection),
  _bgImage(nullptr),
  _bgColor(bgColor),
  _clipArea(std::nullopt)
{
}

Camera::Camera(
	RenderSystem& renderSystem,
	ecs::component_reference<const Transform2> trans,
	RenderSystem::render_mask_t mask,
	const gr::Projection& projection,
	int order
)
: _scope(renderSystem.add(*this, order)),
  _trans(trans),
  _mask(mask),
  _projection(projection),
  _bgImage(nullptr),
  _bgColor(std::nullopt),
  _clipArea(std::nullopt)
{
}

Camera::Camera(
	RenderSystem& renderSystem,
	ecs::component_reference<const Transform2> trans,
	RenderSystem::render_mask_t mask,
	const gr::Projection& projection,
	const gr::Color& bgColor,
	gr::MeshHandle bgImage,
	int order
)
: _scope(renderSystem.add(*this, order)),
  _trans(trans),
  _mask(mask),
  _projection(projection),
  _bgImage(bgImage),
  _bgColor(bgColor),
  _clipArea(std::nullopt)
{
}

Camera::Camera(
	RenderSystem& renderSystem,
	ecs::component_reference<const Transform2> trans,
	RenderSystem::render_mask_t mask,
	const gr::Projection& projection,
	gr::MeshHandle bgImage,
	int order
)
: _scope(renderSystem.add(*this, order)),
  _trans(trans),
  _mask(mask),
  _projection(projection),
  _bgImage(bgImage),
  _bgColor(std::nullopt),
  _clipArea(std::nullopt)
{
}

void Camera::setBackgroundColor(const gr::Color& color)
{
	_bgColor = color;
}

void Camera::setClipArea(const math::AxisAlignedBox2<unsigned int>& clipArea)
{
	_clipArea = clipArea;
}

void Camera::resetClipArea()
{
	_clipArea = std::nullopt;
}

void Camera::startFrame(gr::ClientFrame& frame) const
{
	frame.push(
		[
		  bgImage = _bgImage,
		  clearColor = _bgColor, 
		  clipArea = _clipArea,
		  projection = _projection,
		  mvTrans = static_cast<gr::Matrix4x4_r>(_trans.get().world().inverse())
		]
		(gr::Surface& surface)
		{
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glEnable(GL_BLEND);
			glEnable(GL_TEXTURE_2D);
			glDisable(GL_DEPTH_TEST);

			if (clipArea)
			{
				glEnable(GL_SCISSOR_TEST);

				const auto& botLeft = clipArea->min();

				glScissor(botLeft[0], botLeft[1], clipArea->width(), clipArea->height());
			}

			if (clearColor)
			{
				glClearColor(clearColor->r, clearColor->g, clearColor->b, clearColor->a);
				glClear(GL_COLOR_BUFFER_BIT);
			}


			if (bgImage)
			{
				// TODO Set scale.
				auto rect = surface.getViewport();

				auto meshW = bgImage->bounds().width();
				auto meshH = bgImage->bounds().height();

				auto scaleX = 2.0_r / meshW;
				auto scaleY = 2.0_r / meshH;

				gr::Transform2 meshTrans = gr::Transform2::IDENTITY;

				meshTrans.scale = scaleX > scaleY ? scaleX : scaleY;

				glMatrixMode(GL_PROJECTION);
				glLoadIdentity();

				glMatrixMode(GL_MODELVIEW);
				glLoadIdentity();

				gr::TextureRenderer::render(
					surface,
					meshTrans,
					bgImage
				);
			}

			glMatrixMode(GL_PROJECTION);
			glLoadMatrixf(projection.data());

			glMatrixMode(GL_MODELVIEW);
			glLoadMatrixf(mvTrans.data());
		}
	);
}

void Camera::endFrame(gr::ClientFrame& frame) const
{
	frame.push(
		[clipArea = _clipArea](gr::Surface& surface)
		{
			glDisable(GL_BLEND);
			glEnable(GL_DEPTH_TEST);

			if (clipArea)
				glDisable(GL_SCISSOR_TEST);
		}
	);
}

gr::Projection& Camera::projection()
{
	return _projection;
}

const gr::Projection& Camera::projection() const
{
	return _projection;
}

const Transform2& Camera::transformation() const
{
	return _trans.get();
}

RenderSystem::render_mask_t Camera::mask() const
{
	return _mask;
}

} }
