#include "ecs/StringRenderer.hpp"

#include <gr/TextureRenderer.hpp>
#include <vector>
#include <numeric>

namespace pm { namespace ecs {

namespace {

auto widths(const std::vector<std::vector<gr::MeshHandle>>& lines)
{
	std::vector<gr::real_t> lineWidths;
	lineWidths.reserve(lines.size());

	gr::real_t maxWidth = 0.0f;

	for (const auto& line : lines)
	{
		gr::real_t width = std::accumulate(
			line.begin(),
			line.end(),
			0.0_r,
			[&](gr::real_t w, gr::MeshHandle mesh)
			{
				return w + mesh->bounds().width();
			}
		);

		lineWidths.push_back(width);
		maxWidth = std::max(maxWidth, width);
	}

	return std::make_pair(std::move(lineWidths), maxWidth);
}

}

StringRenderer::StringRenderer(
	RenderSystem& renderSystem,
	component_reference<const std::string> text,
	component_reference<const Transform2> trans,
	RenderSystem::render_category_t type,
	const gr::MeshFont2& font,
	Alignment alignment,
	const gr::Vector2_r& pivot,
	int order
)
: _scope(renderSystem.add<
	PM_TMETHOD(StringRenderer::render), 
	PM_TMETHOD(StringRenderer::renderType)
  >(*this, order)),
  _text(text),
  _trans(trans),
  _type(type),
  _font(font),
  _alignment(alignment),
  _pivot(pivot),
  _active(true)
{
}

void StringRenderer::render(gr::ClientFrame& frame) const
{
	if (!_active)
		return;

	auto lines = _font.getLines(_text.get());
	auto size = widths(lines);

	frame.push(
		[
			lines = std::move(lines),
			lineHeight = _font.fontHeight(),
			lineWidths = std::move(size.first),
			textWidth = size.second,
			textHeight = _font.fontHeight() * lines.size(),
			alignment = _alignment,
			relativePivot = _pivot,
			trans = _trans.get().world()
		]
		(gr::Surface& surface)
		{
			auto lock = surface.activate();

			gr::Vector2_r pivot = { 
				(0.5_r + relativePivot[0]) * textWidth, 
				relativePivot[1] * textHeight
			};

			for (unsigned int i = 0; i < lines.size(); ++i)
			{
				gr::Vector2_r offset;

				switch (alignment)
				{
				case Alignment::LEFT:
					offset[0] = 0.0_r;
					break;

				case Alignment::CENTER:
					offset[0] = 0.5_r * (textWidth - lineWidths[i]);
					break;

				case Alignment::RIGHT:
					offset[0] = textWidth - lineWidths[i];
					break;
				}

				offset[1] = lineHeight * -static_cast<gr::real_t>(i);

				gr::real_t w = 0.0_r;

				for (const auto& mesh : lines[i])
				{
					auto charBounds = mesh->bounds();

					auto charTrans = gr::Transform2::createTranslation(
						-pivot 
						+ offset 
						+ gr::Vector2_r{ w + 0.5_r * charBounds.width(), 0.5_r * charBounds.height() }
					);

					w += charBounds.width();

					gr::TextureRenderer::render(
						surface,
						(trans * charTrans).asAffineMatrix3<false>(),
						mesh
					);
				}
			}
		}
	);
}

RenderSystem::render_category_t StringRenderer::renderType() const
{
	return _type;
}

bool StringRenderer::isActive() const
{
	return _active;
}

void StringRenderer::setActive(bool active)
{
	_active = active;
}

StringRenderer::Alignment StringRenderer::getAlignment() const
{
	return _alignment;
}

void StringRenderer::setAlignment(Alignment alignment)
{
	_alignment = alignment;
}

gr::Vector2_r StringRenderer::getPivot() const
{
	return _pivot;
}

void StringRenderer::setPivot(const gr::Vector2_r& pivot)
{
	_pivot = pivot;
}

} }
