#include "ecs/Camera.hpp"

#include <gr/TextureRenderer.hpp>

namespace pm { namespace ecs {

Camera::Camera(
	RenderSystem& renderSystem,
	ecs::component_reference<const Transform2> trans,
	RenderSystem::render_mask_t mask,
	const gr::Projection& projection,
	const gr::Color& bgColor,
	int order
)
: _scope(renderSystem.add(*this, order)),
  _trans(trans),
  _mask(mask),
  _projection(projection),
  _bgImage(nullptr),
  _bgColor(bgColor),
  _clipArea(std::nullopt)
{
}

Camera::Camera(
	RenderSystem& renderSystem,
	ecs::component_reference<const Transform2> trans,
	RenderSystem::render_mask_t mask,
	const gr::Projection& projection,
	int order
)
: _scope(renderSystem.add(*this, order)),
  _trans(trans),
  _mask(mask),
  _projection(projection),
  _bgImage(nullptr),
  _bgColor(std::nullopt),
  _clipArea(std::nullopt)
{
}

Camera::Camera(
	RenderSystem& renderSystem,
	ecs::component_reference<const Transform2> trans,
	RenderSystem::render_mask_t mask,
	const gr::Projection& projection,
	const gr::Color& bgColor,
	gr::MeshHandle bgImage,
	int order
)
: _scope(renderSystem.add(*this, order)),
  _trans(trans),
  _mask(mask),
  _projection(projection),
  _bgImage(bgImage),
  _bgColor(bgColor),
  _clipArea(std::nullopt)
{
}

Camera::Camera(
	RenderSystem& renderSystem,
	ecs::component_reference<const Transform2> trans,
	RenderSystem::render_mask_t mask,
	const gr::Projection& projection,
	gr::MeshHandle bgImage,
	int order
)
: _scope(renderSystem.add(*this, order)),
  _trans(trans),
  _mask(mask),
  _projection(projection),
  _bgImage(bgImage),
  _bgColor(std::nullopt),
  _clipArea(std::nullopt)
{
}

void Camera::setBackgroundColor(const gr::Color& color)
{
	_bgColor = color;
}

void Camera::setClipArea(const math::AxisAlignedBox2<unsigned int>& clipArea)
{
	_clipArea = clipArea;
}

void Camera::resetClipArea()
{
	_clipArea = std::nullopt;
}

void Camera::startFrame(gr::ClientFrame& frame) const
{
	// TODO
}

void Camera::endFrame(gr::ClientFrame& frame) const
{
	// TODO
}

gr::Projection& Camera::projection()
{
	return _projection;
}

const gr::Projection& Camera::projection() const
{
	return _projection;
}

const Transform2& Camera::transformation() const
{
	return _trans.get();
}

RenderSystem::render_mask_t Camera::mask() const
{
	return _mask;
}

} }
