#include "ecs/Mesh2Renderer.hpp"

namespace pm { namespace ecs {

RenderSystem::render_category_t Mesh2Renderer::renderType() const
{
	return _type;
}

void Mesh2Renderer::render(gr::ClientFrame& frame) const
{
	// TODO
}

bool& Mesh2Renderer::active()
{
	return _active;
}

const bool& Mesh2Renderer::active() const
{
	return _active;
}

} }
