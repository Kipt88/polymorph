#include "ecs/Game.hpp"

namespace pm { 

void startupMain(ecs::Game& game);

namespace ecs {

namespace {

auto createQuitHandler(
	concurrency::RunnerTasks<Game>& tasks,
	os::AppSystem& app
)
{
	return app.addWindowEventListener<os::window_events::Quit>(
		[&]
		{
			tasks.access().pushAction(
				[](Game& game)
				{
					game.quit();
				}
			);
		}
	);
}

}

Game::Game(
	concurrency::RunnerTasks<Game>& tasks,
	os::AppSystem& app,
	gr::Graphics& graphics, 
	physics2d::Physics& physics
)
: _tasks(tasks),
  _scene(app, graphics, physics),
  _quitScope(createQuitHandler(tasks, app)),
  _state(_scene.create()),
  _quitFlag(false),
  _stateRemover()
{
	startupMain(*this);
}

bool Game::update(const concurrency::FrameTime& time)
{
	if (_stateRemover)
	{
		_stateRemover();
		_stateRemover.reset();
	}

	_scene.update(time);

	return !_quitFlag;
}

ecs::Scene& Game::scene()
{
	return _scene;
}

ecs::entity_handle_t Game::state()
{
	return _state;
}

ecs::entity_handle_t Game::newState()
{
	ASSERT(!_stateRemover, "Trying to switch states twice in a frame.");
	_stateRemover = [state = _state, scene = std::ref(_scene)]
	{
		scene.get().remove(state);
	};

	_state = _scene.create();

	return _state;
}

void Game::quit()
{
	_quitFlag = true;
}

} }
