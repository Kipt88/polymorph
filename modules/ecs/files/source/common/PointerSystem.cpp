#include "ecs/PointerSystem.hpp"

#include "ecs/Camera.hpp"

namespace pm { namespace ecs {

namespace {

auto windowToNormalized(gr::real_t winW, gr::real_t winH)
{
	// Converts coordinate to ([-1, 1], [-1, 1]) range

	auto invW = 1.0_r / winW;
	auto invH = 1.0_r / winH;

	return gr::Matrix4x4_r
	{
		2.0_r * invW,        0.0_r, 0.0_r, -1.0_r,
		       0.0_r, 2.0_r * invH, 0.0_r, -1.0_r,
		       0.0_r,        0.0_r, 1.0_r, 0.0_r,
		       0.0_r,        0.0_r, 0.0_r, 1.0_r
	};
}

auto createInputListeners(os::AppSystem& app, PointerSystem::InputQueue& queue)
{
	return std::make_tuple(
		app.addWindowEventListener<os::window_events::PointerDown>(
			[&](const os::window_events::PointerDown& ev)
			{
				auto access = queue.access();

				access.pushAction(
					[ev](PointerSystem::listeners_storage_t& listeners)
					{
						auto unProjMat = windowToNormalized(
							static_cast<gr::real_t>(ev.winW),
							static_cast<gr::real_t>(ev.winH)
						);
						
						auto v = unProjMat * gr::Vector4_r{
							static_cast<gr::real_t>(ev.x),
							static_cast<gr::real_t>(ev.y),
							0.0_r,
							1.0_r
						};

						for (auto& listener : listeners)
						{
							auto& handler = listener.first;
							auto& camera = listener.second.get();

							handler.pointerDown(
								handler.obj,
								camera.transformation().world()
								* (camera.projection().inverse() 
								   * gr::Vector2_r{ v[0], v[1] })
							);
						}
					}
				);
			}
		),
		app.addWindowEventListener<os::window_events::PointerUp>(
			[&](const os::window_events::PointerUp& ev)
			{
				auto access = queue.access();

				access.pushAction(
					[ev](PointerSystem::listeners_storage_t& listeners)
					{
						auto unProjMat = windowToNormalized(
							static_cast<gr::real_t>(ev.winW),
							static_cast<gr::real_t>(ev.winH)
						);
						
						auto v = unProjMat * gr::Vector4_r{
							static_cast<gr::real_t>(ev.x),
							static_cast<gr::real_t>(ev.y),
							0.0_r,
							1.0_r
						};

						for (auto& listener : listeners)
						{
							auto& handler = listener.first;
							auto& camera = listener.second.get();

							handler.pointerUp(
								handler.obj,
								camera.transformation().world()
								* (camera.projection().inverse() 
								   * gr::Vector2_r{ v[0], v[1] })
							);
						}
					}
				);
			}
		),
		app.addWindowEventListener<os::window_events::PointerZoom>(
			[&](const os::window_events::PointerZoom& ev)
			{
				auto access = queue.access();

				access.pushAction(
					[ev](PointerSystem::listeners_storage_t& listeners)
					{
						for (auto& listener : listeners)
						{
							auto& handler = listener.first;

							handler.pointerZoom(handler.obj, ev.dz);
						}
					}
				);
			}
		),
		app.addWindowEventListener<os::window_events::PointerMove>(
			[&](const os::window_events::PointerMove& ev)
			{
				auto access = queue.access();

				access.pushAction(
					[ev](PointerSystem::listeners_storage_t& listeners)
					{
						auto unProjMat = windowToNormalized(
							static_cast<gr::real_t>(ev.winW),
							static_cast<gr::real_t>(ev.winH)
						);
						
						auto v1 = unProjMat * gr::Vector4_r{
							static_cast<gr::real_t>(ev.x),
							static_cast<gr::real_t>(ev.y),
							0.0_r,
							1.0_r
						};

						auto v2 = unProjMat * gr::Vector4_r{
							static_cast<gr::real_t>(ev.lastX),
							static_cast<gr::real_t>(ev.lastY),
							0.0_r,
							1.0_r
						};

						for (auto& listener : listeners)
						{
							auto& handler = listener.first;
							auto& camera = listener.second.get();

							auto start = 
								camera.transformation().world()
								* (camera.projection().inverse()
								   * gr::Vector2_r{ v2[0], v2[1] }
							);

							auto end = 
								camera.transformation().world()
								* (camera.projection().inverse()
								   * gr::Vector2_r{ v1[0], v1[1] }
							);

							handler.pointerMove(handler.obj, start, end - start);
						}
					}
				);
			}
		),
		app.addWindowEventListener<os::window_events::PointerDrag>(
			[&](const os::window_events::PointerDrag& ev)
			{
				auto access = queue.access();

				access.pushAction(
					[ev](PointerSystem::listeners_storage_t& listeners)
					{
						auto unProjMat = windowToNormalized(
							static_cast<gr::real_t>(ev.winW),
							static_cast<gr::real_t>(ev.winH)
						);

						auto v1 = unProjMat * gr::Vector4_r{
							static_cast<gr::real_t>(ev.x),
							static_cast<gr::real_t>(ev.y),
							0.0_r,
							1.0_r
						};

						auto v2 = unProjMat * gr::Vector4_r{
							static_cast<gr::real_t>(ev.lastX),
							static_cast<gr::real_t>(ev.lastY),
							0.0_r,
							1.0_r
						};

						for (auto& listener : listeners)
						{
							auto& handler = listener.first;
							auto& camera = listener.second.get();

							auto start = 
								camera.transformation().world()
								* (camera.projection().inverse()
								   * gr::Vector2_r{ v2[0], v2[1] }
							);

							auto end = 
								camera.transformation().world()
								* (camera.projection().inverse()
								   * gr::Vector2_r{ v1[0], v1[1] }
							);

							handler.pointerDrag(handler.obj, end, end - start);
						}
					}
				);
			}
		)
	);
}

}


PointerSystem::PointerSystem(os::AppSystem& app)
: _handlers(),
  _tasks(),
  _inputScope(createInputListeners(app, _tasks))
{
}

void PointerSystem::remove(handle_t handle)
{
	_handlers.erase(handle);
}

void PointerSystem::update(const concurrency::FrameTime&)
{
	{
		auto access = _tasks.access();

		while (!access.empty())
			access.doAction(_handlers);
	}
}

} }
