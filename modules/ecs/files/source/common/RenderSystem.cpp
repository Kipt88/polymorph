#include "ecs/RenderSystem.hpp"

#include "ecs/Camera.hpp"

namespace pm { namespace ecs {

RenderSystem::RenderSystem(gr::Graphics& graphics)
: _cameras(),
  _renderables(),
  _graphics(graphics)
{
}

void RenderSystem::remove(renderable_handle_t handle)
{
	_renderables.erase(handle);
}

void RenderSystem::remove(camera_handle_t handle)
{
	_cameras.erase(handle);
}

void RenderSystem::update(const concurrency::FrameTime&) const
{
	auto frame = _graphics.createFrame();

	for (const auto& cameraOrder : _cameras)
	{
		const auto& camera = cameraOrder.second.get();

		camera.startFrame(frame);

		for (const auto& renderableOrder : _renderables)
		{
			const auto& renderable = renderableOrder.second;

			if ((camera.mask() & renderable.renderType(renderable.obj)) != 0) 
				renderable.render(renderable.obj, frame);
		}

		camera.endFrame(frame);
	}
}

RenderSystem::CameraScope RenderSystem::add(const Camera& camera, int order)
{
	camera_handle_t handle = _cameras.insert(
		std::find_if(
			_cameras.begin(),
			_cameras.end(),
			[&](const std::pair<const int, std::reference_wrapper<const Camera>>& a)
			{
				return a.first > order;
			}
		),
		std::make_pair(
			order,
			std::cref(camera)
		)
	);

	return CameraScope(*this, handle);
}

} }
