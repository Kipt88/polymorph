#include "ecs/Button.hpp"

#include "ecs/Mesh2Renderer.hpp"
#include <gr/TextureRenderer.hpp>

namespace pm { namespace ecs {

namespace Button {

Interface_const::Interface_const(const_entity_handle_t handle)
: const_entity_handle_t(handle)
{
}

component_reference<const HitBox> Interface_const::hitbox() const
{
	return entity().get<HitBox>();
}

component_reference<const Transform2> Interface_const::transform() const
{
	return entity().get<Transform2>();
}

component_reference<const gr::MeshHandle> Interface_const::mesh() const
{
	return entity().get<gr::MeshHandle>();
}

const gr::AABox2& Interface_const::getBounds() const
{
	return entity().get<HitBox>().getBounds();
}


Interface::Interface(entity_handle_t handle)
: entity_handle_t(handle)
{
}

component_reference<HitBox> Interface::hitbox() const
{
	return entity().get<HitBox>();
}

component_reference<Transform2> Interface::transform() const
{
	return entity().get<Transform2>();
}

component_reference<const gr::MeshHandle> Interface::mesh() const
{
	return entity().get<gr::MeshHandle>();
}

const gr::AABox2& Interface::getBounds() const
{
	return entity().get<HitBox>().getBounds();
}

void Interface::setTransform(const Transform2& trans) const
{
	entity().get<Transform2>() = trans;
}

void Interface::setMesh(gr::MeshHandle mesh) const
{
	auto& hitboxComponent = entity().get<HitBox>();
	auto& meshComponent = entity().get<gr::MeshHandle>();

	meshComponent = mesh;
	hitboxComponent.setBounds(mesh->bounds());
}

Interface::operator Interface_const() const
{
	return Interface_const(static_cast<const entity_handle_t&>(*this));
}


Interface create(
	ecs::entity_handle_t handle,
	const Camera& uiCamera,
	RenderSystem::render_category_t type,
	int renderOrder
)
{
	auto& entity = handle.entity();

	auto& meshComponent = entity.add<gr::MeshHandle>(nullptr);
	auto& transformComponent = entity.add<Transform2>();

	auto& hitbox = entity.add<HitBox>(
		handle.systems().pointerSystem(), 
		transformComponent, 
		uiCamera
	);
	
	entity.add<Mesh2Renderer>(
		handle.systems().renderSystem(),
		gr::TextureRenderer::render,
		meshComponent,
		transformComponent,
		type,
		renderOrder
	);

	return Interface(handle);
}


}

} }
