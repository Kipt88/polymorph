#include "ecs/HitBox.hpp"

namespace pm { namespace ecs {

HitBox::HitBox(
	PointerSystem& pointerSystem,
	ecs::component_reference<const Transform2> transform,
	ecs::component_reference<const Camera> camera
)
: _delegator(),
  _scope(pointerSystem.add<
	  PM_TMETHOD(HitBox::onPointerDown), 
	  PM_TMETHOD(HitBox::onPointerUp), 
	  PM_TNULL_METHOD(),
	  PM_TNULL_METHOD(),
	  PM_TNULL_METHOD()
  >(*this, camera)),
  _localBounds(),
  _transform(transform)
{
}

const gr::AABox2& HitBox::getBounds() const
{
	return _localBounds;
}

void HitBox::setBounds(const gr::AABox2& box)
{
	_localBounds = box;
}

void HitBox::setBounds(gr::real_t w, gr::real_t h)
{
	_localBounds = gr::AABox2(-0.5_r * w, -0.5_r * h, 0.5_r * w, 0.5_r * h);
}

bool HitBox::inBounds(const gr::Vector2_r& worldPos) const
{
	auto localPos = _transform.get().world().inverse() * worldPos;

	return _localBounds.inside(localPos);
}

void HitBox::resetDelegate()
{
	_delegator = Delegate();
}

void HitBox::onPointerDown(const gr::Vector2_r& pos)
{
	if (_delegator && inBounds(pos))
		_delegator.callOnHitDown();
}

void HitBox::onPointerUp(const gr::Vector2_r& pos)
{
	if (_delegator && inBounds(pos))
		_delegator.callOnHitUp();
}

HitBox::Delegate::Delegate()
: _obj(nullptr),
  _onHitDown(nullptr),
  _onHitUp(nullptr)
{
}

void HitBox::Delegate::callOnHitDown()
{
	if (_onHitDown)
		(*_onHitDown)(_obj, _param);
}

void HitBox::Delegate::callOnHitUp()
{
	if (_onHitUp)
		(*_onHitUp)(_obj, _param);
}

HitBox::Delegate::operator bool() const
{
	return _obj != nullptr;
}

} }
