#include "ecs/Entity.hpp"

namespace pm { namespace ecs {

Entity::~Entity()
{
	clear();
}

void Entity::clear()
{
	for (auto insertIt = _insertionOrder.rbegin(); 
	     insertIt != _insertionOrder.rend();
		 ++insertIt)
	{
		DEBUG_OUT("Entity", "Removing component: %s", (*insertIt)->second.typeName());
		
		// This ensures the handle is valid while destruction is run.
		(*insertIt)->second.release();
		_components.erase(*insertIt);
	}

	_insertionOrder.clear();
}

const std::map<id_t, Component>& Entity::components() const
{
	return _components;
}

} }
