#include "ecs/Systems.hpp"

namespace pm { namespace ecs {

Systems::Systems(os::AppSystem& app, gr::Graphics& graphics, physics2d::Physics& physics)
: _app(app),
  _graphics(graphics),
  _physics(physics),
  _updateSystem(app),
  _pointerSystem(app),
  _keySystem(app),
  _gamepadSystem(app),
  _renderSystem(graphics),
  _physicsSystem(physics)
{
}

void Systems::update(const concurrency::FrameTime& time)
{
	_updateSystem.update(time);
	_renderSystem.update(time);
	_pointerSystem.update(time);
	_keySystem.update(time);
	_gamepadSystem.update(time);
	_physicsSystem.update(time);
}

RenderSystem& Systems::renderSystem()
{
	return _renderSystem;
}

const RenderSystem& Systems::renderSystem() const
{
	return _renderSystem;
}

PointerSystem& Systems::pointerSystem()
{
	return _pointerSystem;
}

const PointerSystem& Systems::pointerSystem() const
{
	return _pointerSystem;
}

KeySystem& Systems::keySystem()
{
	return _keySystem;
}

const KeySystem& Systems::keySystem() const
{
	return _keySystem;
}

GamepadSystem& Systems::gamepadSystem()
{
	return _gamepadSystem;
}

const GamepadSystem& Systems::gamepadSystem() const
{
	return _gamepadSystem;
}

UpdateSystem& Systems::updateSystem()
{
	return _updateSystem;
}

const UpdateSystem& Systems::updateSystem() const
{
	return _updateSystem;
}

PhysicsSystem& Systems::physicsSystem()
{
	return _physicsSystem;
}

const PhysicsSystem& Systems::physicsSystem() const
{
	return _physicsSystem;
}

os::AppSystem& Systems::app()
{
	return _app;
}

const os::AppSystem& Systems::app() const
{
	return _app;
}

gr::Graphics& Systems::graphics()
{
	return _graphics;
}

const gr::Graphics& Systems::graphics() const
{
	return _graphics;
}

physics2d::Physics& Systems::physics()
{
	return _physics;
}

const physics2d::Physics& Systems::physics() const
{
	return _physics;
}

gr::Resources& Systems::graphicsResources()
{
	return _graphics.resources();
}

physics2d::BodyProvider& Systems::physicsResources()
{
	return _physics.bodyProvider();
}

} }
