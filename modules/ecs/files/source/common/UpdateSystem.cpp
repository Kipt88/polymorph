#include "ecs/UpdateSystem.hpp"

namespace pm { namespace ecs {

UpdateSystem::UpdateSystem(os::AppSystem& app)
: _app(app),
  _updaters(),
  _events()
{
}

bool UpdateSystem::Updater::operator==(const Updater& other) const
{
	return obj == other.obj && update == other.update;
}

void UpdateSystem::remove(updater_handle_t handle)
{
	handle->obj = nullptr;
}

void UpdateSystem::update(const concurrency::FrameTime& time)
{
	{
		auto events = _events.access();

		while (!events.empty())
			events.doAction();
	}

	for (auto it = _updaters.begin(); it != _updaters.end();)
	{
		if (it->obj)
		{
			it->update(it->obj, time.delta);
			++it;
		}
		else
		{
			it = _updaters.erase(it);
		}
	}
}

} }
