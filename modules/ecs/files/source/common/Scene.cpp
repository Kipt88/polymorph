#include "ecs/Scene.hpp"

#include <Assert.hpp>
#include <Debug.hpp>
#include <algorithm>

namespace pm { namespace ecs {

namespace {

const char* TAG = "Scene";

auto getChildEnd(const entity_storage_t::const_iterator& parent)
{
	auto end = std::next(parent);

	for (auto it = parent; it != end; ++it)
		std::advance(end, it->childCount);

	return end;
}

auto getChildEnd(const entity_storage_t::iterator& parent)
{
	auto end = std::next(parent);

	for (auto it = parent; it != end; ++it)
		std::advance(end, it->childCount);

	return end;
}

}


bool operator==(const const_entity_handle_t& a, const const_entity_handle_t& b)
{
	return a._it == b._it;
}

bool operator!=(const const_entity_handle_t& a, const const_entity_handle_t& b)
{
	return !(a == b);
}


const_entity_handle_t::const_entity_handle_t(const Scene& scene, const entity_storage_t::const_iterator& it)
: _it(it),
  _scene(scene)
{
}

const Entity& const_entity_handle_t::entity() const
{
	return _it->entity;
}

unsigned int const_entity_handle_t::childrenSize() const
{
	return _it->childCount;
}

const_entity_handle_t const_entity_handle_t::child(unsigned int index) const
{
	ASSERT(index < _it->childCount, "Trying to get child outside range.");

	auto childIt = std::next(_it);
	for (unsigned int i = 0; i < index; ++i)
		childIt = getChildEnd(childIt);

	return const_entity_handle_t(_scene, childIt);
}

bool const_entity_handle_t::hasParent() const
{
	return _it->parent != _scene.get()._entities.end();
}

const_entity_handle_t const_entity_handle_t::parent() const
{
	return const_entity_handle_t(_scene, _it->parent);
}

const Systems& const_entity_handle_t::systems() const
{
	return _scene.get().systems();
}

entity_handle_t::entity_handle_t(Scene& scene, const entity_storage_t::iterator& it)
: _it(it),
  _scene(scene)
{
}

Entity& entity_handle_t::entity() const
{
	return _it->entity;
}

unsigned int entity_handle_t::childrenSize() const
{
	return _it->childCount;
}

entity_handle_t entity_handle_t::child(unsigned int index) const
{
	ASSERT(index < _it->childCount, "Trying to get child outside range.");

	auto childIt = std::next(_it);
	for (unsigned int i = 0; i < index; ++i)
		childIt = getChildEnd(childIt);

	return entity_handle_t(_scene, childIt);
}

bool entity_handle_t::hasParent() const
{
	return _it->parent != _scene.get()._entities.end();
}

entity_handle_t entity_handle_t::parent() const
{
	return entity_handle_t(_scene, _it->parent);
}

Systems& entity_handle_t::systems() const
{
	return _scene.get().systems();
}

entity_handle_t entity_handle_t::createChild() const
{
	return _scene.get().create(*this);
}

void entity_handle_t::remove() const
{
	_scene.get().remove(*this);
}

void entity_handle_t::removeChildren() const
{
	_scene.get().removeChildren(*this);
}

void entity_handle_t::removeChild(unsigned int index) const
{
	child(index).remove();
}

entity_handle_t::operator const_entity_handle_t() const
{
	return const_entity_handle_t(_scene, _it);
}

Scene::Scene(os::AppSystem& app, gr::Graphics& graphics, physics2d::Physics& physics)
: _systems(app, graphics, physics),
  _entities()
{
}

Scene::~Scene()
{
	clear();
}

entity_handle_t Scene::create()
{
	return entity_handle_t(
		*this,
		_entities.insert(
			_entities.end(),
			{ 0, _entities.end(), Entity() }
		)
	);
}

entity_handle_t Scene::create(entity_handle_t parent)
{
	auto end = getChildEnd(parent._it);

	++parent._it->childCount;

	return entity_handle_t(
		*this,
		_entities.insert(
			end,
			{ 0, parent._it, Entity() }
		)
	);
}

void Scene::remove(entity_handle_t handle)
{
	auto start = handle._it;
	auto end = getChildEnd(start);

	for (auto it = std::prev(end); it != start; --it)
		it->entity.clear();

	start->entity.clear();

	if (start->parent != _entities.end())
		--start->parent->childCount;

	_entities.erase(start, end);
}

void Scene::removeChildren(entity_handle_t handle)
{
	while (handle.childrenSize() > 0)
		remove(handle.child(handle.childrenSize() - 1));
}

void Scene::update(const concurrency::FrameTime& time)
{
	_systems.update(time);
}

void Scene::clear()
{
	for (auto it = _entities.rbegin(); it != _entities.rend(); ++it)
		it->entity.clear();
}

Systems& Scene::systems()
{
	return _systems;
}

const Systems& Scene::systems() const
{
	return _systems;
}

void Scene::dbgPrintHierarchy() const
{
	DEBUG_OUT(TAG, "==== Hierarchy ====");

	std::string indentation = "";

	std::vector<unsigned int> childCounters;

	for (const auto& entry : _entities)
	{
		DEBUG_OUT(TAG, "%s(entity 0x%p)", indentation.c_str(), &entry.entity);
		
		for (const auto& componentEntry : entry.entity.components())
			DEBUG_OUT(TAG, "%s  %s", indentation.c_str(), componentEntry.second.typeName());

		if (entry.childCount > 0)
		{
			childCounters.push_back(entry.childCount);
			indentation += "  ";
		}
		else if (!childCounters.empty())
		{
			auto& counter = childCounters.back();
			if (--counter == 0)
			{
				indentation.pop_back();
				indentation.pop_back();

				childCounters.pop_back();
			}
		}
	}

	DEBUG_OUT(TAG, "===================");
}

} }
