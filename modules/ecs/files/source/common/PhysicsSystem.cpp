#include "ecs/PhysicsSystem.hpp"

namespace pm { namespace ecs {

namespace {

const char* LOG_TAG = "ecs::PhysicsSystem";

}

PhysicsSystem::DelegateScope::DelegateScope(PhysicsSystem& system, delegate_handle_t handle)
: SystemScope<PhysicsSystem, delegate_handle_t>(system, handle)
{
}

void PhysicsSystem::DelegateScope::setPosition(const Vector2_r& pos)
{
	_system->_nextFrame.setPosition(
		_handle, 
		static_cast<physics2d::Vector2_r>(pos)
	);
}

void PhysicsSystem::DelegateScope::setRotation(gr::real_t angle)
{
	_system->_nextFrame.setRotation(_handle, angle);
}

void PhysicsSystem::DelegateScope::setVelocity(const Vector2_r& v)
{
	_system->_nextFrame.setVelocity(
		_handle, 
		static_cast<physics2d::Vector2_r>(v)
	);
}

void PhysicsSystem::DelegateScope::applyForce(const Vector2_r& force, const Vector2_r& localPos)
{
	_system->_nextFrame.applyForce(
		_handle, 
		static_cast<physics2d::Vector2_r>(force),
		static_cast<physics2d::Vector2_r>(localPos)
	);
}

void PhysicsSystem::DelegateScope::applyImpulse(const Vector2_r& impulse, const Vector2_r& localPos)
{
	_system->_nextFrame.applyImpulse(
		_handle, 
		static_cast<physics2d::Vector2_r>(impulse),
		static_cast<physics2d::Vector2_r>(localPos)
	);
}


PhysicsSystem::PhysicsSystem(physics2d::Physics& physics)
: _physics(physics),
  _delegates(),
  _nextFrame(_physics),
  _lastState(),
  _counter(0)
{
}

void PhysicsSystem::update(const concurrency::FrameTime& time)
{
	_lastState = _physics.waitForState(time.t0, time.t1);

	const auto& bodies = _lastState.snapshot.bodies();

	for (const auto& body : bodies)
	{
		auto it = _delegates.find(body.id);

		if (it != _delegates.end())
			it->second.updateState(it->second.obj, body);
	}

	for (const auto& collision : _lastState.collisions)
	{
		auto itA = _delegates.find(collision.bodyA);
		auto itB = _delegates.find(collision.bodyB);

		if (itA != _delegates.end() && itB != _delegates.end())
		{
			physics2d::CollisionData data;

			data.firstContact = collision.firstContact;
			data.worldPoint = collision.worldPoint;


			data.worldImpulse = collision.worldImpulseA;
			data.otherBody = itB->first;
			data.otherHandler = itB->second.obj;

			itA->second.onCollision(itA->second.obj, data);


			data.worldImpulse = -collision.worldImpulseA;
			data.otherBody = itA->first;
			data.otherHandler = itA->second.obj;

			itB->second.onCollision(itB->second.obj, data);
		}
	}

	_nextFrame.flush();
}

void PhysicsSystem::remove(delegate_handle_t handle)
{
	DEBUG_OUT(LOG_TAG, "Removing body %i", static_cast<int>(handle));

	_nextFrame.removeBody(handle);
	_delegates.erase(handle);
}

std::optional<physics2d::CastHit> PhysicsSystem::castLine(
	const physics2d::Vector2_r& p0,
	const physics2d::Vector2_r& p1,
	const physics2d::CollisionFilter& filter
) const
{
	return _lastState.snapshot.castLine(p0, p1, filter);
}

void PhysicsSystem::setGravity(const ecs::Vector2_r& v)
{
	_nextFrame.setGravity({ v[0], v[1] });
}

void PhysicsSystem::setDamping(gr::real_t d)
{
	_nextFrame.setDamping(d);
}

} }
