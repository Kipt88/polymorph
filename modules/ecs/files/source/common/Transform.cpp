#include "ecs/Transform.hpp"

namespace pm { namespace ecs {

Transform2::Transform2(const LocalTransform2& transform)
: _parent(nullptr),
  _local(transform)
{
}

Transform2::Transform2(const Transform2& parent, const LocalTransform2& local)
: _parent(&parent), 
  _local(local)
{
}

LocalTransform2& Transform2::local()
{
	return _local;
}

const LocalTransform2& Transform2::local() const
{
	return _local;
}

LocalTransform2 Transform2::world() const
{
	gr::Transform2 trans = _local;

	const auto* parent = _parent;

	while (parent)
	{
		trans = parent->local() * trans;
		parent = parent->_parent;
	}

	return trans;
}

void Transform2::setParent(const Transform2& parent)
{
	_parent = &parent;
}

} }
