#include "ecs/TextButton.hpp"

#include "ecs/StringRenderer.hpp"
#include "ecs/Button.hpp"
#include <gr/TextureRenderer.hpp>

namespace pm { namespace ecs {

namespace TextButton {

component_reference<const std::string> Interface_const::text() const
{
	return entity().get<std::string>();
}

component_reference<std::string> Interface::text() const
{
	return entity().get<std::string>();
}

void Interface::setText(const std::string& text) const
{
	entity().get<std::string>() = text;
}

void Interface::setText(std::string&& text) const
{
	entity().get<std::string>() = std::move(text);
}


Interface::operator Interface_const() const
{
	return Interface_const(static_cast<const Button::Interface_const&>(*this));
}


Interface create(
	ecs::entity_handle_t handle,
	const Camera& uiCamera,
	RenderSystem::render_category_t type,
	const gr::MeshFont2& font,
	int renderOrder
)
{
	auto button = Button::create(
		handle,
		uiCamera,
		type,
		renderOrder
	);
	
	auto& textComp = button.entity().add<std::string>();
	button.entity().add<StringRenderer>(
		handle.systems().renderSystem(),
		textComp,
		button.transform().get(),
		type,
		font,
		StringRenderer::Alignment::CENTER,
		gr::Vector2_r{ 0.0_r, 0.5_r },
		renderOrder + 1
	);

	return Interface(button);
}

}

} }
