#include "ecs/TimerComponent.hpp"

namespace pm { namespace ecs {

TimerComponent::TimerComponent(
	UpdateSystem& updateSystem,
	const Time::duration& time
)
: _timer(time),
  _onComplete(),
  _scope(updateSystem.add<PM_TMETHOD(TimerComponent::update)>(*this))
{
}

void TimerComponent::reset(const Time::duration& time)
{
	_timer = time;
}

void TimerComponent::update(const Time::duration& dt)
{
	if (_timer > Time::duration::zero())
	{
		_timer -= dt;

		if (_timer <= Time::duration::zero())
		{
			_timer = Time::duration::zero();

			if (_onComplete)
				_onComplete();
		}
	}
}

const Time::duration& TimerComponent::timeLeft() const
{
	return _timer;
}

} }
