#include "ecs/KeySystem.hpp"

namespace pm { namespace ecs {

namespace {

auto createInputListeners(os::AppSystem& app, KeySystem::InputQueue& queue)
{
	return std::make_tuple(
		app.addWindowEventListener<os::window_events::KeyDown>(
			[&](const os::window_events::KeyDown& ev)
			{
				auto access = queue.access();

				access.pushAction(
					[ev](KeySystem::listeners_storage_t& handlers)
					{
						for (auto& handler : handlers)
						{
							handler.keyDown(handler.obj, ev.key);
						}
					}
				);
			}
		),
		app.addWindowEventListener<os::window_events::KeyUp>(
			[&](const os::window_events::KeyUp& ev)
			{
				auto access = queue.access();

				access.pushAction(
					[ev](KeySystem::listeners_storage_t& handlers)
					{
						for (auto& handler : handlers)
						{
							handler.keyUp(handler.obj, ev.key);
						}
					}
				);
			}
		)
	);
}

}

KeySystem::KeySystem(os::AppSystem& app)
: _handlers(),
  _tasks(),
  _inputScope(createInputListeners(app, _tasks))
{
}

void KeySystem::update(const concurrency::FrameTime&)
{
	{
		auto access = _tasks.access();

		while (!access.empty())
			access.doAction(_handlers);
	}
}

void KeySystem::remove(handle_t handle)
{
	_handlers.erase(handle);
}

} }
