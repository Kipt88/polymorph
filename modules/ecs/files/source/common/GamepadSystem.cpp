#include "ecs/GamepadSystem.hpp"

namespace pm { namespace ecs {

namespace {

auto createInputListeners(os::AppSystem& app, GamepadSystem::InputQueue& queue)
{
	return std::make_tuple(
		app.addWindowEventListener<os::window_events::GamepadConnect>(
			[&](const os::window_events::GamepadConnect& ev)
			{
				auto access = queue.access();

				access.pushAction(
					[ev](GamepadSystem::listeners_storage_t& handlers)
					{
						for (auto& handler : handlers)
						{
							if (ev.id == handler.padId)
								handler.connect(handler.obj);
						}
					}
				);
			}
		),
		app.addWindowEventListener<os::window_events::GamepadDisconnect>(
			[&](const os::window_events::GamepadDisconnect& ev)
			{
				auto access = queue.access();

				access.pushAction(
					[ev](GamepadSystem::listeners_storage_t& handlers)
					{
						for (auto& handler : handlers)
						{
							if (ev.id == handler.padId)
								handler.disconnect(handler.obj);
						}
					}
				);
			}
		),
		app.addWindowEventListener<os::window_events::GamepadKeyDown>(
			[&](const os::window_events::GamepadKeyDown& ev)
			{
				auto access = queue.access();

				access.pushAction(
					[ev](GamepadSystem::listeners_storage_t& handlers)
					{
						for (auto& handler : handlers)
						{
							if (ev.id == handler.padId)
								handler.keyDown(handler.obj, ev.button);
						}
					}
				);
			}
		),
		app.addWindowEventListener<os::window_events::GamepadKeyUp>(
			[&](const os::window_events::GamepadKeyUp& ev)
			{
				auto access = queue.access();

				access.pushAction(
					[ev](GamepadSystem::listeners_storage_t& handlers)
					{
						for (auto& handler : handlers)
						{
							if (ev.id == handler.padId)
								handler.keyUp(handler.obj, ev.button);
						}
					}
				);
			}
		),
		app.addWindowEventListener<os::window_events::GamepadAxis>(
			[&](const os::window_events::GamepadAxis& ev)
			{
				auto access = queue.access();

				access.pushAction(
					[ev](GamepadSystem::listeners_storage_t& handlers)
					{
						gr::Vector2_r oldPos = {
							static_cast<gr::real_t>(ev.oldX),
							static_cast<gr::real_t>(ev.oldY)
						};
						
						gr::Vector2_r newPos = {
							static_cast<gr::real_t>(ev.newX),
							static_cast<gr::real_t>(ev.newY)
						};

						for (auto& handler : handlers)
						{
							if (ev.id == handler.padId)
								handler.axis(handler.obj, ev.axis, newPos, oldPos);
						}
					}
				);
			}
		)
	);
}

}

GamepadSystem::GamepadSystem(os::AppSystem& app)
: _handlers(),
  _tasks(),
  _inputScope(createInputListeners(app, _tasks))
{
}

void GamepadSystem::update(const concurrency::FrameTime&)
{
	{
		auto access = _tasks.access();

		while (!access.empty())
			access.doAction(_handlers);
	}
}

void GamepadSystem::remove(handle_t handle)
{
	_handlers.erase(handle);
}

} }
