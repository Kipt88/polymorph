#include <gtest/gtest.h>
#include <ecs/Scene.hpp>
#include <memory>

using namespace pm;

struct TestA
{
	~TestA() { if (onDeath) onDeath(); }
	std::function<void()> onDeath;
};

struct TestB
{
	~TestB() { if (onDeath) onDeath(); }
	std::function<void()> onDeath;
};

PM_MAKE_COMPONENT_ID(TestA);
PM_MAKE_COMPONENT_ID(TestB);


TEST(Entity, entity_lifetime)
{
	os::InputSystem inputSystem(33ms, "test entity_lifetime");
	gr::GraphicsSystem graphicsSystem(16ms, inputSystem);
	physics2d::PhysicsSystem physicsSystem(40ms);

	{
		bool aDead = false;
		bool bDead = false;

		bool failed = false;

		{
			ecs::Scene scene(inputSystem, graphicsSystem, physicsSystem);

			auto& entityA = scene.create().entity();
			entityA.add<TestA>().onDeath = [&]()
			{
				aDead = true;
				if (!bDead)
					failed = true;
			};

			auto& entityB = scene.create().entity();
			entityB.add<TestA>().onDeath = [&]()
			{
				bDead = true;
				if (aDead)
					failed = true;
			};
		}

		ASSERT_TRUE(aDead && bDead);
		ASSERT_FALSE(failed);
	}

	{
		bool aDead = false;
		bool bDead = false;

		bool failed = false;

		{
			ecs::Scene scene(inputSystem, graphicsSystem, physicsSystem);

			auto& entityA = scene.create().entity();
			entityA.add<TestA>().onDeath = [&]()
			{
				aDead = true;
				if (!bDead)
					failed = true;
			};

			auto handle = scene.create();

			auto& entityB = scene.create().entity();
			entityB.add<TestA>().onDeath = [&]()
			{
				bDead = true;
				if (aDead)
					failed = true;
			};

			scene.remove(handle);
		}

		ASSERT_TRUE(aDead && bDead);
		ASSERT_FALSE(failed);
	}
}

TEST(Entity, hierarchy)
{
	os::InputSystem inputSystem(33ms, "test entity_lifetime");
	gr::GraphicsSystem graphicsSystem(16ms, inputSystem);
	physics2d::PhysicsSystem physicsSystem(40ms);

	ecs::Scene scene(inputSystem, graphicsSystem, physicsSystem);

	auto a_handle = scene.create();
	auto b_handle = scene.create();
	auto aa_handle = scene.create(a_handle);
	auto ab_handle = scene.create(a_handle);
	auto aaa_handle = scene.create(aa_handle);
	auto aab_handle = scene.create(aa_handle);
	auto aba_handle = scene.create(ab_handle);

	ASSERT_EQ(a_handle.childrenSize(), 2);
	ASSERT_EQ(a_handle.child(0), aa_handle);
	ASSERT_EQ(a_handle.child(1), ab_handle);

	ASSERT_EQ(aa_handle.childrenSize(), 2);
	ASSERT_EQ(aa_handle.child(0), aaa_handle);
	ASSERT_EQ(aa_handle.child(1), aab_handle);

	ASSERT_EQ(aaa_handle.childrenSize(), 0);

	ASSERT_EQ(aab_handle.childrenSize(), 0);

	ASSERT_EQ(ab_handle.childrenSize(), 1);
	ASSERT_EQ(ab_handle.child(0), aba_handle);

	ASSERT_EQ(aba_handle.childrenSize(), 0);

	ASSERT_EQ(b_handle.childrenSize(), 0);
}

//
// a
// - aa
//   - aaa
// - ab
//   - aba
// b
//
TEST(Entity, child_lifetime)
{
	{
		bool failed = false;

		bool a_dead = false;
		bool aa_dead = false;
		bool aaa_dead = false;
		bool ab_dead = false;
		bool aba_dead = false;
		bool b_dead = false;

		os::InputSystem inputSystem(33ms, "test entity_lifetime");
		gr::GraphicsSystem graphicsSystem(16ms, inputSystem);
		physics2d::PhysicsSystem physicsSystem(40ms);

		{
			ecs::Scene scene(inputSystem, graphicsSystem, physicsSystem);

			auto a_handle = scene.create();
			auto b_handle = scene.create();

			auto aa_handle = scene.create(a_handle);
			
			auto temp_handle = scene.create(aa_handle);
			
			auto ab_handle = scene.create(a_handle);
			auto aaa_handle = scene.create(aa_handle);
			auto aba_handle = scene.create(ab_handle);

			a_handle.entity().add<TestA>().onDeath = [&]()
			{
				a_dead = true;

				if (!aa_dead || !aaa_dead || !ab_dead || !aba_dead || !b_dead)
				{
					DEBUG_OUT("Entity_test", "a_failed");
					failed = true;
				}
			};

			b_handle.entity().add<TestA>().onDeath = [&]()
			{
				b_dead = true;

				if (a_dead || aa_dead || aaa_dead || ab_dead || aba_dead)
				{
					DEBUG_OUT("Entity_test", "b_failed");
					failed = true;
				}
			};

			aa_handle.entity().add<TestA>().onDeath = [&]()
			{
				aa_dead = true;

				if (a_dead || !b_dead || !aaa_dead || !ab_dead || !aba_dead)
				{
					DEBUG_OUT("Entity_test", "aa_failed");
					failed = true;
				}
			};

			ab_handle.entity().add<TestA>().onDeath = [&]()
			{
				ab_dead = true;

				if (a_dead || !b_dead || aa_dead || aaa_dead || !aba_dead)
				{
					DEBUG_OUT("Entity_test", "ab_failed");
					failed = true;
				}
			};

			aaa_handle.entity().add<TestA>().onDeath = [&]()
			{
				aaa_dead = true;

				if (a_dead || !b_dead || aa_dead || !ab_dead || !aba_dead)
				{
					DEBUG_OUT("Entity_test", "aaa_failed");
					failed = true;
				}
			};

			aba_handle.entity().add<TestA>().onDeath = [&]()
			{
				aba_dead = true;

				if (a_dead || !b_dead || aa_dead || ab_dead || aaa_dead)
				{
					DEBUG_OUT("Entity_test", "aba_failed");
					failed = true;
				}
			};

			scene.remove(temp_handle);
		}

		ASSERT_TRUE(a_dead);
		ASSERT_TRUE(b_dead);
		ASSERT_TRUE(aa_dead);
		ASSERT_TRUE(ab_dead);
		ASSERT_TRUE(aaa_dead);
		ASSERT_TRUE(aba_dead);

		ASSERT_FALSE(failed);
	}
}

TEST(Entity, component_lifetime)
{
	bool aDead = false;
	bool bDead = false;

	bool failed = false;

	{
		ecs::Entity entity;

		auto& a = entity.add<TestA>();
		a.onDeath = [&]()
		{
			aDead = true;
			if (!bDead)
				failed = true;
		};

		auto& b = entity.add<TestB>();
		b.onDeath = [&]()
		{
			bDead = true;
			if (aDead)
				failed = true;
		};
	}

	ASSERT_TRUE(aDead && bDead);
	ASSERT_FALSE(failed);
}

