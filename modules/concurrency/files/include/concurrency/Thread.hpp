#pragma once

#include <mutex>
#include <Time.hpp>
#include <NonCopyable.hpp>
#include <Exception.hpp>

#include <functional>
#include <thread>
#include <condition_variable>

#include <atomic>

namespace pm { namespace concurrency {

PM_MAKE_EXCEPTION_CLASS(InterruptException, Exception);


class Thread : NonCopyable
{
public:
	typedef std::thread::id id_t;

	/**
	 * Creates an uninitialized thread.
	 * Only valid methods to call is assignment operator.
	 */
	Thread();

	template <typename Callable>
	Thread(Callable&& action);

	Thread(Thread&& other);

	~Thread();

	Thread& operator=(Thread&& other);

	void interrupt();
	void join();

	const id_t getID() const;

	static void setName(const char* name);

	static void sleep(const Time::duration& duration);
	static void sleepUntil(const Time::time_point& t);

	static void interruptionPoint();

	static void interruptCurrent();

	static void setInterrupted(bool interrupted = true);

	static const id_t getCurrentID();

public:

	// STD specific:

	static void setCondition(std::condition_variable* cond);

private:
	struct Data
	{
		Data();

		bool interruptionFlag;
		std::condition_variable* currentCondition;
		std::mutex mutex;
	};

	static Data* threadLocalData(Data* setter = nullptr);

	std::thread _stdThread;
	std::unique_ptr<Data> _localData;
};

} }

#include "concurrency/Thread.inl"
