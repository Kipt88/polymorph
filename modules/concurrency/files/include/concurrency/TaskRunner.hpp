#pragma once

#include "concurrency/Thread.hpp"
#include "concurrency/Mutex.hpp"
#include "concurrency/Condition.hpp"
#include "concurrency/TaskQueue.hpp"

#include <NonCopyable.hpp>

namespace pm { namespace concurrency {

template <typename R>
class TaskRunner : NonCopyable, NonMovable
{
public:
	TaskRunner();

	template <typename Callable>
	Future<std::result_of_t<Callable()>> pushAction(Callable&& action);

private:
	TaskQueue<R(void)> _actions;
	Condition _pushCond;
	Thread _runner;
};

} }

#include "concurrency/TaskRunner.inl"