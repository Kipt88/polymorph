#pragma once

#include "concurrency/Task.hpp"
#include "concurrency/Mutex.hpp"

#include <UniqueFunction.hpp>
#include <NonCopyable.hpp>

#include <type_traits>
#include <queue>

namespace pm { namespace concurrency {

struct dynamic_return_tag {};

template <typename T>
class TaskQueueImpl;

template <typename R, typename... Args>
class TaskQueueImpl<R(Args...)> : NonCopyable
{
public:
	template <typename Callable>
	Future<R> pushAction(Callable&& action);

	void doAction(Args... params);

	std::size_t size() const;
	bool empty() const;

	void clear();

private:
	typedef Task<R(Args...)> task_t;

	std::queue<task_t> _tasks;
};

template <typename... Args>
class TaskQueueImpl<dynamic_return_tag(Args...)>
{
public:
	template <typename Callable>
	auto pushAction(Callable&& action);

	void doAction(Args... params);

	std::size_t size() const;
	bool empty() const;

	void clear();

private:
	std::queue<UniqueFunction<void(Args...)>> _tasks;
};

template <typename T>
class TaskQueue;

template <typename R, typename... Args>
class TaskQueue<R(Args...)>
{
public:
	class AccessScope
	{
	public:
		AccessScope(TaskQueueImpl<R(Args...)>& queue, MutexLockGuard&& lock);

		void reset();

		template <typename Callable>
		auto pushAction(Callable&& action);

		void doAction(Args... args);

		std::size_t size() const;
		bool empty() const;

		void clear();

		MutexLockGuard& lockGuard();

	private:
		TaskQueueImpl<R(Args...)>& _queue;
		MutexLockGuard _lock;
	};

	AccessScope access();

private:
	TaskQueueImpl<R(Args...)> _queue;
	mutable Mutex _mutex;
};


} }

#include "concurrency/TaskQueue.inl"