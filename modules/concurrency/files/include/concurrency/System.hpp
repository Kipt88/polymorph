#pragma once

#include "concurrency/Thread.hpp"
#include "concurrency/Mutex.hpp"
#include "concurrency/Condition.hpp"
#include "concurrency/TaskQueue.hpp"

#include <Time.hpp>
#include <Scoped.hpp>
#include <Debug.hpp>

namespace pm { namespace concurrency {

PM_MAKE_EXCEPTION_CLASS(SystemDeadException, Exception);

template <typename Runner>
using RunnerTasks = TaskQueue<dynamic_return_tag(Runner&)>;

/**
* Template for systems.
*
* @param Runner	Type that updates the system.
* Runner must implement method update(void) that 
* returns true if system should continue running.
*/
template <typename Runner>
class System : Unique
{
public:
	System(const Time::duration& sync);

	template <typename Callable>
	Future<std::result_of_t<Callable(Runner&)>> pushAction(Callable&& action);

	template <typename... CtorArgs>
	void enter(CtorArgs&&... args);

private:
	RunnerTasks<Runner> _actions;

	const Time::duration _sync;

	Condition _pushCond;

	bool _alive;
};


template <typename Runner>
class ConcurrentSystem : Unique
{
public:
	template <typename... CtorArgs>
	ConcurrentSystem(const Time::duration& sync, CtorArgs&&... args);

	// TODO Add const-overload...
	template <typename Callable>
	Future<std::result_of_t<Callable(Runner&)>> pushAction(Callable&& action);

private:
	System<Runner> _system;
	Thread _runner;
};

} }

#include "concurrency/System.inl"
