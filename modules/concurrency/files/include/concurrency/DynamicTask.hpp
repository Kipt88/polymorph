#pragma once

#include "concurrency/Task.hpp"

#include <UniqueFunction.hpp>
#include <NonCopyable.hpp>

#include <memory>

namespace pm { namespace concurrency {

class DynamicTask : NonCopyable
{
public:
	template <typename R, typename... Args>
	DynamicTask(Task<R(Args...)>&& task, Args... args);

	inline DynamicTask(DynamicTask&& other);

	inline void operator()();

private:
	UniqueFunction<void()> _callable;
};

} }

#include "concurrency/DynamicTask.inl"