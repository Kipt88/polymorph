#pragma once

#include <Exception.hpp>
#include <NonCopyable.hpp>

#include <future>

namespace pm { namespace concurrency {

PM_MAKE_EXCEPTION_CLASS(TaskDeadException, Exception);
PM_MAKE_EXCEPTION_CLASS(TaskInterruptException, Exception);

template <typename R>
class Future : NonCopyable
{
public:
	Future(std::future<R>&& stdFuture);

	Future(Future<R>&& other);

	R get();

	bool ready() const;

private:
	std::future<R> _future;
};

template <typename T>
class Task;

template <typename R, typename... Args>
class Task<R(Args...)> : NonCopyable
{
public:
	template <
		typename Callable, 
		typename = 
		std::enable_if_t<
			!std::is_same<
				Task<R(Args...)>, 
		        std::decay_t<Callable>
			>::value
		>
	>
	Task(Callable&& callable);

	Task(Task<R(Args...)>&& other);

	void operator()(Args&&... args) const;

	Future<R> getFuture();

	void reset();

	void validAssert() const;

private:
	mutable std::packaged_task<R(Args...)> _task;
};

} }

#include "concurrency/Task.inl"