#pragma once

#include <NonCopyable.hpp>

#include <mutex>


namespace pm { namespace concurrency {

// TODO Implement interruption functionality.

class Mutex : NonCopyable
{
public:
	void lock();
	void unlock();

	std::mutex _impl;
};

class MutexLockGuard : NonCopyable
{
public:
	MutexLockGuard(Mutex& mutex);

	void reset();

	std::unique_lock<std::mutex> _impl;
};

} }
