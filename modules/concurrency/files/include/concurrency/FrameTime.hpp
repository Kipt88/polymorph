#pragma once

#include <Time.hpp>

namespace pm { namespace concurrency {

struct FrameTime
{
	Time::time_point t0;
	Time::time_point t1;

	Time::duration delta;
};

} }
