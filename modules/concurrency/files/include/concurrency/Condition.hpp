#pragma once

#include "concurrency/Mutex.hpp"

#include <Time.hpp>
#include <NonCopyable.hpp>

#include <functional>

#include <condition_variable>


namespace pm { namespace concurrency {

class Condition : NonCopyable
{
public:
	template <typename Predicate>
	void wait(MutexLockGuard& lock, Predicate&& pred);

	bool wait_for(MutexLockGuard& lock, Time::duration timeout);

	template <typename Predicate>
	bool wait_for(MutexLockGuard& lock, Time::duration timeout, Predicate&& pred);

	bool wait_until(MutexLockGuard& lock, Time::time_point timeout);

	template <typename Predicate>
	bool wait_until(MutexLockGuard& lock, Time::time_point timeout, Predicate&& pred);

	void notify_all();
	void notify_one();

private:
	std::condition_variable _cond;
};

} }

#include "concurrency/Condition.inl"
