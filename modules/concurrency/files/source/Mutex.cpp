#include "concurrency/Mutex.hpp"
#include "concurrency/Thread.hpp"

namespace pm { namespace concurrency {

void Mutex::lock()
{
	_impl.lock();
}

void Mutex::unlock()
{
	_impl.unlock();
}

MutexLockGuard::MutexLockGuard(Mutex& mutex)
: _impl(mutex._impl)
{
}

void MutexLockGuard::reset()
{
	_impl = std::unique_lock<std::mutex>();
}

} }
