#include "concurrency/Thread.hpp"
#include <pthread.h>

namespace pm { namespace concurrency {

void Thread::setName(const char* threadName)
{
	pthread_setname_np(pthread_self(), threadName);
}

} }
