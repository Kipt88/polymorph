#include "concurrency/Thread.hpp"

#include <Cpp11.hpp>
#include <Debug.hpp>
#include <Assert.hpp>

namespace pm { namespace concurrency {

Thread::Data::Data()
: interruptionFlag(false),
  currentCondition(nullptr),
  mutex()
{
}

Thread::Thread()
: _stdThread(),
  _localData(nullptr)
{
}

Thread::Thread(Thread&& other)
: _stdThread(std::move(other._stdThread)),
  _localData(std::move(other._localData))
{
}

Thread::~Thread()
{
	// NOTE Seems this will cause deadlock if thread is being destroyed
	//      during static de-initialization.

	if (_stdThread.joinable())
	{
		interrupt();
		_stdThread.join();
	}
}

Thread& Thread::operator=(Thread&& other)
{
	// Let current thread die before assigning it to new.
	if (_stdThread.joinable())
	{
		interrupt();
		_stdThread.join();
	}

	_stdThread = std::move(other._stdThread);
	_localData = std::move(other._localData);

	return *this;
}

void Thread::join()
{
	interruptionPoint();

	if (_stdThread.joinable())
		_stdThread.join();
}

void Thread::interrupt()
{
	std::condition_variable* condition = nullptr;

	{
		std::unique_lock<std::mutex> lock(_localData->mutex);

		_localData->interruptionFlag = true;
		condition = _localData->currentCondition;
	}

	if (condition != nullptr)
		condition->notify_all();
}

const Thread::id_t Thread::getID() const
{
	return _stdThread.get_id();
}

void Thread::sleep(const Time::duration& timeDuration)
{
	Data* localData = threadLocalData();

	if (localData != nullptr)
	{
		std::condition_variable condition;

		{
			std::unique_lock<std::mutex> lock(localData->mutex);

			localData->currentCondition = &condition;

			if (condition.wait_for(
					lock, 
					timeDuration,
					[localData]{ return localData->interruptionFlag; })
			   )
			{
				localData->interruptionFlag = false;
				throw InterruptException();
			}
		}
	}
	else
	{
		// Non-pm threads may sleep without interruption.
		std::this_thread::sleep_for(timeDuration);
	}
}

void Thread::sleepUntil(const Time::time_point& t)
{
	Data* localData = threadLocalData();

	if (localData != nullptr)
	{
		std::condition_variable condition;

		{
			std::unique_lock<std::mutex> lock(localData->mutex);

			localData->currentCondition = &condition;

			if (condition.wait_until(
					lock,
					t,
					[localData] { return localData->interruptionFlag; })
				)
			{
				localData->interruptionFlag = false;
				throw InterruptException();
			}
		}
	}
	else
	{
		// Non-pm threads may sleep without interruption.
		std::this_thread::sleep_for(t - Time::now());
	}
}

void Thread::interruptionPoint()
{
	Data* localData = threadLocalData();

	// No-op for non-pm threads.
	if (localData == nullptr)
		return;

	{
		std::unique_lock<std::mutex> lock(localData->mutex);

		if (localData->interruptionFlag)
		{
			localData->interruptionFlag = false;
			throw InterruptException();
		}
	}
}

void Thread::interruptCurrent()
{
	Data* localData = threadLocalData();

	if (localData != nullptr)
	{
		std::unique_lock<std::mutex> lock(localData->mutex);
		localData->interruptionFlag = false;
	}

	throw InterruptException();
}

void Thread::setInterrupted(bool interrupted)
{
	Data* localData = threadLocalData();

	if (localData != nullptr)
	{
		std::unique_lock<std::mutex> lock(localData->mutex);
		localData->interruptionFlag = interrupted;
	}
}

const Thread::id_t Thread::getCurrentID()
{
	return std::this_thread::get_id();
}

void Thread::setCondition(std::condition_variable* cond)
{
	Data* localData = threadLocalData();

	if (localData != nullptr)
	{
		std::unique_lock<std::mutex> lock(localData->mutex);
		localData->currentCondition = cond;
	}
}

Thread::Data* Thread::threadLocalData(Data* setter)
{
	thread_local static Data* localData = nullptr;

	if (setter != nullptr)
	{
		ASSERT(localData == nullptr, "Trying to overwrite current thread data.");
		localData = setter;
	}

	return localData;
}

} }
