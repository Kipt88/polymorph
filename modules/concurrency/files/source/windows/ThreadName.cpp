#include "concurrency/Thread.hpp"
#include <Windows.h>

namespace pm { namespace concurrency {

void Thread::setName(const char* threadName)
{
#pragma pack(push,8)
	struct THREADNAME_INFO
	{
		DWORD dwType;     // Must be 0x1000.
		LPCSTR szName;    // Pointer to name (in user addr space).
		DWORD dwThreadID; // Thread ID (-1=caller thread).
		DWORD dwFlags;    // Reserved for future use, must be zero.
	};
#pragma pack(pop)

	const DWORD MS_VC_EXCEPTION = 0x406D1388;
	THREADNAME_INFO info = { 0x1000, threadName, GetCurrentThreadId(), 0 };

	__try {
		// NOTE: this is a Visual Studio specific trick which does nothing for other debuggers
		RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
	}
	__except (EXCEPTION_EXECUTE_HANDLER) { }
}

} }
