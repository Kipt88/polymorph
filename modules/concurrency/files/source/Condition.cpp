#include "concurrency/Condition.hpp"
#include "concurrency/Thread.hpp"

#include <Scoped.hpp>

#if defined(_MSC_VER)
#include <windows.h>
#endif

namespace pm { namespace concurrency {

void Condition::notify_one()
{
	_cond.notify_one();
}

void Condition::notify_all()
{
	_cond.notify_all();
}

bool Condition::wait_for(MutexLockGuard& lock, Time::duration timeout)
{
#if defined(_MSC_VER)
	auto winErrScope = at_exit([oldError = GetLastError()]{ SetLastError(oldError); });
#endif

	auto conditionSetScope = make_scoped(
		[this] { Thread::setCondition(&_cond);  },
		[]     { Thread::setCondition(nullptr); }
	);

	{
		return _cond.wait_for(
			lock._impl,
			timeout,
			[&]
			{
				Thread::interruptionPoint();
				return true;
			}
		);
	}
}

bool Condition::wait_until(MutexLockGuard& lock, Time::time_point timeout)
{
#if defined(_MSC_VER)
	auto winErrScope = at_exit([oldError = GetLastError()]{ SetLastError(oldError); });
#endif

	auto conditionSetScope = make_scoped(
		[this] { Thread::setCondition(&_cond);  },
		[]     { Thread::setCondition(nullptr); }
	);

	{
		return _cond.wait_until(
			lock._impl,
			timeout,
			[&]
			{
				Thread::interruptionPoint();
				return true;
			}
		);
	}
}

} }
