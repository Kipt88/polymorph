#include "concurrency/Thread.hpp"

#import <Foundation/Foundation.h>

namespace pm { namespace concurrency {

void Thread::setName(const char* threadName)
{
	[[NSThread currentThread] setName:@(threadName)];
}

} }
