#include "concurrency/DynamicTask.hpp"

namespace pm { namespace concurrency {

template <typename R, typename... Args>
DynamicTask::DynamicTask(Task<R(Args...)>&& task, Args... args)
: _callable()
{
	_callable = [t = std::move(task), args...]() mutable
	{
		t(args);
	};
}

DynamicTask::DynamicTask(DynamicTask&& other)
: _callable(std::move(other._callable))
{
}

void DynamicTask::operator()()
{
	_callable();
}

} }
