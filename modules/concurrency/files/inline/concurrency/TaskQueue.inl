#include "concurrency/TaskQueue.hpp"

#include <utility>

#include <Debug.hpp>

namespace pm { namespace concurrency {

namespace
{
	template <typename R, typename... Args>
	Task<R(Args...)> popAndGet(std::queue<Task<R(Args...)>>& tasks)
	{
		tasks.front().validAssert();

		auto task = std::move(tasks.front());
		tasks.pop();

		return std::move(task);
	}

	template <typename... Args>
	UniqueFunction<void(Args...)> popAndGet(std::queue<UniqueFunction<void(Args...)>>& tasks)
	{
		UniqueFunction<void(Args...)> task(std::move(tasks.front()));
		tasks.pop();

		return std::move(task);
	}
}

// TaskQueueImpl:

template <typename R, typename... Args>
template <typename Callable>
Future<R> TaskQueueImpl<R(Args...)>::pushAction(Callable&& action)
{
	task_t task(std::forward<Callable>(action));
	Future<R> future(task.getFuture());

	_tasks.push(std::move(task));

	_tasks.front().validAssert();

	return std::move(future);
}

template <typename R, typename... Args>
void TaskQueueImpl<R(Args...)>::doAction(Args... params)
{
	auto&& task = popAndGet(_tasks);

	task(std::forward<Args>(params)...);
}

template <typename R, typename... Args>
std::size_t TaskQueueImpl<R(Args...)>::size() const
{
	return _tasks.size();
}

template <typename R, typename... Args>
bool TaskQueueImpl<R(Args...)>::empty() const
{
	return _tasks.empty();
}

template <typename R, typename... Args>
void TaskQueueImpl<R(Args...)>::clear()
{
	_tasks = std::queue<task_t>();
}

// DynamicTaskQueueImpl:

template <typename... Args>
template <typename Callable>
auto TaskQueueImpl<dynamic_return_tag(Args...)>::pushAction(Callable&& action)
{
	Task<std::result_of_t<Callable(Args...)>(Args...)> task(std::forward<Callable>(action));
	auto&& future = task.getFuture();

	_tasks.push(std::move(task));

	return std::move(future);
}

template <typename... Args>
void TaskQueueImpl<dynamic_return_tag(Args...)>::doAction(Args... params)
{
	auto&& task = popAndGet(_tasks);

	task(std::forward<Args>(params)...);
}

template <typename... Args>
std::size_t TaskQueueImpl<dynamic_return_tag(Args...)>::size() const
{
	return _tasks.size();
}

template <typename... Args>
bool TaskQueueImpl<dynamic_return_tag(Args...)>::empty() const
{
	return _tasks.empty();
}

template <typename... Args>
void TaskQueueImpl<dynamic_return_tag(Args...)>::clear()
{
	_tasks = std::queue<UniqueFunction<void(Args...)>>();
}

// TaskQueue:

template <typename R, typename... Args>
auto TaskQueue<R(Args...)>::access() -> AccessScope
{
	return AccessScope(_queue, concurrency::MutexLockGuard(_mutex));
}


// TaskQueue::AccessScope

template <typename R, typename... Args>
TaskQueue<R(Args...)>::AccessScope::AccessScope(TaskQueueImpl<R(Args...)>& queue,
												concurrency::MutexLockGuard&& lock)
: _queue(queue),
  _lock(std::move(lock))
{
}

template <typename R, typename... Args>
void TaskQueue<R(Args...)>::AccessScope::reset()
{
	_lock.reset();
}

template <typename R, typename... Args>
template <typename Callable>
auto TaskQueue<R(Args...)>::AccessScope::pushAction(Callable&& action)
{
	return _queue.pushAction(std::forward<Callable>(action));
}

template <typename R, typename... Args>
void TaskQueue<R(Args...)>::AccessScope::doAction(Args... args)
{
	_queue.doAction(std::forward<Args>(args)...);
}

template <typename R, typename... Args>
std::size_t TaskQueue<R(Args...)>::AccessScope::size() const
{
	return _queue.size();
}

template <typename R, typename... Args>
bool TaskQueue<R(Args...)>::AccessScope::empty() const
{
	return _queue.empty();
}

template <typename R, typename... Args>
void TaskQueue<R(Args...)>::AccessScope::clear()
{
	_queue.clear();
}

template <typename R, typename... Args>
MutexLockGuard& TaskQueue<R(Args...)>::AccessScope::lockGuard()
{
	return _lock;
}

} }