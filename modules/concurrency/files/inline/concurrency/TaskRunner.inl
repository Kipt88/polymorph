#include "concurrency/TaskRunner.hpp"

#include <Scoped.hpp>

namespace pm { namespace concurrency { 

template <typename R>
TaskRunner<R>::TaskRunner()
: _actions(),
  _pushCond(),
  _runner()
{
	_runner = [this]
	{
		auto queue = _actions.access();

		for (;;)
		{
			_pushCond.wait(queue.lockGuard(), [&] { return !queue.empty(); });
			queue.doAction();
		}
	};
}

template <typename R>
template <typename Callable>
Future<std::result_of_t<Callable()>> TaskRunner<R>::pushAction(Callable&& action)
{
	auto notification = at_exit([&]{ _pushCond.notify_one(); });

	{
		auto queue = _actions.access();
		return queue.pushAction(std::forward<Callable>(action));
	}
}

} }
