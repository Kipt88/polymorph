#include "concurrency/Thread.hpp"

#include <Debug.hpp>

namespace pm { namespace concurrency {

template <typename Callable>
Thread::Thread(Callable&& action)
: _stdThread(),
  _localData(new Data)
{
	_stdThread = std::thread(
		[
		  localData = _localData.get(),
		  runner = std::forward<Callable>(action)
		]() mutable
		{
			const auto* THREAD_LOG_TAG = "Thread";

			threadLocalData(localData);

			std::hash<std::thread::id> hasher;
			std::size_t id = hasher(std::this_thread::get_id());

			VERBOSE_OUT(THREAD_LOG_TAG, "Thread starting %zu", id);

			try
			{
				runner();
			}
			catch (InterruptException&)
			{
				// Normal exit.
			}
			catch (std::exception& e)
			{
				ERROR_OUT(THREAD_LOG_TAG,
						  "Exception caught while running thread\nMessage: %s",
						  e.what());
				std::terminate();
			}
			catch (...)
			{
				ERROR_OUT(THREAD_LOG_TAG, "Unknown exception caught while running thread");
				std::terminate();
			}
			VERBOSE_OUT(THREAD_LOG_TAG, "Thread exited %ul", id);
		}
	);
}

} }