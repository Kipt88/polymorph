#include "concurrency/Condition.hpp"

#include "concurrency/Thread.hpp"

#include <Scoped.hpp>

#if defined(_MSC_VER)
	#include <windows.h>
#endif

namespace pm { namespace concurrency {

template <typename Predicate>
void Condition::wait(MutexLockGuard& lock, Predicate&& pred)
{
	Thread::interruptionPoint();

	auto conditionSetScope = make_scoped(
		[this] { Thread::setCondition(&_cond);  },
		[]     { Thread::setCondition(nullptr); }
	);

	{
		_cond.wait(
			lock._impl,
			[&]
			{
				Thread::interruptionPoint();
				return pred();
			}
		);
	}
}

template <typename Predicate>
bool Condition::wait_for(MutexLockGuard& lock, Time::duration timeout, Predicate&& pred)
{
#if defined(_MSC_VER)
	auto winErrScope = at_exit([oldError = GetLastError()] { SetLastError(oldError); });
#endif

	auto conditionSetScope = make_scoped(
		[this] { Thread::setCondition(&_cond);  },
		[]     { Thread::setCondition(nullptr); }
	);

	{
		return _cond.wait_for(
			lock._impl,
			timeout,
			[&]
			{
				Thread::interruptionPoint();
				return pred();
			}
		);
	}
}

template <typename Predicate>
bool Condition::wait_until(MutexLockGuard& lock, Time::time_point timeout, Predicate&& pred)
{
#if defined(_MSC_VER)
	auto winErrScope = at_exit([oldError = GetLastError()] { SetLastError(oldError); });
#endif

	auto conditionSetScope = make_scoped(
		[this] { Thread::setCondition(&_cond);  },
		[]     { Thread::setCondition(nullptr); }
	);

	{
		return _cond.wait_for(
			lock._impl,
			timeout - Time::now(),
			[&]
			{
				Thread::interruptionPoint();
				return pred();
			}
		);
	}
}

} }
