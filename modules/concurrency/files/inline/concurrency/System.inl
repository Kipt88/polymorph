#include "concurrency/System.hpp"

#include "concurrency/FrameTime.hpp"
#include <profiler/Markers.hpp>

#include <Scoped.hpp>

namespace pm { namespace concurrency {

template <typename Runner>
System<Runner>::System(const Time::duration& sync)
: _actions(),
  _sync(sync),
  _pushCond(),
  _alive(true)
{
}

template <typename Runner>
template <typename... CtorArgs>
void System<Runner>::enter(CtorArgs&&... args)
{
	const auto* SYS_TAG = Runner::name;
	Thread::setName(SYS_TAG);

	profiler::Block block("%s init", SYS_TAG);

	DEBUG_OUT(SYS_TAG, "Starting system %p", this);

	auto atExit = at_exit(
		[&]
		{
			DEBUG_OUT(SYS_TAG, "Killing system %p", this);

			{
				auto queue = _actions.access();

				_alive = false;
				queue.clear();
			}
		}
	);
	
	Runner runner(_actions, args...);

	auto frameStartTime = Time::now();

	//---------------- Main system loop ----------------//

	for (;;)
	{
		block("update");

		// DEBUG_OUT(SYS_TAG, "Sync miss: %.4f", toSeconds<float>(Time::now() - frameStartTime));

		FrameTime frameTime;
		frameTime.t0 = frameStartTime;
		frameTime.t1 = (frameStartTime += _sync);
		frameTime.delta = _sync;

		//---------------- Runner updating ----------------//

		if (!runner.update(frameTime))
		{
			DEBUG_OUT(SYS_TAG, "Normal system exit %p", this);
			return;
		}

		// DEBUG_OUT(SYS_TAG, "Update time: %.4fs", toSeconds<float>(Time::now() - frameStartTime));

		//---------------- Action updating ----------------//

		block("wait");

		{
			auto queue = _actions.access();

			while (_pushCond.wait_until(queue.lockGuard(), frameStartTime, [&] { return !queue.empty(); }))
			{
				// profiler::Block block("action");
				queue.doAction(runner);
			}
		}
		/*
		DEBUG_OUT(SYS_TAG, "Post action time: %.4fs", toSeconds<float>(Time::now() - frameStartTime));
		DEBUG_OUT(SYS_TAG, "Real update time: %.4fs", toSeconds<float>(Time::now() - t0));
		*/
	}
}

template <typename Runner>
template <typename Callable>
concurrency::Future<std::result_of_t<Callable(Runner&)>> System<Runner>::pushAction(Callable&& action)
{
	auto atExit = at_exit([&]{ _pushCond.notify_one(); });

	{
		auto queue = _actions.access();

		if (!_alive)
			throw SystemDeadException();

		auto&& future = queue.pushAction(std::forward<Callable>(action));

		return std::move(future);
	}
}

template <typename Runner>
template <typename... CtorArgs>
ConcurrentSystem<Runner>::ConcurrentSystem(const Time::duration& sync, CtorArgs&&... args)
: _system(sync),
  _runner()
{
	_runner = [=] { _system.enter(args...); };
}

template <typename Runner>
template <typename Callable>
Future<std::result_of_t<Callable(Runner&)>> ConcurrentSystem<Runner>::pushAction(Callable&& action)
{
	return _system.pushAction(std::forward<Callable>(action));
}

} }
