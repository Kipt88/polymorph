#include "concurrency/Task.hpp"

#include "concurrency/Thread.hpp"

#include <Assert.hpp>
#include <Debug.hpp>
#include <SharedFunction.hpp>

#include <utility>
#include <exception>

namespace pm { namespace concurrency {

// Future:

template <typename R>
Future<R>::Future(std::future<R>&& stdFuture)
: _future(std::move(stdFuture))
{
}

template <typename R>
Future<R>::Future(Future<R>&& other)
: _future(std::move(other._future))
{
}

template <typename R>
R Future<R>::get()
{
	try
	{
		return _future.get();
	}
	catch (std::future_error& e)
	{
		ASSERT(e.code() == std::future_errc::broken_promise, 
			   "Expected broken_promise error code.");

		(void)e;

		throw TaskDeadException();
	}
}

template <typename R>
bool Future<R>::ready() const
{
	return _future.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
}

// Task:

template <typename R, typename... Args>
template <typename Callable, typename>
Task<R(Args...)>::Task(Callable&& callable)
: _task()
{
	_task = std::packaged_task<R(Args...)>(
		SharedFunction<R(Args...)>(
			[action = std::forward<Callable>(callable)](Args&&... args) mutable
			{
				try
				{
					return action(std::forward<Args>(args)...);
				}
				catch (InterruptException&)
				{
					// If thread is interrupted inside task, abort task and 
					// interrupt thread after call.

					Thread::setInterrupted();
					throw TaskInterruptException();
				}
			}
		)
	);

	ASSERT(_task.valid(), "Tasks should never be invalid.");
}

template <typename R, typename... Args>
Task<R(Args...)>::Task(Task<R(Args...)>&& other)
: _task(std::move(other._task))
{
	ASSERT(_task.valid(), "Tasks should never be invalid.");
}

template <typename R, typename... Args>
void Task<R(Args...)>::operator()(Args&&... args) const
{
	_task(std::forward<Args>(args)...);
	
	ASSERT(_task.valid(), "Tasks should never be invalid.");
	
	Thread::interruptionPoint();
}

template <typename R, typename... Args>
Future<R> Task<R(Args...)>::getFuture()
{
	return Future<R>(_task.get_future());
}

template <typename R, typename... Args>
void Task<R(Args...)>::reset()
{
	_task.reset();
}

////
template <typename R, typename... Args>
void Task<R(Args...)>::validAssert() const
{
	ASSERT(_task.valid(), "Tasks should never be invalid.");
}
////

} }