#include <gtest/gtest.h>

#include <concurrency/Task.hpp>
#include <concurrency/TaskQueue.hpp>
#include <concurrency/Thread.hpp>
#include <concurrency/TaskRunner.hpp>

#include <Debug.hpp>
#include <NonCopyable.hpp>
#include <SharedFunction.hpp>

#include <vector>
#include <memory>


class MoveOnly : NonCopyable
{
public:
	MoveOnly() : MoveOnly(3) {}
	MoveOnly(int i_) : i(i_) {}

	MoveOnly(MoveOnly&& other) : i(other.i) {}
	MoveOnly& operator=(MoveOnly&& other)
	{
		i = other.i;
		return *this;
	}

	int i;
};

TEST(Task, startup_shutdown)
{
	{
		pm::concurrency::Task<int()> t1([]{ return 1; });
		auto&& f = t1.getFuture();

		pm::concurrency::Task<int()> t2(std::move(t1));

		t2();

		ASSERT_EQ(f.get(), 1);
	}

	{
		pm::concurrency::Task<MoveOnly()> t([]() -> MoveOnly { return MoveOnly(1); });

		auto&& future = t.getFuture();
		
		t();

		ASSERT_EQ(future.get().i, 1);
	}

	{
		pm::concurrency::Task<int()> task([]{ return 1; });
		auto&& f = task.getFuture();

		pm::concurrency::Thread runner(
			[&]
			{
				task();
			}
		);

		ASSERT_EQ(f.get(), 1);
	}

	{
		std::vector<pm::concurrency::Task<int()>> tasks;

		pm::concurrency::Task<int()> task([]{ return 1; });
		auto&& f = task.getFuture();

		tasks.push_back(std::move(task));
		tasks[0]();
		tasks.clear();

		ASSERT_EQ(f.get(), 1);
	}

	{
		pm::concurrency::Task<void()> task(pm::SharedFunction<void()>([data = MoveOnly(3)]{}));


		task();
	}

	{
		std::vector<pm::concurrency::Task<int()>> tasks;

		pm::concurrency::Task<int()> task1([]{ return 1; });
		auto&& f1 = task1.getFuture();
		tasks.push_back(std::move(task1));

		pm::concurrency::Task<int()> task2([]{ return 2; });
		auto&& f2 = task2.getFuture();
		tasks.push_back(std::move(task2));

		pm::concurrency::Task<int()> task3([]{ return 3; });
		auto&& f3 = task3.getFuture();
		tasks.push_back(std::move(task3));

		pm::concurrency::Thread runner(
			[&]
			{
				for (auto& t : tasks)
					t();

				tasks.clear();
			}
		);

		ASSERT_EQ(f1.get(), 1);
		ASSERT_EQ(f2.get(), 2);
		ASSERT_EQ(f3.get(), 3);
	}
}

PM_MAKE_EXCEPTION_CLASS(TestException, pm::Exception);

TEST(Task, error_handling)
{
	{
		pm::concurrency::Task<void()> t1([]{ return; });
		auto&& f = t1.getFuture();

		{
			// This will destroy the shared return state.
			pm::concurrency::Task<void()> t2(std::move(t1));
		}

		ASSERT_THROW(
			f.get(),
			pm::concurrency::TaskDeadException
		);
	}

	{
		pm::concurrency::Task<void()> task([]{ throw TestException(); });
		
		bool passedTask = false;

		pm::concurrency::Thread runner(
			[&]
			{
				task();
				passedTask = true;
			}
		);

		ASSERT_THROW(
			task.getFuture().get(),
			TestException
		);

		runner.join();

		ASSERT_TRUE(passedTask);
	}
}

TEST(Task, ready)
{
	{
		pm::concurrency::Task<void()> task([]{ return; });
		auto&& f = task.getFuture();

		ASSERT_TRUE(!f.ready());

		task();

		ASSERT_TRUE(f.ready());
	}

	{
		pm::concurrency::Task<void()> task([]{ return; });
		auto&& f = task.getFuture();

		pm::concurrency::Thread runner(
			[&]
			{
				task();
			}
		);

		runner.join();

		ASSERT_TRUE(f.ready());
	}
}

TEST(Task, queue)
{
	{
		pm::concurrency::TaskQueue<int(int)> taskQueue;
		auto&& access = taskQueue.access();

		auto&& f1 = access.pushAction([](int i) { return i + 1; });
		auto&& f2 = access.pushAction([](int i) { return i + 2; });
		auto&& f3 = access.pushAction([](int i) { return i + 3; });

		for (int i = 0; !access.empty(); ++i)
			access.doAction(i);

		ASSERT_EQ(f1.get(), 1);
		ASSERT_EQ(f2.get(), 2 + 1);
		ASSERT_EQ(f3.get(), 3 + 2);
	}

	{
		pm::concurrency::TaskQueue<int(int)> taskQueue;

		{
			auto&& access = taskQueue.access();
			auto&& f1 = access.pushAction([](int i) { return i + 1; });
			auto&& f2 = access.pushAction([](int i) { return i + 2; });
			auto&& f3 = access.pushAction([](int i) { return i + 3; });
			access.reset();

			{
				pm::concurrency::Thread runner(
					[&]
					{
						auto&& access = taskQueue.access();

						for (int i = 0; !access.empty(); ++i)
							access.doAction(i);
					}
				);

				ASSERT_EQ(f1.get(), 1);
				ASSERT_EQ(f2.get(), 2 + 1);
				ASSERT_EQ(f3.get(), 3 + 2);
			}
		}
	}
}

TEST(Task, dynamic_queue)
{
	{
		pm::concurrency::TaskQueue<pm::concurrency::dynamic_return_tag(int)> taskQueue;
		auto&& access = taskQueue.access();

		auto&& f1 = access.pushAction(
			[](int i) -> int { return i + 1; }
		);

		auto&& f2 = access.pushAction(
			[](int i) -> float { return i + 0.5f; }
		);

		auto&& f3 = access.pushAction(
			[](int i) -> std::string { return std::to_string(i); }
		);

		for (int i = 0; !access.empty(); ++i)
			access.doAction(i);

		ASSERT_EQ(f1.get(), 1);
		ASSERT_EQ(f2.get(), 1 + 0.5f);
		ASSERT_EQ(f3.get(), std::to_string(2));
	}

	{
		pm::concurrency::TaskQueue<pm::concurrency::dynamic_return_tag(int)> taskQueue;
		
		auto&& access = taskQueue.access();

		auto&& f1 = access.pushAction(
			[](int i) -> int { return i + 1; }
		);

		auto&& f2 = access.pushAction(
			[](int i) -> float { return i + 0.5f; }
		);

		auto&& f3 = access.pushAction(
			[](int i) -> std::string { return std::to_string(i); }
		);
		
		access.reset();

		pm::concurrency::Thread runner(
			[&]
			{
				auto&& access = taskQueue.access();

				for (int i = 0; !access.empty(); ++i)
					access.doAction(i);
			}
		);

		ASSERT_EQ(f1.get(), 1);
		ASSERT_EQ(f2.get(), 1 + 0.5f);
		ASSERT_EQ(f3.get(), std::to_string(2));
	}
}

TEST(Task, runner)
{
	{
		pm::concurrency::TaskRunner<void> runner;
	}

	{
		pm::concurrency::TaskRunner<void> runner;
		runner.pushAction([] {});
	}

	{
		pm::concurrency::TaskRunner<int> runner;

		auto&& future = runner.pushAction([] { return 2; });

		ASSERT_EQ(future.get(), 2);
	}
}

TEST(Task, dynamic_runner)
{
	{
		pm::concurrency::TaskRunner<pm::concurrency::dynamic_return_tag> runner;
	}

	{
		pm::concurrency::TaskRunner<pm::concurrency::dynamic_return_tag> runner;

		auto&& futureInt = runner.pushAction([] { return 2; });
		auto&& futureVoid = runner.pushAction([] {});

		futureVoid.get();
		ASSERT_TRUE(futureInt.ready());
		ASSERT_EQ(futureInt.get(), 2);
	}
}
