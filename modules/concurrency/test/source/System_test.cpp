#include <gtest/gtest.h>
#include <concurrency/System.hpp>
#include <Time.hpp>

#include <string>

using namespace pm;

struct DummyRunner
{
	static constexpr const char* name = "DummyRunner";
	
	bool update(const concurrency::FrameTime&)
	{
		return true;
	}
};

struct QuitRunner
{
	static constexpr const char* name = "QuitRunner";
	
	bool update(const concurrency::FrameTime&)
	{
		updated = true;
		return false;
	}

	bool updated = false;
};

struct ConstructedRunner
{
	static constexpr const char* name = "ConstructedRunner";
	
	ConstructedRunner(int a, std::string b)
	: i(a), j(b)
	{
	}

	bool update(const concurrency::FrameTime&)
	{
		return true;
	}

	int i;
	std::string j;
};

TEST(System, startup)
{
	{
		pm::concurrency::ConcurrentSystem<DummyRunner> testSys(1ms);
	}

	{
		pm::concurrency::ConcurrentSystem<ConstructedRunner> testSys(
			1ms,
			1, 
			std::string("foobar")
		);
	}
}

TEST(System, shutdown)
{
	{
		pm::concurrency::ConcurrentSystem<QuitRunner> testSys(1ms);
		pm::concurrency::Thread::sleep(100ms); // Should be enough time for runner to run once.
		
		ASSERT_THROW(
			{
				auto&& future = testSys.pushAction(
					[](QuitRunner& runner){ return runner.updated; }
				);
				
				future.get();
			},
			pm::concurrency::SystemDeadException
		);
	}
}

TEST(System, running)
{
	{
		pm::concurrency::ConcurrentSystem<DummyRunner> testSys(1ms);

		auto futureInt = testSys.pushAction(
			[](DummyRunner&) { return 1; }
		);

		auto futureVoid = testSys.pushAction(
			[](DummyRunner&) {}
		);

		futureVoid.get();
		ASSERT_TRUE(futureInt.ready());
		ASSERT_EQ(futureInt.get(), 1);
	}

	{
		pm::concurrency::ConcurrentSystem<ConstructedRunner> testSys(
			1ms,
			1,
			std::string("foobar")
		);

		auto&& futureI = testSys.pushAction(
			[](ConstructedRunner& runner) { return runner.i; }
		);

		auto&& futureJ = testSys.pushAction(
			[](ConstructedRunner& runner) { return runner.j; }
		);

		ASSERT_EQ(futureI.get(), 1);
		ASSERT_EQ(futureJ.get(), "foobar");
	}
}

