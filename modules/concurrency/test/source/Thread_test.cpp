#include <gtest/gtest.h>

#include <concurrency/Thread.hpp>
#include <concurrency/Mutex.hpp>
#include <concurrency/Condition.hpp>

TEST(Thread, startup_shutdown)
{
	{
		pm::concurrency::Thread testThread;
	}

	{
		bool flag = false;

		{
			pm::concurrency::Thread testThread([&] { flag = true; });
		}

		ASSERT_TRUE(flag);
	}

	{
		bool flag = false;

		{
			pm::concurrency::Thread testThread([&]() { flag = true; });
			pm::concurrency::Thread testThread2(std::move(testThread));
		}

		ASSERT_TRUE(flag);
	}

	{
		bool flag = false;

		{
			pm::concurrency::Thread testThread;
			pm::concurrency::Thread testThread2([&]() { flag = true; });

			testThread = std::move(testThread2);
		}

		ASSERT_TRUE(flag);
	}
}

TEST(Thread, interruption)
{
	{
		pm::concurrency::Thread testThread(
			[&]
			{
				for (;;)
					pm::concurrency::Thread::interruptionPoint();
			}
		);


		testThread.interrupt();
		testThread.join();
	}

	{
		pm::concurrency::Thread testThread(
			[&]
			{
				for (;;)
					pm::concurrency::Thread::sleep(100000ms);
			}
		);

		testThread.interrupt();
		testThread.join();
	}

	{
		bool ran = false;

		{
			pm::concurrency::Condition cond;
			pm::concurrency::Mutex mutex;

			bool kill = false;


			pm::concurrency::Thread testThread(
				[&]
				{
					pm::concurrency::MutexLockGuard lock(mutex);
					cond.wait(lock, [&]{ return kill; });
					ran = true;
				}
			);

			{
				pm::concurrency::MutexLockGuard lock(mutex);
				kill = true;
			}

			cond.notify_all();

			testThread.join();
		}
		
		ASSERT_TRUE(ran);
	}

	{
		bool ran = false;

		{
			pm::concurrency::Condition cond;
			pm::concurrency::Mutex mutex;

			bool kill = false;

			pm::concurrency::Thread testThread(
				[&]
				{
					pm::concurrency::MutexLockGuard lock(mutex);
					cond.wait(lock, [&]{ return kill; });
					ran = true;
				}
			);
		}

		ASSERT_FALSE(ran);
	}

	{
		bool ran = false;
		bool ran2 = false;

		{
			pm::concurrency::Condition cond;
			pm::concurrency::Mutex mutex;

			bool kill = false;

			pm::concurrency::Thread testThread(
				[&]
				{
					try
					{
						pm::concurrency::MutexLockGuard lock(mutex);
						cond.wait(lock, [&]{ return kill; });
						ran = true;
					}
					catch (...)
					{
						pm::concurrency::Thread::sleep(15ms);
						ran2 = true;

						std::rethrow_exception(std::current_exception());
					}
				}
			);
		}

		ASSERT_FALSE(ran);
		ASSERT_TRUE(ran2);
	}
}
