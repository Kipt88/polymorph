include("../../options.cmake")

set(
	dependent_modules
	"ext"
	"filesystem"
	"math"
)

set(
	source_files
	"source/NavigationMesh.cpp"
	"source/NavigationMeshReader.cpp"
)

set(
	inline_files
)

set(
	include_files
	"include/ai/NavigationMesh.hpp"
	"include/ai/NavigationMeshReader.hpp"
)

require_modules(${dependent_modules})

source_group("Sources" FILES ${common_source_files})
source_group("Inlines" FILES ${common_inline_files})
source_group("Headers" FILES ${common_include_files})

add_library("ai" STATIC ${source_files} ${include_files} ${inline_files})

target_include_directories("ai" PUBLIC "include" "inline")

target_link_libraries("ai" PUBLIC ${dependent_modules})