#include "ai/NavigationMesh.hpp"

#include <math/Geometry.hpp>
#include <optional.hpp>
#include <set>
#include <map>

namespace pm { namespace ai {

namespace {

std::optional<face_index_t> findEnclosingFace(const NavigationMesh::Topology& mesh, const Vector2_r& point)
{
	// TODO Implement spacial partitioner.

	for (face_index_t i = 0; i < mesh.faceSize(); ++i)
	{
		const std::vector<vertex_index_t>& vertexIndices = mesh.faceVertices(i);

		std::vector<Vector2_r> vertices;
		vertices.reserve(vertexIndices.size());

		for (vertex_index_t index : vertexIndices)
			vertices.push_back(mesh.vertex(index));

		if (math::insideConvex(point, vertices))
			return i;
	}

	return std::nullopt;
}

ai::Vector2_r centerOfEdge(edge_index_t edge, const NavigationMesh::Topology& mesh)
{
	auto vertexIndices = mesh.edgeVertices(edge);

	auto v0 = mesh.vertex(vertexIndices[0]);
	auto v1 = mesh.vertex(vertexIndices[1]);

	return 0.5_r * (v0 + v1);
}

std::vector<Portal> generatePath(
	const face_index_t& start,
	const std::vector<edge_index_t>& edges,
	const face_index_t& end,
	real_t radius,
	const NavigationMesh::Topology& mesh
)
{
	std::vector<Portal> path;

	path.reserve(edges.size());

	Vector2_r startCenter = Vector2_r::ZERO;

	{
		auto vertexIndices = mesh.faceVertices(start);

		for (vertex_index_t index : vertexIndices)
			startCenter += mesh.vertex(index);
		
		startCenter /= static_cast<real_t>(vertexIndices.size());
	}

	{
		auto edgeVertices = mesh.edgeVertices(edges[0]);

		path.push_back(
			Portal{ startCenter, mesh.vertex(edgeVertices[0]), mesh.vertex(edgeVertices[1]) }
		);
	}

	for (unsigned int i = 1; i < edges.size(); ++i)
	{
		auto edgeVertices = mesh.edgeVertices(edges[i]);

		path.push_back(
			Portal{ path[i - 1], mesh.vertex(edgeVertices[0]), mesh.vertex(edgeVertices[1]) }
		);
	}

	return path;
}

}

Portal::Portal(const Portal& previous, const Vector2_r& a, const Vector2_r& b)
: Portal(previous.center(), a, b)
{
}

Portal::Portal(const Vector2_r& previousPoint, const Vector2_r& a, const Vector2_r& b)
: _centerPoint(),
  _forwardDir(),
  _points({ a, b })
{
	ASSERT(math::lengthSq(b - a) > 0.0_r, "Portal size is zero.");

	_centerPoint = 0.5_r * (a + b);
	
	Vector2_r toCenter = _centerPoint - previousPoint;

	ASSERT(toCenter != Vector2_r::ZERO, "Previous point is on portal.");

	Vector2_r aToB = b - a;
	Vector2_r v = { -aToB[1], aToB[0] };

	if (toCenter * v > 0)
		_forwardDir = v;
	else
		_forwardDir = -v;

	math::normalizeSelf(_forwardDir);
}

const Vector2_r& Portal::center() const
{
	return _centerPoint;
}

const Vector2_r& Portal::forward() const
{
	return _forwardDir;
}

const std::array<Vector2_r, 2>& Portal::points() const
{
	return _points;
}

Vector2_r Portal::closestPoint(const Vector2_r& from, real_t radius) const
{
	Vector2_r A = _points[0];
	Vector2_r B = _points[1];

	Vector2_r AtoB = B - A;
	Vector2_r AtoP = from - A;

	real_t t = (AtoB * AtoP) / math::lengthSq(AtoB);

	real_t minT = radius / math::length(AtoB);
	real_t maxT = 1.0_r - radius / math::length(AtoB);

	ASSERT(minT < maxT, "");

	t = math::clamped(t, minT, maxT);

	return A + t * AtoB;
}

int Portal::sign(const Vector2_r& point) const
{
	real_t res = (_centerPoint - point) * _forwardDir;

	if (res > 0.0_r)
		return 1;
	else if (res < 0.0_r)
		return -1;
	
	return 0;
}

NavigationMesh::NavigationMesh(Topology&& topology)
: _topology(std::move(topology))
{
}

auto NavigationMesh::findPath(const Vector2_r& from, const Vector2_r& to, real_t radius) const -> std::optional<path_t>
{
	face_index_t startFace;
	face_index_t endFace;

	if (auto face = findEnclosingFace(_topology, from))
		startFace = *face;
	else
		return std::nullopt;

	if (auto face = findEnclosingFace(_topology, to))
		endFace = *face;
	else
		return std::nullopt;

	if (startFace == endFace)
		return path_t{ from, to, {} };

	// TODO Optimize a bit at least...

	std::set<edge_index_t> closed;
	std::set<edge_index_t> open;

	std::map<edge_index_t, real_t> gScore;
	std::map<edge_index_t, real_t> fScore;

	std::map<edge_index_t, edge_index_t> cameFrom;

	{
		auto startEdges = _topology.faceEdges(startFace);

		for (auto edge : startEdges)
		{
			auto faces = _topology.edgeFaces(edge);

			// Edges that don't have faces on either end can't be traversed.
			if (!(faces[0] && faces[1]))
			{
				closed.insert(edge);
				continue;
			}

			auto vertexIndices = _topology.edgeVertices(edge);

			auto v0 = _topology.vertex(vertexIndices[0]);
			auto v1 = _topology.vertex(vertexIndices[1]);

			if (math::length(v1 - v0) < std::sqrt(math::square(radius * 2.0_r)))
			{
				closed.insert(edge);
				continue;
			}

			open.insert(edge);

			auto vCenter = centerOfEdge(edge, _topology);

			gScore[edge] = math::length(vCenter - from);
			fScore[edge] = gScore[edge] + math::length(to - vCenter);
		}
	}

	while (!open.empty())
	{
		auto it = std::min_element(
			open.begin(), 
			open.end(), 
			[&](edge_index_t a, edge_index_t b)
			{
				return fScore.find(a)->second < fScore.find(b)->second;
			}
		);

		edge_index_t edge = *it;

		auto faces = _topology.edgeFaces(edge);

		ASSERT(faces[0] && faces[1], "Edge without both faces should already be exluded.");

		{
			if (*faces[0] == endFace || *faces[1] == endFace)
			{
				std::vector<edge_index_t> edges;

				edges.push_back(edge);

				for (auto it = cameFrom.find(edge); it != cameFrom.end(); it = cameFrom.find(it->second))
				{
					edges.push_back(it->second);
				}

				std::reverse(edges.begin(), edges.end());

				return path_t{ from, to, generatePath(startFace, edges, endFace, radius, _topology) };
			}
		}

		open.erase(it);
		closed.insert(edge);

		std::vector<edge_index_t> reachableEdges;

		// Add all non-visited adjacent edges that have faces on both sides and fit radius.

		for (const auto& face : faces)
		{
			auto faceEdges = _topology.faceEdges(*face);

			for (auto e : faceEdges)
			{
				if (closed.find(e) != closed.end())
					continue;

				auto nextFaces = _topology.edgeFaces(e);

				if (!(nextFaces[0] && nextFaces[1]))
				{
					closed.insert(e);
					continue;
				}

				auto vertexIndices = _topology.edgeVertices(e);

				auto v0 = _topology.vertex(vertexIndices[0]);
				auto v1 = _topology.vertex(vertexIndices[1]);

				if (math::length(v1 - v0) < std::sqrt(math::square(radius)))
				{
					closed.insert(e);
					continue;
				}

				reachableEdges.push_back(e);
			}
		}

		real_t edgeScore = gScore.find(edge)->second;

		for (auto neighbor : reachableEdges)
		{
			ASSERT(closed.find(neighbor) == closed.end(), "Revisiting...");

			real_t score = edgeScore + math::length(centerOfEdge(edge, _topology) - centerOfEdge(neighbor, _topology));

			if (open.find(neighbor) == open.end())
				open.insert(neighbor);
			else if (score >= gScore.find(neighbor)->second)
				continue;
			
			cameFrom[neighbor] = edge;
			gScore[neighbor] = score;
			fScore[neighbor] = score + math::length(centerOfEdge(neighbor, _topology) - to);
		}
	}

	return std::nullopt;
}

const NavigationMesh::Topology& NavigationMesh::topology() const
{
	return _topology;
}

} }
