#include "ai/NavigationMeshReader.hpp"

namespace pm { namespace ai {

namespace NavigationMeshReader {

NavigationMesh read(io::InputStream& input)
{
	NavigationMesh::Topology mesh;

	{
		std::uint32_t vertexSize;
		input >> vertexSize;

		for (unsigned int i = 0; i < vertexSize; ++i)
		{
			float x, y;

			input >> x >> y;

			mesh.addVertex(Vector2_r{ x, y });
		}
	}

	{
		std::uint32_t edgeSize;
		input >> edgeSize;

		for (unsigned int i = 0; i < edgeSize; ++i)
		{
			std::uint32_t v1, v2;

			input >> v1 >> v2;

			mesh.addEdge({}, v1, v2);
		}
	}

	{
		std::uint32_t faceSize;
		input >> faceSize;

		for (unsigned int i = 0; i < faceSize; ++i)
		{
			std::uint32_t edgeSize;
			input >> edgeSize;

			std::vector<edge_index_t> edges;
			edges.reserve(edgeSize);

			for (unsigned int j = 0; j < edgeSize; ++j)
			{
				std::uint32_t edge;
				input >> edge;

				edges.push_back(edge);
			}

			mesh.addFace({}, edges);
		}
	}

	return NavigationMesh{ std::move(mesh) };
}

}

} }
