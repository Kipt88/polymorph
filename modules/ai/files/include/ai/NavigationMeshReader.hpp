#pragma once

#include "ai/NavigationMesh.hpp"
#include <io/IOStream.hpp>

namespace pm { namespace ai {

namespace NavigationMeshReader {

NavigationMesh read(io::InputStream& input);

}

} }
