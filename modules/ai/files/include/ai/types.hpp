#pragma once

#include <math/types.hpp>
#include <math/Matrix.hpp>

namespace pm { namespace ai {

typedef math::real_t real_t;
typedef math::Vector<real_t, 2> Vector2_r;

constexpr real_t operator""_r(long double d)
{
	return static_cast<real_t>(d);
}

} }
