#pragma once

#include "ai/types.hpp"
#include <WingedEdgeMesh.hpp>
#include <vector>

namespace pm { namespace ai {

class Portal
{
public:
	explicit Portal(const Portal& previous, const Vector2_r& a, const Vector2_r& b);
	explicit Portal(const Vector2_r& previousPoint, const Vector2_r& a, const Vector2_r& b);

	const Vector2_r& center() const;
	const Vector2_r& forward() const;

	const std::array<Vector2_r, 2>& points() const;

	Vector2_r closestPoint(const Vector2_r& from, real_t radius) const;

	int sign(const Vector2_r& point) const;

private:
	Vector2_r _centerPoint;
	Vector2_r _forwardDir;

	std::array<Vector2_r, 2> _points;
};

class NavigationMesh
{
public:
	typedef WingedEdgeMesh<Vector2_r> Topology;
	
	struct path_t
	{
		Vector2_r from;
		Vector2_r to;
		std::vector<Portal> portals;
	};

	explicit NavigationMesh(Topology&& topology);

	std::optional<path_t> findPath(const Vector2_r& from, const Vector2_r& to, real_t radius = 0.0_r) const;
	const Topology& topology() const;

private:
	Topology _topology;
};

} }
