require_modules("ai")

set(
	source_files
	"source/NavigationMesh_test.cpp"
)

source_group("Sources" ".*cpp")

find_package(GTest REQUIRED)

add_executable("test_ai" ${source_files} ${GTest_SOURCES})

target_include_directories(
	"test_ai" PRIVATE
	"include" ${GTest_INCLUDE_DIRS}
)

target_link_libraries(
	"test_ai" PRIVATE
	"ai" ${GTest_LIBRARIES}
)

add_test(NAME test_ai COMMAND test_ai)
