#include <ai/NavigationMesh.hpp>
#include <gtest/gtest.h>

using namespace pm;
using namespace pm::ai;

namespace {

// Returned mesh:
//
// 7 - - - - - 5
// |\    2    /|
// | 6 - - - 4 |
// | |XXXXXXX| |
// |3|XXXXXXX|1|
// | |XXXXXXX| |
// | 0 - - - 2 |
// |/    0    \|
// 1 - - - - - 3
//
NavigationMesh createSquareHoleMesh(real_t w, real_t h)
{
	NavigationMesh::Topology topology;

	for (unsigned int i = 0; i < 4; ++i)
	{
		real_t signX = (i == 0 || i == 3) ? -1.0_r : 1.0_r;
		real_t signY = (i == 0 || i == 1) ? -1.0_r : 1.0_r;

		topology.addVertex(Vector2_r{ signX * w * 0.25_r, signY * h * 0.25_r });
		topology.addVertex(Vector2_r{ signX * w * 0.5_r, signY * h * 0.5_r });
	}

	auto e01 = topology.addEdge({}, 0, 1); // 0
	auto e02 = topology.addEdge({}, 0, 2); // 1
	auto e06 = topology.addEdge({}, 0, 6); // 2
	auto e13 = topology.addEdge({}, 1, 3); // 3
	auto e17 = topology.addEdge({}, 1, 7); // 4
	auto e23 = topology.addEdge({}, 2, 3); // 5
	auto e24 = topology.addEdge({}, 2, 4); // 6
	auto e35 = topology.addEdge({}, 3, 5); // 7
	auto e45 = topology.addEdge({}, 4, 5); // 8
	auto e46 = topology.addEdge({}, 4, 6); // 9
	auto e57 = topology.addEdge({}, 5, 7); // 10
	auto e67 = topology.addEdge({}, 6, 7); // 11

	topology.addFace({}, { e01, e13, e23, e02 });
	topology.addFace({}, { e23, e35, e45, e24 });
	topology.addFace({}, { e45, e57, e67, e46 });
	topology.addFace({}, { e01, e06, e67, e17 });

	return NavigationMesh{ std::move(topology) };
}

// Returned mesh:
//
// 7 - - - - - 5
// |\    2    /|
// | 6 - - - 4 |
// | |       | |
// |3|   4   |1|
// | |       | |
// | 0 - - - 2 |
// |/    0    \|
// 1 - - - - - 3
//
NavigationMesh createSquareTessellatedMesh(real_t w, real_t h)
{
	NavigationMesh::Topology topology;

	for (unsigned int i = 0; i < 4; ++i)
	{
		real_t signX = (i == 0 || i == 3) ? -1.0_r : 1.0_r;
		real_t signY = (i == 0 || i == 1) ? -1.0_r : 1.0_r;

		topology.addVertex(Vector2_r{ signX * w * 0.25_r, signY * h * 0.25_r });
		topology.addVertex(Vector2_r{ signX * w * 0.5_r, signY * h * 0.5_r });
	}

	auto e01 = topology.addEdge({}, 0, 1); // 0
	auto e02 = topology.addEdge({}, 0, 2); // 1
	auto e06 = topology.addEdge({}, 0, 6); // 2
	auto e13 = topology.addEdge({}, 1, 3); // 3
	auto e17 = topology.addEdge({}, 1, 7); // 4
	auto e23 = topology.addEdge({}, 2, 3); // 5
	auto e24 = topology.addEdge({}, 2, 4); // 6
	auto e35 = topology.addEdge({}, 3, 5); // 7
	auto e45 = topology.addEdge({}, 4, 5); // 8
	auto e46 = topology.addEdge({}, 4, 6); // 9
	auto e57 = topology.addEdge({}, 5, 7); // 10
	auto e67 = topology.addEdge({}, 6, 7); // 11

	topology.addFace({}, { e01, e13, e23, e02 });
	topology.addFace({}, { e23, e35, e45, e24 });
	topology.addFace({}, { e45, e57, e67, e46 });
	topology.addFace({}, { e01, e06, e67, e17 });
	topology.addFace({}, { e02, e24, e46, e06 });

	return NavigationMesh{ std::move(topology) };
}

// Returned mesh:
//
// 0 - - - - - 7
// |           |
// 1 - 2 - 5 - 6
//     |   |
//     3 - 4
//
NavigationMesh createTMesh(real_t w, real_t h)
{
	NavigationMesh::Topology topology;

	auto v0 = topology.addVertex({ -0.50_r * w,  0.5_r * h });
	auto v1 = topology.addVertex({ -0.50_r * w,  0.0_r * h });
	auto v2 = topology.addVertex({ -0.25_r * w,  0.0_r * h });
	auto v3 = topology.addVertex({ -0.25_r * w, -0.5_r * h });
	auto v4 = topology.addVertex({  0.25_r * w, -0.5_r * h });
	auto v5 = topology.addVertex({  0.25_r * w,  0.0_r * h });
	auto v6 = topology.addVertex({  0.50_r * w,  0.0_r * h });
	auto v7 = topology.addVertex({  0.50_r * w,  0.5_r * h });

	auto e01 = topology.addEdge({}, v0, v1); // 0
	auto e07 = topology.addEdge({}, v0, v7); // 1
	auto e12 = topology.addEdge({}, v1, v2); // 2
	auto e23 = topology.addEdge({}, v2, v3); // 3
	auto e25 = topology.addEdge({}, v2, v5); // 4
	auto e34 = topology.addEdge({}, v3, v4); // 5
	auto e45 = topology.addEdge({}, v4, v5); // 6
	auto e56 = topology.addEdge({}, v5, v6); // 7
	auto e67 = topology.addEdge({}, v6, v7); // 8

	topology.addFace({}, { e01, e12, e25, e56, e67, e07 });
	topology.addFace({}, { e23, e34, e45, e25 });

	return NavigationMesh{ std::move(topology) };
}

}

TEST(NavigationMesh, findPath_squareHole)
{
	NavigationMesh squareHoleMesh = createSquareHoleMesh(1.0_r, 1.0_r);

	{
		auto path = squareHoleMesh.findPath(Vector2_r::ZERO, Vector2_r{ 0.0_r, 0.3_r });
		ASSERT_FALSE(path);
	}

	{
		auto path = squareHoleMesh.findPath(Vector2_r{ 0.3_r, 0.3_r }, Vector2_r{ 0.0_r, 5.1_r });
		ASSERT_FALSE(path);
	}

	{
		auto path = squareHoleMesh.findPath(Vector2_r{ 0.3_r, 0.2_r }, Vector2_r{ 0.3_r, -0.2_r });
		ASSERT_TRUE(path);
		ASSERT_EQ(path->portals.size(), 0);
	}

	// From edge 4-5 to edge 2-3. Should not pass the edges themselves.
	/*
	{
		auto path = squareHoleMesh.findPath(Vector2_r{ 0.3_r, 0.3_r }, Vector2_r{ 0.3_r, -0.3_r });
		ASSERT_TRUE(path);
		ASSERT_EQ(path->portals.size(), 0);
	}
	*/

	{
		auto path = squareHoleMesh.findPath(Vector2_r{ 0.0_r, 0.3_r }, Vector2_r{ 0.3_r, 0.2_r });
		ASSERT_TRUE(path);
		ASSERT_EQ(path->portals.size(), 1);
	}

	{
		auto path = squareHoleMesh.findPath(Vector2_r{ 0.1_r, 0.3_r }, Vector2_r{ 0.1_r, -0.3_r });
		ASSERT_TRUE(path);
		ASSERT_EQ(path->portals.size(), 2);
	}
}

TEST(NavigationMesh, findPath_tessellatedSquare)
{
	NavigationMesh squareHoleMesh = createSquareTessellatedMesh(1.0_r, 1.0_r);

	{
		auto path = squareHoleMesh.findPath(Vector2_r::ZERO, Vector2_r{ -0.6_r, 0.3_r });
		ASSERT_FALSE(path);
	}

	{
		auto path = squareHoleMesh.findPath(Vector2_r{ 0.3_r, 0.3_r }, Vector2_r{ 0.0_r, 5.1_r });
		ASSERT_FALSE(path);
	}

	{
		auto path = squareHoleMesh.findPath(Vector2_r{ 0.3_r, 0.2_r }, Vector2_r{ 0.3_r, -0.2_r });
		ASSERT_TRUE(path);
		ASSERT_EQ(path->portals.size(), 0);
	}

	// From edge 4-5 to edge 2-3. Should not pass the edges themselves.
	/*
	{
	auto path = squareHoleMesh.findPath(Vector2_r{ 0.3_r, 0.3_r }, Vector2_r{ 0.3_r, -0.3_r });
	ASSERT_TRUE(path);
	ASSERT_EQ(path->portals.size(), 0);
	}
	*/

	{
		auto path = squareHoleMesh.findPath(Vector2_r{ 0.31_r, 0.3_r }, Vector2_r{ 0.3_r, -0.31_r });
		ASSERT_TRUE(path);
		ASSERT_EQ(path->portals.size(), 1);
	}

	{
		auto path = squareHoleMesh.findPath(Vector2_r{ 0.1_r, 0.3_r }, Vector2_r{ 0.1_r, -0.3_r });
		ASSERT_TRUE(path);
		ASSERT_EQ(path->portals.size(), 2);
	}
}

TEST(NavigationMesh, findPath_TShape)
{
	NavigationMesh tMesh = createTMesh(1.0_r, 1.0_r);

	{
		auto path = tMesh.findPath(Vector2_r::ZERO, Vector2_r{ 0.3_r, -0.3_r });
		ASSERT_FALSE(path);
	}

	{
		auto path = tMesh.findPath(Vector2_r{ 0.3_r, -0.3_r }, Vector2_r::ZERO);
		ASSERT_FALSE(path);
	}

	{
		auto path = tMesh.findPath(Vector2_r{ -0.3_r, 0.3_r }, Vector2_r{ 0.3_r, 0.3_r });
		ASSERT_TRUE(path);
		ASSERT_EQ(path->portals.size(), 0);
	}

	{
		auto path = tMesh.findPath(Vector2_r{ -0.3_r, 0.3_r }, Vector2_r{ 0.0_r, -0.3_r });
		ASSERT_TRUE(path);
		ASSERT_EQ(path->portals.size(), 1);
	}
}