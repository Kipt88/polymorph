require_modules("io")

set(
	source_files
	"source/DataViewInputStream_test.cpp"
	"source/DataOutputStream_test.cpp"
	"source/Writer_test.cpp"
)

source_group("Sources" FILES ${source_files})

find_package(GTest REQUIRED)

add_executable("test_io" ${source_files} ${GTest_SOURCES})

target_include_directories(
	"test_io" PRIVATE
	"include" 
	${GTest_INCLUDE_DIRS}
)

target_link_libraries(
	"test_io" PRIVATE 
	"io" ${GTest_LIBRARIES}
)

add_test(NAME test_io COMMAND test_io)
