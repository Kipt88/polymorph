#include <gtest/gtest.h>

#include <io/Writer.hpp>
#include <io/DataOutputStream.hpp>

#include <cstring>

using namespace pm;

TEST(Writer, write_string)
{
	{
		std::string testString = "The big brown fox";

		io::DataOutputStream out;

		io::Writer writer(out);
		writer << testString;

		ASSERT_EQ(out.size(), testString.size());
		ASSERT_EQ(std::memcmp(out.data(), testString.c_str(), testString.size()), 0);
	}

	{
		std::string testString = "The big brown fox";

		io::DataOutputStream out;

		io::Writer writer(out);
		writer << testString.c_str();

		ASSERT_EQ(out.size(), testString.size());
		ASSERT_EQ(std::memcmp(out.data(), testString.c_str(), testString.size()), 0);
	}

	{
		std::string testString = "The big brown fox";

		io::DataOutputStream out;

		io::Writer writer(out);
		writer << "The big brown fox";

		ASSERT_EQ(out.size(), testString.size());
		ASSERT_EQ(std::memcmp(out.data(), testString.c_str(), testString.size()), 0);
	}
}

TEST(Writer, write_mixed)
{
	io::DataOutputStream out;

	io::Writer writer(out);
	writer << "Number one = " << 1;

	ASSERT_EQ(std::memcmp(out.data(), "Number one = 1", out.size()), 0);
}
