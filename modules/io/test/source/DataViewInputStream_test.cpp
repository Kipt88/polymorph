#include <gtest/gtest.h>

#include <io/DataViewInputStream.hpp>
#include <memory>

using namespace pm;

namespace {

struct DataTestStruct
{
	char aChar;
	int aInt;
	char bChar;
	int bInt;
};

std::unique_ptr<DataTestStruct[]> createData(std::size_t size)
{
	std::unique_ptr<DataTestStruct[]> data(new DataTestStruct[size]);

	for (std::size_t i = 0; i < size; ++i)
	{
		data[i].aChar = char(i);
		data[i].aInt = i + 1;
		data[i].bChar = char(i + 2);
		data[i].bInt = i + 3;
	}

	return data;
}

}

TEST(DataViewInputStream, read_struct)
{
	const std::size_t dataSize = 100;
	
	std::unique_ptr<DataTestStruct[]> data(createData(dataSize));

	ASSERT_TRUE(data != nullptr);

	io::DataViewInputStream dsis(data.get(), dataSize * sizeof(DataTestStruct));

	for (int i = 0; i < dataSize; ++i)
	{
		DataTestStruct readStruct;
		dsis >> readStruct;

		ASSERT_EQ(readStruct.aChar, char(i));
		ASSERT_EQ(readStruct.aInt, i + 1);
		ASSERT_EQ(readStruct.bChar, char(i + 2));
		ASSERT_EQ(readStruct.bInt, i + 3);
	}

	ASSERT_THROW(
		{
			DataTestStruct readStruct;
			dsis >> readStruct;
		},
		io::IOException
	);
}

TEST(DataViewInputStream, read_primitive)
{
	const std::size_t dataSize = 100;
	
	std::unique_ptr<DataTestStruct[]> data(createData(dataSize));

	ASSERT_TRUE(data != nullptr);

	io::DataViewInputStream dsis(data.get(), dataSize * sizeof(DataTestStruct));

	for (int i = 0; i < dataSize; ++i)
	{
		char tempC;
		int tempI;

		dsis >> tempC >> tempI;
		ASSERT_EQ(tempC, char(i));
		ASSERT_EQ(tempI, i + 1);

		dsis >> tempC >> tempI;
		ASSERT_EQ(tempC, char(i + 2));
		ASSERT_EQ(tempI, i + 3);
	}

	ASSERT_THROW(
		{
			int i;
			dsis >> i;
		},
		io::IOException
	);
}

TEST(DataViewInputStream, read_string)
{
	std::string testStr = "foobar";

	io::DataViewInputStream dsis(testStr.data(), testStr.size());

	char c;
	for (unsigned int i = 0; dsis.available() > 0; ++i)
	{
		dsis >> c;
		ASSERT_EQ(c, testStr[i]);
	}
}