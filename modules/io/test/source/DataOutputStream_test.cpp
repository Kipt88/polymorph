#include <gtest/gtest.h>

#include <io/DataOutputStream.hpp>
#include <io/DataViewInputStream.hpp>

using namespace pm;

namespace {

struct DataTestStructA
{
	char aChar;
	int aInt;
	char bChar;
	int bInt;
};

struct DataTestStructB
{
	char aChar;
	int aInt;
	char bChar;
};

}

TEST(DataOutputStream, write_struct)
{
	io::DataOutputStream out;

	out << DataTestStructA{ 1, 2, 3, 4 } << DataTestStructA{ 5, 6, 7, 8 };

	const DataTestStructA* data = reinterpret_cast<const DataTestStructA*>(out.data());

	ASSERT_EQ(data[0].aChar, 1);
	ASSERT_EQ(data[0].aInt, 2);
	ASSERT_EQ(data[0].bChar, 3);
	ASSERT_EQ(data[0].bInt, 4);

	ASSERT_EQ(data[1].aChar, 5);
	ASSERT_EQ(data[1].aInt, 6);
	ASSERT_EQ(data[1].bChar, 7);
	ASSERT_EQ(data[1].bInt, 8);
}

TEST(DataOutputStream, writer_read_struct)
{
	io::DataOutputStream out;

	out << DataTestStructA{ 1, 2, 3, 4 } << DataTestStructB{ 5, 6, 7 };
	
	{
		io::DataViewInputStream in(out.data(), out.size());

		DataTestStructA a;
		DataTestStructB b;

		in >> a >> b;

		ASSERT_EQ(a.aChar, 1);
		ASSERT_EQ(a.aInt, 2);
		ASSERT_EQ(a.bChar, 3);
		ASSERT_EQ(a.bInt, 4);

		ASSERT_EQ(b.aChar, 5);
		ASSERT_EQ(b.aInt, 6);
		ASSERT_EQ(b.bChar, 7);
	}
}

TEST(DataOutputStream, writer_read_primitive)
{
	io::DataOutputStream out;

	out << DataTestStructA{ 1, 2, 3, 4 } << DataTestStructB{ 5, 6, 7 };

	{
		io::DataViewInputStream in(out.data(), out.size());

		char a;
		int b;

		{
			in >> a >> b;

			ASSERT_EQ(a, 1);
			ASSERT_EQ(b, 2);
		}

		{
			in >> a >> b;

			ASSERT_EQ(a, 3);
			ASSERT_EQ(b, 4);
		}

		{
			in >> a >> b;

			ASSERT_EQ(a, 5);
			ASSERT_EQ(b, 6);
		}

		{
			in >> a;

			ASSERT_EQ(a, 7);
		}
	}
}