#include "io/Writer.hpp"

namespace pm { namespace io {

namespace detail {

template <typename T>
struct string_writer
{
	static void write(Writer& writer, const T& s)
	{
		writer.write(std::to_string(s));
	}
};

template <>
struct string_writer<char>
{
	static void write(Writer& writer, const char& c)
	{
		writer.write<1>(&c);
	}
};

template <std::size_t N>
struct string_writer<char[N]>
{
	static void write(Writer& writer, const char s[N])
	{
		writer.write(s);
	}
};

template <>
struct string_writer<const char*>
{
	static void write(Writer& writer, const char* s)
	{
		writer.write(s);
	}
};

template <>
struct string_writer<std::string>
{
	static void write(Writer& writer, const std::string& s)
	{
		writer.write(s);
	}
};

}

template <typename T>
Writer& operator<<(Writer& writer, const T& data)
{
	detail::string_writer<T>::write(writer, data);

	return writer;
}


template <std::size_t N>
void Writer::write(const char s[N])
{
	_out.write(s, 1, 1, N - 1);
}

} }
