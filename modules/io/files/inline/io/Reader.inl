#include "io/Reader.hpp"

namespace pm { namespace io {

namespace detail {

template <typename T>
struct string_reader;

template <>
struct string_reader<std::string>
{
	static std::string read(Reader& reader)
	{
		return reader.readWord();
	}
};

template <>
struct string_reader<int>
{
	static int read(Reader& reader)
	{
		auto word = reader.readWord();

		try
		{
			return std::stoi(word);
		}
		catch (...)
		{
			std::throw_with_nested(ParseException("Exception while parsing " + word));
		}
	}
};

template <>
struct string_reader<long>
{
	static long read(Reader& reader)
	{
		auto word = reader.readWord();

		try
		{
			return std::stol(word);
		}
		catch (...)
		{
			std::throw_with_nested(ParseException("Exception while parsing " + word));
		}
	}
};

template <>
struct string_reader<long long>
{
	static long long read(Reader& reader)
	{
		auto word = reader.readWord();

		try
		{
			return std::stol(word);
		}
		catch (...)
		{
			std::throw_with_nested(ParseException("Exception while parsing " + word));
		}
	}
};

template <>
struct string_reader<unsigned long>
{
	static unsigned long read(Reader& reader)
	{
		auto word = reader.readWord();

		try
		{
			return std::stoul(word);
		}
		catch (...)
		{
			std::throw_with_nested(ParseException("Exception while parsing " + word));
		}
	}
};

template <>
struct string_reader<unsigned int>
{
	static unsigned int read(Reader& reader)
	{
		return static_cast<unsigned int>(string_reader<unsigned long>::read(reader));
	}
};

template <>
struct string_reader<unsigned long long>
{
	static unsigned long long read(Reader& reader)
	{
		auto word = reader.readWord();

		try
		{
			return std::stoull(word);
		}
		catch (...)
		{
			std::throw_with_nested(ParseException("Exception while parsing " + word));
		}
	}
};

template <>
struct string_reader<float>
{
	static float read(Reader& reader)
	{
		auto word = reader.readWord();

		try
		{
			return std::stof(word);
		}
		catch (...)
		{
			std::throw_with_nested(ParseException("Exception while parsing " + word));
		}
	}
};

template <>
struct string_reader<double>
{
	static double read(Reader& reader)
	{
		auto word = reader.readWord();

		try
		{
			return std::stod(word);
		}
		catch (...)
		{
			std::throw_with_nested(ParseException("Exception while parsing " + word));
		}
	}
};

template <>
struct string_reader<long double>
{
	static long double read(Reader& reader)
	{
		auto word = reader.readWord();

		try
		{
			return std::stold(word);
		}
		catch (...)
		{
			std::throw_with_nested(ParseException("Exception while parsing " + word));
		}
	}
};

}

template <typename T>
Reader& operator>>(Reader& reader, T& data)
{
	data = detail::string_reader<T>::read(reader);
	return reader;
}

} }
