#include "io/IOStream.hpp"

#include <type_traits>

namespace pm { namespace io {

template <typename T, bool = std::is_trivially_copyable<T>::value>
struct reader;

template <typename T>
struct reader<T, true>
{
	static void read(InputStream& in, T& data)
	{
		in.read(
			&data,
			std::alignment_of<T>::value,
			sizeof(T),
			1
		);
	}

	static void readBlock(InputStream& in, T* data, std::size_t count)
	{
		in.read(
			data,
			std::alignment_of<T>::value,
			sizeof(T),
			count
		);
	}
};

template <typename T>
inline InputStream& operator>>(InputStream& in, T& data)
{
	reader<T>::read(in, data);
	return in;
}

template <typename T>
inline InputStream& readBlock(InputStream& in, T* data, std::size_t count)
{
	reader<T>::readBlock(in, data, count);
	return in;
}

template <typename T, bool = std::is_trivially_copyable<T>::value>
struct writer;

template <typename T>
struct writer<T, true>
{
	static void write(OutputStream& out, const T& data)
	{
		out.write(
			&data,
			std::alignment_of<T>::value,
			sizeof(T),
			1
		);
	}

	static void writeBlock(OutputStream& out, const T* data, std::size_t count)
	{
		out.write(
			data,
			std::alignment_of<T>::value,
			sizeof(T),
			count
		);
	}
};

template <typename T>
inline OutputStream& operator<<(OutputStream& out, const T& data)
{
	writer<T>::write(out, data);
	return out;
}

template <typename T>
inline OutputStream& writeBlock(OutputStream& out, const T* data, std::size_t count)
{
	writer<T>::writeBlock(out, data, count);
	return out;
}

} }
