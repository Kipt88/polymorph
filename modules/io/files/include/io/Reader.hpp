#pragma once

#include "io/IOStream.hpp"
#include <string>

namespace pm { namespace io {

PM_MAKE_EXCEPTION_CLASS(ParseException, IOException);

class Reader;

template <typename T>
Reader& operator>>(Reader& reader, T& data);

class Reader
{
public:
	explicit Reader(InputStream& in, const char* newLine = "\n");

	/** Reads until end or new line. */
	std::string readLine();

	/** Reads until end or whitespace. */
	std::string readWord();

private:
	char getNextChar();
	std::size_t available();

	InputStream& _in;
	const char* _newLine;
};

} }

#include "io/Reader.inl"
