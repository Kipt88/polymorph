#pragma once

#include <Exception.hpp>

namespace pm { namespace io {

PM_MAKE_EXCEPTION_CLASS(IOException, Exception);

} }