#pragma once

#include "io/IOException.hpp"

#include <NonCopyable.hpp>
#include <string>
#include <memory>
#include <type_traits>

namespace pm { namespace io {

class InputStream : NonCopyable
{
public:
	virtual ~InputStream() = default;
	
	/**
	 * Returns the amount of bytes left in stream.
	 * May throw io::IOexception if unable to determine.
	 */
	virtual std::size_t available() const = 0;

	/**
	 * Read implementation.
	 *
	 * @param data			Read target. Type of data is trivially copyable.
	 * @param typeAlignment	Alignment of data's type.
	 * @param size			Size of data element's type.
	 * @param count			Number of elements to read.
	 */
	virtual void read(
		void* data, 
		std::size_t typeAlignment, 
		std::size_t size, 
		std::size_t count
	) = 0;
};

template <typename T>
inline InputStream& operator>>(InputStream& in, T& data);

template <typename T>
inline InputStream& readBlock(InputStream& in, T* data, std::size_t count);

class OutputStream : NonCopyable
{
public:
	virtual ~OutputStream() = default;

	/**
	 * Write implementation.
	 *
	 * @param data			Write source. Type of data is trivially copyable.
	 * @param typeAlignment	Alignment of data's type.
	 * @param size			Size of data element's type.
	 * @param count			Number of elements to read.
	 */
	virtual void write(
		const void* data, 
		std::size_t typeAlignment, 
		std::size_t size, 
		std::size_t count
	) = 0;
};

template <typename T>
inline OutputStream& operator<<(OutputStream& out, const T& data);

template <typename T>
inline OutputStream& writeBlock(OutputStream& out, const T* data, std::size_t count);

} }


#include "io/IOStream.inl"
