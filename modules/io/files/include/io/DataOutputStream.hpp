#pragma once

#include "io/IOStream.hpp"
#include <vector>

namespace pm { namespace io {

class DataOutputStream : public OutputStream
{
public:
	DataOutputStream();
	explicit DataOutputStream(std::size_t initialCapacity);

	void write(
		const void* data,
		std::size_t typeAlignment,
		std::size_t size,
		std::size_t count
	) override;

	const void* data() const;

	std::size_t size() const;

private:
	std::vector<char> _data;
};

} }
