#pragma once

#include "io/IOStream.hpp"

#include <cstdint>
#include <memory>

namespace pm { namespace io {

class DataViewInputStream : public InputStream
{
public:
	DataViewInputStream(const void* data, std::size_t size);
	DataViewInputStream(DataViewInputStream&& other);

	std::size_t available() const override;

	void read(
		void* data, 
		std::size_t typeAlignment, 
		std::size_t size, 
		std::size_t count
	) override;

protected:
	std::uintptr_t _readPos;
	std::uintptr_t _readEnd;
};

} }
