#pragma once

#include "io/IOStream.hpp"
#include <string>

namespace pm { namespace io {

class Writer;

template <typename T>
Writer& operator<<(Writer& writer, const T& data);

class Writer
{
public:
	explicit Writer(OutputStream& out);

	template <std::size_t N>
	void write(const char s[N]);

	void write(const std::string& s);

private:
	OutputStream& _out;
};

} }

#include "io/Writer.inl"
