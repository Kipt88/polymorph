#include "io/Writer.hpp"

namespace pm { namespace io {

Writer::Writer(OutputStream& out)
: _out(out)
{
}

void Writer::write(const std::string& s)
{
	_out.write(s.data(), std::alignment_of<char>::value, 1, s.size());
}

} }
