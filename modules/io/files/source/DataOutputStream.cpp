#include "io/DataOutputStream.hpp"

#include <Alignment.hpp>

namespace pm { namespace io {

DataOutputStream::DataOutputStream()
: _data()
{
}

DataOutputStream::DataOutputStream(std::size_t initialCapacity)
: _data(initialCapacity)
{
}

void DataOutputStream::write(
	const void* data,
	std::size_t typeAlignment,
	std::size_t size,
	std::size_t count
)
{
	_data.reserve(_data.size() + size * (count + 1));

	const char* bufferEnd = _data.data() + _data.size();
	char* outputStart = reinterpret_cast<char*>(align(typeAlignment, bufferEnd));

	_data.resize(_data.size() + outputStart - bufferEnd + size * count);

	std::memcpy(outputStart, data, size * count);
}

const void* DataOutputStream::data() const
{
	return _data.data();
}

std::size_t DataOutputStream::size() const
{
	return _data.size();
}

} }
