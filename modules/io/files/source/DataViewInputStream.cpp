#include "io/DataViewInputStream.hpp"

#include <Alignment.hpp>
#include <Assert.hpp>

namespace pm { namespace io {

DataViewInputStream::DataViewInputStream(const void* data, std::size_t size)
:	_readPos(std::uintptr_t(data)), 
	_readEnd(_readPos + size)
{}

DataViewInputStream::DataViewInputStream(DataViewInputStream&& other)
:	_readPos(other._readPos),
	_readEnd(other._readEnd)
{}

void DataViewInputStream::read(void* data, 
								  std::size_t typeAlignment, 
								  std::size_t size, 
								  std::size_t count)
{
	void* aligned = align(typeAlignment, reinterpret_cast<void*>(_readPos));

	auto bytesToRead = size * count;

	if (std::uintptr_t(aligned) + bytesToRead > _readEnd)
		throw IOException();

	std::memcpy(data, aligned, bytesToRead);

	_readPos = std::uintptr_t(aligned) + bytesToRead;
}

std::size_t DataViewInputStream::available() const
{
	return std::size_t(_readEnd - _readPos);
}

} }