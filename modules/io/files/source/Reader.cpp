#include "io/Reader.hpp"

namespace pm { namespace io {

Reader::Reader(InputStream& in, const char* newLine)
: _in(in),
  _newLine(newLine)
{
}

std::string Reader::readLine()
{
	std::string s;

	auto newLineSize = std::strlen(_newLine);

	while (available() > 0)
	{
		char c = getNextChar();

		s.push_back(c);

		if (s.size() >= newLineSize && std::strcmp(s.c_str() + s.size() - newLineSize, _newLine) == 0)
		{
			for (unsigned int i = 0; i < newLineSize; ++i)
				s.pop_back();

			break;
		}
	}

	return s;
}

std::string Reader::readWord()
{
	std::string s;

	auto newLineSize = std::strlen(_newLine);

	while (available() > 0)
	{
		char c = getNextChar();

		s.push_back(c);

		if (s.size() >= newLineSize && std::strcmp(s.c_str() + s.size() - newLineSize, _newLine) == 0)
		{
			for (unsigned int i = 0; i < newLineSize; ++i)
				s.pop_back();

			break;
		}
		else if (s.back() == ' ' || s.back() == '\t')
		{
			s.pop_back();
			break;
		}
	}

	return s;
}

char Reader::getNextChar()
{
	char c;
	_in.read(&c, std::alignment_of<char>::value, 1, 1);

	return c;
}

std::size_t Reader::available()
{
	return _in.available();
}

} }
