#include <gtest/gtest.h>

#if 0
#include <os/MessageDialog.hpp>

using namespace pm::os;

TEST(MessageDialog, response)
{
	pm::os::MessageDialog dialog(
		"Test Title",
		"Test message. Press YES.",
		MessageDialog::Buttons::BUTTON_YES_NO,
		MessageDialog::Type::TYPE_INFO
	);

	auto response = dialog.show();

	ASSERT_EQ(response.get(), MessageDialog::UserResponse::RESP_YES);
}
#endif
