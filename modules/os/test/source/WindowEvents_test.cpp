#include <gtest/gtest.h>

#include <os/Window.hpp>
#include <concurrency/Thread.hpp>
#include <Debug.hpp>

using namespace pm::os;
/*
TEST(WindowEvents, pointer)
{
	Window window("Pointer Test - Press window and close to continue");

	window.show();

	bool quitFlag = false;
	auto quitScope = window.addWindowEventListener<window_events::Quit>(
		[&]() mutable
		{
			quitFlag = true;
		}
	);

	bool gotPointerDownEvent = false;
	auto pointerDownScope = window.addWindowEventListener<window_events::PointerDown>(
		[&](const pm::os::window_events::PointerDown& e) mutable
		{
			gotPointerDownEvent = true;
		}
	);

	while (!quitFlag && !gotPointerDownEvent)
	{
		while (window.pollEvent());

		pm::concurrency::Thread::sleep(33ms);
	}

	ASSERT_TRUE(gotPointerDownEvent);
}
*/
