#include <gtest/gtest.h>

#include <os/App.hpp>
#include <os/DisplaySettings.hpp>
#include <os/InputSystem.hpp>
#include <concurrency/Thread.hpp>
#include <string>
/*
TEST(InputSystem, run)
{
	pm::os::InputSystem inputSystem(
		33ms,
		std::string("Input System Test - wait 5s")
	);

	pm::concurrency::Thread::sleep(5s);
}
*/

TEST(AppSystem, run)
{
	pm::os::AppSystem appSystem("Test window - close to continue", 800, 600);
	
	appSystem.pushAction(
		[](pm::os::App& app)
		{
			auto&& allSettings = pm::os::DisplaySettings::enumerate();
			
			for (const auto& settings : allSettings)
			{
				DEBUG_OUT(
					"DisplayTest",
					"%lux%lu @%lu bpp %lu",
					settings.width(),
					settings.height(),
					settings.frequency(),
					settings.bitsPerPixel()
				);
			}
			
			auto&& currentSettings = pm::os::DisplaySettings::current();
			
			DEBUG_OUT(
				"DisplayTest",
				"Current: %lux%lu @%lu bpp %lu",
				currentSettings.width(),
				currentSettings.height(),
				currentSettings.frequency(),
				currentSettings.bitsPerPixel()
			);
			
			// auto displaySettings = allSettings.back();
			
			// displaySettings.activate();
		}
	);
	
	appSystem.enter();
}
