#include "os/DisplaySettings.hpp"

#include <Debug.hpp>
#include <algorithm>

namespace pm { namespace os {

namespace {

const char* LOG_TAG = "DisplaySettings";

}

CGDisplayModeRef DisplaySettings::defaultMode = nullptr;

DisplaySettings::DisplaySettings(CGDisplayModeRef mode)
: _mode(mode)
{
}

DisplaySettings::DisplaySettings(const DisplaySettings& other)
: _mode(other._mode)
{
	CGDisplayModeRetain(_mode);
}

DisplaySettings::~DisplaySettings()
{
	CGDisplayModeRelease(_mode);
}

DisplaySettings& DisplaySettings::operator=(const DisplaySettings& other)
{
	CGDisplayModeRelease(_mode);
	
	_mode = other._mode;
	
	CGDisplayModeRetain(_mode);
	
	return *this;
}

unsigned long DisplaySettings::bitsPerPixel() const
{
	// Can't seem to find any replacement for this function...
	// FIXME
	
	CFStringRef pixelEncoding = CGDisplayModeCopyPixelEncoding(_mode);
	
	if (CFStringCompare(pixelEncoding, CFSTR(IO32BitDirectPixels), 0) == kCFCompareEqualTo)
	{
		CFRelease(pixelEncoding);
		return 32;
	}
	else if (CFStringCompare(pixelEncoding, CFSTR(IO16BitDirectPixels), 0) == kCFCompareEqualTo)
	{
		CFRelease(pixelEncoding);
		return 16;
	}
	
	CFRelease(pixelEncoding);
	
	return 0;
}

unsigned long DisplaySettings::width() const
{
	return CGDisplayModeGetWidth(_mode);
}

unsigned long DisplaySettings::height() const
{
	return CGDisplayModeGetHeight(_mode);
}

unsigned long DisplaySettings::frequency() const
{
	return CGDisplayModeGetRefreshRate(_mode);
}

void DisplaySettings::activate()
{
	if (!defaultMode)
		defaultMode = CGDisplayCopyDisplayMode(CGMainDisplayID());
	
	CGError err = CGDisplaySetDisplayMode(CGMainDisplayID(), _mode, nullptr);

	if (err != kCGErrorSuccess)
	{
		ERROR_OUT(LOG_TAG, "Can't change display mode, error %i", static_cast<int>(err));
		throw ResolutionChangeException();
	}
}

void DisplaySettings::activateDefault()
{
	if (defaultMode)
	{
		CGError err = CGDisplaySetDisplayMode(CGMainDisplayID(), defaultMode, nullptr);

		if (err != kCGErrorSuccess)
		{
			ERROR_OUT(LOG_TAG, "Can't change display mode, error %i", static_cast<int>(err));
			throw ResolutionChangeException();
		}
		
		CGDisplayModeRelease(defaultMode);
		defaultMode = nullptr;
	}
}

std::vector<DisplaySettings> DisplaySettings::enumerate()
{
	std::vector<DisplaySettings> rVec;
	
	CFArrayRef allModes = CGDisplayCopyAllDisplayModes(CGMainDisplayID(), nullptr);
	
	if (!allModes)
		throw DisplayException();
	
	CFIndex size = CFArrayGetCount(allModes);
	
	for (CFIndex i = 0; i < size; ++i)
	{
		CGDisplayModeRef mode = (CGDisplayModeRef)CFArrayGetValueAtIndex(allModes, i);
		
		rVec.push_back(DisplaySettings{CGDisplayModeRetain(mode)});
	}
	
	CFRelease(allModes);
	
	std::sort(
		rVec.begin(),
		rVec.end(),
		[](const DisplaySettings& a, const DisplaySettings& b)
		{
			if (a.bitsPerPixel() != b.bitsPerPixel())
				return a.bitsPerPixel() < b.bitsPerPixel();

			if (a.frequency() != b.frequency())
				return a.frequency() < b.frequency();

			if (a.width() != b.width())
				return a.width() < b.width();

			return a.height() < b.height();
		}
	);

	return rVec;
}

DisplaySettings DisplaySettings::current()
{
	return DisplaySettings{CGDisplayCopyDisplayMode(CGMainDisplayID())};
}

} }
