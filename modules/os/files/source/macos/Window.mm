#include "os/Window.hpp"

#include <Assert.hpp>
#include <Debug.hpp>
#include <Scoped.hpp>

#include <algorithm>

@interface InputWindow : NSWindow
@property (nonatomic, nonnull, assign) pm::os::WindowEventManager* eventManager;
@end

@implementation InputWindow

- (void)keyDown:(NSEvent*)event
{
	NSString* chars = event.charactersIgnoringModifiers;
	
	if (chars)
	{
		for (NSUInteger i = 0; i < [chars length]; ++i)
		{
			unichar key = [chars characterAtIndex:i];
			_eventManager->triggerEvent<pm::os::window_events::KeyDown>({static_cast<pm::os::key_t>(key)});
		}
	}
	else
		[super keyDown:event];
}

- (void)keyUp:(NSEvent*)event
{
	NSString* chars = event.charactersIgnoringModifiers;
	
	if (chars)
	{
		for (NSUInteger i = 0; i < [chars length]; ++i)
		{
			unichar key = [chars characterAtIndex:i];
			_eventManager->triggerEvent<pm::os::window_events::KeyUp>({static_cast<pm::os::key_t>(key)});
		}
	}
	else
		[super keyUp:event];
}

@end

@interface WindowDelegate : NSObject <NSWindowDelegate>

@property (nonatomic, nonnull, assign) pm::os::WindowEventManager* eventManager;

- (instancetype)initWithEventManager:(pm::os::WindowEventManager*)eventManager;

@end

@implementation WindowDelegate

- (instancetype)initWithEventManager:(pm::os::WindowEventManager*)eventManager
{
	self = [super init];
	
	if (self)
	{
		_eventManager = eventManager;
	}
	
	return self;
}

- (void)windowDidResize:(NSNotification*)notification
{
	NSWindow* window = notification.object;
	
	NSAssert(window, @"Window not passed in notification.");
	
	pm::os::window_events::Resize ev;
	
	ev.w = window.frame.size.width;
	ev.h = window.frame.size.height;
	
	_eventManager->triggerEvent<pm::os::window_events::Resize>(ev);
}

- (void)windowDidEnterFullScreen:(NSNotification*)notification
{
	NSWindow* window = notification.object;
	
	NSAssert(window, @"Window not passed in notification.");
	
	pm::os::window_events::Resize ev;
	
	ev.w = window.screen.frame.size.width;
	ev.h = window.screen.frame.size.height;
	
	[window setFrame:window.screen.frame display:NO];
	
	_eventManager->triggerEvent<pm::os::window_events::Resize>(ev);
}

- (void)windowDidMiniaturize:(NSNotification*)notification
{
	// TODO
}

- (BOOL)windowShouldClose:(id)sender
{
	_eventManager->triggerEvent<pm::os::window_events::Quit>();
	
	return NO;
}

@end

namespace pm { namespace os {
namespace {

const char* TAG = "Window";

NSWindow* createWindow(
	WindowEventManager& eventManager,
	const std::string& title,
	long w,
	long h
)
{
	InputWindow* window = [[InputWindow alloc] initWithContentRect:NSMakeRect(0, 0, w, h)
	                                                     styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable
												           backing:NSBackingStoreBuffered
													         defer:NO];
	window.eventManager = &eventManager;
	window.collectionBehavior = NSWindowCollectionBehaviorFullScreenPrimary;
	window.title = @(title.c_str());
	
	[window cascadeTopLeftFromPoint:NSMakePoint(0, 0)];
	[window center];
	
	return window;
}

}


Window::Window(const std::string& title, long width, long height) 
: _eventManager(),
  _impl(createWindow(_eventManager, title, width, height)),
  _delegate(nil),
  _inFullscreen(false)
{
	_delegate = [[WindowDelegate alloc] initWithEventManager:&_eventManager];
	_impl.delegate = _delegate;
	
	show();
}

/*
Window::Window(const std::string& title, DisplaySettings& settings)
: _impl(createWindow(title, 1, 1)),
  _inFullscreen(false)
{
	enterFullscreen(settings);
	show();
}
*/

Window::~Window()
{
	// Window should be destroyed before event handler so it can respond.
	_impl = nil;
}

bool Window::show(bool show)
{
	/*
	BOOL wasVisible = ShowWindow(_windowHandle.get(), show ? SW_SHOW : SW_HIDE);
	if (show)
		VERIFY(UpdateWindow(_windowHandle.get()));

	return !!wasVisible;
	*/
	
	BOOL wasMiniturized = [_impl isMiniaturized];
	
	if (show)
		[_impl makeKeyAndOrderFront:nil];
	else
		[_impl miniaturize:nil];
	
	return wasMiniturized == YES;
}

void Window::enterFullscreen(DisplaySettings& settings)
{
	[_impl toggleFullScreen:nil];
	
	/*
	NSApplication* app = [NSApplication sharedApplication];
	
	app.presentationOptions = NSApplicationPresentationFullScreen;
	*/
}

void Window::exitFullscreen()
{
	[_impl toggleFullScreen:nil];
	/*
	NSApplication* app = [NSApplication sharedApplication];
	
	app.presentationOptions = NSApplicationPresentationDefault;
	*/
}

void Window::setSize(unsigned long w, unsigned long h)
{
	/*
	RECT rect;
	VERIFY(GetWindowRect(_windowHandle.get(), &rect));

	SetWindowPos(
		_windowHandle.get(),
		nullptr,
		rect.left,
		rect.top,
		w,
		h,
		0
	);

	InvalidateRect(_windowHandle.get(), nullptr, true);
	*/
}

bool Window::pollEvent()
{
	// return _eventSource.handleInput();
	return false;
}

NSWindow* Window::nativeHandle()
{
	return _impl;
}

} }	// namespace sys
