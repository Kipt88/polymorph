#include "os/App.hpp"

#include "Debug.hpp"
#include <memory>

typedef pm::concurrency::TaskQueue<pm::concurrency::dynamic_return_tag(pm::os::App&)> TaskQueue;

@interface AppDelegate ()

- (instancetype)initWithQueue:(TaskQueue&)taskQueue andApp:(pm::os::App&)app;

@property (nonatomic, readonly) pm::os::App* app;
@property (nonatomic, readonly) TaskQueue* taskQueue;

@property (nonatomic) unsigned int windowStartWidth;
@property (nonatomic) unsigned int windowStartHeight;
@property (nonatomic) std::string windowStartTitle;

@property (nonatomic, readonly) pm::os::Window* window;

@end

@implementation AppDelegate

- (instancetype)initWithQueue:(TaskQueue&)taskQueue andApp:(pm::os::App&)app
{
	self = [super init];
	
	if (self)
	{
		_app = &app;
		_taskQueue = &taskQueue;
	}
	
	return self;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
	return YES;
}

- (void)applicationDidFinishLaunching:(NSNotification*)notification
{
	_window = new pm::os::Window(_windowStartTitle, _windowStartWidth, _windowStartHeight);
}

- (void)applicationWillTerminate:(NSNotification*)notification
{
	if (_window)
	{
		delete _window;
		_window = nullptr;
	}
}

- (void)applicationWillUpdate:(NSNotification*)notification
{
	if (!_window)
		return;
	
	auto&& access = _taskQueue->access();
	
	while (!access.empty())
		access.doAction(*_app);
}

@end

namespace pm { namespace os {

namespace {

void addMenuBar()
{
	id appName = [[NSProcessInfo processInfo] processName];
	
	id menubar = [NSMenu new];
	id appMenuItem = [NSMenuItem new];
	[menubar addItem:appMenuItem];
	
	id appMenu = [NSMenu new];
	id quitTitle = [@"Quit " stringByAppendingString:appName];
	id quitMenuItem = [[NSMenuItem alloc] initWithTitle:quitTitle action:@selector(terminate:) keyEquivalent:@"q"];
	
	[appMenu addItem:quitMenuItem];
	[appMenuItem setSubmenu:appMenu];
	
	NSApp.mainMenu = menubar;
}

}

App::App(concurrency::TaskQueue<concurrency::dynamic_return_tag(App&)>& queue, const std::string& windowTitle, unsigned int winW, unsigned int winH)
: _delegate(nullptr)
{
	[NSApplication sharedApplication];
		
	[NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];
	
	addMenuBar();
	
	_delegate = [[AppDelegate alloc] initWithQueue:queue andApp:*this];
	
	_delegate.windowStartTitle = windowTitle;
	_delegate.windowStartWidth = winW;
	_delegate.windowStartHeight = winH;
	
	NSApp.delegate = _delegate;
	
	[NSApp activateIgnoringOtherApps:YES];
}

Window& App::window()
{
	return *_delegate.window;
}


AppSystem::AppSystem(const std::string& windowTitle, unsigned int winW, unsigned int winH)
: _actions(),
  _app(_actions, windowTitle, winW, winH)
{
}

void AppSystem::enter()
{
	[NSApp run];
}

void AppSystem::quit()
{
	[NSApp terminate:nil];
}

} }
