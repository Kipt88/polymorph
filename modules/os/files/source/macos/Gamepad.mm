#include "os/Gamepad.hpp"
#include <Debug.hpp>

namespace pm { namespace os {

namespace {

const char* LOG_TAG = "Gamepad";

constexpr double AXIS_SIZE = 32767.0;

}

Gamepad::Gamepad(WindowEventManager& eventManager)
: _eventManager(eventManager)
{
}

void Gamepad::poll()
{
}

} }
