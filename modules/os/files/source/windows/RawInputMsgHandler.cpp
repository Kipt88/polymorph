#include "os/RawInputMsgHandler.hpp"

#include <Debug.hpp>
#include <Assert.hpp>
#include <array>

namespace pm { namespace os {

namespace {

const char* TAG = "RawInputHandler";

const std::array<USHORT, 3> DOWN_BUTTON_ID =
{
	RI_MOUSE_LEFT_BUTTON_DOWN,
	RI_MOUSE_RIGHT_BUTTON_DOWN,
	RI_MOUSE_MIDDLE_BUTTON_DOWN
};

const std::array<USHORT, 3> UP_BUTTON_ID =
{
	RI_MOUSE_LEFT_BUTTON_UP,
	RI_MOUSE_RIGHT_BUTTON_UP,
	RI_MOUSE_MIDDLE_BUTTON_UP
};

void mouseInput(WindowEventManager& eventManager,
				HWND window,
				const RAWMOUSE& mouse,
				USHORT& mouseDownFlags)
{
	POINT cursorPoint;

	if (!GetCursorPos(&cursorPoint))
		return;

	if (!ScreenToClient(window, &cursorPoint))
		return;

	RECT rect;

	if (!GetClientRect(window, &rect))
		return;

	for (pointer_button_t i = 0; i < DOWN_BUTTON_ID.size(); ++i)
	{
		if ((mouse.usButtonFlags & DOWN_BUTTON_ID[i]) == DOWN_BUTTON_ID[i])
		{
			mouseDownFlags |= DOWN_BUTTON_ID[i];
			eventManager.triggerEvent<window_events::PointerDown>(
				{
					static_cast<unsigned long>(rect.right),
					static_cast<unsigned long>(rect.bottom),
					1,
					i,
					cursorPoint.x,
					rect.bottom - cursorPoint.y
				}
			);
		}
	}

	for (pointer_button_t i = 0; i < UP_BUTTON_ID.size(); ++i)
	{
		if ((mouse.usButtonFlags & UP_BUTTON_ID[i]) == UP_BUTTON_ID[i])
		{
			mouseDownFlags &= ~DOWN_BUTTON_ID[i];
			eventManager.triggerEvent<window_events::PointerUp>(
				{
					static_cast<unsigned long>(rect.right),
					static_cast<unsigned long>(rect.bottom),
					1,
					i,
					cursorPoint.x,
					rect.bottom - cursorPoint.y
				}
			);
		}
	}

	int dx = static_cast<int>(mouse.lLastX);
	int dy = static_cast<int>(mouse.lLastY);

	if (dx != 0 && dy != 0)
	{
		bool dragging = false;

		for (pointer_button_t i = 0; i < DOWN_BUTTON_ID.size(); ++i)
		{
			if ((mouseDownFlags & DOWN_BUTTON_ID[i]) == DOWN_BUTTON_ID[i])
			{
				dragging = true;
				break;
			}
		}

		if (dragging)
		{
			eventManager.triggerEvent<window_events::PointerDrag>(
				{
					static_cast<unsigned long>(rect.right),
					static_cast<unsigned long>(rect.bottom),
					1,
					cursorPoint.x,
					rect.bottom - cursorPoint.y,
					cursorPoint.x + mouse.lLastX,
					rect.bottom - (cursorPoint.y + mouse.lLastY)
				}
			);
		}
		else
		{
			eventManager.triggerEvent<window_events::PointerMove>(
				{
					static_cast<unsigned long>(rect.right),
					static_cast<unsigned long>(rect.bottom),
					1,
					cursorPoint.x,
					rect.bottom - cursorPoint.y,
					cursorPoint.x + mouse.lLastX,
					rect.bottom - (cursorPoint.y + mouse.lLastY)
				}
			);
		}
	}

	if ((mouse.usButtonFlags & RI_MOUSE_WHEEL) == RI_MOUSE_WHEEL)
	{
		eventManager.triggerEvent<window_events::PointerZoom>(
			{
				static_cast<float>(static_cast<short>(mouse.usButtonData) / WHEEL_DELTA)
			}
		);
	}
}

void keyboardInput(WindowEventManager& eventManager, const RAWKEYBOARD& keyboard)
{
	switch (keyboard.Message)
	{
		case WM_KEYDOWN:
			eventManager.triggerEvent<window_events::KeyDown>({ keyboard.VKey });
			break;

		case WM_KEYUP:
			eventManager.triggerEvent<window_events::KeyUp>({ keyboard.VKey });
			break;

		default:
			INFO_OUT(
				TAG,
				"Unrecognized keyboard message: VKey = 0x%X Msg = 0x%X",
				keyboard.VKey,
				keyboard.Message
			);
			break;
	}
}

}

RawInputMsgHandler::RawInputMsgHandler(HWND window)
: _mouseButtonDownFlags(0)
{
	// Get raw input data from the mouse/pointer and keyboard device.

	RAWINPUTDEVICE rid[2];

	// Mouse:
	rid[0].usUsagePage = 0x01;
	rid[0].usUsage = 0x02;
	rid[0].dwFlags = 0x00;
	rid[0].hwndTarget = window;

	// Keyboard:
	rid[1].usUsagePage = 0x01;
	rid[1].usUsage = 0x06;
	rid[1].dwFlags = 0x00;
	rid[1].hwndTarget = window;

	VERIFY(RegisterRawInputDevices(rid, 2, sizeof(rid[0])));
}

bool RawInputMsgHandler::pollEvent(WindowEventManager& eventManager,
								HWND window,
								UINT msg,
								WPARAM wParam,
								LPARAM lParam)
{
	if (msg != WM_INPUT)
		return false;

	if (wParam != RIM_INPUT)
		return true;

	RAWINPUT input;
	UINT szData = sizeof(input);
	UINT szHeader = sizeof(RAWINPUTHEADER);
	HRAWINPUT handle = reinterpret_cast<HRAWINPUT>(lParam);

	UINT bytesWritten = GetRawInputData(handle, RID_INPUT, &input, &szData, szHeader);

	ASSERT(bytesWritten <= szData, "Unexpected return value from GetRawInputData.");
	ASSERT(bytesWritten != UINT(-1), "GetRawInputData returned error.");

	switch (input.header.dwType)
	{
		case RIM_TYPEMOUSE:
			mouseInput(
				eventManager,
				window,
				input.data.mouse,
				_mouseButtonDownFlags
			);
			break;

		case RIM_TYPEKEYBOARD:
			keyboardInput(eventManager, input.data.keyboard);
			break;
	}

	return true;
}

} }