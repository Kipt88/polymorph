#include "os/FocusMsgHandler.hpp"

namespace pm { namespace os {

bool FocusMsgHandler::pollEvent(WindowEventManager& eventManager,
								HWND /*window*/,
								UINT msg,
								WPARAM wParam,
								LPARAM /*lParam*/)
{
	if (msg != WM_ACTIVATE)
		return false;

	bool deactivated = (wParam == WA_INACTIVE);

	if (deactivated)
		eventManager.triggerEvent<window_events::Hide>();
	else
		eventManager.triggerEvent<window_events::Show>();

	return true;
}

} }