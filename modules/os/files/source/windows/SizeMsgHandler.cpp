#include "os/SizeMsgHandler.hpp"

namespace pm { namespace os {

bool SizeMsgHandler::pollEvent(WindowEventManager& eventManager,
							   HWND /*window*/,
							   UINT msg,
							   WPARAM /*wParam*/,
							   LPARAM lParam)
{
	if (msg != WM_SIZE)
		return false;

	int w = LOWORD(lParam);
	int h = HIWORD(lParam);

	eventManager.triggerEvent<window_events::Resize>({ w, h });

	return true;
}

} }