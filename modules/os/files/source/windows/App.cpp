#include "os/App.hpp"

#include "Debug.hpp"
#include <memory>

typedef pm::concurrency::TaskQueue<pm::concurrency::dynamic_return_tag(pm::os::App&)> TaskQueue;

namespace pm { namespace os {

App::App(
	concurrency::TaskQueue<concurrency::dynamic_return_tag(App&)>& queue, 
	const std::string& windowTitle, 
	unsigned int winW, 
	unsigned int winH
)
: _window(windowTitle, winW, winH),
  _exiting(false)
{
}

bool App::update(const concurrency::FrameTime&)
{
	while (_window.pollEvent());

	return true;
}

void App::shutdown()
{
	throw concurrency::InterruptException();
}

Window& App::window()
{
	return _window;
}

const Window& App::window() const
{
	return _window;
}

} }
