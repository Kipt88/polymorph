#include "os/Window.hpp"

#include <Assert.hpp>
#include <Debug.hpp>
#include <Scoped.hpp>

#include <algorithm>


namespace pm { namespace os {
namespace {

const char* TAG = "Window";

ATOM registerClass(HINSTANCE hInstance)
{
	WNDCLASSEX wc;

	wc.cbSize = sizeof(wc);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = os::handler;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance  = hInstance;
	wc.hIcon   = LoadIcon(NULL, IDI_APPLICATION);
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = TEXT("wndclass");

	ATOM name = RegisterClassEx(&wc);
	if (name == 0)
		throw os::WindowException(std::string("WinAPI error:") + std::to_string(GetLastError()));

	return name;
}

HWND createWindow(WindowEventSource* eventSource, 
				  const std::string& title,
				  long w,
				  long h)
{
	HINSTANCE module = GetModuleHandle(NULL);
	if (module == NULL)
		throw WindowException(std::string("WinAPI error:") + std::to_string(GetLastError()));

	static ATOM name = registerClass(module);

	DWORD dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	DWORD dwStyle = WS_OVERLAPPED 
		            | WS_CAPTION 
					| WS_SYSMENU
					| WS_MINIMIZEBOX
					| WS_CLIPCHILDREN 
					| WS_CLIPSIBLINGS;

	long cx = GetSystemMetrics(SM_CXSCREEN) / 2;
	long cy = GetSystemMetrics(SM_CYSCREEN) / 2;

	RECT rect;
	rect.left = cx - w / 2;
	rect.top = cy - h / 2;

	rect.right = rect.left + w;
	rect.bottom = rect.top + h;

	VERIFY(AdjustWindowRectEx(&rect, dwStyle, FALSE, dwExStyle));

	HWND hwnd = CreateWindowEx(
		dwExStyle,
		MAKEINTATOM(name),
		TEXT(title.c_str()),
		dwStyle,
		rect.left, rect.top,
		rect.right - rect.left, rect.bottom - rect.top,
		NULL,
		NULL,
		module,
		eventSource // WARNING: This will fail if Window instance is moved.
	);

	if (!hwnd)
		throw WindowException(std::string("WinAPI error:") + std::to_string(GetLastError()));

	DEBUG_OUT(TAG, "Created window %p", hwnd);

	return hwnd;
}

}	// anonymous namespace

namespace detail {

void window_deleter::operator()(HWND window)
{
	VERIFY(DestroyWindow(window));
}

}	// namespace detail

Window::Window(const std::string& title, long width, long height) 
: _windowHandle(createWindow(&_eventSource, title, width, height)),
  _windowedData(),
  _inFullscreen(false),
  _eventSource(_windowHandle.get())
{
	show();
}

Window::Window(const std::string& title, DisplaySettings& settings)
: _windowHandle(createWindow(&_eventSource, title, 1, 1)),
  _windowedData(),
  _inFullscreen(false),
  _eventSource(_windowHandle.get())
{
	enterFullscreen(settings);
	show();
}

Window::~Window()
{
	// Window should be destroyed before event handler so it can respond.
	_windowHandle = nullptr;
}

bool Window::show(bool show)
{
	BOOL wasVisible = ShowWindow(_windowHandle.get(), show ? SW_SHOW : SW_HIDE);
	if (show)
		VERIFY(UpdateWindow(_windowHandle.get()));

	return !!wasVisible;
}

void Window::enterFullscreen(DisplaySettings& settings)
{
	VERIFY(GetWindowRect(_windowHandle.get(), &_windowedData.rect));

	settings.activate();

	SetWindowRgn(_windowHandle.get(), nullptr, false);

	DWORD dwStyle = GetWindowLong(_windowHandle.get(), GWL_STYLE);
	SetWindowLong(_windowHandle.get(), GWL_STYLE, dwStyle & ~WS_CAPTION);

	SetWindowPos(
		_windowHandle.get(), 
		HWND_TOP,
		0, 0, 
		settings.width(), 
		settings.height(), 
		0
	);
}

void Window::exitFullscreen()
{
	DisplaySettings::activateDefault();

	DWORD dwStyle = GetWindowLong(_windowHandle.get(), GWL_STYLE);
	SetWindowLong(_windowHandle.get(), GWL_STYLE, dwStyle | WS_CAPTION);

	SetWindowPos(
		_windowHandle.get(),
		nullptr,
		_windowedData.rect.left,
		_windowedData.rect.top,
		_windowedData.rect.right - _windowedData.rect.left,
		_windowedData.rect.bottom - _windowedData.rect.top,
		0
	);

	InvalidateRect(_windowHandle.get(), nullptr, true);
}

void Window::setSize(unsigned long w, unsigned long h)
{
	RECT rect;
	VERIFY(GetWindowRect(_windowHandle.get(), &rect));

	SetWindowPos(
		_windowHandle.get(),
		nullptr,
		rect.left,
		rect.top,
		w,
		h,
		0
	);

	InvalidateRect(_windowHandle.get(), nullptr, true);
}

bool Window::pollEvent()
{
	return _eventSource.handleInput();
}

std::experimental::filesystem::path Window::openFileDialog() const
{
	TCHAR filepath[MAX_PATH];
	filepath[0] = '\0';

	OPENFILENAME ofn;
	
	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = _windowHandle.get();
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	ofn.lpstrFile = filepath;
	ofn.nMaxFile = MAX_PATH;
	
	if (GetOpenFileName(&ofn))
	{
		return ofn.lpstrFile;
	}
	else
	{
		DEBUG_OUT(
			TAG, 
			"Error for GetOpenFileName 0x%x", 
			static_cast<int>(CommDlgExtendedError())
		);
		return "";
	}
}

HWND Window::nativeHandle()
{
	return _windowHandle.get();
}

} }	// namespace sys
