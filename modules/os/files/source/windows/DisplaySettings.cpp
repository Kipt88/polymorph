#include "os/DisplaySettings.hpp"

#include <algorithm>

namespace pm { namespace os {

// ResolutionChangeException:

ResolutionChangeException::ResolutionChangeException(LONG errCode)
: _errCode(errCode)
{
}

const char* ResolutionChangeException::what() const noexcept
{
	return "ResolutionChangeException";
}

const LONG ResolutionChangeException::errorCode() const
{
	return _errCode;
}


// DisplaySettings:

DisplaySettings::DisplaySettings(DEVMODE devmode)
: _devmode(devmode)
{
}

unsigned long DisplaySettings::bitsPerPixel() const
{
	return _devmode.dmBitsPerPel;
}

unsigned long DisplaySettings::width() const
{
	return _devmode.dmPelsWidth;
}

unsigned long DisplaySettings::height() const
{
	return _devmode.dmPelsHeight;
}

unsigned long DisplaySettings::frequency() const
{
	return _devmode.dmDisplayFrequency;
}

void DisplaySettings::activate()
{
	LONG r = ChangeDisplaySettings(&_devmode, CDS_FULLSCREEN);

	if (r != DISP_CHANGE_SUCCESSFUL)
		throw ResolutionChangeException(r);
}

void DisplaySettings::activateDefault()
{
	LONG r = ChangeDisplaySettings(nullptr, 0);

	if (r != DISP_CHANGE_SUCCESSFUL)
		throw ResolutionChangeException(r);
}

std::vector<DisplaySettings> DisplaySettings::enumerate()
{
	std::vector<DisplaySettings> rVec;

	DEVMODE settings;
	
	BOOL r = EnumDisplaySettings(nullptr, 0, &settings);

	if (r == FALSE)
		throw DisplayException();

	settings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT | DM_DISPLAYFREQUENCY;

	for (DWORD i = 1; EnumDisplaySettings(nullptr, i, &settings) == TRUE; ++i)
		rVec.push_back(settings);

	std::sort(
		rVec.begin(),
		rVec.end(),
		[](const DisplaySettings& a, const DisplaySettings& b)
		{
			if (a.bitsPerPixel() != b.bitsPerPixel())
				return a.bitsPerPixel() < b.bitsPerPixel();

			if (a.frequency() != b.frequency())
				return a.frequency() < b.frequency();

			if (a.width() != b.width())
				return a.width() < b.width();

			return a.height() < b.height();
		}
	);

	return rVec;
}

DisplaySettings DisplaySettings::current()
{
	DEVMODE settings;

	BOOL r = EnumDisplaySettings(nullptr, ENUM_CURRENT_SETTINGS, &settings);

	if (r == FALSE)
		throw DisplayException();

	settings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT | DM_DISPLAYFREQUENCY;

	return settings;
}

} }