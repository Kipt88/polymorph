#include "os/WindowEventSource.hpp"

#include "os/Window.hpp"
#include <Debug.hpp>
#include <Assert.hpp>
#include <Cpp11.hpp>
#include <optional.hpp>
#include <Xinput.h>
#include <array>

namespace pm { namespace os {

namespace {

const char* TAG = "WindowEventSource";

// NOTE: return type and user data index macro of GetWindowLongPtr()
// are dependent on target architecture (64-bit or 32-bit).
#ifdef _WIN64
	#define USERDATA GWLP_USERDATA
	typedef LONG_PTR user_data;
#else
	#define USERDATA GWL_USERDATA
	typedef LONG user_data;
#endif

template <typename T>
T getUserData(HWND window)
{
	user_data ud = GetWindowLongPtr(window, GWLP_USERDATA);
	return reinterpret_cast<T>(ud);
}

template <typename T>
void setUserData(HWND window, T ud)
{
	SetLastError(0);
	user_data old = SetWindowLongPtr(window, USERDATA, reinterpret_cast<user_data>(ud));
	
	DWORD lastError = GetLastError();
	if (old == 0 && lastError)
		throw WindowException(std::string("WinAPI ") + std::to_string(lastError));
}

template <std::size_t I = std::tuple_size<WindowEventSource::InputHandler>::value - 1>
struct EventHandlers
{
	static bool poll(WindowEventSource::InputHandler& tuple,
					 WindowEventManager& eventManager,
					 HWND window,
					 UINT msg,
					 WPARAM wParam,
					 LPARAM lParam)
	{
		auto& handler = std::get<I>(tuple);
		bool handledMessage = handler.pollEvent(eventManager,
												window,
												msg,
												wParam,
												lParam);

		if (handledMessage)
			return true;

		return EventHandlers<I - 1>::poll(
			tuple,
			eventManager,
			window,
			msg,
			wParam,
			lParam
		);
	}
};

template <>
struct EventHandlers<0>
{
	static bool poll(WindowEventSource::InputHandler& tuple,
					 WindowEventManager& eventManager,
					 HWND window,
					 UINT msg,
					 WPARAM wParam,
					 LPARAM lParam)
	{
		auto& handler = std::get<0>(tuple);
		bool handledMessage = handler.pollEvent(eventManager,
												window,
												msg,
												wParam,
												lParam);

		return handledMessage;
	}
};

}	// anonymous namespace

LRESULT CALLBACK handler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg) 
	{
		case WM_CREATE:
		{
			// Set pointer to event source instance as the HWND user data.
			// NOTE: receives last argument of CreateWindowEx() as lpCreateParams
			CREATESTRUCT* pCreate = reinterpret_cast<CREATESTRUCT*>(lParam);
			setUserData(hwnd, pCreate->lpCreateParams);
			return 0;
		}
		default:
		{
			auto* thiz = getUserData<WindowEventSource*>(hwnd);

			bool res = EventHandlers<>::poll(
				thiz->_inputHandlers,
				thiz->_eventManager,
				hwnd,
				msg,
				wParam,
				lParam
			);

			if (res)
				return 0;

			break;
		}
	}

	// allow the default event handler to process the message
	return DefWindowProc(hwnd, msg, wParam, lParam);
}


WindowEventSource::WindowEventSource(HWND window)
: _eventManager(),
  _inputHandlers(
	  RawInputMsgHandler(window), 
	  QuitMsgHandler(), 
	  FocusMsgHandler(),
	  SysCommandMsgHandler(),
	  SizeMsgHandler()
  ),
  _gamepad(_eventManager),
  _window(window)
{
}

bool WindowEventSource::handleInput()
{
	_gamepad.poll();

	MSG msg;
	if (PeekMessage(&msg, _window, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);

		return true; // More messages might be in queue.
	}

	return false;
}

} }
