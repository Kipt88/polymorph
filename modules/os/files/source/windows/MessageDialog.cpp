#include "os/MessageDialog.hpp"

#include <Assert.hpp>

namespace pm { namespace os {

MessageDialog::MessageDialog(const std::string& title, 
							 const std::string& message, 
							 Buttons buttons, 
							 Type type,
							 HWND parent)
: _title(title),
  _message(message),
  _buttons(buttons),
  _type(type),
  _parent(parent),
  _runner()
{
}

MessageDialog::MessageDialog(MessageDialog&& other)
: _title(std::move(other._title)),
  _message(std::move(other._message)),
  _buttons(other._buttons),
  _type(other._type),
  _parent(other._parent),
  _runner(std::move(other._runner))
{
}

concurrency::Future<MessageDialog::UserResponse> MessageDialog::show()
{
	concurrency::Task<UserResponse()> task =
		[
			parent = _parent,
			msg = _message,
			title = _title,
			type = (static_cast<UINT>(_type) | static_cast<UINT>(_buttons))
		]() mutable -> UserResponse
		{
			int resp = MessageBox(parent,
								  msg.c_str(),
								  title.c_str(),
								  type);

			return static_cast<UserResponse>(resp);
		};

	auto&& future = task.getFuture();

	_runner = std::move(task);
	
	return std::move(future);
}

} }


