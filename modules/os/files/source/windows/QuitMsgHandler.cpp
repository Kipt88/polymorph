#include "os/QuitMsgHandler.hpp"

namespace pm { namespace os {

bool QuitMsgHandler::pollEvent(WindowEventManager& eventManager,
							   HWND /*window*/,
							   UINT msg,
							   WPARAM /*wParam*/,
							   LPARAM /*lParam*/)
{
	if (msg != WM_CLOSE)
		return false;

	eventManager.triggerEvent<window_events::Quit>();

	return true;
}

} }
