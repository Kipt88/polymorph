#include "os/SysCommandMsgHandler.hpp"

namespace pm { namespace os {

bool SysCommandMsgHandler::pollEvent(WindowEventManager& /*eventManager*/,
										HWND /*window*/,
										UINT msg,
										WPARAM wParam,
										LPARAM /*lParam*/)
{
	if (msg != WM_SYSCOMMAND)
		return false;

	// Prevents screen saver and power saving mode while window active.

	switch (wParam)
	{
		case SC_SCREENSAVE:
		case SC_MONITORPOWER:
			return true;
		default:
			return false;
	}
}

} }