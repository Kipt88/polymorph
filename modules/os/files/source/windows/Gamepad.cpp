#include "os/Gamepad.hpp"
#include <Debug.hpp>

namespace pm { namespace os {

namespace {

const char* LOG_TAG = "Gamepad";

constexpr double AXIS_SIZE = 32767.0;

}

Gamepad::Gamepad(WindowEventManager& eventManager)
: _states(),
  _eventManager(eventManager)
{
}

void Gamepad::poll()
{
	for (unsigned int i = 0; i < MAX_CONTROLLERS; ++i)
	{
		XINPUT_STATE state;
		ZeroMemory(&state, sizeof(XINPUT_STATE));

		DWORD res = XInputGetState(i, &state);

		if (res == ERROR_SUCCESS)
		{
			if (_states[i])
			{
				if (_states[i]->dwPacketNumber == state.dwPacketNumber)
					continue;

				auto oldState = *_states[i];

				if (std::abs(state.Gamepad.sThumbLX) < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
					state.Gamepad.sThumbLX = 0;

				if (std::abs(state.Gamepad.sThumbLY) < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
					state.Gamepad.sThumbLY = 0;

				if (std::abs(state.Gamepad.sThumbRX) < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
					state.Gamepad.sThumbRX = 0;

				if (std::abs(state.Gamepad.sThumbRY) < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
					state.Gamepad.sThumbRY = 0;

				_states[i] = state;

				if (auto keysDiff = (oldState.Gamepad.wButtons ^ state.Gamepad.wButtons))
				{
					const std::array<
						std::pair<unsigned int, GamepadButton>, 
						static_cast<unsigned int>(GamepadButton::ENUM_SIZE)
					> buttonMap = {{
						{ XINPUT_GAMEPAD_DPAD_UP,        GamepadButton::DPAD_UP },
						{ XINPUT_GAMEPAD_DPAD_DOWN,      GamepadButton::DPAD_DOWN },
						{ XINPUT_GAMEPAD_DPAD_LEFT,      GamepadButton::DPAD_LEFT },
						{ XINPUT_GAMEPAD_DPAD_RIGHT,     GamepadButton::DPAD_RIGHT },
						{ XINPUT_GAMEPAD_START,          GamepadButton::START },
						{ XINPUT_GAMEPAD_BACK,           GamepadButton::BACK },
						{ XINPUT_GAMEPAD_LEFT_THUMB,     GamepadButton::LEFT_THUMB },
						{ XINPUT_GAMEPAD_RIGHT_THUMB,    GamepadButton::RIGHT_THUMB },
						{ XINPUT_GAMEPAD_LEFT_SHOULDER,  GamepadButton::LEFT_SHOULDER },
						{ XINPUT_GAMEPAD_RIGHT_SHOULDER, GamepadButton::RIGHT_SHOULDER },
						{ XINPUT_GAMEPAD_A,              GamepadButton::A },
						{ XINPUT_GAMEPAD_B,              GamepadButton::B },
						{ XINPUT_GAMEPAD_X,              GamepadButton::X },
						{ XINPUT_GAMEPAD_Y,              GamepadButton::Y }
					}};

					for (const auto& button : buttonMap)
					{
						if (keysDiff & button.first)
						{
							if (state.Gamepad.wButtons & button.first)
							{
								_eventManager.triggerEvent<window_events::GamepadKeyDown>(
									{
										i, 
										button.second
									}
								);
							}
							else
							{
								_eventManager.triggerEvent<window_events::GamepadKeyUp>(
									{
										i, 
										button.second
									}
								);
							}
						}
					}
				}

				if (oldState.Gamepad.sThumbLX != state.Gamepad.sThumbLX
					|| oldState.Gamepad.sThumbLY != state.Gamepad.sThumbLY)
				{
					double oldX = oldState.Gamepad.sThumbLX / AXIS_SIZE;
					double oldY = oldState.Gamepad.sThumbLY / AXIS_SIZE;

					double newX = state.Gamepad.sThumbLX / AXIS_SIZE;
					double newY = state.Gamepad.sThumbLY / AXIS_SIZE;

					_eventManager.triggerEvent<window_events::GamepadAxis>(
						{
							i,
							0,
							newX,
							newY,
							oldX,
							oldY
						}
					);
				}

				if (oldState.Gamepad.sThumbRX != state.Gamepad.sThumbRX
					|| oldState.Gamepad.sThumbRY != state.Gamepad.sThumbRY)
				{
					double oldX = oldState.Gamepad.sThumbRX / AXIS_SIZE;
					double oldY = oldState.Gamepad.sThumbRY / AXIS_SIZE;

					double newX = state.Gamepad.sThumbRX / AXIS_SIZE;
					double newY = state.Gamepad.sThumbRY / AXIS_SIZE;

					_eventManager.triggerEvent<window_events::GamepadAxis>(
						{
							i,
							1,
							newX,
							newY,
							oldX,
							oldY
						}
					);
				}
			}
			else
			{
				DEBUG_OUT("Gamepad", "Connect %i", i);

				_states[i] = state;

				_eventManager.triggerEvent<window_events::GamepadConnect>({ i });
			}
		}
		else
		{
			if (_states[i])
			{
				DEBUG_OUT("Gamepad", "Disconnect %i", i);

				_states[i] = std::nullopt;
				
				_eventManager.triggerEvent<window_events::GamepadDisconnect>({ i });
			}
		}
	}
}

} }
