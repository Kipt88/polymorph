#include "os/WindowEventSource.hpp"

namespace pm { namespace os {

template <typename WindowEventType, typename Callable>
WindowEventManager::scope_t<WindowEventType> WindowEventSource::addListener(Callable&& action)
{
	return _eventManager.addListener<WindowEventType>(std::forward<Callable>(action));
}

} }
