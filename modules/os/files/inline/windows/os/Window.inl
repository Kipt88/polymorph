#include "os/Window.hpp"

namespace pm { namespace os {

template <typename WindowEventType, typename Callable>
WindowEventManager::scope_t<WindowEventType> Window::addWindowEventListener(Callable&& action)
{
	return _eventSource.addListener<WindowEventType>(std::forward<Callable>(action));
}


} }
