#include "os/App.hpp"

namespace pm { namespace os {

namespace detail {

template <typename EventType, typename Callable>
concurrency::Future<WindowEventManager::scope_t<EventType>>
	addEventListener(AppSystem& system, Callable&& action)
{
	return system.pushAction(
		[action = std::forward<Callable>(action)](App& runner) mutable
		{
			return runner.window().addWindowEventListener<EventType>(action);
		}
	);
}

}

template <typename EventType>
template <typename Callable>
AppEventScope<EventType>::AppEventScope(AppSystem& system, Callable&& action)
: _system(&system),
  _future(detail::addEventListener<EventType>(system, std::forward<Callable>(action)))
{
}

template <typename EventType>
AppEventScope<EventType>::AppEventScope(AppEventScope&& other)
: _system(other._system),
  _future(std::move(other._future))
{
	other._system = nullptr;
}

template <typename EventType>
AppEventScope<EventType>::~AppEventScope()
{
	if (_system)
	{
		auto&& destroyFuture = reset();
		destroyFuture.get();
	}
}

template <typename EventType>
AppEventScope<EventType>& AppEventScope<EventType>::operator=(AppEventScope&& other)
{
	if (_system)
	{
		auto&& destroyFuture = reset();
		destroyFuture.get();
	}

	_system = other._system;
	_future = std::move(other._future);

	other._system = nullptr;

	return *this;
}

template <typename EventType>
concurrency::Future<void> AppEventScope<EventType>::reset()
{
	ASSERT(_system != nullptr, "Trying to reset already reset event scope.");

	auto& system = *_system;
	_system = nullptr;

	return system.pushAction(
		[scope = std::move(_future.get())](App&) mutable
		{
			scope.reset();
		}
	);
}

// AppSystem:

template <typename Callable>
concurrency::Future<std::result_of_t<Callable(App&)>> AppSystem::pushAction(Callable&& action)
{
	return _actions.access().pushAction(std::forward<Callable>(action));
}

template <typename EventType, typename Callable>
AppEventScope<EventType> AppSystem::addWindowEventListener(Callable&& action)
{
	return AppEventScope<EventType>{*this, std::forward<Callable>(action)};
}

} }
