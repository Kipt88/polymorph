#pragma once

#import <AppKit/AppKit.h>

namespace pm { namespace os {

namespace Keys {

/*
constexpr unsigned int MOUSE_LEFT = VK_LBUTTON;
constexpr unsigned int MOUSE_RIGHT = VK_RBUTTON;
constexpr unsigned int MOUSE_MIDDLE = VK_MBUTTON;
*/
/*
constexpr unsigned int BACKSPACE = VK_BACK;
constexpr unsigned int TAB = VK_TAB;
constexpr unsigned int RETURN = VK_RETURN;
constexpr unsigned int SHIFT = VK_SHIFT;
constexpr unsigned int CONTROL = VK_CONTROL;
constexpr unsigned int ESCAPE = VK_ESCAPE;
constexpr unsigned int SPACE = VK_SPACE;
constexpr unsigned int DEL = VK_DELETE;

constexpr unsigned int LEFT = VK_LEFT;
constexpr unsigned int UP = VK_UP;
constexpr unsigned int RIGHT = VK_RIGHT;
constexpr unsigned int DOWN = VK_DOWN;

constexpr unsigned int PLUS = VK_OEM_PLUS;
constexpr unsigned int MINUS = VK_OEM_MINUS;

constexpr unsigned int NUMPAD_MINUS = VK_SUBTRACT;
constexpr unsigned int NUMPAD_PLUS = VK_ADD;
*/
constexpr unsigned int SPACE = u' ';
constexpr unsigned int RETURN = u'\r';

constexpr unsigned int A = u'a';
constexpr unsigned int B = u'b';
constexpr unsigned int C = u'c';
constexpr unsigned int D = u'd';
constexpr unsigned int E = u'e';
constexpr unsigned int F = u'f';
constexpr unsigned int G = u'g';
constexpr unsigned int H = u'h';
constexpr unsigned int I = u'i';
constexpr unsigned int J = u'j';
constexpr unsigned int K = u'k';
constexpr unsigned int L = u'l';
constexpr unsigned int M = u'm';
constexpr unsigned int N = u'n';
constexpr unsigned int O = u'o';
constexpr unsigned int P = u'p';
constexpr unsigned int Q = u'q';
constexpr unsigned int R = u'r';
constexpr unsigned int S = u's';
constexpr unsigned int T = u't';
constexpr unsigned int U = u'u';
constexpr unsigned int V = u'v';
constexpr unsigned int W = u'w';
constexpr unsigned int X = u'x';
constexpr unsigned int Y = u'y';
constexpr unsigned int Z = u'z';

constexpr unsigned int F1 = NSF1FunctionKey;
constexpr unsigned int F2 = NSF2FunctionKey;
constexpr unsigned int F3 = NSF3FunctionKey;
constexpr unsigned int F4 = NSF4FunctionKey;
constexpr unsigned int F5 = NSF5FunctionKey;
constexpr unsigned int F6 = NSF6FunctionKey;
constexpr unsigned int F7 = NSF7FunctionKey;
constexpr unsigned int F8 = NSF8FunctionKey;
constexpr unsigned int F9 = NSF9FunctionKey;
constexpr unsigned int F10 = NSF10FunctionKey;
constexpr unsigned int F11 = NSF11FunctionKey;
constexpr unsigned int F12 = NSF12FunctionKey;

// TODO Fix:
constexpr std::array<unsigned int, 10> NUM = {{0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39}};
constexpr std::array<unsigned int, 10> NUMPAD = {{u'0', u'1', u'2', u'3', u'4', u'5', u'6', u'7', u'8', u'9'}};

constexpr bool isNum(unsigned int key)
{
	return 0x30 <= key && key <= 0x39;
}

constexpr unsigned int keyToNum(unsigned int num)
{
	return num - 0x30;
}

constexpr bool isAlpha(unsigned int key)
{
	return A <= key && key <= Z;
}

constexpr char toAlpha(unsigned int key)
{
	return isAlpha(key) ? key : 0;
}

}

} }
