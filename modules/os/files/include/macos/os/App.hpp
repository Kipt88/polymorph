#pragma once

#include "os/Window.hpp"

#include <concurrency/TaskQueue.hpp>
#include <concurrency/Thread.hpp>

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>
@end

namespace pm { namespace os {

class AppSystem;

template <typename EventType>
class AppEventScope : NonCopyable
{
public:
	template <typename Callable>
	explicit AppEventScope(AppSystem& system, Callable&& action);

	AppEventScope(AppEventScope&& other);

	~AppEventScope();

	AppEventScope& operator=(AppEventScope&& other);

	concurrency::Future<void> reset();

private:
	AppSystem* _system;
	concurrency::Future<WindowEventManager::scope_t<EventType>> _future;
};

class App
{
public:
	explicit App(
		concurrency::TaskQueue<concurrency::dynamic_return_tag(App&)>& queue,
		const std::string& windowTitle,
		unsigned int winW,
		unsigned int winH
	);
	
	Window& window();
	
private:
	__strong AppDelegate* _delegate;
};

class AppSystem
{
public:
	explicit AppSystem(const std::string& windowTitle, unsigned int winW, unsigned int winH);
	
	void enter();
	
	void quit();
	
	template <typename Callable>
	concurrency::Future<std::result_of_t<Callable(App&)>> pushAction(Callable&& action);
	
	template <typename EventType, typename Callable>
	AppEventScope<EventType> addWindowEventListener(Callable&& action);
	
private:
	concurrency::TaskQueue<concurrency::dynamic_return_tag(App&)> _actions;
	App _app;
};

} }

#include "os/App.inl"
