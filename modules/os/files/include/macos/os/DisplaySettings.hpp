#pragma once

#include "os/Exception.hpp"

#include <NonCopyable.hpp>
#include <vector>

#import <CoreGraphics/CoreGraphics.h>

namespace pm { namespace os {

PM_MAKE_EXCEPTION_CLASS(DisplayException, Exception);
PM_MAKE_EXCEPTION_CLASS(ResolutionChangeException, DisplayException);

// TODO Deal with multiple monitors.

class DisplaySettings : NonMovable
{
public:
	DisplaySettings(const DisplaySettings& other);
	~DisplaySettings();
	
	DisplaySettings& operator=(const DisplaySettings& other);
	
	unsigned long bitsPerPixel() const;
	unsigned long width() const;
	unsigned long height() const;
	unsigned long frequency() const;

	void activate();

	static void activateDefault();
	static std::vector<DisplaySettings> enumerate();
	static DisplaySettings current();

private:
	explicit DisplaySettings(CGDisplayModeRef mode);
	
	CGDisplayModeRef _mode;
	
	static CGDisplayModeRef defaultMode;
};

} }
