#pragma once

#include "os/window_events.hpp"
#include "os/DisplaySettings.hpp"
#include <NonCopyable.hpp>

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>


namespace pm { namespace os {

/**
 * OS window.
 */
class Window : NonCopyable
{
public:
	/**
	 * Creates and shows a window in windowed mode.
	 */
	explicit Window(const std::string& title, long width = 800, long height = 600);

	/**
	 * Creates and shows a window in fullscreen mode.
	 */
	// explicit Window(const std::string& title, DisplaySettings& settings);

	~Window();

	/**
	 * Shows or hides the window.
	 * @return true if the window was visible previously
	 */
	bool show(bool show = true);

	void enterFullscreen(DisplaySettings& settings);
	void exitFullscreen();
	void setSize(unsigned long w, unsigned long h);

	/**
	 * Polls the window event source.
	 * @return true if the event source has more events in queue.
	 */
	bool pollEvent();

	template <typename WindowEventType, typename Callable>
	WindowEventManager::scope_t<WindowEventType> addWindowEventListener(Callable&& action);

	// std::experimental::filesystem::path openFileDialog() const;

	/**
	 * @return A handle to the underlying native window
	 */
	NSWindow* nativeHandle();

private:
	WindowEventManager _eventManager;
	
	__strong NSWindow* _impl;
	__strong id<NSWindowDelegate> _delegate;
	
	bool _inFullscreen;
};

} }

// TODO Move to .inl-file.

namespace pm { namespace os {

template <typename WindowEventType, typename Callable>
WindowEventManager::scope_t<WindowEventType> Window::addWindowEventListener(Callable&& action)
{
	return _eventManager.addListener<WindowEventType>(std::forward<Callable>(action));
}

} }

