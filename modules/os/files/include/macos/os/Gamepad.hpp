#pragma once

#include "os/window_events.hpp"
#include <optional.hpp>

#include <array>
#include <bitset>

namespace pm { namespace os {

class Gamepad
{
public:
	explicit Gamepad(WindowEventManager& eventManager);

	void poll();

	static constexpr unsigned int MAX_CONTROLLERS = 4;

private:
	WindowEventManager& _eventManager;
};

} }
