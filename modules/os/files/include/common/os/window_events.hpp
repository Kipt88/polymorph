#pragma once

#include <Event.hpp>

namespace pm { namespace os {

typedef unsigned int key_t;
typedef unsigned int pointer_id_t;
typedef unsigned short pointer_button_t;

enum class GamepadButton : unsigned int
{
	DPAD_UP,
	DPAD_DOWN,
	DPAD_LEFT,
	DPAD_RIGHT,
	START,
	BACK,
	LEFT_THUMB,
	RIGHT_THUMB,
	LEFT_SHOULDER,
	RIGHT_SHOULDER,
	A,
	B,
	X,
	Y,
	ENUM_SIZE
};

namespace window_events
{
	struct Show	{};
	struct Hide {};
	struct Quit {};
	struct Resize {	int w; int h; };

	struct KeyDown { key_t key; };
	struct KeyUp  { key_t key; };

	struct PointerDown { unsigned long winW; unsigned long winH; pointer_id_t id; pointer_button_t button; long x; long y; };
	struct PointerMove { unsigned long winW; unsigned long winH; pointer_id_t id; long x; long y; long lastX; long lastY; };
	struct PointerDrag { unsigned long winW; unsigned long winH; pointer_id_t id; long x; long y; long lastX; long lastY; };
	struct PointerUp   { unsigned long winW; unsigned long winH; pointer_id_t id; pointer_button_t button; long x; long y; };
	struct PointerZoom { float dz; };

	struct GamepadConnect    { unsigned int id; };
	struct GamepadDisconnect { unsigned int id; };
	struct GamepadKeyUp      { unsigned int id; GamepadButton button; };
	struct GamepadKeyDown    { unsigned int id; GamepadButton button; };
	struct GamepadAxis       { unsigned int id; unsigned int axis;  double newX; double newY; double oldX; double oldY; };
}

typedef EventManager<
	window_events::Show,
	window_events::Quit,
	window_events::Hide,
	window_events::Resize,
	window_events::KeyDown,
	window_events::KeyUp,
	window_events::PointerDown,
	window_events::PointerDrag,
	window_events::PointerMove,
	window_events::PointerUp,
	window_events::PointerZoom,
	window_events::GamepadConnect,
	window_events::GamepadDisconnect,
	window_events::GamepadKeyUp,
	window_events::GamepadKeyDown,
	window_events::GamepadAxis
> WindowEventManager;

} }
