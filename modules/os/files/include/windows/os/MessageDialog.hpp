#pragma once

#include <concurrency/Thread.hpp>
#include <concurrency/Task.hpp>

#include <windows.h>

namespace pm { namespace os {

/**
 * Non-blocking message dialog.
 * @FIXME Dialog is blocking main WinProc.
 */
class MessageDialog
{
public:
	enum class Buttons : UINT
	{
		BUTTON_OK			= MB_OK,
		BUTTON_RETRY_CANCEL	= MB_RETRYCANCEL,
		BUTTON_YES_NO		= MB_YESNO
	};

	enum class Type : UINT
	{
		TYPE_WARNING	= MB_ICONWARNING,
		TYPE_ERROR		= MB_ICONERROR,
		TYPE_QUESTION	= MB_ICONQUESTION,
		TYPE_INFO		= MB_ICONINFORMATION
	};

	enum class UserResponse : int
	{
		RESP_ABORT		= IDABORT,
		RESP_CANCEL		= IDCANCEL,
		RESP_IGNORE		= IDIGNORE,
		RESP_NO			= IDNO,
		RESP_OK			= IDOK,
		RESP_RETRY		= IDRETRY,
		RESP_TRYAGAIN	= IDTRYAGAIN,
		RESP_YES		= IDYES
	};

	MessageDialog(const std::string& title, 
				  const std::string& message, 
				  Buttons buttons, 
				  Type type,
				  HWND parent = NULL);

	MessageDialog(MessageDialog&& other);

	concurrency::Future<UserResponse> show();

private:
	const std::string	_title;
	const std::string	_message;

	const Buttons		_buttons;
	const Type			_type;
	const HWND			_parent;

	concurrency::Thread _runner;
};

} }
