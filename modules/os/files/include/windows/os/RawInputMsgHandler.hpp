#pragma once

#include "os/window_events.hpp"
#include <windows.h>

namespace pm { namespace os {

class RawInputMsgHandler
{
public:
	RawInputMsgHandler(HWND window);

	bool pollEvent(WindowEventManager& eventManager, 
				   HWND window, 
				   UINT msg,
				   WPARAM wParam, 
				   LPARAM lParam);

private:
	USHORT _mouseButtonDownFlags;
};

} }