#pragma once

#include "os/window_events.hpp"
#include <optional.hpp>

#include <Windows.h>
#include <Xinput.h>

#include <array>
#include <bitset>

namespace pm { namespace os {

class Gamepad
{
public:
	explicit Gamepad(WindowEventManager& eventManager);

	void poll();

	static constexpr unsigned int MAX_CONTROLLERS = 4;

private:

	std::array<std::optional<XINPUT_STATE>, MAX_CONTROLLERS> _states;

	WindowEventManager& _eventManager;
};

} }
