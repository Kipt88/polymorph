#pragma once

#include "os/window_events.hpp"
#include "os/WindowEventSource.hpp"
#include "os/DisplaySettings.hpp"

#include <Event.hpp>

#include <windows.h>

#include <memory>
#include <set>
#include <string>
#include <experimental/filesystem>

namespace pm { namespace os {

PM_MAKE_EXCEPTION_CLASS(WindowException, Exception);

namespace detail {

/**
 * Custom deleter type to for HWND__ pointers.
 */
struct window_deleter
{
	void operator()(HWND window);
};

}

/**
 * OS window.
 */
class Window : NonCopyable
{
public:
	/**
	 * Creates and shows a window in windowed mode.
	 */
	explicit Window(const std::string& title, long width = 800, long height = 600);

	/**
	 * Creates and shows a window in fullscreen mode.
	 */
	explicit Window(const std::string& title, DisplaySettings& settings);

	~Window();

	/**
	 * Shows or hides the window.
	 * @return true if the window was visible previously
	 */
	bool show(bool show = true);

	void enterFullscreen(DisplaySettings& settings);
	void exitFullscreen();
	void setSize(unsigned long w, unsigned long h);

	/**
	 * Polls the window event source.
	 * @return true if the event source has more events in queue.
	 */
	bool pollEvent();

	template <typename WindowEventType, typename Callable>
	WindowEventManager::scope_t<WindowEventType> addWindowEventListener(Callable&& action);

	std::experimental::filesystem::path openFileDialog() const;

	/**
	 * @return A handle to the underlying native window
	 */
	HWND nativeHandle();

private:
	// NOTE: surface has to be destroyed before the window
	std::unique_ptr<HWND__, detail::window_deleter> _windowHandle;

	struct WindowedData
	{
		RECT rect;
	} _windowedData;

	bool _inFullscreen;
	
	WindowEventSource	_eventSource;
};

} }

#include "os/Window.inl"