#pragma once

#include "os/window_events.hpp"
#include "os/RawInputMsgHandler.hpp"
#include "os/QuitMsgHandler.hpp"
#include "os/FocusMsgHandler.hpp"
#include "os/SysCommandMsgHandler.hpp"
#include "os/SizeMsgHandler.hpp"
#include "os/Gamepad.hpp"
#include "os/Exception.hpp"

#include <Event.hpp>

#include <windows.h>

namespace pm { namespace os {

/** 
 * @see "WindowProc callback function" in MSDN
 */
LRESULT CALLBACK handler(HWND window,
                         UINT msg,
                         WPARAM wParam,
                         LPARAM lParam);

class WindowEventSource : NonCopyable
{
public:
	// Source of events.
	friend LRESULT CALLBACK handler(HWND window,
	                                UINT msg,
	                                WPARAM wParam,
	                                LPARAM lParam);

	typedef std::tuple<
		RawInputMsgHandler,
		QuitMsgHandler,
		FocusMsgHandler,
		SysCommandMsgHandler,
		SizeMsgHandler
	> InputHandler;

	explicit WindowEventSource(HWND window);

	WindowEventSource(WindowEventSource&&) = default;

	bool handleInput();

	template <typename WindowEventType, typename Callable>
	WindowEventManager::scope_t<WindowEventType> addListener(Callable&& action);

private:
	WindowEventManager _eventManager;
	InputHandler _inputHandlers;
	
	Gamepad _gamepad; // Could be someplace else maybe...

	HWND _window;
};

} }

#include "os/WindowEventSource.inl"