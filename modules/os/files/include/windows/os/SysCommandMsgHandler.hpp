#pragma once

#include "os/window_events.hpp"
#include <windows.h>

namespace pm { namespace os {

class SysCommandMsgHandler
{
public:
	bool pollEvent(WindowEventManager& eventManager,
				   HWND window,
				   UINT msg,
				   WPARAM wParam,
				   LPARAM lParam);
};

} }