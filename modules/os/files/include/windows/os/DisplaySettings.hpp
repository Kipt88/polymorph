#pragma once

#include "os/Exception.hpp"

#include <windows.h>
#include <vector>

namespace pm { namespace os {

PM_MAKE_EXCEPTION_CLASS(DisplayException, Exception);

class ResolutionChangeException : public DisplayException
{
public:
	ResolutionChangeException(LONG errCode);

	const char* what() const noexcept override;

	const LONG errorCode() const;

private:
	LONG _errCode;
};

class DisplaySettings
{
public:
	unsigned long bitsPerPixel() const;
	unsigned long width() const;
	unsigned long height() const;
	unsigned long frequency() const;

	void activate();

	static void activateDefault();
	static std::vector<DisplaySettings> enumerate();
	static DisplaySettings current();

private:
	DisplaySettings(DEVMODE devmode);

	DEVMODE _devmode;
};

} }