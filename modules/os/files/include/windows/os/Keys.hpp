#pragma once

#include <windows.h>
#include <array>

namespace pm { namespace os {

namespace Keys {

constexpr unsigned int MOUSE_LEFT = VK_LBUTTON;
constexpr unsigned int MOUSE_RIGHT = VK_RBUTTON;
constexpr unsigned int MOUSE_MIDDLE = VK_MBUTTON;

constexpr unsigned int BACKSPACE = VK_BACK;
constexpr unsigned int TAB = VK_TAB;
constexpr unsigned int RETURN = VK_RETURN;
constexpr unsigned int SHIFT = VK_SHIFT;
constexpr unsigned int CONTROL = VK_CONTROL;
constexpr unsigned int ESCAPE = VK_ESCAPE;
constexpr unsigned int SPACE = VK_SPACE;
constexpr unsigned int DEL = VK_DELETE;

constexpr unsigned int LEFT = VK_LEFT;
constexpr unsigned int UP = VK_UP;
constexpr unsigned int RIGHT = VK_RIGHT;
constexpr unsigned int DOWN = VK_DOWN;

constexpr unsigned int PLUS = VK_OEM_PLUS;
constexpr unsigned int MINUS = VK_OEM_MINUS;

constexpr unsigned int NUMPAD_MINUS = VK_SUBTRACT;
constexpr unsigned int NUMPAD_PLUS = VK_ADD;

constexpr unsigned int A = 0x41;
constexpr unsigned int B = 0x42;
constexpr unsigned int C = 0x43;
constexpr unsigned int D = 0x44;
constexpr unsigned int E = 0x45;
constexpr unsigned int F = 0x46;
constexpr unsigned int G = 0x47;
constexpr unsigned int H = 0x48;
constexpr unsigned int I = 0x49;
constexpr unsigned int J = 0x4a;
constexpr unsigned int K = 0x4b;
constexpr unsigned int L = 0x4c;
constexpr unsigned int M = 0x4d;
constexpr unsigned int N = 0x4e;
constexpr unsigned int O = 0x4f;
constexpr unsigned int P = 0x50;
constexpr unsigned int Q = 0x51;
constexpr unsigned int R = 0x52;
constexpr unsigned int S = 0x53;
constexpr unsigned int T = 0x54;
constexpr unsigned int U = 0x55;
constexpr unsigned int V = 0x56;
constexpr unsigned int W = 0x57;
constexpr unsigned int X = 0x58;
constexpr unsigned int Y = 0x59;
constexpr unsigned int Z = 0x5a;

constexpr unsigned int F1 = VK_F1;
constexpr unsigned int F2 = VK_F2;
constexpr unsigned int F3 = VK_F3;
constexpr unsigned int F4 = VK_F4;
constexpr unsigned int F5 = VK_F5;
constexpr unsigned int F6 = VK_F6;
constexpr unsigned int F7 = VK_F7;
constexpr unsigned int F8 = VK_F8;
constexpr unsigned int F9 = VK_F9;
constexpr unsigned int F10 = VK_F10;
constexpr unsigned int F11 = VK_F11;
constexpr unsigned int F12 = VK_F12;

constexpr std::array<unsigned int, 10> NUM = {0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39};
constexpr std::array<unsigned int, 10> NUMPAD = { 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69 };

constexpr bool isNum(unsigned int key)
{
	return 0x30 <= key && key <= 0x39;
}

constexpr unsigned int keyToNum(unsigned int num)
{
	return num - 0x30;
}

constexpr bool isAlpha(unsigned int key)
{
	return A <= key && key <= Z;
}

constexpr char toAlpha(unsigned int key)
{
	return isAlpha(key) ? key : 0;
}

}

} }
