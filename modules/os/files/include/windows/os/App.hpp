#pragma once

#include <os/Window.hpp>
#include <concurrency/System.hpp>
#include <NonCopyable.hpp>
#include <string>

namespace pm { namespace os {

class AppSystem;

template <typename EventType>
class AppEventScope : NonCopyable
{
public:
	template <typename Callable>
	explicit AppEventScope(AppSystem& system, Callable&& action);

	AppEventScope(AppEventScope&& other);

	~AppEventScope();

	AppEventScope& operator=(AppEventScope&& other);

	concurrency::Future<void> reset();

private:
	AppSystem* _system;
	concurrency::Future<WindowEventManager::scope_t<EventType>> _future;
};

class App : Unique
{
public:
	static constexpr const char* name = "App";

	explicit App(
		concurrency::TaskQueue<concurrency::dynamic_return_tag(App&)>& queue,
		const std::string& windowTitle,
		unsigned int winW = 800,
		unsigned int winH = 600
	);

	bool update(const concurrency::FrameTime&);

	void shutdown();

	Window& window();
	const Window& window() const;

private:
	Window _window;

	bool _exiting;
};

class AppSystem : public concurrency::ConcurrentSystem<App>
{
public:
	using concurrency::ConcurrentSystem<App>::ConcurrentSystem;

	AppSystem() = delete;

	template <typename EventType, typename Callable>
	AppEventScope<EventType> addWindowEventListener(Callable&& action);
};

} }

#include "os/App.inl"