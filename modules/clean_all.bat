@echo off
rmdir /S /Q ..\build
rmdir /S /Q concurrency\build
rmdir /S /Q ecs\build
rmdir /S /Q ext\build
rmdir /S /Q filesystem\build
rmdir /S /Q gr\build
rmdir /S /Q gr2d\build
rmdir /S /Q io\build
rmdir /S /Q os\build
rmdir /S /Q pm_td\build