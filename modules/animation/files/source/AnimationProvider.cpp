#include "animation/AnimationProvider.hpp"

namespace pm { namespace animation {

AnimationProvider::AnimationProvider(const path_t& root)
: _root(root)
{
}

auto AnimationProvider::loadAnimation(animation_id_t id) -> AnimationHandle
{
	// TODO
	return nullptr;
}

auto AnimationProvider::registerAnimation(animation_id_t animation, const path_t& path) -> animation_id_t
{
	auto& metaData = getMetaData();

	ASSERT(
		metaData.animations.find(animation) == metaData.animations.end(),
		"Trying to overwrite animation registry."
	);

	metaData.animations.insert(
		std::make_pair(
			animation,
			path
		)
	);

	return animation;
}

auto AnimationProvider::getMetaData() -> MetaData&
{
	static MetaData metaData;

	return metaData;
}

} }
