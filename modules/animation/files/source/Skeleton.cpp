#include "animation/Skeleton.hpp"

#include <Assert.hpp>

namespace pm { namespace animation {

joint_handle::joint_handle()
: _skeleton(nullptr),
  _index(null_joint_index)
{
}

joint_handle::joint_handle(
	Skeleton& skeleton,
	joint_index_t index
)
: _skeleton(&skeleton),
  _index(index)
{
}

auto joint_handle::bindTransform() const -> const Transform2&
{
	return _skeleton->_joints[_index].bindTransform;
}

auto joint_handle::bindTransform() -> Transform2&
{
	return _skeleton->_joints[_index].bindTransform;
}

auto joint_handle::parent() const -> joint_handle
{
	ASSERT(*this, "Can't get parent, this is a null joint node.");

	return joint_handle(*_skeleton, _skeleton->_joints[_index].parentIndex);
}

auto joint_handle::index() const -> joint_index_t
{
	return _index;
}

joint_handle::operator bool() const
{
	return _index != null_joint_index;
}

joint_handle::operator const_joint_handle() const
{
	return const_joint_handle(*_skeleton, _index);
}


const_joint_handle::const_joint_handle()
: _skeleton(nullptr),
  _index(null_joint_index)
{
}

const_joint_handle::const_joint_handle(
	const Skeleton& skeleton,
	joint_index_t index
)
: _skeleton(&skeleton),
  _index(index)
{
}

auto const_joint_handle::bindTransform() const -> const Transform2&
{
	return _skeleton->_joints[_index].bindTransform;
}

auto const_joint_handle::parent() const -> const_joint_handle
{
	ASSERT(*this, "Can't get parent, this is a null joint node.");

	return const_joint_handle(*_skeleton, _skeleton->_joints[_index].parentIndex);
}

auto const_joint_handle::index() const -> joint_index_t
{
	return _index;
}

const_joint_handle::operator bool() const
{
	return _index != null_joint_index;
}


Skeleton::Skeleton(std::size_t initialCapacity)
: _joints()
{
	_joints.reserve(initialCapacity);
}

auto Skeleton::pushJoint(const Transform2& bindTransform, const_joint_handle parent) -> joint_handle
{
	_joints.push_back({ bindTransform, parent ? parent.index() : null_joint_index });

	return joint_handle{ *this, static_cast<joint_index_t>(_joints.size() - 1) };
}

auto Skeleton::joint(joint_index_t i) -> joint_handle
{
	return joint_handle(*this, i);
}

auto Skeleton::joint(joint_index_t i) const -> const_joint_handle
{
	return const_joint_handle(*this, i);
}

auto Skeleton::size() const -> std::size_t
{
	return _joints.size();
}

} }
