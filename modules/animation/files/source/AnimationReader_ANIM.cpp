#include "animation/AnimationReader_ANIM.hpp"

#include "animation/AnimationReader.hpp"
#include <Id.hpp>

namespace pm { namespace animation {

namespace AnimationReader_ANIM {

// File format:
//
//attachments:
//  size (u16)
//    id (u64 {id_t})
//
//joints:
//  size (u16)
//    id (u64 {id_t})
//    parent index (u16)
//    transform (x float, y float, scale float, rotation float, {SimilarityTransform2<float>})
//
//slots:
//  size (u16)
//    id (u64 {id_t})
//    attachment index (u16)
//    joint index (u16)
// 
//skins:
//  size (u16)
//    id (u64 {id_t})
//    slot size (u16)
//      slot index (u16)
//      attachment index (u16)
//      transform (x float, y float, scale float, rotation float, {SimilarityTransform2<float>})
//
//clips:
//  size (u16)
//    id (u64 {id_t})
//    joints size (u16)
//      joint index (u16)
//      translate size (u16)
//        type (u8)
//        bezier float[4]
//        t float
//        x float, y float
//      rotate size (u16)
//        type (u8)
//        bezier float[4]
//        t float
//        angle float
//      scale size (u16)
//        type (u8)
//        bezier float[4]
//        t float
//        scale float
//    slots size (u16)
//      slot index (u16)
//      attachment size (u16)
//        t (float)
//        attachment index (u16)

namespace {

struct DataSlot
{
	id_t id;
	std::uint16_t attachmentIndex;
	std::uint16_t jointIndex;
};

struct DataSkin
{
	struct Binding
	{
		std::uint16_t slotIndex;
		std::uint16_t attachmentIndex;
		math::SimilarityTransform2<float> transform;
	};

	id_t id;
	std::vector<Binding> bindings;
};

struct DataJoint
{
	id_t id;
	std::uint16_t parent;
	math::SimilarityTransform2<float> transform;
};

struct TranslateKey
{
	std::uint8_t type;
	std::array<float, 4> bezier;
	float t;

	float x;
	float y;
};

struct RotateKey
{
	std::uint8_t type;
	std::array<float, 4> bezier;
	float t;

	float angle;
};

struct ScaleKey
{
	std::uint8_t type;
	std::array<float, 4> bezier;
	float t;

	float scale;
};

struct jointClipData
{
	std::uint16_t jointIndex;

	std::vector<TranslateKey> translations;
	std::vector<RotateKey> rotations;
	std::vector<ScaleKey> scales;
};

struct AttachmentKey
{
	float t;
	std::uint16_t attachmentIndex;
};

struct SlotClipData
{
	std::uint16_t slotIndex;
	std::vector<AttachmentKey> attachments;
};

struct ClipData
{
	id_t id;
	
	std::vector<jointClipData> jointClips;
	std::vector<SlotClipData> slotClips;
};

std::vector<id_t> parseAttachments(io::InputStream& input)
{
	std::vector<id_t> attachments;

	{
		std::uint16_t size;
		input >> size;

		attachments.resize(size);
		io::readBlock(input, attachments.data(), size);
	}

	return attachments;
}

std::vector<DataJoint> parseJoints(io::InputStream& input)
{
	std::vector<DataJoint> joints;

	{
		std::uint16_t size;
		input >> size;

		joints.resize(size);

		for (auto& joint : joints)
		{
			input >> joint.id;
			input >> joint.parent;
			input >> joint.transform;
		}
	}

	return joints;
}

std::vector<DataSlot> parseSlots(io::InputStream& input)
{
	std::vector<DataSlot> slots;

	{
		std::uint16_t size;
		input >> size;

		slots.resize(size);

		for (auto& slot : slots)
		{
			input >> slot.id;
			input >> slot.attachmentIndex;
			input >> slot.jointIndex;
		}
	}

	return slots;
}

std::vector<DataSkin> parseSkins(io::InputStream& input)
{
	std::vector<DataSkin> skins;

	{
		std::uint16_t size;
		input >> size;

		skins.resize(size);
	}

	for (auto& skin : skins)
	{
		{
			id_t id;
			input >> id;

			skin.id = id;
		}

		{
			std::uint16_t size;
			input >> size;

			skin.bindings.resize(size);
			
			for (auto& binding : skin.bindings)
			{
				input >> binding.slotIndex;
				input >> binding.attachmentIndex;
				input >> binding.transform;
			}
		}
	}

	return skins;
}

std::vector<ClipData> parseClips(io::InputStream& input)
{
	std::vector<ClipData> clips;

	{
		std::uint16_t size;
		input >> size;

		clips.resize(size);
	}

	for (auto& clip : clips)
	{
		input >> clip.id;

		{
			std::uint16_t size;
			input >> size;

			clip.jointClips.resize(size);
		}

		for (auto& jointClip : clip.jointClips)
		{
			input >> jointClip.jointIndex;

			{
				std::uint16_t size;
				input >> size;

				jointClip.translations.resize(size);
				io::readBlock(input, jointClip.translations.data(), size);
			}

			{
				std::uint16_t size;
				input >> size;

				jointClip.rotations.resize(size);
				io::readBlock(input, jointClip.rotations.data(), size);
			}

			{
				std::uint16_t size;
				input >> size;

				jointClip.scales.resize(size);
				io::readBlock(input, jointClip.scales.data(), size);
			}
		}

		{
			std::uint16_t size;
			input >> size;

			clip.slotClips.resize(size);
		}

		for (auto& slotClip : clip.slotClips)
		{
			input >> slotClip.slotIndex;

			{
				std::uint16_t size;
				input >> size;

				slotClip.attachments.resize(size);

				for (auto& attachment : slotClip.attachments)
					input >> attachment.t >> attachment.attachmentIndex;
			}
		}
	}

	return clips;
}

}

Animation read(io::InputStream& input)
{
	const auto& attachments = parseAttachments(input);
	const auto& joints      = parseJoints(input);
	const auto& slots       = parseSlots(input);
	const auto& skins       = parseSkins(input);
	const auto& clips       = parseClips(input);
	
	Animation animation;

	for (const auto& joint : joints)
	{
		if (joint.parent != static_cast<std::uint16_t>(-1))
			animation.skeleton.pushJoint(joint.transform, animation.skeleton.joint(joint.parent));
		else
			animation.skeleton.pushJoint(joint.transform);
	}

	for (const auto& skin : skins)
	{
		Skin animSkin;
		
		animSkin.attachments.reserve(skin.bindings.size());

		for (const auto& binding : skin.bindings)
		{
			Skin::Attachment attachment;
			attachment.attachmentId = attachments[binding.attachmentIndex];
			attachment.joint = animation.skeleton.joint(slots[binding.slotIndex].jointIndex);
			attachment.localTransform = binding.transform;

			animSkin.attachments.push_back(attachment);
		}

		animation.skins.insert(
			std::make_pair(
				skin.id,
				animSkin
			)
		);
	}

	for (const auto& clip : clips)
	{
		Clip animClip;

		for (const auto& jointClip : clip.jointClips)
		{
			auto joint = animation.skeleton.joint(jointClip.jointIndex);

			for (const auto& key : jointClip.translations)
			{
				animClip.addTranslationKey(
					joint,
					Clock::time_point{ Clock::duration{ key.t } },
					Vector2_r{ key.x, key.y },
					Clip::Interpolation{
						static_cast<Clip::Interpolation::CurveType>(key.type),
						key.bezier
					}
				);
			}

			for (const auto& key : jointClip.rotations)
			{
				animClip.addAngleKey(
					joint,
					Clock::time_point{ Clock::duration{ key.t } },
					key.angle,
					Clip::Interpolation{
						static_cast<Clip::Interpolation::CurveType>(key.type),
						key.bezier
					}
				);
			}

			for (const auto& key : jointClip.scales)
			{
				animClip.addScaleKey(
					joint,
					Clock::time_point{ Clock::duration{ key.t } },
					key.scale,
					Clip::Interpolation{
						static_cast<Clip::Interpolation::CurveType>(key.type),
						key.bezier
					}
				);
			}
		}

		// TODO Add attachment clips...

		animation.clips.insert(
			std::make_pair(
				clip.id,
				animClip
			)
		);
	}

	return animation;
}

}

} }
