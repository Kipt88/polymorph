#include "animation/Clip.hpp"

#include <algorithm>

namespace pm { namespace animation {

namespace {

template <typename T>
T interpolate(const Clip::Interpolation& inter, float t, const T& v1, const T& v2)
{
	switch (inter.type)
	{
	case Clip::Interpolation::BEZIER:
		// TODO

	case Clip::Interpolation::LINEAR:
		return v1 + t * (v2 - v1);

	case Clip::Interpolation::STEPPED:
		return v1;
	}

	std::terminate();
}

template <typename T>
T interpolateKey(
	const std::vector<Clip::Key<T>>& keys, 
	const Clock::time_point& t,
	ClipWrap wrap,
	const Clock::duration& duration,
	T def
)
{
	auto it0 = std::lower_bound(
		keys.begin(),
		keys.end(),
		t,
		[](const AnimationClip2::Key<T>& key, const clip_clock::time_point& t)
		{
			return key.time < t;
		}
	);

	if (it0 != keys.end())
	{
		auto it1 = it0 + 1;
				
		clip_clock::time_point t0 = it0->time;
		clip_clock::time_point t1;

		if (it1 == keys.end())
		{
			if (wrap == ClipWrap::REPEAT)
			{
				it1 = keys.begin() + 1; // Last frame == first frame.

				if (it1 == keys.end()) // keys.size() == 1
					it1 = it0;
				else
					t1 = duration + it1->time;
			}
			else
			{
				it1 = it0;
				t1 = it1->time;
			}
		}
		else
		{
			t1 = it1->time;
		}

		float localTime = (t - t0) / (t1 - t0);

		def = interpolate(
			it0->interpolation, 
			localTime, 
			it0->value, 
			it1->value
		);
	}

	return def;
}

}

Clip::Clip()
: _duration(Clock::duration::zero()),
  _channels()
{
}

void Clip::addTranslationKey(
	const_joint_handle bone,
	const Clock::time_point& time,
	const Vector2_r& v,
	const Interpolation& interpolation
)
{
	_duration = std::max(_duration, time.time_since_epoch());

	Key<Vector2_r> key;

	key.time = time;
	key.value = v;
	key.interpolation = interpolation;

	auto& channel = findOrInsertChannel(bone);

	channel.translations.insert(
		std::lower_bound(
			channel.translations.begin(),
			channel.translations.end(),
			key.time,
			[](const Key<Vector2_r>& key, const Clock::time_point& time)
			{
				return key.time < time;
			}
		),
		key
	);
}

void Clip::addScaleKey(
	const_joint_handle bone,
	const Clock::time_point& time,
	real_t scale,
	const Interpolation& interpolation
)
{
	_duration = std::max(_duration, time.time_since_epoch());

	Key<real_t> key;

	key.time = time;
	key.value = scale;
	key.interpolation = interpolation;

	auto& channel = findOrInsertChannel(bone);

	channel.scales.insert(
		std::lower_bound(
			channel.scales.begin(),
			channel.scales.end(),
			key.time,
			[](const Key<real_t>& key, const Clock::time_point& time)
			{
				return key.time < time;
			}
		),
		key
	);
}

void Clip::addAngleKey(
	const_joint_handle bone,
	const Clock::time_point& time,
	real_t angle,
	const Interpolation& interpolation
)
{
	_duration = std::max(_duration, time.time_since_epoch());

	Key<real_t> key;

	key.time = time;
	key.value = angle;
	key.interpolation = interpolation;

	auto& channel = findOrInsertChannel(bone);

	channel.angles.insert(
		std::lower_bound(
			channel.angles.begin(),
			channel.angles.end(),
			key.time,
			[](const Key<real_t>& key, const Clock::time_point& time)
			{
				return key.time < time;
			}
		),
		key
	);
}

/*
void Clip::addMeshKey(
	Skeleton::const_joint_handle_t bone,
	const Clock::time_point& time,
	MeshHandle mesh
)
{
	_duration = std::max(_duration, time.time_since_epoch());

	MeshKey key;

	key.time = time;
	key.mesh = mesh;

	auto& channel = findOrInsertChannel(bone);

	channel.meshes.insert(
		std::lower_bound(
			channel.meshes.begin(),
			channel.meshes.end(),
			key.time,
			[](const MeshKey& key, const clip_clock::time_point& time)
			{
				return key.time < time;
			}
		),
		key
	);
}
*/

Clip::Channel& Clip::findOrInsertChannel(const_joint_handle bone)
{
	auto channelIt = std::lower_bound(
		_channels.begin(),
		_channels.end(),
		bone,
		[](const Channel& channel, const const_joint_handle& bone)
		{
			return channel.bone < bone;
		}
	);

	if (channelIt == _channels.end() || channelIt->bone != bone)
	{
		Channel newChannel;
		newChannel.bone = bone;

		channelIt = _channels.insert(channelIt, newChannel);
	}

	return *channelIt;
}

/*
AnimationPose2 AnimationClip2::evaluate(const clip_clock::time_point& time, ClipWrap wrap) const
{
	AnimationPose2 pose;

	const clip_clock::time_point animationStart{};
	clip_clock::time_point t = time;

	switch (wrap)
	{
	case ClipWrap::REPEAT:
		t = clip_clock::time_point{
			clip_clock::duration{
				std::fmod(t.time_since_epoch().count(), _duration.count())
			}
		};

		break;

	case ClipWrap::CLAMP:
		if (t < animationStart)
			t = animationStart;
		else if (t > animationStart + _duration)
			t = animationStart + _duration;
		break;
	}

	for (const auto& channel : _channels)
	{
		Transform2 transform;
		transform.translation = interpolateKey(channel.translations, time, wrap, _duration, Vector2_r::ZERO);
		transform.rotation    = interpolateKey(channel.angles, time, wrap, _duration, 0.0_r);
		transform.scale       = interpolateKey(channel.scales, time, wrap, _duration, 1.0_r);

		MeshHandle mesh = nullptr;

		{
			auto it = std::lower_bound(
				channel.meshes.begin(),
				channel.meshes.end(),
				t,
				[](const MeshKey& key, const clip_clock::time_point& t)
				{
					return key.time < t;
				}
			);

			if (it != channel.meshes.end())
				mesh = it->mesh;
		}

		pose.add(channel.bone, transform, mesh);
	}

	return pose;
}
*/

} }
