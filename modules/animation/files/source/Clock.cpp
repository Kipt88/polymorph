#include "animation/Clock.hpp"

namespace pm { namespace animation {

Clock::Clock()
: _t()
{
}

auto Clock::now() const -> time_point
{
	return _t;
}

auto Clock::increment(const duration& dt) -> time_point&
{
	_t += dt;

	return _t;
}

} }
