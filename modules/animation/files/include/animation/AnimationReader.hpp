#pragma once

#include "animation/Animation.hpp"
#include <io/IOStream.hpp>
#include <Exception.hpp>

#include <cstddef>

namespace pm { namespace animation {

PM_MAKE_EXCEPTION_CLASS(AnimationResourceException, Exception);
PM_MAKE_EXCEPTION_CLASS(UnknownAnimationFormat, AnimationResourceException);
PM_MAKE_EXCEPTION_CLASS(AnimationReadException, AnimationResourceException);

namespace AnimationReader
{
	typedef Animation(*Reader)(io::InputStream&);

	enum class animation_t : std::size_t
	{
		ANIM = 0
	};

	Animation read(io::InputStream& input, animation_t animationType);
}

} }
