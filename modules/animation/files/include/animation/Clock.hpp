#pragma once

#include "animation/types.hpp"
#include <chrono>

namespace pm { namespace animation {

class Clock
{
public:
	typedef real_t rep;
	typedef std::ratio<1> period;
	typedef std::chrono::duration<rep, period> duration;
	typedef std::chrono::time_point<Clock> time_point;

	static constexpr bool is_steady() { return false; }

	Clock();

	time_point now() const;
	time_point& increment(const duration& dt);

private:
	time_point _t;
};

} }
