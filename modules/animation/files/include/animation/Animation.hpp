#pragma once

#include "animation/Skeleton.hpp"
#include "animation/Clip.hpp"
#include "animation/Skin.hpp"
#include <memory>
#include <map>

namespace pm { namespace animation {

constexpr id_t DEFAULT_SKIN = "default"_id;

struct Animation
{
	Skeleton skeleton;

	std::map<id_t, Clip> clips;
	std::map<id_t, Skin> skins;
};

typedef std::shared_ptr<Animation> AnimationHandle;
typedef std::weak_ptr<Animation> WeakAnimationHandle;

template <typename... Args>
AnimationHandle make_animation(Args&&... args)
{
	return std::make_shared<Animation>(std::forward<Args>(args)...);
}

} }
