#pragma once

#include "animation/Skeleton.hpp"
#include "animation/types.hpp"
#include <Id.hpp>

namespace pm { namespace animation {

struct Skin
{
	struct Attachment
	{
		Transform2 localTransform;
		const_joint_handle joint;
		id_t attachmentId;
	};

	std::vector<Attachment> attachments;
};

} }
