#pragma once

#include <animation/types.hpp>
#include <vector>

namespace pm { namespace animation {

class Skeleton;

class const_joint_handle
{
public:
	const_joint_handle();

	explicit const_joint_handle(
		const Skeleton& skeleton,
		joint_index_t index
	);

	const Transform2& bindTransform() const;

	const_joint_handle parent() const;
	joint_index_t index() const;

	operator bool() const;

private:
	const Skeleton* _skeleton;
	joint_index_t _index;
};

class joint_handle
{
public:
	joint_handle();

	explicit joint_handle(
		Skeleton& skeleton,
		joint_index_t index
	);

	const Transform2& bindTransform() const;
	Transform2& bindTransform();

	joint_handle parent() const;
	joint_index_t index() const;

	operator bool() const;

	operator const_joint_handle() const;

private:
	Skeleton* _skeleton;
	joint_index_t _index;
};

class Skeleton
{
public:
	friend class const_joint_handle;
	friend class joint_handle;

	Skeleton() = default;
	explicit Skeleton(std::size_t initialCapacity);

	joint_handle pushJoint(
		const Transform2& bindTransform,
		const_joint_handle parent = const_joint_handle()
	);

	joint_handle joint(joint_index_t i);
	const_joint_handle joint(joint_index_t i) const;

	std::size_t size() const;

private:
	struct JointData
	{
		Transform2 bindTransform;
		joint_index_t parentIndex;
	};

	std::vector<JointData> _joints;
};

} }
