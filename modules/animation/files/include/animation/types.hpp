#pragma once

#include <math/Transform2.hpp>
#include <chrono>
#include <memory>

namespace pm { namespace animation {

typedef std::size_t joint_index_t;
typedef float real_t;
typedef math::SimilarityTransform2<real_t> Transform2;
typedef math::Vector<real_t, 2> Vector2_r;

constexpr joint_index_t null_joint_index = -1;

constexpr real_t operator "" _r(long double v) { return static_cast<real_t>(v); }

enum class ClipWrap
{
	CLAMP,
	REPEAT
};

} }
