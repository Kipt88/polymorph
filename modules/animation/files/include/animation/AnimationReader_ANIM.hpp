#pragma once

#include "animation/Animation.hpp"
#include <io/IOStream.hpp>

namespace pm { namespace animation {

namespace AnimationReader_ANIM
{
	Animation read(io::InputStream& input);
}

} }
