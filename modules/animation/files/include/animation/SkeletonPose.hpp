#pragma once

#include "animation/Skeleton.hpp"
#include "animation/types.hpp"
#include <utility>

namespace pm { namespace animation {

class SkeletonPose
{
public:
	explicit SkeletonPose(Skeleton& skeleton);

	std::size_t size() const;

	Transform2& jointTransform(std::size_t i);
	const Transform2& jointTransform(std::size_t i) const;



private:
	Skeleton& _skeleton;
	std::vector<Transform2> _pose;
};

} }
