#pragma once

// #include "gr/AnimationPose2.hpp"

#include "animation/Clock.hpp"
#include "animation/Skeleton.hpp"
#include "animation/types.hpp"
#include <vector>
#include <map>

namespace pm { namespace animation {


class Clip
{
public:
	struct Interpolation
	{
		enum CurveType
		{
			LINEAR,
			STEPPED,
			BEZIER
		} type;

		std::array<float, 4> bezier;
	};

	template <typename T>
	struct Key
	{
		Clock::time_point time;
		Interpolation interpolation;
		T value;
	};

	Clip();

	void addTranslationKey(
		const_joint_handle bone,
		const Clock::time_point& time, 
		const Vector2_r& v,
		const Interpolation& interpolation
	);

	void addScaleKey(
		const_joint_handle bone,
		const Clock::time_point& time,
		real_t scale,
		const Interpolation& interpolation
	);

	void addAngleKey(
		const_joint_handle bone,
		const Clock::time_point& time,
		real_t angle,
		const Interpolation& interpolation
	);
	/*
	void addMeshKey(
		joint_handle_t bone,
		const clip_clock::time_point& time,
		MeshHandle mesh
	);
	*/
	// AnimationPose2 evaluate(const clip_clock::time_point& time, ClipWrap wrap) const;

private:
	/*
	struct MeshKey
	{
		clip_clock::time_point time;
		MeshHandle mesh;
	};
	*/

	struct Channel
	{
		const_joint_handle bone;

		std::vector<Key<Vector2_r>> translations;
		std::vector<Key<real_t>> scales;
		std::vector<Key<real_t>> angles;
		// std::vector<MeshKey> meshes;
	};

	Channel& findOrInsertChannel(const_joint_handle bone);

	Clock::duration _duration;
	std::vector<Channel> _channels;
};

} }
