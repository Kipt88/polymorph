#pragma once

#include "animation/Animation.hpp"
#include <Cache.hpp>
#include <Id.hpp>
#include <NonCopyable.hpp>
#include <experimental/filesystem>

namespace pm { namespace animation {

class AnimationProvider : Unique
{
public:
	typedef std::experimental::filesystem::path path_t;

	typedef id_t animation_id_t;

	explicit AnimationProvider(
		const path_t& root = std::experimental::filesystem::current_path()
	);

	AnimationHandle loadAnimation(animation_id_t id);

	static animation_id_t registerAnimation(animation_id_t id, const path_t& path);

private:
	struct MetaData
	{
		std::map<animation_id_t, path_t> animations;
	};

	path_t _root;

	CacheMap<animation_id_t, AnimationHandle> _animations;
	
	static MetaData& getMetaData();
};

} }
