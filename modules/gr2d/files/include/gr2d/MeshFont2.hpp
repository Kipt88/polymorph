#pragma once

#include <gr/MeshProvider.hpp>
#include <gr/types.hpp>
#include <map>
#include <string>

namespace pm { namespace gr {

class MeshFont2
{
public:
	MeshFont2(const std::map<char, MeshHandle>& charMapping);
	MeshFont2(std::map<char, MeshHandle>&& charMapping);

	std::vector<std::vector<MeshHandle>> getLines(const std::string& text) const;

	real_t fontHeight() const;

private:
	std::map<char, MeshHandle> _charMapping;
	real_t _fontHeight;
};

} }
