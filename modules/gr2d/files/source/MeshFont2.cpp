#include "gr2d/MeshFont2.hpp"

#include <Debug.hpp>
#include <algorithm>
#include <numeric>

namespace pm { namespace gr {

namespace {

const char* LOG_TAG = "MeshFont2";

real_t getHighestChar(const std::map<char, MeshHandle>& charMapping)
{
	real_t hight = 0.0f;

	for (const auto& charPair : charMapping)
		hight = std::max(hight, charPair.second->bounds().height());

	return hight;
}

auto clipText(const std::string& text)
{
	std::vector<std::pair<std::string::const_iterator, std::string::const_iterator>> lines;

	for (auto lineStart = text.begin(), lineEnd = std::find(text.begin(), text.end(), L'\n'); 

	     lineStart != text.end(); 

		 lineStart = (lineEnd == text.end() ? text.end() : lineEnd + 1), 
		 lineEnd = (lineEnd == text.end() ? text.end() : std::find(lineEnd + 1, text.end(), L'\n')))
	{
		lines.push_back(std::make_pair(lineStart, lineEnd));
	}

	return lines;
}

real_t getLineWidth(const std::map<char, MeshHandle>& charMapping,
					std::string::const_iterator begin,
					std::string::const_iterator end)
{
	return std::accumulate(
		begin,
		end,
		0.0_r,
		[&](real_t w, char c)
		{
			auto it = charMapping.find(c);

			if (it == charMapping.end())
				return w;
			else
				return w + it->second->bounds().width();
		}
	);
}

}

MeshFont2::MeshFont2(const std::map<char, MeshHandle>& charMapping)
: _charMapping(charMapping),
  _fontHeight(getHighestChar(_charMapping))
{
}

MeshFont2::MeshFont2(std::map<char, MeshHandle>&& charMapping)
: _charMapping(std::move(charMapping)),
  _fontHeight(getHighestChar(_charMapping))
{
}

std::vector<std::vector<MeshHandle>> MeshFont2::getLines(const std::string& text) const
{
	std::vector<std::vector<MeshHandle>> rVec;

	const auto& lines = clipText(text);

	for (const auto& line : lines)
	{
		std::vector<MeshHandle> meshLine;
		meshLine.reserve(line.second - line.first);

		for (auto it = line.first; it != line.second; ++it)
		{
			auto meshIt = _charMapping.find(*it);

			if (meshIt != _charMapping.end())
				meshLine.push_back(meshIt->second);
			else
				ERROR_OUT(LOG_TAG, "Missing char %lc", *it);
		}

		rVec.push_back(std::move(meshLine));
	}

	return rVec;
}

real_t MeshFont2::fontHeight() const
{
	return _fontHeight;
}

} }