set(
	dependent_modules
	"gr"
)

set(
	source_files
	"source/MeshFont2.cpp"
)

set(
	include_files
	"include/gr2d/MeshFont2.hpp"
)

require_modules(${dependent_modules})

source_group("Sources" FILES ${source_files})
source_group("Headers" FILES ${include_files})

add_library("gr2d" STATIC ${source_files} ${include_files})

target_include_directories("gr2d" PUBLIC "include")
target_link_libraries("gr2d" PUBLIC ${dependent_modules})
