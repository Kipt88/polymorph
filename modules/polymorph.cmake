# Note: CMake variabes are only defined inside a "project" so this must be included after.

if(WIN32)
	if(CMAKE_VS_PLATFORM_NAME STREQUAL "x64")
		set(PLATFORM "win_x64")
		set(CMAKE_LIBRARY_ARCHITECTURE "x64")
	else()
		set(PLATFORM "win_x86")
	endif()
elseif(APPLE)
	set(PLATFORM "macos")
	set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1z -x objective-c++")
else()
	message(FATAL_ERROR "Unknown platform.")
endif()

message("Platform: ${PLATFORM}")

get_filename_component(MODULES_ROOT "${CMAKE_CURRENT_LIST_DIR}" ABSOLUTE)
get_filename_component(EXTERNALS_ROOT "${CMAKE_CURRENT_LIST_DIR}/../external" ABSOLUTE)
get_filename_component(TOOLS_ROOT "${CMAKE_CURRENT_LIST_DIR}/../tools" ABSOLUTE)
get_filename_component(PM_CMAKE_MODULES_ROOT "${CMAKE_CURRENT_LIST_DIR}/../cmake_modules" ABSOLUTE)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PM_CMAKE_MODULES_ROOT}")

message("MODULES_ROOT = ${MODULES_ROOT}")
message("EXTERNALS_ROOT = ${EXTERNALS_ROOT}")

if(MSVC)
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT")
	set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} /MT")
	set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MTd")
	
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP /GR-") # No RTTI 'cos who needs it...
endif()

include("${CMAKE_CURRENT_LIST_DIR}/options.cmake")

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

function(assert cnd)
	if(NOT ${cnd})
		message(FATAL_ERROR "Assert fail, '${cnd}' evaluates to false.")
	endif()
endfunction()

function(require_modules)
	foreach(module ${ARGV})
		if(NOT TARGET ${module})
			add_subdirectory(
				"${MODULES_ROOT}/${module}/files" 
				"${MODULES_ROOT}/${module}/build/${PLATFORM}"
			)
		endif()
	endforeach()
endfunction()

function(require_tools)
	foreach(tool ${ARGV})
		if(NOT TARGET ${tool})
			add_subdirectory(
				"${TOOLS_ROOT}/${tool}/files"
				"${TOOLS_ROOT}/${tool}/build/${PLATFORM}"
			)
		endif()
	endforeach()
endfunction()

function(_parse_arguments_impl out_prefix keys params)
	set(current_key)
	
	foreach(p ${params})
		if(${p} IN_LIST keys)
			set(current_key ${p})
			set(${out_prefix}_HAS_${current_key} YES PARENT_SCOPE)
		else()
			if(current_key)
				list(APPEND ${out_prefix}_${current_key} ${p})
				set(${out_prefix}_${current_key} ${${out_prefix}_${current_key}} PARENT_SCOPE)
			else()
				list(APPEND ${out_prefix} ${p})
				set(${out_prefix} ${${out_prefix}} PARENT_SCOPE)
			endif()
		endif()
	endforeach()
endfunction()

function(parse_arguments)
	_parse_arguments_impl("P" "PARAM_KEYS;PARAM_OUTPUT_PREFIX;PARAMS" "${ARGV}")
	
	if(NOT P_PARAM_KEYS)
		message(FATAL_ERROR "Missing PARAM_KEYS")
	endif()
	
	if(NOT P_PARAM_OUTPUT_PREFIX)
		message(FATAL_ERROR "Missing PARAM_OUTPUT_PREFIX")
	endif()
	
	if(NOT P_PARAMS)
		message(FATAL_ERROR "Missing PARAMS")
	endif()
	
	_parse_arguments_impl(${P_PARAM_OUTPUT_PREFIX} "${P_PARAM_KEYS}" "${P_PARAMS}")
	
	foreach(key ${P_PARAM_KEYS})
		set(${P_PARAM_OUTPUT_PREFIX}_${key} ${${P_PARAM_OUTPUT_PREFIX}_${key}} PARENT_SCOPE)
		set(${P_PARAM_OUTPUT_PREFIX}_HAS_${key} ${${P_PARAM_OUTPUT_PREFIX}_HAS_${key}} PARENT_SCOPE)
	endforeach()
	
	set(${P_PARAM_OUTPUT_PREFIX} ${${P_PARAM_OUTPUT_PREFIX}} PARENT_SCOPE)
endfunction()
