#include <gtest/gtest.h>

#include <os/App.hpp>
#include <os/Window.hpp>
#include <gr/Surface.hpp>
#include <gr/BufferProvider.hpp>
#include <gr/VertexList.hpp>
#include <gr/VertexFormat.hpp>
#include <gr/Mesh.hpp>
#include <concurrency/Thread.hpp>
#include <Scoped.hpp>

#include <vector>
#include <array>

using namespace pm;

namespace {

struct TriFace
{
	std::array<gr::index_t, 3> indices;
	gr::MaterialHandle material;
	gr::Primitive primitive;
};

struct QuadFace
{
	std::array<gr::index_t, 4> indices;
	gr::MaterialHandle material;
	gr::Primitive primitive;
};


void draw(const gr::Mesh& mesh)
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glLineWidth(2.0f);

	for (const auto& face : mesh.faces())
		mesh.vertices().draw(face.second.primitive, face.second.buffer);
}

void gfxRun(os::AppSystem& appSystem, os::App& app)
{
	auto scope = at_exit([&] { appSystem.quit(); });
	
	gr::VertexList<gr::VertexFormat::V2> quadList1;

	quadList1.push_back({ { -0.75f, -0.75f } });
	quadList1.push_back({ {  0.10f, -0.75f } });
	quadList1.push_back({ {  0.10f,  0.10f } });
	quadList1.push_back({ { -0.75f,  0.10f } });

	gr::VertexList<gr::VertexFormat::V2> quadList2;

	quadList2.push_back({ { -0.10f, -0.10f } });
	quadList2.push_back({ {  0.75f, -0.10f } });
	quadList2.push_back({ {  0.75f,  0.75f } });
	quadList2.push_back({ { -0.10f,  0.75f } });

	gr::VertexList<gr::VertexFormat::V2_C3> triangleList;

	triangleList.push_back({ { -0.25f, -0.25f }, { 0.25f, 0.25f, 0.0f } });
	triangleList.push_back({ {  0.25f, -0.25f }, { 0.75f, 0.25f, 0.0f } });
	triangleList.push_back({ {  0.00f,  0.50f }, { 0.50f, 1.00f, 0.0f } });

	std::vector<QuadFace> faces;
	faces.push_back({ { {0, 1, 2, 3} }, nullptr, gr::Primitive::QUADS });

	std::vector<TriFace> triFaces;
	triFaces.push_back({ { {0, 1, 2} }, nullptr, gr::Primitive::TRIANGLES });
	
	{
		gr::Surface surface(app.window());

		bool quitFlag = false;
		pm::concurrency::Mutex m;

		{
			gr::BufferProvider manager;

			std::vector<gr::Mesh> meshes;

			meshes.push_back(
				gr::Mesh(
					quadList1, 
					faces,
					gr::BufferUsage::STATIC,
					manager
				)
			);
			meshes.push_back(
				gr::Mesh(
					quadList2,
					faces,
					gr::BufferUsage::STATIC,
					manager
				)
			);
			meshes.push_back(
				gr::Mesh(
					triangleList,
					triFaces,
					gr::BufferUsage::STATIC,
					manager
				)
			);

			auto keyUpScope = appSystem.addWindowEventListener<os::window_events::KeyUp>(
				[&](const os::window_events::KeyUp&) mutable
				{
					pm::concurrency::MutexLockGuard lock(m);

					if (meshes.empty())
						quitFlag = true;
					else
						meshes.pop_back();

					DEBUG_OUT("KEY_EVENT", "Left: %i", static_cast<int>(meshes.size()));
				}
			);

			while (!quitFlag)
			{
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				
				{
					pm::concurrency::MutexLockGuard mlock(m);
					
					for (const auto& mesh : meshes)
						draw(mesh);
				}
				
				surface.flipBuffers();
				
				pm::concurrency::Thread::sleep(33ms);
			}
		}
	}
}

}

TEST(Mesh, normal)
{
	os::AppSystem appSystem("Test mesh window - press any key", 800, 600);
	
	{
		pm::concurrency::Thread gfxThread;
		
		appSystem.pushAction(
			[&](os::App& app)
			{
				gfxThread = [&] { gfxRun(appSystem, app); };
			}
		);
		
		appSystem.enter();
	}
}
