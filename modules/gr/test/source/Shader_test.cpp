#include <gtest/gtest.h>

#include <os/Window.hpp>
#include <gr/Surface.hpp>
#include <gr/Shader.hpp>
#include <gr/ShaderSuite.hpp>
#include <Debug.hpp>

#include <vector>
#include <string>

using namespace pm;

TEST(Shader, compilation_error)
{
	ASSERT_THROW(
		{
			os::InputSystem inputSystem(16ms, "testCompilationError");
			gr::Surface surface(inputSystem);
			
			std::string badShaderSource = " foobar; ";

			{
				auto lock = surface.activate();

				gr::Shader shader(
					badShaderSource, 
					gr::ShaderType::VERTEX_SHADER
				);
			}
		},
		gr::ShaderCompilationException
	);
}

TEST(Shader, link_error)
{
	ASSERT_THROW(
		{
			os::InputSystem inputSystem(16ms, "testLinkingError");
			gr::Surface surface(inputSystem);

			{
				auto lock = surface.activate();

				std::vector<gr::ShaderHandle> shaders;

				shaders.push_back(
					gr::ShaderHandle(
						new gr::Shader(
							R"(
								vec4 foobar(void);
						
								void main()
								{
									gl_Position = foobar();
								}
							)",
							gr::ShaderType::VERTEX_SHADER
						)
					)
				);

				shaders.push_back(
					gr::ShaderHandle(
						new gr::Shader(
							R"(
								void main()
								{
									gl_FragColor = vec4(0.5, 0.5, 0.5, 1.0);
								}
							)",
							gr::ShaderType::PIXEL_SHADER
						)
					)
				);

				gr::ShaderSuite suite(shaders.begin(), shaders.end());
			}
		},
		gr::ShaderLinkingException
	);
}

TEST(Shader, normal)
{
	os::InputSystem inputSystem(16ms, "testNormalShaders");
	gr::Surface surface(inputSystem);

	{
		auto lock = surface.activate();

		std::vector<gr::ShaderHandle> shaders;
		
		shaders.push_back(
			gr::ShaderHandle(
				new gr::Shader(
					R"(
						void main()
						{
							gl_Position = ftransform();
						}
					)",
					gr::ShaderType::VERTEX_SHADER
				)
			)
		);

		shaders.push_back(
			gr::ShaderHandle(
				new gr::Shader(
					R"(
						void main()
						{
							gl_FragColor = vec4(0.5, 0.5, 0.5, 1.0);
						}
					)",
					gr::ShaderType::PIXEL_SHADER
				)
			)
		);

		gr::ShaderSuite suite(shaders.begin(), shaders.end());
	}

	{
		auto lock = surface.activate();

		std::vector<gr::ShaderHandle> shaders;

		shaders.push_back(
			gr::ShaderHandle(
				new gr::Shader(
					R"(
						vec4 foobar();
                
						void main()
						{
							gl_Position = foobar();
						}
					)",
					gr::ShaderType::VERTEX_SHADER
				)
			)
		);

		shaders.push_back(
			gr::ShaderHandle(
				new gr::Shader(
					R"(
						vec4 foobar()
						{
							return ftransform();
						}
					)",
					gr::ShaderType::VERTEX_SHADER
				)
			)
		);

		shaders.push_back(
			gr::ShaderHandle(
				new gr::Shader(
					R"(
						void main()
						{
							gl_FragColor = vec4(0.5, 0.5, 0.5, 1.0);
						}
					)",
					gr::ShaderType::PIXEL_SHADER
				)
			)
		);

		gr::ShaderSuite shader(shaders.begin(), shaders.end());
	}
}

TEST(Shader, uniform)
{
	os::InputSystem inputSystem(16ms, "testUniform");
	gr::Surface surface(inputSystem);

	{
		auto lock = surface.activate();

		std::vector<gr::ShaderHandle> shaders;

		shaders.push_back(
			gr::ShaderHandle(
				new gr::Shader(
					R"(
						uniform float testFloat;
				
						void main()
						{
							gl_Position = ftransform() * testFloat;
						}
					)",
					gr::ShaderType::VERTEX_SHADER
				)
			)
		);

		shaders.push_back(
			gr::ShaderHandle(
				new gr::Shader(
					R"(
						void main()
						{
							gl_FragColor = vec4(0.5, 0.5, 0.5, 1.0);
						}
					)",
					gr::ShaderType::PIXEL_SHADER
				)
			)
		);

		gr::ShaderSuite shader(shaders.begin(), shaders.end());

		ASSERT_NO_THROW(
			{
				shader.bind();
				auto loc = shader.getLocation("testFloat");
				shader.set(loc, 3.0f);
				shader.unbind();
			}
		);

		ASSERT_THROW(
			{
				shader.bind();
				shader.getLocation("foobar");
				shader.unbind();
			},
			gr::NoSuchUniformException
		);
	}

}

pm::gr::ShaderSuite createTestShader(pm::gr::Surface& surface)
{
	auto lock = surface.activate();

	std::vector<pm::gr::ShaderHandle> shaders;

	shaders.push_back(
		pm::gr::ShaderHandle(
			new pm::gr::Shader(
				R"(
					uniform float testFloat;
				
					void main()
					{
						vec4 pos = ftransform();
						gl_Position = pos + vec4(testFloat, 0.0f, 0.0f, 1.0f);
					}
				)",
				pm::gr::ShaderType::VERTEX_SHADER
			)
		)
	);

	shaders.push_back(
		pm::gr::ShaderHandle(
			new pm::gr::Shader(
				R"(
					void main()
					{
						gl_FragColor = vec4(0.7, 0.5, 0.5, 1.0);
					}
				)",
				pm::gr::ShaderType::PIXEL_SHADER
			)
		)
	);

	return pm::gr::ShaderSuite(shaders.begin(), shaders.end());
}

TEST(Shader, draw)
{
	os::InputSystem inputSystem(16ms, "Shader - test draw");
	gr::Surface surface(inputSystem);

	bool quitTest = false;

	auto scope = inputSystem.addWindowEventListener<pm::os::window_events::Quit>(
		[&]() mutable
		{
			quitTest = true;
		}
	);

	auto&& shader = createTestShader(surface);

	while (!quitTest)
	{
		{
			auto lock = surface.activate();

			shader.bind();

			shader.set(shader.getLocation("testFloat"), -1.0f);

			glBegin(GL_QUADS);
			glVertex2f(-0.5f, -0.5f);
			glVertex2f( 0.5f, -0.5f);
			glVertex2f( 0.5f,  0.5f);
			glVertex2f(-0.5f,  0.5f);
			glEnd();

			surface.flipBuffers();

			shader.unbind();
		}
	}
}
