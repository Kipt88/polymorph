#include <gtest/gtest.h>

#include <os/Window.hpp>
#include <gr/Surface.hpp>
#include <concurrency/Thread.hpp>

using namespace pm;

TEST(Surface, blank)
{
	os::InputSystem inputSystem(16ms, "Test surface window - should show blank window");

	{
		gr::Surface surface(inputSystem);

		bool quitFlag = false;

		auto quitScope = inputSystem.addWindowEventListener<os::window_events::Quit>(
			[&]() mutable
			{
				quitFlag = true;
			}
		);

		while (!quitFlag)
		{
			surface.flipBuffers();

			pm::concurrency::Thread::sleep(33ms);
		}
	}
}