#pragma once

#include "gr/opengl.hpp"
#include "gr/types.hpp"
#include "gr/VertexFormat.hpp"
#include "gr/IndexBuffer.hpp"
#include "gr/GraphicsException.hpp"

#include <Exception.hpp>
#include <NonCopyable.hpp>

#include <memory>
#include <utility>


namespace pm { namespace gr {

PM_MAKE_EXCEPTION_CLASS(VertexBufferCreationException, GraphicsException);

/**
 * Vertex buffer.
 *
 * Handles a GPU vertex buffer.
 * All mutators require surface to be locked before calling.
 */
class VertexBuffer : NonCopyable
{
public:
	VertexBuffer(const VertexFormatData& format,
	             BufferUsage usage,
	             std::size_t size,
	             const void* data = nullptr);

	VertexBuffer(VertexBuffer&& vertexBuffer);

	~VertexBuffer();

	VertexBuffer& operator=(VertexBuffer&& other);

	void write(const void* vertices, std::size_t index, std::size_t size = 1);

	void draw(Primitive primitive) const;

	void draw(Primitive primitive,
	          std::size_t start,
	          std::size_t size) const;

	void draw(Primitive primitive,
	          const IndexBuffer& indices) const;

	void draw(Primitive primitive,
			  const IndexBuffer& indices,
			  std::size_t offset,
			  std::size_t size) const;

	std::size_t  size()  const;
	std::size_t  capacity() const;
	const VertexFormatData& vertexFormatData() const;
	BufferUsage  bufferUsage()  const;

	void resize(std::size_t size);

private:
	GLuint _bufferID;

	std::size_t _size;
	std::size_t _capacity;

	BufferUsage _usage;

	VertexFormatData _vertexFormatData;
};


typedef std::shared_ptr<VertexBuffer> VertexBufferHandle;

} }
