#pragma once

#include "gr/opengl.hpp"

#include "gr/types.hpp"

#include <Exception.hpp>
#include <NonCopyable.hpp>


#include <vector>
#include <memory>
#include <string>

namespace pm { namespace gr {

PM_MAKE_EXCEPTION_CLASS(ShaderException, Exception);
PM_MAKE_EXCEPTION_CLASS(NoSuchUniformException, ShaderException);
PM_MAKE_EXCEPTION_CLASS(ShaderCompilationException, ShaderException);
PM_MAKE_EXCEPTION_CLASS(ShaderLinkingException, ShaderException);

enum class ShaderType : GLenum
{
	VERTEX_SHADER = GL_VERTEX_SHADER,
	PIXEL_SHADER = GL_FRAGMENT_SHADER
};

class Shader : NonCopyable
{
public:
	Shader(const std::string& source, ShaderType type);

	Shader(Shader&& other);

	~Shader();

	ShaderType type() const;
	GLuint glHandle() const;

private:
	GLuint _shader;
	ShaderType _type;
};

typedef std::shared_ptr<Shader> ShaderHandle;

} }
