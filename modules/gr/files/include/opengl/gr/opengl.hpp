#pragma once

// OpenGL includes here

#ifdef _WIN32
#	include <windows.h>
#	include <gl/glew.h>
#elif __APPLE__
// TODO: Get rid of gl.h, some legacy usage is around...
#	include <OpenGL/gl3.h>
#	include <OpenGL/gl.h>
#endif
