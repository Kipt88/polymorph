#pragma once

#include "gr/opengl.hpp"

#include <math/Projection.hpp>
#include <math/Transform2.hpp>
#include <math/Transform3.hpp>
#include <math/Box.hpp>

#include <array>

namespace pm { namespace gr {

typedef GLfloat real_t;
typedef GLuint  index_t;
typedef GLenum 	enum_t;

constexpr gr::real_t operator "" _r(long double d)
{
	return static_cast<gr::real_t>(d);
}

// OpenGL expects column-major matrices
typedef math::Projection<> Projection;
typedef math::SimilarityTransform2<real_t> Transform2;
typedef math::SimilarityTransform3<real_t> Transform3;

typedef math::Quaternion<real_t> Quaternion_r;

typedef math::AxisAlignedBox2<real_t>   AABox2;

typedef math::Vector<real_t, 2> 	Vector2_r;
typedef math::Vector<real_t, 3> 	Vector3_r;
typedef math::Vector<real_t, 4> 	Vector4_r;
typedef math::Matrix<real_t, 3, 3>	Matrix3x3_r;
typedef math::Matrix<real_t, 4, 4>	Matrix4x4_r;

const enum_t REAL_TYPE 	= GL_FLOAT;
const enum_t INDEX_TYPE = GL_UNSIGNED_INT;

enum class BufferUsage : enum_t
{
	STATIC 	= GL_STATIC_DRAW,
	DYNAMIC = GL_DYNAMIC_DRAW,
	STREAM 	= GL_STREAM_DRAW
};

enum class Primitive : enum_t
{
	POINTS          = GL_POINTS,
	LINES 			= GL_LINES,
	LINE_STRIP      = GL_LINE_STRIP,
	LINE_LOOP       = GL_LINE_LOOP,
	TRIANGLES 		= GL_TRIANGLES,
	TRIANGLE_STRIP 	= GL_TRIANGLE_STRIP,
	QUADS 			= GL_QUADS,
};

enum class BufferFlag : enum_t
{
	NONE 		= 0,
	COLOR 		= GL_COLOR_BUFFER_BIT,
	DEPTH		= GL_DEPTH_BUFFER_BIT,
	ALL			= COLOR | DEPTH,
};

enum class TypeTag
{
	FLOAT = GL_FLOAT,
	FLOAT_2 = GL_FLOAT_VEC2,
	FLOAT_3 = GL_FLOAT_VEC3,
	FLOAT_4 = GL_FLOAT_VEC4,
	FLOAT_2X2 = GL_FLOAT_MAT2,
	FLOAT_3X3 = GL_FLOAT_MAT3,
	FLOAT_4X4 = GL_FLOAT_MAT4,

	INT = GL_INT,
	INT_2 = GL_INT_VEC2,
	INT_3 = GL_INT_VEC3,
	INT_4 = GL_INT_VEC4,

	UNSIGNED_INT = GL_UNSIGNED_INT
};

}

#define LOG_GL_CALLS			0
#define CHECK_GL_OUT_OF_MEMORY	1
#define ASSERT_NO_GL_ERRORS		1

// Wrapper to OpenGL.
namespace gl {

// Direct rendering
void vertex(gr::real_t x, gr::real_t y);
void vertex(gr::real_t x, gr::real_t y, gr::real_t z);
void vertex(gr::real_t x, gr::real_t y, gr::real_t z);
void vertex(gr::real_t x, gr::real_t y, gr::real_t z, gr::real_t w);

void begin(gr::Primitive primitive);
void end();
//

// Buffer handling
void genBuffers(GLuint count, GLuint* ids);
void bindBuffer(GLenum type, GLuint id);
void bufferData(GLenum type, GLsizei size, const GLvoid* data, gr::BufferUsage usage);
void bufferSubData(GLenum type, GLuint offset, GLsizei size, const GLvoid* data);
void getBufferSubData(GLenum type, GLintptr offset, GLsizeiptr size, GLvoid* data);
void deleteBuffers(GLuint count, const GLuint* ids);
//

// Array handling
void vertexPointer(GLuint dim, GLenum type, GLuint offset, const void* data);
void colorPointer(GLuint dim, GLenum type, GLuint offset, const void* data);
void texCoordPointer(GLuint dim, GLenum type, GLuint offset, const void* data);
void normalPointer(GLenum type, GLuint offset, const void* data);
//

// State handling
void enableClientState(GLenum type);
void disableClientState(GLenum type);
void enable(GLenum type);
void disable(GLenum type);
//

// Matrix handling
void matrixMode(GLenum type);
void loadIdentity();
void loadMatrix(const float* mat);
void multMatrix(const float* mat);
//

// Framebuffer handling
void clear(GLbitfield mask);
//

} }

using pm::gr::operator "" _r;

#include "gr/types.inl"
