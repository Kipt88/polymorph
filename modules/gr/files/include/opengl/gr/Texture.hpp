#pragma once

#include "gr/opengl.hpp"
#include "gr/Image.hpp"

#include <Id.hpp>
#include <NonCopyable.hpp>

#include <memory>

namespace pm { namespace gr {

class Texture : NonCopyable
{
public:
	class BindLock : ::NonCopyable
	{
	public:
		BindLock();
		BindLock(const Texture& texture);
		BindLock(BindLock&& other);
		~BindLock();

		BindLock& operator=(BindLock&& other);

	private:
		const Texture* _texture;
	};

	struct Params
	{
		enum Wrap : GLint
		{
			CLAMP			= GL_CLAMP,
			CLAMP_TO_EDGE	= GL_CLAMP_TO_EDGE,
			REPEAT			= GL_REPEAT,
		} sWrap, tWrap;

		enum Zoom : GLint
		{
			NEAREST	= GL_NEAREST,
			LINEAR	= GL_LINEAR,
		} minZoom, magZoom;

		Params(Wrap _sWrap = CLAMP,
			   Wrap _tWrap = CLAMP,
			   Zoom _minZoom = LINEAR,
			   Zoom _magZoom = LINEAR);
	};

public:
	Texture(const Image& source, const Params& texParams, id_t resourceId = 0);
	Texture(Texture&& other);
	~Texture();

	BindLock bindLock() const;

	void bind() const;
	void unbind() const;

	id_t resourceId() const;

private:
	GLuint _texID;

	id_t _resourceId;
};

typedef std::shared_ptr<const Texture> TextureHandle;
typedef std::weak_ptr<const Texture> WeakTextureHandle;

} }
