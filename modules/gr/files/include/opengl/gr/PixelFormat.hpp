#pragma once

#include "gr/opengl.hpp"

namespace pm { namespace gr {

enum class PixelFormat : GLint
{
	R8G8B8		= GL_RGB,
	R8G8B8A8	= GL_RGBA,
};

struct PixelFormatData
{
	short pixelSize;
};

const PixelFormatData& getPixelFormatData(PixelFormat format);

// PixelFormat getFormat(char rbits, char gbits, char bbits, char abits);

} }