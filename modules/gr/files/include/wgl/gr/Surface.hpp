#pragma once

#include "gr/types.hpp"

#include <os/Window.hpp>

#include <math/Box.hpp>
#include <concurrency/Mutex.hpp>

#include <NonCopyable.hpp>

#include <memory>
#include <windows.h>
#include <wingdi.h>

namespace pm { namespace gr {

namespace detail {

/**
 * Custom deleter type to for HDC__ pointers.
 */
class dc_deleter
{
public:
	explicit dc_deleter(HWND window = nullptr);
	void operator()(HDC dc);

private:
	HWND _window;
};

/**
 * Custom deleter type to allow HGLRC__ pointers.
 */
struct rc_deleter
{
    void operator()(HGLRC rc);
};

}

typedef HWND WindowHandle;

/**
 *
 * TODO: Append GetLastError() information to thrown exceptions.
 */
class Surface : NonCopyable
{
public:
	explicit Surface(os::Window& window);

	void setVSync(bool enable = true);

	void flipBuffers();

	void setViewport(const math::AxisAlignedBox2<int>& box);
	math::AxisAlignedBox2<int> getViewport() const;

	Vector2_r normalizedToWindow(const Vector2_r& v) const;
	Vector3_r normalizedToWindow(const Vector3_r& v) const;

	Vector2_r windowToNormalized(const Vector2_r& v) const;
	Vector3_r windowToNormalized(const Vector3_r& v) const;

private:
	HWND _win;
	std::unique_ptr<HDC__,   detail::dc_deleter> _deviceHandle;
	std::unique_ptr<HGLRC__, detail::rc_deleter> _glHandle;

	struct WGLExtensions
	{
		typedef BOOL (GLAPIENTRY * PFNGLSWAPINTERVALPROC) (int interval);

		WGLExtensions();

		bool loaded;

		PFNGLSWAPINTERVALPROC wglSwapInterval;
	} _wglExtensions;
};

} }
