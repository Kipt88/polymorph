#pragma once

#include <io/IOStream.hpp>

#include <gr/Image.hpp>

namespace pm { namespace gr {

namespace ImageReader_PNG
{
	gr::Image read(io::InputStream& input);
};

} }
