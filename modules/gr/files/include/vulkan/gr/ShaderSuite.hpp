#pragma once

#include "gr/Shader.hpp"
#include "gr/types.hpp"

#include <NonCopyable.hpp>

#include <vector>
#include <string>

namespace pm { namespace gr {

struct ShaderVariableData
{
	std::string name;
	TypeTag type;
	int blockIndex;
	int offset;
};

struct ShaderBlockData
{
	std::string name;
};

struct ShaderMetaData
{
	std::vector<ShaderBlockData> blocks;
	std::vector<ShaderVariableData> variables;
};

class ShaderSuite : NonCopyable
{
public:
	template <typename InputIt>
	ShaderSuite(InputIt shadersBegin, InputIt shadersEnd);

	ShaderSuite(ShaderSuite&& other);

	~ShaderSuite();

	// TODO
	typedef uint32_t location_t;

	location_t getLocation(const std::string& name) const;

	void set(location_t loc, real_t f) const;
	void set(location_t loc, const Vector2_r& v) const;
	void set(location_t loc, const Vector3_r& v) const;
	void set(location_t loc, const Matrix4x4_r& m) const;
	void set(location_t loc, int i) const;

	ShaderMetaData getMetaData() const;

	void bind() const;
	void unbind() const;

private:
	// TODO
};

typedef std::shared_ptr<ShaderSuite> ShaderSuiteHandle;

} }
