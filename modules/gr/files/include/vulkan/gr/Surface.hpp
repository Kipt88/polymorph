#pragma once

#include "gr/types.hpp"

#include <math/Box.hpp>
#include <concurrency/Mutex.hpp>

#include <os/InputSystem.hpp>

#include <NonCopyable.hpp>

#include <memory>

namespace pm { namespace gr {

/**
 *
 * TODO: Append GetLastError() information to thrown exceptions.
 */
class Surface : Unique
{
public:
	class ActivationLock : ::NonCopyable
	{
	public:
		ActivationLock();
		ActivationLock(const Surface& surface);
		ActivationLock(ActivationLock&& other);
		~ActivationLock();

		ActivationLock& operator=(ActivationLock&& other);

	private:
		const Surface* _surface;
	};

public:
	explicit Surface(os::InputSystem& input);

	// Surface(Surface&& surface);

	/**
	 * @return true if the underlying rendering context is active in the calling thread.
	 */
	bool isActive() const;

	ActivationLock activate() const;

	void setVSync(bool enable = true);

	void flipBuffers();

	/**
	 * Gets the drawable area in window coordinates, usually from (0, 0) to window dimension.
	 */
	math::AxisAlignedBox2<int> getScreenRect() const;

	Vector2_r normalizedToWindow(const Vector2_r& v) const;
	Vector3_r normalizedToWindow(const Vector3_r& v) const;

	Vector2_r windowToNormalized(const Vector2_r& v) const;
	Vector3_r windowToNormalized(const Vector3_r& v) const;

private:
	void setActive(bool activate = true) const;

	// TODO:
};

} }
