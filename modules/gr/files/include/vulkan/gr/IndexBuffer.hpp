#pragma once

#include "gr/vulkan.hpp"
#include "gr/types.hpp"
#include "gr/GraphicsException.hpp"

#include <NonCopyable.hpp>
#include <Exception.hpp>

#include <memory>

namespace pm { namespace gr {

PM_MAKE_EXCEPTION_CLASS(IndexBufferCreationException, GraphicsException);

class IndexBuffer : NonCopyable
{
public:
	friend class VertexBuffer;

	IndexBuffer(BufferUsage usage,
	            std::size_t size,
	            const index_t* data=nullptr);

	IndexBuffer(IndexBuffer&& indexBuffer);

	~IndexBuffer();

	IndexBuffer& operator=(IndexBuffer&& other);

	void write(std::size_t offset, std::size_t size, const index_t* indices);

	std::size_t size() const;
	std::size_t capacity() const;
	BufferUsage bufferUsage() const;

	void resize(std::size_t size);

private:
	// TODO
};

typedef std::shared_ptr<IndexBuffer> IndexBufferHandle;

} }
