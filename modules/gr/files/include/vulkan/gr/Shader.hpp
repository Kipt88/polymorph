#pragma once

#include "gr/vulkan.hpp"

#include "gr/types.hpp"

#include <Exception.hpp>
#include <NonCopyable.hpp>


#include <vector>
#include <memory>
#include <string>

namespace pm { namespace gr {

PM_MAKE_EXCEPTION_CLASS(ShaderException, Exception);
PM_MAKE_EXCEPTION_CLASS(NoSuchUniformException, ShaderException);
PM_MAKE_EXCEPTION_CLASS(ShaderCompilationException, ShaderException);
PM_MAKE_EXCEPTION_CLASS(ShaderLinkingException, ShaderException);

enum class ShaderType //: GLenum
{
	// TODO
	VERTEX_SHADER = 0,
	PIXEL_SHADER = 1
};

class Shader : NonCopyable
{
public:
	Shader(const std::string& source, ShaderType type);

	Shader(Shader&& other);

	~Shader();

	ShaderType type() const;
	uint32_t glHandle() const;

private:
	// TODO
};

typedef std::shared_ptr<Shader> ShaderHandle;

} }
