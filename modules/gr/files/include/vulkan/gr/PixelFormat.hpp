#pragma once

#include "gr/vulkan.hpp"

namespace pm { namespace gr {

enum class PixelFormat //: uint32_t
{
	// TODO
	R8G8B8		= 0,
	R8G8B8A8	= 1,
};

struct PixelFormatData
{
	short pixelSize;
};

const PixelFormatData& getPixelFormatData(PixelFormat format);

// PixelFormat getFormat(char rbits, char gbits, char bbits, char abits);

} }
