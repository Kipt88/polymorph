#pragma once

#include "gr/vulkan.hpp"
#include "gr/Image.hpp"

#include <Id.hpp>
#include <NonCopyable.hpp>

#include <memory>

namespace pm { namespace gr {

class Texture : NonCopyable
{
public:
	class BindLock : ::NonCopyable
	{
	public:
		BindLock();
		BindLock(const Texture& texture);
		BindLock(BindLock&& other);
		~BindLock();

		BindLock& operator=(BindLock&& other);

	private:
		const Texture* _texture;
	};

	struct Params
	{
		enum Wrap //: GLint
		{
			// TODO
			CLAMP			= 0,
			CLAMP_TO_EDGE	= 1,
			REPEAT			= 2,
		} sWrap, tWrap;

		enum Zoom //: GLint
		{
			NEAREST	= 0,
			LINEAR	= 1,
		} minZoom, magZoom;

		Params(Wrap _sWrap = CLAMP,
			   Wrap _tWrap = CLAMP,
			   Zoom _minZoom = LINEAR,
			   Zoom _magZoom = LINEAR);
	};

public:
	Texture(const Image& source, const Params& texParams, id_t resourceId = 0);
	Texture(Texture&& other);
	~Texture();

	BindLock bindLock() const;

	void bind() const;
	void unbind() const;

	id_t resourceId() const;

private:
	// TODO
};

typedef std::shared_ptr<const Texture> TextureHandle;

} }
