#pragma once

#include "gr/Surface.hpp"
#include "gr/Mesh.hpp"
#include "gr/types.hpp"

namespace pm { namespace gr {

namespace PlainRenderer {

void render(
	Surface& surface,
	const Matrix4x4_r& transform,
	MeshHandle mesh
);

}

} }