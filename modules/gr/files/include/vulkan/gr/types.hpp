#pragma once

#include "gr/vulkan.hpp"

#include <math/Projection.hpp>
#include <math/Transform2.hpp>
#include <math/Transform3.hpp>
#include <math/Box.hpp>

#include <array>

namespace pm { namespace gr {

// TODO
typedef float    real_t;
typedef uint32_t index_t;
typedef uint32_t enum_t;

constexpr gr::real_t operator "" _r(long double d)
{
	return static_cast<gr::real_t>(d);
}

// TODO
// Vulkan expects row/column-major matrices?
typedef math::Projection<> Projection;
typedef math::Transform2<real_t> Transform2;
typedef math::Transform3<real_t> Transform3;

typedef math::Quaternion<real_t> Quaternion_r;

typedef math::AxisAlignedBox2<real_t>   AABox2;

typedef math::Vector<real_t, 2> 	Vector2_r;
typedef math::Vector<real_t, 3> 	Vector3_r;
typedef math::Vector<real_t, 4> 	Vector4_r;
typedef math::Matrix<real_t, 3, 3>	Matrix3x3_r;
typedef math::Matrix<real_t, 4, 4>	Matrix4x4_r;

// TODO
const enum_t REAL_TYPE 	= 0;
const enum_t INDEX_TYPE = 1;

enum class BufferUsage : enum_t
{
	// TODO
	STATIC 	= 0,
	DYNAMIC = 1,
	STREAM 	= 2,
	COPY_READ = 3,
};

enum class Primitive : enum_t
{
	// TODO
	POINTS          = 0,
	LINES 			= 1,
	LINE_STRIP      = 2,
	LINE_LOOP       = 3,
	TRIANGLES 		= 4,
	TRIANGLE_STRIP 	= 5,
	QUADS 			= 6,
};

enum class BufferFlag : enum_t
{
	// TODO
	NONE 		= 0,
	COLOR 		= 1,
	DEPTH		= 2,
	ALL			= COLOR | DEPTH,
};

enum class TypeTag
{
	// TODO
	FLOAT = 0,
	FLOAT_2 = 1,
	FLOAT_3 = 2,
	FLOAT_4 = 3,
	FLOAT_2X2 = 4,
	FLOAT_3X3 = 5,
	FLOAT_4X4 = 6,

	INT = 7,
	INT_2 = 8,
	INT_3 = 9,
	INT_4 = 10,

	UNSIGNED_INT = 11,
	UNSIGNED_INT_2 = 12,
	UNSIGNED_INT_3 = 13,
	UNSIGNED_INT_4 = 14
};

}

#define LOG_VULKAN_CALLS			0
#define CHECK_VULKAN_OUT_OF_MEMORY	1
#define ASSERT_NO_VULKAN_ERRORS		1

// Wrapper to Vulkan.
namespace vulkan {

/* TODO
// Direct rendering
void vertex(gr::real_t x, gr::real_t y);
void vertex(gr::real_t x, gr::real_t y, gr::real_t z);
void vertex(gr::real_t x, gr::real_t y, gr::real_t z);
void vertex(gr::real_t x, gr::real_t y, gr::real_t z, gr::real_t w);

void begin(gr::Primitive primitive);
void end();
//

// Buffer handling
void genBuffers(GLuint count, GLuint* ids);
void bindBuffer(GLenum type, GLuint id);
void bufferData(GLenum type, GLsizei size, const GLvoid* data, gr::BufferUsage usage);
void bufferSubData(GLenum type, GLuint offset, GLsizei size, const GLvoid* data);
void deleteBuffers(GLuint count, const GLuint* ids);
//

// Array handling
void vertexPointer(GLuint dim, GLenum type, GLuint offset, const void* data);
void colorPointer(GLuint dim, GLenum type, GLuint offset, const void* data);
void texCoordPointer(GLuint dim, GLenum type, GLuint offset, const void* data);
void normalPointer(GLenum type, GLuint offset, const void* data);
//

// State handling
void enableClientState(GLenum type);
void disableClientState(GLenum type);
void enable(GLenum type);
void disable(GLenum type);
//

// Matrix handling
void matrixMode(GLenum type);
void loadIdentity();
void loadMatrix(const float* mat);
void multMatrix(const float* mat);
//

// Framebuffer handling
void clear(GLbitfield mask);
//
*/

} }

using pm::gr::operator "" _r;
