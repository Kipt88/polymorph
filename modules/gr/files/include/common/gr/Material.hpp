#pragma once

#include "gr/types.hpp"
#include "gr/ShaderSuite.hpp"
#include "gr/Texture.hpp"

#include <tuple>
#include <string>
#include <vector>

namespace pm { namespace gr {

struct Material
{
	Vector3_r ambientColor;
	Vector3_r diffuseColor;
	Vector3_r specularColor;

	std::vector<TextureHandle> textures;
	// ShaderSuiteHandle shader;
};

typedef std::shared_ptr<const Material> MaterialHandle;
typedef std::weak_ptr<const Material> WeakMaterialHandle;

} }
