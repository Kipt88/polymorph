#pragma once

#include <gr/Image.hpp>
#include <io/IOStream.hpp>
#include <Exception.hpp>

#include <cstddef>

namespace pm { namespace gr {

PM_MAKE_EXCEPTION_CLASS(ImageResourceException, Exception);
PM_MAKE_EXCEPTION_CLASS(UnknownImageFormat, ImageResourceException);
PM_MAKE_EXCEPTION_CLASS(ImageReadException, ImageResourceException);

namespace ImageReader
{
	typedef gr::Image (*Reader)(io::InputStream&);
	
	enum class image_t : std::size_t
	{
		PNG = 0
	};

	gr::Image read(io::InputStream& input, image_t imageType);
}

} }
