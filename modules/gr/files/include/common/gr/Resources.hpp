#pragma once

#include "gr/Material.hpp"
#include "gr/Texture.hpp"
#include "gr/Mesh.hpp"
#include "gr/SystemRunner.hpp"

#include <concurrency/System.hpp>
#include <concurrency/Task.hpp>

#include <map>

namespace pm { namespace gr {

class Resources
{
public:
	explicit Resources(concurrency::ConcurrentSystem<SystemRunner>& system);

	MaterialHandle acquireMaterial(id_t id);
	TextureHandle acquireTexture(id_t id);

	// TODO Merge meshes and sprites...

	MeshHandle acquireMesh(id_t id);
	MeshHandle acquireSprite(id_t id);

	template <VertexFormat VF, int FaceSize>
	MeshHandle createMesh(id_t id, MeshDefinition<VF, FaceSize>&& def)
	{
		auto mesh = _system.pushAction(
			[id, d = std::move(def)](SystemRunner& runner) mutable
			{
				return runner.meshes().createMesh(id, std::move(d));
			}
		).get();

		_meshes.insert_or_assign(id, mesh);

		return mesh;
	}

private:
	concurrency::ConcurrentSystem<SystemRunner>& _system;
	
	std::map<id_t, WeakMaterialHandle> _materials;
	std::map<id_t, WeakTextureHandle> _textures;
	std::map<id_t, WeakMeshHandle> _meshes;
};

} }
