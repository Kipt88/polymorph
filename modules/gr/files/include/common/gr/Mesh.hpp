#pragma once

#include "gr/VertexList.hpp"
#include "gr/Material.hpp"
#include "gr/types.hpp"
#include "gr/BufferProvider.hpp"
#include "gr/Material.hpp"

#include <math/Box.hpp>

#include <NonCopyable.hpp>

#include <memory>
#include <map>

namespace pm { namespace gr {

class Mesh : NonCopyable
{
public:
	struct FaceShape
	{
		FaceShape() = delete;

		IndexSubBufferView buffer;
		Primitive primitive;
	};

	typedef std::multimap<MaterialHandle, FaceShape> face_collection_t;

	/**
	 * @param vertices 
	 *		Vertices of mesh.
	 * @param faces 
	 *		Collection of collection of vertex indices defining the faces with a material property.
	 * @param shape 
	 *		Primitive used when rendering.
	 * @param manager 
	 *		Buffer manager used to create buffers.
	 */
	template <VertexFormat VF, typename FaceList>
	explicit Mesh(
		const VertexList<VF>& vertices, 
		FaceList&& faces,
		BufferUsage usage,
		BufferProvider& manager
	);

	explicit Mesh(
		VertexSubBufferView&& vertices, 
		face_collection_t&& faces,
		const math::AxisAlignedBox3<real_t>& bounds
	);

	Mesh(Mesh&& other);

	VertexSubBufferView& vertices();
	const VertexSubBufferView& vertices() const;

	face_collection_t& faces();
	const face_collection_t& faces() const;

	math::AxisAlignedBox3<real_t>& bounds();
	const math::AxisAlignedBox3<real_t>& bounds() const;

private:
	VertexSubBufferView _vertices;
	face_collection_t _faces;
	math::AxisAlignedBox3<real_t> _bounds;
};

typedef std::shared_ptr<Mesh> MeshHandle;
typedef std::weak_ptr<Mesh> WeakMeshHandle;

template <typename... Args>
MeshHandle make_mesh(Args&&... args)
{
	return std::make_shared<Mesh>(std::forward<Args>(args)...);
}

} }

#include "gr/Mesh.inl"