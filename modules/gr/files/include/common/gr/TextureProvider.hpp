#pragma once

#include "gr/ImageReader.hpp"
#include "gr/Image.hpp"
#include "gr/Texture.hpp"

#include <Id.hpp>
#include <Cache.hpp>
#include <NonCopyable.hpp>

#include <string>
#include <map>
#include <filesystem.hpp>

namespace pm { namespace gr {

class TextureProvider : Unique
{
public:
	typedef std::filesystem::path path_t;
	typedef id_t image_id_t;
	typedef ImageReader::image_t image_t;
	typedef CacheMap<image_id_t, TextureHandle> TextureCache;

	explicit TextureProvider(
		const path_t& root = std::filesystem::current_path()
	);

	/**
	 * Loads texture if not already loaded, possibly removing unused textures
	 * according to cache algorithm.
	 * 
	 * @precond Image with id must've been registered.
	 * @param id Id of image to use as source for texture.
	 */
	TextureHandle loadTexture(image_id_t id, const Texture::Params& params = Texture::Params{});

	/**
	 * Gets a texture based on id, may return null-handle.
	 * This method is thread-safe.
	 *
	 * @param id Id or texture to get.
	 * @return texture handle, possibly null-handle.
	 */
	TextureHandle get(image_id_t id) const;

	TextureHandle acquire(image_id_t id, const Texture::Params& params = Texture::Params{});

	/**
	 * Sets maximum graphics memory to use (approximate value).
	 * This method may not be called after a texture manager has been instatiated.
	 * @param bytes New size of graphics memory limit.
	 */
	// static void memoryLimit(std::size_t bytes);

	/**
	 * Registers an image resource.
	 * This may not be called after a texture manager has been instatiated.
	 *
	 * @param path image path.
	 * @return Hashed ID of path, used as identifier of image and texture.
	 */
	static image_id_t registerImage(image_id_t id, const path_t& path, image_t typeId);

private:
	struct ImageMetaData
	{
		path_t path;
		image_t imageType;
	};

	TextureCache _textures;

	path_t _root;

	static std::map<image_id_t, ImageMetaData>& getImagesMetaData();
};

} }
