#pragma once

#include "gr/PixelFormat.hpp"

#include <memory>
#include <NonCopyable.hpp>

namespace pm { namespace gr {

class Image : NonCopyable
{
public:
	Image(unsigned int w,
	      unsigned int h,
	      PixelFormat format,
	      unsigned int rowAlignment,
	      std::unique_ptr<unsigned char[]>&& pixels);

	Image(Image&& image);

	std::size_t estimatedSize() const;
	unsigned int width() const;
	unsigned int height() const;
	PixelFormat pixelFormat() const;
	unsigned int rowAlignment() const;
	const unsigned char* data() const;

private:
	unsigned int _width;
	unsigned int _height;
	PixelFormat _format;
	unsigned int _rowAlignment;
	std::unique_ptr<unsigned char[]> _pixels;
};

} }
