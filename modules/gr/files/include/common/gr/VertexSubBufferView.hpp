#pragma once

#include "gr/VertexBuffer.hpp"
#include "gr/IndexSubBufferView.hpp"

#include <NonCopyable.hpp>

namespace pm { namespace gr {

class VertexSubBufferView;

class VertexSubBufferViewHandler
{
public:
	virtual void releaseVertexSubBufferView(const VertexSubBufferView& view) = 0;
};

class VertexSubBufferView
{
public:
	explicit VertexSubBufferView(
		VertexSubBufferViewHandler& handler,
		VertexBufferHandle buffer,
		std::size_t startIndex,
		std::size_t size
	);

	VertexSubBufferView(VertexSubBufferView&& other);

	~VertexSubBufferView();

	VertexSubBufferView& operator=(VertexSubBufferView&& other);
	VertexSubBufferView& operator=(const VertexSubBufferView& other);

	void write(const void* vertices, std::size_t index, std::size_t size = 1);

	void draw(Primitive primitive, const IndexSubBufferView& indices) const;
	void draw(Primitive primitive, std::size_t index, std::size_t count) const;
	void draw(Primitive primitive) const;

	const VertexBufferHandle& buffer() const;
	std::size_t startIndex() const;
	std::size_t size() const;

private:
	VertexSubBufferViewHandler* _handler;
	VertexBufferHandle _buffer;
	std::size_t _startIndex;
	std::size_t _size;
};

} }