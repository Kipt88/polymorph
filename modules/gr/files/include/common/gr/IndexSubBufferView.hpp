#pragma once

#include "gr/IndexBuffer.hpp"

#include <NonCopyable.hpp>

namespace pm { namespace gr {

class IndexSubBufferView;

class IndexSubBufferViewHandler
{
public:
	virtual void releaseIndexSubBufferView(const IndexSubBufferView& view) = 0;
};

class IndexSubBufferView : NonCopyable
{
public:
	IndexSubBufferView(IndexSubBufferViewHandler& handler,
					   IndexBufferHandle buffer,
					   std::size_t startIndex,
					   std::size_t size);

	IndexSubBufferView(IndexSubBufferView&& other);

	~IndexSubBufferView();

	IndexSubBufferView& operator=(IndexSubBufferView&& other);

	void write(std::size_t offset, std::size_t size, const index_t* indices);

	/**
	 * Partitions this buffer view to two new partitions.
	 * @pre size must be smaller than this size.
	 * @param size Size of new partition.
	 * @returns New partition of requested size.
	 */
	IndexSubBufferView partition(std::size_t size);

	const IndexBufferHandle& buffer() const;
	std::size_t startIndex() const;
	std::size_t size() const;

private:
	IndexSubBufferViewHandler* _handler;
	IndexBufferHandle _buffer;
	std::size_t _startIndex;
	std::size_t _size;
};

} }