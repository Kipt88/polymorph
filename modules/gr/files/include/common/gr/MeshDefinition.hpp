#pragma once

#include <gr/VertexList.hpp>
#include <gr/types.hpp>
#include <Id.hpp>
#include <vector>
#include <array>

namespace pm { namespace gr {

template <int FaceSize>
struct FaceDefinition
{
	std::array<index_t, FaceSize> indices;
	Primitive primitive;
	id_t materialId;
};

template <>
struct FaceDefinition<-1>
{
	std::vector<index_t> indices;
	Primitive primitive;
	id_t materialId;
};


template <VertexFormat VF, int FaceSize = -1>
struct MeshDefinition
{
	typedef FaceDefinition<FaceSize> face_t;

	gr::BufferUsage usage;
	VertexList<VF> vertices;
	std::vector<face_t> faces;
};

} }
