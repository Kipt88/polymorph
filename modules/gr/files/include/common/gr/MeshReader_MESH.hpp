#pragma once

#include <io/IOStream.hpp>

#include <gr/Mesh.hpp>

namespace pm { namespace gr {

namespace MeshReader_MESH
{
	gr::Mesh read(io::InputStream& input, gr::BufferProvider& bufferManager);
}

} }
