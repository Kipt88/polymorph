#pragma once

#include "gr/Surface.hpp"
#include "gr/MaterialProvider.hpp"
#include "gr/TextureProvider.hpp"
#include "gr/MeshProvider.hpp"
#include "gr/BufferProvider.hpp"
#include "gr/Frame.hpp"

#include <os/Window.hpp>
#include <concurrency/System.hpp>
#include <concurrency/FrameTime.hpp>
#include <concurrency/Mutex.hpp>

namespace pm { namespace gr {

class SystemRunner : Unique
{
public:
	static constexpr const char* name = "Graphics";

	explicit SystemRunner(concurrency::RunnerTasks<SystemRunner>&, os::Window& window, Frame& lastFrame, concurrency::Mutex& frameMutex);

	bool update(const concurrency::FrameTime&);

	Surface& surface();

	MaterialProvider& materials();
	TextureProvider& textures();
	MeshProvider& meshes();
	BufferProvider& buffers();

private:
	Surface _surface;

	BufferProvider   _buffers;
	TextureProvider  _textures;
	MaterialProvider _materials;
	MeshProvider     _meshes;
	
	Frame& _lastFrame;
	concurrency::Mutex& _frameMutex;
};

} }
