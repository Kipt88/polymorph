#pragma once

#include "gr/TextureProvider.hpp"
#include "gr/Material.hpp"
#include "gr/Color.hpp"

#include <NonCopyable.hpp>
#include <Cache.hpp>
#include <Id.hpp>

namespace pm { namespace gr {

class MaterialProvider : Unique
{
public:
	struct MaterialMetaData
	{
		Color ambientColor;
		Color diffuseColor;
		Color specularColor;

		std::vector<TextureProvider::image_id_t> textures;
	};

	typedef id_t material_id_t;
	
	MaterialProvider(TextureProvider& textureProvider);

	MaterialHandle loadMaterial(material_id_t id);

	MaterialHandle get(material_id_t id);

	MaterialHandle acquire(material_id_t id);

	static material_id_t registerMaterial(
		const std::string& materialName,
		const Color& ambientColor,
		const Color& diffuseColor,
		const Color& specularColor,
		const std::vector<TextureProvider::image_id_t>& textures
	);

private:
	typedef CacheMap<material_id_t, MaterialHandle> MaterialCache;

	TextureProvider& _textureProvider;
	MaterialCache _materials;


	static std::map<material_id_t, MaterialMetaData>& getMaterialMetaData();
};

} }
