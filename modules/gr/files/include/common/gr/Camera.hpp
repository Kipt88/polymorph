#pragma once

#include "gr/types.hpp"


namespace pm { namespace gr {

template <typename TransformType>
struct Camera
{
	TransformType transform;
	Projection projection;
};


} }