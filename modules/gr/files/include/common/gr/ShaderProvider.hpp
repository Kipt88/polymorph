#pragma once

#include <gr/Surface.hpp>
#include <gr/Shader.hpp>
#include <gr/ShaderSuite.hpp>

#include <concurrency/Thread.hpp>
#include <concurrency/Task.hpp>
#include <concurrency/TaskRunner.hpp>
#include <concurrency/Condition.hpp>
#include <concurrency/Mutex.hpp>

#include <Id.hpp>
#include <Cache.hpp>
#include <NonCopyable.hpp>

#include <string>
#include <map>


namespace pm { namespace gr {

PM_MAKE_EXCEPTION_CLASS(NoSuchShaderException, Exception);
PM_MAKE_EXCEPTION_CLASS(NoSuchShaderSuiteException, Exception);

class ShaderProvider : NonCopyable, NonMovable
{
public:
	typedef std::string path_t;
	typedef id_t shader_suite_id_t;
	typedef id_t shader_id_t;

	typedef concurrency::Future<gr::ShaderSuiteHandle> LoadResult;

	typedef CacheMap<shader_suite_id_t, gr::ShaderSuiteHandle> ShaderSuiteCache;
	typedef CacheMap<shader_id_t, gr::ShaderHandle> ShaderCache;
	

	ShaderProvider(gr::Surface& surface);

	LoadResult loadShaderSuite(shader_suite_id_t id);

	gr::ShaderSuiteHandle get(shader_suite_id_t id) const;

	static shader_id_t registerShader(const path_t& path, gr::ShaderType type);

	static shader_suite_id_t registerShaderSuite(
		std::initializer_list<shader_id_t> shaders
	);

private:
	gr::Surface& _surface;

	mutable concurrency::Mutex _cacheMutex;
	ShaderCache _shaders;
	ShaderSuiteCache _shaderSuites;

	concurrency::TaskRunner<gr::ShaderSuiteHandle> _loader;

	struct ShaderMetaData
	{
		path_t path;
		gr::ShaderType type;
	};

	struct ShaderSuiteMetaData
	{
		std::vector<shader_id_t> shaders;
	};

	//
	// Expected to be constant after construction of first instance.
	//

	static std::map<shader_id_t, ShaderMetaData>& getShadersMetaData();
	static std::map<shader_suite_id_t, ShaderSuiteMetaData>& getShaderSuitesMetaData();
};

} }