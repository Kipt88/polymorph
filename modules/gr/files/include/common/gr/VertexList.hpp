#pragma once

#include "gr/types.hpp"
#include "gr/VertexFormat.hpp"

#include <vector>
#include <iterator>

namespace pm { namespace gr {

template <VertexFormat VF = VertexFormat::DYNAMIC_TAG>
class VertexList
{
public:
	typedef typename std::vector<Vertex<VF>>::iterator iterator;
	typedef typename std::vector<Vertex<VF>>::const_iterator const_iterator;

	explicit VertexList(std::size_t initialCapacity);
	explicit VertexList(const std::vector<Vertex<VF>>& vertices = std::vector<Vertex<VF>>());
	explicit VertexList(std::vector<Vertex<VF>>&& vertices);

	void* operator[](index_t index);
	const void* operator[](index_t index) const;

	Vertex<VF>& get(index_t index);
	const Vertex<VF>& get(index_t index) const;

	void* data();
	const void* data() const;

	iterator begin();
	const_iterator begin() const;

	iterator end();
	const_iterator end() const;

	bool empty() const;
	std::size_t size() const;

	void erase(const_iterator it);

	void clear();
	void push_back(const Vertex<VF>& vertex);
	void pop_back();

	void resize(std::size_t size);
	void reserve(std::size_t size);

	VertexFormatData formatData() const;

private:
	std::vector<Vertex<VF>> _data;
};

namespace detail {

template <typename P>
class dynamic_iterator : public std::iterator<std::random_access_iterator_tag, P>
{
public:
	typedef typename std::iterator<std::random_access_iterator_tag, P>::difference_type difference_type;
	typedef typename std::iterator<std::random_access_iterator_tag, P>::reference reference;
	typedef typename std::iterator<std::random_access_iterator_tag, P>::pointer pointer;
	
	dynamic_iterator operator+(difference_type);
	dynamic_iterator operator-(difference_type);
	difference_type operator-(const dynamic_iterator&);
	dynamic_iterator& operator++();
	dynamic_iterator operator++(int);
	dynamic_iterator& operator--();
	dynamic_iterator operator--(int);

	bool operator<(const dynamic_iterator&);
	bool operator>(const dynamic_iterator&);
	bool operator<=(const dynamic_iterator&);
	bool operator>=(const dynamic_iterator&);
	bool operator==(const dynamic_iterator&);
	bool operator!=(const dynamic_iterator&);

	dynamic_iterator();
	dynamic_iterator(P marker, P end, std::ptrdiff_t typeSize);
	dynamic_iterator(const dynamic_iterator&) = default;

	reference operator[](difference_type i);
	reference operator*();
	pointer operator->();

private:
	P _marker;
	P _end;

	std::ptrdiff_t _typeSize;
};

struct free_deleter
{
	inline void operator()(void* mem);
};

}

template <>
class VertexList<VertexFormat::DYNAMIC_TAG>
{
public:
	typedef detail::dynamic_iterator<void*> iterator;
	typedef detail::dynamic_iterator<const void*> const_iterator;

	template <VertexFormat VF>
	VertexList(const std::vector<Vertex<VF>>& vertices);

	VertexList(const VertexFormatData& fmtData, const void* data = nullptr, index_t size = 0);

	void* operator[](index_t index);
	const void* operator[](index_t index) const;

	template <VertexFormat VF>
	const Vertex<VF>& get(index_t index) const;

	template <VertexFormat VF>
	Vertex<VF>& get(index_t index);

	void* data();
	const void* data() const;

	iterator begin();
	const_iterator begin() const;

	iterator end();
	const_iterator end() const;

	bool empty() const;
	std::size_t size() const;

	void clear();

	template <VertexFormat VF>
	void push_back(const Vertex<VF>& vertex);

	void push_back(const void* vertex);

	void pop_back();

	void resize(std::size_t newSize);
	void reserve(std::size_t size);

	VertexFormatData formatData() const;

private:
	std::unique_ptr<void, detail::free_deleter> _begin;
	std::size_t _size;
	std::size_t _capacity;

	VertexFormatData _fmtData;
	std::size_t _vertexSize;
};

} }

#include "gr/VertexList.inl"
