#pragma once

#include "gr/Surface.hpp"
#include "gr/VertexSubBufferView.hpp"
#include "gr/IndexSubBufferView.hpp"
#include "gr/IndexBuffer.hpp"
#include "gr/VertexBuffer.hpp"
#include "gr/VertexList.hpp"

#include <LinearSpacePartitioner.hpp>
#include <NonCopyable.hpp>
#include <Cache.hpp>

#include <list>

namespace pm { namespace gr {

/**
 * Vertex and index buffer provider.
 * Handles lifetime and reuse of graphics buffers.
 */
class BufferProvider 
: VertexSubBufferViewHandler, 
  IndexSubBufferViewHandler, 
  Unique
{
public:
	/**
	 * Allocates or reuses a free part of a buffer with same vertex format and usage.
	 * This will lock the surface when modifying any vertex buffer.
	 */
	VertexSubBufferView getVertexBuffer(
		const VertexFormatData& fmtData,
		std::size_t size,
		BufferUsage usage,
		const void* data = nullptr
	);

	template <VertexFormat VF>
	VertexSubBufferView getVertexBuffer(
		const VertexList<VF>& vl,
		BufferUsage usage
	);

	IndexSubBufferView getIndexBuffer(
		BufferUsage usage,
		std::size_t size,
		const index_t* data = nullptr
	);

	template <typename InputIt>
	std::vector<IndexSubBufferView> getIndexBuffers(
		BufferUsage usage,
		InputIt sizeDataBegin,
		InputIt sizeDataEnd
	);

	void releaseVertexSubBufferView(const VertexSubBufferView& view) override;
	void releaseIndexSubBufferView(const IndexSubBufferView& view) override;

private:
	std::map<VertexBufferHandle, LinearSpacePartitioner> _freeVBPartitions;
	Cache<VertexBufferHandle> _vertexBuffers;

	std::map<IndexBufferHandle, LinearSpacePartitioner> _freeIBPartitions;
	Cache<IndexBufferHandle> _indexBuffers;
};

} }


#include "gr/BufferProvider.inl"