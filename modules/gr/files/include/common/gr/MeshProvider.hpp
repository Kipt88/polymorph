#pragma once

#include "gr/MaterialProvider.hpp"
#include "gr/BufferProvider.hpp"
#include "gr/MeshReader.hpp"
#include "gr/TextureProvider.hpp"
#include "gr/MeshDefinition.hpp"

#include <concurrency/Task.hpp>

#include <NonCopyable.hpp>
#include <Cache.hpp>
#include <Id.hpp>

#include <string>

namespace pm { namespace gr {

class MeshProvider : Unique
{
public:
	// TODO Merge sprites-meshes.

	typedef std::filesystem::path path_t;
	typedef id_t mesh_id_t;
	typedef id_t sprite_id_t;
	typedef MeshReader::mesh_t mesh_t;

	explicit MeshProvider(BufferProvider& bufferProvider, MaterialProvider& materialProvider);

	MeshHandle loadMesh(mesh_id_t id);
	MeshHandle loadSprite(sprite_id_t id);

	template <VertexFormat VF, int FaceSize>
	MeshHandle createMesh(mesh_id_t id, MeshDefinition<VF, FaceSize>&& def);

	MeshHandle get(mesh_id_t id);
	MeshHandle acquireMesh(mesh_id_t id);
	MeshHandle acquireSprite(mesh_id_t id);

	static sprite_id_t registerSprite(
		sprite_id_t id,
		TextureProvider::image_id_t texture,
		const Vector2_r& meshSize,
		const Vector2_r& uv,
		const Vector2_r& size
	);

	static mesh_id_t registerMesh(mesh_id_t id, const path_t& path, mesh_t typeId);

private:
	typedef CacheMap<mesh_id_t, MeshHandle> MeshCache;
	
	struct MeshMetaData
	{
		path_t path;
		mesh_t meshType;
	};

	struct SpriteMetaData
	{
		MaterialProvider::material_id_t materialId;
		Vector2_r uv;
		Vector2_r size;
		Vector2_r meshSize;
	};

	BufferProvider& _bufferProvider;
	MaterialProvider& _materialProvider;

	MeshCache _meshes;

	static std::map<sprite_id_t, SpriteMetaData>& spriteMetaData();
	static std::map<mesh_id_t, MeshMetaData>& getMeshMetaData();
};

} }

#include "gr/MeshProvider.inl"
