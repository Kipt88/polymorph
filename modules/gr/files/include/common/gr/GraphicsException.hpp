#pragma once

#include <Exception.hpp>

namespace pm { namespace gr {

PM_MAKE_EXCEPTION_CLASS(GraphicsException, Exception);

} }