#pragma once

#include "gr/Resources.hpp"
#include "gr/SystemRunner.hpp"
#include "gr/Frame.hpp"

#include <os/App.hpp>
#include <Time.hpp>

namespace pm { namespace gr {

class Graphics
{
public:
	explicit Graphics(const Time::duration& sync, os::AppSystem& app);

	ClientFrame createFrame();

	Resources& resources();
	math::Vector<int, 2> getSurfaceSize() const;

	template <typename Callable>
	concurrency::Future<std::result_of_t<Callable(Surface&)>> pushAction(Callable&& action)
	{
		return _system.pushAction(
			[a = std::forward<Callable>(action)](SystemRunner& runner)
			{
				return a(runner.surface());
			}
		);
	}

private:
	mutable concurrency::ConcurrentSystem<SystemRunner> _system;

	concurrency::Mutex _frameMutex;
	Frame _lastFrame;

	Resources _resources;

	os::AppEventScope<os::window_events::Resize> _onResize;
};

} }
