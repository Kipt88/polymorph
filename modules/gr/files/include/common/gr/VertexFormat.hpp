#pragma once

#include "gr/types.hpp"

#include <cstring>

namespace pm { namespace gr {

// Vertices must store components in VCTN order.
enum class VertexFormat : size_t
{
	DYNAMIC_TAG,
	V2,
	V2_C3,
	V2_C4,
	V2_T2,
	V2_C3_T2,
	V2_C4_T2,
	V3,
	V3_C3,
	V3_C4,
	V3_T2,
	V3_C3_T2,
	V3_C4_T2
};

struct VertexFormatData
{
	unsigned int vertDim;
	unsigned int colorDim;
	unsigned int textDim;
	unsigned int normalDim;

	// Byte offsets to components:
	unsigned int vertOffset() const;
	unsigned int colorOffset() const;
	unsigned int textOffset() const;
	unsigned int normalOffset() const;

	// Size of a vertex in bytes.
	unsigned int size() const;
};

bool operator==(const VertexFormatData& a, const VertexFormatData& b);
bool operator!=(const VertexFormatData& a, const VertexFormatData& b);


// Common vertex formats:

template <VertexFormat VF>
struct Vertex;

template <>
struct Vertex<VertexFormat::V2>
{
	Vector2_r pos;

	static const VertexFormatData FORMAT_DATA;
};

template <>
struct Vertex<VertexFormat::V2_C3>
{
	Vector2_r pos;
	Vector3_r color;

	static const VertexFormatData FORMAT_DATA;
};

template <>
struct Vertex<VertexFormat::V2_C4>
{
	Vector2_r pos;
	Vector4_r color;

	static const VertexFormatData FORMAT_DATA;
};

template <>
struct Vertex<VertexFormat::V2_T2>
{
	Vector2_r pos;
	Vector2_r texCoord;

	static const VertexFormatData FORMAT_DATA;
};

template <>
struct Vertex<VertexFormat::V2_C3_T2>
{
	Vector2_r pos;
	Vector3_r color;
	Vector2_r texCoord;

	static const VertexFormatData FORMAT_DATA;
};

template <>
struct Vertex<VertexFormat::V2_C4_T2>
{
	Vector2_r pos;
	Vector4_r color;
	Vector2_r texCoord;

	static const VertexFormatData FORMAT_DATA;
};

template <>
struct Vertex<VertexFormat::V3>
{
	Vector3_r pos;

	static const VertexFormatData FORMAT_DATA;
};

template <>
struct Vertex<VertexFormat::V3_C3>
{
	Vector3_r pos;
	Vector3_r color;

	static const VertexFormatData FORMAT_DATA;
};

template <>
struct Vertex<VertexFormat::V3_C4>
{
	Vector3_r pos;
	Vector4_r color;

	static const VertexFormatData FORMAT_DATA;
};

template <>
struct Vertex<VertexFormat::V3_T2>
{
	Vector3_r pos;
	Vector2_r texCoord;

	static const VertexFormatData FORMAT_DATA;
};

template <>
struct Vertex<VertexFormat::V3_C3_T2>
{
	Vector3_r pos;
	Vector3_r color;
	Vector2_r texCoord;

	static const VertexFormatData FORMAT_DATA;
};

template <>
struct Vertex<VertexFormat::V3_C4_T2>
{
	Vector3_r pos;
	Vector4_r color;
	Vector2_r texCoord;

	static const VertexFormatData FORMAT_DATA;
};

} }