#pragma once

#include "gr/Surface.hpp"

#include <type_traits>
#include <memory>

namespace pm { namespace gr {

class Scene
{
public:
	Scene() = default;
	Scene(Scene&&) = default;
	Scene& operator=(Scene&&) = default;
	
	Scene(const Scene& other);
	Scene& operator=(const Scene& other);

	/**
	 * Sets the scene instance.
	 */
	template <typename SceneType>
	void set(SceneType&& scene);

	/**
	 * Updates the scene instance.
	 * @pre SceneType must be same type as previously called with set.
	 */
	template <typename SceneType>
	void update(const SceneType& scene);

	void draw(Surface&) const;

private:
	class SceneBase
	{
	public:
		virtual ~SceneBase() = default;

		virtual void draw(Surface& surface) const = 0;
		virtual std::unique_ptr<SceneBase> clone() const = 0;

		virtual void assign(const void* scene) = 0;
	};

	template <typename SceneType>
	class SceneDerived : public SceneBase
	{
	public:
		typedef std::decay_t<SceneType> DecayScene;

		static_assert(std::is_copy_assignable<DecayScene>::value, "Scene must be copy assignable.");
		static_assert(std::is_copy_constructible<DecayScene>::value, "Scene must be copy constructible.");

		SceneDerived(const DecayScene& scene);

		void assign(const void* scene) override;

		void draw(gr::Surface& surface) const override;
		std::unique_ptr<SceneBase> clone() const override;

	private:
		DecayScene _scene;
	};

	std::unique_ptr<SceneBase> _impl;
};

} }

#include "gr/Scene.inl"