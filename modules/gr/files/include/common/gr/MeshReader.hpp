#pragma once

#include <gr/Mesh.hpp>
#include <gr/BufferProvider.hpp>
#include <io/IOStream.hpp>
#include <Exception.hpp>

#include <cstddef>

namespace pm { namespace gr {

PM_MAKE_EXCEPTION_CLASS(MeshResourceException, Exception);
PM_MAKE_EXCEPTION_CLASS(UnknownMeshFormat, MeshResourceException);
PM_MAKE_EXCEPTION_CLASS(MeshReadException, MeshResourceException);

namespace MeshReader
{
	typedef gr::Mesh(*Reader)(io::InputStream&, gr::BufferProvider&);

	enum class mesh_t : std::size_t
	{
		OBJ = 0,
		MESH = 1
	};

	gr::Mesh read(io::InputStream& input, mesh_t meshType, gr::BufferProvider& bufferManager);
}

} }
