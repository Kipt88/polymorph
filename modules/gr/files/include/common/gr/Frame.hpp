#pragma once

#include "gr/Surface.hpp"

#include <concurrency/Mutex.hpp>

#include <UniqueFunction.hpp>
#include <NonCopyable.hpp>

#include <vector>
#include <functional>

namespace pm { namespace gr {

class Frame : NonCopyable
{
public:
	void set(std::vector<UniqueFunction<void(Surface&)>>&& queue);

	void render(Surface& surface) const;

private:
	std::vector<UniqueFunction<void(Surface&)>> _queue;
};

class ClientFrame : NonCopyable
{
public:
	ClientFrame(Frame& frame, concurrency::Mutex& frameMutex);
	ClientFrame(ClientFrame&& other);
	~ClientFrame();

	ClientFrame& operator=(ClientFrame&& other);

	template <typename Callable>
	void push(Callable&& action);

private:
	std::vector<UniqueFunction<void(Surface&)>> _queue;

	Frame* _frame;
	concurrency::Mutex* _frameMutex;
};

} }

#include "gr/Frame.inl"
