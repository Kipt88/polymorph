#pragma once

#include "gr/types.hpp"

namespace pm { namespace gr {

struct Color
{
	gr::real_t r;
	gr::real_t g;
	gr::real_t b;
	gr::real_t a;

	operator Vector3_r() const
	{
		return Vector3_r{ r, g, b };
	}
	
	operator Vector4_r() const
	{
		return Vector4_r{ r, g, b, a };
	}
};

constexpr Color BLACK = { 0.0_r, 0.0_r, 0.0_r, 1.0_r };
constexpr Color WHITE = { 1.0_r, 1.0_r, 1.0_r, 1.0_r };
constexpr Color GRAY  = { 0.5_r, 0.5_r, 0.5_r, 1.0_r };
constexpr Color RED   = { 1.0_r, 0.0_r, 0.0_r, 1.0_r };
constexpr Color GREEN = { 0.0_r, 1.0_r, 0.0_r, 1.0_r };
constexpr Color BLUE  = { 0.0_r, 0.0_r, 1.0_r, 1.0_r };

} }
