#pragma once

#include "gr/types.hpp"

#include <os/Window.hpp>
#import <Cocoa/Cocoa.h>

namespace pm { namespace gr {

class Surface
{
public:
	explicit Surface(os::Window& window);
	
	void setVSync(bool enable = true);

	void flipBuffers();

	void setViewport(const math::AxisAlignedBox2<int>& box);
	math::AxisAlignedBox2<int> getViewport() const;

	Vector2_r normalizedToWindow(const Vector2_r& v) const;
	Vector3_r normalizedToWindow(const Vector3_r& v) const;

	Vector2_r windowToNormalized(const Vector2_r& v) const;
	Vector3_r windowToNormalized(const Vector3_r& v) const;
	
private:
	__strong NSOpenGLContext* _context;
};

} }
