include("${CMAKE_CURRENT_SOURCE_DIR}/../../options.cmake")

set(
	dependent_modules
	"ext"
	"concurrency"
	"math"
	"filesystem"
	"io"
	"os"
	"profiler"
)

set(
	opengl_source_files
	"source/opengl/IndexBuffer.cpp"
	"source/opengl/PlainRenderer.cpp"
	"source/opengl/Shader.cpp"
	"source/opengl/ShaderSuite.cpp"
	"source/opengl/Texture.cpp"
	"source/opengl/TextureRenderer.cpp"
	"source/opengl/VertexBuffer.cpp"
	"source/opengl/VertexFormat.cpp"
)

set(
	opengl_inline_files
	"inline/opengl/gr/Mesh.inl"
	"inline/opengl/gr/ShaderSuite.inl"
	"inline/opengl/gr/types.inl"
)

set(
	opengl_include_files
	"include/opengl/gr/IndexBuffer.hpp"
	"include/opengl/gr/opengl.hpp"
	"include/opengl/gr/PixelFormat.hpp"
	"include/opengl/gr/PlainRenderer.hpp"
	"include/opengl/gr/Shader.hpp"
	"include/opengl/gr/ShaderSuite.hpp"
	"include/opengl/gr/Texture.hpp"
	"include/opengl/gr/TextureRenderer.hpp"
	"include/opengl/gr/types.hpp"
	"include/opengl/gr/VertexBuffer.hpp"
)

set(
	wgl_header_files
	"include/wgl/gr/Surface.hpp"
)

set(
	wgl_source_files
	"source/wgl/Surface.cpp"
)

set(
	macgl_header_files
	"include/macgl/gr/Surface.hpp"
	"include/macgl/gr/ImageReader_PNG.hpp"
)

set(
	macgl_source_files
	"source/macgl/Surface.mm"
	"source/macgl/ImageReader_PNG.mm"
)

set(
	vulkan_source_files
	"source/vulkan/IndexBuffer.cpp"
	"source/vulkan/PlainRenderer.cpp"
	"source/vulkan/Shader.cpp"
	"source/vulkan/ShaderSuite.cpp"
	"source/vulkan/Surface.cpp"
	"source/vulkan/Texture.cpp"
	"source/vulkan/TextureRenderer.cpp"
	"source/vulkan/VertexBuffer.cpp"
	"source/vulkan/VertexFormat.cpp"
)

set(
	vulkan_inline_files
	"inline/vulkan/gr/Mesh.inl"
)

set(
	vulkan_include_files
	"include/vulkan/gr/IndexBuffer.hpp"
	"include/vulkan/gr/PixelFormat.hpp"
	"include/vulkan/gr/PlainRenderer.hpp"
	"include/vulkan/gr/Shader.hpp"
	"include/vulkan/gr/ShaderSuite.hpp"
	"include/vulkan/gr/Surface.hpp"
	"include/vulkan/gr/Texture.hpp"
	"include/vulkan/gr/TextureRenderer.hpp"
	"include/vulkan/gr/types.hpp"
	"include/vulkan/gr/VertexBuffer.hpp"
	"include/vulkan/gr/vulkan.hpp"
)

set(
	libpng_header_files
	"include/libpng/gr/ImageReader_PNG.hpp"
)

set(
	libpng_source_files
	"source/libpng/ImageReader_PNG.cpp"
)

set(
	common_source_files
	"source/common/BufferProvider.cpp"
	"source/common/Frame.cpp"
	"source/common/Graphics.cpp"
	"source/common/Image.cpp"
	"source/common/ImageReader.cpp"
	"source/common/IndexSubBufferView.cpp"
	"source/common/MaterialProvider.cpp"
	"source/common/Mesh.cpp"
	"source/common/MeshProvider.cpp"
	"source/common/MeshReader.cpp"
	"source/common/MeshReader_MESH.cpp"
	"source/common/MeshReader_OBJ.cpp"
	"source/common/PixelFormat.cpp"
	"source/common/Resources.cpp"
	"source/common/Scene.cpp"
	"source/common/ShaderProvider.cpp"
	"source/common/SystemRunner.cpp"
	"source/common/TextureProvider.cpp"
	"source/common/VertexSubBufferView.cpp"
)

set(
	common_inline_files
	"inline/common/gr/BufferProvider.inl"
	"inline/common/gr/Frame.inl"
	"inline/common/gr/GraphicsSystem.inl"
	"inline/common/gr/MeshProvider.inl"
	"inline/common/gr/Model.inl"
	"inline/common/gr/Scene.inl"
	"inline/common/gr/VertexList.inl"
)

set(
	common_include_files
	"include/common/gr/BufferProvider.hpp"
	"include/common/gr/Camera.hpp"
	"include/common/gr/Color.hpp"
	"include/common/gr/Frame.hpp"
	"include/common/gr/FrameBuffer.hpp"
	"include/common/gr/GraphicsException.hpp"
	"include/common/gr/Graphics.hpp"
	"include/common/gr/Image.hpp"
	"include/common/gr/ImageReader.hpp"
	"include/common/gr/IndexSubBufferView.hpp"
	"include/common/gr/Material.hpp"
	"include/common/gr/MaterialProvider.hpp"
	"include/common/gr/Mesh.hpp"
	"include/common/gr/MeshDefinition.hpp"
	"include/common/gr/MeshProvider.hpp"
	"include/common/gr/MeshReader.hpp"
	"include/common/gr/MeshReader_MESH.hpp"
	"include/common/gr/MeshReader_OBJ.hpp"
	"include/common/gr/Resources.hpp"
	"include/common/gr/Scene.hpp"
	"include/common/gr/ShaderProvider.hpp"
	"include/common/gr/SurfaceException.hpp"
	"include/common/gr/SystemRunner.hpp"
	"include/common/gr/TextureProvider.hpp"
	"include/common/gr/VertexFormat.hpp"
	"include/common/gr/VertexList.hpp"
	"include/common/gr/VertexSubBufferView.hpp"
)

require_modules(${dependent_modules})

source_group("Sources" FILES ${common_source_files})
source_group("Inlines" FILES ${common_inline_files})
source_group("Headers" FILES ${common_include_files})
source_group("Sources\\OpenGL" FILES ${opengl_source_files})
source_group("Inlines\\OpenGL" FILES ${opengl_inline_files})
source_group("Headers\\OpenGL" FILES ${opengl_include_files})
source_group("Sources\\Vulkan" FILES ${vulkan_source_files})
source_group("Inlines\\Vulkan" FILES ${vulkan_inline_files})
source_group("Headers\\Vulkan" FILES ${vulkan_include_files})
source_group("Sources\\WGL" FILES ${wgl_source_files})
source_group("Headers\\WGL" FILES ${wgl_header_files})
source_group("Sources\\LibPNG" FILES ${libpng_source_files})
source_group("Headers\\LibPNG" FILES ${libpng_header_files})
source_group("Sources\\MacGL" FILES ${macgl_source_files})
source_group("Headers\\MacGL" FILES ${macgl_header_files})

add_library(
	"gr" STATIC 
	${common_source_files} 
	${common_inline_files} 
	${common_include_files}
)

target_include_directories(
	"gr" PUBLIC 
	"include/common" 
	"inline/common"
)

target_link_libraries(
	"gr" PUBLIC 
	${dependent_modules}
)

if(API_OPENGL)
	target_sources(
		"gr" PRIVATE
		${opengl_source_files}
		${opengl_inline_files}
		${opengl_include_files}
	)
	
	target_include_directories(
		"gr" PUBLIC
		"include/opengl"
		"inline/opengl"
	)
	
	if(WIN32)
		target_sources(
			"gr" PRIVATE 
			${wgl_source_files} 
			${wgl_header_files}
		)
		
		find_package(GLEW REQUIRED)
		
		target_compile_definitions(
			"gr" PUBLIC 
			${GLEW_DEFINITIONS}
		)
		
		target_include_directories(
			"gr" PUBLIC 
			"include/wgl"
			${GLEW_INCLUDE_DIRS}
		)
		
		target_link_libraries(
			"gr" PUBLIC 
			"opengl32"
			"$<$<CONFIG:Debug>:${GLEW_LIBRARY_DEBUG}>"
			"$<$<CONFIG:Release>:${GLEW_LIBRARY_RELEASE}>"
			"$<$<CONFIG:RelWithDebInfo>:${GLEW_LIBRARY_RELEASE}>"
		)
	elseif(APPLE)
		target_sources(
			"gr" PRIVATE
			${macgl_source_files}
			${macgl_header_files}
		)

		target_include_directories(
			"gr" PUBLIC
			"include/macgl"
		)

		target_link_libraries(
			"gr" PUBLIC
			"-framework Cocoa"
			"-framework OpenGL"
		)
	else()
		message(FATAL_ERROR "Module 'gr' does not support non-windows builds at this time.")
	endif()

elseif(API_VULKAN)
	target_sources(
		"gr" PRIVATE
		${vulkan_source_files}
		${vulkan_inline_files}
		${vulkan_include_files}
	)
	
	find_package(Vulkan REQUIRED)
	
	target_include_directories(
		"gr" PUBLIC
		"include/vulkan"
		"inline/vulkan"
		${VULKAN_INCLUDE_DIRS}
	)
	
	target_link_libraries(
		"gr" PUBLIC
		${VULKAN_LIBRARIES}
	)
	
	if(WIN32)
		## TODO:
	else()
		message(FATAL_ERROR "Module 'gr' with Vulkan does not support non-windows builds at this time.")
	endif()

else()
	message(FATAL_ERROR "Module 'gr' only supports OpenGL or Vulkan at this time.")
endif()

if(API_LIBPNG)
	find_package(LibPNG REQUIRED)
	
	target_sources(
		"gr" PRIVATE
		${libpng_source_files}
		${libpng_header_files}
	)
	
	target_include_directories(
		"gr" PRIVATE
		"include/libpng"
		${LibPNG_INCLUDE_DIRS}
	)
	
	target_link_libraries(
		"gr" PRIVATE 
		"$<$<CONFIG:Debug>:${LibPNG_LIBRARY_DEBUG}>"
		"$<$<CONFIG:Release>:${LibPNG_LIBRARY_RELEASE}>"
		"$<$<CONFIG:RelWithDebInfo>:${LibPNG_LIBRARY_RELEASE}>"
	)
	
elseif(APPLE)
	

else()
	message(FATAL_ERROR "Module 'gr' does not support non-LibPNG for current platform at this time.")
endif()
