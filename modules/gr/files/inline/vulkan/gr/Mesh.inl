#include "gr/Mesh.hpp"

namespace pm { namespace gr {

namespace detail {

template <VertexFormat VF>
math::AxisAlignedBox3<real_t> createBounds(const VertexList<VF>& vertices)
{
	Vector3_r min;
	Vector3_r max;
	return math::AxisAlignedBox3<real_t>(min, max);
}

template <VertexFormat VF>
VertexSubBufferView createVertices(const VertexList<VF>& vertices,
                                   BufferProvider& manager)
{
	return manager.getVertexBuffer(vertices, BufferUsage::STATIC);
}

template <typename FaceList>
Mesh::face_collection_t createFaces(FaceList&& faces,
                                    std::size_t baseIndex,
                                    BufferProvider& manager)
{
	Mesh::face_collection_t rFaces;
	return rFaces;
}

}

template <VertexFormat VF, typename FaceList>
Mesh::Mesh(const VertexList<VF>& vertices,
           FaceList&& faces,
           BufferProvider& manager)
: _vertices(detail::createVertices(vertices, manager)),
  _faces(detail::createFaces(std::move(faces), _vertices.startIndex(), manager)),
  _bounds(detail::createBounds(vertices))
{
}

} }
