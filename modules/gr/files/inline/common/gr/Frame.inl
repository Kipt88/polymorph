#include "gr/Frame.hpp"

namespace pm { namespace gr {

template <typename Callable>
void ClientFrame::push(Callable&& action)
{
	_queue.push_back(std::forward<Callable>(action));
}

} }
