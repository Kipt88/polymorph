#include "gr/MeshProvider.hpp"

#include <Debug.hpp>

namespace pm { namespace gr {

namespace details {

template <int FaceSize>
struct Face
{
	std::array<index_t, FaceSize> indices;
	MaterialHandle material;
	Primitive primitive;
};

template <>
struct Face<-1>
{
	std::vector<index_t> indices;
	MaterialHandle material;
	Primitive primitive;
};

}

template <VertexFormat VF, int FaceSize>
MeshHandle MeshProvider::createMesh(mesh_id_t id, MeshDefinition<VF, FaceSize>&& def)
{
	std::vector<details::Face<FaceSize>> faceDefinitions;

	for (auto&& face : def.faces)
	{
		details::Face<FaceSize> f;

		f.indices = std::move(face.indices);
		f.primitive = face.primitive;

		f.material = face.materialId == 0 ? nullptr : _materialProvider.loadMaterial(face.materialId);
		
		faceDefinitions.push_back(std::move(f));
	}

	auto meshHandle = make_mesh(def.vertices, std::move(faceDefinitions), def.usage, _bufferProvider);

	auto handle = _meshes.find(id);

	if (handle)
	{
		DEBUG_OUT("MeshProvider", "Overwriting mesh.");
		_meshes.erase(handle);
	}

	_meshes.insert(std::make_pair(id, meshHandle));

	return meshHandle;
}

} }
