#include "gr/Scene.hpp"

#include <Debug.hpp>

namespace pm { namespace gr {

// Scene:

template <typename SceneType>
void Scene::set(SceneType&& scene)
{
	_impl.reset(new SceneDerived<SceneType>(std::forward<SceneType>(scene)));
}

template <typename SceneType>
void Scene::update(const SceneType& scene)
{
	ASSERT(_impl != nullptr, "Need to set scene instance with set first.");

	_impl->assign(&scene);
}

// Scene::SceneDerived

template <typename SceneType>
Scene::SceneDerived<SceneType>::SceneDerived(const DecayScene& scene)
: _scene(scene)
{
}

template <typename SceneType>
void Scene::SceneDerived<SceneType>::assign(const void* scene)
{
	_scene = *reinterpret_cast<const std::decay_t<SceneType>*>(scene);
}

template <typename SceneType>
void Scene::SceneDerived<SceneType>::draw(gr::Surface& surface) const
{
	_scene.draw(surface);
}

template <typename SceneType>
std::unique_ptr<Scene::SceneBase> Scene::SceneDerived<SceneType>::clone() const
{
	return std::unique_ptr<SceneBase>(new SceneDerived<SceneType>(_scene));
}

} }