#include "gr/Model.hpp"

#include <Debug.hpp>

#include <limits>
#include <algorithm>

namespace pm { namespace gr {

template <typename TransformType>
Model<TransformType>::Model(std::vector<ModelMesh>&& modelMeshes, 
							unsigned int mask,
							const TransformType& transform,
							user_data_pointer data)
:	 _modelMeshes(std::move(modelMeshes)),
	_mask(mask),
	_transform(transform),
	_localBounds(detail::createBoundingBox<TransformType>(_modelMeshes)),
	_userData(data)
{
}

template <typename TransformType>
Model<TransformType>::Model(Model<TransformType>&& other)
:	_modelMeshes(std::move(other._modelMeshes)),
	_mask(other._mask),
	_transform(other._transform),
	_localBounds(other._localBounds),
	_userData(other._userData)
{
}

template <typename TransformType>
void Model<TransformType>::render(Renderer& renderer) const
{
	for (const auto& modelMesh : _modelMeshes)
		modelMesh.mesh->render(renderer, _transform * modelMesh.transform);
}

template <typename TransformType>
void Model<TransformType>::render(Renderer& renderer, 
								  const TransformType& parentTransform) const
{
	const auto& modelTransform = parentTransform * _transform;

	for (const auto& modelMesh : _modelMeshes)
		modelMesh.mesh->render(renderer, modelTransform * modelMesh.transform);
}

template <typename TransformType>
unsigned int Model<TransformType>::mask() const
{
	return _mask;
}

template <typename TransformType>
const typename Model<TransformType>::user_data_pointer Model<TransformType>::userData() const
{
	return _userData;
}

template <typename TransformType>
typename Model<TransformType>::user_data_pointer& Model<TransformType>::userData()
{
	return _userData;
}

template <typename TransformType>
TransformType& Model<TransformType>::transform()
{
	return _transform;
}

template <typename TransformType>
const TransformType& Model<TransformType>::transform() const
{
	return _transform;
}

template <typename TransformType>
bool Model<TransformType>::insideBoundBox(const MapVector2_r& p) const
{
	const auto& localSpaceVec2 = _transform * p;

	return _localBounds.inside(localSpaceVec2[0], localSpaceVec2[1], 0);
}

template <typename TransformType>
bool Model<TransformType>::insideBoundBox(const MapVector3_r& p) const
{
	const auto& localSpaceVec3 = _transform * p;

	return _localBounds.inside(localSpaceVec3);
}

template <typename TransformType>
bool Model<TransformType>::insideBoundBox(const MapVector_r& p) const
{
	real_t buffer[4];
	ASSERT(p.rows() < 5, "Unsupported vector size.");

	MapVector_r bufVec(buffer, p.rows());
	bufVec = p;

	_transform.transform(bufVec);

	real_t x = bufVec[0];
	real_t y = bufVec[1];
	real_t z = bufVec.rows() >= 3 ? bufVec[2] : 0.0f;

	return _localBounds.inside(x, y, z);
}


} }