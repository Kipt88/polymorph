#include "gr/VertexList.hpp"

#include <Assert.hpp>
#include <cstdlib>

namespace pm { namespace gr {

template <VertexFormat VF>
VertexList<VF>::VertexList(std::size_t initialCapacity)
: _data()
{
	_data.reserve(initialCapacity);
}

template <VertexFormat VF>
VertexList<VF>::VertexList(const std::vector<Vertex<VF>>& vertices)
: _data(vertices)
{
}

template <VertexFormat VF>
VertexList<VF>::VertexList(std::vector<Vertex<VF>>&& vertices)
: _data(std::move(vertices))
{
}

template <VertexFormat VF>
auto VertexList<VF>::operator[](index_t index) -> void*
{
	return &_data[index];
}

template <VertexFormat VF>
auto VertexList<VF>::operator[](index_t index) const -> const void*
{
	return &_data[index];
}

template <VertexFormat VF>
auto VertexList<VF>::get(index_t index) -> Vertex<VF>&
{
	return _data[index];
}

template <VertexFormat VF>
auto VertexList<VF>::get(index_t index) const -> const Vertex<VF>&
{
	return _data[index];
}

template <VertexFormat VF>
auto VertexList<VF>::data() -> void*
{
	return _data.data();
}

template <VertexFormat VF>
auto VertexList<VF>::data() const -> const void*
{
	return _data.data();
}

template <VertexFormat VF>
auto VertexList<VF>::begin() -> iterator
{
	return _data.begin();
}

template <VertexFormat VF>
auto VertexList<VF>::begin() const -> const_iterator
{
	return _data.begin();
}

template <VertexFormat VF>
auto VertexList<VF>::end() -> iterator
{
	return _data.end();
}

template <VertexFormat VF>
auto VertexList<VF>::end() const -> const_iterator
{
	return _data.end();
}

template <VertexFormat VF>
auto VertexList<VF>::empty() const -> bool
{
	return _data.empty();
}

template <VertexFormat VF>
auto VertexList<VF>::size() const -> std::size_t
{
	return _data.size();
}

template <VertexFormat VF>
auto VertexList<VF>::erase(const_iterator it) -> void
{
	_data.erase(it);
}

template <VertexFormat VF>
auto VertexList<VF>::clear() -> void
{
	_data.clear();
}

template <VertexFormat VF>
auto VertexList<VF>::push_back(const Vertex<VF>& vertex) -> void
{
	_data.push_back(vertex);
}

template <VertexFormat VF>
auto VertexList<VF>::pop_back() -> void
{
	_data.pop_back();
}

template <VertexFormat VF>
auto VertexList<VF>::reserve(std::size_t size) -> void
{
	_data.reserve(size);
}

template <VertexFormat VF>
auto VertexList<VF>::resize(std::size_t size) -> void
{
	_data.resize(size);
}

template <VertexFormat VF>
auto VertexList<VF>::formatData() const -> VertexFormatData
{
	return Vertex<VF>::FORMAT_DATA;
}

namespace detail {

template <typename P>
auto dynamic_iterator<P>::operator+(difference_type n) -> dynamic_iterator
{
	ASSERT(reinterpret_cast<std::uintptr_t>(_marker) + n * _typeSize <= reinterpret_cast<std::uintptr_t>(_end), "Iterator passing end.");
	return dynamic_iterator(reinterpret_cast<void*>(reinterpret_cast<std::uintptr_t>(_marker) + n * _typeSize), _end, _typeSize);
}

template <typename P>
auto dynamic_iterator<P>::operator-(difference_type n) -> dynamic_iterator
{
	ASSERT(reinterpret_cast<std::uintptr_t>(_marker) >= n * _typeSize, "Iterator passing beginning.");
	return dynamic_iterator(reinterpret_cast<void*>(reinterpret_cast<std::uintptr_t>(_marker) - n * _typeSize), _end, _typeSize);
}

template <typename P>
auto dynamic_iterator<P>::operator-(const dynamic_iterator<P>& other) -> difference_type
{
	ASSERT(_typeSize == other._typeSize, "Incorrect iterators.");
	ASSERT((reinterpret_cast<std::uintptr_t>(_marker) 
		    - reinterpret_cast<std::uintptr_t>(other._marker)) % _typeSize == 0, "Incorrect iterators.");

	return (reinterpret_cast<std::uintptr_t>(_marker) 
			- reinterpret_cast<std::uintptr_t>(other._marker)) / _typeSize;
}

template <typename P>
auto dynamic_iterator<P>::operator++() -> dynamic_iterator&
{
	return *this = *this + 1;
}

template <typename P>
auto dynamic_iterator<P>::operator++(int) -> dynamic_iterator
{
	dynamic_iterator it2 = *this;
	++*this;

	return it2;
}

template <typename P>
auto dynamic_iterator<P>::operator--() -> dynamic_iterator&
{
	return *this = *this - 1;
}

template <typename P>
auto dynamic_iterator<P>::operator--(int) -> dynamic_iterator<P>
{
	dynamic_iterator it2 = *this;
	--*this;

	return it2;
}

template <typename P>
auto dynamic_iterator<P>::operator<(const dynamic_iterator<P>& other) -> bool
{
	return _marker < other._marker;
}

template <typename P>
auto dynamic_iterator<P>::operator>(const dynamic_iterator<P>& other) -> bool
{
	return other < *this;
}

template <typename P>
auto dynamic_iterator<P>::operator<=(const dynamic_iterator<P>& other) -> bool
{
	return *this < other || *this == other;
}

template <typename P>
auto dynamic_iterator<P>::operator>=(const dynamic_iterator<P>& other) -> bool
{
	return *this > other || *this == other;
}

template <typename P>
auto dynamic_iterator<P>::operator==(const dynamic_iterator<P>& other) -> bool
{
	return _marker == other._marker;
}

template <typename P>
auto dynamic_iterator<P>::operator!=(const dynamic_iterator<P>& other) -> bool
{
	return !(*this == other);
}

template <typename P>
dynamic_iterator<P>::dynamic_iterator()
: dynamic_iterator(nullptr, nullptr, 0)
{
}

template <typename P>
dynamic_iterator<P>::dynamic_iterator(P marker, P end, std::ptrdiff_t typeSize)
: _marker(marker), _end(end), _typeSize(typeSize)
{
}

template <typename P>
auto dynamic_iterator<P>::operator[](difference_type i) -> reference
{
	return *(*this + i);
}

template <typename P>
auto dynamic_iterator<P>::operator*() -> reference
{
	return _marker;
}

template <typename P>
auto dynamic_iterator<P>::operator->() -> pointer
{
	return &_marker;
}



inline void free_deleter::operator()(void* mem)
{
	std::free(mem);
}

}

/*
template<VertexFormat::DYNAMIC_TAG>
template <VertexFormat VF>
inline pm::gr::VertexList<VertexFormat::DYNAMIC_TAG>::VertexList(const std::vector<Vertex<VF>>& vertices)
{
}
*/

template <VertexFormat VF>
inline VertexList<VertexFormat::DYNAMIC_TAG>::VertexList(const std::vector<Vertex<VF>>& vertices)
: _begin(), 
  _size(vertices.size()), 
  _capacity(0),
  _fmtData(Vertex<VF>::FORMAT_DATA), 
  _vertexSize(sizeof(Vertex<VF>))
{
	static_assert(std::is_trivially_copyable<Vertex<VF>>::value,
				  "Vertex type must be trivially copyable.");

	reserve(_size);

	std::memcpy(_begin.get(), vertices.data(), _size * _vertexSize);
}

inline VertexList<VertexFormat::DYNAMIC_TAG>::VertexList(const VertexFormatData& fmtData, const void* data, index_t size)
: _begin(),
  _size(size),
  _capacity(0),
  _fmtData(fmtData),
  _vertexSize(fmtData.size())
{
	reserve(_size);

	if (data)
		std::memcpy(_begin.get(), data, _size * _vertexSize);
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::operator[](index_t index) -> void*
{
	return begin()[index];
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::operator[](index_t index) const -> const void*
{
	return begin()[index];
}

template <VertexFormat VF>
inline auto VertexList<VertexFormat::DYNAMIC_TAG>::get(index_t index) -> Vertex<VF>&
{
	ASSERT(Vertex<VF>::FORMAT_DATA == _fmtData, "Incorrect vertex format.");
	return *reinterpret_cast<Vertex<VF>*>((*this)[index]);
}

template <VertexFormat VF>
inline auto VertexList<VertexFormat::DYNAMIC_TAG>::get(index_t index) const -> const Vertex<VF>&
{
	ASSERT(Vertex<VF>::FORMAT_DATA == _fmtData, "Incorrect vertex format.");
	return *reinterpret_cast<const Vertex<VF>*>((*this)[index]);
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::data() -> void*
{
	return _begin.get();
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::data() const -> const void*
{
	return _begin.get();
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::begin() -> iterator
{
	std::uintptr_t end = reinterpret_cast<std::uintptr_t>(_begin.get())
		                 + _size * _vertexSize;

	return iterator(_begin.get(), reinterpret_cast<void*>(end), _vertexSize);
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::begin() const -> const_iterator
{
	std::uintptr_t end = reinterpret_cast<std::uintptr_t>(_begin.get())
		                 + _size * _vertexSize;

	return const_iterator(_begin.get(), reinterpret_cast<void*>(end), _vertexSize);
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::end() -> iterator
{
	std::uintptr_t end = reinterpret_cast<std::uintptr_t>(_begin.get())
		                 + _size * _vertexSize;

	return iterator(
		reinterpret_cast<void*>(end),
		reinterpret_cast<void*>(end),
		_vertexSize
	);
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::end() const -> const_iterator
{
	std::uintptr_t end = reinterpret_cast<std::uintptr_t>(_begin.get())
		                 + _size * _vertexSize;

	return const_iterator(reinterpret_cast<void*>(end),
						  reinterpret_cast<void*>(end),
						  _vertexSize);
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::empty() const -> bool
{
	return size() == 0;
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::size() const -> std::size_t
{
	return end() - begin();
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::clear() -> void
{
	_size = 0;
}

template <VertexFormat VF>
inline auto VertexList<VertexFormat::DYNAMIC_TAG>::push_back(const Vertex<VF>& vertex) -> void
{
	ASSERT(Vertex<VF>::FORMAT_DATA == _fmtData, "Incorrect vertex format.");

	reserve(++_size);

	void* out = reinterpret_cast<void*>(reinterpret_cast<std::uintptr_t>(_begin.get()) + (_size - 1) * _vertexSize);

	std::memcpy(out, &vertex, sizeof(Vertex<VF>));
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::push_back(const void* vertex) -> void
{
	reserve(++_size);

	void* out = reinterpret_cast<void*>(reinterpret_cast<std::uintptr_t>(_begin.get()) + (_size - 1) * _vertexSize);

	std::memcpy(out, vertex, _fmtData.size());
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::pop_back() -> void
{
	ASSERT(_size > 0, "Trying to pop empty vertex list.");
	--_size;
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::resize(std::size_t size) -> void
{
	reserve(size);
	_size = size;
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::reserve(std::size_t size) -> void
{
	if (_capacity >= size || size == 0)
		return;

	void* newBegin = std::realloc(_begin.release(), size * _vertexSize);

	if (!newBegin)
		throw std::bad_alloc();

	_capacity = size;

	_begin.reset(newBegin);
}

inline auto VertexList<VertexFormat::DYNAMIC_TAG>::formatData() const -> VertexFormatData
{
	return _fmtData;
}

} }
