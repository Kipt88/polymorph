#include "gr/BufferProvider.hpp"

namespace pm { namespace gr {

template <VertexFormat VF>
VertexSubBufferView BufferProvider::getVertexBuffer(
	const VertexList<VF>& vl,
	BufferUsage usage
)
{
	return getVertexBuffer(vl.formatData(), vl.size(), usage, vl.data());
}


template <typename InputIt>
std::vector<IndexSubBufferView> BufferProvider::getIndexBuffers(
	BufferUsage usage,
	InputIt sizeDataBegin,
	InputIt sizeDataEnd
)
{
	if (std::distance(sizeDataBegin, sizeDataEnd) == 0)
		return {};

	std::vector<std::pair<std::size_t, const index_t*>> bufferData;

	std::size_t totalSize = 0;

	for (auto it = sizeDataBegin; it != sizeDataEnd; ++it)
	{
		const auto& indices = it->indices;

		totalSize += indices.size();

		bufferData.push_back(std::make_pair(indices.size(), indices.data()));
	}

	auto&& indexBuffer = getIndexBuffer(usage, totalSize);

	std::vector<IndexSubBufferView> bufferViews;
	bufferViews.reserve(bufferData.size());

	for (auto it = bufferData.begin(); it != bufferData.end() - 1; ++it)
	{
		auto&& newPartition = indexBuffer.partition(it->first);

		newPartition.write(0, it->first, it->second);
		
		bufferViews.push_back(std::move(newPartition));
	}

	ASSERT(indexBuffer.size() == bufferData.back().first, "Incorrect buffer partitioning.");

	indexBuffer.write(0, bufferData.back().first, bufferData.back().second);
	
	bufferViews.push_back(std::move(indexBuffer));

	return bufferViews;
}

} }
