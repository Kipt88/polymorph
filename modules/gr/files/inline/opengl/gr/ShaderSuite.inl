#include "gr/Shader.hpp"

namespace pm { namespace gr {

namespace detail { 

inline std::string getLinkLog(GLuint program)
{
	GLint logLen;

	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLen);

	if (logLen == 0)
		return "";

	std::string log;
	log.resize(logLen);

	glGetProgramInfoLog(program, logLen, nullptr, &log[0]);

	return log;
}

}

template <typename InputIt>
ShaderSuite::ShaderSuite(InputIt shadersBegin, InputIt shadersEnd)
: _program(0),
  _shaders()
{
	_program = glCreateProgram();

	if (_program == 0)
		throw ShaderException();

	for (auto it = shadersBegin; it != shadersEnd; ++it)
	{
		const ShaderHandle& shaderHandle = *it;

		glAttachShader(_program, shaderHandle->glHandle());
		_shaders.push_back(shaderHandle);
	}

	glLinkProgram(_program);

	GLint success;
	glGetProgramiv(_program, GL_LINK_STATUS, &success);

	if (success != GL_TRUE)
	{
		auto&& log = detail::getLinkLog(_program);

		glDeleteProgram(_program);

		throw ShaderLinkingException(log);
	}
}

} }
