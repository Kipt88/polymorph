#include "gr/Mesh.hpp"

#include <Assert.hpp>

namespace pm { namespace gr {

namespace detail {

template <VertexFormat VF>
math::AxisAlignedBox3<real_t> createBounds(const VertexList<VF>& vertices)
{
	Vector3_r min;
	Vector3_r max;

	const auto& vertexData = vertices.formatData();

	if (vertices.empty() || vertexData.vertDim == 0)
		return math::AxisAlignedBox3<real_t>();

	const real_t* first = 
		reinterpret_cast<const real_t*>(
			reinterpret_cast<std::uintptr_t>(
				vertices[0]
			) + vertexData.vertOffset()
		);

	min[0] = max[0] = first[0];

	if (vertexData.vertDim >= 2)
	{
		min[1] = max[1] = first[1];

		if (vertexData.vertDim >= 3)
			min[2] = max[2] = first[2];
	}

	for (unsigned int i = 1; i < vertices.size(); ++i)
	{
		const real_t* v = 
			reinterpret_cast<const real_t*>(
				reinterpret_cast<std::uintptr_t>(
					vertices[i]
				) + vertexData.vertOffset()
			);
		
		for (unsigned int r = 0; r < vertexData.vertDim; ++r)
		{
			if (v[r] < min[r])
				min[r] = v[r];

			if (max[r] < v[r])
				max[r] = v[r];
		}
	}

	return math::AxisAlignedBox3<real_t>(min, max);
}

template <VertexFormat VF>
VertexSubBufferView createVertices(
	const VertexList<VF>& vertices,
	BufferUsage usage,
	BufferProvider& manager
)
{
	return manager.getVertexBuffer(vertices, usage);
}

template <typename FaceList>
Mesh::face_collection_t createFaces(
	FaceList&& faces,
	std::size_t baseIndex,
	BufferUsage usage,
	BufferProvider& manager
)
{
	if (baseIndex != 0)
	{
		for (auto& face : faces)
		{
			for (auto& index : face.indices)
				index += static_cast<index_t>(baseIndex);
		}
	}

	auto&& bufferViews = manager.getIndexBuffers(usage, faces.begin(), faces.end());

	Mesh::face_collection_t rFaces;
	
	ASSERT(faces.size() == bufferViews.size(), "BufferProvider error.");

	for (std::size_t i = 0; i < bufferViews.size(); ++i)
	{
		rFaces.insert(
			std::make_pair(
				faces[i].material,
				Mesh::FaceShape{ std::move(bufferViews[i]), faces[i].primitive }
			)
		);
	}

	return rFaces;
}

}

template <VertexFormat VF, typename FaceList>
Mesh::Mesh(
	const VertexList<VF>& vertices,
	FaceList&& faces,
	BufferUsage usage,
	BufferProvider& manager
)
: _vertices(detail::createVertices(vertices, usage, manager)),
  _faces(detail::createFaces(std::move(faces), _vertices.startIndex(), usage, manager)),
  _bounds(detail::createBounds(vertices))
{
}

} }
