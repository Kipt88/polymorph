#include "gr/ShaderSuite.hpp"

namespace pm { namespace gr {

ShaderSuite::ShaderSuite(ShaderSuite&& other)
{
}

ShaderSuite::~ShaderSuite()
{
}

ShaderSuite::location_t ShaderSuite::getLocation(const std::string& name) const
{
	return 0;
}

void ShaderSuite::set(location_t loc, real_t f) const
{
}

void ShaderSuite::set(location_t loc, const Vector2_r& v) const
{
}

void ShaderSuite::set(location_t loc, const Vector3_r& v) const
{
}

void ShaderSuite::set(location_t loc, const Matrix4x4_r& m) const
{
}

void ShaderSuite::set(location_t loc, int i) const
{
}

ShaderMetaData ShaderSuite::getMetaData() const
{
	ShaderMetaData meta;
	return meta;
}

void ShaderSuite::bind() const
{
}

void ShaderSuite::unbind() const
{
}

} }
