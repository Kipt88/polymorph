#include "gr/Surface.hpp"

#include "gr/SurfaceException.hpp"
#include "gr/vulkan.hpp"

#include <Assert.hpp>
#include <Debug.hpp>

namespace pm { namespace gr {

// Surface::ActivationLock

Surface::ActivationLock::ActivationLock()
: _surface(nullptr)
{
}

Surface::ActivationLock::ActivationLock(const Surface& surface)
: _surface(&surface)
{
	_surface->setActive(true);
}

Surface::ActivationLock::ActivationLock(ActivationLock&& other)
: _surface(other._surface)
{
	other._surface = nullptr;
}

Surface::ActivationLock::~ActivationLock()
{
	if (_surface)
		_surface->setActive(false);
}

Surface::ActivationLock& Surface::ActivationLock::operator=(ActivationLock&& other)
{
	if (_surface)
	{
		_surface->setActive(false);
	}

	_surface = other._surface;
	other._surface = nullptr;

	return *this;
}

// Surface

Surface::Surface(os::InputSystem& input)
{
}

bool Surface::isActive() const
{
	return false;
}

Surface::ActivationLock Surface::activate() const
{
	return ActivationLock(*this);
}

void Surface::setActive(bool activate) const
{
}

void Surface::setVSync(bool enable)
{
}

void Surface::flipBuffers()
{
}

math::AxisAlignedBox2<int> Surface::getScreenRect() const
{
	return math::AxisAlignedBox2<int>(0,0,0,0);
}

Vector2_r Surface::normalizedToWindow(const Vector2_r& v) const
{
	return v;
}

Vector3_r Surface::normalizedToWindow(const Vector3_r& v) const
{
	return v;
}

Vector2_r Surface::windowToNormalized(const Vector2_r& v) const
{
	return v;
}

Vector3_r Surface::windowToNormalized(const Vector3_r& v) const
{
	return v;
}

} }
