#include "gr/VertexFormat.hpp"

#include <Assert.hpp>

namespace pm { namespace gr {

unsigned int VertexFormatData::vertOffset() const
{
	return 0;
}

unsigned int VertexFormatData::colorOffset() const
{
	return vertDim * sizeof(real_t);
}

unsigned int VertexFormatData::textOffset() const
{
	return (vertDim + colorDim) * sizeof(real_t);
}

unsigned int VertexFormatData::normalOffset() const
{
	return (vertDim + colorDim + textDim) * sizeof(real_t);
}

unsigned int VertexFormatData::size() const
{
	return (vertDim + colorDim + textDim + normalDim) * sizeof(real_t);
}

bool operator==(const VertexFormatData& a, const VertexFormatData& b)
{
	return &a == &b 
		   || a.vertDim == b.vertDim
		      && a.colorDim == b.colorDim
		      && a.textDim == b.textDim
		      && a.normalDim == b.normalDim;
}

bool operator!=(const VertexFormatData& a, const VertexFormatData& b)
{
	return !(a == b);
}

const VertexFormatData Vertex<VertexFormat::V2>::FORMAT_DATA       = { 2, 0, 0, 0 };
const VertexFormatData Vertex<VertexFormat::V2_C3>::FORMAT_DATA    = { 2, 3, 0, 0 };
const VertexFormatData Vertex<VertexFormat::V2_C4>::FORMAT_DATA    = { 2, 4, 0, 0 };
const VertexFormatData Vertex<VertexFormat::V2_T2>::FORMAT_DATA    = { 2, 0, 2, 0 };
const VertexFormatData Vertex<VertexFormat::V2_C3_T2>::FORMAT_DATA = { 2, 3, 2, 0 };
const VertexFormatData Vertex<VertexFormat::V2_C4_T2>::FORMAT_DATA = { 2, 4, 2, 0 };

const VertexFormatData Vertex<VertexFormat::V3>::FORMAT_DATA       = { 3, 0, 0, 0 };
const VertexFormatData Vertex<VertexFormat::V3_C3>::FORMAT_DATA    = { 3, 3, 0, 0 };
const VertexFormatData Vertex<VertexFormat::V3_C4>::FORMAT_DATA    = { 3, 4, 0, 0 };
const VertexFormatData Vertex<VertexFormat::V3_T2>::FORMAT_DATA    = { 3, 0, 2, 0 };
const VertexFormatData Vertex<VertexFormat::V3_C3_T2>::FORMAT_DATA = { 3, 3, 2, 0 };
const VertexFormatData Vertex<VertexFormat::V3_C4_T2>::FORMAT_DATA = { 3, 4, 2, 0 };

} }
