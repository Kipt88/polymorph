#include "gr/IndexBuffer.hpp"

namespace pm { namespace gr {

IndexBuffer::IndexBuffer(BufferUsage usage,
                         std::size_t size,
                         const index_t* data)
{
}

IndexBuffer::IndexBuffer(IndexBuffer&& other)
{
}

IndexBuffer::~IndexBuffer()
{
}

IndexBuffer& IndexBuffer::operator=(IndexBuffer&& other)
{
	return *this;
}

void IndexBuffer::write(std::size_t offset, std::size_t size, const index_t* indices)
{
}

std::size_t IndexBuffer::size() const
{
	return 0;
}

std::size_t IndexBuffer::capacity() const
{
	return 0;
}

BufferUsage IndexBuffer::bufferUsage() const
{
	return BufferUsage::STATIC;
}

void IndexBuffer::resize(std::size_t size)
{
}

} }
