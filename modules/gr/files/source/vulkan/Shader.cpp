#include "gr/Shader.hpp"

namespace pm { namespace gr {

Shader::Shader(const std::string& source, ShaderType type)
{
}

Shader::Shader(Shader&& other)
{
}

Shader::~Shader()
{
}

ShaderType Shader::type() const
{
	return ShaderType::VERTEX_SHADER;
}

uint32_t Shader::glHandle() const
{
	return 0;
}

} }
