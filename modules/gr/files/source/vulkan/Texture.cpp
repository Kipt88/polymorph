#include "gr/Texture.hpp"

namespace pm { namespace gr {

Texture::BindLock::BindLock()
{
}

Texture::BindLock::BindLock(const Texture& texture)
{
}

Texture::BindLock::BindLock(BindLock&& other)
{
}

Texture::BindLock::~BindLock()
{
}

Texture::BindLock& Texture::BindLock::operator=(BindLock&& other)
{
	return *this;
}

Texture::Params::Params(Wrap _sWrap, Wrap _tWrap, Zoom _minZoom, Zoom _magZoom)
{
}

Texture::Texture(const Image& source, const Params& texParams, id_t resourceId)
{
}

Texture::Texture(Texture&& other)
{
}

Texture::~Texture()
{
}

Texture::BindLock Texture::bindLock() const
{
	return BindLock(*this);
}

void Texture::bind() const
{
}

void Texture::unbind() const
{
}

id_t Texture::resourceId() const
{
	return 0;
}

} }
