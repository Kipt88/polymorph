#include "gr/VertexBuffer.hpp"

namespace pm { namespace gr {

VertexBuffer::VertexBuffer(const VertexFormatData& formatData,
                           BufferUsage usage,
                           std::size_t size,
                           const void* data)
{
}

VertexBuffer::VertexBuffer(VertexBuffer&& other)
{
}

VertexBuffer::~VertexBuffer()
{
}

VertexBuffer& VertexBuffer::operator=(VertexBuffer&& other)
{
	return *this;
}

void VertexBuffer::write(const void* vertices, std::size_t index, std::size_t size)
{
}

void VertexBuffer::draw(Primitive primitive,
						std::size_t start,
						std::size_t size) const
{
}

void VertexBuffer::draw(Primitive primitive) const
{
}

void VertexBuffer::draw(Primitive primitive,
                        const IndexBuffer& indices) const
{
}

void VertexBuffer::draw(Primitive primitive,
						const IndexBuffer& indices,
						std::size_t offset,
						std::size_t size) const
{
}

std::size_t VertexBuffer::size() const
{
	return 0;
}

std::size_t VertexBuffer::capacity() const
{
	return 0;
}

VertexFormatData VertexBuffer::vertexFormatData() const
{
	return VertexFormatData{};
}

BufferUsage VertexBuffer::bufferUsage() const
{
	return BufferUsage::STATIC;
}

void VertexBuffer::resize(std::size_t size)
{
}

} }
