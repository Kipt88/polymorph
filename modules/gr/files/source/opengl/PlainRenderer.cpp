#include "gr/PlainRenderer.hpp"

namespace pm { namespace gr {

namespace PlainRenderer {

void render(
	Surface& surface,
	const Matrix4x4_r& transform,
	MeshHandle mesh
)
{
	glPushMatrix();

	glMultMatrixf(transform.data());

	for (const auto& face : mesh->faces())
	{
		mesh->vertices().draw(
			face.second.primitive,
			face.second.buffer
		);
	}

	glPopMatrix();
}

}

} }
