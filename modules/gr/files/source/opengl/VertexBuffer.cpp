#include "gr/VertexBuffer.hpp"

#include <Assert.hpp>
#include <Debug.hpp>

namespace pm { namespace gr {

namespace {

const char* TAG = "VertexBuffer";

void setPointers(const VertexFormatData& formatData)
{
	if (formatData.vertDim)
	{
		gl::vertexPointer(
			formatData.vertDim,
			REAL_TYPE,
			formatData.size(),
			reinterpret_cast<const void*>(static_cast<std::uintptr_t>(formatData.vertOffset()))
		);
	}

	if (formatData.colorDim)
	{
		gl::colorPointer(
			formatData.colorDim, 
			REAL_TYPE, 
			formatData.size(), 
			reinterpret_cast<const void*>(static_cast<std::uintptr_t>(formatData.colorOffset()))
		);
	}

	if (formatData.textDim)
	{
		gl::texCoordPointer(
			formatData.textDim, 
			REAL_TYPE, 
			formatData.size(), 
			reinterpret_cast<const void*>(static_cast<std::uintptr_t>(formatData.textOffset()))
		);
	}

	if (formatData.normalDim)
	{
		ASSERT(formatData.normalDim == 3,
		       "Bad normal dimension");

		gl::normalPointer(
			REAL_TYPE, 
			formatData.size(), 
			reinterpret_cast<const void*>(static_cast<std::uintptr_t>(formatData.normalOffset()))
		);
	}
}

void enableVertexFormats(const VertexFormatData& formatData)
{
	if (formatData.vertDim)
		gl::enableClientState(GL_VERTEX_ARRAY);

	if (formatData.colorDim)
		gl::enableClientState(GL_COLOR_ARRAY);

	if (formatData.textDim)
		gl::enableClientState(GL_TEXTURE_COORD_ARRAY);

	if (formatData.normalDim)
		gl::enableClientState(GL_NORMAL_ARRAY);
}

void disableVertexFormats(const VertexFormatData& formatData)
{
	if (formatData.vertDim)
		gl::disableClientState(GL_VERTEX_ARRAY);

	if (formatData.colorDim)
		gl::disableClientState(GL_COLOR_ARRAY);

	if (formatData.textDim)
		gl::disableClientState(GL_TEXTURE_COORD_ARRAY);

	if (formatData.normalDim)
		gl::disableClientState(GL_NORMAL_ARRAY);
}

}

VertexBuffer::VertexBuffer(const VertexFormatData& formatData,
                           BufferUsage usage,
                           std::size_t size,
                           const void* data)
: _bufferID(0),
  _size(size),
  _capacity(size),
  _usage(usage),
  _vertexFormatData(formatData)
{
	ASSERT(_capacity > 0, "Capacity must be greater than zero.");


	gl::genBuffers(1, &_bufferID);

	gl::bindBuffer(GL_ARRAY_BUFFER, _bufferID);
	
	gl::bufferData(
		GL_ARRAY_BUFFER,
		static_cast<GLsizei>(_capacity) * _vertexFormatData.size(),
		data,
		usage
	);

	GLint bufferSize = 0;
	glGetBufferParameteriv(GL_ARRAY_BUFFER,
		                    GL_BUFFER_SIZE,
		                    &bufferSize);

	DEBUG_OUT(TAG, "Allocated VertexBuffer %i: %i bytes.", _bufferID, bufferSize);

	if (bufferSize != static_cast<GLint>(_capacity * _vertexFormatData.size()))
	{
		gl::deleteBuffers(1, &_bufferID);
		gl::bindBuffer(GL_ARRAY_BUFFER, 0);

		throw VertexBufferCreationException("Can't allocate buffer for target size.");
	}

	gl::bindBuffer(GL_ARRAY_BUFFER, 0);
}

VertexBuffer::VertexBuffer(VertexBuffer&& other)
: _bufferID(other._bufferID),
  _size(other._size),
  _capacity(other._capacity),
  _usage(other._usage),
  _vertexFormatData(other._vertexFormatData)
{
	other._bufferID = 0;
}

VertexBuffer::~VertexBuffer()
{
	if (_bufferID != 0)
	{
		gl::deleteBuffers(1, &_bufferID);
		DEBUG_OUT(TAG, "Deallocated VertexBuffer %i", _bufferID);
	}
}

VertexBuffer& VertexBuffer::operator=(VertexBuffer&& other)
{
	if (_bufferID != 0)
	{
		gl::deleteBuffers(1, &_bufferID);
		DEBUG_OUT(TAG, "Deallocated VertexBuffer %i", _bufferID);
	}

	_bufferID = other._bufferID;
	_size = other._size;
	_capacity = other._capacity;
	_usage = other._usage;
	_vertexFormatData = other._vertexFormatData;

	other._bufferID = 0;

	return *this;
}

void VertexBuffer::write(const void* vertices, std::size_t index, std::size_t size)
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");
	ASSERT(index <= _size, "Buffer overflow.");

	gl::bindBuffer(GL_ARRAY_BUFFER, _bufferID);
	gl::bufferSubData(GL_ARRAY_BUFFER,
					  index * _vertexFormatData.size(),
					  size * _vertexFormatData.size(),
					  vertices);
	gl::bindBuffer(GL_ARRAY_BUFFER, 0);
}

void VertexBuffer::draw(Primitive primitive,
						std::size_t start,
						std::size_t size) const
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");

	gl::bindBuffer(GL_ARRAY_BUFFER, _bufferID);

	enableVertexFormats(_vertexFormatData);

	setPointers(_vertexFormatData);

	glDrawArrays(GLenum(primitive), start, size);

	gl::bindBuffer(GL_ARRAY_BUFFER, 0);
	disableVertexFormats(_vertexFormatData);
}

void VertexBuffer::draw(Primitive primitive) const
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");
	draw(primitive, 0, _size);
}

void VertexBuffer::draw(Primitive primitive,
                        const IndexBuffer& indices) const
{
	draw(primitive, indices, 0, indices.size());
}

void VertexBuffer::draw(Primitive primitive,
						const IndexBuffer& indices,
						std::size_t offset,
						std::size_t size) const
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");

	gl::bindBuffer(GL_ARRAY_BUFFER, _bufferID);
	gl::bindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices._bufferID);

	enableVertexFormats(_vertexFormatData);
	setPointers(_vertexFormatData);

	glDrawElements(
		GLenum(primitive),
		size,
		INDEX_TYPE,
		reinterpret_cast<GLvoid*>(offset * sizeof(index_t))
	);

	gl::bindBuffer(GL_ARRAY_BUFFER, 0);
	gl::bindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	disableVertexFormats(_vertexFormatData);
}

std::size_t VertexBuffer::size() const
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");
	return _size;
}

std::size_t VertexBuffer::capacity() const
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");
	return _capacity;
}

const VertexFormatData& VertexBuffer::vertexFormatData() const
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");
	return _vertexFormatData;
}

BufferUsage VertexBuffer::bufferUsage() const
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");
	return _usage;
}

void VertexBuffer::resize(std::size_t size)
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");
	ASSERT(size > 0, "May not resize to 0.");

	if (_capacity >= size)
	{
		_size = size;
		return;
	}
	
	std::unique_ptr<char[]> data(new char[size * _vertexFormatData.size()]);
	
	gl::bindBuffer(GL_ARRAY_BUFFER, _bufferID);
	gl::getBufferSubData(GL_ARRAY_BUFFER, 0, _size * _vertexFormatData.size(), data.get());
	gl::bindBuffer(GL_ARRAY_BUFFER, 0);
	
	VertexBuffer tempBuffer(_vertexFormatData, _usage, size, data.get());
	
	/* This is faster but can't get it to work on MacOS:
	gl::bindBuffer(GL_COPY_READ_BUFFER, _bufferID);
	gl::bindBuffer(GL_COPY_WRITE_BUFFER, tempBuffer._bufferID);

	glCopyBufferSubData(
		GL_COPY_READ_BUFFER, 
		GL_COPY_WRITE_BUFFER, 
		0, 
		0, 
		_capacity * _vertexFormatData.size()
	);

	gl::bindBuffer(GL_COPY_READ_BUFFER, 0);
	gl::bindBuffer(GL_COPY_WRITE_BUFFER, 0);
	*/

	*this = std::move(tempBuffer);
}

} }
