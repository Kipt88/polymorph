#include "gr/Shader.hpp"

#include <Assert.hpp>

namespace pm { namespace gr {

namespace {

const char* TAG = "Shader";

std::string getShaderLog(GLuint shader)
{
	GLint logLength;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);

	if (logLength == 0)
		return "";

	std::string log;
	log.resize(logLength);

	glGetShaderInfoLog(
		shader,
		logLength,
		nullptr,
		&log[0]
	);

	return log;
}

GLuint createShader(GLenum shaderType, const std::string& source)
{
	GLuint rShader = glCreateShader(shaderType);

	if (rShader == 0)
		throw ShaderException();

	const char* src = source.c_str();
	glShaderSource(rShader, 1, &src, nullptr);
	glCompileShader(rShader);

	GLint success;
	glGetShaderiv(rShader, GL_COMPILE_STATUS, &success);

	DEBUG_OUT(TAG, "Compilation log:\n%s", getShaderLog(rShader).c_str());

	if (success != GL_TRUE)
	{
		GLint logLength;
		glGetShaderiv(rShader, GL_INFO_LOG_LENGTH, &logLength);

		auto&& log = getShaderLog(rShader);

		glDeleteShader(rShader);

		throw ShaderCompilationException(log);
	}

	return rShader;
}

}

Shader::Shader(const std::string& source, ShaderType type)
: _shader(createShader(static_cast<GLint>(type), source)),
  _type(type)
{
}

Shader::Shader(Shader&& other)
: _shader(other._shader),
  _type(other._type)
{
	other._shader = 0;
}

Shader::~Shader()
{
	if (_shader)
		glDeleteShader(_shader);
}

ShaderType Shader::type() const
{
	return _type;
}

GLuint Shader::glHandle() const
{
	return _shader;
}

} }