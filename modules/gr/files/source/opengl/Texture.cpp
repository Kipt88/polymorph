#include "gr/Texture.hpp"
#include "gr/GraphicsException.hpp"

#include <Assert.hpp>

namespace pm { namespace gr {

// Texture::BindLock

Texture::BindLock::BindLock()
: _texture(nullptr)
{
}

Texture::BindLock::BindLock(const Texture& texture)
: _texture(&texture)
{
	_texture->bind();
}

Texture::BindLock::BindLock(BindLock&& other)
: _texture(other._texture)
{
	other._texture = nullptr;
}

Texture::BindLock::~BindLock()
{
	if (_texture)
		_texture->unbind();
}

Texture::BindLock& Texture::BindLock::operator=(BindLock&& other)
{
	if (_texture)
		_texture->unbind();

	_texture = other._texture;
	other._texture = nullptr;

	return *this;
}

// Texture::Params

Texture::Params::Params(Wrap _sWrap, Wrap _tWrap, Zoom _minZoom, Zoom _magZoom)
: sWrap(_sWrap),
  tWrap(_tWrap),
  minZoom(_minZoom),
  magZoom(_magZoom)
{
}

// Texture

Texture::Texture(const Image& source, const Params& texParams, id_t resourceId)
: _texID(0),
  _resourceId(resourceId)
{
	ASSERT(glGetError() == GL_NO_ERROR, "Unhandled OpenGL error prior to this.");

	glGenTextures(1, &_texID);
	if (_texID == 0)
		throw GraphicsException();

	glPixelStorei(GL_UNPACK_ALIGNMENT, source.rowAlignment());

	glBindTexture(GL_TEXTURE_2D, _texID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, 
					static_cast<GLint>(texParams.sWrap));
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, 
					static_cast<GLint>(texParams.tWrap));
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, 
					static_cast<GLint>(texParams.magZoom));
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, 
					static_cast<GLint>(texParams.minZoom));

	glTexImage2D(GL_TEXTURE_2D, 
				 0, 
				 GL_RGBA,
	             source.width(), 
				 source.height(),
	             0, 
				 static_cast<GLint>(source.pixelFormat()),
	             GL_UNSIGNED_BYTE, 
				 source.data());

	glBindTexture(GL_TEXTURE_2D, 0);

	auto err = glGetError();
	ASSERT(err == GL_NO_ERROR || err == GL_OUT_OF_MEMORY, "Programming error.");

	if (err == GL_OUT_OF_MEMORY)
		throw GraphicsException();
}

Texture::Texture(Texture&& other)
: _texID(other._texID),
  _resourceId(other._resourceId)
{
	other._texID = 0;
	other._resourceId = 0;
}

Texture::~Texture()
{
	if (_texID != 0)
		glDeleteTextures(1, &_texID);
}

Texture::BindLock Texture::bindLock() const
{
	return BindLock(*this);
}

void Texture::bind() const
{
	ASSERT(_texID != 0, "Trying to bind invalid texture.");
	glBindTexture(GL_TEXTURE_2D, _texID);
}

void Texture::unbind() const
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

id_t Texture::resourceId() const
{
	return _resourceId;
}

} }