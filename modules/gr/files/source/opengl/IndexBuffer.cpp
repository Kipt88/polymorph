#include "gr/IndexBuffer.hpp"

#include "gr/GraphicsException.hpp"
#include "gr/opengl.hpp"

#include <Assert.hpp>
#include <Debug.hpp>

namespace pm { namespace gr {

namespace 
{
	const char* TAG = "IndexBuffer";
}

IndexBuffer::IndexBuffer(BufferUsage usage,
        	             std::size_t size,
        	             const index_t* data)
: _bufferID(0),
  _usage(usage),
  _size(size),
  _capacity(size)
{
	ASSERT(size > 0, "Size must be greater than zero.");

	gl::genBuffers(1, &_bufferID);
	gl::bindBuffer(GL_ELEMENT_ARRAY_BUFFER, _bufferID);
	
	gl::bufferData(GL_ELEMENT_ARRAY_BUFFER,
				   _capacity * sizeof(index_t),
				   data,
				   usage);

	GLint bufferSize = 0;
	glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &bufferSize);

	DEBUG_OUT(TAG, "Allocated IndexBuffer %i: %i bytes.", _bufferID, bufferSize);

	if ( bufferSize != static_cast<GLint>(_capacity * sizeof(index_t)) )
	{
		gl::deleteBuffers(1, &_bufferID);
		gl::bindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		throw IndexBufferCreationException();
	}

	gl::bindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

IndexBuffer::IndexBuffer(IndexBuffer&& other)
: _bufferID(other._bufferID),
  _usage(other._usage),
  _size(other._size),
  _capacity(other._capacity)
{
	other._bufferID = 0;
}

IndexBuffer::~IndexBuffer()
{
	if (_bufferID != 0)
	{
		gl::deleteBuffers(1, &_bufferID);
		DEBUG_OUT(TAG, "Deallocated IndexBuffer %i", _bufferID);
	}
}

IndexBuffer& IndexBuffer::operator=(IndexBuffer&& other)
{
	if (_bufferID != 0)
	{
		gl::deleteBuffers(1, &_bufferID);
		DEBUG_OUT(TAG, "Deallocated IndexBuffer %i", _bufferID);
	}

	_bufferID = other._bufferID;
	_size = other._size;
	_capacity = other._capacity;
	_usage = other._usage;
	
	other._bufferID = 0;

	return *this;
}

void IndexBuffer::write(std::size_t offset, std::size_t size, const index_t* indices)
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");
	ASSERT(offset + size <= _size, "Buffer overflow.");

	gl::bindBuffer(GL_ELEMENT_ARRAY_BUFFER, _bufferID);
	gl::bufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset * sizeof(index_t), size * sizeof(index_t), indices);
	gl::bindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

std::size_t IndexBuffer::size() const
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");
	return _size;
}

std::size_t IndexBuffer::capacity() const
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");
	return _capacity;
}

BufferUsage IndexBuffer::bufferUsage() const
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");
	return _usage;
}

void IndexBuffer::resize(std::size_t size)
{
	ASSERT(_bufferID != 0, "Trying to use dead buffer.");

	if (_capacity >= size)
	{
		_size = size;
		return;
	}

	
	std::unique_ptr<index_t[]> data(new index_t[size]);
	
	gl::bindBuffer(GL_ARRAY_BUFFER, _bufferID);
	
	glGetBufferSubDataARB(GL_ARRAY_BUFFER, 0, size * sizeof(index_t), data.get());
	
	gl::bindBuffer(GL_ARRAY_BUFFER, 0);

	IndexBuffer tempBuffer(_usage, size);
	
	/*
	gl::bindBuffer(GL_COPY_READ_BUFFER, _bufferID);
	gl::bindBuffer(GL_COPY_WRITE_BUFFER, tempBuffer._bufferID);

	glCopyBufferSubData(
		GL_COPY_READ_BUFFER,
		GL_COPY_WRITE_BUFFER,
		0,
		0,
		_capacity * sizeof(index_t)
	);

	gl::bindBuffer(GL_COPY_READ_BUFFER, 0);
	gl::bindBuffer(GL_COPY_WRITE_BUFFER, 0);
	*/
	
	*this = std::move(tempBuffer);
}

} }
