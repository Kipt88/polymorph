#include "gr/TextureRenderer.hpp"

namespace pm { namespace gr {

namespace TextureRenderer {

void render(
	Surface& surface, 
	const Matrix4x4_r& transform, 
	MeshHandle mesh
)
{
	glPushMatrix();

	glMultMatrixf(transform.data());

	for (const auto& face : mesh->faces())
	{
		auto textureLock = face.first->textures.front()->bindLock();

		mesh->vertices().draw(
			face.second.primitive,
			face.second.buffer
		);
	}

	glPopMatrix();
}

}

} }
