#include "gr/ShaderSuite.hpp"

namespace pm { namespace gr {

ShaderSuite::ShaderSuite(ShaderSuite&& other)
: _shaders(std::move(other._shaders)),
  _program(other._program)
{
	other._program = 0;
}

ShaderSuite::~ShaderSuite()
{
	if (_program)
		glDeleteProgram(_program);
}

ShaderSuite::location_t ShaderSuite::getLocation(const std::string& name) const
{
	auto loc = glGetUniformLocation(_program, name.c_str());
	ASSERT(glGetError() == GL_NO_ERROR, "Expected no error.");

	if (loc == -1)
		throw NoSuchUniformException();

	return loc;
}

void ShaderSuite::set(location_t loc, real_t f) const
{
	glProgramUniform1f(_program, loc, f);
	ASSERT(glGetError() == GL_NO_ERROR, "Expected no error.");
}

void ShaderSuite::set(location_t loc, const Vector2_r& v) const
{
	glProgramUniform2f(_program, loc, v[0], v[1]);
	ASSERT(glGetError() == GL_NO_ERROR, "Expected no error.");
}

void ShaderSuite::set(location_t loc, const Vector3_r& v) const
{
	glProgramUniform3f(_program, loc, v[0], v[1], v[2]);
	ASSERT(glGetError() == GL_NO_ERROR, "Expected no error.");
}

void ShaderSuite::set(location_t loc, const Matrix4x4_r& m) const
{
	glProgramUniformMatrix4fv(_program, loc, 1, false, m.data());
	ASSERT(glGetError() == GL_NO_ERROR, "Expected no error.");
}

void ShaderSuite::set(location_t loc, int i) const
{
	glProgramUniform1i(_program, loc, i);
	ASSERT(glGetError() == GL_NO_ERROR, "Expected no error.");
}

ShaderMetaData ShaderSuite::getMetaData() const
{
	ShaderMetaData meta;

	{
		GLint activeUniformCount = 0;

		glGetProgramiv(_program, GL_ACTIVE_UNIFORMS, &activeUniformCount);

		meta.variables.reserve(activeUniformCount);

		for (GLuint i = 0; i < static_cast<GLuint>(activeUniformCount); ++i)
		{
			ShaderVariableData data;

			{
				GLint nameSize;
				glGetActiveUniformsiv(_program, 1, &i, GL_UNIFORM_NAME_LENGTH, &nameSize);

				data.name.resize(nameSize);

				glGetActiveUniformName(_program, i, nameSize, nullptr, &data.name[0]);
				data.name.pop_back(); // Removes trailing '\0'.
			}

			{
				GLint type;

				glGetActiveUniformsiv(_program, 1, &i, GL_UNIFORM_TYPE, &type);

				data.type = static_cast<TypeTag>(type);
			}

			{
				GLint blockIndex;

				glGetActiveUniformsiv(_program, 1, &i, GL_UNIFORM_BLOCK_INDEX, &blockIndex);

				data.blockIndex = blockIndex;
			}

			{
				GLint offset;

				glGetActiveUniformsiv(_program, 1, &i, GL_UNIFORM_OFFSET, &offset);

				data.offset = offset;
			}

			meta.variables.push_back(std::move(data));
		}
	}

	{
		GLint activeBlockCount = 0;

		glGetProgramiv(_program, GL_ACTIVE_UNIFORM_BLOCKS, &activeBlockCount);

		for (GLuint i = 0; i < static_cast<GLuint>(activeBlockCount); ++i)
		{
			ShaderBlockData data;

			{
				GLint nameSize;
				glGetActiveUniformBlockiv(_program, i, GL_UNIFORM_BLOCK_NAME_LENGTH, &nameSize);

				data.name.resize(nameSize);

				glGetActiveUniformBlockName(_program, i, nameSize, nullptr, &data.name[0]);
				data.name.pop_back(); // Removes trailing '\0'.
			}

			meta.blocks.push_back(std::move(data));
		}
	}

	return meta;
}

void ShaderSuite::bind() const
{
	glUseProgram(_program);
}

void ShaderSuite::unbind() const
{
	glUseProgram(0);
}

} }