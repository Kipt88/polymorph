#include "gr/ImageReader_PNG.hpp"

#include "gr/ImageReader.hpp"

#include <gr/PixelFormat.hpp>

#include <Debug.hpp>

#include <png.h>

#include <cstdio>
#include <memory>

namespace pm { namespace gr {

namespace {

const unsigned int PNG_HEADER_CHECK_SIZE = 8;
const char* TAG = "ImageReader_PNG";

bool isPNGFile(io::InputStream& input)
{
	unsigned char header[PNG_HEADER_CHECK_SIZE];

	io::readBlock(input, header, PNG_HEADER_CHECK_SIZE);

	return !png_sig_cmp(header, 0, PNG_HEADER_CHECK_SIZE);
}

void ioRead(png_structp pngStruct, png_bytep bytes, png_size_t size)
{
	auto* inputPtr = reinterpret_cast<io::InputStream*>(png_get_io_ptr(pngStruct));

	try
	{
		io::readBlock(*inputPtr, bytes, size);
	}
	catch (io::IOException& e)
	{
		ERROR_OUT(TAG, "PNG read failed, %s.", e.what());
		png_error(pngStruct, "ioRead exception.");
	}
}

typedef unsigned char pixel_data_t;

}

Image ImageReader_PNG::read(io::InputStream& input)
{
	INFO_OUT(TAG, "Loading image.");

	try
	{
		if (!isPNGFile(input))
		{
			ERROR_OUT(TAG, "Input is not png file.");
			throw UnknownImageFormat();
		}

		png_structp pngStruct = png_create_read_struct(PNG_LIBPNG_VER_STRING,
													   nullptr,
													   nullptr,
													   nullptr);

		if (!pngStruct)
		{
			ERROR_OUT(TAG, "Out of memory while creating read struct.");
			throw ImageReadException();
		}

		png_infop pngInfo = png_create_info_struct(pngStruct);
		if (!pngInfo)
		{
			png_destroy_read_struct(&pngStruct, nullptr, nullptr);
			
			ERROR_OUT(TAG, "Out of memory while creating info struct.");
			throw ImageReadException();
		}

		if (setjmp(png_jmpbuf(pngStruct)))
		{
			png_destroy_read_struct(&pngStruct, &pngInfo, nullptr);
			
			ERROR_OUT(TAG, "Error while reading data.");
			throw ImageReadException();
		}

		png_set_read_fn(pngStruct, &input, ioRead);

		png_set_sig_bytes(pngStruct, PNG_HEADER_CHECK_SIZE);

		// FIXME Write directly to buffer, don't copy.

		png_read_png(pngStruct,
					 pngInfo,
					 PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND,
					 NULL);

		struct
		{
			png_uint_32 width;
			png_uint_32 height;
			png_byte bitsPerPixel;
			png_byte colorType;
			png_size_t rowSize;
		} imageData;

		imageData.width = png_get_image_width(pngStruct, pngInfo);
		imageData.height = png_get_image_height(pngStruct, pngInfo);
		imageData.bitsPerPixel = png_get_bit_depth(pngStruct, pngInfo);
		imageData.colorType = png_get_color_type(pngStruct, pngInfo);
		imageData.rowSize = png_get_rowbytes(pngStruct, pngInfo);

		// FIXME Let PixelFormat decide what format is suitable.

		gr::PixelFormat fmt;
		if (imageData.colorType == PNG_COLOR_TYPE_RGBA)
			fmt = gr::PixelFormat::R8G8B8A8;
		else
			fmt = gr::PixelFormat::R8G8B8;

		std::unique_ptr<pixel_data_t[]> pixels(new pixel_data_t[imageData.rowSize 
											                    * imageData.height]);
		png_bytepp rowPointers = png_get_rows(pngStruct, pngInfo);

		for (png_uint_32 i = 0; i < imageData.height; ++i)
		{
			memcpy(pixels.get() + (imageData.rowSize * (imageData.height - 1 - i)),
				   rowPointers[i],
				   imageData.rowSize);
		}

		png_destroy_read_struct(&pngStruct, &pngInfo, NULL);

		return gr::Image(static_cast<unsigned int>(imageData.width),
						 static_cast<unsigned int>(imageData.height),
						 fmt,
						 1,
						 std::move(pixels));
	}
	catch (io::IOException& e)
	{
		ERROR_OUT(TAG, "Error reading input, %s.", e.what());
		
		throw ImageReadException();
	}
}


} }
