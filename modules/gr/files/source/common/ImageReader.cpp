#include "gr/ImageReader.hpp"

#include "gr/ImageReader_PNG.hpp"

#include <array>

namespace pm { namespace gr {

namespace {

ImageReader::Reader getReader(ImageReader::image_t imageType)
{
	static std::array<ImageReader::Reader, 1> readers =
	{
		{ImageReader_PNG::read}
	};

	return readers[static_cast<std::size_t>(imageType)];
}

}

gr::Image ImageReader::read(io::InputStream& input, image_t imageType)
{
	return getReader(imageType)(input);
}

} }
