#include "gr/IndexSubBufferView.hpp"

namespace pm { namespace gr {

IndexSubBufferView::IndexSubBufferView(IndexSubBufferViewHandler& handler,
									   IndexBufferHandle buffer,
									   std::size_t startIndex,
									   std::size_t size)
: _handler(&handler),
  _buffer(buffer),
  _startIndex(startIndex),
  _size(size)
{
}

IndexSubBufferView::IndexSubBufferView(IndexSubBufferView&& other)
: _handler(other._handler),
  _buffer(other._buffer),
  _startIndex(other._startIndex),
  _size(other._size)
{
	other._handler = nullptr;
}

IndexSubBufferView::~IndexSubBufferView()
{
	if (_handler)
		_handler->releaseIndexSubBufferView(*this);
}

IndexSubBufferView& IndexSubBufferView::operator=(IndexSubBufferView&& other)
{
	_handler = other._handler;
	_buffer = other._buffer;
	_startIndex = other._startIndex;
	_size = other._size;

	other._handler = nullptr;

	return *this;
}

void IndexSubBufferView::write(std::size_t offset, 
							   std::size_t size, 
							   const index_t* indices)
{
	ASSERT(offset + size <= _size, "Buffer overflow.");

	_buffer->write(_startIndex + offset, size, indices);
}

IndexSubBufferView IndexSubBufferView::partition(std::size_t size)
{
	ASSERT(size < _size, "Trying to partition bigger buffer view.");

	auto&& newBufferView = IndexSubBufferView(*_handler, 
											  _buffer, 
											  _startIndex, 
											  size);

	_size -= size;
	_startIndex += size;

	return std::move(newBufferView);
}

const IndexBufferHandle& IndexSubBufferView::buffer() const
{
	return _buffer;
}

std::size_t IndexSubBufferView::startIndex() const
{
	return _startIndex;
}

std::size_t IndexSubBufferView::size() const
{
	return _size;
}

} }