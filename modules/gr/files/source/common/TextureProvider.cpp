#include "gr/TextureProvider.hpp"

#include "gr/ImageReader.hpp"

#include <gr/Image.hpp>
#include <gr/Texture.hpp>

#include <filesystem/FileInputStream.hpp>

#include <Id.hpp>
#include <Assert.hpp>
#include <Debug.hpp>
#include <Scoped.hpp>

#include <set>
#include <algorithm>

namespace pm { namespace gr {

namespace {

const char* LOG_TAG = "TextureProvider";

/*
void removeUnusedTextures(std::size_t bytesToFree,
							TextureProvider::TextureCache& cache)
{
	auto count = cache.erase_if(
		[&](const std::pair<const TextureProvider::image_id_t, gr::TextureHandle>& handle) -> bool
		{
			bool willRemove = handle.second.unique();

			if (willRemove)
			{
				// TODO Update _memoryAllocated
			}

			return willRemove;
		}
	);

	if (count == 0)
	{
		ERROR_OUT(LOG_TAG,
					"Didn't manage to free required texture memory.");
	}
}

std::map<TextureProvider::image_id_t, gr::TextureHandle> filterLoadedTextures(
		std::set<TextureProvider::image_id_t>& ids,
		const TextureProvider::TextureCache& cache)
{
	std::map<TextureProvider::image_id_t, gr::TextureHandle> rMap;

	for (auto it = ids.begin(); it != ids.end();)
	{
		if (cache.has(*it))
		{
			rMap.insert(std::make_pair(*it, cache.find(*it)->second));
			ids.erase(it++);
		}
		else
		{
			++it;
		}
	}
		
	return rMap;
}
*/

}

auto TextureProvider::getImagesMetaData() -> std::map<image_id_t, TextureProvider::ImageMetaData>&
{
	static std::map<image_id_t, ImageMetaData> images;
	
	return images;
}


TextureProvider::TextureProvider(const path_t& root)
: _root(root)
{
}

TextureHandle TextureProvider::loadTexture(image_id_t id, const Texture::Params& params)
{
	{
		auto handle = _textures.find(id);

		if (handle)
			_textures.erase(handle);
	}

	const auto& imageData = getImagesMetaData();
	auto it = imageData.find(id);

	ASSERT(it != imageData.end(), "Image data not registered.");

	const auto& path = it->second.path;
	const auto& type = it->second.imageType;

	filesystem::FileInputStream input(path.string());

	auto&& image = ImageReader::read(input, type);

	TextureHandle textureHandle = std::make_shared<gr::Texture>(image, params, id);

	_textures.insert(std::make_pair(id, textureHandle));

	return textureHandle;
}

TextureHandle TextureProvider::get(image_id_t id) const
{
	if (_textures.has(id))
		return _textures.find(id)->second;
	else
		return nullptr;
}

TextureHandle TextureProvider::acquire(image_id_t id, const Texture::Params& params)
{
	auto handle = get(id);

	if (!handle)
		handle = loadTexture(id, params);

	return handle;
}

TextureProvider::image_id_t TextureProvider::registerImage(image_id_t id, const path_t& path, image_t typeId)
{
	auto& images = getImagesMetaData();

	ASSERT(images.find(id) == images.end(), "Trying to ovewrite image meta data.");

	images.insert(std::make_pair(id, ImageMetaData{ path, typeId }));

	return id;
}

} }
