#include "gr/MeshReader_OBJ.hpp"

#include "gr/MeshReader.hpp"

#include <gr/VertexList.hpp>
#include <gr/types.hpp>
#include <gr/VertexFormat.hpp>

#include <Debug.hpp>

#include <tuple>
#include <string>
#include <map>
#include <algorithm>
#include <cctype>

namespace pm { namespace gr {

namespace MeshReader_OBJ {

namespace {
	
const char* TAG = "MeshReader_OBJ";

struct VertexReference
{
	unsigned long vertex;
	unsigned long texture;
	unsigned long normal;
};

bool operator==(const VertexReference& a, const VertexReference& b)
{
	return a.vertex == b.vertex
		   && a.texture == b.texture
		   && a.normal == b.normal;
}

struct FileData
{
	FileData() = delete;

	gr::VertexList<gr::VertexFormat::DYNAMIC_TAG> vertices;
	std::vector<std::vector<gr::index_t>> faces;
};

struct VertexFileData
{
	std::vector<gr::real_t> vertices;
	std::vector<gr::real_t> textureCoords;
	std::vector<gr::real_t> normals;
	std::size_t size;
	gr::VertexFormatData formatData;
};

bool operator<(const VertexReference& a, const VertexReference& b)
{
	if (a.vertex < b.vertex)
		return true;

	if (a.texture < b.texture)
		return true;

	return a.normal < b.normal;
}

std::string readLine(io::InputStream& input)
{
	std::string line = "";

	char c;

	while (input.available() > 0)
	{
		input >> c;

		if (c == '\r')
			continue;
		else if (c == '\n')
			break;
		else
			line += c;
	}

	return line;
}

template <typename Callable>
void forEachFloat(const std::string& str, Callable&& action)
{
	std::size_t i = 0;

	auto isDigitPart = [](char c)
	{
		return std::isdigit(c) || c == '-';
	};

	for (auto it = std::find_if(str.begin() + i, str.end(), isDigitPart);
	     it != str.end();
	     it = std::find_if(str.begin() + i, str.end(), isDigitPart))
	{
		action(std::stof(str.substr(it - str.begin()), &i));

		i += it - str.begin();
	}
}

void appendVector(const std::string& line, std::vector<gr::real_t>& vertices)
{
	forEachFloat(line, [&](float f) { vertices.push_back(f); });
}

unsigned long normalizedIndex(long index, std::size_t numVertices)
{
	long rIndex = index;

	if (rIndex == 0)
	{
		ERROR_OUT(TAG, "Invalid index 0.");
		throw MeshReadException();
	}
	else if (rIndex < 0)
	{
		rIndex += numVertices;
	}

	if (rIndex <= 0)
	{
		ERROR_OUT(TAG, "Index too low, %l", index);
		throw MeshReadException();
	}
	else
	{
		--rIndex;

		if (static_cast<std::size_t>(rIndex) >= numVertices)
		{
			ERROR_OUT(TAG, "Index too high, %l", index);
			throw MeshReadException();
		}
	}

	return static_cast<unsigned long>(rIndex);
}

VertexReference readVertexRef(const std::string& str, std::size_t numVertices)
{
	auto isDigitPart = [](char c)
	{
		return std::isdigit(c) || c == '-';
	};

	auto digitBegin = std::find_if(str.begin(), str.end(), isDigitPart);
	auto slashBegin = std::find(str.begin(), str.end(), '/');

	if (digitBegin == str.end()
		|| digitBegin >= slashBegin)
	{
		throw MeshReadException();
	}

	VertexReference vertexReference = { 0, 0, 0 };

	vertexReference.vertex = 
		normalizedIndex(
			std::stol(
				std::string(digitBegin, slashBegin)
			),
			numVertices
		);

	if (slashBegin == str.end())
		return vertexReference;

	auto lastSlash = slashBegin;
	slashBegin = std::find(lastSlash + 1, str.end(), '/');

	if (lastSlash + 1 != slashBegin)
	{
		vertexReference.texture =
			normalizedIndex(
				std::stol(
					std::string(lastSlash + 1, slashBegin)
				), 
				numVertices
			);
	}

	if (slashBegin == str.end())
		return vertexReference;

	vertexReference.normal = 
		normalizedIndex(
			std::stol(
				std::string(slashBegin + 1, str.end())
			), 
			numVertices
		);

	return vertexReference;
}

std::vector<VertexReference> readFace(const std::string& line, std::size_t numVertices)
{
	std::vector<VertexReference> parsed;

	auto isSpace = [](char c) { return std::isspace(c); };

	auto itBegin = std::find_if(line.begin(), line.end(), isSpace);

	if (itBegin == line.end())
		throw MeshReadException();

	for (auto itEnd = std::find_if(itBegin + 1, line.end(), isSpace);
	     itBegin != line.end();
		 itEnd = itEnd == line.end() ? line.end() : std::find_if(itEnd + 1, line.end(), isSpace), 
		 itBegin = std::find_if(itBegin + 1, line.end(), isSpace))
	{
		parsed.push_back(readVertexRef(std::string(itBegin + 1, itEnd), numVertices));
	}

	return parsed;
}

bool isComment(const std::string& line)
{
	if (line.size() < 1)
		return false;

	return line[0] == '#';
}

bool isGroup(const std::string& line)
{
	if (line.size() == 1 && line[0] == 'g')
		return true;

	if (line.size() < 2)
		return false;

	return line[0] == 'g' && std::isspace(line[1]);
}

bool isSmoothingGroup(const std::string& line)
{
	if (line.size() < 2)
		return false;

	return line[0] == 's' && std::isspace(line[1]);
}

bool isVertex(const std::string& line)
{
	if (line.size() < 2)
		return false;

	return line[0] == 'v' && std::isspace(line[1]);
}

bool isTextureCoord(const std::string& line)
{
	if (line.size() < 3)
		return false;

	return line[0] == 'v' && line[1] == 't' && std::isspace(line[2]);
}

bool isNormal(const std::string& line)
{
	if (line.size() < 3)
		return false;

	return line[0] == 'v' && line[1] == 'n' && std::isspace(line[2]);
}

bool isFace(const std::string& line)
{
	if (line.size() < 2)
		return false;

	return line[0] == 'f' && std::isspace(line[1]);
}

std::pair<VertexFileData, std::string> parseVertices(io::InputStream& input)
{
	VertexFileData fileData;

	auto& vertices = fileData.vertices;
	auto& textureCoords = fileData.textureCoords;
	auto& normals = fileData.normals;
	auto& formatData = fileData.formatData;
	auto& vertexCount = fileData.size;

	std::vector<VertexReference> vertexMapping;

	std::size_t numVertices = 0;
	std::size_t numTextureCoords = 0;
	std::size_t numNormals = 0;

	bool passedVertexParsing = false;

	std::string line;

	while (input.available() > 0)
	{
		line = readLine(input);

		if (line.empty() || isComment(line) || isGroup(line))
		{
			continue;
		}
		else if (isVertex(line))
		{
			if (passedVertexParsing)
			{
				ERROR_OUT(TAG, "Vertex definition passed element definition at %s.", line.c_str());
				throw MeshReadException();
			}

			++numVertices;
			appendVector(line, vertices);
		}
		else if (isTextureCoord(line))
		{
			if (passedVertexParsing)
			{
				ERROR_OUT(TAG, "Vertex definition passed element definition at %s.", line.c_str());
				throw MeshReadException();
			}

			++numTextureCoords;
			appendVector(line, textureCoords);
		}
		else if (isNormal(line))
		{
			if (passedVertexParsing)
			{
				ERROR_OUT(TAG, "Vertex definition passed element definition at %s.", line.c_str());
				throw MeshReadException();
			}

			++numNormals;
			appendVector(line, normals);
		}
		else if (isFace(line))
		{
			break;
		}
		else
		{
			ERROR_OUT(TAG, "Unrecognized type tag, line: '%s'", line.c_str());
		}
	}

	if (numVertices <= 0)
	{
		ERROR_OUT(TAG, "No vertices in definition.");
		throw MeshReadException();
	}

	if (vertices.size() % numVertices != 0)
	{
		ERROR_OUT(TAG, "Vertex size is inconsistent.");
		throw MeshReadException();
	}
	else if (numTextureCoords > 0 && textureCoords.size() % numTextureCoords != 0)
	{
		ERROR_OUT(TAG, "Texture coord size is inconsistent.");
		throw MeshReadException();
	}
	else if (numNormals > 0 && normals.size() % numNormals != 0)
	{
		ERROR_OUT(TAG, "Normal coord size is inconsistent.");
		throw MeshReadException();
	}

	if (numNormals > 0 && numNormals != numVertices)
	{
		ERROR_OUT(TAG, "Inconsistent vertex format (normals not same size as vertices).");
		throw MeshReadException();
	}

	if (numTextureCoords > 0 && numTextureCoords != numVertices)
	{
		ERROR_OUT(TAG, "Inconsistent vertex format (texture coords not same size as vertices).");
		throw MeshReadException();
	}

	vertexCount = numVertices;

	// Note: vertex colors not supported by OBJ files.
	formatData.colorDim = 0;

	formatData.vertDim = vertices.size() / vertexCount;

	if (textureCoords.size() > 0)
		formatData.textDim = textureCoords.size() / vertexCount;
	else
		formatData.textDim = 0;

	if (normals.size() > 0)
		formatData.normalDim = normals.size() / vertexCount;
	else
		formatData.normalDim = 0;

	return std::make_pair(fileData, line);
}

struct FaceData
{
	std::vector<gr::index_t> indices;
	gr::MaterialHandle material;
	gr::Primitive primitive;
};

void decomposeAndPushBackFace(std::vector<FaceData>& faces,
				              std::vector<gr::index_t>&& face)
{
	std::vector<gr::index_t> quads;

	while (face.size() > 4)
	{
		quads.insert(quads.end(), face.begin(), face.begin() + 4);

		face.erase(face.begin() + 1, face.begin() + 3);
	}

	ASSERT(face.size() == 4 || face.size() == 3, "Expected triangle or quad to be left.");

	if (face.size() == 4)
	{
		quads.insert(quads.end(), face.begin(), face.end());
	}
	else if (face.size() == 3)
	{
		faces.push_back(
			FaceData{
				std::move(face),
				nullptr,
				gr::Primitive::TRIANGLES
			}
		);
	}
	
	if (quads.size() > 0)
	{
		faces.push_back(
			FaceData{
				std::move(quads),
				nullptr,
				gr::Primitive::QUADS
			}
		);
	}
}

std::vector<FaceData> parseFaces(io::InputStream& input,
						         gr::VertexList<gr::VertexFormat::DYNAMIC_TAG>& vl,
								 const std::string& firstLine,
								 const VertexFileData& vertexData)
{
	std::vector<FaceData> faces;
	std::vector<VertexReference> refMapping;

	std::string line = firstLine;

	while (input.available() > 0)
	{
		if (line.empty() || isComment(line) || isGroup(line) || isSmoothingGroup(line))
		{
			line = readLine(input);
			continue;
		}
		else if (isFace(line))
		{
			auto&& faceDef = readFace(line, vertexData.size);

			std::vector<gr::index_t> face;

			for (const auto& vertexRef : faceDef)
			{
				auto it = std::find(refMapping.begin(), refMapping.end(), vertexRef);

				if (it != refMapping.end())
				{
					// Found suitible vertex to re-use.

					auto index = it - refMapping.begin();

					face.push_back(index);
				}
				else
				{
					// No perfect vertex found to re-use, add new.

					void* vertex = std::malloc(vertexData.formatData.size());

					if (!vertex)
						throw std::bad_alloc();

					std::memcpy(
						reinterpret_cast<void*>(reinterpret_cast<std::uintptr_t>(vertex) + vertexData.formatData.vertOffset()),
						&vertexData.vertices[vertexRef.vertex * vertexData.formatData.vertDim],
						vertexData.formatData.size()
					);

					vl.push_back(vertex);
					refMapping.push_back(vertexRef);

					std::free(vertex);

					face.push_back(vl.size() - 1);
				}
			}

			decomposeAndPushBackFace(faces, std::move(face));
		}
		else
		{
			ERROR_OUT(TAG, "Unrecognized type tag, line: '%s'", line.c_str());
		}

		line = readLine(input);
	}

	return faces;
}

}

gr::Mesh read(io::InputStream& input, gr::BufferProvider& bufferManager)
{
	INFO_OUT(TAG, "Loading mesh.");

	auto&& data = parseVertices(input);

	gr::VertexList<gr::VertexFormat::DYNAMIC_TAG> vertexList(data.first.formatData);
	vertexList.reserve(data.first.size);

	auto&& faces = parseFaces(input, vertexList, data.second, data.first);

	return gr::Mesh(vertexList, faces, BufferUsage::STATIC, bufferManager);
}

}

} }