#include "gr/MeshProvider.hpp"

#include "gr/VertexList.hpp"
#include <filesystem/FileInputStream.hpp>

namespace pm { namespace gr {

namespace {

struct SpriteFace
{
	explicit SpriteFace(MaterialHandle material);

	std::array<index_t, 4> indices;
	MaterialHandle material;
	Primitive primitive;
};

SpriteFace::SpriteFace(MaterialHandle material_)
: indices({{0, 1, 2, 3}}),
  material(material_),
  primitive(Primitive::QUADS)
{
}

}

auto MeshProvider::spriteMetaData() -> std::map<sprite_id_t, SpriteMetaData>&
{
	static std::map<sprite_id_t, SpriteMetaData> sprites;

	return sprites;
}

auto MeshProvider::getMeshMetaData() -> std::map<mesh_id_t, MeshMetaData>&
{
	static std::map<mesh_id_t, MeshMetaData> meshes;

	return meshes;
}

MeshProvider::MeshProvider(BufferProvider& bufferProvider, MaterialProvider& materialProvider)
: _bufferProvider(bufferProvider),
  _materialProvider(materialProvider)
{
}

MeshHandle MeshProvider::loadMesh(mesh_id_t id)
{
	{
		auto handle = _meshes.find(id);

		if (handle)
			_meshes.erase(handle);
	}

	const auto& meshData = getMeshMetaData();
	auto it = meshData.find(id);

	ASSERT(it != meshData.end(), "Mesh data not registered.");
			
	const auto& path = it->second.path;
	const auto& type = it->second.meshType;

	filesystem::FileInputStream input(path.string());

	MeshHandle meshHandle = make_mesh(MeshReader::read(input, type, _bufferProvider));

	_meshes.insert(std::make_pair(id, meshHandle));

	return meshHandle;
}

MeshHandle MeshProvider::loadSprite(sprite_id_t id)
{
	{
		auto handle = _meshes.find(id);

		if (handle)
			_meshes.erase(handle);
	}

	const auto& spriteDatas = spriteMetaData();
	auto it = spriteDatas.find(id);

	ASSERT(it != spriteDatas.end(), "Sprite data not registered.");

	const auto& spriteData = it->second;

	VertexList<VertexFormat::V2_T2> vl;

	real_t left = -0.5_r * spriteData.meshSize[0];
	real_t right = 0.5_r * spriteData.meshSize[0];

	real_t bottom = -0.5_r * spriteData.meshSize[1];
	real_t top = 0.5_r * spriteData.meshSize[1];

	vl.push_back({
		{ left, bottom },
		{ spriteData.uv[0], spriteData.uv[1] }
	});
	vl.push_back({ 
		{ right, bottom },
		{ spriteData.uv[0] + spriteData.size[0], spriteData.uv[1] }
	});
	vl.push_back({ 
		{ right, top },
		{ spriteData.uv[0] + spriteData.size[0], spriteData.uv[1] + spriteData.size[1] }
	});
	vl.push_back({
		{ left, top },
		{ spriteData.uv[0], spriteData.uv[1] + spriteData.size[1] }
	});

	std::array<SpriteFace, 1> faces =
	{
		{SpriteFace(_materialProvider.acquire(spriteData.materialId))}
	};

	MeshHandle meshHandle = make_mesh(vl, faces, BufferUsage::STATIC, _bufferProvider);

	_meshes.insert(std::make_pair(id, meshHandle));

	return meshHandle;
}

MeshHandle MeshProvider::get(mesh_id_t id)
{
	if (_meshes.has(id))
		return _meshes.find(id)->second;
	else
		return nullptr;
}

MeshHandle MeshProvider::acquireMesh(mesh_id_t id)
{
	auto mesh = get(id);

	if (!mesh)
		mesh = loadMesh(id);

	return mesh;
}

MeshHandle MeshProvider::acquireSprite(mesh_id_t id)
{
	auto mesh = get(id);

	if (!mesh)
		mesh = loadSprite(id);

	return mesh;
}

MeshProvider::sprite_id_t MeshProvider::registerSprite(
	sprite_id_t id,
	TextureProvider::image_id_t texture,
	const Vector2_r& meshSize,
	const Vector2_r& uv,
	const Vector2_r& size
)
{
	auto& sprites = spriteMetaData();
	
	ASSERT(sprites.find(id) == sprites.end(), "Trying to overwrite meta data.");

	SpriteMetaData metaData;
	
	metaData.uv = uv;
	metaData.size = size;
	metaData.meshSize = meshSize;
	metaData.materialId = MaterialProvider::registerMaterial(
		"MAT_IMAGE_" + std::to_string(texture),
		gr::Color{ 0.0_r, 0.0_r, 0.0_r, 1.0_r },
		gr::Color{ 0.0_r, 0.0_r, 0.0_r, 1.0_r },
		gr::Color{ 0.0_r, 0.0_r, 0.0_r, 1.0_r },
		{ texture }
	);

	sprites.insert(std::make_pair(id, metaData));

	return id;
}

MeshProvider::mesh_id_t MeshProvider::registerMesh(mesh_id_t id, const path_t& path, mesh_t typeId)
{
	auto& meshes = getMeshMetaData();

	ASSERT(meshes.find(id) == meshes.end(), "Trying to overwrite mesh meta data.");

	meshes.insert(std::make_pair(id, MeshMetaData{ path, typeId }));

	return id;
}

} }
