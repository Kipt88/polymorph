#include "gr/Scene.hpp"

namespace pm { namespace gr {

Scene::Scene(const Scene& other)
: _impl()
{
	if (other._impl)
		_impl = other._impl->clone();
}

Scene& Scene::operator=(const Scene& other)
{
	if (other._impl)
		_impl = other._impl->clone();
	else
		_impl = nullptr;

	return *this;
}

void Scene::draw(Surface& surface) const
{
	if (_impl)
		_impl->draw(surface);
}

} }
