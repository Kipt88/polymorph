#include "gr/MeshReader_MESH.hpp"

#include <gr/VertexList.hpp>
#include <Debug.hpp>

namespace pm { namespace gr {

namespace MeshReader_MESH {

namespace {

const char* LOG_TAG = "MeshReader_MESH";

struct FaceData
{
	std::vector<gr::index_t> indices;
	gr::MaterialHandle material;
	gr::Primitive primitive;
};

gr::VertexList<> parseVertices(io::InputStream& input)
{
	gr::VertexFormatData vertexFormatData;
	input >> vertexFormatData;

	std::uint32_t vertexSize;
	input >> vertexSize;

	std::vector<gr::real_t> rawVertices;
	rawVertices.resize(vertexSize * vertexFormatData.size() / sizeof(gr::real_t));

	io::readBlock(input, rawVertices.data(), rawVertices.size());

	return gr::VertexList<>{vertexFormatData, rawVertices.data(), vertexSize};
}

std::vector<FaceData> parseFaces(io::InputStream& input)
{
	std::vector<FaceData> faces;

	std::uint32_t faceSize;
	input >> faceSize;

	faces.reserve(faceSize);

	for (std::uint32_t i = 0; i < faceSize; ++i)
	{
		gr::Primitive primitive;
		input >> primitive;

		std::uint32_t indexSize;
		input >> indexSize;

		std::vector<gr::index_t> indices;
		indices.resize(indexSize);

		io::readBlock(input, indices.data(), indices.size());

		faces.push_back(FaceData{ std::move(indices), nullptr, primitive });
	}

	return faces;
}

}

gr::Mesh read(io::InputStream& input, gr::BufferProvider& bufferManager)
{
	auto&& vertices = parseVertices(input);
	auto&& faces = parseFaces(input);

	return gr::Mesh{ vertices, std::move(faces), BufferUsage::STATIC,  bufferManager };

}

}

} }
