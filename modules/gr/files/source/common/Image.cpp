
#include "gr/Image.hpp"

namespace pm { namespace gr {

Image::Image(unsigned int w,
             unsigned int h,
             PixelFormat format,
             unsigned int rowAlignment,
             std::unique_ptr<unsigned char[]>&& pixels)
: _width(w),
  _height(h),
  _format(format),
  _rowAlignment(rowAlignment),
  _pixels(std::move(pixels))
{
}

Image::Image(Image&& image)
: _width(image._width),
  _height(image._height),
  _format(image._format),
  _rowAlignment(image._rowAlignment),
  _pixels(std::move(image._pixels))
{
}

std::size_t Image::estimatedSize() const
{
	return 0; // TODO
}

unsigned int Image::width() const
{
	return _width;
}

unsigned int Image::height() const
{
	return _height;
}

PixelFormat Image::pixelFormat() const
{
	return _format;
}

unsigned int Image::rowAlignment() const
{
	return _rowAlignment;
}

const unsigned char* Image::data() const
{
	return _pixels.get();
}

} }