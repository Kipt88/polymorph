#include "gr/Graphics.hpp"

namespace pm { namespace gr {

namespace {

os::Window& fetchWindow(os::AppSystem& system)
{
	auto* windowPtr = system.pushAction(
		[](os::App& runner)
		{
			return &runner.window();
		}
	).get();

	return *windowPtr;
}

}

Graphics::Graphics(const Time::duration& sync, os::AppSystem& app)
: _system(sync, std::ref(fetchWindow(app)), std::ref(_lastFrame), std::ref(_frameMutex)),
  _frameMutex(),
  _lastFrame(),
  _resources(_system),
  _onResize(
	  app.addWindowEventListener<os::window_events::Resize>(
		  [this](const os::window_events::Resize& ev)
		  {
			  _system.pushAction(
				  [ev](SystemRunner& runner)
				  {
					  runner.surface().setViewport(math::AxisAlignedBox2<int>(0, 0, ev.w, ev.h));
				  }
			  );
		  }
	  )
  )
{
	DEBUG_OUT("GraphicsSystem", "Created.");
}

ClientFrame Graphics::createFrame()
{
	return ClientFrame(_lastFrame, _frameMutex);
}

Resources& Graphics::resources()
{
	return _resources;
}

math::Vector<int, 2> Graphics::getSurfaceSize() const
{
	// TODO Cache result...

	return _system.pushAction(
		[](SystemRunner& runner)
		{
			auto viewport = runner.surface().getViewport();

			return math::Vector<int, 2>{ viewport.width(), viewport.height() };
		}
	).get();
}

} }
