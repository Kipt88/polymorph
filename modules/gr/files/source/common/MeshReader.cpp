#include "gr/MeshReader.hpp"

#include "gr/MeshReader_OBJ.hpp"
#include "gr/MeshReader_MESH.hpp"

#include <array>

namespace pm { namespace gr {

namespace {

MeshReader::Reader getReader(MeshReader::mesh_t meshType)
{
	static std::array<MeshReader::Reader, 2> readers =
	{
		{MeshReader_OBJ::read, MeshReader_MESH::read}
	};

	return readers[static_cast<std::size_t>(meshType)];
}

}

gr::Mesh MeshReader::read(io::InputStream& input, mesh_t meshType, gr::BufferProvider& bufferManager)
{
	return getReader(meshType)(input, bufferManager);
}

} }
