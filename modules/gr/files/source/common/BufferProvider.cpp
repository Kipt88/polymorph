#include "gr/BufferProvider.hpp"

#include <algorithm>

namespace pm { namespace gr {

namespace {

// 1-4MB buffer limit seems to be recommended by most vendors.
const std::size_t MAX_BUFFER_SIZE = 2000000;

const std::size_t MIN_BUFFER_SIZE = 2048;

}

VertexSubBufferView BufferProvider::getVertexBuffer(
	const VertexFormatData& fmtData,
	std::size_t size,
	BufferUsage usage,
	const void* data
)
{
	//
	// Find a reusable buffer.
	// 1: If not found then create a new one and return it.
	// 2: If found then find a free partition in the buffer.
	//   2.1: If found then return the partition.
	//   2.2: If not found then resize buffer to fit new data and return that part.
	//

	// Find suitible buffer to reuse.
	
	auto candidate = _vertexBuffers.find_if(
		[&](const VertexBufferHandle& handle) -> bool
		{
			return (handle->vertexFormatData() == fmtData)
					&& (handle->bufferUsage() == usage)
					&& (handle->vertexFormatData().size()
						* (handle->size() + size) < MAX_BUFFER_SIZE);
		}
	);

	if (!candidate)
	{
		// 1:

		std::size_t byteSize = fmtData.size() * size;
		std::size_t extraBytes = 0;

		if (byteSize < MIN_BUFFER_SIZE)
			extraBytes = MIN_BUFFER_SIZE - byteSize;

		auto extraSize = extraBytes / fmtData.size();
		auto sizeToAllocate = size + extraSize;

		{
			auto handle = std::make_shared<VertexBuffer>(fmtData, usage, sizeToAllocate);

			LinearSpacePartitioner newPartition;

			if (extraSize > 0)
			{
				newPartition.insert(
					LinearSpacePartitioner::Partition{size, size + extraSize}
				);
			}

			_freeVBPartitions.insert(std::make_pair(handle, std::move(newPartition)));
			_vertexBuffers.insert(handle);

			VertexSubBufferView view(*this, handle, 0, size);

			if (data)
				view.write(data, 0, size);

			return view;
		}
	}

	candidate.use();

	// 2:

	VertexBufferHandle buffer = *candidate;

	auto it = _freeVBPartitions.find(buffer);
	ASSERT(it != _freeVBPartitions.end(), "Missing free partition storage.");

	auto& partitions = it->second;

	const auto& partition = partitions.removeSubPartition(size);

	if (partition.size() > 0)
	{
		// 2.1:

		VertexSubBufferView view(*this, buffer, partition.begin, partition.size());
		
		if (data)
		{
			view.write(data, 0, size);
		}

		return view;
	}

	// 2.2:
	{
		VertexSubBufferView view(*this, buffer, buffer->size(), size);

		{
			// TODO Should allocated more than required to avoid multiple allocations

			buffer->resize(buffer->size() + size);

			if (data)
				view.write(data, 0, size);
		}

		return view;
	}
}

IndexSubBufferView BufferProvider::getIndexBuffer(
	gr::BufferUsage usage,
	std::size_t size,
	const index_t* data
)
{
	//
	// Find a reusable buffer.
	// 1: If not found then create a new one and return it.
	// 2: If found then find a free partition in the buffer.
	//   2.1: If found then return the partition.
	//   2.2: If not found then resize buffer to fit new data and return that part.
	//

	// Find suitible buffer to reuse.
	
	auto reusable = [&](const IndexBufferHandle& handle) -> bool
	{
		return handle->bufferUsage() == usage 
			   && sizeof(real_t) * (handle->size() + size) < MAX_BUFFER_SIZE;
	};

	auto candidate = _indexBuffers.find_if(reusable);

	if (!candidate)
	{
		// 1:

		std::size_t byteSize = sizeof(index_t) * size;
		std::size_t extraBytes = 0;

		if (byteSize < MIN_BUFFER_SIZE)
			extraBytes = MIN_BUFFER_SIZE - byteSize;

		auto extraSize = extraBytes / sizeof(index_t);
		auto sizeToAllocate = size + extraSize;

		auto handle = std::make_shared<IndexBuffer>(usage, sizeToAllocate);
		
		if (data)
			handle->write(0, size, data);
		
		LinearSpacePartitioner newPartition;

		if (extraSize > 0)
		{
			newPartition.insert(
				LinearSpacePartitioner::Partition{ size, size + extraSize }
			);
		}

		_freeIBPartitions.insert(std::make_pair(handle, std::move(newPartition)));
		_indexBuffers.insert(handle);

		IndexSubBufferView view(*this, handle, 0, size);
		
		if (data)
			view.write(0, size, data);

		return view;
	}

	candidate.use();

	// 2:

	IndexBufferHandle buffer = *candidate;

	auto it = _freeIBPartitions.find(buffer);
	ASSERT(it != _freeIBPartitions.end(), "Missing free partition storage.");

	auto& partitions = it->second;

	const auto& partition = partitions.removeSubPartition(size);

	if (partition.size() > 0)
	{
		// 2.1:
		IndexSubBufferView view(*this, buffer, partition.begin, partition.size());
		
		if (data)
			view.write(0, size, data);
		
		return view;
	}

	// 2.2:
	{
		IndexSubBufferView view(*this, buffer, buffer->size(), size);

		{
			buffer->resize(buffer->size() + size);
			view.write(0, size, data);
		}

		return view;
	}
}

void BufferProvider::releaseVertexSubBufferView(const VertexSubBufferView& view)
{
	const auto& buffer = view.buffer();

	auto it = _freeVBPartitions.find(buffer);
	ASSERT(it != _freeVBPartitions.end(), "Missing free partition storage.");

	auto& partitions = it->second;

	partitions.insert({ view.startIndex(), view.startIndex() + view.size() });
}

void BufferProvider::releaseIndexSubBufferView(const IndexSubBufferView& view)
{
	const auto& buffer = view.buffer();

	auto it = _freeIBPartitions.find(buffer);
	ASSERT(it != _freeIBPartitions.end(), "Missing free partition storage.");

	auto& partitions = it->second;

	partitions.insert({ view.startIndex(), view.startIndex() + view.size() });
}

} }
