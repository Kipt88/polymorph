#include "gr/MaterialProvider.hpp"

namespace pm { namespace gr {

auto MaterialProvider::getMaterialMetaData() -> std::map<material_id_t, MaterialMetaData>&
{
	static std::map<material_id_t, MaterialMetaData> materials;

	return materials;
}

MaterialProvider::MaterialProvider(TextureProvider& textureProvider)
: _textureProvider(textureProvider),
  _materials()
{
}

MaterialHandle MaterialProvider::loadMaterial(material_id_t id)
{
	{
		auto handle = _materials.find(id);

		if (handle)
			_materials.erase(handle);
	}

	const auto& materialData = getMaterialMetaData();
	auto it = materialData.find(id);

	ASSERT(it != materialData.end(), "Material data not registered.");

	Material material;

	material.ambientColor = it->second.ambientColor;
	material.diffuseColor = it->second.diffuseColor;
	material.specularColor = it->second.specularColor;
			
	for (const auto& imageId : it->second.textures)
		material.textures.push_back(_textureProvider.acquire(imageId));

	MaterialHandle materialHandle = std::make_shared<Material>(material);

	_materials.insert(std::make_pair(id, materialHandle));

	return materialHandle;
}

MaterialHandle MaterialProvider::get(material_id_t id)
{
	if (_materials.has(id))
		return _materials.find(id)->second;
	else
		return nullptr;
}

MaterialHandle MaterialProvider::acquire(material_id_t id)
{
	auto handle = get(id);

	if (!handle)
		handle = loadMaterial(id);

	return handle;
}

MaterialProvider::material_id_t MaterialProvider::registerMaterial(
	const std::string& materialName,
	const Color& ambientColor,
	const Color& diffuseColor,
	const Color& specularColor,
	const std::vector<TextureProvider::image_id_t>& textures)
{
	auto hashed = hash(materialName);
	auto& materials = getMaterialMetaData();

#ifdef _DEBUG
	for (const auto& imageId : textures)
		ASSERT(imageId != 0, "Material image is 0, are images registered?");
#endif

	materials.insert(
		std::make_pair(
			hashed,
			MaterialMetaData{
				ambientColor,
				diffuseColor,
				specularColor,
				textures
			}
		)
	);

	return hashed;
}

} }
