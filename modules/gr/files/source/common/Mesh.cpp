#include "gr/Mesh.hpp"

#include <algorithm>

namespace pm { namespace gr {

Mesh::Mesh(
	VertexSubBufferView&& vertices, 
	face_collection_t&& faces,
	const math::AxisAlignedBox3<real_t>& bounds
)
: _vertices(std::move(vertices)),
  _faces(std::move(faces)),
  _bounds(bounds)
{
}

Mesh::Mesh(Mesh&& other)
: _vertices(std::move(other._vertices)),
  _faces(std::move(other._faces)),
  _bounds(other._bounds)
{
}

VertexSubBufferView& Mesh::vertices()
{
	return _vertices;
}

const VertexSubBufferView& Mesh::vertices() const
{
	return _vertices;
}

Mesh::face_collection_t& Mesh::faces()
{
	return _faces;
}

const Mesh::face_collection_t& Mesh::faces() const
{
	return _faces;
}

math::AxisAlignedBox3<real_t>& Mesh::bounds()
{
	return _bounds;
}

const math::AxisAlignedBox3<real_t>& Mesh::bounds() const
{
	return _bounds;
}

} }
