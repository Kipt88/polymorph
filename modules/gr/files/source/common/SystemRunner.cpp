#include "gr/SystemRunner.hpp"

namespace pm { namespace gr {

SystemRunner::SystemRunner(concurrency::RunnerTasks<SystemRunner>&, os::Window& window, Frame& lastFrame, concurrency::Mutex& frameMutex)
: _surface(window),
  _buffers(),
  _textures(),
  _materials(_textures),
  _meshes(_buffers, _materials),
  _lastFrame(lastFrame),
  _frameMutex(frameMutex)
{
}

bool SystemRunner::update(const concurrency::FrameTime&)
{
	Frame frame;

	{
		concurrency::MutexLockGuard sceneLock(_frameMutex);

		frame = std::move(_lastFrame);
	}

	frame.render(_surface);

	return true;
}

Surface& SystemRunner::surface()
{
	return _surface;
}

MaterialProvider& SystemRunner::materials()
{
	return _materials;
}

TextureProvider& SystemRunner::textures()
{
	return _textures;
}

MeshProvider& SystemRunner::meshes()
{
	return _meshes;
}

BufferProvider& SystemRunner::buffers()
{
	return _buffers;
}

} }
