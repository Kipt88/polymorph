#include "gr/Resources.hpp"

namespace pm { namespace gr {

Resources::Resources(concurrency::ConcurrentSystem<SystemRunner>& system)
: _system(system)
{
}

MaterialHandle Resources::acquireMaterial(id_t id)
{
	{
		auto it = _materials.find(id);

		if (it != _materials.end())
		{
			if (auto material = it->second.lock())
				return material;
		}
	}

	auto material = _system.pushAction(
		[id](SystemRunner& runner)
		{
			return runner.materials().acquire(id);
		}
	).get();

	_materials.insert(std::make_pair(id, material));

	return material;
}

TextureHandle Resources::acquireTexture(id_t id)
{
	{
		auto it = _textures.find(id);

		if (it != _textures.end())
		{
			if (auto texture = it->second.lock())
				return texture;
		}
	}

	auto texture = _system.pushAction(
		[id](SystemRunner& runner)
		{
			return runner.textures().acquire(id);
		}
	).get();

	_textures.insert(std::make_pair(id, texture));

	return texture;
}

MeshHandle Resources::acquireMesh(id_t id)
{
	{
		auto it = _meshes.find(id);

		if (it != _meshes.end())
		{
			if (auto mesh = it->second.lock())
				return mesh;
		}
	}

	auto mesh = _system.pushAction(
		[id](SystemRunner& runner)
		{
			return runner.meshes().acquireMesh(id);
		}
	).get();

	_meshes.insert(std::make_pair(id, mesh));

	return mesh;
}

MeshHandle Resources::acquireSprite(id_t id)
{
	{
		auto it = _meshes.find(id);

		if (it != _meshes.end())
		{
			if (auto mesh = it->second.lock())
				return mesh;
		}
	}

	auto mesh = _system.pushAction(
		[id](SystemRunner& runner)
		{
			return runner.meshes().acquireSprite(id);
		}
	).get();

	_meshes.insert(std::make_pair(id, mesh));

	return mesh;
}

} }
