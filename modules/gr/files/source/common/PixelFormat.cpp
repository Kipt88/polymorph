#include "gr/PixelFormat.hpp"

#include <Assert.hpp>
#include <exception>

namespace pm { namespace gr {

const PixelFormatData& getPixelFormatData(PixelFormat format)
{
	static const PixelFormatData d_R8G8B8 =
	{
		3
	};

	static const PixelFormatData d_R8G8B8A8 =
	{
		4
	};

	switch (format)
	{
	case PixelFormat::R8G8B8:
		return d_R8G8B8;
	case PixelFormat::R8G8B8A8:
		return d_R8G8B8A8;
	}

	ASSERT(false, "Bad pixel format input.");
	std::terminate();
}

} }