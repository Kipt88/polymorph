#include "gr/Frame.hpp"
#include "profiler/Markers.hpp"

namespace pm { namespace gr {

// Frame:

void Frame::set(std::vector<UniqueFunction<void(Surface&)>>&& queue)
{
	_queue = std::move(queue);
}

void Frame::render(Surface& surface) const
{
	if (_queue.empty())
	{
		INFO_OUT("Frame", "Nothing to render.");
		return;
	}

	profiler::Block block("render");

	for (const auto& action : _queue)
		action(surface);

	block("vsync");

	surface.flipBuffers();
}


// ClientFrame:

ClientFrame::ClientFrame(Frame& frame, concurrency::Mutex& frameMutex)
: _queue(),
  _frame(&frame),
  _frameMutex(&frameMutex)
{
}

ClientFrame::ClientFrame(ClientFrame&& other)
: _queue(std::move(other._queue)),
  _frame(other._frame),
  _frameMutex(other._frameMutex)
{
	other._frame = nullptr;
	other._frameMutex = nullptr;
}

ClientFrame::~ClientFrame()
{
	if (_frame)
	{
		concurrency::MutexLockGuard lock(*_frameMutex);
		_frame->set(std::move(_queue));
	}
}

ClientFrame& ClientFrame::operator=(ClientFrame&& other)
{
	_queue = std::move(other._queue);
	_frame = other._frame;
	_frameMutex = other._frameMutex;

	other._frame = nullptr;
	other._frameMutex = nullptr;

	return *this;
}

} }
