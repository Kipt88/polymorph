#include "gr/ShaderProvider.hpp"

#include <filesystem/FileInputStream.hpp>

#include <Scoped.hpp>

#include <numeric>

namespace pm { namespace gr {

namespace {

gr::ShaderHandle loadShader(gr::Surface& surface,
							const ShaderProvider::path_t& path, 
							gr::ShaderType type)
{
	filesystem::FileInputStream input(path);

	std::string source;
	source.resize(input.available());

	io::readBlock(input, &source[0], source.size());

	{
		return std::make_shared<gr::Shader>(source, type);
	}
}

}

std::map<ShaderProvider::shader_id_t, ShaderProvider::ShaderMetaData>& 
	ShaderProvider::getShadersMetaData()
{
	static std::map<shader_id_t, ShaderMetaData> data;

	return data;
}

std::map<ShaderProvider::shader_suite_id_t, ShaderProvider::ShaderSuiteMetaData>&
	ShaderProvider::getShaderSuitesMetaData()
{
	static std::map<shader_suite_id_t, ShaderSuiteMetaData> data;

	return data;
}

ShaderProvider::shader_id_t ShaderProvider::registerShader(const path_t& path,
														 gr::ShaderType type)
{
	auto& shadersMetaData = getShadersMetaData();

	auto hashed = hash(path);

	ASSERT(shadersMetaData.find(hashed) == shadersMetaData.end(), 
		   "Trying to overwrite shader meta data.");

	shadersMetaData.insert(std::make_pair(hashed, ShaderMetaData{ path, type }));

	return hashed;
}

ShaderProvider::shader_suite_id_t ShaderProvider::registerShaderSuite(
	std::initializer_list<shader_id_t> shaders)
{
	auto& shaderSuitesMetaData = getShaderSuitesMetaData();

	shader_suite_id_t id = 0;

	for (const auto& shaderId : shaders)
		id += shaderId;

	ASSERT(shaderSuitesMetaData.find(id) == shaderSuitesMetaData.end(),
		   "Trying to overwrite shader suite meta data.");

	shaderSuitesMetaData.insert(std::make_pair(id, ShaderSuiteMetaData{ shaders }));

	return id;
}

ShaderProvider::ShaderProvider(gr::Surface& surface)
: _surface(surface),
  _cacheMutex(),
  _loader(),
  _shaders()
{
}

ShaderProvider::LoadResult ShaderProvider::loadShaderSuite(shader_suite_id_t id)
{
	return _loader.pushAction(
		[this, id]
		{
			auto shaderSuiteHandle = get(id);

			if (shaderSuiteHandle)
				return shaderSuiteHandle;

			const auto& shaderSuiteDatas = getShaderSuitesMetaData();
			const auto& shaderDatas = getShadersMetaData();

			auto shaderSuiteIt = shaderSuiteDatas.find(id);

			if (shaderSuiteIt == shaderSuiteDatas.end())
				throw NoSuchShaderSuiteException();

			std::vector<gr::ShaderHandle> suiteShaders;

			{
				concurrency::MutexLockGuard lock(_cacheMutex);

				for (const shader_id_t& shaderId : shaderSuiteIt->second.shaders)
				{
					if (_shaders.has(shaderId))
					{
						suiteShaders.push_back(
							_shaders.find(shaderId)->second
						);
					}
					else
					{
						auto shaderIt = shaderDatas.find(shaderId);

						if (shaderIt == shaderDatas.end())
							throw NoSuchShaderException();

						auto shaderHandle = loadShader(_surface,
													   shaderIt->second.path,
													   shaderIt->second.type);

						suiteShaders.push_back(shaderHandle);

						_shaders.insert(
							std::make_pair(
								shaderId,
								shaderHandle
							)
						);
					}
				}
			}

			{
				shaderSuiteHandle = std::make_shared<gr::ShaderSuite>(
					suiteShaders.begin(),
					suiteShaders.end()
				);
			}

			{
				concurrency::MutexLockGuard shadersLock(_cacheMutex);

				_shaderSuites.insert(
					std::make_pair(
						id, 
						shaderSuiteHandle
					)
				);
			}

			return shaderSuiteHandle;
		}
	);
}

gr::ShaderSuiteHandle ShaderProvider::get(shader_suite_id_t id) const
{
	concurrency::MutexLockGuard lock(_cacheMutex);

	if (_shaderSuites.has(id))
		return _shaderSuites.find(id)->second;
	else
		return nullptr;
}

} }
