#include "gr/VertexSubBufferView.hpp"

namespace pm { namespace gr {

VertexSubBufferView::VertexSubBufferView(VertexSubBufferViewHandler& handler,
										 VertexBufferHandle buffer,
										 std::size_t startIndex,
										 std::size_t size)
: _handler(&handler),
  _buffer(buffer),
  _startIndex(startIndex),
  _size(size)
{
}

VertexSubBufferView::VertexSubBufferView(VertexSubBufferView&& other)
: _handler(other._handler),
  _buffer(other._buffer),
  _startIndex(other._startIndex),
  _size(other._size)
{
	other._handler = nullptr;
}

VertexSubBufferView::~VertexSubBufferView()
{
	if (_handler)
		_handler->releaseVertexSubBufferView(*this);
}

VertexSubBufferView& VertexSubBufferView::operator=(VertexSubBufferView&& other)
{
	_handler = other._handler;
	_buffer = other._buffer;
	_startIndex = other._startIndex;
	_size = other._size;

	other._handler = nullptr;

	return *this;
}

VertexSubBufferView& VertexSubBufferView::operator=(const VertexSubBufferView& other)
{
	if (_handler)
		_handler->releaseVertexSubBufferView(*this);

	_handler = nullptr;
	_buffer = other._buffer;
	_startIndex = other._startIndex;
	_size = other._size;

	return *this;
}

void VertexSubBufferView::write(const void* vertices, 
								std::size_t index, 
								std::size_t size)
{
	ASSERT(index + size <= _size, "Sub-buffer overflow.");

	_buffer->write(vertices, _startIndex + index, size);
}

void VertexSubBufferView::draw(Primitive primitive, const IndexSubBufferView& indices) const
{
	_buffer->draw(primitive, *indices.buffer(), indices.startIndex(), indices.size());
}

void VertexSubBufferView::draw(Primitive primitive, std::size_t index, std::size_t count) const
{
	ASSERT(index + count <= _size, "Overdrawing.");

	_buffer->draw(primitive, _startIndex + index, count);
}

void VertexSubBufferView::draw(Primitive primitive) const
{
	_buffer->draw(primitive, _startIndex, _size);
}

const VertexBufferHandle& VertexSubBufferView::buffer() const
{
	return _buffer;
}

std::size_t VertexSubBufferView::startIndex() const
{
	return _startIndex;
}

std::size_t VertexSubBufferView::size() const
{
	return _size;
}

} }