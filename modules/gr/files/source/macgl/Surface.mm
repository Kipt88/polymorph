#include "gr/Surface.hpp"

#include "gr/SurfaceException.hpp"

#include "gr/opengl.hpp"

namespace pm { namespace gr {

Surface::Surface(os::Window& window)
: _context(nil)
{
	NSOpenGLPixelFormatAttribute attributes[] = {
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFADepthSize, 16,
		0
	};
	
	NSOpenGLPixelFormat* fmt = [[NSOpenGLPixelFormat alloc] initWithAttributes:attributes];
	
	if (!fmt)
		throw SurfaceException("Can't create pixel format.");
	
	NSWindow* nsWindow = window.nativeHandle();
	
	NSOpenGLView* view = [[NSOpenGLView alloc] initWithFrame:NSMakeRect(0, 0, nsWindow.frame.size.width, nsWindow.frame.size.height) pixelFormat:fmt];
	
	_context = view.openGLContext;
	
	nsWindow.contentView = view;
}

void Surface::setVSync(bool enabled)
{
	// TODO Implement...
}

void Surface::flipBuffers()
{
	[_context flushBuffer];
}

void Surface::setViewport(const math::AxisAlignedBox2<int>& box)
{
	glViewport(box.min()[0], box.min()[1], box.width(), box.height());
}

math::AxisAlignedBox2<int> Surface::getViewport() const
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	
	return math::AxisAlignedBox2<int>{
		viewport[0], viewport[1], viewport[2], viewport[3]
	};
}

Vector2_r Surface::normalizedToWindow(const Vector2_r& v) const
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	return Vector2_r {
		static_cast<real_t>(viewport[0]) + 0.5_r * static_cast<real_t>(viewport[2]) * (v[0] + 1.0_r),
		static_cast<real_t>(viewport[1]) + 0.5_r * static_cast<real_t>(viewport[3]) * (v[1] + 1.0_r)
	};
}

Vector3_r Surface::normalizedToWindow(const Vector3_r& v) const
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	return Vector3_r{
		static_cast<real_t>(viewport[0]) + 0.5_r * static_cast<real_t>(viewport[2]) * (v[0] + 1.0_r),
		static_cast<real_t>(viewport[1]) + 0.5_r * static_cast<real_t>(viewport[3]) * (v[1] + 1.0_r),
		0.5_r * (v[2] + 1.0_r)
	};
}

Vector2_r Surface::windowToNormalized(const Vector2_r& v) const
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	return Vector2_r{
		2.0_r * (v[0] - static_cast<real_t>(viewport[0])) / static_cast<real_t>(viewport[2]) - 1.0_r,
		2.0_r * (v[1] - static_cast<real_t>(viewport[1])) / static_cast<real_t>(viewport[3]) - 1.0_r,
	};
}

Vector3_r Surface::windowToNormalized(const Vector3_r& v) const
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	return Vector3_r{
		2.0_r * (v[0] - static_cast<real_t>(viewport[0])) / static_cast<real_t>(viewport[2]) - 1.0_r,
		2.0_r * (v[1] - static_cast<real_t>(viewport[1])) / static_cast<real_t>(viewport[3]) - 1.0_r,
		2.0_r * v[2] - 1.0_r
	};
}

} }
