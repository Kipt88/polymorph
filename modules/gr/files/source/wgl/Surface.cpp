#include "gr/Surface.hpp"

#include "gr/SurfaceException.hpp"
#include "gr/opengl.hpp"

#include <Assert.hpp>
#include <Debug.hpp>

#include <windows.h>

#include <Debug.hpp>

namespace pm { namespace gr {

namespace {

const char* TAG = "Surface"; 

void initGlew(Surface& surface)
{
	GLenum err = glewInit();

	if (err != GLEW_OK)
		throw SurfaceException(); // ((const char*) glewGetErrorString(err));

	//_surface.setVSync();

	int openGLVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &openGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &openGLVersion[1]);
	VERBOSE_OUT(TAG, "GL Version %i.%i", openGLVersion[0], openGLVersion[1]);

	glEnable(GL_TEXTURE_2D);
}

}

namespace detail {

dc_deleter::dc_deleter(HWND window)
: _window(window)
{
}

void dc_deleter::operator()(HDC dc)
{
	if (_window)
		VERIFY(ReleaseDC(_window, dc));
}

void rc_deleter::operator()(HGLRC rc)
{
	DEBUG_OUT(TAG, "Destroying rendering context");

	// NOTE: if the rendering context is active in some other thread,
	// wglDeleteContext() will produce an error
	VERIFY(wglDeleteContext(rc));
}

//////////
// COPIED THIS JUST TO SEE THE ERROR DESCRIPTION

// for convenience
struct ScopedLocalFree : NonCopyable {
	ScopedLocalFree(HLOCAL p) : _p(p) {}
	~ScopedLocalFree() { LocalFree(_p); }
private:
	HLOCAL _p;
};

std::string getWin32Message(DWORD error)
{
	LPVOID buffer = NULL;

	// NOTE: force call FormatMessageA so we can use a char buffer
	// instead of a TCHAR buffer and easily convert to std::string.
	DWORD ret = FormatMessageA(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		error,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPSTR)&buffer,
		0,
		NULL);

	if (ret == 0)
		return "Unknown error";

	ScopedLocalFree localfree(buffer);
	return std::string(static_cast<LPCSTR>(buffer));
}

//////////

}	// namespace detail

Surface::WGLExtensions::WGLExtensions()
: loaded(false), 
  wglSwapInterval(nullptr)
{
}

// Surface

Surface::Surface(os::Window& window)
: _win(window.nativeHandle()),
  _deviceHandle(),
  _glHandle(),
  _wglExtensions()
{
	_deviceHandle = std::unique_ptr<HDC__, detail::dc_deleter>(GetDC(_win), detail::dc_deleter(_win));

	static bool glewInitialized = false;

	{
		auto err = GetLastError();
		if (err)
			ERROR_OUT("Surface", "ctor %i %s", err, detail::getWin32Message(err).c_str());
	}

	if (!_deviceHandle)
	{
		ERROR_OUT(TAG, "Device handle cannot be created. HWND=%p", _win);
		throw SurfaceException(); //("Unable to get device context");
	}

	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(pfd));

	pfd.nSize        = sizeof(pfd);
	pfd.nVersion     = 1; // default version
	pfd.dwFlags      = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType   = PFD_TYPE_RGBA;
	pfd.cColorBits   = 32;
	pfd.cDepthBits   = 16;
	pfd.cStencilBits = 8;
	pfd.iLayerType   = PFD_MAIN_PLANE; // main drawing plane

	// choose best matching pixel format
	int pixelFormat = ChoosePixelFormat(_deviceHandle.get(), &pfd);
	if (pixelFormat == 0 || !SetPixelFormat(_deviceHandle.get(), pixelFormat, &pfd))
	{
		throw SurfaceException(); // ("Unable to set pixel format for context");
	}

	// create the OpenGL rendering context
	_glHandle.reset(wglCreateContext(_deviceHandle.get()));
	if (!_glHandle)
	{
		/*
		std::string msg = "Unable to create OpenGL rendering context.\n";
		msg += detail::getWin32Message(GetLastError());
		*/

		throw SurfaceException(); //(msg);
	}

	{
		auto err = GetLastError();
		if (err)
			ERROR_OUT("Surface", "ctor %i %s", err, detail::getWin32Message(err).c_str());
	}

	wglMakeCurrent(_deviceHandle.get(), _glHandle.get());

	if (!glewInitialized)
	{
		initGlew(*this);
		glewInitialized = true;
	}
}

void Surface::setVSync(bool enable)
{
	_wglExtensions.wglSwapInterval(enable ? 1 : 0);
}

void Surface::flipBuffers()
{
	auto err = SwapBuffers(_deviceHandle.get());

	if (err == FALSE)
	{
		ERROR_OUT("Surface", detail::getWin32Message(GetLastError()).c_str());

		ASSERT(false, "Unable to flip buffers.");
	}
}

void Surface::setViewport(const math::AxisAlignedBox2<int>& box)
{
	glViewport(box.min()[0], box.min()[1], box.width(), box.height());
}

math::AxisAlignedBox2<int> Surface::getViewport() const
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	return math::AxisAlignedBox2<int>(
		viewport[0], 
		viewport[1], 
		viewport[0] + viewport[2],
		viewport[1] + viewport[3]
	);
}

Vector2_r Surface::normalizedToWindow(const Vector2_r& v) const
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	return Vector2_r {
		static_cast<real_t>(viewport[0]) + 0.5_r * static_cast<real_t>(viewport[2]) * (v[0] + 1.0_r),
		static_cast<real_t>(viewport[1]) + 0.5_r * static_cast<real_t>(viewport[3]) * (v[1] + 1.0_r)
	};
}

Vector3_r Surface::normalizedToWindow(const Vector3_r& v) const
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	return Vector3_r{
		static_cast<real_t>(viewport[0]) + 0.5_r * static_cast<real_t>(viewport[2]) * (v[0] + 1.0_r),
		static_cast<real_t>(viewport[1]) + 0.5_r * static_cast<real_t>(viewport[3]) * (v[1] + 1.0_r),
		0.5_r * (v[2] + 1.0_r)
	};
}

Vector2_r Surface::windowToNormalized(const Vector2_r& v) const
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	return Vector2_r{
		2.0_r * (v[0] - static_cast<real_t>(viewport[0])) / static_cast<real_t>(viewport[2]) - 1.0_r,
		2.0_r * (v[1] - static_cast<real_t>(viewport[1])) / static_cast<real_t>(viewport[3]) - 1.0_r,
	};
}

Vector3_r Surface::windowToNormalized(const Vector3_r& v) const
{
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	return Vector3_r{
		2.0_r * (v[0] - static_cast<real_t>(viewport[0])) / static_cast<real_t>(viewport[2]) - 1.0_r,
		2.0_r * (v[1] - static_cast<real_t>(viewport[1])) / static_cast<real_t>(viewport[3]) - 1.0_r,
		2.0_r * v[2] - 1.0_r
	};
}

} }
