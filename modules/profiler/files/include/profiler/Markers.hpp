#pragma once

#include <NonCopyable.hpp>
#include <cstdarg>

namespace pm { namespace profiler {

///
struct Block : NonMovable, NonCopyable {

	/// Creates a new empty profiling block object.
	/// No profiling block is started by this constructor.
	Block();

	/// Equivalent to `Block blk; blk(msg, ...);`.
	Block(const char* msg, ...);

	/// Calls `operator()`
	~Block();

	/// Terminates the current profiling block (if any), and begins a new one.
	/// @param msg `printf` style format string
	void operator()(const char* msg, ...);

	/// Terminates the profiling block.
	void operator()();

#if defined(POLYMORPH_PROFILING_ENABLED)
private:
	void start();
	void stop();
	void push();
	void pop();

	void*  _impl;
	Block* _prev;
	char   _strbuf[64];
#endif
};

// When profiling is turned off provide zero-overhead implementations for the
// functions that are easily optimized out by the compiler
#if !defined(POLYMORPH_PROFILING_ENABLED)
inline Block::Block() {}
inline Block::Block(const char*, ...) {}
inline Block::~Block() {}
inline void Block::operator()(const char* msg, ...) {}
inline void Block::operator()() {}
#endif

} }
