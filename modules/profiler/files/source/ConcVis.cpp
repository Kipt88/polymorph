#include <profiler/Markers.hpp>
#include <Assert.hpp>
#include <Scoped.hpp>
#include <system_error>
#include <cvmarkers.h>

namespace pm { namespace profiler {
namespace {
// some sanity checks since we don't expose the real types in the interface
static_assert(std::is_convertible<PCV_SPAN, void*>::value,
	"cannot store a CV span into a void pointer");

struct Provider {

	Provider()
	{
		const auto hres = CvCreateDefaultMarkerSeriesOfDefaultProvider(&_provider, &_series);
		if (FAILED(hres))
			throw std::system_error(hres, std::system_category(),
				"Failed to initialize CV profiler handles");
	}

	~Provider()
	{
		CvReleaseMarkerSeries(_series);
		CvReleaseProvider(_provider);
	}

	// for convenience of use
	operator PCV_PROVIDER()     const { return _provider; }
	operator PCV_MARKERSERIES() const { return _series; }

private:
	PCV_PROVIDER _provider;
	PCV_MARKERSERIES _series;
};

// to avoid the overhead of thread-safe static variables we construct the
// necessary provider handles during static initialization; it's unlikely
// that the profiler functions will be used during that time
Provider cvProvider;

// Concurrency Visualizer doesn't display nested spans clearly, so we maintain
// a linked list of active profiler blocks which we manually stop and restart
// as they are entered and exited
thread_local Block* currentBlock = nullptr;

void* enterSpan(const char* msg)
{
	PCV_SPAN span;
	VERIFY(SUCCEEDED(CvEnterSpanA(cvProvider, &span, msg)));
	return span;
}

void* leaveSpan(void* span)
{
	// NOTE: it's safe to call CvLeaveSpan with a nullptr
	VERIFY(SUCCEEDED(CvLeaveSpan(static_cast<PCV_SPAN>(span))));
	return nullptr;
}

} // anonymous namespace

Block::Block()
	: _impl(nullptr)
	, _prev(nullptr)
{
}

Block::Block(const char* msg, ...)
	: _impl(nullptr)
	, _prev(nullptr)
{
	va_list args = nullptr;
	va_start(args, msg);
	::vsnprintf(_strbuf, sizeof(_strbuf), msg, args);
	va_end(args);

	// always guarantee null-termination
	_strbuf[sizeof(_strbuf)-1] = '\0';

	push();
}

void Block::operator()(const char* msg, ...)
{
	va_list args = nullptr;
	va_start(args, msg);
	::vsnprintf(_strbuf, sizeof(_strbuf), msg, args);
	va_end(args);

	// always guarantee null-termination
	_strbuf[sizeof(_strbuf)-1] = '\0';

	// ensure previous block is closed
	// before opening a new one
	stop();
	start();
}

void Block::operator()()
{
	pop();
}

void Block::start()
{
	ASSERT(_impl == nullptr, "implementation error");
	_impl = enterSpan(_strbuf);
}

void Block::stop()
{
	_impl = leaveSpan(_impl);
}

void Block::push()
{
	if (_prev = currentBlock)
		_prev->stop();

	start();

	currentBlock = this;
}

void Block::pop()
{
	stop();

	if (currentBlock = _prev)
		currentBlock->start();
}

Block::~Block()
{
	if (_impl)
		pop();
}

} }
