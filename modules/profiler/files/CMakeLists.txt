set (dependent_modules
	ext
)

set (source_files
)

set (include_files
	include/profiler/Markers.hpp
)

require_modules (${dependent_modules})

source_group ("Sources" ".*cpp")
source_group ("Headers" ".*hpp")

if(WIN32)
	find_package (ConcVis)

	if (CONCVIS_FOUND)
		list (APPEND source_files
			source/ConcVis.cpp
		)
	else()
		message ("Concurrency Visualizer not installed, disabling profiler")
	endif()
endif()

if (source_files)
	add_library(
		profiler STATIC
		${source_files}
		${include_files}
	)

	target_include_directories (profiler PUBLIC include)
	target_link_libraries (profiler PUBLIC ${dependent_modules})

	if (CONCVIS_FOUND)
		target_compile_definitions (profiler PUBLIC POLYMORPH_PROFILING_ENABLED)
		target_include_directories (profiler PRIVATE ${CONCVIS_INCLUDE_DIRS})
	endif()
else()
	add_library (profiler INTERFACE)
	target_include_directories (profiler INTERFACE include)
endif()
