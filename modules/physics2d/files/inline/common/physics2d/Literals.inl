#include "physics2d/Literals.hpp"

namespace pm { 

constexpr physics2d::real_t operator "" _kg(long double d)
{
	return static_cast<physics2d::real_t>(d);
}

constexpr physics2d::real_t operator "" _g(long double d)
{
	return static_cast<physics2d::real_t>(d * 0.001);
}

constexpr physics2d::real_t operator "" _km(long double d)
{
	return static_cast<physics2d::real_t>(d * 1000.0);
}

constexpr physics2d::real_t operator "" _m(long double d)
{
	return static_cast<physics2d::real_t>(d);
}

constexpr physics2d::real_t operator "" _mm(long double d)
{
	return static_cast<physics2d::real_t>(d * 0.001);
}

constexpr physics2d::real_t operator "" _mps(long double d)
{
	return static_cast<physics2d::real_t>(d);
}

constexpr physics2d::real_t operator "" _kph(long double d)
{
	return static_cast<physics2d::real_t>(d * 3.6);
}

constexpr physics2d::real_t operator "" _mps2(long double d)
{
	return static_cast<physics2d::real_t>(d);
}

constexpr physics2d::real_t operator "" _rad(long double d)
{
	return static_cast<physics2d::real_t>(d);
}

constexpr physics2d::real_t operator "" _deg(long double d)
{
	return static_cast<physics2d::real_t>(d * 3.1415926535897932384626433832795 / 180.0);
}

constexpr physics2d::real_t operator "" _radps(long double d)
{
	return static_cast<physics2d::real_t>(d);
}

constexpr physics2d::real_t operator "" _degps(long double d)
{
	return static_cast<physics2d::real_t>(d * 3.1415926535897932384626433832795 / 180.0);
}

constexpr physics2d::real_t operator "" _kgpm2(long double d)
{
	return static_cast<physics2d::real_t>(d);
}

constexpr physics2d::real_t operator "" _N(long double d)
{
	return static_cast<physics2d::real_t>(d);
}

constexpr physics2d::real_t operator "" _J(long double d)
{
	return static_cast<physics2d::real_t>(d);
}

constexpr physics2d::real_t operator "" _kJ(long double d)
{
	return static_cast<physics2d::real_t>(1000.0 * d);
}

constexpr physics2d::real_t operator "" _MJ(long double d)
{
	return static_cast<physics2d::real_t>(1000000.0 * d);
}

constexpr physics2d::real_t operator "" _Jps(long double d)
{
	return static_cast<physics2d::real_t>(d);
}

constexpr physics2d::real_t operator "" _kJps(long double d)
{
	return static_cast<physics2d::real_t>(1000.0 * d);
}

constexpr physics2d::real_t operator "" _MJps(long double d)
{
	return static_cast<physics2d::real_t>(1000000.0 * d);
}

} 
