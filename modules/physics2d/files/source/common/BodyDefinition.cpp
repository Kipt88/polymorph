#include "physics2d/BodyDefinition.hpp"

namespace pm { namespace physics2d {

namespace {

bool defaultPreSolve(Scene&, ShapeHandle&, ShapeHandle&)
{
	return true;
}

void defaultPostSolve(Scene&, ShapeHandle&, ShapeHandle&, const ContactData&)
{
}

}

ShapeDefinition::PolygonDef::PolygonDef(const std::initializer_list<Vector2_r>& vertices_)
{
	ASSERT(vertices_.size() <= MAX_POLYGON_VERTICES, "Too many vertices for polygon.");

	size = static_cast<unsigned int>(vertices_.size());

	for (unsigned int i = 0; i < size; ++i)
		vertices[i] = *(vertices_.begin() + i);
}

ShapeDefinition::PolygonDef::PolygonDef(const std::vector<Vector2_r>& vertices_)
{
	ASSERT(vertices_.size() <= MAX_POLYGON_VERTICES, "Too many vertices for polygon.");

	size = vertices_.size();

	for (unsigned int i = 0; i < size; ++i)
		vertices[i] = vertices_[i];
}

ShapeDefinition::ShapeDefinition()
: geometry{},
  localPos({ 0, 0 }),
  material(),
  type(0),
  filter({ 0, 0, 0 }),
  userData(nullptr)
{
	material.density = 1.0;
	material.elasticity = 1.0;
	material.friction = 0.0;

	filter.categories.flip();
	filter.mask.flip();
}

ShapeDefinition::ShapeDefinition(const CircleDef& circle_)
: shape(Shape::CIRCLE),
  geometry{},
  localPos({ 0, 0 }),
  material(),
  type(0),
  filter({ 0, 0, 0 }),
  userData(nullptr)
{
	geometry.circle = circle_;

	material.density = 1.0;
	material.elasticity = 1.0;
	material.friction = 0.0;

	filter.categories.flip();
	filter.mask.flip();
}

ShapeDefinition::ShapeDefinition(const BoxDef& box_)
: shape(Shape::BOX),
  geometry{},
  localPos({ 0, 0 }),
  material(),
  type(0),
  filter({ 0, 0, 0 }),
  userData(nullptr)
{
	geometry.box = box_;

	material.density = 1.0;
	material.elasticity = 1.0;
	material.friction = 0.0;

	filter.categories.flip();
	filter.mask.flip();
}

ShapeDefinition::ShapeDefinition(const PolygonDef& polygon_)
: shape(Shape::POLYGON),
  geometry{},
  localPos({ 0, 0 }),
  material(),
  type(0),
  filter({ 0, 0, 0 }),
  userData(nullptr)
{
	geometry.polygon = polygon_;

	material.density = 1.0;
	material.elasticity = 1.0;
	material.friction = 0.0;

	filter.categories.flip();
	filter.mask.flip();
}

BodyDefinition::BodyDefinition()
: shapes(),
  bullet(false),
  type(BodyType::DYNAMIC),
  userData(nullptr),
  preSolveFunc(defaultPreSolve),
  postSolveFunc(defaultPostSolve)
{
}

} }
