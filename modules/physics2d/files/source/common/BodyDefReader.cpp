#include "physics2d/BodyDefReader.hpp"

#include <cstddef>

namespace pm { namespace physics2d {

namespace BodyDefReader {

/*
 * File format:
 * 
 *	body type (dynamic 0, kinetic 1, static 2)
 *	shape count
 *  shapes:
 *		shape type
 *		shape definition
 *		local position
 *		material
 *		collision type
 *		filter
 */

BodyDefinition read(io::InputStream& input)
{
	try
	{
		BodyDefinition bodyDef;

		{
			std::uint32_t type;
			input >> type;

			bodyDef.type = static_cast<BodyType>(type);
		}

		{
			bool bullet;
			input >> bullet;

			bodyDef.bullet = bullet;
		}

		switch (bodyDef.type)
		{
		case BodyType::DYNAMIC:
		case BodyType::KINEMATIC:
		case BodyType::STATIC:
			break;
		default:
			throw PhysicsResourceException("Unknown body type.");
		}

		std::uint32_t shapes;
		input >> shapes;

		for (std::uint32_t i = 0; i < shapes; ++i)
		{
			Shape shapeType;

			{
				std::uint32_t type;
				input >> type;

				shapeType = static_cast<Shape>(type);
			}

			switch (shapeType)
			{
			case Shape::CIRCLE:
			{
				float radius;
				input >> radius;

				bodyDef.shapes.push_back(
					ShapeDefinition{
						ShapeDefinition::CircleDef{ static_cast<real_t>(radius) }
					}
				);

				break;
			}
			case Shape::BOX:
			{
				float w;
				float h;

				input >> w >> h;

				bodyDef.shapes.push_back(
					ShapeDefinition{
						ShapeDefinition::BoxDef{
							static_cast<real_t>(w),
							static_cast<real_t>(h)
						}
					}
				);

				break;
			}
			case Shape::POLYGON:
			{
				ShapeDefinition::PolygonDef polygon;

				{
					std::uint32_t s;
					input >> s;

					if (s > MAX_POLYGON_VERTICES)
						throw PhysicsResourceException("Too many polygon vertices.");

					polygon.size = s;
				}

				for (unsigned int i = 0; i < polygon.size; ++i)
				{
					float x;
					float y;

					input >> x >> y;

					polygon.vertices[i][0] = static_cast<real_t>(x);
					polygon.vertices[i][1] = static_cast<real_t>(y);
				}

				bodyDef.shapes.push_back(ShapeDefinition{ polygon });

				break;
			}
			default:
				throw PhysicsResourceException("Unknown shape type.");
			}

			auto& shape = bodyDef.shapes.back();

			{
				float x;
				float y;

				input >> x >> y;

				shape.localPos[0] = static_cast<real_t>(x);
				shape.localPos[1] = static_cast<real_t>(y);
			}

			{
				float density;
				float friction;
				float elasticity;

				input >> density >> friction >> elasticity;

				shape.material.density = static_cast<real_t>(density);
				shape.material.friction = static_cast<real_t>(friction);
				shape.material.elasticity = static_cast<real_t>(elasticity);
			}

			{
				std::uint64_t collisionType;

				input >> collisionType;

				shape.type = static_cast<collision_type_t>(collisionType);
			}

			{
				std::uint64_t filterGroup;
				std::uint32_t filterCategories;
				std::uint32_t filterMask;

				input >> filterGroup >> filterCategories >> filterMask;

				shape.filter.group = static_cast<collision_group_t>(filterGroup);
				shape.filter.categories = bitmask_t{ filterCategories };
				shape.filter.mask = bitmask_t{ filterMask };
			}

			shape.userData = nullptr;
		}

		if (bodyDef.shapes.empty())
			throw PhysicsResourceException("Couldn't find any shapes defined.");

		return bodyDef;
	}
	catch (const io::IOException& e)
	{
		throw PhysicsResourceException("IOException: " + std::string(e.what()));
	}
}

}

} }
