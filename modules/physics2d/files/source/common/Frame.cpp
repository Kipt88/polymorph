#include "physics2d/Frame.hpp"

#include "physics2d/Physics.hpp"

namespace pm { namespace physics2d {

ClientFrame::ClientFrame(Physics& system)
: _queue(),
  _system(system)
{
}

void ClientFrame::createBody(body_id_t id, const BodyDefinition& def, body_user_data_t data)
{
	withScene(
		[=](Scene& scene)
		{
			scene.create(def, id).setUserData(data);
		}
	);
}

void ClientFrame::removeBody(body_id_t id)
{
	withScene(
		[=](Scene& scene)
		{
			scene.remove(scene.get(id));
		}
	);
}

void ClientFrame::setPosition(body_id_t id, const Vector2_r& pos)
{
	withBody(
		id,
		[=](BodyHandle body)
		{
			body.setPosition(pos);
		}
	);
}

void ClientFrame::setRotation(body_id_t id, rad_t angle)
{
	withBody(
		id,
		[=](BodyHandle body)
		{
			body.setAngle(angle);
		}
	);
}

void ClientFrame::setVelocity(body_id_t id, const Vector2_r& v)
{
	withBody(
		id,
		[=](BodyHandle body)
		{
			body.setVelocity(v);
		}
	);
}


void ClientFrame::applyForce(body_id_t id, const Vector2_r& force, const Vector2_r& localPos)
{
	withBody(
		id,
		[=](BodyHandle body)
		{
			body.addForce(force, localPos);
		}
	);
}

void ClientFrame::applyImpulse(body_id_t id, const Vector2_r& impulse, const Vector2_r& localPos)
{
	withBody(
		id,
		[=](BodyHandle body)
		{
			body.applyImpulse(impulse, localPos);
		}
	);
}

void ClientFrame::setGravity(const Vector2_r& gravity)
{
	withScene(
		[=](Scene& scene)
		{
			scene.setGravity(gravity);
		}
	);
}

void ClientFrame::setDamping(real_t d)
{
	ASSERT(d >= 0, "Damping must be positive.");
	withScene(
		[=](Scene& scene)
		{
			scene.setDamping(d);
		}
	);
}

void ClientFrame::flush()
{
	_system.get()._system.pushAction(
		[queue = std::move(_queue)](SystemRunner& runner)
		{
			runner.scene().resetAllForces();

			for (auto& action : queue)
				action(runner);
		}
	);
}

ClientFrame::~ClientFrame()
{
	flush();
}

} }
