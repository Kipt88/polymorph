#include "physics2d/BodyProvider.hpp"

#include "physics2d/BodyDefReader.hpp"
#include <filesystem/FileInputStream.hpp>

namespace pm { namespace physics2d {

BodyProvider::BodyProvider(const path_t& root)
: _loader(),
  _bodiesMutex(),
  _bodies(),
  _root(root)
{
}

BodyProvider::LoadResult BodyProvider::loadBodyDefinition(body_def_id_t id)
{
	return _loader.pushAction(
		[this, id]
		{
			auto bodyHandle = get(id);

			if (bodyHandle)
				return bodyHandle;

			const auto& data = getBodyDefMetaData();
			auto it = data.find(id);

			ASSERT(it != data.end(), "Body data not registered.");

			filesystem::FileInputStream input(it->second.path.string());

			auto&& def = BodyDefReader::read(input);

			bodyHandle = std::make_shared<BodyDefinition>(std::move(def));

			{
				concurrency::MutexLockGuard lock(_bodiesMutex);

				_bodies.insert(std::make_pair(id, bodyHandle));
			}

			return bodyHandle;
		}
	);
}

BodyDefinitionHandle BodyProvider::get(body_def_id_t id)
{
	concurrency::MutexLockGuard lock(_bodiesMutex);

	if (_bodies.has(id))
		return _bodies.find(id)->second;
	else
		return nullptr;
}

BodyProvider::body_def_id_t BodyProvider::registerBodyDefinition(body_def_id_t id, const path_t& path)
{
	auto& meta = getBodyDefMetaData();
	ASSERT(meta.find(id) == meta.end(), "Trying to overwrite body data.");

	meta.insert(std::make_pair(id, BodyDefMetaData{ path }));

	return id;
}

std::map<BodyProvider::body_def_id_t, BodyProvider::BodyDefMetaData>& BodyProvider::getBodyDefMetaData()
{
	static std::map<body_def_id_t, BodyDefMetaData> data;
	return data;
}

} }
