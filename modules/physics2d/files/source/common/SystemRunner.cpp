#include "physics2d/SystemRunner.hpp"

namespace pm { namespace physics2d {

SystemRunner::SnapshotBuffer::SnapshotBuffer(const Time::time_point& t)
: mutex(),
  updateCondition(),
  collisions(),
  old(t),
  first(t),
  second(t)
{
}


SystemRunner::SystemRunner(concurrency::RunnerTasks<SystemRunner>&, real_t physicsScale)
: _scene(physicsScale),
  _snapshots(Time::now())
{
}

bool SystemRunner::update(const concurrency::FrameTime& time)
{
	auto collisionEvents = _scene.update(time.delta);

	std::vector<CollisionPairData> collisions;
	collisions.reserve(collisionEvents.size());

	for (const auto& collidingShapes : collisionEvents)
	{
		collisions.push_back(
			{
				BodyHandle::enclosing(collidingShapes.shapeA).id(),
				BodyHandle::enclosing(collidingShapes.shapeB).id(),
				collidingShapes.position,
				collidingShapes.impulse,
				collidingShapes.firstContact
			}
		);
	}

	{
		concurrency::MutexLockGuard lock(_snapshots.mutex);

		_snapshots.collisions = std::move(collisions);
		_snapshots.old = std::move(_snapshots.first);
		_snapshots.first = std::move(_snapshots.second);
		_snapshots.second = Snapshot{ time.t1, _scene };
	}

	_snapshots.updateCondition.notify_all();

	return true;
}

Scene& SystemRunner::scene()
{
	return _scene;
}

const SystemRunner::SnapshotBuffer& SystemRunner::snapshots() const
{
	return _snapshots;
}

} }
