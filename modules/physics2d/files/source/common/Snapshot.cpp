#include "physics2d/Snapshot.hpp"

#include <math/Geometry.hpp>
#include <Debug.hpp>
#include <algorithm>
#include <array>

namespace pm { namespace physics2d {

namespace {

const char* LOG_TAG = "physics2d::Snapshot";

bool doesCollide(const CollisionFilter& a, const CollisionFilter& b)
{
	if (a.group)
	{
		if (a.group == b.group)
			return false;
	}

	return (a.mask & b.categories).any();
}

math::single_intersection_param_t hitTest(
	const Vector2_r& p0, 
	const Vector2_r& p1, 
	const ShapeDefinition::CircleDef& circle
)
{
	return math::lineCircleIntersectionParam(p0, p1, Vector2_r::ZERO, circle.radius).first;
}

math::single_intersection_param_t hitTest(
	const Vector2_r& p0,
	const Vector2_r& p1,
	const ShapeDefinition::PolygonDef& polygon
)
{
	auto intersections = math::linePolygonIntersectionParam(
		polygon.vertices.begin(),
		polygon.vertices.begin() + polygon.size,
		p0,
		p1
	);

	if (intersections.empty())
		return std::nullopt;

	return intersections.front();
}

math::single_intersection_param_t hitTest(
	const Vector2_r& p0,
	const Vector2_r& p1,
	const ShapeDefinition::BoxDef& box
)
{
	// NOTE: Could be done faster since box is axis-aligned.

	std::array<Vector2_r, 4> vertices{{
		{ static_cast<real_t>(-0.5) * box.width, static_cast<real_t>(-0.5) * box.height },
		{ static_cast<real_t>( 0.5) * box.width, static_cast<real_t>(-0.5) * box.height },
		{ static_cast<real_t>( 0.5) * box.width, static_cast<real_t>( 0.5) * box.height },
		{ static_cast<real_t>(-0.5) * box.width, static_cast<real_t>( 0.5) * box.height }
	}};

	auto intersections = math::linePolygonIntersectionParam(
		vertices.begin(),
		vertices.end(),
		p0,
		p1
	);

	if (intersections.empty())
		return std::nullopt;

	return intersections.front();
}

math::single_intersection_param_t hitTest(
	const Vector2_r& p0,
	const Vector2_r& p1,
	const ShapeState& shape
)
{
	switch (shape.shape)
	{
	case Shape::CIRCLE:
		return hitTest(p0, p1, shape.geometry.circle);
	case Shape::POLYGON:
		if (math::lineCircleIntersectionParam(p0, p1, Vector2_r::ZERO, shape.boundingRadius).first)
			return hitTest(p0, p1, shape.geometry.polygon);
		else
			return std::nullopt;
		
	case Shape::BOX:
		return hitTest(p0, p1, shape.geometry.box);
	}

	abort();
}

}

Snapshot::Snapshot(const Time::time_point& time)
: _bodies(),
  _time(time)
{
}

Snapshot::Snapshot(const Time::time_point& time, const Scene& scene)
: _bodies(),
  _time(time)
{
	for (const auto& entry : scene)
	{
		auto body = entry.second;
		
		std::vector<ShapeState> shapes;

		for (const auto& shape : body.getShapes())
		{
			shapes.push_back(
				{
					shape.getShape(),
					shape.getGeometry(),
					shape.getCollisionFilter(),
					shape.getLocalPosition(),
					shape.getBoundingRadius(),
					shape.getUserData()
				}
			);
		}

		Transform2_r trans;
		trans.translation = body.getPosition();
		trans.rotation = body.getAngle();
		trans.scale = real_t{ 1.0 };

		_bodies.push_back(
			{
				std::move(shapes),
				trans, 
				body.getVelocity(),
				body.getAngularVelocity(),
				body.getMass(),
				body.getUserData(), 
				body.id() 
			}
		);
	}

	std::sort(
		_bodies.begin(), 
		_bodies.end(),
		[](const BodyState& a, const BodyState& b)
		{
			return a.id < b.id;
		}
	);
}

const std::vector<BodyState>& Snapshot::bodies() const
{
	return _bodies;
}

const Time::time_point& Snapshot::timestamp() const
{
	return _time;
}

std::optional<CastHit> Snapshot::castLine(
	const Vector2_r& p0,
	const Vector2_r& p1,
	const CollisionFilter& filter
) const
{
	std::optional<CastHit> hit;
	float hitTime = 1.0f;

	for (const auto& body : _bodies)
	{
		for (const auto& shape : body.shapes)
		{
			if (!doesCollide(filter, shape.filter))
				continue;

			auto worldToShape = (body.transform * Transform2_r::createTranslation(shape.localPos)).inverse();

			auto localP0 = worldToShape * p0;
			auto localP1 = worldToShape * p1;

			if (auto intersection = hitTest(localP0, localP1, shape))
			{
				if (hitTime > *intersection)
				{
					hitTime = *intersection;
					hit = CastHit{
						math::curveLine(p0, p1, hitTime),
						Vector2_r::ZERO, // TODO Get normal...
						shape
					};
				}
			}
		}
	}

	return hit;
}

Snapshot Snapshot::interpolate(const Snapshot& t0, const Snapshot& t1, const Time::time_point& time)
{
	
	/*
	{
		DEBUG_OUT(
			LOG_TAG, 
			"0 %.4f %.4f", 
			toSeconds<float>(time - t0._time),
			toSeconds<float>(t1._time - t0._time)
		);
	}
	*/

	ASSERT(t0._time <= t1._time, "Input argument not sorted by time.");

	if (time <= t0._time)
	{
		ERROR_OUT(LOG_TAG, "Failing to interpolate physics snapshot, time too old.");
		return t0;
	}
	else if (time >= t1._time)
	{
		ERROR_OUT(LOG_TAG, "Failing to interpolate physics snapshot, time too new.");
		return t1;
	}

	auto r = static_cast<real_t>((time - t0._time).count()) / (t1._time - t0._time).count();

	Snapshot snapshot(time);

	auto it0 = t0._bodies.begin();
	auto it1 = t1._bodies.begin();

	while (it0 != t0._bodies.end() && it1 != t1._bodies.end())
	{
		// Note: All bodies are sorted by id.

		if (it0->id == it1->id)
		{
			BodyState state;

			state.transform.translation = math::lerp(it0->transform.translation, it1->transform.translation, r);
			state.transform.rotation = math::lerp(it0->transform.rotation, it1->transform.rotation, r);
			state.transform.scale = real_t{ 1.0 };

			state.velocity = math::lerp(it0->velocity, it1->velocity, r);
			state.angularVelocity = math::lerp(it0->angularVelocity, it1->angularVelocity, r);

			state.mass = it1->mass;

			state.userData = it1->userData;
			state.id = it1->id;

			state.shapes = it1->shapes;

			snapshot._bodies.push_back(state);

			++it0;
			++it1;
		}
		else
		{
			if (it0->id < it1->id)
				++it0;
			else
				++it1;
		}
	}

	return snapshot;
}

} }
