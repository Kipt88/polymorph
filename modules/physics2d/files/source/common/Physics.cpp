#include "physics2d/Physics.hpp"
#include <Debug.hpp>

namespace pm { namespace physics2d {

namespace {

const SystemRunner::SnapshotBuffer& 
	fetchBuffers(concurrency::ConcurrentSystem<SystemRunner>& system)
{
	auto res = system.pushAction(
		[](SystemRunner& runner)
		{
			return &runner.snapshots();
		}
	);

	return *res.get();
}

}

Physics::Physics(const Time::duration& sync, real_t physicsScale)
: _system(sync, physicsScale),
  _buffers(fetchBuffers(_system)),
  _bodyProvider()
{
	DEBUG_OUT("PhysicsSystem", "Created.");
}

State Physics::waitForState(const Time::time_point& from, const Time::time_point& to) const
{
	ASSERT(from < to, "Timestamps are messed up.");

	// FIXME: If system has died, this will segfault.

	concurrency::MutexLockGuard lock(_buffers.mutex);

	if (to > _buffers.second.timestamp())
	{
		/*
		DEBUG_OUT(
			"PhysicsSystem", 
			"Need to wait for state at least (%.2fms)", 
			toSeconds<float>(to - _buffers.second.timestamp()) * 1000.0f
		);
		*/

		_buffers.updateCondition.wait(
			lock,
			[this, to]
			{
				return to <= _buffers.second.timestamp();
			}
		);
	}

	auto t0 = _buffers.first.timestamp();

	if (to < t0)
	{
		return {
			std::vector<CollisionPairData>{},
			Snapshot::interpolate(_buffers.old, _buffers.first, to)
		};
	}

	auto t1 = _buffers.second.timestamp();

	// Pretend all events have ToI in middle of frame, chipmunk doesn't supply ToI.
	Time::time_point eventTime = t0 + (t1 - t0) / 2;

	bool passingEventTime = from <= eventTime && eventTime < to;

	return { 
		passingEventTime ? _buffers.collisions : std::vector<CollisionPairData>{},
		Snapshot::interpolate(_buffers.first, _buffers.second, to)
	};
}

ClientFrame Physics::createFrame()
{
	return ClientFrame(*this);
}

BodyProvider& Physics::bodyProvider()
{
	return _bodyProvider;
}

} }
