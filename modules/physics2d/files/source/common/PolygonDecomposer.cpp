#include "physics2d/PolygonDecomposer.hpp"

#include <numeric>

namespace pm { namespace physics2d {

namespace PolygonDecomposer {

namespace {

const real_t EPSILON = 1.192092896e-07;

bool eq(float a, float b)
{
	return std::abs(a - b) <= EPSILON;
}

const Vector2_r& at(int i, const std::vector<Vector2_r>& v) 
{
	const auto s = v.size();
	return v[i < 0 ? s - (-i % s): i % s];
}

real_t area(const Vector2_r& a, const Vector2_r& b, const Vector2_r& c) 
{
	return (b[0] - a[0]) * (c[1] - a[1]) - (c[0] - a[0]) * (b[1] - a[1]);
}

bool left(const Vector2_r& a, const Vector2_r& b, const Vector2_r& c) 
{
	return area(a, b, c) > 0;
}

bool leftOn(const Vector2_r& a, const Vector2_r& b, const Vector2_r& c)
{
	return area(a, b, c) >= 0;
}

bool right(const Vector2_r& a, const Vector2_r& b, const Vector2_r& c) 
{
	return area(a, b, c) < 0;
}

bool rightOn(const Vector2_r& a, const Vector2_r& b, const Vector2_r& c) 
{
	return area(a, b, c) <= 0;
}

bool isReflex(int i, const std::vector<Vector2_r>& v)
{
	return right(at(i - 1, v), at(i, v), at(i + 1, v));
}

bool collinear(const Vector2_r& a, const Vector2_r& b, const Vector2_r& c) 
{
	return area(a, b, c) == 0;
}

real_t sqdist(const Vector2_r& a, const Vector2_r& b) 
{
	return math::lengthSq(a - b);
}

std::vector<Vector2_r> copy(int i, int j, const std::vector<Vector2_r>& v)
{
	std::vector<Vector2_r> p;
	
	if (i < j) 
	{
		p.insert(p.begin(), v.begin() + i, v.begin() + j + 1);
	}
	else 
	{
		p.insert(p.begin(), v.begin() + i, v.end());
		p.insert(p.end(), v.begin(), v.begin() + j + 1);
	}

	return p;
}

Vector2_r lineInt(const std::pair<Vector2_r, Vector2_r>& l1, const std::pair<Vector2_r, Vector2_r>& l2)
{
	Vector2_r v = Vector2_r::ZERO;
	real_t a1, b1, c1, a2, b2, c2, det;

	a1 = l1.second[1] - l1.first[1];
	b1 = l1.first[0] - l1.second[0];
	c1 = a1 * l1.first[0] + b1 * l1.first[1];
	a2 = l2.second[1] - l2.first[1];
	b2 = l2.first[0] - l2.second[0];
	c2 = a2 * l2.first[0] + b2 * l2.first[1];
	det = a1 * b2 - a2*b1;

	if (!eq(det, 0)) 
	{
		// lines are not parallel

		v[0] = (b2 * c1 - b1 * c2) / det;
		v[1] = (a1 * c2 - a2 * c1) / det;
	}

	return v;
}

bool canSee(int a, int b, const std::vector<Vector2_r>& v) 
{
	Vector2_r p = Vector2_r::ZERO;
	real_t dist;

	if (leftOn(at(a + 1, v), at(a, v), at(b, v)) && rightOn(at(a - 1, v), at(a, v), at(b, v))) 
		return false;

	dist = sqdist(at(a, v), at(b, v));
	for (int i = 0; i < v.size(); ++i) 
	{
		if ((i + 1) % v.size() == a || i == a) // ignore incident edges
			continue;

		if (leftOn(at(a, v), at(b, v), at(i + 1, v)) && rightOn(at(a, v), at(b, v), at(i, v))) 
		{
			p = lineInt(std::make_pair(at(a, v), at(b, v)), std::make_pair(at(i, v), at(i + 1, v)));
			
			if (sqdist(at(a, v), p) < dist) 
			{
				// if edge is blocking visibility to b
				return false;
			}
		}
	}

	return true;
}


}

std::vector<std::vector<Vector2_r>> decomposeToConvex(const std::vector<Vector2_r>& v, unsigned int maxVertices)
{
	typedef std::vector<Vector2_r> Polygon;

	std::vector<Polygon> min, tmp1, tmp2;
	auto nSubPolygons = std::numeric_limits<int>::max();

	for (int i = 0; i < v.size(); ++i) 
	{
		if (isReflex(i, v)) 
		{
			for (int j = 0; j < v.size(); ++j) 
			{
				if (canSee(i, j, v)) 
				{
					tmp1 = decomposeToConvex(copy(i, j, v));
					tmp2 = decomposeToConvex(copy(j, i, v));

					tmp1.insert(tmp1.end(), tmp2.begin(), tmp2.end());

					if (tmp1.size() < nSubPolygons)
					{
						min = tmp1;
						nSubPolygons = tmp1.size();
					}
				}
			}
		}
	}
	
	if (maxVertices)
	{
		for (auto it = min.begin(); it != min.end();)
		{
			if (it->size() > maxVertices)
			{
				unsigned int nextSize = std::max<unsigned int>(it->size() - maxVertices, 3);
				unsigned int thisSize = it->size() - nextSize;

				Polygon thisPolygon;
				thisPolygon.reserve(thisSize);

				Polygon nextPolygon;
				nextPolygon.reserve(nextSize);

				for (unsigned int i = 0; i < thisSize; ++i)
					thisPolygon[i] = (*it)[i];

				for (unsigned int i = 0; i < nextSize; ++i)
					nextPolygon[i] = (*it)[thisSize + i];
				
				*it = std::move(thisPolygon);
				it = min.insert(it + 1, std::move(nextPolygon));
			}
			else
			{
				++it;
			}
		}
	}

	return min.empty() ? std::vector<Polygon>{v} : min;
}

}

} }
