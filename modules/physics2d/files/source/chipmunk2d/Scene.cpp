#include "physics2d/Scene.hpp"

#include <chipmunk/chipmunk.h>
#include <Debug.hpp>
#include <algorithm>

namespace pm { namespace physics2d {

namespace {

// Collision handling:

void postSolve(cpArbiter* arb, cpSpace* space, cpDataPointer userData)
{
	auto* collisions = reinterpret_cast<Scene::collision_events_t*>(userData);

	cpShape* shapeA;
	cpShape* shapeB;

	cpArbiterGetShapes(arb, &shapeA, &shapeB);

	collisions->push_back(
		{
			ShapeHandle(shapeA),
			ShapeHandle(shapeB),
			cpArbiterTotalKE(arb),
			cpArbiterIsFirstContact(arb) == cpTrue
		}
	);
}

}


void Scene::space_deleter::operator()(cpSpace* space)
{
	cpSpaceFree(space);
}


Scene::Scene()
: _space(cpSpaceNew()),
  _bodies(),
  _collisions()
{
	cpSpaceSetIterations(_space.get(), 1);

	auto* collisionHandler = cpSpaceAddDefaultCollisionHandler(_space.get());

	collisionHandler->userData = &_collisions;
	collisionHandler->postSolveFunc = &postSolve;
}

Scene::~Scene()
{
	for (const auto& e : *this)
		deleteBody(e.second);
}

BodyHandle Scene::create(const BodyDefinition& def, body_id_t id)
{
	ASSERT(_bodies.count(id) == 0, "Trying to overwrite body.");

	return _bodies.insert(
		std::make_pair(id, BodyHandle(BodyHandle::create(_space.get(), def, id)))
	).first->second;
}

void Scene::remove(BodyHandle body)
{
	auto id = body.id();

	ASSERT(_bodies.count(id), "Can't find body.");

	deleteBody(body);

	_bodies.erase(id);
}

BodyHandle Scene::get(body_id_t id) const
{
	auto it = _bodies.find(id);
	ASSERT(it != _bodies.end(), "Can't find specified body.");

	return it->second;
}

auto Scene::begin() -> iterator
{
	return _bodies.begin();
}

auto Scene::begin() const -> const_iterator
{
	return _bodies.begin();
}

auto Scene::cbegin() const -> const_iterator
{
	return _bodies.cbegin();
}

auto Scene::end() -> iterator
{
	return _bodies.end();
}

auto Scene::end() const -> const_iterator
{
	return _bodies.end();
}

auto Scene::cend() const -> const_iterator
{
	return _bodies.cend();
}

void Scene::setGravity(const Vector2_r& gravity)
{
	cpSpaceSetGravity(_space.get(), { gravity[0], gravity[1] });
}

void Scene::setDamping(real_t d)
{
	cpSpaceSetDamping(_space.get(), d);
}

void Scene::resetAllForces()
{
	for (auto& e : *this)
		e.second.resetForces();
}

std::optional<Scene::CastHit> Scene::castFirst(const LineCast& line)
{
	cpSegmentQueryInfo info;

	auto* shape = cpSpaceSegmentQueryFirst(
		_space.get(),
		{ line.start[0], line.start[1] },
		{ line.end[0], line.end[1] },
		line.radius,
		cpShapeFilterNew(
			line.filter.group,
			static_cast<cpBitmask>(line.filter.categories.to_ulong()),
			static_cast<cpBitmask>(line.filter.mask.to_ulong())
		),
		&info
	);

	if (shape)
	{
		Vector2_r point = { info.point.x, info.point.y };
		Vector2_r normal = { info.normal.x, info.normal.y };
		real_t dist = info.alpha;

		return CastHit{
			ShapeHandle(shape),
			point,
			normal,
			dist
		};
	}

	return std::nullopt;
}

Scene::collision_events_t Scene::update(const Time::duration& dt)
{
	_collisions.clear();

	for (auto& e : *this)
		e.second.applyForces();

	cpSpaceStep(_space.get(), toSeconds<real_t>(dt));

	return _collisions;
}

void Scene::deleteBody(BodyHandle body)
{
	auto* bodyUserData = body.internalUserData();

	for (auto* shape : bodyUserData->shapes)
	{
		ShapeHandle handle(shape);

		delete handle.internalUserData();

		cpSpaceRemoveShape(_space.get(), shape);
		cpShapeDestroy(shape);
	}

	delete bodyUserData;
	cpSpaceRemoveBody(_space.get(), body._body);
	cpBodyDestroy(body._body);
}

} }
