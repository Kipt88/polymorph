#include "physics2d/Shape.hpp"

#include <chipmunk/chipmunk.h>

namespace pm { namespace physics2d {

ShapeDefinition::PolygonDef::PolygonDef(const std::initializer_list<Vector2_r>& vertices_)
{
	ASSERT(vertices_.size() <= MAX_POLYGON_VERTICES, "Too many vertices for polygon.");

	size = static_cast<unsigned int>(vertices_.size());

	for (unsigned int i = 0; i < size; ++i)
		vertices[i] = *(vertices_.begin() + i);
}

ShapeDefinition::PolygonDef::PolygonDef(const std::vector<Vector2_r>& vertices_)
{
	ASSERT(vertices_.size() <= MAX_POLYGON_VERTICES, "Too many vertices for polygon.");

	size = vertices_.size();

	for (unsigned int i = 0; i < size; ++i)
		vertices[i] = vertices_[i];
}

ShapeDefinition::ShapeDefinition()
: geometry{},
  localPos({ 0, 0 }),
  material(),
  type(0),
  filter({ 0, 0, 0 }),
  userData(nullptr)
{
	material.density = 1.0;
	material.elasticity = 1.0;
	material.friction = 0.0;

	filter.categories.flip();
	filter.mask.flip();
}

ShapeDefinition::ShapeDefinition(const CircleDef& circle_)
: shape(Shape::CIRCLE),
  geometry{},
  localPos({ 0, 0 }),
  material(),
  type(0),
  filter({ 0, 0, 0 }),
  userData(nullptr)
{
	geometry.circle = circle_;

	material.density = 1.0;
	material.elasticity = 1.0;
	material.friction = 0.0;

	filter.categories.flip();
	filter.mask.flip();
}

ShapeDefinition::ShapeDefinition(const BoxDef& box_)
: shape(Shape::BOX),
  geometry{},
  localPos({ 0, 0 }),
  material(),
  type(0),
  filter({ 0, 0, 0 }),
  userData(nullptr)
{
	geometry.box = box_;

	material.density = 1.0;
	material.elasticity = 1.0;
	material.friction = 0.0;

	filter.categories.flip();
	filter.mask.flip();
}

ShapeDefinition::ShapeDefinition(const PolygonDef& polygon_)
: shape(Shape::POLYGON),
  geometry{},
  localPos({ 0, 0 }),
  material(),
  type(0),
  filter({ 0, 0, 0 }),
  userData(nullptr)
{
	geometry.polygon = polygon_;

	material.density = 1.0;
	material.elasticity = 1.0;
	material.friction = 0.0;

	filter.categories.flip();
	filter.mask.flip();
}

ShapeHandle::ShapeHandle(cpShape* shape)
: _shape(shape)
{
}

bool ShapeHandle::isSensor() const
{
	return cpShapeGetSensor(_shape) == cpTrue;
}

void ShapeHandle::setSensor(bool v)
{
	cpShapeSetSensor(_shape, v);
}

real_t ShapeHandle::getElasticity() const
{
	return cpShapeGetElasticity(_shape);
}

void ShapeHandle::setElasticity(real_t v)
{
	cpShapeSetElasticity(_shape, v);
}

real_t ShapeHandle::getFriction() const
{
	return cpShapeGetFriction(_shape);
}

void ShapeHandle::setFriction(real_t v)
{
	cpShapeSetFriction(_shape, v);
}

collision_type_t ShapeHandle::getCollisionType() const
{
	return cpShapeGetCollisionType(_shape);
}

void ShapeHandle::setCollisionType(collision_type_t v)
{
	cpShapeSetCollisionType(_shape, v);
}

CollisionFilter ShapeHandle::getCollisionFilter() const
{
	auto filter = cpShapeGetFilter(_shape);

	CollisionFilter collisionFilter;

	collisionFilter.group = filter.group;
	collisionFilter.categories = static_cast<unsigned long>(filter.categories);
	collisionFilter.mask = static_cast<unsigned long>(filter.mask);

	return collisionFilter;
}

const Vector2_r& ShapeHandle::getLocalPosition() const
{
	return internalUserData()->localPos;
}

Shape ShapeHandle::getShape() const
{
	return internalUserData()->shape;
}

const ShapeDefinition::Geometry& ShapeHandle::getGeometry() const
{
	return internalUserData()->geometry;
}

const ShapeDefinition::BoxDef& ShapeHandle::asBox() const
{
	return internalUserData()->geometry.box;
}

const ShapeDefinition::CircleDef& ShapeHandle::asCircle() const
{
	return internalUserData()->geometry.circle;
}

shape_user_data_t ShapeHandle::getUserData() const
{
	return internalUserData()->userData;
}

void ShapeHandle::setUserData(shape_user_data_t v)
{
	internalUserData()->userData = v;
}

ShapeHandle::UserData* ShapeHandle::internalUserData() const
{
	return reinterpret_cast<UserData*>(cpShapeGetUserData(_shape));
}

cpShape* ShapeHandle::create(cpSpace* space, cpBody* body, const ShapeDefinition& def)
{
	cpShape* shape;

	real_t x = def.localPos[0];
	real_t y = def.localPos[1];

	switch (def.shape)
	{
	case Shape::CIRCLE:
		shape = cpCircleShapeNew(body, def.geometry.circle.radius, { x, y });
		break;

	case Shape::BOX:
		{
			cpVect v[4] = {
				{ x - 0.5 * def.geometry.box.width, y - 0.5 * def.geometry.box.height },
				{ x + 0.5 * def.geometry.box.width, y - 0.5 * def.geometry.box.height },
				{ x + 0.5 * def.geometry.box.width, y + 0.5 * def.geometry.box.height },
				{ x - 0.5 * def.geometry.box.width, y + 0.5 * def.geometry.box.height }
			};

			shape = cpPolyShapeNewRaw(body, 4, v, 0);
		}
		break;

	case Shape::POLYGON:
		{
			const auto& verts = def.geometry.polygon.vertices;

			std::array<cpVect, MAX_POLYGON_VERTICES> cpVectors;

			for (unsigned int i = 0; i < def.geometry.polygon.size; ++i)
				cpVectors[i] = { verts[i][0], verts[i][1] };

			shape = cpPolyShapeNewRaw(body, def.geometry.polygon.size, cpVectors.data(), 0);
		}
		break;
	}

	cpShapeSetDensity(shape, def.material.density);
	cpShapeSetElasticity(shape, def.material.elasticity);
	cpShapeSetFriction(shape, def.material.friction);
	cpShapeSetCollisionType(shape, def.type);

	// Filters should probably be re-used...
	cpShapeSetFilter(
		shape,
		cpShapeFilterNew(
			def.filter.group,
			static_cast<cpBitmask>(def.filter.categories.to_ulong()),
			static_cast<cpBitmask>(def.filter.mask.to_ulong())
		)
	);

	UserData ud = {
		def.shape,
		def.geometry,
		def.localPos,
		def.userData
	};

	cpShapeSetUserData(shape, new UserData(ud));
	cpSpaceAddShape(space, shape);

	return shape;
}

} }
