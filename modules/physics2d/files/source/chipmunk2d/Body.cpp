#include "physics2d/Body.hpp"

#include <chipmunk/chipmunk.h>

namespace pm { namespace physics2d {


BodyHandle::BodyHandle(cpBody* body)
: _body(body)
{
}

real_t BodyHandle::getMass() const
{
	return cpBodyGetMass(_body);
}

void BodyHandle::setMass(real_t v)
{
	cpBodySetMass(_body, v);
}

real_t BodyHandle::getInertia() const
{
	return cpBodyGetMoment(_body);
}

void BodyHandle::setInertia(real_t v)
{
	cpBodySetMoment(_body, v);
}

Vector2_r BodyHandle::getPosition() const
{
	auto vec = cpBodyGetPosition(_body);

	return { vec.x, vec.y };
}

void BodyHandle::setPosition(const Vector2_r& v)
{
	cpBodySetPosition(_body, { v[0], v[1] });

	if (cpBodyGetType(_body) == CP_BODY_TYPE_STATIC)
		cpSpaceReindexShapesForBody(cpBodyGetSpace(_body), _body);
}

rad_t BodyHandle::getAngle() const
{
	return cpBodyGetAngle(_body);
}

void BodyHandle::setAngle(rad_t v)
{
	cpBodySetAngle(_body, v);
	cpSpaceReindexShapesForBody(cpBodyGetSpace(_body), _body);
}

Vector2_r BodyHandle::getVelocity() const
{
	auto v = cpBodyGetVelocity(_body);

	return { v.x, v.y };
}

void BodyHandle::setVelocity(const Vector2_r& v)
{
	cpBodySetVelocity(_body, cpVect{ v[0], v[1] });
}

void BodyHandle::applyImpulse(const Vector2_r& impulse, const Vector2_r& localPos)
{
	cpBodyApplyImpulseAtLocalPoint(_body, { impulse[0], impulse[1] }, { localPos[0], localPos[1] });
}

void BodyHandle::addForce(const Vector2_r& force, const Vector2_r& localPos)
{
	internalUserData()->forces.push_back({ force, localPos });
}

void BodyHandle::resetForces()
{
	internalUserData()->forces.clear();
}

void BodyHandle::applyForces()
{
	for (const auto& f : internalUserData()->forces)
		cpBodyApplyForceAtLocalPoint(_body, { f.force[0], f.force[1] }, { f.localPos[0], f.localPos[1] });
}

Transform2_r BodyHandle::getTransform() const
{
	return Transform2_r{ getPosition(), static_cast<real_t>(1), getAngle() };
}

std::vector<ShapeHandle> BodyHandle::getShapes() const
{
	std::vector<ShapeHandle> shapes;

	for (const auto& shape : internalUserData()->shapes)
		shapes.push_back(ShapeHandle(shape));

	return shapes;
}

void BodyHandle::removeShape(unsigned int index)
{
	auto* userData = internalUserData();

	auto* shape = userData->shapes[index];
	ShapeHandle handle(shape);

	delete handle.internalUserData();
	
	auto space = cpBodyGetSpace(_body);

	cpSpaceRemoveShape(space, shape);
	cpShapeDestroy(shape);

	userData->shapes.erase(userData->shapes.begin() + index);
}

body_user_data_t BodyHandle::getUserData() const
{
	return reinterpret_cast<UserData*>(cpBodyGetUserData(_body))->userData;
}

void BodyHandle::setUserData(body_user_data_t v)
{
	reinterpret_cast<UserData*>(cpBodyGetUserData(_body))->userData = v;
}

body_id_t BodyHandle::id() const
{
	return internalUserData()->id;
}

BodyHandle BodyHandle::enclosing(ShapeHandle shape)
{
	return BodyHandle(cpShapeGetBody(shape._shape));
}

cpBody* BodyHandle::create(cpSpace* space, const BodyDefinition& def, body_id_t id)
{
	cpBody* body;

	switch (def.type)
	{
	case BodyType::DYNAMIC:
		body = cpBodyNew(0, 0);
		break;
	case BodyType::KINETIC:
		body = cpBodyNewKinematic();
		break;
	case BodyType::STATIC:
		body = cpBodyNewStatic();
		break;
	}

	std::vector<cpShape*> shapes;

	for (const auto& shapeDef : def.shapes)
	{
		shapes.push_back(ShapeHandle::create(space, body, shapeDef));
	}

	cpBodySetUserData(body, new UserData{ id, {}, shapes, nullptr });

	cpSpaceAddBody(space, body);

	return body;
}

BodyHandle::UserData* BodyHandle::internalUserData() const
{
	return reinterpret_cast<UserData*>(cpBodyGetUserData(_body));
}

} }
