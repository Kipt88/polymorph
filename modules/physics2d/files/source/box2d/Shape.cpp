#include "physics2d/Shape.hpp"

#include <Box2D/Box2D.h>

namespace pm { namespace physics2d {

ShapeHandle::ShapeHandle(b2Fixture* shape)
: _shape(shape)
{
}

bool ShapeHandle::isSensor() const
{
	return _shape->IsSensor();
}

void ShapeHandle::setSensor(bool v)
{
	_shape->SetSensor(v);
}

real_t ShapeHandle::getElasticity() const
{
	// Not possible?
	return 1.0f;
}

void ShapeHandle::setElasticity(real_t v)
{
	// cpShapeSetElasticity(_shape, v);
	// Not possible?
}

real_t ShapeHandle::getFriction() const
{
	return _shape->GetFriction();
}

void ShapeHandle::setFriction(real_t v)
{
	_shape->SetFriction(v);
}

collision_type_t ShapeHandle::getCollisionType() const
{
	// TODO
	return 0; // cpShapeGetCollisionType(_shape);
}

void ShapeHandle::setCollisionType(collision_type_t v)
{
	// TODO
	// cpShapeSetCollisionType(_shape, v);
}

CollisionFilter ShapeHandle::getCollisionFilter() const
{
	auto filter = _shape->GetFilterData();
	
	CollisionFilter collisionFilter;

	collisionFilter.group = filter.groupIndex;
	collisionFilter.categories = static_cast<unsigned long>(filter.categoryBits);
	collisionFilter.mask = static_cast<unsigned long>(filter.maskBits);
	
	return collisionFilter;
}

void ShapeHandle::setCollisionFilter(const CollisionFilter& filter)
{
	b2Filter boxFilter;

	boxFilter.groupIndex = filter.group;
	boxFilter.categoryBits = static_cast<unsigned long>(filter.categories.to_ulong());
	boxFilter.maskBits = static_cast<unsigned long>(filter.categories.to_ulong());

	_shape->SetFilterData(boxFilter);
}

real_t ShapeHandle::getBoundingRadius() const
{
	return internalUserData()->boundingRadius;
}

const Vector2_r& ShapeHandle::getLocalPosition() const
{
	return internalUserData()->localPos;
}

Shape ShapeHandle::getShape() const
{
	return internalUserData()->shape;
}

const ShapeDefinition::Geometry& ShapeHandle::getGeometry() const
{
	return internalUserData()->geometry;
}

const ShapeDefinition::BoxDef& ShapeHandle::asBox() const
{
	return internalUserData()->geometry.box;
}

const ShapeDefinition::CircleDef& ShapeHandle::asCircle() const
{
	return internalUserData()->geometry.circle;
}

shape_user_data_t ShapeHandle::getUserData() const
{
	return internalUserData()->userData;
}

void ShapeHandle::setUserData(shape_user_data_t v)
{
	internalUserData()->userData = v;
}

ShapeHandle::UserData* ShapeHandle::internalUserData() const
{
	return reinterpret_cast<UserData*>(_shape->GetUserData());
}

b2Fixture* ShapeHandle::create(b2Body* body, const ShapeDefinition& def, real_t physicsScale)
{
	b2FixtureDef fixtureDef;
	fixtureDef.density = def.material.density;
	fixtureDef.friction = def.material.friction;
	fixtureDef.restitution = def.material.elasticity;

	fixtureDef.filter.categoryBits = def.filter.categories.to_ulong();
	fixtureDef.filter.maskBits = def.filter.mask.to_ulong();
	fixtureDef.filter.groupIndex = def.filter.group;

	fixtureDef.isSensor = false;
	
	b2CircleShape circle;
	b2PolygonShape polygon;

	real_t x = def.localPos[0] * physicsScale;
	real_t y = def.localPos[1] * physicsScale;

	real_t boundingRadius;

	switch (def.shape)
	{
	case Shape::CIRCLE:
		circle.m_p = { x, y };
		circle.m_radius = def.geometry.circle.radius * physicsScale;
		boundingRadius = def.geometry.circle.radius;

		fixtureDef.shape = &circle;
		break;

	case Shape::BOX:
		{
			b2Vec2 v[4] = {
				{ (x - 0.5f * def.geometry.box.width) * physicsScale, (y - 0.5f * def.geometry.box.height) * physicsScale },
				{ (x + 0.5f * def.geometry.box.width) * physicsScale, (y - 0.5f * def.geometry.box.height) * physicsScale },
				{ (x + 0.5f * def.geometry.box.width) * physicsScale, (y + 0.5f * def.geometry.box.height) * physicsScale },
				{ (x - 0.5f * def.geometry.box.width) * physicsScale, (y + 0.5f * def.geometry.box.height) * physicsScale }
			};

			polygon.Set(v, 4);
		}

		boundingRadius = std::sqrt(
			math::square(0.5f * def.geometry.box.width / physicsScale)
			+ math::square(0.5f * def.geometry.box.height / physicsScale)
		);

		fixtureDef.shape = &polygon;
		break;

	case Shape::POLYGON:
		{
			const auto& verts = def.geometry.polygon.vertices;

			std::array<b2Vec2, MAX_POLYGON_VERTICES> b2Vectors;

			real_t maxDistanceSq = 0.0f;

			for (unsigned int i = 0; i < def.geometry.polygon.size; ++i)
			{
				b2Vectors[i] = { verts[i][0] * physicsScale, verts[i][1] * physicsScale };

				real_t distanceSq = math::square(b2Vectors[i].x / physicsScale)
									+ math::square(b2Vectors[i].y / physicsScale);

				if (distanceSq > maxDistanceSq)
					maxDistanceSq = distanceSq;
			}

			polygon.Set(b2Vectors.data(), def.geometry.polygon.size);

			boundingRadius = std::sqrt(maxDistanceSq);
		}

		fixtureDef.shape = &polygon;
		break;
	}

	fixtureDef.userData = new UserData{
		def.shape,
		def.geometry,
		def.localPos,
		boundingRadius,
		def.userData
	};

	return body->CreateFixture(&fixtureDef);
}

void ShapeHandle::destroy(b2Fixture* fixture)
{
	auto* body = fixture->GetBody();
	
	delete reinterpret_cast<UserData*>(fixture->GetUserData());

	body->DestroyFixture(fixture);
}

} }
