#include "physics2d/Body.hpp"

#include <Box2D/Box2D.h>

namespace pm { namespace physics2d {


BodyHandle::BodyHandle(b2Body* body)
: _body(body)
{
}

real_t BodyHandle::getMass() const
{
	return _body->GetMass() / math::pow(internalUserData()->physicsScale, 2);
}

Vector2_r BodyHandle::getPosition() const
{
	auto vec = _body->GetPosition();

	return Vector2_r{ vec.x, vec.y } / internalUserData()->physicsScale;
}

void BodyHandle::setPosition(const Vector2_r& v)
{
	_body->SetTransform(
		{ 
			v[0] * internalUserData()->physicsScale, 
			v[1] * internalUserData()->physicsScale 
		}, 
		getAngle()
	);
}

rad_t BodyHandle::getAngle() const
{
	return _body->GetAngle();
}

void BodyHandle::setAngle(rad_t v)
{
	_body->SetTransform(_body->GetPosition(), v);
}

Vector2_r BodyHandle::getVelocity() const
{
	auto v = _body->GetLinearVelocity();

	return Vector2_r{ v.x, v.y } / internalUserData()->physicsScale;
}

void BodyHandle::setVelocity(const Vector2_r& v)
{
	_body->SetLinearVelocity(
		{ 
			v[0] * internalUserData()->physicsScale, 
			v[1] * internalUserData()->physicsScale
		}
	);
}

real_t BodyHandle::getAngularVelocity() const
{
	return _body->GetAngularVelocity();
}

void BodyHandle::setAngularVelocity(real_t v)
{
	_body->SetAngularVelocity(v);
}

void BodyHandle::applyImpulse(const Vector2_r& impulse, const Vector2_r& localPos)
{
	_body->ApplyLinearImpulse(
		_body->GetWorldVector(
			{ 
				impulse[0] * math::pow(internalUserData()->physicsScale, 3),
				impulse[1] * math::pow(internalUserData()->physicsScale, 3)
			}
		),
		_body->GetWorldPoint(
			{ 
				localPos[0] * internalUserData()->physicsScale, 
				localPos[1] * internalUserData()->physicsScale
			}
		),
		true
	);
}

void BodyHandle::addForce(const Vector2_r& force, const Vector2_r& localPos)
{
	internalUserData()->forces.push_back({ force, localPos });
}

void BodyHandle::resetForces()
{
	internalUserData()->forces.clear();
}

void BodyHandle::applyForces()
{
	for (const auto& f : internalUserData()->forces)
	{
		_body->ApplyForce(
			_body->GetWorldVector(
				{
					f.force[0] * math::pow(internalUserData()->physicsScale, 3),
					f.force[1] * math::pow(internalUserData()->physicsScale, 3)
				}
			),
			_body->GetWorldPoint(
				{ 
					f.localPos[0] * internalUserData()->physicsScale, 
					f.localPos[1] * internalUserData()->physicsScale
				}
			),
			true
		);
	}
}

Transform2_r BodyHandle::getTransform() const
{
	return Transform2_r{ getPosition(), static_cast<real_t>(1), getAngle() };
}

std::vector<ShapeHandle> BodyHandle::getShapes() const
{
	std::vector<ShapeHandle> shapes;

	for (auto* shape = _body->GetFixtureList(); shape != nullptr; shape = shape->GetNext())
	{
		shapes.push_back(ShapeHandle(shape));
	}

	return shapes;
}

void BodyHandle::removeShape(unsigned int index)
{
	auto shape = getShapes()[index];

	ShapeHandle::destroy(shape._shape);
}

void BodyHandle::createShape(const ShapeDefinition& def)
{
	ShapeHandle::create(_body, def, internalUserData()->physicsScale);
}

body_user_data_t BodyHandle::getUserData() const
{
	return reinterpret_cast<UserData*>(_body->GetUserData())->userData;
}

void BodyHandle::setUserData(body_user_data_t v)
{
	reinterpret_cast<UserData*>(_body->GetUserData())->userData = v;
}

body_id_t BodyHandle::id() const
{
	return internalUserData()->id;
}

BodyHandle BodyHandle::enclosing(ShapeHandle shape)
{
	return BodyHandle(shape._shape->GetBody());
}

b2Body* BodyHandle::create(
	b2World& world, 
	const BodyDefinition& def, 
	real_t damping, 
	real_t physicsScale,
	body_id_t id
)
{
	b2BodyDef bodyDef;

	switch (def.type)
	{
	case BodyType::DYNAMIC:
		bodyDef.type = b2_dynamicBody;
		break;
	case BodyType::KINEMATIC:
		bodyDef.type = b2_kinematicBody;
		break;
	case BodyType::STATIC:
		bodyDef.type = b2_staticBody;
		break;
	}

	bodyDef.linearDamping = damping;
	bodyDef.angularDamping = damping;
	bodyDef.bullet = def.bullet;
	bodyDef.userData = new UserData{ id, {}, physicsScale, def.userData, def.preSolveFunc, def.postSolveFunc };

	auto* body = world.CreateBody(&bodyDef);

	for (const auto& shapeDef : def.shapes)
	{
		ShapeHandle::create(body, shapeDef, physicsScale);
	}

	b2MassData massData;
	body->GetMassData(&massData);
	
	massData.I = massData.I + massData.mass * b2Dot(massData.center, massData.center);
	
	body->SetMassData(&massData);

	return body;
}

void BodyHandle::destroy(b2World* world, b2Body* body)
{
	while (auto* fixture = body->GetFixtureList())
	{
		ShapeHandle::destroy(fixture);
	}

	delete reinterpret_cast<UserData*>(body->GetUserData());

	world->DestroyBody(body);
}

BodyHandle::UserData* BodyHandle::internalUserData() const
{
	return reinterpret_cast<UserData*>(_body->GetUserData());
}

} }
