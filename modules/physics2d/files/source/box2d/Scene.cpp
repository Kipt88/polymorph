#include "physics2d/Scene.hpp"

#include <Debug.hpp>
#include <algorithm>

namespace pm { namespace physics2d {


Scene::Scene(real_t physicsScale)
: _damping(0.0), 
  _physicsScale(physicsScale),
  _world(b2Vec2_zero),
  _bodies(),
  _collisions()
{
	_world.SetContactListener(this);
}

Scene::~Scene()
{
	_world.SetContactListener(nullptr);
}

BodyHandle Scene::create(const BodyDefinition& def, body_id_t id)
{
	ASSERT(_bodies.count(id) == 0, "Trying to overwrite body.");

	return _bodies.insert(
		std::make_pair(id, BodyHandle(BodyHandle::create(_world, def, _damping, _physicsScale, id)))
	).first->second;
}

void Scene::remove(BodyHandle body)
{
	auto id = body.id();

	ASSERT(_bodies.count(id), "Can't find body.");

	for (const auto& shape : body.getShapes())
	{
		for (auto it = _collisions.begin(); it != _collisions.end();)
		{
			if (it->second.shapeA._shape == shape._shape || it->second.shapeB._shape == shape._shape)
				it = _collisions.erase(it);
			else
				++it;
		}
	}

	BodyHandle::destroy(&_world, body._body);

	_bodies.erase(id);
}

BodyHandle Scene::get(body_id_t id) const
{
	auto it = _bodies.find(id);
	ASSERT(it != _bodies.end(), "Can't find specified body.");

	return it->second;
}

auto Scene::begin() -> iterator
{
	return _bodies.begin();
}

auto Scene::begin() const -> const_iterator
{
	return _bodies.begin();
}

auto Scene::cbegin() const -> const_iterator
{
	return _bodies.cbegin();
}

auto Scene::end() -> iterator
{
	return _bodies.end();
}

auto Scene::end() const -> const_iterator
{
	return _bodies.end();
}

auto Scene::cend() const -> const_iterator
{
	return _bodies.cend();
}

void Scene::setGravity(const Vector2_r& gravity)
{
	_world.SetGravity(
		{ 
			gravity[0] * _physicsScale, 
			gravity[1] * _physicsScale 
		}
	);
}

void Scene::setDamping(real_t d)
{
	_damping = d;

	// TODO Iterate all bodies and set damping.
}

void Scene::resetAllForces()
{
	for (auto& e : *this)
		e.second.resetForces();
}

Scene::collision_events_t Scene::update(const Time::duration& dt)
{
	for (auto it = _collisions.begin(); it != _collisions.end();)
	{
		if (it->second.hasEnded)
			it = _collisions.erase(it);
		else
			++it;
	}

	collision_events_t collisions;
	collisions.reserve(_collisions.size());

	for (auto& c : _collisions)
	{
		c.second.firstContact = false;
	}

	for (auto& e : *this)
	{
		e.second.applyForces();

#ifdef _DEBUG
		auto movementSize = 
			b2Sqrt(b2Dot(e.second._body->GetLinearVelocity(), e.second._body->GetLinearVelocity())) * toSeconds<real_t>(dt);

		if (std::abs(movementSize - b2_maxTranslation) < 0.0001f)
			ERROR_OUT("Physics", "At max translation %i", static_cast<unsigned int>(e.first));

		auto rotationSize = e.second.getAngularVelocity() * toSeconds<real_t>(dt);
		if (std::abs(rotationSize - b2_maxRotation) < 0.0001f)
			ERROR_OUT("Physics", "At max angular velocity %i", static_cast<unsigned int>(e.first));
#endif
	}

	_world.Step(toSeconds<real_t>(dt), 1, 1);

	for (const auto& c : _collisions)
		collisions.push_back(c.second);

	return collisions;
}

void Scene::BeginContact(b2Contact* contact)
{
	_collisions.insert_or_assign(
		contact,
		CollisionData{
			ShapeHandle(contact->GetFixtureA()),
			ShapeHandle(contact->GetFixtureB()),
			Vector2_r::ZERO,
			Vector2_r::ZERO,
			true,
			false
		}
	);
}

void Scene::EndContact(b2Contact* contact)
{
	auto it = _collisions.find(contact);

	if (it != _collisions.end())
		it->second.hasEnded = true;
}

void Scene::PreSolve(b2Contact* contact, const b2Manifold* manifold)
{
	auto it = _collisions.find(contact);

	ASSERT(it != _collisions.end(), "Can't find contact.");

	// TODO Pre-solve hook..
}

void Scene::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{
	auto it = _collisions.find(contact);

	ASSERT(it != _collisions.end(), "Can't find contact.");

	b2WorldManifold worldManifold;
	contact->GetWorldManifold(&worldManifold);
	
	it->second.impulse =
		(-Vector2_r{ worldManifold.normal.x, worldManifold.normal.y } *impulse->normalImpulses[0]
		+ Vector2_r{ -worldManifold.normal.y, worldManifold.normal.x } *impulse->tangentImpulses[0]) 
		/ math::pow(_physicsScale, 3);

	it->second.position = Vector2_r{ worldManifold.points[0].x, worldManifold.points[0].y } 
	                      / math::pow(_physicsScale, 3);

}

} }
