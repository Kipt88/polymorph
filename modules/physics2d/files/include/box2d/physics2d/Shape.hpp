#pragma once

#include "physics2d/BodyDefinition.hpp"
#include "physics2d/types.hpp"
#include <vector>

class b2Fixture;
class b2Body;
class b2World;

namespace pm { namespace physics2d {

class BodyHandle;
class Scene;

class ShapeHandle
{
public:
	friend class BodyHandle;
	friend class Scene;

	explicit ShapeHandle(b2Fixture* shape);

	bool isSensor() const;
	void setSensor(bool v);

	real_t getElasticity() const;
	void setElasticity(real_t v);

	real_t getFriction() const;
	void setFriction(real_t v);

	collision_type_t getCollisionType() const;
	void setCollisionType(collision_type_t v);

	CollisionFilter getCollisionFilter() const;
	void setCollisionFilter(const CollisionFilter& filter);

	real_t getBoundingRadius() const;

	const Vector2_r& getLocalPosition() const;

	Shape getShape() const;
	const ShapeDefinition::Geometry& getGeometry() const;
	const ShapeDefinition::BoxDef& asBox() const;
	const ShapeDefinition::CircleDef& asCircle() const;

	shape_user_data_t getUserData() const;
	void setUserData(shape_user_data_t v);

private:
	static b2Fixture* create(b2Body* body, const ShapeDefinition& def, real_t physicsScale);
	static void destroy(b2Fixture* fixture);

	struct UserData
	{
		Shape shape;
		ShapeDefinition::Geometry geometry;

		Vector2_r localPos;
		real_t boundingRadius;
		shape_user_data_t userData;
	};

	UserData* internalUserData() const;

	b2Fixture* _shape;
};

} }
