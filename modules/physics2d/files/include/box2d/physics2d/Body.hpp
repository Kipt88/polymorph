#pragma once

#include "physics2d/BodyDefinition.hpp"
#include "physics2d/Shape.hpp"
#include "physics2d/types.hpp"
#include <vector>

class b2Body;
class b2World;
class b2Fixture;

namespace pm { namespace physics2d {

class Scene;

typedef void* body_user_data_t;
typedef std::uint32_t body_id_t;

class BodyHandle
{
public:
	friend class Scene;

	explicit BodyHandle(b2Body* body);

	real_t getMass() const;
	Vector2_r getPosition() const;
	void setPosition(const Vector2_r& v);

	rad_t getAngle() const;
	void setAngle(rad_t v);

	Vector2_r getVelocity() const;
	void setVelocity(const Vector2_r& v);

	real_t getAngularVelocity() const;
	void setAngularVelocity(real_t v);

	void applyImpulse(const Vector2_r& impulse, const Vector2_r& localPos);

	void addForce(const Vector2_r& force, const Vector2_r& localPos);
	void resetForces();

	void applyForces();

	Transform2_r getTransform() const;
	
	std::vector<ShapeHandle> getShapes() const;
	void removeShape(unsigned int index);
	void createShape(const ShapeDefinition& def);

	body_user_data_t getUserData() const;
	void setUserData(body_user_data_t v);

	body_id_t id() const;

	static BodyHandle enclosing(ShapeHandle shape);

private:
	static b2Body* create(
		b2World& world, 
		const BodyDefinition& def, 
		real_t damping, 
		real_t physicsScale,
		body_id_t id
	);

	static void destroy(b2World* world, b2Body* body);

	struct Force
	{
		Vector2_r force;
		Vector2_r localPos;
	};

	struct UserData
	{
		body_id_t id;
		std::vector<Force> forces;
		real_t physicsScale;
		// TODO Add constraints.
		body_user_data_t userData;
		pre_solve_func_t preSolve;
		post_solve_func_t postSolve;
	};

	UserData* internalUserData() const;

	b2Body* _body;
};

} }
