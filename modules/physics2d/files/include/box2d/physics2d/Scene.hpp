#pragma once

#include "physics2d/Body.hpp"
#include "physics2d/ContactData.hpp"
#include <Time.hpp>
#include <UniqueFunction.hpp>
#include <optional.hpp>
#include <Box2D/Box2D.h>
#include <memory>
#include <map>

namespace pm { namespace physics2d {

class Scene : b2ContactListener, Unique
{
public:
	struct CollisionData
	{
		ShapeHandle shapeA;
		ShapeHandle shapeB;

		Vector2_r position;
		Vector2_r impulse;

		bool firstContact;

		bool hasEnded; // Use dirty flag in case collision ends same step it's begun.
	};

	struct CastHit
	{
		ShapeHandle shape;
		Vector2_r point;
		Vector2_r normal;
		real_t normalizedDistance;
	};

	struct LineCast
	{
		CollisionFilter filter;

		Vector2_r start; 
		Vector2_r end;
		real_t radius;
	};

	typedef std::vector<CollisionData> collision_events_t;

	typedef std::map<body_id_t, BodyHandle>::iterator iterator;
	typedef std::map<body_id_t, BodyHandle>::const_iterator const_iterator;

	explicit Scene(real_t physicsScale = 1.0f);
	~Scene();

	BodyHandle create(const BodyDefinition& def, body_id_t id = 0);

	void remove(BodyHandle body);

	BodyHandle get(body_id_t id) const;

	iterator begin();
	const_iterator begin() const;
	const_iterator cbegin() const;

	iterator end();
	const_iterator end() const;
	const_iterator cend() const;

	void setGravity(const Vector2_r& gravity);
	void setDamping(real_t d);
	void resetAllForces();

	collision_events_t update(const Time::duration& dt);

private:	
	void BeginContact(b2Contact* contact) override;
	void EndContact(b2Contact* contact) override;
	void PreSolve(b2Contact* contact, const b2Manifold* oldManifold) override;
	void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) override;

	real_t _damping;
	real_t _physicsScale;

	b2World _world;
	std::map<body_id_t, BodyHandle> _bodies;
	std::map<b2Contact*, CollisionData> _collisions;
};

} }
