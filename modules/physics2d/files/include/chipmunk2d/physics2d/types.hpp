#pragma once

#include <math/Transform2.hpp>
#include <math/Matrix.hpp>
#include <cinttypes>
#include <bitset>

namespace pm { namespace physics2d {

typedef double real_t;
typedef math::Vector<real_t, 2> Vector2_r;
typedef math::Transform2<real_t> Transform2_r;
typedef real_t rad_t;

typedef std::uintptr_t collision_type_t;
typedef std::uintptr_t collision_group_t;
typedef std::bitset<32> bitmask_t;

struct CollisionFilter
{
	/** Body shapes of same group don't collisde. 0 is "no group". */
	collision_group_t group;

	/** Categories this object belongs to. Default is "all categories". */
	bitmask_t categories;

	/** Categories this object collides with. Default is "all categories". */
	bitmask_t mask;
};

} }
