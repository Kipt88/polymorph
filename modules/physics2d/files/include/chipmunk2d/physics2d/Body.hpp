#pragma once

#include "physics2d/Shape.hpp"
#include "physics2d/types.hpp"
#include <vector>

struct cpBody;

namespace pm { namespace physics2d {

class Scene;

typedef void* body_user_data_t;
typedef std::uint32_t body_id_t;

enum class BodyType { DYNAMIC, KINETIC, STATIC };

struct BodyDefinition
{
	std::vector<ShapeDefinition> shapes;
	BodyType type;
};

typedef std::shared_ptr<const BodyDefinition> BodyDefinitionHandle;

class BodyHandle
{
public:
	friend class Scene;

	explicit BodyHandle(cpBody* body);

	real_t getMass() const;
	void setMass(real_t v);

	real_t getInertia() const;
	void setInertia(real_t v);

	Vector2_r getPosition() const;
	void setPosition(const Vector2_r& v);

	rad_t getAngle() const;
	void setAngle(rad_t v);

	Vector2_r getVelocity() const;
	void setVelocity(const Vector2_r& v);

	void applyImpulse(const Vector2_r& impulse, const Vector2_r& localPos);

	void addForce(const Vector2_r& force, const Vector2_r& localPos);
	void resetForces();

	void applyForces();

	Transform2_r getTransform() const;
	
	std::vector<ShapeHandle> getShapes() const;
	void removeShape(unsigned int index);

	body_user_data_t getUserData() const;
	void setUserData(body_user_data_t v);

	body_id_t id() const;

	static BodyHandle enclosing(ShapeHandle shape);

private:
	static cpBody* create(cpSpace* space, const BodyDefinition& def, body_id_t id);

	struct Force
	{
		Vector2_r force;
		Vector2_r localPos;
	};

	struct UserData
	{
		body_id_t id;
		std::vector<Force> forces;
		std::vector<cpShape*> shapes;
		// TODO Add constraints.
		body_user_data_t userData;
	};

	UserData* internalUserData() const;

	cpBody* _body;
};

} }
