#pragma once

#include "physics2d/Material.hpp"
#include "physics2d/types.hpp"
#include <vector>

struct cpShape;
struct cpBody;
struct cpSpace;

namespace pm { namespace physics2d {

class BodyHandle;
class Scene;

constexpr unsigned int MAX_POLYGON_VERTICES = 20;

typedef void* shape_user_data_t;

enum class Shape { CIRCLE, BOX, POLYGON };

typedef void* shape_user_data_t;

struct ShapeDefinition
{
	struct CircleDef
	{
		real_t radius;
	};

	struct BoxDef
	{
		real_t width;
		real_t height;
	};

	struct PolygonDef
	{
		PolygonDef() = default;
		PolygonDef(const std::initializer_list<Vector2_r>& vertices);
		PolygonDef(const std::vector<Vector2_r>& vertices);

		// Vertices must be CCW winding order and represent a convex polygon.
		std::array<Vector2_r, MAX_POLYGON_VERTICES> vertices;
		unsigned int size;
	};

	ShapeDefinition();
	explicit ShapeDefinition(const CircleDef& circle);
	explicit ShapeDefinition(const BoxDef& box);
	explicit ShapeDefinition(const PolygonDef& polygon);

	Shape shape;

	union Geometry
	{
		CircleDef circle;
		BoxDef box;
		PolygonDef polygon;
	} geometry;

	Vector2_r localPos;
	Material material;
	collision_type_t type;

	CollisionFilter filter;

	shape_user_data_t userData;
};

class ShapeHandle
{
public:
	friend class BodyHandle;
	friend class Scene;

	explicit ShapeHandle(cpShape* shape);

	bool isSensor() const;
	void setSensor(bool v);

	real_t getElasticity() const;
	void setElasticity(real_t v);

	real_t getFriction() const;
	void setFriction(real_t v);

	collision_type_t getCollisionType() const;
	void setCollisionType(collision_type_t v);

	CollisionFilter getCollisionFilter() const;

	const Vector2_r& getLocalPosition() const;

	Shape getShape() const;
	const ShapeDefinition::Geometry& getGeometry() const;
	const ShapeDefinition::BoxDef& asBox() const;
	const ShapeDefinition::CircleDef& asCircle() const;

	shape_user_data_t getUserData() const;
	void setUserData(shape_user_data_t v);

private:
	static cpShape* create(cpSpace* space, cpBody* body, const ShapeDefinition& def);

	struct UserData
	{
		Shape shape;
		ShapeDefinition::Geometry geometry;

		Vector2_r localPos;
		shape_user_data_t userData;
	};

	UserData* internalUserData() const;

	cpShape* _shape;
};

} }
