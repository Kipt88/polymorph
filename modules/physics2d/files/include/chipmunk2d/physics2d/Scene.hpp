#pragma once

#include "physics2d/Body.hpp"
#include <Time.hpp>
#include <UniqueFunction.hpp>
#include <memory>
#include <map>
#include <optional.hpp>

struct cpSpace;

namespace pm { namespace physics2d {

class Scene
{
public:
	struct CollisionData
	{
		ShapeHandle shapeA;
		ShapeHandle shapeB;

		real_t energyLost;
		bool firstContact;
	};

	struct CastHit
	{
		ShapeHandle shape;
		Vector2_r point;
		Vector2_r normal;
		real_t normalizedDistance;
	};

	struct LineCast
	{
		CollisionFilter filter;

		Vector2_r start; 
		Vector2_r end;
		real_t radius;
	};

	typedef std::vector<CollisionData> collision_events_t;

	typedef std::map<body_id_t, BodyHandle>::iterator iterator;
	typedef std::map<body_id_t, BodyHandle>::const_iterator const_iterator;

	Scene();
	~Scene();

	BodyHandle create(const BodyDefinition& def, body_id_t id = 0);

	void remove(BodyHandle body);

	BodyHandle get(body_id_t id) const;

	iterator begin();
	const_iterator begin() const;
	const_iterator cbegin() const;

	iterator end();
	const_iterator end() const;
	const_iterator cend() const;

	void setGravity(const Vector2_r& gravity);
	void setDamping(real_t d);
	void resetAllForces();

	std::optional<CastHit> castFirst(const LineCast& line);

	collision_events_t update(const Time::duration& dt);

private:
	struct space_deleter { void operator()(cpSpace* space); };

	void deleteBody(BodyHandle body);

	std::unique_ptr<cpSpace, space_deleter> _space;
	std::map<body_id_t, BodyHandle> _bodies;
	
	collision_events_t _collisions;
};

} }
