#pragma once

#include "physics2d/Snapshot.hpp"
#include "physics2d/types.hpp"

namespace pm { namespace physics2d {

struct CollisionPairData
{
	body_id_t bodyA;
	body_id_t bodyB;

	Vector2_r worldPoint;
	Vector2_r worldImpulseA;
	bool firstContact;
};

struct State
{
	std::vector<CollisionPairData> collisions;
	Snapshot snapshot;
};

} }
