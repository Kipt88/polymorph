#pragma once

#include "physics2d/Scene.hpp"
#include "physics2d/SystemRunner.hpp"
#include <UniqueFunction.hpp>
#include <NonCopyable.hpp>

namespace pm { namespace physics2d {

class Physics;

class ClientFrame : NonCopyable
{
public:
	explicit ClientFrame(Physics& system);
	~ClientFrame();

	ClientFrame(ClientFrame&&) = default;
	ClientFrame& operator=(ClientFrame&&) = default;

	void createBody(body_id_t id, const BodyDefinition& def, body_user_data_t data = nullptr);
	void removeBody(body_id_t id);

	void setPosition(body_id_t id, const Vector2_r& pos);
	void setRotation(body_id_t id, rad_t angle);
	void setVelocity(body_id_t id, const Vector2_r& v);

	void applyForce(body_id_t id, const Vector2_r& force, const Vector2_r& localPos = Vector2_r::ZERO);
	void applyImpulse(body_id_t id, const Vector2_r& impulse, const Vector2_r& localPos = Vector2_r::ZERO);

	void setGravity(const Vector2_r& gravity);
	void setDamping(real_t d);

	template <typename Action>
	void withScene(Action&& action);

	template <typename Action>
	void withBody(body_id_t id, Action&& action);

	void flush();

private:
	std::vector<UniqueFunction<void(SystemRunner&)>> _queue;

	std::reference_wrapper<Physics> _system;
};

} }

// TODO Move to .inl-file

namespace pm { namespace physics2d {

template <typename Action>
void ClientFrame::withScene(Action&& action)
{
	_queue.push_back(
		[action = std::forward<Action>(action)](SystemRunner& runner) mutable
		{
			action(runner.scene());
		}
	);
}

template <typename Action>
void ClientFrame::withBody(body_id_t id, Action&& action)
{
	_queue.push_back(
		[action = std::forward<Action>(action), id](SystemRunner& runner) mutable
		{
			action(runner.scene().get(id));
		}
	);
}

} }

//
