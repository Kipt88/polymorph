#pragma once

#include "physics2d/types.hpp"
#include <vector>

namespace pm { namespace physics2d {

namespace PolygonDecomposer {

// See https://mpen.ca/406/keil
std::vector<std::vector<Vector2_r>> decomposeToConvex(const std::vector<Vector2_r>& points, unsigned int maxVertices = 0);

}

} }
