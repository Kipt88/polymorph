#pragma once

#include "physics2d/types.hpp"
#include <type_traits>

namespace pm {

// Length units:
constexpr physics2d::real_t operator "" _km(long double d);
constexpr physics2d::real_t operator "" _m(long double d);
constexpr physics2d::real_t operator "" _mm(long double d);

// Speed units:
constexpr physics2d::real_t operator "" _mps(long double d);
constexpr physics2d::real_t operator "" _kph(long double d);

// Acceleration units:
constexpr physics2d::real_t operator "" _mps2(long double d);

// Rotation units:
constexpr physics2d::real_t operator "" _rad(long double d);
constexpr physics2d::real_t operator "" _deg(long double d);

// Rotation acceleration:
constexpr physics2d::real_t operator "" _radps(long double d);
constexpr physics2d::real_t operator "" _degps(long double d);

// Mass units:
constexpr physics2d::real_t operator "" _kg(long double d);
constexpr physics2d::real_t operator "" _g(long double d);

// Density units:
constexpr physics2d::real_t operator "" _kgpm2(long double d);

// Force units:
constexpr physics2d::real_t operator "" _N(long double d);

// Energy units:
constexpr physics2d::real_t operator "" _J(long double d);
constexpr physics2d::real_t operator "" _kJ(long double d);
constexpr physics2d::real_t operator "" _MJ(long double d);

// Energy regeneration units:
constexpr physics2d::real_t operator "" _Jps(long double d);
constexpr physics2d::real_t operator "" _kJps(long double d);
constexpr physics2d::real_t operator "" _MJps(long double d);

}

#include "physics2d/Literals.inl"