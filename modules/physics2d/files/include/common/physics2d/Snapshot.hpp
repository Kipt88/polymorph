#pragma once

#include "physics2d/Scene.hpp"
#include "physics2d/types.hpp"
#include <Time.hpp>
#include <vector>

namespace pm { namespace physics2d {

struct ShapeState
{
	Shape shape;
	ShapeDefinition::Geometry geometry;
	CollisionFilter filter;
	Vector2_r localPos;
	real_t boundingRadius;
	shape_user_data_t userData;
};

struct BodyState
{
	std::vector<ShapeState> shapes;
	Transform2_r transform;
	Vector2_r velocity;
	real_t angularVelocity;
	real_t mass;
	body_user_data_t userData;
	unsigned int id;
};

struct CastHit
{
	Vector2_r pos;
	Vector2_r normal;
	ShapeState shape;
};

class Snapshot
{
public:
	/**
	 * Creates empty snapshot at target time.
	 */
	explicit Snapshot(const Time::time_point& time = Time::time_point{});

	/**
	 * Creates a snapshot of the scene at target time.
	 */
	explicit Snapshot(const Time::time_point& time, const Scene& scene);

	const std::vector<BodyState>& bodies() const;
	const Time::time_point& timestamp() const;

	std::optional<CastHit> castLine(
		const Vector2_r& p0, 
		const Vector2_r& p1, 
		const CollisionFilter& filter
	) const;

	static Snapshot interpolate(const Snapshot& t0, const Snapshot& t1, const Time::time_point& time);

private:
	// Could add partitioning to speed up casting.

	std::vector<BodyState> _bodies;

	Time::time_point _time;
};

} }
