#pragma once

#include "physics2d/types.hpp"

namespace pm { namespace physics2d {

struct ContactData
{
	Vector2_r impulse;
	Vector2_r localPoint;
};

} }
