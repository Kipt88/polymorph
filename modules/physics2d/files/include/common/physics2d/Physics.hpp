#pragma once

#include "physics2d/SystemRunner.hpp"
#include "physics2d/Scene.hpp"
#include "physics2d/Snapshot.hpp"
#include "physics2d/BodyProvider.hpp"
#include "physics2d/State.hpp"
#include "physics2d/Frame.hpp"
#include <concurrency/FrameTime.hpp>
#include <concurrency/System.hpp>
#include <UniqueFunction.hpp>
#include <tuple>
#include <optional.hpp>

namespace pm { namespace physics2d {

struct CollisionData
{
	body_id_t otherBody;
	void* otherHandler;

	physics2d::Vector2_r worldPoint;
	physics2d::Vector2_r worldImpulse;
	bool firstContact;
};

class Physics
{
public:
	friend class ClientFrame;

	explicit Physics(const Time::duration& sync, real_t physicsScale = 1.0);

	State waitForState(const Time::time_point& from, const Time::time_point& to) const;
	ClientFrame createFrame();

	BodyProvider& bodyProvider();

private:
	concurrency::ConcurrentSystem<SystemRunner> _system;

	BodyProvider _bodyProvider;

	const SystemRunner::SnapshotBuffer& _buffers;
};

} }
