#pragma once

#include "physics2d/Scene.hpp"
#include "physics2d/Snapshot.hpp"
#include "physics2d/State.hpp"
#include <concurrency/System.hpp>
#include <concurrency/Condition.hpp>
#include <concurrency/Mutex.hpp>
#include <concurrency/FrameTime.hpp>
#include <Time.hpp>
#include <array>

namespace pm { namespace physics2d {

class SystemRunner
{
public:
	static constexpr const char* name = "Physics";

	struct SnapshotBuffer
	{
		explicit SnapshotBuffer(const Time::time_point& t);

		mutable concurrency::Mutex mutex;
		mutable concurrency::Condition updateCondition;

		std::vector<CollisionPairData> collisions;

		Snapshot old;

		Snapshot first;
		Snapshot second;
	};

	explicit SystemRunner(concurrency::RunnerTasks<SystemRunner>&, real_t physicsScale);

	bool update(const concurrency::FrameTime& time);

	Scene& scene();
	const SnapshotBuffer& snapshots() const;

private:
	Scene _scene;

	SnapshotBuffer _snapshots;
};

} }
