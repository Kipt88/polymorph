#pragma once

#include "physics2d/Body.hpp"
#include <io/IOStream.hpp>
#include <Exception.hpp>

namespace pm { namespace physics2d {

PM_MAKE_EXCEPTION_CLASS(PhysicsResourceException, Exception);

namespace BodyDefReader
{
	BodyDefinition read(io::InputStream& input);
}

} }
