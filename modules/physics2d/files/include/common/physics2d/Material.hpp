#pragma once

#include "physics2d/types.hpp"

namespace pm { namespace physics2d {

struct Material
{
	real_t density;
	real_t friction;
	real_t elasticity;
};

} }
