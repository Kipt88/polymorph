#pragma once

#include "physics2d/Material.hpp"
#include "physics2d/types.hpp"
#include <vector>

namespace pm { namespace physics2d {

class Scene;
class ShapeHandle;
struct ContactData;

constexpr unsigned int MAX_POLYGON_VERTICES = 8;

enum class Shape { CIRCLE, BOX, POLYGON };

using shape_user_data_t = void*;
using body_user_data_t = void*;

using pre_solve_func_t = bool(*)(Scene&, ShapeHandle&, ShapeHandle&);
using post_solve_func_t = void(*)(Scene&, ShapeHandle&, ShapeHandle&, const ContactData&);

struct ShapeDefinition
{
	struct CircleDef
	{
		real_t radius;
	};

	struct BoxDef
	{
		real_t width;
		real_t height;
	};

	struct PolygonDef
	{
		PolygonDef() = default;
		PolygonDef(const std::initializer_list<Vector2_r>& vertices);
		PolygonDef(const std::vector<Vector2_r>& vertices);

		// Vertices must be CCW winding order and represent a convex polygon.
		std::array<Vector2_r, MAX_POLYGON_VERTICES> vertices;
		unsigned int size;
	};

	ShapeDefinition();
	explicit ShapeDefinition(const CircleDef& circle);
	explicit ShapeDefinition(const BoxDef& box);
	explicit ShapeDefinition(const PolygonDef& polygon);

	Shape shape;

	union Geometry
	{
		CircleDef circle;
		BoxDef box;
		PolygonDef polygon;
	} geometry;

	Vector2_r localPos;
	Material material;
	collision_type_t type;

	CollisionFilter filter;

	shape_user_data_t userData;
};


enum class BodyType { DYNAMIC, KINEMATIC, STATIC };

struct BodyDefinition
{
	BodyDefinition();

	std::vector<ShapeDefinition> shapes;
	bool bullet;
	BodyType type;

	body_user_data_t userData;
	pre_solve_func_t preSolveFunc;
	post_solve_func_t postSolveFunc;
};

typedef std::shared_ptr<const BodyDefinition> BodyDefinitionHandle;

} }
