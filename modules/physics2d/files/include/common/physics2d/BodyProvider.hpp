#pragma once

#include "physics2d/Body.hpp"

#include <concurrency/TaskRunner.hpp>
#include <concurrency/Task.hpp>

#include <Id.hpp>
#include <Cache.hpp>
#include <NonCopyable.hpp>

#include <filesystem.hpp>

namespace pm { namespace physics2d {

class BodyProvider : Unique
{
public:
	typedef std::filesystem::path path_t;
	typedef id_t body_def_id_t;
	typedef CacheMap<body_def_id_t, BodyDefinitionHandle> BodyDefinitionCache;
	typedef concurrency::Future<BodyDefinitionHandle> LoadResult;

	explicit BodyProvider(const path_t& root = std::filesystem::current_path());

	LoadResult loadBodyDefinition(body_def_id_t id);

	BodyDefinitionHandle get(body_def_id_t id);

	static body_def_id_t registerBodyDefinition(body_def_id_t id, const path_t& path);

private:
	struct BodyDefMetaData
	{
		path_t path;
	};

	concurrency::TaskRunner<BodyDefinitionHandle> _loader;

	mutable concurrency::Mutex _bodiesMutex;
	BodyDefinitionCache _bodies;

	path_t _root;

	static std::map<body_def_id_t, BodyDefMetaData>& getBodyDefMetaData();
};

} }
