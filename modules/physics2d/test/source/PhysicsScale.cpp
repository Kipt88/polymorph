#include <gtest/gtest.h>
#include <physics2d/Scene.hpp>
#include <Debug.hpp>
#include <vector>

using namespace pm::physics2d;

#define ASSERT_SAME_BODY_STATE(bodyA, bodyB) \
	ASSERT_FLOAT_EQ(bodyA.getPosition()[0], bodyB.getPosition()[0]); \
	ASSERT_FLOAT_EQ(bodyA.getPosition()[1], bodyB.getPosition()[1]); \
	ASSERT_FLOAT_EQ(bodyA.getAngle(), bodyB.getAngle()); \
	ASSERT_FLOAT_EQ(bodyA.getVelocity()[0], bodyB.getVelocity()[0]); \
	ASSERT_FLOAT_EQ(bodyA.getVelocity()[0], bodyB.getVelocity()[0])

TEST(PhysicsScale, gravity)
{
	std::vector<std::unique_ptr<Scene>> scenes;

	scenes.push_back(std::make_unique<Scene>(0.5f));
	scenes.push_back(std::make_unique<Scene>(1.0f));
	scenes.push_back(std::make_unique<Scene>(2.0f));

	BodyDefinition def;

	def.type = BodyType::DYNAMIC;
	ShapeDefinition shape(ShapeDefinition::CircleDef{ 1.0f });
	shape.material.density = 1.0f;

	def.shapes.push_back(shape);

	std::vector<BodyHandle> bodies;

	for (auto& scene : scenes)
	{
		bodies.push_back(scene->create(def));

		scene->setGravity({ -10.0f, 0.0f });
		scene->update(10ms);
	}

	for (unsigned int i = 0; i < bodies.size(); ++i)
	{
		unsigned int j = (i == bodies.size() - 1 ? 0 : i + 1);

		ASSERT_SAME_BODY_STATE(bodies[i], bodies[j]);
	}
}

TEST(PhysicsScale, velocity)
{
	std::vector<std::unique_ptr<Scene>> scenes;

	scenes.push_back(std::make_unique<Scene>(0.5f));
	scenes.push_back(std::make_unique<Scene>(1.0f));
	scenes.push_back(std::make_unique<Scene>(2.0f));

	BodyDefinition def;

	def.type = BodyType::DYNAMIC;
	ShapeDefinition shape(ShapeDefinition::CircleDef{ 1.0f });
	shape.material.density = 1.0f;

	def.shapes.push_back(shape);

	std::vector<BodyHandle> bodies;

	for (auto& scene : scenes)
	{
		bodies.push_back(scene->create(def));

		bodies.back().setVelocity({ 0.0f, 10.0f });

		scene->update(10ms);
	}

	for (unsigned int i = 0; i < bodies.size(); ++i)
	{
		unsigned int j = (i == bodies.size() - 1 ? 0 : i + 1);

		ASSERT_SAME_BODY_STATE(bodies[i], bodies[j]);
	}
}

TEST(PhysicsScale, impulse)
{
	std::vector<std::unique_ptr<Scene>> scenes;

	scenes.push_back(std::make_unique<Scene>(0.5f));
	scenes.push_back(std::make_unique<Scene>(1.0f));
	scenes.push_back(std::make_unique<Scene>(2.0f));

	BodyDefinition def;

	def.type = BodyType::DYNAMIC;
	ShapeDefinition shape(ShapeDefinition::CircleDef{ 1.0f });
	shape.material.density = 1.0f;

	def.shapes.push_back(shape);

	std::vector<BodyHandle> bodies;

	for (auto& scene : scenes)
	{
		bodies.push_back(scene->create(def));

		bodies.back().applyImpulse({ 10.0f, 10.0f }, { 0.5f, 0.1f });

		scene->update(10ms);
	}

	for (unsigned int i = 0; i < bodies.size(); ++i)
	{
		unsigned int j = (i == bodies.size() - 1 ? 0 : i + 1);

		ASSERT_SAME_BODY_STATE(bodies[i], bodies[j]);
	}
}

TEST(PhysicsScale, force)
{
	std::vector<std::unique_ptr<Scene>> scenes;

	scenes.push_back(std::make_unique<Scene>(0.5f));
	scenes.push_back(std::make_unique<Scene>(1.0f));
	scenes.push_back(std::make_unique<Scene>(2.0f));

	BodyDefinition def;

	def.type = BodyType::DYNAMIC;
	ShapeDefinition shape(ShapeDefinition::CircleDef{ 1.0f });
	shape.material.density = 1.0f;

	def.shapes.push_back(shape);

	std::vector<BodyHandle> bodies;

	for (auto& scene : scenes)
	{
		bodies.push_back(scene->create(def));

		bodies.back().addForce({ 10.0f, 10.0f }, { 0.5f, 0.2f });

		scene->update(10ms);
	}

	for (unsigned int i = 0; i < bodies.size(); ++i)
	{
		unsigned int j = (i == bodies.size() - 1 ? 0 : i + 1);

		ASSERT_SAME_BODY_STATE(bodies[i], bodies[j]);
	}
}