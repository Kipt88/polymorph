message("Box2D: Running FindBox2D.cmake")

if(NOT BOX2D_FIND_VERSION)
	set(BOX2D_FIND_VERSION "2.3.1")
endif()

message("Box2D: Using version ${BOX2D_FIND_VERSION}")

set(box2d_path "${EXTERNALS_ROOT}/box2d/${BOX2D_FIND_VERSION}")

if(WIN32)
	find_library(
		BOX2D_LIBRARY_RELEASE "Box2D-mt"
		PATHS "${box2d_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
	)

	find_library(
		BOX2D_LIBRARY_DEBUG "Box2D-mtd"
		PATHS "${box2d_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
	)
elseif(APPLE)
	find_library(
		BOX2D_LIBRARY_RELEASE "Box2D"
		PATHS "${box2d_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
	)

	find_library(
		BOX2D_LIBRARY_DEBUG "Box2D-d"
		PATHS "${box2d_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
	)
endif()

if(BOX2D_LIBRARY_DEBUG AND BOX2D_LIBRARY_RELEASE)
	set(BOX2D_FOUND TRUE)
	set(BOX2D_INCLUDE_DIRS "${box2d_path}/include")
	set(BOX2D_LIBRARIES 
		"$<$<CONFIG:Debug>:${BOX2D_LIBRARY_DEBUG}>" 
		"$<$<CONFIG:Release>:${BOX2D_LIBRARY_RELEASE}>"
		"$<$<CONFIG:RelWithDebInfo>:${BOX2D_LIBRARY_RELEASE}>"
	)
	
	message("Box2D: Found in ${box2d_path}")
else()
	set(BOX2D_FOUND FALSE)
	message("Box2D: Couldn't find in ${box2d_path}")
endif()

set(box2d_path)
