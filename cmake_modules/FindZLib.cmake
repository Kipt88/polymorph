message("ZLib: Running FindZLib.cmake")

if(NOT ZLib_FIND_VERSION)
	set(ZLib_FIND_VERSION "1.2.8")
endif()

message("ZLib: Using version ${ZLib_FIND_VERSION}")

set(zlib_path "${EXTERNALS_ROOT}/zlib/${ZLib_FIND_VERSION}")

find_library(
	ZLib_LIBRARY_RELEASE "zlib-mt" 
	PATHS "${zlib_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
)

find_library(
	ZLib_LIBRARY_DEBUG "zlib-mtd" 
	PATHS "${zlib_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
)

if(ZLib_LIBRARY_DEBUG AND ZLib_LIBRARY_RELEASE)
	set(ZLib_FOUND TRUE)
	set(ZLib_INCLUDE_DIRS "${zlib_path}/include")
	message("ZLib: Found in ${zlib_path}")
else()
	set(ZLib_FOUND FALSE)
	message("ZLib: Couldn't find in ${zlib_path}")
endif()

set(zlib_path)
