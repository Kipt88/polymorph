message("GLEW: Running FindGLEW.cmake")

if(NOT GLEW_FIND_VERSION)
	set(GLEW_FIND_VERSION "1.13.0")
endif()

message("GLEW: Using version ${GLEW_FIND_VERSION}")

set(glew_path "${EXTERNALS_ROOT}/glew/${GLEW_FIND_VERSION}")

if(USE_GLEW_MX)
	message("GLEW: Using MX")
	
	find_library(
		GLEW_LIBRARY_RELEASE "glewmxs-mt" 
		PATHS "${glew_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
	)
	
	find_library(
		GLEW_LIBRARY_DEBUG "glewmxs-mtd" 
		PATHS "${glew_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
	)
else()
	find_library(
		GLEW_LIBRARY_RELEASE "glews-mt" 
		PATHS "${glew_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
	)
	
	find_library(
		GLEW_LIBRARY_DEBUG "glews-mtd" 
		PATHS "${glew_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
	)
endif()

if(GLEW_LIBRARY_DEBUG AND GLEW_LIBRARY_RELEASE)
	set(GLEW_FOUND TRUE)
	set(GLEW_INCLUDE_DIRS "${glew_path}/include")
	set(GLEW_DEFINITIONS "GLEW_STATIC")
	message("GLEW: Found in ${glew_path}")
else()
	set(GLEW_FOUND FALSE)
	message("GLEW: Couldn't find in ${glew_path}")
endif()

set(glew_path)
