message("Spine-C: Running FindSpine-C.cmake")

set(spine-c_path "${EXTERNALS_ROOT}/spine-c")

find_library(
	SPINE-C_LIBRARY_RELEASE "spine-c-mt"
	PATHS "${spine-c_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
)

find_library(
	SPINE-C_LIBRARY_DEBUG "spine-c-mtd"
	PATHS "${spine-c_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
)

if(SPINE-C_LIBRARY_DEBUG AND SPINE-C_LIBRARY_RELEASE)
	set(SPINE-C_FOUND TRUE)
	set(SPINE-C_INCLUDE_DIRS "${spine-c_path}/include")
	set(SPINE-C_LIBRARIES 
		"$<$<CONFIG:Debug>:${SPINE-C_LIBRARY_DEBUG}>" 
		"$<$<CONFIG:Release>:${SPINE-C_LIBRARY_RELEASE}>"
		"$<$<CONFIG:RelWithDebInfo>:${SPINE-C_LIBRARY_RELEASE}>"
	)
	
	message("Spine-C: Found in ${spine-c_path}")
else()
	set(SPINE-C_FOUND FALSE)
	message("Spine-C: Couldn't find in ${spine-c_path}")
endif()

set(spine-c_path)
