message("GTest: Running FindGTest.cmake")

if(NOT GTest_FIND_VERSION)
	set(GTest_FIND_VERSION "1.7.0")
endif()

message("GTest: Using version ${GTest_FIND_VERSION}")

set(gtest_path "${EXTERNALS_ROOT}/gtest/${GTest_FIND_VERSION}")

if(WIN32)
	find_library(
		GTest_LIBRARY_RELEASE "gtest-mt"
		PATHS "${gtest_path}/lib/${PLATFORM}"
	)

	find_library(
		GTest_LIBRARY_DEBUG "gtest-mtd"
		PATHS "${gtest_path}/lib/${PLATFORM}"
	)
else()
	find_library(
		GTest_LIBRARY_RELEASE "gtest"
		PATHS "${gtest_path}/lib/${PLATFORM}"
	)

	find_library(
		GTest_LIBRARY_DEBUG "gtest-d"
		PATHS "${gtest_path}/lib/${PLATFORM}"
	)
	
	#set(GTest_LIBRARY_RELEASE "")
	#set(GTest_LIBRARY_DEBUG "")

endif()

if(GTest_LIBRARY_DEBUG AND GTest_LIBRARY_RELEASE)
	set(GTest_FOUND TRUE)
	set(GTest_INCLUDE_DIRS "${gtest_path}/include")
	set(GTest_SOURCES "${gtest_path}/source/gtest_main.cc" "${gtest_path}/source/gtest-all.cc")
	set(GTest_LIBRARIES "$<$<CONFIG:Debug>:${GTest_LIBRARY_DEBUG}>" "$<$<CONFIG:Release>:${GTest_LIBRARY_RELEASE}>")
	message("GTest: Found in ${gtest_path}")
	message("Incl. dir = ${GTest_INCLUDE_DIRS}")
else()
	set(GTest_FOUND FALSE)
	message("GTest: Couldn't find in ${gtest_path}")
endif()

set(gtest_path)
