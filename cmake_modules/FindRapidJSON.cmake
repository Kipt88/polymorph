message("RapidJSON: Running RapidJSON.cmake")

if(NOT RapidJSON_FIND_VERSION)
	set(RapidJSON_FIND_VERSION "1.0.2")
endif()

message("RapidJSON: Using version ${RapidJSON_FIND_VERSION}")

set(rapidjson_path "${EXTERNALS_ROOT}/rapidjson/${RapidJSON_FIND_VERSION}")

if(EXISTS ${rapidjson_path})
	set(RapidJSON_FOUND TRUE)
	set(RapidJSON_INCLUDE_DIRS "${rapidjson_path}/include")
	message("RapidJSON: Found in ${rapidjson_path}")
else()
	set(RapidJSON_FOUND FALSE)
	message("RapidJSON: Couldn't find in ${rapidjson_path}")
endif()

set(rapidjson_path)
