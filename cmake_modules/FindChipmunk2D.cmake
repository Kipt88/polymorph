message("Chipmunk2D: Running FindChipmunk2D.cmake")

if(NOT CHIPMUNK2D_FIND_VERSION)
	set(CHIPMUNK2D_FIND_VERSION "7.0.1")
endif()

message("Chipmunk2D: Using version ${CHIPMUNK2D_FIND_VERSION}")

set(chipmunk2d_path "${EXTERNALS_ROOT}/chipmunk2d/${CHIPMUNK2D_FIND_VERSION}")

find_library(
	CHIPMUNK2D_LIBRARY_RELEASE "chipmunk-mt"
	PATHS "${chipmunk2d_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
)

find_library(
	CHIPMUNK2D_LIBRARY_DEBUG "chipmunk-mtd"
	PATHS "${chipmunk2d_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
)

if(CHIPMUNK2D_LIBRARY_DEBUG AND CHIPMUNK2D_LIBRARY_RELEASE)
	set(CHIPMUNK2D_FOUND TRUE)
	set(CHIPMUNK2D_INCLUDE_DIRS "${chipmunk2d_path}/include")
	set(CHIPMUNK2D_LIBRARIES 
		"$<$<CONFIG:Debug>:${CHIPMUNK2D_LIBRARY_DEBUG}>" 
		"$<$<CONFIG:Release>:${CHIPMUNK2D_LIBRARY_RELEASE}>"
	)
	
	message("Chipmunk2D: Found in ${chipmunk2d_path}")
else()
	set(CHIPMUNK2D_FOUND FALSE)
	message("Chipmunk2D: Couldn't find in ${chipmunk2d_path}")
endif()

set(chipmunk2d_path)
