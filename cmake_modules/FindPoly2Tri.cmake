message("Poly2Tri: Running FindPoly2Tri.cmake")

set(poly2tri_path "${EXTERNALS_ROOT}/poly2tri/lib/${PLATFORM}")

find_library(
	Poly2Tri_LIBRARY_RELEASE "poly2tri-mt" 
	PATHS "${poly2tri_path}" NO_DEFAULT_PATH
)

find_library(
	Poly2Tri_LIBRARY_DEBUG "poly2tri-mtd" 
	PATHS "${poly2tri_path}" NO_DEFAULT_PATH
)

if(Poly2Tri_LIBRARY_DEBUG AND Poly2Tri_LIBRARY_RELEASE)
	set(Poly2Tri_LIBRARY_DEBUG ${Poly2Tri_LIBRARY_DEBUG})
	set(Poly2Tri_LIBRARY_RELEASE ${Poly2Tri_LIBRARY_RELEASE})
	
	set(Poly2Tri_FOUND TRUE)
	set(Poly2Tri_INCLUDE_DIRS "${EXTERNALS_ROOT}/poly2tri/include")
	message("Poly2Tri: Found in ${poly2tri_path}")
else()
	set(Poly2Tri_FOUND FALSE)
	message("Poly2Tri: Couldn't find in ${poly2tri_path}")
endif()

set(poly2tri_path)
