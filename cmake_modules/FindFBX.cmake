message("FBX: Running FindFBX.cmake")

if(NOT FBX_FIND_VERSION)
	set(FBX_FIND_VERSION "2016.1.2")
endif()

message("FBX: Using version ${FBX_FIND_VERSION}")

set(fbx_path "${EXTERNALS_ROOT}/fbx/${FBX_FIND_VERSION}")

find_library(
	FBX_LIBRARY_RELEASE "libfbxsdk-mt"
	PATHS "${fbx_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
)

find_library(
	FBX_LIBRARY_DEBUG "libfbxsdk-mtd"
	PATHS "${fbx_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
)

if(FBX_LIBRARY_DEBUG AND FBX_LIBRARY_RELEASE)
	set(FBX_FOUND TRUE)
	set(FBX_INCLUDE_DIRS "${fbx_path}/include")
	set(FBX_LIBRARIES "$<$<CONFIG:Debug>:${FBX_LIBRARY_DEBUG}>" "$<$<CONFIG:Release>:${FBX_LIBRARY_RELEASE}>")
	message("FBX: Found in ${fbx_path}")
else()
	set(FBX_FOUND FALSE)
	message("FBX: Couldn't find in ${fbx_path}")
endif()

set(fbx_path)
