# On Android Vulkan is supported by the NDK as long as the API level is 24 or greater.
if (ANDROID)
	set (VULKAN_INCLUDE_DIRS )
	set (VULKAN_LIBRARIES vulkan)
	set (VULKAN_TOOLSDIR $ENV{ANDROID_NDK}/shader-tools/${ANDROID_HOST_TAG})

# Asssume LunarG Vulkan SDK for non-Android platforms;
# LunarG SDK installer sets VULKAN_SDK environment variable on Windows
# so we use that to find the headers and library (needs to be set manually on Linux)
else()

	if (NOT DEFINED ENV{VULKAN_SDK})
		message ("VULKAN_SDK not found or specified")
		set(VULKAN_FOUND FALSE)

		if (VULKAN_FIND_REQUIRED)
			message (FATAL_ERROR "VULKAN_SDK not specified, unable to locate SDK")
		endif()

		return()
	endif()

	if (WIN32)
		set (VULKAN_INCLUDE_DIRS $ENV{VULKAN_SDK}/Include)
		set (VULKAN_LIBDIR $ENV{VULKAN_SDK}/Bin)
		set (VULKAN_LIB vulkan-1)

		if (CMAKE_SIZEOF_VOID_P EQUAL 4)
			set (VULKAN_LIBDIR ${VULKAN_LIBDIR}32) # Bin32
		endif()

		set (VULKAN_TOOLSDIR ${VULKAN_LIBDIR})

	elseif (LINUX)
		set (VULKAN_INCLUDE_DIRS $ENV{VULKAN_SDK}/x86_64/include)
		set (VULKAN_LIBDIR $ENV{VULKAN_SDK}/x86_64/lib)
		set (VULKAN_TOOLSDIR $ENV{VULKAN_SDK}/x86_64/bin)
		set (VULKAN_LIB vulkan)

	endif()

	find_library(
		VULKAN_LIBRARY ${VULKAN_LIB}
		PATHS "${VULKAN_LIBDIR}" NO_DEFAULT_PATH
	)

	if (VULKAN_LIBRARY STREQUAL VULKAN_LIBRARY-NOTFOUND)
		message (FATAL_ERROR "Unable to locate Vulkan library")
	endif()

	set (VULKAN_LIBRARIES ${VULKAN_LIBRARY})

	unset (VULKAN_LIB)
	unset (VULKAN_LIBRARY)
	unset (VULKAN_LIBDIR)

endif()

set(VULKAN_FOUND TRUE)
