message("FlatBuffers: Running FindFlatBuffers.cmake")

if(NOT FLATBUFFERS_FIND_VERSION)
	set(FLATBUFFERS_FIND_VERSION "1.4.0")
endif()

message("FlatBuffers: Using version ${FLATBUFFERS_FIND_VERSION}")

set(flatbuffers_path "${EXTERNALS_ROOT}/flatbuffers/${FLATBUFFERS_FIND_VERSION}")

find_library(
	FLATBUFFERS_LIBRARY_RELEASE "flatbuffers-mt"
	PATHS "${flatbuffers_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
)

find_library(
	FLATBUFFERS_LIBRARY_DEBUG "flatbuffers-mtd"
	PATHS "${flatbuffers_path}/lib/${PLATFORM}" NO_DEFAULT_PATH
)

find_program(
	FLATBUFFERS_EXECUTABLE "flatc-mt"
	PATHS "${flatbuffers_path}/bin/${PLATFORM}" NO_DEFAULT_PATH
)

if(FLATBUFFERS_LIBRARY_DEBUG AND FLATBUFFERS_LIBRARY_RELEASE AND FLATBUFFERS_EXECUTABLE)
	set(FLATBUFFERS_FOUND TRUE)
	set(FLATBUFFERS_INCLUDE_DIRS "${flatbuffers_path}/include")
	set(FLATBUFFERS_LIBRARIES 
		"$<$<CONFIG:Debug>:${FLATBUFFERS_LIBRARY_DEBUG}>"
		"$<$<CONFIG:Release>:${FLATBUFFERS_LIBRARY_RELEASE}>"
	)
	
	message("FlatBuffers: Found in ${flatbuffers_path}")
else()
	set(FLATBUFFERS_FOUND FALSE)
	message("FlatBuffers: Couldn't find in ${flatbuffers_path}")
endif()

set(flatbuffers_path)

if(FLATBUFFERS_FOUND)
	function(flatbuffers_generate_header var_out out_folder)
		set(FLATC_OUTPUTS)
		foreach(schema ${ARGN})
			get_filename_component(FLATC_OUTPUT ${schema} NAME_WE)
			set(
				FLATC_OUTPUT
				"${out_folder}/${FLATC_OUTPUT}_generated.h"
			)
			list(APPEND FLATC_OUTPUTS ${FLATC_OUTPUT})
			
			add_custom_command(
				OUTPUT ${FLATC_OUTPUT}
				COMMAND "${FLATBUFFERS_EXECUTABLE}" "--cpp" "-o" "${out_folder}" "${schema}"
				COMMENT "${FLATBUFFERS_EXECUTABLE}: Building C++ header for ${schema} -> ${FLATC_OUTPUT}"
				WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
			)
		endforeach()
		set(${var_out} ${FLATC_OUTPUTS} PARENT_SCOPE)
	endfunction()
endif()
