message("LibPNG: Running FindLibPNG.cmake")

if(LibPNG_FIND_REQUIRED)
	find_package(ZLib REQUIRED)
else()
	find_package(ZLib)
endif()

if(NOT LibPNG_FIND_VERSION)
	set(LibPNG_FIND_VERSION "1.6.18")
endif()

message("LibPNG: Using version ${LibPNG_FIND_VERSION}")

set(libpng_path "${EXTERNALS_ROOT}/libpng/${LibPNG_FIND_VERSION}/lib/${PLATFORM}")

find_library(
	LibPNG_LIBRARY_RELEASE "libpng-mt" 
	PATHS "${libpng_path}" NO_DEFAULT_PATH
)

find_library(
	LibPNG_LIBRARY_DEBUG "libpng-mtd" 
	PATHS "${libpng_path}" NO_DEFAULT_PATH
)

if(LibPNG_LIBRARY_DEBUG AND LibPNG_LIBRARY_RELEASE)
	set(LibPNG_LIBRARY_DEBUG ${LibPNG_LIBRARY_DEBUG} ${ZLib_LIBRARY_DEBUG})
	set(LibPNG_LIBRARY_RELEASE ${LibPNG_LIBRARY_RELEASE} ${ZLib_LIBRARY_RELEASE})
	
	set(LibPNG_FOUND TRUE)
	set(LibPNG_INCLUDE_DIRS "${EXTERNALS_ROOT}/libpng/${LibPNG_FIND_VERSION}/include" ${ZLib_INCLUDE_DIRS})
	message("LibPNG: Found in ${libpng_path}")
else()
	set(LibPNG_FOUND FALSE)
	message("LibPNG: Couldn't find in ${libpng_path}")
endif()

set(libpng_path)
