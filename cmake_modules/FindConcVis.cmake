# Concurrency Visualizer is only available on Windows
if (NOT WIN32)
	set(CONCVIS_FOUND FALSE)

	if (CONCVIS_FIND_REQUIRED)
		message (FATAL_ERROR "Concurrency Visualizer is not supported on non-Windows platforms")
	endif()

	return()
endif()

set (VS_TOOLSDIR $ENV{VS140COMNTOOLS})
get_filename_component (VS_COMMONDIR ${VS_TOOLSDIR} DIRECTORY)
set (VS_EXT_DIR ${VS_COMMONDIR}/IDE/Extensions)

file (GLOB CV_HEADER ${VS_EXT_DIR}/*/SDK/Native/Inc/cvmarkers.h)

# Concurrency Visualizer not installed
if (NOT CV_HEADER)
	set(CONCVIS_FOUND FALSE)
	message ("CONCVIS_INC not found")

	if (CONCVIS_FIND_REQUIRED)
		message (FATAL_ERROR "Concurrency Visualizer is not installed")
	endif()

	return()
endif()

get_filename_component (CONCVIS_INCLUDE_DIRS ${CV_HEADER} DIRECTORY)
set (CONCVIS_FOUND TRUE)

unset (VS_TOOLSDIR)
unset (VS_COMMONDIR)
unset (VS_EXT_DIR)
unset (CV_HEADER)
