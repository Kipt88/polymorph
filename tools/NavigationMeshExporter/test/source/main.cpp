#include <ai/NavigationMeshReader.hpp>
#include <Debug.hpp>

#include <filesystem/FileInputStream.hpp>
#include <filesystem.hpp>

int main()
{
	using namespace pm;
	namespace fs = std::filesystem;

	try
	{
		{
			fs::path p = fs::current_path() / ".." / ".." / "test" / "out_meshes" / "test1.navmesh";
			filesystem::FileInputStream input(p);

			ai::NavigationMeshReader::read(input);
		}

		{
			fs::path p = fs::current_path() / ".." / ".." / "test" / "out_meshes" / "test2.navmesh";
			filesystem::FileInputStream input(p);

			ai::NavigationMeshReader::read(input);
		}
	}
	catch (const std::exception& e)
	{
		ERROR_OUT("NavigationMeshExporter_test", "Error loading mesh: '%s'", e.what());
		return -1;
	}

	return 0;
}