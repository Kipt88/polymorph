#include "Exporter.hpp"

#include <physics2d/PolygonDecomposer.hpp>
#include <physics2d/BodyDefReader.hpp>
#include <filesystem/FileInputStream.hpp>
#include <filesystem/FileOutputStream.hpp>
#include <math/Box.hpp>
#include <WingedEdgeMesh.hpp>
#include <Exception.hpp>
#include <Id.hpp>
#include <optional.hpp>

#include <poly2tri.h>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/geometries.hpp>
#include <boost/geometry/io/io.hpp>
#include <boost/geometry/arithmetic/arithmetic.hpp>

#include <algorithm>
#include <numeric>
#include <iostream>
#include <fstream>
#include <cctype>

namespace fs = std::filesystem;

namespace boost { namespace geometry { namespace traits { 

template <>
struct tag<pm::math::Vector<pm::physics2d::real_t, 2>>
{
	typedef point_tag type;
};

template <>
struct coordinate_type<pm::math::Vector<pm::physics2d::real_t, 2>>
{
	typedef pm::physics2d::real_t type;
};

template <>
struct coordinate_system<pm::math::Vector<pm::physics2d::real_t, 2>>
{
	typedef cs::cartesian type;
};

template <>
struct dimension<pm::math::Vector<pm::physics2d::real_t, 2>> : boost::mpl::int_<2> {};

template <>
struct access<pm::math::Vector<pm::physics2d::real_t, 2>, 0>
{
	static pm::physics2d::real_t get(const pm::math::Vector<pm::physics2d::real_t, 2>& p)
	{
		return p[0];
	}

	static void set(pm::math::Vector<pm::physics2d::real_t, 2>& p, const pm::physics2d::real_t& val)
	{
		p[0] = val;
	}
};

template <>
struct access<pm::math::Vector<pm::physics2d::real_t, 2>, 1>
{
	static pm::physics2d::real_t get(const pm::math::Vector<pm::physics2d::real_t, 2>& p)
	{
		return p[1];
	}

	static void set(pm::math::Vector<pm::physics2d::real_t, 2>& p, const pm::physics2d::real_t& val)
	{
		p[1] = val;
	}
};

} } }

namespace pm {

PM_MAKE_EXCEPTION_CLASS(ParsingException, Exception);

namespace {

typedef math::Vector<physics2d::real_t, 2> Vector2_r;
typedef Vector2_r point_t;
typedef boost::geometry::model::polygon<point_t, false, false> polygon_t;
typedef boost::geometry::model::ring<point_t, false, false> ring_t;

typedef WingedEdgeMesh<Vector2_r, detail::empty, physics2d::real_t> Mesh;

bool needExport(const fs::path& inputFile, const fs::path& outputFile)
{
	if (!fs::exists(outputFile))
		return true;

	auto lastExportTime = fs::last_write_time(outputFile);
	auto lastEditTime = fs::last_write_time(inputFile);

	return lastExportTime < lastEditTime;
}

/*
void dumpPolygons(const std::vector<polygon_t>& polygons)
{
	std::ofstream svg("output.svg");
	boost::geometry::svg_mapper<point_t> mapper(svg, 400, 400);

	for (const auto& polygon : polygons)
	{
		mapper.add(polygon.outer());
		mapper.map(polygon.outer(), "fill-opacity:0;stroke:rgb(255,0,0);stroke-width:2");

		for (const auto& inner : polygon.inners())
		{
			mapper.add(inner);
			mapper.map(inner, "fill-opacity:0;stroke:rgb(0,255,0);stroke-width:2");
		}
	}
}

void dumpRings(const std::vector<ring_t>& rings)
{
	std::ofstream svg("output.svg");
	boost::geometry::svg_mapper<point_t> mapper(svg, 600, 600);

	for (const auto& ring : rings)
	{
		mapper.add(ring);
		mapper.map(ring, "fill-opacity:0;stroke:rgb(255,0,0);stroke-width:2");
	}
}
*/

math::AxisAlignedBox2<physics2d::real_t> calculateBounds(const physics2d::BodyDefinition& body)
{
	if (body.shapes.empty())
		throw ParsingException("Input body lacks shapes.");

	Vector2_r minPos{ 
		std::numeric_limits<physics2d::real_t>::infinity(), 
		std::numeric_limits<physics2d::real_t>::infinity()
	};
	
	Vector2_r maxPos{
		-std::numeric_limits<physics2d::real_t>::infinity(),
		-std::numeric_limits<physics2d::real_t>::infinity()
	};

	for (const auto& shape : body.shapes)
	{
		Vector2_r shapeMinPos;
		Vector2_r shapeMaxPos;

		switch (shape.shape)
		{
		case physics2d::Shape::BOX:
			shapeMinPos = -Vector2_r{ shape.geometry.box.width * 0.5f, shape.geometry.box.height * 0.5f };
			shapeMaxPos =  Vector2_r{ shape.geometry.box.width * 0.5f, shape.geometry.box.height * 0.5f };
			break;

		case physics2d::Shape::POLYGON:
			shapeMinPos = shape.geometry.polygon.vertices[0];
			shapeMaxPos = shape.geometry.polygon.vertices[0];

			for (unsigned int i = 1; i < shape.geometry.polygon.size; ++i)
			{
				const auto& v = shape.geometry.polygon.vertices[i];

				shapeMinPos[0] = std::min(shapeMinPos[0], v[0]);
				shapeMinPos[1] = std::min(shapeMinPos[1], v[1]);
				shapeMaxPos[0] = std::max(shapeMaxPos[0], v[0]);
				shapeMaxPos[1] = std::max(shapeMaxPos[1], v[1]);
			}

			break;

		case physics2d::Shape::CIRCLE:
			shapeMinPos = -Vector2_r{ shape.geometry.circle.radius, shape.geometry.circle.radius };
			shapeMaxPos =  Vector2_r{ shape.geometry.circle.radius, shape.geometry.circle.radius };
			break;
		}

		shapeMinPos += shape.localPos;
		shapeMaxPos += shape.localPos;

		minPos[0] = std::min(minPos[0], shapeMinPos[0]);
		minPos[1] = std::min(minPos[1], shapeMinPos[1]);
		maxPos[0] = std::max(maxPos[0], shapeMaxPos[0]);
		maxPos[1] = std::max(maxPos[1], shapeMaxPos[1]);
	}

	return math::AxisAlignedBox2<physics2d::real_t>{ minPos, maxPos };
}

std::vector<polygon_t> createPolygonMesh(const physics2d::BodyDefinition& body)
{
	auto aabb = calculateBounds(body);

	std::vector<polygon_t> resolvedPolygons;

	{
		polygon_t bounds;

		boost::geometry::append(bounds.outer(), point_t{ aabb.min()[0], aabb.min()[1] });
		boost::geometry::append(bounds.outer(), point_t{ aabb.max()[0], aabb.min()[1] });
		boost::geometry::append(bounds.outer(), point_t{ aabb.max()[0], aabb.max()[1] });
		boost::geometry::append(bounds.outer(), point_t{ aabb.min()[0], aabb.max()[1] });

		resolvedPolygons.push_back(std::move(bounds));
	}

	for (const auto& shape : body.shapes)
	{
		if (shape.shape != physics2d::Shape::POLYGON)
		{
			std::cout << "Warning, ignoring non-polygon shape.\n";
			continue;
		}

		polygon_t shapePolygon;

		for (unsigned int i = 0; i < shape.geometry.polygon.size; ++i)
		{
			boost::geometry::append(
				shapePolygon.outer(),
				point_t{ shape.geometry.polygon.vertices[i][0], shape.geometry.polygon.vertices[i][1] }
			);
		}

		std::string msg;
		if (!boost::geometry::is_valid(shapePolygon, msg))
		{
			ERROR_OUT("Test", "Invalid input polygon: %s", msg.c_str());
			std::terminate();
		}

		std::vector<polygon_t> inputPolygons = std::move(resolvedPolygons);

		for (const auto& polygon : inputPolygons)
		{
			boost::geometry::difference(polygon, shapePolygon, resolvedPolygons);
		}
	}

	return resolvedPolygons;
}

Mesh triangulate(std::vector<polygon_t>&& polygons)
{
	Mesh mesh;

	for (const auto& polygon : polygons)
	{
		std::deque<p2t::Point> points;
		std::vector<p2t::Point*> pointPointers; // Horrors of p2t...

		std::deque<p2t::Point> holePoints;

		for (auto v : polygon.outer())
		{
			auto it = points.insert(points.end(), p2t::Point{ v[0], v[1] });

			pointPointers.push_back(std::addressof(*it));
		}

		p2t::CDT cdt(pointPointers);

		for (const auto& hole : polygon.inners())
		{
			std::vector<p2t::Point*> holePointPointers;

			for (const auto& v : hole)
			{
				auto it = holePoints.insert(holePoints.end(), p2t::Point{ v[0], v[1] });

				holePointPointers.push_back(std::addressof(*it));
			}

			cdt.AddHole(holePointPointers);
		}

		cdt.Triangulate();

		auto triangles = cdt.GetTriangles();

		std::map<p2t::Point*, vertex_index_t> vertexMapping;
		std::map<std::pair<vertex_index_t, vertex_index_t>, edge_index_t> edgeMapping;

		for (auto* triangle : triangles)
		{
			std::array<vertex_index_t, 3> vertices;
			std::array<edge_index_t, 3> edges;

			// Add vertices...
			for (unsigned int i = 0; i < 3; ++i)
			{
				auto it = vertexMapping.find(triangle->GetPoint(i));

				if (it == vertexMapping.end())
				{
					it = vertexMapping.insert(
						std::make_pair(
							triangle->GetPoint(i),
							mesh.addVertex(
								Vector2_r{
									static_cast<physics2d::real_t>(triangle->GetPoint(i)->x),
									static_cast<physics2d::real_t>(triangle->GetPoint(i)->y)
								}
							)
						)
					).first;
				}

				vertices[i] = it->second;
			}

			// Add edges...
			for (unsigned int i = 0; i < 3; ++i)
			{
				auto edge = std::make_pair(
					vertices[i],
					vertices[(i + 1) % 3]
				);

				if (edge.first > edge.second)
					std::swap(edge.first, edge.second);

				auto it = edgeMapping.find(edge);

				if (it == edgeMapping.end())
				{
					it = edgeMapping.insert(
						std::make_pair(
							edge,
							mesh.addEdge(
								{}, 
								vertices[i], 
								vertices[(i + 1) % 3]
							)
						)
					).first;
				}

				edges[i] = it->second;
			}

			physics2d::real_t area;

			{
				ring_t ring;
				ring.reserve(3);

				for (vertex_index_t i = 0; i < 3; ++i)
					ring.push_back(mesh.vertex(vertices[i]));

				area = boost::geometry::area(ring);
			}

			// Add face...
			mesh.addFace(area, { edges[0], edges[1], edges[2] });
		}
	}
	
	return mesh;
}

bool isConvex(const std::vector<Vector2_r>& vertices)
{
	if (vertices.size() < 4)
		return true;

	bool sign = false;

	auto size = vertices.size();

	for (unsigned int i = 0; i < size; ++i)
	{
		Vector2_r d1 = vertices[(i + 2) % size] - vertices[(i + 1) % size];
		Vector2_r d2 = vertices[(i + 1) % size] - vertices[i];

		auto cross = d1[0] * d2[1] - d1[1] * d2[0];

		if (i == 0)
			sign = (cross > 0);
		else if (sign != (cross > 0))
			return false;
	}

	return true;
}

std::vector<vertex_index_t> mergedPolygon(const Mesh& mesh, edge_index_t edge)
{
	auto faces = mesh.edgeFaces(edge);
	if (!(faces[0] && faces[1]))
		return {};

	auto edgeVertices = mesh.edgeVertices(edge);

	std::vector<vertex_index_t> mergedVertexIndices = mesh.faceVertices(*faces[0]);
	std::vector<vertex_index_t>&& rightVertices = mesh.faceVertices(*faces[1]);

	std::size_t edgeLeftIndex;

	{
		auto itA = std::find(mergedVertexIndices.begin(), mergedVertexIndices.end(), edgeVertices[0]);
		auto itB = std::find(mergedVertexIndices.begin(), mergedVertexIndices.end(), edgeVertices[1]);

		std::size_t indexA = std::distance(mergedVertexIndices.begin(), itA);
		std::size_t indexB = std::distance(mergedVertexIndices.begin(), itB);

		if ((indexA + 1) % mergedVertexIndices.size() == indexB)
		{
			edgeLeftIndex = indexA;
		}
		else
		{
			ASSERT((indexB + 1) % mergedVertexIndices.size() == indexA, "Can't find index to extend polygon.");
			edgeLeftIndex = indexB;
		}
	}

	std::size_t edgeRightIndex;

	{
		auto itA = std::find(rightVertices.begin(), rightVertices.end(), edgeVertices[0]);
		auto itB = std::find(rightVertices.begin(), rightVertices.end(), edgeVertices[1]);

		std::size_t indexA = std::distance(rightVertices.begin(), itA);
		std::size_t indexB = std::distance(rightVertices.begin(), itB);

		if ((indexA + 1) % rightVertices.size() == indexB)
		{
			edgeRightIndex = indexB;
		}
		else
		{
			ASSERT((indexB + 1) % rightVertices.size() == indexA, "Can't find index to extend polygon.");
			edgeRightIndex = indexA;
		}
	}

	std::size_t startIndex = edgeLeftIndex + 1;

	mergedVertexIndices.insert(
		mergedVertexIndices.begin() + startIndex,
		rightVertices.size() - 2,
		static_cast<vertex_index_t>(-1)
	);

	for (unsigned int i = 0; i < rightVertices.size() - 2; ++i)
	{
		mergedVertexIndices[startIndex + i] = rightVertices[(edgeRightIndex + 1 + i) % rightVertices.size()];
	}

	return mergedVertexIndices;
}

void mergeMesh(Mesh& mesh)
{
	edge_index_t minEdge;

	do
	{
		minEdge = -1;

		physics2d::real_t minArea;
		std::array<face_index_t, 2> minFaces;
		std::vector<edge_index_t> newFace;

		for (edge_index_t i = 0; i < mesh.edgeSize(); ++i)
		{
			auto vertexIndices = mergedPolygon(mesh, i);

			if (!vertexIndices.empty())
			{
				std::vector<Vector2_r> vertices;
				vertices.reserve(vertexIndices.size());

				for (vertex_index_t index : vertexIndices)
					vertices.push_back(mesh.vertex(index));

				if (isConvex(vertices))
				{
					auto faces = mesh.edgeFaces(i);

					auto area = mesh.face(*faces[0]) + mesh.face(*faces[1]);

					if (minEdge == -1 || area < minArea)
					{
						minEdge = i;
						minArea = area;
						minFaces = { *faces[0], *faces[1] };
						
						newFace.clear();

						for (unsigned int i = 0; i < vertexIndices.size(); ++i)
						{
							vertex_index_t vertexIndexA = vertexIndices[i];
							vertex_index_t vertexIndexB = vertexIndices[(i + 1) % vertexIndices.size()];

							auto edge = mesh.findEdge(vertexIndexA, vertexIndexB);

							ASSERT(edge, "Can't find edge?");

							newFace.push_back(*edge);
						}
					}
				}
			}
		}

		if (minEdge != -1)
		{
			Mesh tempMesh;

			for (vertex_index_t i = 0; i < mesh.vertexSize(); ++i)
				tempMesh.addVertex(mesh.vertex(i));
			
			for (edge_index_t i = 0; i < mesh.edgeSize(); ++i)
			{
				if (i != minEdge)
					tempMesh.addEdge(mesh.edge(i), mesh.edgeVertices(i)[0], mesh.edgeVertices(i)[1]);
			}

			for (face_index_t i = 0; i < mesh.faceSize(); ++i)
			{
				if (i != minFaces[0] && i != minFaces[1])
				{
					auto&& edges = mesh.faceEdges(i);

					for (edge_index_t& edgeIndex : edges)
					{
						ASSERT(edgeIndex != minEdge, "Trying to create new face with removed edge.");

						if (edgeIndex > minEdge)
							--edgeIndex;
					}

					tempMesh.addFace(mesh.face(i), edges);
				}
			}

			for (edge_index_t& edgeIndex : newFace)
			{
				ASSERT(edgeIndex != minEdge, "Trying to create new face with removed edge.");

				if (edgeIndex > minEdge)
					--edgeIndex;
			}

			tempMesh.addFace(minArea, newFace);

			mesh = std::move(tempMesh);

			/*
			std::vector<ring_t> rings;

			for (face_index_t i = 0; i < mesh.faceSize(); ++i)
			{
				ring_t ring;
				ring.reserve(3);

				const auto& vertexIndices = mesh.faceVertices(i);

				for (auto index : vertexIndices)
					ring.push_back(mesh.vertex(index));

				rings.push_back(std::move(ring));
			}

			dumpRings(rings);
			*/
		}
	}
	while (minEdge != -1);
}

Mesh generateNavigationMesh(const physics2d::BodyDefinition& body)
{
	auto mesh = triangulate(createPolygonMesh(body));
	
	// Merged small triangles to create a convex polygon mesh.
	mergeMesh(mesh);

	/*
	std::vector<ring_t> rings;

	for (face_index_t i = 0; i < mesh.faceSize(); ++i)
	{
		ring_t ring;
		ring.reserve(3);

		const auto& vertexIndices = mesh.faceVertices(i);

		for (auto index : vertexIndices)
		{
			ring.push_back(mesh.vertex(index));
		}

		rings.push_back(std::move(ring));
	}

	dumpRings(rings);
	*/

	return mesh;
}

void exportMesh(io::OutputStream& output, const Mesh& mesh)
{
	output << static_cast<std::uint32_t>(mesh.vertexSize());

	for (unsigned int i = 0; i < mesh.vertexSize(); ++i)
	{
		auto v = mesh.vertex(i);

		output << static_cast<float>(v[0]) << static_cast<float>(v[1]);
	}

	output << static_cast<std::uint32_t>(mesh.edgeSize());

	for (unsigned int i = 0; i < mesh.edgeSize(); ++i)
	{
		auto vs = mesh.edgeVertices(i);

		output << static_cast<std::uint32_t>(vs[0]) << static_cast<std::uint32_t>(vs[1]);
	}

	output << static_cast<std::uint32_t>(mesh.faceSize());

	for (unsigned int i = 0; i < mesh.faceSize(); ++i)
	{
		auto edges = mesh.faceEdges(i);

		output << static_cast<std::uint32_t>(edges.size());

		for (unsigned int j = 0; j < edges.size(); ++j)
			output << static_cast<std::uint32_t>(edges[j]);
	}
}

}

void exportNavigationMesh(const fs::path& input, const fs::path& output, bool forceExport)
{
	if (forceExport || needExport(input, output))
	{
		std::cout << "Exporting '" << input << "'.\n";
		
		filesystem::FileInputStream inputStream(input);
		physics2d::BodyDefinition body = physics2d::BodyDefReader::read(inputStream);
		
		auto&& mesh = generateNavigationMesh(body);

		filesystem::FileOutputStream outputStream(output);
		exportMesh(outputStream, mesh);
	}
	else
	{
		std::cout << "Not exporting '" << input << "', no need.\n";
	}
}

}
