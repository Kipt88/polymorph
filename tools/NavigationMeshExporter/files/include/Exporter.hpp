#pragma once

#include <experimental/filesystem>
#include <vector>

namespace pm {

void exportNavigationMesh(
	const std::experimental::filesystem::path& input,
	const std::experimental::filesystem::path& output,
	bool forceExport
);

}