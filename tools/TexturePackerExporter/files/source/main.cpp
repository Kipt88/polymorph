#include "SpriteExporter.hpp"

#include <gen/Indexer.hpp>
#include <gen/CommandLineArgs.hpp>
#include <Debug.hpp>
#include <experimental/filesystem>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cstdio>

using namespace pm;

namespace fs = std::experimental::filesystem;

PM_MAKE_EXCEPTION_CLASS(InvalidCmdLineArgumentException, Exception);

namespace {

const char* LOG_TAG = "TexturePackerExporter";

}

int main(int argc, char* argv[])
{
	std::cout << "Starting TexturePackerExporter\n";

	std::cout << "Current directory: '" << std::experimental::filesystem::current_path().string() << "'.\n";

	for (int i = 1; i < argc; ++i)
		std::cout << '\'' << argv[i] << "' ";

	std::cout << '\n';

	try
	{
		fs::path headerOutPath;
		fs::path sheetInPath;
		fs::path exportOutPath;
		std::string nspace;
		fs::path assetRoot;
		bool forceExport = false;

		gen::CommandLineArgs::parse(
			argc,
			argv,
			[&](const std::string& key, const std::string& value)
			{
				if (key == "--out-header")
				{
					if (!headerOutPath.empty())
						throw InvalidCmdLineArgumentException("--out-header specified twice.");

					headerOutPath = value;
				}
				else if (key == "--in")
				{
					if (!sheetInPath.empty())
						throw InvalidCmdLineArgumentException("--in specified twice.");

					sheetInPath = value;
				}
				else if (key == "--out-folder")
				{
					if (!exportOutPath.empty())
						throw InvalidCmdLineArgumentException("--out-folder specified twice.");

					exportOutPath = value;
				}
				else if (key == "--namespace")
				{
					if (!nspace.empty())
						throw InvalidCmdLineArgumentException("--namespace specified twice.");

					nspace = value;
				}
				else if (key == "--root")
				{
					if (!assetRoot.empty())
						throw InvalidCmdLineArgumentException("--root specified twice.");

					assetRoot = value;
				}
				else if (key == "--force-export")
				{
					if (forceExport)
						throw InvalidCmdLineArgumentException("--force-export specified twice.");

					forceExport = true;
				}
				else
				{
					throw InvalidCmdLineArgumentException("Unkown directive " + key + ".");
				}
			}
		);

		const auto& spriteFiles = exportSprites(
			exportOutPath,
			sheetInPath,
			forceExport
		);

		gen::Indexer::generateHeader(
			headerOutPath,
			nspace,
			spriteFiles,
			assetRoot,
			forceExport
		);
	}
	catch (const std::exception& e)
	{
		ERROR_OUT(
			LOG_TAG,
			"Error while running exporter: '%s'.", 
			e.what()
		);

		std::cout << "Error running TexturePackerExporter: \"" << e.what() << "\"\n";

		return -1;
	}
	
	std::cout << "TexturePackerExporter success.\n";

	return 0;
}
