#pragma once

#include <Exception.hpp>
#include <experimental/filesystem>
#include <vector>
#include <string>

namespace pm {

PM_MAKE_EXCEPTION_CLASS(ParsingException, Exception);

std::vector<std::experimental::filesystem::path> exportSprites(
	const std::experimental::filesystem::path& outputFolder,
	const std::experimental::filesystem::path& inputSheet,
	bool forceExport
);

}