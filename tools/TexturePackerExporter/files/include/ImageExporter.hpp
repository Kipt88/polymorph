#pragma once

#include <Exception.hpp>
#include <experimental/filesystem>
#include <vector>
#include <string>

constexpr const char* IMAGE_OUTPUT_FILE = "images.hpp";

void exportImages(
	const std::experimental::filesystem::path& outputFile,
	const std::vector<std::experimental::filesystem::path>& inputFiles
);
