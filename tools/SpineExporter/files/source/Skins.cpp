#include "Skins.hpp"

namespace pm {

namespace {

math::SimilarityTransform2<float> loadTransform(const rapidjson::Value& jsonValue)
{
	math::SimilarityTransform2<float> trans = math::SimilarityTransform2<float>::IDENTITY;

	{
		auto it = jsonValue.FindMember("x");
		if (it != jsonValue.MemberEnd())
		{
			if (!it->value.IsNumber())
				throw BoneLoadingException("\"x\" is not a number.");

			trans.translation[0] = static_cast<float>(it->value.GetDouble());
		}
	}

	{
		auto it = jsonValue.FindMember("y");
		if (it != jsonValue.MemberEnd())
		{
			if (!it->value.IsNumber())
				throw BoneLoadingException("\"y\" is not a number.");

			trans.translation[1] = static_cast<float>(it->value.GetDouble());
		}
	}

	{
		auto it = jsonValue.FindMember("rotation");
		if (it != jsonValue.MemberEnd())
		{
			if (!it->value.IsNumber())
				throw BoneLoadingException("\"rotation\" is not a number.");

			trans.rotation = static_cast<float>(math::degToRad(it->value.GetDouble()));
		}
	}

	// TODO Create AffineTransform type and read scaleX, scaleY, shearX and shearY.

	{
		auto it = jsonValue.FindMember("scaleX");
		if (it != jsonValue.MemberEnd())
		{
			if (!it->value.IsNumber())
				throw BoneLoadingException("\"scaleX\" is not a number.");

			trans.scale = static_cast<float>(it->value.GetDouble());
		}
	}

	return trans;
}

std::vector<SkinSlotAttachment> loadSlotAttachments(
	const rapidjson::Value& jsonSlotAttachments,
	Attachments& attachments
)
{
	if (!jsonSlotAttachments.IsObject())
		throw SkinLoadingException("Skin slot attachments is not an object.");

	std::vector<SkinSlotAttachment> skinSlotAttachments;

	for (auto it = jsonSlotAttachments.MemberBegin(); it != jsonSlotAttachments.MemberEnd(); ++it)
	{
		if (!it->value.IsObject())
			throw SkinLoadingException("Attachment is not object.");

		SkinSlotAttachment attachment;

		attachment.attachment = attachments.acquire(it->name.GetString());
		attachment.setupTransform = loadTransform(it->value);

		skinSlotAttachments.push_back(attachment);
	}

	return skinSlotAttachments;
}

std::multimap<slot_handle_t, SkinSlotAttachment> loadSkinSlots(
	const rapidjson::Value& jsonSkinSlot,
	const Slots& slots,
	Attachments& attachments
)
{
	if (!jsonSkinSlot.IsObject())
		throw SkinLoadingException("Skin slot is not an object.");

	// jsonSkinSlot = "skins".$skin_name

	std::multimap<slot_handle_t, SkinSlotAttachment> skinSlotAttachments;

	for (auto it = jsonSkinSlot.MemberBegin(); it != jsonSkinSlot.MemberEnd(); ++it)
	{
		slot_handle_t slotIndex = slots.find(it->name.GetString());

		if (slotIndex == SLOT_NULL)
			throw SkinLoadingException("Can't find slot " + std::string(it->name.GetString()));

		// it = skins.$skin_name.$slotName

		for (const auto& slotAttachment : loadSlotAttachments(it->value, attachments))
		{
			skinSlotAttachments.insert(
				std::make_pair(
					slotIndex,
					slotAttachment
				)
			);
		}
	}

	return skinSlotAttachments;
}

Skin loadSkin(
	const std::string& name,
	const rapidjson::Value& jsonSkinSlot,
	const Slots& slots,
	Attachments& attachments
)
{
	if (!jsonSkinSlot.IsObject())
		throw SkinLoadingException("Skin slot is no an object.");

	Skin skin;
	skin.name = name;
	skin.slotAttachments = loadSkinSlots(jsonSkinSlot, slots, attachments);

	return skin;
}

}

auto Skins::get(skin_handle_t handle) -> Skin&
{
	return _skins[handle];
}

auto Skins::get(skin_handle_t handle) const -> const Skin&
{
	return _skins[handle];
}

auto Skins::find(const std::string& name) const -> skin_handle_t
{
	auto it = std::find_if(
		begin(),
		end(),
		[&](const Skin& b)
		{
			return b.name == name;
		}
	);

	if (it == end())
		return SKIN_NULL;
	
	return std::distance(begin(), it);
}

auto Skins::begin() -> iterator
{
	return _skins.begin();
}

auto Skins::begin() const -> const_iterator
{
	return _skins.begin();
}

auto Skins::end() -> iterator
{
	return _skins.end();
}

auto Skins::end() const -> const_iterator
{
	return _skins.end();
}

auto Skins::add(const Skin& skin) -> skin_handle_t
{
	_skins.push_back(skin);

	return _skins.size() - 1;
}

auto Skins::size() const -> std::size_t
{
	return _skins.size();
}

Skins Skins::load(const rapidjson::Document& json, const Slots& slots, Attachments& attachments)
{
	Skins skins;

	auto jsonSkins = json.FindMember("skins");
	if (jsonSkins != json.MemberEnd())
	{
		if (!jsonSkins->value.IsObject())
			throw SkinLoadingException("Skins is not an object.");

		for (auto it = jsonSkins->value.MemberBegin(); it != jsonSkins->value.MemberEnd(); ++it)
		{
			// it = "skins".$skin_name

			skins._skins.push_back(loadSkin(it->name.GetString(), it->value, slots, attachments));
		}
	}

	return skins;
}

}