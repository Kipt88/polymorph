#include "Document.hpp"

namespace pm {

Document Document::load(const rapidjson::Document& json)
{
	Document doc;

	doc.bones = Bones::load(json);
	doc.slots = Slots::load(json, doc.bones, doc.attachments);
	doc.skins = Skins::load(json, doc.slots, doc.attachments);
	doc.animations = Animations::load(json, doc.bones, doc.slots, doc.attachments);

	return doc;
}

}