#include "Animations.hpp"

namespace pm {

namespace {

KeyFrame loadKeyFrame(const rapidjson::Value& json)
{
	if (!json.IsObject())
		throw AnimationLoadException("Rotation key frame is not object.");

	KeyFrame frame;

	{
		auto it = json.FindMember("time");
		if (it == json.MemberEnd())
			throw AnimationLoadException("Key frame is missing time.");

		frame.t = static_cast<float>(it->value.GetDouble());
	}

	{
		auto it = json.FindMember("curve");
		if (it != json.MemberEnd())
		{
			if (it->value.IsString())
			{
				std::string type = it->value.GetString();

				if (type == "stepped")
					frame.type = KeyFrame::Type::CURVE_STEPPED;
				else if (type == "linear")
					frame.type = KeyFrame::Type::CURVE_LINEAR;
				else
					throw AnimationLoadException("Unrecognized animation curve type.");
			}
			else if (it->value.IsArray())
			{
				if (it->value.Size() != 4)
					throw AnimationLoadException("Unrecognized animation curve, array size is not 4.");

				for (unsigned int i = 0; i < 4; ++i)
				{
					if (!it->value[i].IsNumber())
						throw AnimationLoadException("Bezier curve value is not number.");

					frame.bezier[i] = static_cast<float>(it->value[i].GetDouble());
				}
			}
			else
			{
				throw AnimationLoadException("Unrecognized animation curve.");
			}
		}
		else
		{
			frame.type = KeyFrame::Type::CURVE_LINEAR;
		}
	}

	return frame;
}

RotateKeyFrame loadRotateKeyFrame(const rapidjson::Value& json)
{
	auto keyFrame = loadKeyFrame(json);

	RotateKeyFrame frame;

	frame.t = keyFrame.t;
	frame.bezier = keyFrame.bezier;
	frame.type = keyFrame.type;

	{
		auto it = json.FindMember("angle");
		if (it == json.MemberEnd())
			throw AnimationLoadException("Key frame is missing angle.");

		frame.angle = static_cast<float>(it->value.GetDouble());
	}

	return frame;
}

TranslateKeyFrame loadTranslateKeyFrame(const rapidjson::Value& json)
{
	auto keyFrame = loadKeyFrame(json);

	TranslateKeyFrame frame;

	frame.t = keyFrame.t;
	frame.bezier = keyFrame.bezier;
	frame.type = keyFrame.type;

	{
		auto it = json.FindMember("x");
		if (it == json.MemberEnd())
			throw AnimationLoadException("Key frame is missing x.");

		frame.x = static_cast<float>(it->value.GetDouble());
	}

	{
		auto it = json.FindMember("y");
		if (it == json.MemberEnd())
			throw AnimationLoadException("Key frame is missing y.");

		frame.y = static_cast<float>(it->value.GetDouble());
	}

	return frame;
}

ScaleKeyFrame loadScaleKeyFrame(const rapidjson::Value& json)
{
	auto keyFrame = loadKeyFrame(json);

	ScaleKeyFrame frame;

	frame.t = keyFrame.t;
	frame.bezier = keyFrame.bezier;
	frame.type = keyFrame.type;

	{
		auto it = json.FindMember("x");
		if (it == json.MemberEnd())
			throw AnimationLoadException("Key frame is missing x.");

		frame.scale = static_cast<float>(it->value.GetDouble());
	}

	// TODO: Implement affine transformation...

	return frame;
}

BoneAnimation loadBoneAnimation(
	const std::string& name, 
	const rapidjson::Value& jsonBoneAnimation, 
	const Bones& bones
)
{
	BoneAnimation animation;

	animation.bone = bones.find(name);
	if (animation.bone == BONE_NULL)
		throw AnimationLoadException("Can't find bone for animation.");

	{
		auto it = jsonBoneAnimation.FindMember("rotate");
		if (it != jsonBoneAnimation.MemberEnd())
		{
			if (!it->value.IsArray())
				throw AnimationLoadException("Rotate key frames is not array.");

			for (unsigned int i = 0; i < it->value.Size(); ++i)
				animation.rotate.push_back(loadRotateKeyFrame(it->value[i]));
		}
	}

	{
		auto it = jsonBoneAnimation.FindMember("translate");
		if (it != jsonBoneAnimation.MemberEnd())
		{
			if (!it->value.IsArray())
				throw AnimationLoadException("Translate key frames is not array.");

			for (unsigned int i = 0; i < it->value.Size(); ++i)
				animation.translate.push_back(loadTranslateKeyFrame(it->value[i]));
		}
	}

	{
		auto it = jsonBoneAnimation.FindMember("scale");
		if (it != jsonBoneAnimation.MemberEnd())
		{
			if (!it->value.IsArray())
				throw AnimationLoadException("Scale key frames is not array.");

			for (unsigned int i = 0; i < it->value.Size(); ++i)
				animation.scale.push_back(loadScaleKeyFrame(it->value[i]));
		}
	}

	return animation;
}

AttachmentKeyFrame loadAttachmentKeyFrame(const rapidjson::Value& json, const Attachments& attachments)
{
	if (!json.IsObject())
		throw AnimationLoadException("Attachment key frame is not object.");

	AttachmentKeyFrame frame;

	{
		auto it = json.FindMember("time");
		if (it == json.MemberEnd())
			throw AnimationLoadException("Can't find time of attachment key frame.");

		if (!it->value.IsNumber())
			throw AnimationLoadException("Attachment key frame time is not number.");

		frame.t = static_cast<float>(it->value.GetDouble());
	}

	{
		auto it = json.FindMember("name");
		if (it == json.MemberEnd())
			throw AnimationLoadException("Can't find name of attachment key frame.");

		if (!it->value.IsString())
			throw AnimationLoadException("Attachment name is not string.");

		frame.attachment = attachments.find(it->value.GetString());

		if (frame.attachment == ATTACHMENT_NULL)
			throw AnimationLoadException("Can't find attachment for attachment key frame.");
	}

	return frame;
}

SlotAnimation loadSlotAnimation(
	const std::string& name,
	const rapidjson::Value& jsonSlotAnimation,
	const Slots& slots,
	const Attachments& attachments
)
{
	SlotAnimation animation;

	animation.slot = slots.find(name);
	if (animation.slot == SLOT_NULL)
		throw AnimationLoadException("Can't find slot for animation.");

	{
		auto it = jsonSlotAnimation.FindMember("attachment");
		if (it != jsonSlotAnimation.MemberEnd())
		{
			if (!it->value.IsArray())
				throw AnimationLoadException("Attachment is not array.");

			for (unsigned int i = 0; i < it->value.Size(); ++i)
				animation.attachment.push_back(loadAttachmentKeyFrame(it->value[i], attachments));
		}
	}

	return animation;
}

Animation loadAnimation(
	const std::string& name, 
	const rapidjson::Value& jsonAnimation, 
	const Bones& bones,
	const Slots& slots,
	const Attachments& attachments
)
{
	if (!jsonAnimation.IsObject())
		throw AnimationLoadException("Animation isn't an object.");

	Animation animation;

	animation.name = name;

	{
		auto it = jsonAnimation.FindMember("bones");
		if (it != jsonAnimation.MemberEnd())
		{
			if (!it->value.IsObject())
				throw AnimationLoadException("Bones animation is not an object.");

			for (auto aIt = it->value.MemberBegin(); aIt != it->value.MemberEnd(); ++aIt)
			{
				animation.bones.push_back(
					loadBoneAnimation(
						aIt->name.GetString(),
						aIt->value,
						bones
					)
				);
			}
		}
	}

	{
		auto it = jsonAnimation.FindMember("slots");
		if (it != jsonAnimation.MemberEnd())
		{
			if (!it->value.IsObject())
				throw AnimationLoadException("Slots animation is not an object.");

			for (auto aIt = it->value.MemberBegin(); aIt != it->value.MemberEnd(); ++aIt)
			{
				animation.slots.push_back(
					loadSlotAnimation(
						aIt->name.GetString(),
						aIt->value,
						slots,
						attachments
					)
				);
			}
		}
	}

	return animation;
}

}

auto Animations::get(animation_handle_t handle) -> Animation&
{
	return _animations[handle];
}

auto Animations::get(animation_handle_t handle) const -> const Animation&
{
	return _animations[handle];
}

auto Animations::find(const std::string& name) const -> animation_handle_t
{
	auto it = std::find_if(
		begin(),
		end(),
		[&](const Animation& b)
		{
			return b.name == name;
		}
	);

	if (it == end())
		return BONE_NULL;

	return std::distance(begin(), it);
}

auto Animations::begin() -> iterator
{
	return _animations.begin();
}

auto Animations::begin() const -> const_iterator
{
	return _animations.begin();
}

auto Animations::end() -> iterator
{
	return _animations.end();
}

auto Animations::end() const -> const_iterator
{
	return _animations.end();
}

auto Animations::add(const Animation& bone) -> animation_handle_t
{
	_animations.push_back(bone);

	return _animations.size() - 1;
}

auto Animations::size() const -> std::size_t
{
	return _animations.size();
}

Animations Animations::load(
	const rapidjson::Document& json, 
	const Bones& bones, 
	const Slots& slots,
	const Attachments& attachments
)
{
	Animations animations;

	auto jsonAnimations = json.FindMember("animations");
	if (jsonAnimations != json.MemberEnd())
	{
		if (!jsonAnimations->value.IsObject())
			throw AnimationLoadException("Animations is not an object.");

		for (auto it = jsonAnimations->value.MemberBegin(); it != jsonAnimations->value.MemberEnd(); ++it)
		{
			animations._animations.push_back(
				loadAnimation(
					it->name.GetString(), 
					it->value, 
					bones,
					slots,
					attachments
				)
			);
		}
	}

	return animations;
}

}