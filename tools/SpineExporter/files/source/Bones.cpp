#include "Bones.hpp"

namespace pm {

namespace {

math::SimilarityTransform2<float> loadTransform(const rapidjson::Value& jsonValue)
{
	math::SimilarityTransform2<float> trans = math::SimilarityTransform2<float>::IDENTITY;

	{
		auto it = jsonValue.FindMember("x");
		if (it != jsonValue.MemberEnd())
		{
			if (!it->value.IsNumber())
				throw BoneLoadingException("\"x\" is not a number.");

			trans.translation[0] = static_cast<float>(it->value.GetDouble());
		}
	}

	{
		auto it = jsonValue.FindMember("y");
		if (it != jsonValue.MemberEnd())
		{
			if (!it->value.IsNumber())
				throw BoneLoadingException("\"y\" is not a number.");

			trans.translation[1] = static_cast<float>(it->value.GetDouble());
		}
	}

	{
		auto it = jsonValue.FindMember("rotation");
		if (it != jsonValue.MemberEnd())
		{
			if (!it->value.IsNumber())
				throw BoneLoadingException("\"rotation\" is not a number.");

			trans.rotation = static_cast<float>(math::degToRad(it->value.GetDouble()));
		}
	}

	// TODO Create AffineTransform type and read scaleX, scaleY, shearX and shearY.

	{
		auto it = jsonValue.FindMember("scaleX");
		if (it != jsonValue.MemberEnd())
		{
			if (!it->value.IsNumber())
				throw BoneLoadingException("\"scaleX\" is not a number.");

			trans.scale = static_cast<float>(it->value.GetDouble());
		}
	}

	return trans;
}

Bone loadBone(const rapidjson::Value& jsonValue, Bones& bones)
{
	if (!jsonValue.IsObject())
		throw BoneLoadingException("Bone is not json object.");

	Bone bone;

	bone.setupTransform = loadTransform(jsonValue);

	{
		auto it = jsonValue.FindMember("name");
		if (it == jsonValue.MemberEnd())
			throw BoneLoadingException("Bone is miss \"name\".");

		if (!it->value.IsString())
			throw BoneLoadingException("Bone \"name\" is not string.");

		bone.name = it->value.GetString();
	}

	{
		auto it = jsonValue.FindMember("parent");
		if (it != jsonValue.MemberEnd())
		{
			if (!it->value.IsString())
				throw BoneLoadingException("Bone \"parent\" is not string.");

			bone.parent = bones.find(it->value.GetString());

			if (bone.parent == BONE_NULL)
				throw BoneLoadingException("Seems bone child was specified before bone.");
		}
		else
		{
			bone.parent = BONE_NULL;
		}
	}

	return bone;
}

}

auto Bones::get(bone_handle_t handle) -> Bone&
{
	return _bones[handle];
}

auto Bones::get(bone_handle_t handle) const -> const Bone&
{
	return _bones[handle];
}

auto Bones::find(const std::string& name) const -> bone_handle_t
{
	auto it = std::find_if(
		begin(),
		end(),
		[&](const Bone& b)
		{
			return b.name == name;
		}
	);

	if (it == end())
		return BONE_NULL;
	
	return std::distance(begin(), it);
}

auto Bones::begin() -> iterator
{
	return _bones.begin();
}

auto Bones::begin() const -> const_iterator
{
	return _bones.begin();
}

auto Bones::end() -> iterator
{
	return _bones.end();
}

auto Bones::end() const -> const_iterator
{
	return _bones.end();
}

auto Bones::add(const Bone& bone) -> bone_handle_t
{
	_bones.push_back(bone);

	return _bones.size() - 1;
}

auto Bones::size() const -> std::size_t
{
	return _bones.size();
}

Bones Bones::load(const rapidjson::Document& json)
{
	Bones bones;

	auto jsonBones = json.FindMember("bones");
	if (jsonBones == json.MemberEnd())
		throw BoneLoadingException("Couldn't find \"bones\".");

	if (!jsonBones->value.IsArray())
		throw BoneLoadingException("Bones is not an array.");

	bones._bones.reserve(jsonBones->value.Size());

	for (unsigned int i = 0; i < jsonBones->value.Size(); ++i)
		bones._bones.push_back(loadBone(jsonBones->value[i], bones));

	return bones;
}

}