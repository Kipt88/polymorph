#include "Exporter.hpp"

#include <gen/CommandLineArgs.hpp>

#include <Debug.hpp>
#include <Exception.hpp>

#include <experimental/filesystem>
#include <iostream>

using namespace pm;

namespace fs = std::experimental::filesystem;

PM_MAKE_EXCEPTION_CLASS(InvalidCmdLineArgumentException, Exception);

namespace {

const char* LOG_TAG = "SpineExporter";

};

int main(int argc, char* argv[])
{
	std::cout << "Starting SpineExporter\n";
	std::cout << "Current directory: '" << fs::current_path().string() << "'.\n";

	for (int i = 1; i < argc; ++i)
		std::cout << '\'' << argv[i] << "' ";

	std::cout << std::endl;

	try
	{
		fs::path input;
		fs::path output;
		bool forceExport = false;

		gen::CommandLineArgs::parse(
			argc, 
			argv,
			[&](const std::string& key, const std::string& value)
			{
				if (key == "--in")
				{
					if (!input.empty())
						throw InvalidCmdLineArgumentException("Multiple --in parameters.");

					input = value;
				}
				else if (key == "--out")
				{
					if (!output.empty())
						throw InvalidCmdLineArgumentException("Multiple '--out' parameters.");

					output = value;	
				}
				else if (key == "--force-export")
				{
					if (forceExport)
						throw InvalidCmdLineArgumentException("Multiple '--force-export' parameters.");
					
					forceExport = true;	
				}
				else
				{
					throw InvalidCmdLineArgumentException("Unrecognized parameter " + key);
				}
			}
		);

		if (output.empty())
			throw InvalidCmdLineArgumentException("Missing '--out' parameter.");

		if (input.empty())
			throw InvalidCmdLineArgumentException("Missing '--in' parameter.");

		exportSpine(input, output, forceExport);
	}
	catch (const std::exception& e)
	{
		ERROR_OUT(
			LOG_TAG,
			"Error while running exporter: %s",
			e.what()
		);

		std::cout << "Error running SpineExporter: \"" << e.what() << "\"" << std::endl;
		
		return -1;
	}

	return 0;
}