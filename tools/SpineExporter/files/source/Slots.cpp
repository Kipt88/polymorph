#include "Slots.hpp"

namespace pm {

namespace {

Slot loadSlot(const rapidjson::Value& jsonSlot, const Bones& bones, Attachments& attachments)
{
	Slot slot;

	{
		auto it = jsonSlot.FindMember("name");
		if (it == jsonSlot.MemberEnd())
			throw SlotLoadingException("Slot is missing \"name\".");

		if (!it->value.IsString())
			throw SlotLoadingException("Slot name is not string.");

		slot.name = it->value.GetString();
	}

	{
		auto it = jsonSlot.FindMember("bone");
		if (it == jsonSlot.MemberEnd())
			throw SlotLoadingException("Slot is missing \"bone\".");

		if (!it->value.IsString())
			throw SlotLoadingException("Slot bone is not a string.");

		slot.bone = bones.find(it->value.GetString());
		
		if (slot.bone == BONE_NULL)
			throw SlotLoadingException("Can't find slot's bone \"" + std::string(it->value.GetString()) + "\".");
	}

	{
		auto it = jsonSlot.FindMember("attachment");
		if (it == jsonSlot.MemberEnd())
			throw SlotLoadingException("Slot is missing \"attachment\".");

		if (!it->value.IsString())
			throw SlotLoadingException("Slot attachment is not string.");

		slot.attachment = attachments.acquire(it->value.GetString());
	}

	return slot;
}

}

auto Slots::get(slot_handle_t handle) -> Slot&
{
	return _slots[handle];
}

auto Slots::get(slot_handle_t handle) const -> const Slot&
{
	return _slots[handle];
}

auto Slots::find(const std::string& name) const -> slot_handle_t
{
	auto it = std::find_if(
		begin(),
		end(),
		[&](const Slot& b)
		{
			return b.name == name;
		}
	);

	if (it == end())
		return SLOT_NULL;

	return std::distance(begin(), it);
}

auto Slots::begin() -> iterator
{
	return _slots.begin();
}

auto Slots::begin() const -> const_iterator
{
	return _slots.begin();
}

auto Slots::end() -> iterator
{
	return _slots.end();
}

auto Slots::end() const -> const_iterator
{
	return _slots.end();
}

auto Slots::add(const Slot& slot) -> slot_handle_t
{
	_slots.push_back(slot);

	return _slots.size() - 1;
}

auto Slots::size() const -> std::size_t
{
	return _slots.size();
}

Slots Slots::load(const rapidjson::Document& json, const Bones& bones, Attachments& attachments)
{
	Slots slots;

	auto jsonSlots = json.FindMember("slots");
	if (jsonSlots != json.MemberEnd())
	{
		if (!jsonSlots->value.IsArray())
			throw SlotLoadingException("Slots is not an array.");

		slots._slots.reserve(jsonSlots->value.Size());

		for (unsigned int i = 0; i < jsonSlots->value.Size(); ++i)
			slots._slots.push_back(loadSlot(jsonSlots->value[i], bones, attachments));
	}

	return slots;
}

}