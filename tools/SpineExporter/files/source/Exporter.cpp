#include "Exporter.hpp"

#include "Document.hpp"

#include <math/Transform2.hpp>
#include <filesystem/FileInputStream.hpp>
#include <filesystem/FileOutputStream.hpp>
#include <io/Writer.hpp>
#include <io/Reader.hpp>
#include <Exception.hpp>
#include <Id.hpp>
#include <rapidjson/document.h>
#include <algorithm>
#include <iostream>
#include <map>
#include <cctype>

namespace fs = std::experimental::filesystem;

namespace pm {

PM_MAKE_EXCEPTION_CLASS(ParsingException, Exception);

namespace {

bool needExport(const fs::path& inputFile, const fs::path& outputFile)
{
	if (!fs::exists(outputFile))
		return true;

	auto lastExportTime = fs::last_write_time(outputFile);
	auto lastEditTime = fs::last_write_time(inputFile);

	if (lastExportTime < lastEditTime)
		return true;

	return false;
}

Document load(const fs::path& input)
{
	rapidjson::Document jsonDoc;

	{
		std::vector<char> data;

		{
			filesystem::FileInputStream jsonInput(input.string());

			data.resize(jsonInput.available());
			io::readBlock(jsonInput, data.data(), data.size());
			data.push_back('\0');

			jsonDoc.Parse(data.data());
		}

		if (jsonDoc.HasParseError())
		{
			auto err = jsonDoc.GetParseError();

			throw ParsingException(std::string("JSON Parse error: ") + std::to_string(err) + " " + data.data());
		}

		if (!jsonDoc.IsObject())
			throw ParsingException("JSON is not object.");
	}

	return Document::load(jsonDoc);
}

void save(filesystem::FileOutputStream& output, const Document& document)
{
	{
		const auto& attachments = document.attachments;

		std::cout << "Attachment size: " << attachments.size() << '\n';
		output << static_cast<std::uint16_t>(attachments.size());

		for (const auto& attachment : attachments)
			output << hash(attachment.name);
	}

	{
		const auto& bones = document.bones;

		std::cout << "Bones size: " << bones.size() << '\n';

		output << static_cast<std::uint16_t>(bones.size());

		for (const auto& bone : bones)
		{
			output << hash(bone.name);

			if (bone.parent == BONE_NULL)
				output << static_cast<std::uint16_t>(-1);
			else
				output << static_cast<std::uint16_t>(bone.parent);

			output << bone.setupTransform;
		}
	}

	{
		const auto& slots = document.slots;

		std::cout << "Slots size: " << slots.size() << '\n';

		output << static_cast<std::uint16_t>(slots.size());

		for (const auto& slot : slots)
		{
			output << hash(slot.name);
			output << static_cast<std::uint16_t>(slot.attachment);
			output << static_cast<std::uint16_t>(slot.bone);
		}
	}

	{
		const auto& skins = document.skins;

		std::cout << "Skins size: " << skins.size() << '\n';

		output << static_cast<std::uint16_t>(skins.size());

		for (const auto& skin : skins)
		{
			output << hash(skin.name);

			output << static_cast<std::uint16_t>(skin.slotAttachments.size());

			for (const auto& slotAttachment : skin.slotAttachments)
			{
				output << static_cast<std::uint16_t>(slotAttachment.first);
				output << static_cast<std::uint16_t>(slotAttachment.second.attachment);
				output << slotAttachment.second.setupTransform;
			}
		}
	}

	{
		const auto& animations = document.animations;

		std::cout << "Animations size: " << animations.size() << '\n';

		output << static_cast<std::uint16_t>(animations.size());

		for (const auto& animation : animations)
		{
			output << hash(animation.name);

			output << static_cast<std::uint16_t>(animation.bones.size());

			for (const auto& boneAnimation : animation.bones)
			{
				output << static_cast<std::uint16_t>(boneAnimation.bone);
				
				output << static_cast<std::uint16_t>(boneAnimation.translate.size());
				for (const auto& translateFrame : boneAnimation.translate)
					output << translateFrame;

				output << static_cast<std::uint16_t>(boneAnimation.rotate.size());
				for (const auto& rotateFrame : boneAnimation.rotate)
					output << rotateFrame;

				output << static_cast<std::uint16_t>(boneAnimation.scale.size());
				for (const auto& scaleFrame : boneAnimation.scale)
					output << scaleFrame;
			}

			output << static_cast<std::uint16_t>(animation.slots.size());

			for (const auto& slotFrame : animation.slots)
			{
				output << static_cast<std::uint16_t>(slotFrame.slot);
				output << static_cast<std::uint16_t>(slotFrame.attachment.size());

				for (const auto& attachmentFrame : slotFrame.attachment)
				{
					output << attachmentFrame.t;
					output << static_cast<std::uint16_t>(attachmentFrame.attachment);
				}
			}
		}
	}
}

}

void exportSpine(const fs::path& input, const fs::path& output, bool forceExport)
{
	if (forceExport || needExport(input, output))
	{
		std::cout << "Exporting '" << input << "'.\n";
		
		auto doc = load(input);

		// Seems some frames are redundant in the spine json format, could check that here and remove them.

		filesystem::FileOutputStream dataOut(output.string());

		save(dataOut, doc);
	}
	else
	{
		std::cout << "Not exporting '" << input << "', no need.\n";
	}
}

}