#include "Attachments.hpp"

#include <algorithm>

namespace pm {

auto Attachments::acquire(const std::string& name) -> attachment_handle_t
{
	auto it = std::find_if(
		begin(), 
		end(), 
		[&](const Attachment& attachment) 
		{ 
			return attachment.name == name; 
		}
	);

	attachment_handle_t handle = std::distance(begin(), it);

	if (it == end())
		_attachments.push_back({ name });

	return handle;
}

auto Attachments::find(const std::string& name) const -> attachment_handle_t
{
	auto it = std::find_if(
		begin(), 
		end(), 
		[&](const Attachment& attachment) 
		{ 
			return attachment.name == name; 
		}
	);

	if (it == end())
		return ATTACHMENT_NULL;

	return std::distance(begin(), it);
}

auto Attachments::get(attachment_handle_t handle) -> Attachment&
{
	return _attachments[handle];
}

auto Attachments::get(attachment_handle_t handle) const -> const Attachment&
{
	return _attachments[handle];
}

auto Attachments::begin() -> iterator
{
	return _attachments.begin();
}

auto Attachments::begin() const -> const_iterator
{
	return _attachments.begin();
}

auto Attachments::end() -> iterator
{
	return _attachments.end();
}

auto Attachments::end() const -> const_iterator
{
	return _attachments.end();
}

auto Attachments::size() const -> std::size_t 
{
	return _attachments.size();
}

}
