#pragma once

#include <experimental/filesystem>

namespace pm {

void exportSpine(
	const std::experimental::filesystem::path& input,
	const std::experimental::filesystem::path& output,
	bool forceExport
);

}