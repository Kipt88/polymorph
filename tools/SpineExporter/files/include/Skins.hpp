#pragma once

#include "Attachments.hpp"
#include "Slots.hpp"
#include <math/Transform2.hpp>
#include <map>
#include <vector>
#include <string>

namespace pm {

PM_MAKE_EXCEPTION_CLASS(SkinLoadingException, Exception);

typedef std::uint64_t skin_handle_t;
constexpr skin_handle_t SKIN_NULL = -1;

struct SkinSlotAttachment
{
	attachment_handle_t attachment;
	math::SimilarityTransform2<float> setupTransform;
};

struct Skin
{
	std::string name;
	std::multimap<slot_handle_t, SkinSlotAttachment> slotAttachments;
};

class Skins
{
public:
	typedef std::vector<Skin>::iterator iterator;
	typedef std::vector<Skin>::const_iterator const_iterator;

	Skin& get(skin_handle_t handle);
	const Skin& get(skin_handle_t handle) const;

	skin_handle_t find(const std::string& name) const;

	iterator begin();
	const_iterator begin() const;

	iterator end();
	const_iterator end() const;

	skin_handle_t add(const Skin& skin);

	std::size_t size() const;

	static Skins load(const rapidjson::Document& json, const Slots& slots, Attachments& attachments);

private:
	std::vector<Skin> _skins;
};

}