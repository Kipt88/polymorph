#pragma once

#include <math/Transform2.hpp>
#include <Exception.hpp>
#include <rapidjson/document.h>
#include <vector>
#include <string>

namespace pm {

PM_MAKE_EXCEPTION_CLASS(BoneLoadingException, Exception);

typedef std::uint64_t bone_handle_t;
constexpr bone_handle_t BONE_NULL = -1;

struct Bone
{
	std::string name;
	bone_handle_t parent;

	math::SimilarityTransform2<float> setupTransform;
};

class Bones
{
public:
	typedef std::vector<Bone>::iterator iterator;
	typedef std::vector<Bone>::const_iterator const_iterator;

	Bone& get(bone_handle_t handle);
	const Bone& get(bone_handle_t handle) const;

	bone_handle_t find(const std::string& name) const;

	iterator begin();
	const_iterator begin() const;

	iterator end();
	const_iterator end() const;

	bone_handle_t add(const Bone& bone);

	std::size_t size() const;

	static Bones load(const rapidjson::Document& json);

private:
	std::vector<Bone> _bones;
};

}