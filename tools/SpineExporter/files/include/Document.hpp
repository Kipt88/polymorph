#pragma once

#include "Attachments.hpp"
#include "Bones.hpp"
#include "Skins.hpp"
#include "Slots.hpp"
#include "Animations.hpp"
#include <rapidjson/document.h>

namespace pm {

struct Document
{
	Attachments attachments;
	Bones bones;
	Slots slots;
	Skins skins;
	Animations animations;

	static Document load(const rapidjson::Document& json);
};

}