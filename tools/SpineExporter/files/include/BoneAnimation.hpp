#pragma once

#include "KeyFrame.hpp"
#include "Bones.hpp"
#include <vector>

namespace pm {

struct RotateKeyFrame : KeyFrame
{
	float angle;
};

struct TranslateKeyFrame : KeyFrame
{
	float x;
	float y;
};

struct ScaleKeyFrame : KeyFrame
{
	float scale;
};

struct BoneAnimation
{
	bone_handle_t bone;

	std::vector<RotateKeyFrame> rotate;
	std::vector<TranslateKeyFrame> translate;
	std::vector<ScaleKeyFrame> scale;
};

}