#pragma once

#include "Attachments.hpp"
#include "Bones.hpp"
#include <Exception.hpp>
#include <rapidjson/document.h>

namespace pm {

PM_MAKE_EXCEPTION_CLASS(SlotLoadingException, Exception);

typedef std::uint64_t slot_handle_t;
constexpr slot_handle_t SLOT_NULL = -1;

struct Slot
{
	std::string name;
	attachment_handle_t attachment;
	bone_handle_t bone;
};

class Slots
{
public:
	typedef std::vector<Slot>::iterator iterator;
	typedef std::vector<Slot>::const_iterator const_iterator;

	Slot& get(slot_handle_t handle);
	const Slot& get(slot_handle_t handle) const;

	slot_handle_t find(const std::string& name) const;

	iterator begin();
	const_iterator begin() const;

	iterator end();
	const_iterator end() const;

	slot_handle_t add(const Slot& slot);

	std::size_t size() const;

	static Slots load(const rapidjson::Document& json, const Bones& bones, Attachments& attachments);

private:
	std::vector<Slot> _slots;
};

}