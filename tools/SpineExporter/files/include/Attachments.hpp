#pragma once

#include <vector>
#include <string>

namespace pm {

typedef std::uint64_t attachment_handle_t;
constexpr attachment_handle_t ATTACHMENT_NULL = -1;

struct Attachment
{
	std::string name;
};

class Attachments
{
public:
	typedef std::vector<Attachment>::iterator iterator;
	typedef std::vector<Attachment>::const_iterator const_iterator;

	attachment_handle_t acquire(const std::string& name);
	attachment_handle_t find(const std::string& name) const;

	Attachment& get(attachment_handle_t handle);
	const Attachment& get(attachment_handle_t handle) const;

	iterator begin();
	const_iterator begin() const;

	iterator end();
	const_iterator end() const;

	std::size_t size() const;

private:
	std::vector<Attachment> _attachments;
};

}