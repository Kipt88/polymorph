#pragma once

#include "Slots.hpp"
#include "Attachments.hpp"
#include <vector>

namespace pm {

struct AttachmentKeyFrame
{
	attachment_handle_t attachment;
	float t;
};

// TODO: Add color key frame.

struct SlotAnimation
{
	slot_handle_t slot;

	std::vector<AttachmentKeyFrame> attachment;
};

}