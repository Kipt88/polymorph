#pragma once

#include "BoneAnimation.hpp"
#include "SlotAnimation.hpp"
#include "Bones.hpp"
#include <string>

namespace pm {

PM_MAKE_EXCEPTION_CLASS(AnimationLoadException, Exception);

typedef std::uint64_t animation_handle_t;

struct Animation
{
	std::string name;

	std::vector<BoneAnimation> bones;
	std::vector<SlotAnimation> slots;

	// TODO Implement events...
};

class Animations
{
public:
	typedef std::vector<Animation>::iterator iterator;
	typedef std::vector<Animation>::const_iterator const_iterator;

	Animation& get(animation_handle_t handle);
	const Animation& get(animation_handle_t handle) const;

	animation_handle_t find(const std::string& name) const;

	iterator begin();
	const_iterator begin() const;

	iterator end();
	const_iterator end() const;

	animation_handle_t add(const Animation& animation);

	std::size_t size() const;

	static Animations load(
		const rapidjson::Document& json, 
		const Bones& bones, 
		const Slots& slots,
		const Attachments& attachments
	);

private:
	std::vector<Animation> _animations;
};

}