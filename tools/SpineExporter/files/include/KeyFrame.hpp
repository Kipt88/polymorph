#pragma once

#include <array>

namespace pm {

struct KeyFrame
{
	enum class Type : std::uint8_t
	{
		CURVE_LINEAR = 0,
		CURVE_STEPPED,
		CURVE_BEZIER
	} type;

	std::array<float, 4> bezier; // Curve parameters.

	float t;
};

}