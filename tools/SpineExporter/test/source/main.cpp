#include <os/InputSystem.hpp>
#include <animation/AnimationReader_ANIM.hpp>
#include <filesystem/FileInputStream.hpp>
#include <Debug.hpp>
#include <experimental/filesystem>


int main()
{
	using namespace pm;
	namespace fs = std::experimental::filesystem;

	try
	{
		fs::path p = fs::current_path() / ".." / ".." / "test" / "out_animations" / "test.anim";
		filesystem::FileInputStream input(p.string());
		animation::AnimationReader_ANIM::read(input);
	}
	catch (const std::exception& e)
	{
		ERROR_OUT("PhysicsBodyExporter_test", "Error loading body: '%s'", e.what());
		return -1;
	}

	return 0;
}