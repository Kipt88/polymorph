#include "shader_info/Exporter.hpp"

#include <filesystem/FileOutputStream.hpp>
#include <os/Window.hpp>
#include <gr/Surface.hpp>
#include <Debug.hpp>

#include <windows.h>

using namespace pm;

int APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR lpCmdLine, int)
{
	os::Window window("Shader info");

	{
		gr::Surface surface(window.nativeHandle());

		filesystem::FileOutputStream out("shader_info_out.txt", true);

		DEBUG_OUT("WinMain", "Command line argument '%s'", lpCmdLine);

		return shader_info::exportData(surface, out, lpCmdLine);
	}
}
