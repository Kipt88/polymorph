#include "shader_info/ShaderReader.hpp"

#include <filesystem/FileInputStream.hpp>
#include <vector>
#include <algorithm>

namespace pm { namespace shader_info {

namespace {

auto parseShaderType(std::string::const_iterator& it, std::string::const_iterator end)
{
	if (std::distance(it, end) < 4)
		throw CommandLineException();

	if (*it == '-')
	{
		const char type = *(it + 1);

		switch (type)
		{
		case 'v':
			it += 3;
			return gr::ShaderType::VERTEX_SHADER;

		case 'p':
			it += 3;
			return gr::ShaderType::PIXEL_SHADER;
		}
	}

	throw CommandLineException();
}

auto parseShaderPath(std::string::const_iterator& it, std::string::const_iterator end)
{
	if (*it != '"' || std::distance(it, end) < 3)
		throw CommandLineException();

	auto endQuote = std::find(it + 1, end, '"');

	if (endQuote == end)
		throw CommandLineException();

	std::string path(it + 1, endQuote);

	if (endQuote + 1 == end)
		it = end;
	else
		it = endQuote + 2;

	return path;
}

auto parseCmdLine(const std::string& cmdLine)
{
	std::vector<std::pair<gr::ShaderType, std::string>> shaderPaths;

	auto it = cmdLine.begin();

	while (it != cmdLine.end())
	{
		auto shaderType = parseShaderType(it, cmdLine.end());
		auto shaderPath = parseShaderPath(it, cmdLine.end());

		shaderPaths.push_back(std::make_pair(shaderType, std::move(shaderPath)));
	}

	return shaderPaths;
}

auto readShaderFile(const std::string& path)
{
	filesystem::FileInputStream in(path);

	std::vector<char> content;
	content.resize(in.available());

	in.read(content.data(), 1, 1, in.available());

	return std::string(content.begin(), content.end());
}

}

gr::ShaderSuite createShader(gr::Surface& surface, const std::string& cmdLine)
{
	auto lock = surface.activate();

	std::vector<gr::ShaderHandle> shaders;

	const auto& paths = parseCmdLine(cmdLine);

	for (const auto& path : paths)
	{
		shaders.push_back(
			std::make_shared<gr::Shader>(readShaderFile(path.second), path.first)
		);
	}

	return gr::ShaderSuite(shaders.begin(), shaders.end());
}

} }
