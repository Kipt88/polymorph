#include "shader_info/Exporter.hpp"

#include "shader_info/ShaderReader.hpp"

#include <os/Window.hpp>
#include <gr/Surface.hpp>


namespace pm { namespace shader_info {

namespace {

std::string typeTagToString(gr::TypeTag tag)
{
	switch (tag)
	{
	case gr::TypeTag::FLOAT:
		return "FLOAT";
	case gr::TypeTag::FLOAT_2:
		return "FLOAT_2";
	case gr::TypeTag::FLOAT_3:
		return "FLOAT_3";
	case gr::TypeTag::FLOAT_4:
		return "FLOAT_4";
	case gr::TypeTag::FLOAT_2X2:
		return "FLOAT_2X2";
	case gr::TypeTag::FLOAT_3X3:
		return "FLOAT_3X3";
	case gr::TypeTag::FLOAT_4X4:
		return "FLOAT_4X4";
	case gr::TypeTag::INT:
		return "INT";
	case gr::TypeTag::INT_2:
		return "INT_2";
	case gr::TypeTag::INT_3:
		return "INT_3";
	case gr::TypeTag::INT_4:
		return "INT_4";
	case gr::TypeTag::UNSIGNED_INT:
		return "UNSIGNED_INT";
	case gr::TypeTag::UNSIGNED_INT_2:
		return "UNSIGNED_INT_2";
	case gr::TypeTag::UNSIGNED_INT_3:
		return "UNSIGNED_INT_3";
	case gr::TypeTag::UNSIGNED_INT_4:
		return "UNSIGNED_INT_4";
	}

	std::terminate();
}

}

int exportData(gr::Surface& surface, io::OutputStream& out, const std::string& cmdLine)
{
	try
	{
		auto shaderSuite = shader_info::createShader(surface, cmdLine);

		{
			auto lock = surface.activate();
			const auto& metaData = shaderSuite.getMetaData();

			out << std::string("[BLOCKS]\n");

			for (unsigned int i = 0; i < metaData.blocks.size(); ++i)
			{
				const auto& block = metaData.blocks[i];

				out << std::string("index(") << std::to_string(i) << std::string(") ")
					<< std::string("name(") << block.name << std::string(")\n");
			}

			out << std::string("[Variables]\n");

			for (unsigned int i = 0; i < metaData.variables.size(); ++i)
			{
				const auto& var = metaData.variables[i];

				if (var.blockIndex == -1)
				{
					out << std::string("location(") 
						<< std::to_string(i) 
						<< std::string(") ");
				}
				else
				{
					out << std::string("block(") 
						<< std::to_string(var.blockIndex) 
						<< std::string(") ")
						<< std::string("offset(") 
						<< std::to_string(var.offset) 
						<< std::string(") ");
				}

				out << std::string("name(") 
					<< var.name 
					<< std::string(") ")
					<< std::string("type(") 
					<< typeTagToString(var.type) 
					<< std::string(")\n");
			}
		}
	}
	catch (const Exception& e)
	{
		ERROR_OUT("ShaderInfo", "Error: %s", e.what());
		out << e.what();
		return 1;
	}

	return 0;
}

} }
