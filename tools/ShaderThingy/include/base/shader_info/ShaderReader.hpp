#pragma once

#include <gr/ShaderSuite.hpp>
#include <gr/Surface.hpp>
#include <Exception.hpp>
#include <string>

namespace pm { namespace shader_info {

PM_MAKE_EXCEPTION_CLASS(CommandLineException, Exception);

gr::ShaderSuite createShader(gr::Surface& surface, const std::string& cmdLine);

} }
