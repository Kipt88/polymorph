#pragma once

#include <gr/Surface.hpp>
#include <io/IOStream.hpp>
#include <string>

namespace pm { namespace shader_info {

int exportData(gr::Surface& surface, io::OutputStream& out, const std::string& cmdLine);

} }
