#include <gen/Indexer.hpp>
#include <gen/CommandLineArgs.hpp>
#include <Exception.hpp>
#include <Debug.hpp>
#include <filesystem.hpp>
#include <vector>
#include <iostream>

using namespace pm;

namespace fs = std::filesystem;

PM_MAKE_EXCEPTION_CLASS(InvalidCmdLineArgumentException, Exception);

int main(int argc, char* argv[])
{
	std::cout << "Starting ResourceIndexer\n";

	std::cout << "Current directory: '" << fs::current_path().string() << "'.";

	for (int i = 1; i < argc; ++i)
		std::cout << "\n\'" << argv[i] << "' ";

	std::cout << '\n';

	try
	{
		fs::path headerOut;
		std::string nspace;
		std::vector<fs::path> resources;
		fs::path root;
		bool forceExport = false;

		gen::CommandLineArgs::parse(
			argc,
			argv,
			[&](const std::string& key, const std::string& value)
			{
				if (key == "--force-export")
				{
					if (forceExport)
						throw InvalidCmdLineArgumentException("--force-export specified twice.");

					forceExport = true;
				}
				else if (key == "--out")
				{
					if (!headerOut.empty())
						throw InvalidCmdLineArgumentException("--out specified twice.");

					headerOut = value;
				}
				else if (key == "--namespace")
				{
					if (!nspace.empty())
						throw InvalidCmdLineArgumentException("--namespace specified twice.");

					nspace = value;
				}
				else if (key == "--res")
				{
					resources.push_back(value);
				}
				else if (key == "--root")
				{
					root = value;
				}
				else
				{
					throw InvalidCmdLineArgumentException("Unknown directive " + key + ".");
				}
			}
		);

		if (headerOut.empty())
			throw InvalidCmdLineArgumentException("--out not specified.");

		if (nspace.empty())
			throw InvalidCmdLineArgumentException("--namespace not specified.");

		gen::Indexer::generateHeader(
			headerOut,
			nspace,
			resources,
			root,
			forceExport
		);
	}
	catch (const std::exception& e)
	{
		std::cerr << "ResourceIndexer failure: \"" << e.what() << "\"\n";

		return -1;
	}
	
	std::cout << "ResourceIndexer success.\n";

	return 0;
}
