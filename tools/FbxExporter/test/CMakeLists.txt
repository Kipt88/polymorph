cmake_minimum_required(VERSION 3.3)

set(dependent_modules "animation" "os")

project("FbxExporter_test")

add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/../files" "${CMAKE_CURRENT_SOURCE_DIR}/../build/${PLATFORM}")

set(
	fbx_files
	"fbx_files/humanoid.fbx"
)

set(
	source_files
	"sources/main.cpp"
)

require_modules(${dependent_modules})

add_executable("FbxExporter_test" WIN32 ${source_files} ${fbx_files})

target_include_directories("FbxExporter_test" PRIVATE "gen_include")
target_link_libraries("FbxExporter_test" PRIVATE ${dependent_modules})
