#include "AnimationExporter.hpp"
#include <Debug.hpp>
#include <Exception.hpp>
#include <experimental/filesystem>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cstdio>

using namespace pm;

PM_MAKE_EXCEPTION_CLASS(InvalidCmdLineArgumentException, Exception);

namespace {

const char* LOG_TAG = "FbxExporter";

struct CommandLineArguments
{
	explicit CommandLineArguments(int argc, char* argv[])
	{
		if (argc < 2)
			throw InvalidCmdLineArgumentException("No arguments provided.");

		outputPath = argv[1];

		enum
		{
			EXPECT_FLAG_OR_END,
			EXPECT_PATH
		} parseState = EXPECT_FLAG_OR_END;

		for (int i = 2; i < argc; ++i)
		{
			switch (parseState)
			{
			case EXPECT_FLAG_OR_END:
				if (std::strcmp(argv[i], "--file") == 0)
					parseState = EXPECT_PATH;
				else
					throw InvalidCmdLineArgumentException("Expected '--file' as parameter " + std::to_string(i - 1) + ", got '" + argv[i] + "'.");
				break;

			case EXPECT_PATH:
				fbxFiles.push_back(argv[i]);
				parseState = EXPECT_FLAG_OR_END;
				break;
			}
		}

		if (parseState != EXPECT_FLAG_OR_END)
			throw InvalidCmdLineArgumentException("Expected path, got end of arguments.");
	}

	std::vector<std::experimental::filesystem::path> fbxFiles;
	std::experimental::filesystem::path outputPath;
};

}

int main(int argc, char* argv[])
{
	std::cout << "Starting FbxExporter\n";

	std::cout << "Current directory: '" << std::experimental::filesystem::current_path().string() << "'.\n";

	for (int i = 1; i < argc; ++i)
		std::cout << '\'' << argv[i] << "' ";

	std::cout << '\n';

	try
	{
		CommandLineArguments cmdArguments(argc, argv);

		exportAnimations(cmdArguments.outputPath, cmdArguments.fbxFiles);

		/*
		auto imageOutputFile = cmdArguments.outputPath / IMAGE_OUTPUT_FILE;

		if (std::experimental::filesystem::exists(imageOutputFile))
		{
			// Don't bother exporting if images are older than export.

			auto it = std::max_element(
				cmdArguments.fbxFiles.begin(),
				cmdArguments.fbxFiles.end(),
				[](const std::experimental::filesystem::path& p1,
				   const std::experimental::filesystem::path& p2)
				{
					return std::experimental::filesystem::last_write_time(p1)
						< std::experimental::filesystem::last_write_time(p2);
				}
			);

			if (it != cmdArguments.fbxFiles.end())
			{
				auto lastImageModification = std::experimental::filesystem::last_write_time(*it);
				auto lastExportTime = std::experimental::filesystem::last_write_time(imageOutputFile);

				if (lastExportTime < lastImageModification)
				{
					exportImages(
						imageOutputFile,
						cmdArguments.fbxFiles
					);
				}
				else
				{
					INFO_OUT(LOG_TAG, "Not exporting animations, no changes.");
					std::cout << "Not exporting animations, no changes.\n";
				}
			}
		}
		else
		{
			exportImages(
				imageOutputFile,
				cmdArguments.images
			);
		}
		*/
	}
	catch (const Exception& e)
	{
		ERROR_OUT(
			LOG_TAG,
			"Error while running exporter: '%s'.", 
			e.what()
		);

		std::cout << "Error running FbxExporter: " << e.what() << '\n';

		return -1;
	}
	
	std::cout << "FbxExporter success.\n";

	return 0;
}
