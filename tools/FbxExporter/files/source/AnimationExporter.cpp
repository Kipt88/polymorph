#include "AnimationExporter.hpp"

#include <animation/Skeleton.hpp>
#include <Debug.hpp>
#include <Exception.hpp>
#include <fbxsdk.h>
#include <iostream>
#include <map>
#include <vector>

//
#include <concurrency/Thread.hpp>
#include <os/Window.hpp>
#include <gr/Surface.hpp>
#include <gr/VertexBuffer.hpp>
#include <gr/VertexList.hpp>
//

PM_MAKE_EXCEPTION_CLASS(ImportError, pm::Exception);

using namespace pm;

namespace {

const char* LOG_TAG = "FbxExporter";

std::pair<gr::VertexBuffer, gr::IndexBuffer> 
	createBuffer(gr::Surface& surface, const animation::Skeleton& skeleton)
{
	gr::VertexList<gr::VertexFormat::V3> vl(skeleton.size());
	std::vector<gr::index_t> indices;

	for (unsigned int i = 0; i < skeleton.size(); ++i)
	{
		auto joint = skeleton.joint(i);

		auto v = gr::Vector3_r::ZERO;

		for (auto j = joint; j; j = j.parent())
		{
			j.invBindPose().inverse().transform(v);
		}

		vl.push_back({ v });

		if (auto p = joint.parent())
		{
			indices.push_back(vl.size() - 1);
			indices.push_back(p.index());
		}
	}

	{
		auto lock = surface.activate();

		return std::make_pair(
			gr::VertexBuffer(
				vl.formatData(),
				gr::BufferUsage::STATIC,
				vl.size(),
				vl.data()
			),
			gr::IndexBuffer(
				gr::BufferUsage::STATIC,
				indices.size(),
				indices.data()
			)
		);
	}
}

void previewSkeleton(const animation::Skeleton& skeleton)
{
	concurrency::Thread runner = [skeleton]()
	{
		os::Window win("Preview");

		win.show();

		bool quitFlag = false;

		auto scope = win.addWindowEventListener<os::window_events::Quit>(
			[&]() { quitFlag = true; }
		);

		{
			gr::Surface surface(win.nativeHandle());

			auto buffers = createBuffer(surface, skeleton);

			float angle = 0.0f;

			while (!quitFlag)
			{
				while (win.pollEvent());

				{
					auto lock = surface.activate();

					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

					glMatrixMode(GL_PROJECTION);
					glLoadIdentity();
					glOrtho(-300.0f, 300.0f, -300.0f, 300.0f, -800.0f, 800.0f);
					
					glMatrixMode(GL_MODELVIEW);
					glLoadIdentity();
					glRotatef(angle, 0.0f, 1.0f, 0.0f);
					// glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);

					buffers.first.draw(gr::Primitive::LINES, buffers.second);
				}

				surface.flipBuffers();

				concurrency::Thread::sleep(33ms);

				angle += 0.1f;
			}
		}
	};

	runner.join();
}

FbxScene* importScene(FbxManager* fbxManager, const std::experimental::filesystem::path& path)
{
	FbxImporter* importer = FbxImporter::Create(fbxManager, "");

	{
		auto* ios = FbxIOSettings::Create(fbxManager, IOSROOT);

		fbxManager->SetIOSettings(ios);
	}

	if (!importer->Initialize(path.string().c_str(), -1, fbxManager->GetIOSettings()))
	{
		importer->Destroy();
		throw ImportError(importer->GetStatus().GetErrorString());
	}

	FbxScene* scene = FbxScene::Create(fbxManager, "");
	importer->Import(scene);

	importer->Destroy();

	return scene;
}

std::vector<FbxNode*> findSkeletonRoots(FbxScene* scene)
{
	std::vector<FbxNode*> roots;

	for (int i = 0; i < scene->GetNodeCount(); ++i)
	{
		auto* node = scene->GetNode(i);

		if (auto* skeleton = node->GetSkeleton())
		{
			if (skeleton->IsSkeletonRoot())
				roots.push_back(node);
		}
	}

	return roots;
}

auto convertFbxTransform(const FbxAMatrix& m)
{
	auto translationV = m.GetT();
	auto rotationQ = m.GetQ();
	auto scaleV = m.GetS();

	auto scale = scaleV[0];

	gr::Transform3 transform;

	transform.translation() = gr::Vector3_r{
		static_cast<gr::real_t>(translationV[0]),
		static_cast<gr::real_t>(translationV[1]),
		static_cast<gr::real_t>(translationV[2])
	};

	gr::real_t d[4] = {
		static_cast<gr::real_t>(rotationQ[0]),
		static_cast<gr::real_t>(rotationQ[1]),
		static_cast<gr::real_t>(rotationQ[2]),
		static_cast<gr::real_t>(rotationQ[3]),
	};
	transform.rotation() = gr::Quaternion_r(d);

	transform.scale() = static_cast<gr::real_t>(scale);

	return transform;
}

auto createSkeleton(const std::vector<FbxNode*>& roots)
{
	std::map<FbxNode*, animation::joint_index_t> jointIndices;
	animation::Skeleton skeleton;

	std::vector<std::pair<animation::joint_index_t, FbxNode*>> eval;

	for (auto* node : roots)
		eval.push_back(std::make_pair(animation::null_joint_index, node));

	while (!eval.empty())
	{
		auto parentIndex = eval.back().first;
		auto* node = eval.back().second;
		eval.pop_back();

		gr::Transform3 trans;
		
		if (parentIndex == animation::null_joint_index)
		{
			trans = convertFbxTransform(node->EvaluateGlobalTransform().Inverse());
		}
		else
		{
			trans = convertFbxTransform(node->EvaluateLocalTransform().Inverse());
		}

		auto index = skeleton.pushJoint(trans, parentIndex);

		jointIndices.insert(std::make_pair(node, index));

		for (int i = 0; i < node->GetChildCount(); ++i)
		{
			auto* childNode = node->GetChild(i);

			eval.push_back(std::make_pair(index, childNode));
		}
	}

	return std::make_pair(std::move(jointIndices), std::move(skeleton));
}

void exportScene(FbxManager* fbxManager, FbxScene* scene)
{
	const auto& skeletonRoots = findSkeletonRoots(scene);

	DEBUG_OUT(LOG_TAG, "Found %zu skeleton root(s).", skeletonRoots.size());

	auto r = createSkeleton(skeletonRoots);

	const auto& skeleton = r.second;

	for (unsigned int i = 0; i < skeleton.size(); ++i)
	{
		auto joint = skeleton.joint(i);

		auto v = gr::Vector3_r::ZERO;

		std::string indentation = "";
		for (auto j = joint; j; j = j.parent())
		{
			indentation += "  ";
			j.invBindPose().inverse().transform(v);
		}

		DEBUG_OUT(
			LOG_TAG,
			"%s(%.2f, %.2f, %.2f) (%.2f) (%.2f, %.2f, %.2f, %.2f)",
			indentation.c_str(),
			joint.invBindPose().translation()[0],
			joint.invBindPose().translation()[1],
			joint.invBindPose().translation()[2],
			joint.invBindPose().scale(),
			joint.invBindPose().rotation().data()[0],
			joint.invBindPose().rotation().data()[1],
			joint.invBindPose().rotation().data()[2],
			joint.invBindPose().rotation().data()[3]
		);

		DEBUG_OUT(
			LOG_TAG,
			"%s(%u) (%.2f,%.2f,%.2f)",
			indentation.c_str(),
			i,
			v[0], v[1], v[2]
		);
	}

	previewSkeleton(skeleton);

	/*
	unsigned int animatedNodes = 0;
	unsigned int compositeNodes = 0;
	int curveNodeCount = 0;

	int stackCount = scene->GetSrcObjectCount<FbxAnimStack>();
	
	DEBUG_OUT(LOG_TAG, "Animation stacks: %i", stackCount);

	for (int i = 0; i < stackCount; ++i)
	{
		auto* stack = scene->GetSrcObject<FbxAnimStack>(i);

		int layerCount = stack->GetMemberCount<FbxAnimLayer>();

		DEBUG_OUT(LOG_TAG, "Stack %i", i);
		DEBUG_OUT(LOG_TAG, "  Animation layers: %i", layerCount);

		for (int j = 0; j < layerCount; ++j)
		{
			auto* layer = stack->GetMember<FbxAnimLayer>(j);

			curveNodeCount = layer->GetMemberCount<FbxAnimCurveNode>();

			DEBUG_OUT(LOG_TAG, "  Layer %i", j);
			DEBUG_OUT(LOG_TAG, "    Curve nodes: %i", curveNodeCount);

			for (int k = 0; k < curveNodeCount; ++k)
			{
				auto* curveNode = layer->GetMember<FbxAnimCurveNode>(k);

				DEBUG_OUT(LOG_TAG, "    Curve node %i", k);
				DEBUG_OUT(LOG_TAG, "      Animated: %s", curveNode->IsAnimated() ? "true" : "false");
				DEBUG_OUT(LOG_TAG, "      Composite: %s", curveNode->IsComposite() ? "true" : "false");
				DEBUG_OUT(LOG_TAG, "      Channels: %u", curveNode->GetChannelsCount());

				for (unsigned int ii = 0; ii < curveNode->GetChannelsCount(); ++ii)
				{
					DEBUG_OUT(
						LOG_TAG, 
						"        %u %s", 
						ii, 
						static_cast<const char*>(curveNode->GetChannelName(ii))
					);

					DEBUG_OUT(
						LOG_TAG,
						"          Curves: %i", curveNode->GetCurveCount(ii)
					);

					for (int jj = 0; jj < curveNode->GetCurveCount(ii); ++jj)
					{
						auto* curve = curveNode->GetCurve(ii, jj);
						DEBUG_OUT(
							LOG_TAG,
							"            Keys: %i", curve->KeyGetCount()
						);


					}
				}

				if (curveNode->IsAnimated())
					++animatedNodes;

				if (curveNode->IsComposite())
					++compositeNodes;

				
			}
		}
	}

	DEBUG_OUT(LOG_TAG, "Animated nodes: %u/%i", animatedNodes, curveNodeCount);
	DEBUG_OUT(LOG_TAG, "Composite nodes: %u/%i", compositeNodes, curveNodeCount);
	*/

	scene->Destroy();
}

}

void exportAnimations(
	const std::experimental::filesystem::path& outPath,
	const std::vector<std::experimental::filesystem::path>& fbxFiles
)
{
	auto* fbxManager = FbxManager::Create();

	fbxManager->SetIOSettings(FbxIOSettings::Create(fbxManager, IOSROOT));

	for (const auto& path : fbxFiles)
	{
		exportScene(fbxManager, importScene(fbxManager, path));
	}

	fbxManager->Destroy();
}
