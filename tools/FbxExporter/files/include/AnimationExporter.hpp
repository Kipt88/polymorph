#pragma once

#include <vector>
#include <experimental/filesystem>

void exportAnimations(
	const std::experimental::filesystem::path& outPath,
	const std::vector<std::experimental::filesystem::path>& fbxFiles
);