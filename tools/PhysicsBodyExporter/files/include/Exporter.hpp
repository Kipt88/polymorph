#pragma once

#include <filesystem.hpp>
#include <vector>

namespace pm {

void exportBody(
	const std::filesystem::path& input,
	const std::filesystem::path& output,
	bool forceExport
);

}
