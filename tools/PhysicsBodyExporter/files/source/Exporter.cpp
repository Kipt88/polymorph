#include "Exporter.hpp"

#include <physics2d/Body.hpp>
#include <physics2d/PolygonDecomposer.hpp>
#include <filesystem/FileInputStream.hpp>
#include <filesystem/FileOutputStream.hpp>
#include <io/Writer.hpp>
#include <io/Reader.hpp>
#include <Exception.hpp>
#include <Id.hpp>
#include <rapidjson/document.h>
#include <iostream>
#include <cctype>

namespace fs = std::filesystem;

namespace pm {

PM_MAKE_EXCEPTION_CLASS(ParsingException, Exception);

namespace {

bool needExport(const fs::path& inputFile, const fs::path& outputFile)
{
	if (!fs::exists(outputFile))
		return true;

	auto lastExportTime = fs::last_write_time(outputFile);
	auto lastEditTime = fs::last_write_time(inputFile);

	if (lastExportTime < lastEditTime)
		return true;

	return false;
}

physics2d::ShapeDefinition::CircleDef readCircle(const rapidjson::Value& jsonDef)
{
	physics2d::ShapeDefinition::CircleDef def;

	auto jsonRadius = jsonDef.FindMember("radius");

	if (jsonRadius == jsonDef.MemberEnd())
		throw ParsingException("Couldn't find radius.");

	if (!jsonRadius->value.IsNumber())
		throw ParsingException("Radius is not a number.");

	def.radius = jsonRadius->value.GetDouble();

	return def;
}

physics2d::ShapeDefinition::BoxDef readBox(const rapidjson::Value& jsonDef)
{
	physics2d::ShapeDefinition::BoxDef def;

	auto jsonWidth = jsonDef.FindMember("w");
	auto jsonHeight = jsonDef.FindMember("h");

	if (jsonWidth == jsonDef.MemberEnd() || jsonHeight == jsonDef.MemberEnd())
		throw ParsingException("Width or height not specified.");

	if (!jsonWidth->value.IsNumber() || !jsonHeight->value.IsNumber())
		throw ParsingException("Width or height not number.");

	def.width = jsonWidth->value.GetDouble();
	def.height = jsonHeight->value.GetDouble();

	return def;
}

std::vector<physics2d::ShapeDefinition::PolygonDef> readAndDecomposePolygon(const rapidjson::Value& jsonDef)
{
	auto jsonVertices = jsonDef.FindMember("vertices");

	if (jsonVertices == jsonDef.MemberEnd())
		throw ParsingException("Vertices not specified.");

	if (!jsonVertices->value.IsArray())
		throw ParsingException("Vertices not array.");

	if (jsonVertices->value.Size() < 3)
		throw ParsingException("Polygon shape has less than 3 vertices.");

	std::vector<physics2d::Vector2_r> vertices;

	auto size = jsonVertices->value.Size();
	vertices.resize(size);

	for (unsigned int i = 0; i < jsonVertices->value.Size(); ++i)
	{
		if (!jsonVertices->value[i].IsArray() || jsonVertices->value[i].Size() != 2)
			throw ParsingException("Vertex not in format [x,y]");

		vertices[i][0] = jsonVertices->value[i][0].GetDouble();
		vertices[i][1] = jsonVertices->value[i][1].GetDouble();
	}

	std::vector<physics2d::ShapeDefinition::PolygonDef> polygons;


	auto decomposed = physics2d::PolygonDecomposer::decomposeToConvex(
		vertices, 
		physics2d::MAX_POLYGON_VERTICES
	);
	
	for (const auto& polygon : decomposed)
	{
		polygons.push_back(polygon);
	}

	return polygons;
}

physics2d::BodyDefinition readBody(const rapidjson::Document& doc)
{
	physics2d::BodyDefinition rVal;

	auto jsonType = doc.FindMember("type");
	if (jsonType != doc.MemberEnd())
	{
		if (!jsonType->value.IsString())
			throw ParsingException();

		std::string strType = jsonType->value.GetString();

		if (strType == "dynamic")
			rVal.type = physics2d::BodyType::DYNAMIC;
		else if (strType == "static")
			rVal.type = physics2d::BodyType::STATIC;
		else
			throw ParsingException("Unknown body type: " + std::string(strType));
	}
	else
	{
		rVal.type = physics2d::BodyType::DYNAMIC;
	}

	auto jsonBullet = doc.FindMember("bullet");
	if (jsonBullet != doc.MemberEnd())
	{
		if (!jsonBullet->value.IsBool())
			throw ParsingException("'bullet' not boolean.");

		rVal.bullet = jsonBullet->value.GetBool();
	}
	else
	{
		rVal.bullet = false;
	}

	auto jsonShapes = doc.FindMember("shapes");

	if (jsonShapes == doc.MemberEnd())
		throw ParsingException("No shapes specified.");

	if (!jsonShapes->value.IsArray())
		throw ParsingException();

	for (unsigned int i = 0; i < jsonShapes->value.Size(); ++i)
	{
		const auto& jsonShape = jsonShapes->value[i];

		if (!jsonShape.IsObject())
			throw ParsingException();

		auto jsonShapeType = jsonShape.FindMember("type");

		if (jsonShapeType == jsonShape.MemberEnd())
			throw ParsingException("Can't find shape type definition.");

		if (!jsonShapeType->value.IsString())
			throw ParsingException();

		std::string strShapeType = jsonShapeType->value.GetString();

		std::vector<physics2d::ShapeDefinition> defs;

		if (strShapeType == "circle")
		{
			defs = { physics2d::ShapeDefinition{ readCircle(jsonShape)} };
		}
		else if (strShapeType == "box")
		{
			defs = { physics2d::ShapeDefinition{ readBox(jsonShape)} };
		}
		else if (strShapeType == "polygon")
		{
			auto&& shapes = readAndDecomposePolygon(jsonShape);

			for (const auto& shape : shapes)
				defs.push_back(physics2d::ShapeDefinition{ shape });
		}
		else
		{
			throw ParsingException("Unrecognized shape type " + strShapeType);
		}

		for (auto& def : defs)
		{
			auto jsonPos = jsonShape.FindMember("pos");
			if (jsonPos != jsonShape.MemberEnd())
			{
				if (!jsonPos->value.IsArray() || jsonPos->value.Size() != 2)
					throw ParsingException("Pos is not in format [x,y].");

				def.localPos[0] = jsonPos->value[0].GetDouble();
				def.localPos[1] = jsonPos->value[1].GetDouble();
			}

			auto jsonMaterial = jsonShape.FindMember("material");
			if (jsonMaterial != jsonShape.MemberEnd())
			{
				if (!jsonMaterial->value.IsObject())
					throw ParsingException();

				auto jsonMatDensity = jsonMaterial->value.FindMember("density");
				if (jsonMatDensity == jsonMaterial->value.MemberEnd())
					throw ParsingException("Material missing density.");

				if (!jsonMatDensity->value.IsNumber())
					throw ParsingException("Material density not number.");

				def.material.density = jsonMatDensity->value.GetDouble();


				auto jsonMatFriction = jsonMaterial->value.FindMember("friction");
				if (jsonMatFriction == jsonMaterial->value.MemberEnd())
					throw ParsingException("Material missing friction.");

				if (!jsonMatFriction->value.IsNumber())
					throw ParsingException("Material friction not number.");

				def.material.friction = jsonMatFriction->value.GetDouble();


				auto jsonMatElasticity = jsonMaterial->value.FindMember("elasticity");
				if (jsonMatElasticity == jsonMaterial->value.MemberEnd())
					throw ParsingException("Material missing elasticity.");

				if (!jsonMatElasticity->value.IsNumber())
					throw ParsingException("Material elasticity not number.");

				def.material.elasticity = jsonMatElasticity->value.GetDouble();
			}

			auto jsonCollisionType = jsonShape.FindMember("collision_type");
			if (jsonCollisionType != jsonShape.MemberEnd())
			{
				if (!jsonCollisionType->value.IsInt())
					throw ParsingException("collision_type not integer.");

				def.type = static_cast<physics2d::collision_type_t>(jsonCollisionType->value.GetInt());
			}

			auto jsonFilter = jsonShape.FindMember("filter");
			if (jsonFilter != jsonShape.MemberEnd())
			{
				if (!jsonFilter->value.IsObject())
					throw ParsingException("'filter' is not an object.");

				auto jsonGroup = jsonFilter->value.FindMember("group");
				if (jsonGroup != jsonFilter->value.MemberEnd())
				{
					const auto& v = jsonGroup->value;

					if (!v.IsInt())
						throw ParsingException("'filter.group' is not integral.");

					if (v.GetInt() < 0)
						throw ParsingException("'filter.group' is negative.");

					def.filter.group = static_cast<physics2d::collision_group_t>(v.GetInt());
				}

				auto jsonCategory = jsonFilter->value.FindMember("category");
				if (jsonCategory != jsonFilter->value.MemberEnd())
				{
					const auto& v = jsonCategory->value;

					if (!v.IsString())
						throw ParsingException("'filter.category isn't string.");

					def.filter.categories = physics2d::bitmask_t{ v.GetString() };
				}

				auto jsonMask = jsonFilter->value.FindMember("mask");
				if (jsonMask != jsonFilter->value.MemberEnd())
				{
					const auto& v = jsonMask->value;

					if (!v.IsString())
						throw ParsingException("'filter.mask isn't string.");

					def.filter.mask = physics2d::bitmask_t{ v.GetString() };
				}
			}

			rVal.shapes.push_back(def);
		}
	}

	return rVal;
}

void writeBody(io::OutputStream& out, const physics2d::BodyDefinition& def)
{
	out << static_cast<std::uint32_t>(def.type);
	out << def.bullet;
	out << static_cast<std::uint32_t>(def.shapes.size());

	for (const auto& shape : def.shapes)
	{
		out << static_cast<std::uint32_t>(shape.shape);

		switch (shape.shape)
		{
		case physics2d::Shape::CIRCLE:
			out << static_cast<float>(shape.geometry.circle.radius);
			break;

		case physics2d::Shape::BOX:
			out << static_cast<float>(shape.geometry.box.width)
				<< static_cast<float>(shape.geometry.box.height);
			break;

		case physics2d::Shape::POLYGON:
			out << static_cast<std::uint32_t>(shape.geometry.polygon.size);

			for (unsigned int i = 0; i < shape.geometry.polygon.size; ++i)
			{
				out << static_cast<float>(shape.geometry.polygon.vertices[i][0])
					<< static_cast<float>(shape.geometry.polygon.vertices[i][1]);
			}
		}

		out << static_cast<float>(shape.localPos[0])
			<< static_cast<float>(shape.localPos[1]);

		out << static_cast<float>(shape.material.density)
			<< static_cast<float>(shape.material.friction)
			<< static_cast<float>(shape.material.elasticity);

		out << static_cast<std::uint64_t>(shape.type);

		out << static_cast<std::uint64_t>(shape.filter.group)
			<< static_cast<std::uint32_t>(shape.filter.categories.to_ullong())
			<< static_cast<std::uint32_t>(shape.filter.mask.to_ulong());

		// Note: userData is not persistent.
	}
}

}

void exportBody(const fs::path& input, const fs::path& output, bool forceExport)
{
	if (forceExport || needExport(input, output))
	{
		std::cout << "Exporting '" << input << "'.\n";
		rapidjson::Document jsonDoc;
		std::vector<char> data;

		{
			filesystem::FileInputStream jsonInput(input.string());

			data.resize(jsonInput.available());
			io::readBlock(jsonInput, data.data(), data.size());
			data.push_back('\0');

			jsonDoc.Parse(data.data());
		}

		if (jsonDoc.HasParseError())
		{
			auto err = jsonDoc.GetParseError();

			throw ParsingException(std::string("JSON Parse error: ") + std::to_string(err) + " " + data.data());
		}

		if (!jsonDoc.IsObject())
			throw ParsingException("JSON is not object.");

		filesystem::FileOutputStream dataOut(output.string());

		writeBody(dataOut, readBody(jsonDoc));
	}
	else
	{
		std::cout << "Not exporting '" << input << "', no need.\n";
	}
}

}
