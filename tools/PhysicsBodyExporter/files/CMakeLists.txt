cmake_minimum_required(VERSION 3.3)

project("PhysicsBodyExporter")

include("../../../modules/polymorph.cmake")

function(export_body)
	parse_arguments(
		PARAM_KEYS BODY_INPUT BODY_OUTPUT FORCE_EXPORT
		PARAM_OUTPUT_PREFIX EXPORTER
		PARAMS ${ARGV}
	)
	
	set(params)
	
	if(NOT ${EXPORTER_HAS_BODY_INPUT})
		message(FATAL_ERROR "Missing parameter BODY_INPUT")
	endif()
	
	if(NOT ${EXPORTER_HAS_BODY_OUTPUT})
		message(FATAL_ERROR "Missing parameter BODY_OUTPUT")
	endif()
	
	list(APPEND params "--out=${EXPORTER_BODY_OUTPUT}")
	list(APPEND params "--in=${EXPORTER_BODY_INPUT}")
	
	if(${EXPORTER_HAS_FORCE_EXPORT})
		list(APPEND params "--force-export")
	endif()
	
	add_custom_command(
		OUTPUT ${EXPORTER_BODY_OUTPUT}
		COMMAND "PhysicsBodyExporter" ${params}
		DEPENDS ${EXPORTER_BODY_INPUT}
		COMMENT "Running PhysicsBodyExporter"
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	)
endfunction()

set(
	dependent_modules
	"ext"
	"io"
	"filesystem"
	"physics2d"
	"gen"
)

set(
	source_files
	"source/main.cpp"
	"source/Exporter.cpp"
)

set(
	include_files
	"include/Exporter.hpp"
)

require_modules(${dependent_modules})

source_group("Sources" FILES ${source_files})
source_group("Headers" FILES ${include_files})

find_package(RapidJSON REQUIRED)

add_executable("PhysicsBodyExporter" ${source_files} ${include_files})
target_link_libraries("PhysicsBodyExporter" PRIVATE ${dependent_modules})
target_include_directories("PhysicsBodyExporter" PRIVATE ${RapidJSON_INCLUDE_DIRS} "include")

set_property(TARGET "PhysicsBodyExporter" PROPERTY FOLDER "Tools")
