#include <physics2d/BodyDefReader.hpp>
#include <Debug.hpp>

#include <filesystem/FileInputStream.hpp>
#include <experimental/filesystem>

int main()
{
	using namespace pm;
	namespace fs = std::experimental::filesystem;

	try
	{
		{
			fs::path p = fs::current_path() / ".." / ".." / "test" / "out_bodies" / "test1.body";
			filesystem::FileInputStream input(p.string());
			physics2d::BodyDefReader::read(input);
		}

		{
			fs::path p = fs::current_path() / ".." / ".." / "test" / "out_bodies" / "test2.body";
			filesystem::FileInputStream input(p.string());
			physics2d::BodyDefReader::read(input);
		}
	}
	catch (const std::exception& e)
	{
		ERROR_OUT("PhysicsBodyExporter_test", "Error loading body: '%s'", e.what());
		return -1;
	}

	return 0;
}