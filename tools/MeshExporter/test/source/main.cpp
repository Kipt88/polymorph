#include <Debug.hpp>

#include <os/InputSystem.hpp>
#include <gr/MeshReader.hpp>
#include <filesystem/FileInputStream.hpp>
#include <experimental/filesystem>

int main()
{
	using namespace pm;
	namespace fs = std::experimental::filesystem;

	try
	{
		os::InputSystem input(15ms, "Test mesh reader");
		gr::Surface surface(input);
		gr::BufferProvider buffers(surface);

		{
			fs::path p = fs::current_path() / ".." / ".." / "test" / "out_meshes" / "test.mesh";
			filesystem::FileInputStream input(p.string());
			gr::MeshReader::read(input, gr::MeshReader::mesh_t::MESH, buffers);
		}
	}
	catch (const std::exception& e)
	{
		ERROR_OUT("PhysicsBodyExporter_test", "Error loading body: '%s'", e.what());
		return -1;
	}

	return 0;
}