#include "Exporter.hpp"

#include <gr/VertexList.hpp>
#include <filesystem/FileInputStream.hpp>
#include <filesystem/FileOutputStream.hpp>
#include <io/Writer.hpp>
#include <io/Reader.hpp>
#include <Exception.hpp>
#include <Id.hpp>
#include <rapidjson/document.h>
#include <iostream>
#include <cctype>

namespace fs = std::filesystem;

namespace pm {

PM_MAKE_EXCEPTION_CLASS(ParsingException, Exception);

namespace {

struct FaceDefinition
{
	gr::Primitive primitive;
	std::vector<std::uint32_t> indices;
};

struct MeshDefinition
{
	gr::VertexList<> vertices;
	
	std::vector<FaceDefinition> faces;
};

bool needExport(const fs::path& inputFile, const fs::path& outputFile)
{
	if (!fs::exists(outputFile))
		return true;

	auto lastExportTime = fs::last_write_time(outputFile);
	auto lastEditTime = fs::last_write_time(inputFile);

	if (lastExportTime < lastEditTime)
		return true;

	return false;
}

gr::VertexFormatData parseVertexFormatData(const std::string& fmt)
{
	if (fmt == "V2")		return gr::Vertex<gr::VertexFormat::V2>::FORMAT_DATA;
	if (fmt == "V2_C3")		return gr::Vertex<gr::VertexFormat::V2_C3>::FORMAT_DATA;
	if (fmt == "V2_C4")		return gr::Vertex<gr::VertexFormat::V2_C4>::FORMAT_DATA;
	if (fmt == "V2_T2")		return gr::Vertex<gr::VertexFormat::V2_T2>::FORMAT_DATA;
	if (fmt == "V2_C3_T2")	return gr::Vertex<gr::VertexFormat::V2_C3_T2>::FORMAT_DATA;
	if (fmt == "V2_C4_T2")	return gr::Vertex<gr::VertexFormat::V2_C4_T2>::FORMAT_DATA;
	if (fmt == "V3")		return gr::Vertex<gr::VertexFormat::V3>::FORMAT_DATA;
	if (fmt == "V3_C3")		return gr::Vertex<gr::VertexFormat::V3_C3>::FORMAT_DATA;
	if (fmt == "V3_C4")		return gr::Vertex<gr::VertexFormat::V3_C4>::FORMAT_DATA;
	if (fmt == "V3_T2")		return gr::Vertex<gr::VertexFormat::V3_T2>::FORMAT_DATA;
	if (fmt == "V3_C3_T2")	return gr::Vertex<gr::VertexFormat::V3_C3_T2>::FORMAT_DATA;
	if (fmt == "V3_C4_T2")	return gr::Vertex<gr::VertexFormat::V3_C4_T2>::FORMAT_DATA;

	throw ParsingException("Unrecognized vertexFormat " + fmt + ".");
}

void addComponent(std::vector<gr::real_t>& vertex, unsigned int dimension, const rapidjson::Value& jsonC)
{
	if (!jsonC.IsArray())
		throw ParsingException("Vertex component is not an array.");

	if (jsonC.Size() != dimension)
		throw ParsingException("Vertex component size is incorrect.");

	for (unsigned int i = 0; i < dimension; ++i)
	{
		const auto& jsonScalar = jsonC[i];
		if (!jsonScalar.IsNumber())
			throw ParsingException("Vertex component has non-scalar value at " + std::to_string(i) + ".");

		vertex.push_back(static_cast<gr::real_t>(jsonScalar.GetDouble()));
	}
}

void addVertices(gr::VertexList<>& vertices, const rapidjson::Value& jsonVertices)
{
	if (!jsonVertices.IsArray())
		throw ParsingException("\"vertices\" is not an array.");

	for (unsigned int i = 0; i < jsonVertices.Size(); ++i)
	{
		std::vector<gr::real_t> vertex;
		vertex.reserve(vertices.formatData().size() / sizeof(gr::real_t));

		const auto& jsonVertex = jsonVertices[i];
		
		if (!jsonVertex.IsArray())
			throw ParsingException("Vertex " + std::to_string(i) + " is not an array.");

		unsigned int k = 0;

		if (jsonVertex.Size() < k + 1)
			throw ParsingException("Vertex " + std::to_string(i) + " is empty.");

		addComponent(vertex, vertices.formatData().vertDim, jsonVertex[k++]);

		if (vertices.formatData().colorDim)
		{
			if (jsonVertex.Size() < k + 1)
				throw ParsingException("Vertex " + std::to_string(i) + " is missing color.");

			addComponent(vertex, vertices.formatData().colorDim, jsonVertex[k++]);
		}

		if (vertices.formatData().textDim)
		{
			if (jsonVertex.Size() < k + 1)
				throw ParsingException("Vertex " + std::to_string(i) + " is missing texture coordinate.");

			addComponent(vertex, vertices.formatData().textDim, jsonVertex[k++]);
		}

		if (vertices.formatData().normalDim)
		{
			if (jsonVertex.Size() < k + 1)
				throw ParsingException("Vertex " + std::to_string(i) + " is missing normal.");

			addComponent(vertex, vertices.formatData().normalDim, jsonVertex[k++]);
		}

		if (k != jsonVertex.Size())
			throw ParsingException("Vertex " + std::to_string(i) + " has additional components.");

		vertices.push_back(vertex.data());
	}
}

gr::Primitive parsePrimitive(const std::string& prm)
{
	if (prm == "POINTS")			return gr::Primitive::POINTS;
	if (prm == "LINES")				return gr::Primitive::LINES;
	if (prm == "LINE_STRIP")		return gr::Primitive::LINE_STRIP;
	if (prm == "LINE_LOOP")			return gr::Primitive::LINE_LOOP;
	if (prm == "TRIANGLES")			return gr::Primitive::TRIANGLES;
	if (prm == "TRIANGLE_STRIP")	return gr::Primitive::TRIANGLE_STRIP;
	if (prm == "QUADS")				return gr::Primitive::QUADS;

	throw ParsingException("Unrecognized primitive " + prm + ".");
}

void addIndices(std::vector<unsigned int>& indices, const rapidjson::Value& jsonIndices)
{
	if (!jsonIndices.IsArray())
		throw ParsingException("Indices is not an array.");

	if (jsonIndices.Size() == 0)
		throw ParsingException("Indices is empty.");

	for (unsigned int i = 0; i < jsonIndices.Size(); ++i)
	{
		const auto& jsonIndex = jsonIndices[i];
		if (!jsonIndex.IsNumber())
			throw ParsingException("Index " + std::to_string(i) + " is not a number.");

		indices.push_back(jsonIndex.GetUint64());
	}
}

void addFaces(std::vector<FaceDefinition>& faces, const rapidjson::Value& jsonFaces)
{
	if (!jsonFaces.IsArray())
		throw ParsingException("\"faces\" is not an array.");

	if (jsonFaces.Size() == 0)
		throw ParsingException("\"faces\" must contain at least one face.");

	for (unsigned int i = 0; i < jsonFaces.Size(); ++i)
	{
		const auto& jsonFace = jsonFaces[i];
		if (!jsonFace.IsObject())
			throw ParsingException("Face " + std::to_string(i) + " is not an object.");

		FaceDefinition face;

		auto jsonPrimitive = jsonFace.FindMember("primitive");
		if (jsonPrimitive == jsonFace.MemberEnd())
			throw ParsingException("Face " + std::to_string(i) + " does not define \"primitive\".");

		if (!jsonPrimitive->value.IsString())
			throw ParsingException("Primitive of face " + std::to_string(i) + " is not a string.");

		face.primitive = parsePrimitive(jsonPrimitive->value.GetString());
		
		auto jsonIndices = jsonFace.FindMember("indices");
		if (jsonIndices == jsonFace.MemberEnd())
			throw ParsingException("Face " + std::to_string(i) + " is missing indices.");

		addIndices(face.indices, jsonIndices->value);
		
		faces.push_back(std::move(face));
	}
}

MeshDefinition readMesh(const rapidjson::Document& doc)
{
	auto jsonVertexFormat = doc.FindMember("vertexFormat");
	if (jsonVertexFormat == doc.MemberEnd())
		throw ParsingException("Couldn't find \"vertexFormat\".");

	if (!jsonVertexFormat->value.IsString())
		throw ParsingException("Vertex format is not string.");

	MeshDefinition def{
		gr::VertexList<>{parseVertexFormatData(jsonVertexFormat->value.GetString())},
		std::vector<FaceDefinition>{}
	};

	auto jsonVertices = doc.FindMember("vertices");
	if (jsonVertices == doc.MemberEnd())
		throw ParsingException("Couldn't find \"vertices\".");

	addVertices(def.vertices, jsonVertices->value);

	auto jsonFaces = doc.FindMember("faces");
	if (jsonFaces == doc.MemberEnd())
		throw ParsingException("Couldn't find \"faces\".");

	addFaces(def.faces, jsonFaces->value);

	return def;
}

void writeMesh(io::OutputStream& output, const MeshDefinition& mesh)
{
	output << mesh.vertices.formatData();

	output << static_cast<std::uint32_t>(mesh.vertices.size());
	output.write(
		mesh.vertices.data(), 
		std::alignment_of<gr::real_t>(), 
		mesh.vertices.formatData().size(),
		mesh.vertices.size()
	);

	output << static_cast<std::uint32_t>(mesh.faces.size());

	for (const auto& face : mesh.faces)
	{
		output << face.primitive;

		output << static_cast<std::uint32_t>(face.indices.size());
		output.write(
			face.indices.data(),
			std::alignment_of<std::uint32_t>(),
			sizeof(std::uint32_t),
			face.indices.size()
		);
	}
}

}

void exportMesh(const fs::path& input, const fs::path& output, bool forceExport)
{
	if (forceExport || needExport(input, output))
	{
		std::cout << "Exporting '" << input << "'.\n";
		rapidjson::Document jsonDoc;
		std::vector<char> data;

		{
			filesystem::FileInputStream jsonInput(input.string());

			data.resize(jsonInput.available());
			io::readBlock(jsonInput, data.data(), data.size());
			data.push_back('\0');

			jsonDoc.Parse(data.data());
		}

		if (jsonDoc.HasParseError())
		{
			auto err = jsonDoc.GetParseError();

			throw ParsingException(std::string("JSON Parse error: ") + std::to_string(err) + " " + data.data());
		}

		if (!jsonDoc.IsObject())
			throw ParsingException("JSON is not object.");

		filesystem::FileOutputStream dataOut(output.string());

		writeMesh(dataOut, readMesh(jsonDoc));
	}
	else
	{
		std::cout << "Not exporting '" << input << "', no need.\n";
	}
}

}
